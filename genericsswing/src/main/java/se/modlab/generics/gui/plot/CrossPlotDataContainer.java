package se.modlab.generics.gui.plot;

import java.util.Comparator;
import java.util.Vector;

import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.util.Sort;

public class CrossPlotDataContainer implements CrossPlottable {
	
	private String nameAx1;
	private String nameAx2;
	
	public CrossPlotDataContainer(String nameAx1, String nameAx2) {
		this.nameAx1 = nameAx1;
		this.nameAx2 = nameAx2;
	}
	
	private static class Pairs {
		public double ax1val;
		public double ax2val;
		public int shape;
		public Pairs(double ax1val, double ax2val, int shape) {
			this.ax1val = ax1val;
			this.ax2val = ax2val;
			this.shape = shape;
		}
	}
	 
	private double maxAx1;
	private double minAx1;
	private double maxAx2;
	private double minAx2;
	private Vector<Pairs> pairs = null;

	public void add(double ax1val, double ax2val) {
		add(ax1val, ax2val, CrossPlottable.CROSS);
	}

	public void add(double ax1val, double ax2val, int shape) {
		if(pairs == null) {
			pairs = new Vector<Pairs>();
			maxAx1 = ax1val;
			minAx1 = ax1val;
			maxAx2 = ax2val;
			minAx2 = ax2val;
		}
		pairs.add(new Pairs(ax1val, ax2val, shape));
		maxAx1 = Math.max(maxAx1, ax1val);
		minAx1 = Math.min(minAx1, ax1val);
		maxAx2 = Math.max(maxAx2, ax2val);
		minAx2 = Math.min(minAx2, ax2val);
	}

	public double getMaxValueAxis1() {
		return maxAx1;
	}

	public double getMinValueAxis1() {
		return minAx1;
	}

	public double getMaxValueAxis2() {
		return maxAx2;
	}

	public double getMinValueAxis2() {
		return minAx2;
	}

	public int getSize() {
		return pairs.size();
	}

	public double getValueAx1(int i) throws IntolerableException {
		return pairs.elementAt(i).ax1val;
	}

	public double getValueAx2(int i) throws IntolerableException {
		return pairs.elementAt(i).ax2val;
	}

	public int getShape(int i) throws IntolerableException {
		return pairs.elementAt(i).shape;
	}

	public String getNameAx1() {
		return nameAx1;
	}

	public String getNameAx2() {
		return nameAx2;
	}

}
