package se.modlab.generics.gui.files;

import java.awt.event.*;
import java.lang.reflect.*;

/*
  MouseEvent e

import java.awt.*;
import java.awt.event.*;

    Object o = e.getSource();
    boolean controlKey = ((InputEvent.CTRL_MASK & e.getModifiers()) != 0);
    boolean leftButton = ((InputEvent.BUTTON1_MASK & e.getModifiers()) != 0);
    boolean rightButton = ((InputEvent.BUTTON3_MASK & e.getModifiers()) != 0);
    boolean singleClick = (e.getClickCount() == 1);
    boolean doubleClick = (e.getClickCount() > 1);
*/

public class UniversalMouseListener implements MouseListener
{
  protected Object target;
  protected Object[] arguments;
  protected Method m;
  protected boolean click;
  protected boolean release;
  protected boolean enter;
  protected boolean exit;
  protected boolean press;

  public static UniversalMouseListener getClick(Object target, 
						    String methodname, 
						    Object arg)
  {
    return new UniversalMouseListener(target, methodname, arg, true, false, false, false, false);
  }
  
  public static UniversalMouseListener getRelease(Object target, 
						     String methodname, 
						     Object arg)
  {
    return new UniversalMouseListener(target, methodname, arg, false, true, false, false, false);
  }
  
  public static UniversalMouseListener getEnter(Object target, 
						   String methodname, 
						   Object arg)
  {
    return new UniversalMouseListener(target, methodname, arg, false, false, true, false, false);
  }
  
  public static UniversalMouseListener getExit(Object target, 
						  String methodname, 
						  Object arg)
  {
    return new UniversalMouseListener(target, methodname, arg, false, false, false, true, false);
  }
  
  public static UniversalMouseListener getPress(Object target, 
						   String methodname, 
						   Object arg)
  {
    return new UniversalMouseListener(target, methodname, arg, false, false, false, false, true);
  }
    
  public UniversalMouseListener(Object target, 
				   String methodname, 
				   Object arg,
				   boolean click,
				   boolean release,
				   boolean enter,
				   boolean exit,
				   boolean press)
       throws SecurityException
  {
    this.target = target;
    //this.arg = arg;
    this.click = click;
    this.release = release;
    this.enter = enter;
    this.exit = exit;
    this.press = press;
    
    // Now look up and save the Method to invoke on that target object
    Class<?> c, parameters[];
    if(target instanceof Class<?>)
      c = (Class<?>) target;
    else
      c = target.getClass();
    if(arg == null) 
      {
	parameters = new Class[1];
	parameters[0] = MouseEvent.class;
	arguments = new Object[1];
      }
    else
      {
	if(arg instanceof Object[])
	  {
	    Object[] array;
	    int len = -1;
		array = (Object[]) arg;
		len = array.length;
	    parameters = new Class[len+1];
	    arguments = new Object[len+1];
	    for(int i = 0 ; i < len ; i++)
	      {
		parameters[i] = (array[i] != null) ? array[i].getClass() : null ;
		arguments[i] = array[i];
	      }
	    parameters[len] = MouseEvent.class;
	    // arguments[len] = the mouse event object
	  }
	else
	  {
	    parameters = new Class[] { arg.getClass(), MouseEvent.class };
	    arguments = new Object[] { arg , null };
	  }
      }
    try {
	m = c.getMethod(methodname, parameters);
      }
    catch(NoSuchMethodException e)
      {
	System.out.print("UniversalMouseListener - NoSuchMethodException "+c.getName()+"."+methodname+"(");
	for(int i = 0 ; i < parameters.length-1 ; i++)
	  {
	    System.out.print(((parameters[i] != null) ? parameters[i].getName() : "null")+", ");
	  }
	if(parameters.length > 0)
	  System.out.println(((parameters[parameters.length-1] != null) ? parameters[parameters.length-1].getName() : "null")+")");
	else
	  System.out.println(")");
	e.printStackTrace();
	System.exit(1);
      }
  }
  
  public void mouseClicked(MouseEvent event) 
  {
    if(!click) return;
    if(m != null) doBeforeClick(event);
    try 
      {
	arguments[arguments.length-1] = event;
	m.invoke(target, arguments); 
      }
    catch(IllegalAccessException e) 
      {
	System.err.println("UniversalMouseListener: "+e);
	e.printStackTrace();
      }
    catch(InvocationTargetException e) 
      {
	System.err.println("UniversalMouseListener: "+e+" : "+e.getTargetException());
	e.printStackTrace();
      }
    if(m != null) doAfterClick(event);
  }

  public void mouseExited(MouseEvent event) 
  {
    if(!exit) return;
    if(m != null) doBeforeExit(event);
    try 
      { 
	arguments[arguments.length-1] = event;
	m.invoke(target, arguments); 
      }
    catch(IllegalAccessException e) 
      {
	System.err.println("UniversalMouseListener: "+e);
	e.printStackTrace();
      }
    catch(InvocationTargetException e) 
      {
	System.err.println("UniversalMouseListener: "+e);
	e.printStackTrace();
      }
    if(m != null) doAfterExit(event);
  }

  public void mouseEntered(MouseEvent event) 
  {
    if(!enter) return;
    if(m != null) doBeforeEnter(event);
    try 
      { 
	arguments[arguments.length-1] = event;
	m.invoke(target, arguments); 
      }
    catch(IllegalAccessException e) 
      {
	System.err.println("UniversalMouseListener: "+e);
	e.printStackTrace();
      }
    catch(InvocationTargetException e) 
      {
	System.err.println("UniversalMouseListener: "+e);
	e.printStackTrace();
      }
    if(m != null) doAfterEnter(event);
  }

  public void mousePressed(MouseEvent event) 
  {
    if(!press) return;
    if(m != null) doBeforePress(event);
    try 
      { 
	arguments[arguments.length-1] = event;
	m.invoke(target, arguments); 
      }
    catch(IllegalAccessException e) 
      {
	System.err.println("UniversalMouseListener: "+e);
	e.printStackTrace();
      }
    catch(InvocationTargetException e) 
      {
	System.err.println("UniversalMouseListener: "+e);
	e.printStackTrace();
      }
    if(m != null) doAfterPress(event);
  }

  public void mouseReleased(MouseEvent event) 
  {
    if(!release) return;
    if(m != null) doBeforeRelease(event);
    try 
      { 
	arguments[arguments.length-1] = event;
	m.invoke(target, arguments); 
      }
    catch(IllegalAccessException e) 
      {
	System.err.println("UniversalMouseListener: "+e);
	e.printStackTrace();
      }
    catch(InvocationTargetException e) 
      {
	System.err.println("UniversalMouseListener: "+e);
	e.printStackTrace();
      }
    if(m != null) doAfterRelease(event);
  }
  
  public static void main(String[] args)
  {
    java.awt.Frame f = new java.awt.Frame("UniversalMouseListener test");
    f.setLayout(new java.awt.FlowLayout());
    java.awt.Button b1 = new java.awt.Button("tick");
    java.awt.Button b2 = new java.awt.Button("tick");
    java.awt.Button b3 = new java.awt.Button("Close Window");
    f.add(b1);
    f.add(b2);
    f.add(b3);
    UniversalMouseListener al;
    
    al=UniversalMouseListener.getClick(b1, "setBackground", java.awt.Color.white);
    b1.addMouseListener(UniversalMouseListener.getClick(al, "aaaa", new Object[] {"tick","tack"}));
    /*    b1.addActionListener(new UniversalActionListener(b2, "setLabel", "tack"));
    b1.addActionListener(new UniversalActionListener(b3, "hide", null));
    b2.addActionListener(new UniversalActionListener(b1, "setLabel", "tack"));
    b2.addActionListener(new UniversalActionListener(b2, "setLabel", "tick"));
    b2.addActionListener(new UniversalActionListener(b3, "show", null));
    b3.addActionListener(al = new UniversalActionListener(f, "dispose", null));
    b3.addActionListener(new UniversalActionListener(al, "aaa", new Object[] {"Ett - un","Tv? - deux"}));*/
    f.pack();
    f.setVisible(true);
  }

  public void doBeforeClick(MouseEvent event)
  {
  }
  
  public void doAfterClick(MouseEvent event)
  {
  }

  public void doBeforePress(MouseEvent event)
  {
  }
  
  public void doAfterPress(MouseEvent event)
  {
  }

  public void doBeforeRelease(MouseEvent event)
  {
  }
  
  public void doAfterRelease(MouseEvent event)
  {
  }

  public void doBeforeExit(MouseEvent event)
  {
  }
  
  public void doAfterExit(MouseEvent event)
  {
  }

  public void doBeforeEnter(MouseEvent event)
  {
  }
  
  public void doAfterEnter(MouseEvent event)
  {
  }

  public void aaa(String ett, String tva)
  {
    System.out.println("aaa:"+ett+":"+tva);
    System.exit(1);
  }
  
  public void aaaa(String ett, String tva, MouseEvent e)
  {
    System.out.println("aaaa:"+ett+":"+tva+":EVENT:"+e);
    System.exit(1);
  }
  
} 
