package se.modlab.generics.gui.progress;

/** Uses a SwingWorker to perform a time-consuming (and utterly fake) task. */

/* 
 * LongTask.java is used by:
 *   ProgressBarDemo.java
 *   ProgressBarDemo2.java
 *   ProgressMonitorDemo
 */
public class LongTask implements Runnable
{
	private int lengthOfTask;
	private int current = 0;
	private boolean busy = false;
	private boolean canceled = false;
	private String barMessage;
	private String dumpMessage;

	public LongTask() 
	{
		//Compute length of task...
		//In a real program, this would figure out
		//the number of bytes to read or whatever.
		lengthOfTask = 1000;
	}

	/**
	 * Called from ProgressBarDemo to start the task.
	 */
	public void run() 
	{
		busy = true;
		current = 0;
		canceled = false;
		barMessage = null;
		dumpMessage = null;
		//Fake a long task,
		//making a random amount of progress every second.
		while(!canceled && busy) 
		{
			try 
			{
				Thread.sleep(1000); //sleep for a second
				current += Math.random() * 100; //make some progress
				if(current >= lengthOfTask) 
				{
					busy = false;
					current = lengthOfTask;
				}
				barMessage = "Completed "+((int) (current/10))+"%";
				dumpMessage = "Completed "+current+" out of "+lengthOfTask;
				//System.out.println("LongTask - ActualTask-ctor "+statMessage);
			} 
			catch(InterruptedException e)
			{
				System.out.println("ActualTask interrupted");
			}
		}
	}

	/**
	 * Called from ProgressBarDemo to find out how much work needs
	 * to be done.
	 */
	public int getLengthOfTask() {
		return lengthOfTask;
	}

	/**
	 * Called from ProgressBarDemo to find out how much has been done.
	 */
	public int getCurrent() {
		return current;
	}

	public void stop() 
	{
		canceled = true;
		busy = false;
		barMessage = null;
		dumpMessage = null;
	}

	/**
	 * Called from ProgressBarDemo to find out if the task has completed.
	 */
	public boolean isDone() {
		return !busy;
	}

	/**
	 * Returns the most recent status message, or null
	 * if there is no current status message.
	 */
	public String getMessageForBar() {
		return barMessage;
	}
	
	public String getMessageForDump() {
		return dumpMessage;
	}

	/**
	 * The actual long running task.  This runs in a SwingWorker thread.
	 */
	class ActualTask 
	{
		ActualTask() 
		{
		}
	}
}

