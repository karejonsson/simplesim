package se.modlab.generics.gui.util;

import java.util.Vector;

public class ErrorMessageCollector
{

  private Vector<String> userMessages = new Vector<String>();
  private Vector<String> internalMessage = new Vector<String>();

  public ErrorMessageCollector()
  {
  }

  public boolean addUserMessage(String msg) 
  {
    if(msg == null) return false;
    Throwable t = new Throwable();
    String s = t.getStackTrace()[1].toString();
    userMessages.addElement(s+"\n"+msg);
    return false;
  }

  public boolean addInternalMessage(String msg) 
  {
    if(msg == null) return false;
    Throwable t = new Throwable();
    String s = t.getStackTrace()[1].toString();
    internalMessage.addElement(s+"\n"+msg);
    return false;
  }

  public boolean allOk()
  {
    return ((userMessages.size() == 0) && 
            (internalMessage.size() == 0));
  }

  public int getMessageCount()
  {
    return (userMessages.size()+internalMessage.size());
  }

  public String getUserMessageAt(int idx)
  {
    if(idx < 0) return null;
    if(idx >= userMessages.size()) return null;
    return userMessages.elementAt(idx).toString();
  }

  public String getInternalMessageAt(int idx)
  {
    if(idx < 0) return null;
    if(idx >= internalMessage.size()) return null;
    return internalMessage.elementAt(idx).toString();
  }

  public String toString()
  {
    if((userMessages.size() == 0) && 
       (internalMessage.size() == 0))
    {
      return "There are no error messages";
    }
    StringBuffer sb = new StringBuffer();
    if(internalMessage.size() != 0)
    {
      sb = sb.append("There are "+internalMessage.size()+" internal messages\n\n");
      for(int i = 0 ; i < internalMessage.size() ; i++)
      {
        sb = sb.append("Message "+i+"\n "+internalMessage.elementAt(i)+"\n\n");
      }
      return sb.toString();
    }
    sb = sb.append("There are "+userMessages.size()+" user messages\n\n");
    for(int i = 0 ; i < userMessages.size() ; i++)
    {
      sb = sb.append("Message "+i+"\n "+userMessages.elementAt(i)+"\n\n");
    }
    return sb.toString();
  }

}
  
