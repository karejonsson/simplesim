package se.modlab.generics.gui.files;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: SmartTrust AB</p>
 * @author K?re Jonsson
 * @version 1.0
 */

public class TextEditorDialog extends JDialog 
{
  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
private JButton cancelButton = null;
  private JButton saveButton = null;
  private JLabel label = new JLabel();
  private JTextArea textarea = new JTextArea();
  private boolean pressedOk = true;

  public TextEditorDialog(JFrame owner,
                          String title,
                          String labelText,
                          String initialText,
                          boolean editable) 
  {
    super(owner, title, true);
    setTitle(title);
    label.setText(labelText);
    textarea.setText(initialText);
    textarea.setEditable(editable);
    init();
  }

  public void init() 
  {
    // main panel
    JPanel mainPanel = new JPanel();
    GridBagConstraints gbc = new GridBagConstraints();
    GridBagLayout gbl = new GridBagLayout();
    mainPanel.setLayout(gbl);

    mainPanel.setAlignmentX(0);
    mainPanel.setAlignmentY(0);

    // add description label
    gbc.insets = new Insets(7,7,5,5);
    gbc.anchor = GridBagConstraints.NORTHWEST;
    gbc.gridx = 0;
    gbc.gridy = 0;
    mainPanel.add(label, gbc);

    // add description text area
    gbc.gridy = 1;
    gbc.weightx = 1;
    gbc.weighty = 1;
    gbc.fill = GridBagConstraints.BOTH;
    gbc.insets = new Insets(5,5,5,5);
    textarea.setLineWrap(true);
    textarea.setWrapStyleWord(true);

    JScrollPane scroll = new JScrollPane(textarea);
    mainPanel.add(scroll, gbc);

    // add button pane
    gbc.gridx = 0;
    gbc.gridy = 2;
    gbc.weighty = 0;
    gbc.fill = GridBagConstraints.NONE;
    gbc.anchor = GridBagConstraints.EAST;
    mainPanel.add(createButtonPanel(), gbc);

    getContentPane().setLayout(new BorderLayout());
    getContentPane().add(mainPanel, BorderLayout.CENTER);
  }

  private JPanel createButtonPanel() 
  {
    // create buttons panel
    JPanel buttonPanel = new JPanel();
    buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
    buttonPanel.setAlignmentX(RIGHT_ALIGNMENT);
    buttonPanel.setAlignmentY(TOP_ALIGNMENT);

    saveButton = new JButton("OK");
    saveButton.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e) 
        {
          setVisible(false);
          dispose();
          pressedOk = true;
        }
      });

    cancelButton = new JButton("Cancel");
    cancelButton.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e) 
        {
          setVisible(false);
          dispose();
          pressedOk = false;
        }
      });

    buttonPanel.add(saveButton);
    buttonPanel.add(Box.createRigidArea(new Dimension(5, 0)));
    buttonPanel.add(cancelButton);
    return buttonPanel;
  }

  public String getText()
  {
    return textarea.getText();
  }

  public boolean getPressedOk()
  {
    return pressedOk;
  }

  public static void main(String args[])
  {
    JFrame f = new JFrame();
    TextEditorDialog ted = new TextEditorDialog(
      f,
      "TITLE",
      "LABELTEXT",
      "INITIAL TEXT",
      false);
    ted.pack();
    ted.setVisible(true);
  }

}