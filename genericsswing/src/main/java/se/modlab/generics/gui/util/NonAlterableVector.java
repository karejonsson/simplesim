package se.modlab.generics.gui.util;

import java.util.Vector;

public class NonAlterableVector
{
   
  private Vector<Object> v;

  public NonAlterableVector(Vector<Object> v)
  {
    this.v = v;
  }

  public NonAlterableVector(Object obs[])
  {
    v = new Vector<Object>();
    for(int i = 0 ; i < obs.length ; i++)
    {
      v.addElement(obs[i]);
    }
  }

  public int size()
  {
    if(v == null) return -1;
    return v.size();
  }

  public Object elementAt(int idx)
  {
    if(v == null) return null;
    if(idx < 0) return null;
    if(idx >= v.size()) return null;
    return v.elementAt(idx);
  }

}
   