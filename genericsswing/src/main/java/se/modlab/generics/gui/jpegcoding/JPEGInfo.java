package se.modlab.generics.gui.jpegcoding;

import java.io.*;
import java.awt.*;

public class JPEGInfo {
	
	private FileInputStream file_in = null;
	public String filename = "";
	public int image_width  = 0;
	public int image_height = 0;
	public int data_precision = 0;
	public int num_components = 0;
    public String jpeg_process = "";
	
	private static final int EOF     = -1;  // read() in FileInputStream returns this for EOF
	
	private static final int M_SOF0  = 0xC0;		// Start Of Frame N
	private static final int M_SOF1  = 0xC1;		// N indicates which compression process
	private static final int M_SOF2  = 0xC2;		// Only SOF0-SOF2 are now in common use
	private static final int M_SOF3  = 0xC3;
	private static final int M_SOF5  = 0xC5;		// NB: codes C4 and CC are NOT SOF markers
	private static final int M_SOF6  = 0xC6;
	private static final int M_SOF7  = 0xC7;
	private static final int M_SOF9  = 0xC9;
	private static final int M_SOF10 = 0xCA;
	private static final int M_SOF11 = 0xCB;
	private static final int M_SOF13 = 0xCD;
	private static final int M_SOF14 = 0xCE;
	private static final int M_SOF15 = 0xCF;
	private static final int M_SOI   = 0xD8;		// Start Of Image (beginning of datastream)
	private static final int M_EOI   = 0xD9;		// End Of Image (end of datastream)
	private static final int M_SOS   = 0xDA;		// Start Of Scan (begins compressed data)
	//private static final int M_APP0  = 0xE0;		// Application-specific marker, type N 
	private static final int M_APP12 = 0xEC;		// (we don't bother to list all 16 APPn's) 
	private static final int M_COM   = 0xFE;		// COMment 

	/***************************************************************************
	 *
	 * Constructor.
	 *
	 ***************************************************************************/
	public JPEGInfo( String jpeg_file ) throws JPEGFileException
	{
		filename = jpeg_file;
		try {
			File f = new File( filename );
			if ( ! f.isDirectory() && f.exists() ) {
				file_in = new FileInputStream( f );
				scan_JPEG_header(false);
				file_in.close();
			} else {
				System.out.println("File " + jpeg_file + " not found.");
			}
		} catch ( IOException ioe ) {
			ioe.printStackTrace();
		}
	}

	/***************************************************************************
	 *
	 * Return next input byte, or EOF if no more 
	 *
	 ***************************************************************************/
	private int NEXTBYTE() 
	{
		int nb = 0;
		
		try {
			nb = file_in.read();
		} catch ( IOException ioe ) {
			ioe.printStackTrace();
		}
		return nb;
	}

	/***************************************************************************
	 *
	 * Error exit handler 
	 *
	 ***************************************************************************/
	/*
	private void ERREXIT( String msg ) {
		System.err.println( msg );
		System.exit(0);
	}
	*/
	/***************************************************************************
	 *
	 * Read one byte, testing for EOF 
	 *
	 ***************************************************************************/
	private int read_1_byte() throws JPEGFileException
	{
	  int c;
	
	  c = NEXTBYTE();
	  if (c == EOF) {
	    throw new JPEGFileException("Premature EOF in JPEG file.");
	  }
	  return c;
	}

	/***************************************************************************
	 * 
	 * Read 2 bytes, convert to unsigned int 
	 * All 2-byte quantities in JPEG markers are MSB first 
	 *
	 ***************************************************************************/
	private int read_2_bytes() throws JPEGFileException
	{
	  int c1, c2;
	
	  c1 = NEXTBYTE();
	  if (c1 == EOF) {
	    throw new JPEGFileException("Premature EOF in JPEG file");
	  }

	  c2 = NEXTBYTE();
	  if (c2 == EOF) {
	    throw new JPEGFileException("Premature EOF in JPEG file.");
	  }
	  return ( (c1 << 8) +  c2 );
	}
	
	/***************************************************************************
	 *
	 * Find the next JPEG marker and return its marker code.
	 * We expect at least one FF byte, possibly more if the compressor used FFs
	 * to pad the file.
	 * There could also be non-FF garbage between markers.  The treatment of such
	 * garbage is unspecified; we choose to skip over it but emit a warning msg.
	 * NB: this routine must not be used after seeing SOS marker, since it will
	 * not deal correctly with FF/00 sequences in the compressed image data...
	 *
	 ***************************************************************************/
	private int next_marker() throws JPEGFileException
	{
	  int c;
	  int discarded_bytes = 0;
	
	  // Find 0xFF byte; count and skip any non-FFs. 
	  c = read_1_byte();
	  while (c != 0xFF) {
	    discarded_bytes++;
	    c = read_1_byte();
	  }
	  
	  // Get marker code byte, swallowing any duplicate FF bytes.  Extra FFs
	  // are legal as pad bytes, so don't count them in discarded_bytes.
	  do {
	    c = read_1_byte();
	  } while (c == 0xFF);
	
	  if (discarded_bytes != 0) {
	    System.err.println("Warning: garbage data found in JPEG file\n");
	  }
	
	  return c;
	}

	/***************************************************************************
	 *
	 * Read the initial marker, which should be SOI.
	 * For a JFIF file, the first two bytes of the file should be literally
	 * 0xFF M_SOI.  To be more general, we could use next_marker, but if the
	 * input file weren't actually JPEG at all, next_marker might read the whole
	 * file and then return a misleading error message...
	 *
	 ***************************************************************************/
	private int first_marker() throws JPEGFileException
	{
	  int c1, c2;
	
	  c1 = NEXTBYTE();
	  c2 = NEXTBYTE();
	  
	  if (c1 != 0xFF || c2 != M_SOI) {
	    throw new JPEGFileException(("Error: file \"" + filename + "\" is not a JPEG file."));
	  }
	  return c2;
	}
	
	/***************************************************************************
	 *
	 * Most types of marker are followed by a variable-length parameter segment.
	 * This routine skips over the parameters for any marker we don't otherwise
	 * want to process.
	 * Note that we MUST skip the parameter segment explicitly in order not to
	 * be fooled by 0xFF bytes that might appear within the parameter segment;
	 * such bytes do NOT introduce new markers.
	 *
	 * Skip over an unknown or uninteresting variable-length marker 
	 *
	 ***************************************************************************/
	private void skip_variable() throws JPEGFileException
	{
	  int length=0;
	
	  // Get the marker parameter length count 
	  length = read_2_bytes();
	  
	  // Length includes itself, so must be at least 2 
	  if (length < 2) {
	    throw new JPEGFileException("Erroneous JPEG marker length.");
	   }
	   
	  length -= 2;
	  
	  // Skip over the remaining bytes 
	  while (length > 0) {
	    read_1_byte();
	    length--;
	  }
	}
	
	/***************************************************************************
	 *
	 * Process a COM marker.
	 * We want to print out the marker contents as legible text;
	 * we must guard against non-text junk and varying newline representations.
	 *
	 ***************************************************************************/
	private void process_COM( boolean verbose ) throws JPEGFileException
	{
	  int length=0;
	  int ch;
	  int lastch = 0;

	  // Get the marker parameter length count 
	  length = read_2_bytes();
	  // Length includes itself, so must be at least 2 
	  if (length < 2) {
	    throw new JPEGFileException("Erroneous JPEG marker length.");
	  }
	  length -= 2;
	
	  while (length > 0) {
	    ch = read_1_byte();
	    // Emit the character in a readable form.
	    // Nonprintables are converted to \nnn form,
	    // while \ is converted to \\.
	    // Newlines in CR, CR/LF, or LF form will be printed as one newline.
	    //
	    if ( verbose ) {
		    if (ch == '\r') {
		      System.out.print("\n");
		    } else if (ch == '\n') {
		      if (lastch != '\r') {
		      	System.out.print("\n");
		      }
		    } else if (ch == '\\') {
		      System.out.print("\\\\");
		    } else if (isprint(ch)) {
		      System.out.print(ch);
		    } else {
		      //System.out.println("\\%03o", ch);
		      System.out.print(ch);
		    }
		}
	    lastch = ch;
	    length--;
	  }
	  if ( verbose ) { System.out.print("\n"); } 
	}
	
	/***************************************************************************
	 *
	 * Returns true if the character is a printable character.
	 * 
	 ***************************************************************************/
	private boolean isprint(int ch)
	{
		return ( ch >= 0x20 && ch <= 0x7E );
	}
	
	/***************************************************************************
	 *
	 * Process a SOFn marker.
	 * This code is only needed if you want to know the image dimensions...
	 *
	 ***************************************************************************/
	private void process_SOFn (int marker) throws JPEGFileException
	{
	  int length=0;
	  int ci;
	
	  length = read_2_bytes();	// usual parameter length count 
	
	  data_precision = read_1_byte();
	  image_height = read_2_bytes();
	  image_width = read_2_bytes();
	  num_components = read_1_byte();
	
	  switch (marker) {
	  case M_SOF0:	jpeg_process = "Baseline";  break;
	  case M_SOF1:	jpeg_process = "Extended sequential";  break;
	  case M_SOF2:	jpeg_process = "Progressive";  break;
	  case M_SOF3:	jpeg_process = "Lossless";  break;
	  case M_SOF5:	jpeg_process = "Differential sequential";  break;
	  case M_SOF6:	jpeg_process = "Differential progressive";  break;
	  case M_SOF7:	jpeg_process = "Differential lossless";  break;
	  case M_SOF9:	jpeg_process = "Extended sequential, arithmetic coding";  break;
	  case M_SOF10:	jpeg_process = "Progressive, arithmetic coding";  break;
	  case M_SOF11:	jpeg_process = "Lossless, arithmetic coding";  break;
	  case M_SOF13:	jpeg_process = "Differential sequential, arithmetic coding";  break;
	  case M_SOF14:	jpeg_process = "Differential progressive, arithmetic coding"; break;
	  case M_SOF15:	jpeg_process = "Differential lossless, arithmetic coding";  break;
	  default:	jpeg_process = "Unknown";  break;
	  }
	
	  if (length != (8 + num_components * 3)) {
	    throw new JPEGFileException("Bogus SOF marker length");
	  }
	
	  for (ci = 0; ci < num_components; ci++) {
	    read_1_byte();	/* Component ID code */
	    read_1_byte();	/* H, V sampling factors */
	    read_1_byte();	/* Quantization table number */
	  }
	}
	
	/***************************************************************************
	 *
	 * Parse the marker stream until SOS or EOI is seen;
	 * display any COM markers.
	 * While the companion program wrjpgcom will always insert COM markers before
	 * SOFn, other implementations might not, so we scan to SOS before stopping.
	 * If we were only interested in the image dimensions, we would stop at SOFn.
	 * (Conversely, if we only cared about COM markers, there would be no need
	 * for special code to handle SOFn; we could treat it like other markers.)
	 *
	 ***************************************************************************/
	private int scan_JPEG_header(boolean verbose) throws JPEGFileException
	{
	  int marker;
	  // Expect SOI at start of file 
	  if (first_marker() != M_SOI) {
	    throw new JPEGFileException("Expected SOI marker first.");
	  }
	
	  // Scan miscellaneous markers until we reach SOS. 
	  for (;;) {
	    marker = next_marker();
	    switch (marker) {
	      // Note that marker codes 0xC4, 0xC8, 0xCC are not, and must not be,
	      // treated as SOFn.  C4 in particular is actually DHT.
	      //
	    case M_SOF0:		// Baseline 
	    case M_SOF1:		// Extended sequential, Huffman 
	    case M_SOF2:		// Progressive, Huffman 
	    case M_SOF3:		// Lossless, Huffman 
	    case M_SOF5:		// Differential sequential, Huffman 
	    case M_SOF6:		// Differential progressive, Huffman 
	    case M_SOF7:		// Differential lossless, Huffman 
	    case M_SOF9:		// Extended sequential, arithmetic 
	    case M_SOF10:		// Progressive, arithmetic 
	    case M_SOF11:		// Lossless, arithmetic 
	    case M_SOF13:		// Differential sequential, arithmetic 
	    case M_SOF14:		// Differential progressive, arithmetic 
	    case M_SOF15:		// Differential lossless, arithmetic 
	    	if (verbose) {
	    		process_SOFn(marker);
	    		return marker; // !!!! SKIP FURTHER PROCESSING, ONLY NEED IMAGE INFO
	    	} else {
	    		skip_variable();
	    	}
	      	break;
	
	    case M_SOS:			// stop before hitting compressed data 
	      return marker;
	
	    case M_EOI:			// in case it's a tables-only JPEG stream 
	      return marker;
	
	    case M_COM:
	      process_COM(verbose);
	      break;
	
	    case M_APP12:
	      // Some digital camera makers put useful textual information into
	      // APP12 markers, so we print those out too when in -verbose mode.
	      //
	      if (verbose) {
	      	System.out.print("APP12 contains:\n");
	      	process_COM(verbose);
	      } else {
	      	skip_variable();
	      }
	      break;
	
	    default:			// Anything else just gets skipped 
	      skip_variable();	// we assume it has a parameter count...
	      break;
	    } // end switch
	  } // end loop
	}
	
	/***************************************************************************
	 *
	 * Returns a String representation of this object.
	 *
	 **************************************************************************/
	public String toString()
	{
		final String LINE_SEP = System.getProperty("line.separator");
		
		StringBuffer sb = new StringBuffer("");
		sb.append("Image width          : ").append( image_width ).append(LINE_SEP);
		sb.append("Image height         : ").append( image_height ).append(LINE_SEP);
		sb.append("Data precision       : ").append( data_precision ).append(LINE_SEP);
		sb.append("Number of components : ").append( num_components ).append(LINE_SEP);
		sb.append("JPEG process         : ").append( jpeg_process ).append(LINE_SEP);
		
		return sb.toString();
	}
	
	/** *************************************************************************
	 *
	 * Gets the width and height in a Dimension object.
	 *
	 **************************************************************************/
	public Dimension getDimension()
	{
		return new Dimension( image_width, image_height );
	}
	
	/***************************************************************************
	 *
	 * Used for testing only.
	 *
	 **************************************************************************/
	public static void main( String[] args )
	{
		if ( args.length < 1 ) {
			System.out.println("usage: JPEGInfo <jpeg_file_path>");
			System.exit(1);
		}
		
		try {
			JPEGInfo ji = new JPEGInfo( args[0] );
			System.out.println( ji.toString() );
		} catch ( Throwable nje ) {
			System.out.println(nje);
		}
		System.exit(1);
	}
};