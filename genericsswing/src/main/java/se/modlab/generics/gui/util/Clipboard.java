package se.modlab.generics.gui.util;

import java.awt.Component;
import java.awt.Desktop;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.net.URI;

import se.modlab.generics.util.OSUtils;

import javax.swing.table.AbstractTableModel;

import se.modlab.generics.gui.exceptions.UniversalTellUser;

public class Clipboard {
	
	public static void export(AbstractTableModel model) {
		StringBuffer sb = new StringBuffer();
		int colcount = model.getColumnCount();
		sb.append(model.getColumnName(0));
		for(int i = 1 ; i < colcount ; i++) {
			sb.append("\t"+model.getColumnName(i));
		}
		sb.append("\n");
		int rowcount = model.getRowCount();
		for(int r = 0 ; r < rowcount ; r++) {
			sb.append(model.getValueAt(r, 0));
			for(int c = 1 ; c < colcount ; c++) {
				sb.append("\t");
				//String value = model.getValueAt(r, c).toString();
				sb.append(model.getValueAt(r, c));
			}
			sb.append("\n");
		}
		//sb.append("\n");
		StringSelection ss = new StringSelection(sb.toString()); 
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null); 
	}
	
	public static void setClipboard(Object o) {
		StringSelection ss = new StringSelection(o == null ? null : o.toString()); 
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, ss); 
	}

	
	public static void desktopOpen(String s, Component ge) {
		//System.out.println("se.modlab.generics.util.Clipboard.desktopOpen("+s+", c)");
		if(s == null) {
			UniversalTellUser.error(ge, "No applicable value in this field for Open action");
			return;
		} 
		
		Exception ertime1 = null;
		Exception ertime2 = null;
		
		if(OSUtils.isWindows()) {
			try {
				//System.out.println("Clipboard.desktopOpen windows 1 "+s);
				String winstuff = "rundll32 SHELL32.DLL, ShellExec_RunDLL";
				Runtime.getRuntime().exec(winstuff+" "+s);
				//System.out.println("Clipboard.desktopOpen windows 1 ok");
				return;
			}
			catch(Exception e) {
				ertime1 = e;
				//universalTellUser.error(ge, e.getMessage());
			}
			try {
				//System.out.println("Clipboard.desktopOpen windows 2 "+s);
				String winstuff = "rundll32 url.dll,FileProtocolHandler";
				Runtime.getRuntime().exec(winstuff+" "+s);
				//System.out.println("Clipboard.desktopOpen windows 2 ok");
				return;
			}
			catch(Exception e) {
				ertime2 = e;
				//universalTellUser.error(ge, e.getMessage());
			}
		}
		Exception efile = null;
		try {
			//System.out.println("Clipboard.desktopOpen open "+s);
			Desktop.getDesktop().open(new File(s));
			//System.out.println("Clipboard.desktopOpen open ok");
			return;
		}
		catch(Exception e) {
			efile = e;
		}
		Exception euri = null;
		try {
			//System.out.println("Clipboard.desktopOpen browse "+s);
			Desktop.getDesktop().browse(new URI(s));
			//System.out.println("Clipboard.desktopOpen browse ok");
			return;
		}
		catch(Exception e) {
			euri = e;
			//universalTellUser.error(ge, e.getMessage());
		}
		UniversalTellUser.error(ge, "Cannot open. Error messages where\n"+
		                            "File: "+efile.getMessage()+"\n"+
				                    "URI: "+euri.getMessage()+"\n"+
		                            "Win-Shell: "+(ertime1 != null ? ertime1.getMessage() : "")+
		                            (ertime2 != null ? ertime2.getMessage() : ""));
	}
	
}
