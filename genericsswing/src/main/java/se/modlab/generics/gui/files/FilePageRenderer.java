package se.modlab.generics.gui.files;

import java.awt.*;
import java.awt.print.*;
import java.io.*;
import java.util.Vector;
import javax.swing.*;

public class FilePageRenderer extends JComponent implements Printable
{
  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
private int mCurrentPage;
  private Vector<String> mLines;
  private Vector<Vector<String>> mPages;
  private Font mFont;
  private int mFontSize;
  private Dimension mPreferredSize;

  public FilePageRenderer(File file, PageFormat pageFormat)
    throws IOException
  {
    this(new FileReader(file), pageFormat);
  }  

  public FilePageRenderer(String s, PageFormat pageFormat)
    throws IOException
  {
    this(new StringReader(s), pageFormat);
  }  

  public FilePageRenderer(Reader reader, PageFormat pageFormat)
    throws IOException
  {
    mFontSize=12;
    mFont=new Font("Serif",Font.PLAIN,mFontSize);

    BufferedReader in = new BufferedReader(reader);
    String line;
    mLines = new Vector<String>();
    while((line=in.readLine())!=null)
    {
      //System.out.println("Snurra "+line);
      mLines.addElement(line);
    }
    in.close();
    paginate(pageFormat);
  }

  public void paginate(PageFormat pageFormat)
  {
    mCurrentPage=0;
    mPages=new Vector<Vector<String>>();
    float y=mFontSize;
    Vector<String> page=new Vector<String>();
    for(int i=0;i<mLines.size();i++)
    {
      String line=(String)mLines.elementAt(i);
      page.addElement(line);
      y+=mFontSize;
      if(y + mFontSize * 2>pageFormat.getImageableHeight())
      {
        y=0;
        mPages.addElement(page);
        page=new Vector<String>();
      }
    }

    if(page.size()>0)
      mPages.addElement(page);
    mPreferredSize=new Dimension((int)
      pageFormat.getImageableWidth(),
      (int)pageFormat.getImageableHeight());
    repaint();
  }

  public void paintComponent(Graphics g)
  {
    Graphics2D g2=(Graphics2D)g;
    java.awt.geom.Rectangle2D r=new
    java.awt.geom.Rectangle2D.Float(0,0,mPreferredSize.width,mPreferredSize.height);
    g2.setPaint(Color.white);
    g2.fill(r);
    Vector<String> page=(Vector<String>)mPages.elementAt(mCurrentPage);
    g2.setFont(mFont);
    g2.setPaint(Color.black);
    float x=0;
    float y=mFontSize;
    for(int i=0;i<page.size();i++)
    {
      String line=(String)page.elementAt(i);
      if(line.length()>0)g2.drawString(line,(int)x,(int)y);
      y+=mFontSize;
    }
  }

  public int print(Graphics g,PageFormat pageFormat,int pageIndex)
  {
    if(pageIndex>=mPages.size())return NO_SUCH_PAGE;
    int savedPage=mCurrentPage;
    Graphics2D g2=(Graphics2D)g;
    g2.translate(pageFormat.getImageableX(),pageFormat.getImageableY());
    paint(g2);
    mCurrentPage=savedPage;
    return PAGE_EXISTS;
  }

  public Dimension getPreferredSize()
  {
    return mPreferredSize;
  }

  public int getCurrentPage()
  {
    return mCurrentPage;
  }

  public int getNumPages()
  {
    return mPages.size();
  }

  public void nextPage()
  {
    if(mCurrentPage < mPages.size()-1)
      mCurrentPage++;
    else 
      mCurrentPage = 0;
    repaint();
  }

  public void previousPage()
  {
    if(mCurrentPage > 0)
      mCurrentPage--;
    else
      mCurrentPage = mPages.size() - 1;
    repaint();
  }

}
