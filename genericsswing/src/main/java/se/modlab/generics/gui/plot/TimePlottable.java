package se.modlab.generics.gui.plot;

import se.modlab.generics.exceptions.IntolerableException;

public interface TimePlottable {

	public static final int BOX = 1;
	public static final int CROSS = 2;
	public static final int CIRCLE = 3;
	
	public int getSize();
	public long getTime(int i) throws IntolerableException;
	public double getValue(int i) throws IntolerableException;
	public int getShape(int i) throws IntolerableException;
	public double getMaxValue();
	public double getMinValue();
	public String getName();
	
}
 