package se.modlab.generics.gui.find;

import java.awt.*;
import java.awt.event.*;
import java.util.Vector;

import javax.swing.*;

public class AskForVisibleTextsDialog extends JDialog
{

	private static final long serialVersionUID = 1L;
	
private JButton doneButton;
  private String title; 
  private String labels[];
  private String defaultTexts[];
  protected AskForVisibleTextsAction afvta;

  private JLabel jlabels[];
  private JTextField jtextfields[];

  private boolean cancelHappened = false;
  private boolean helpHappened = false;
  private boolean doneHappened = false;

  public AskForVisibleTextsDialog(Frame frame, boolean flag, 
                                  String title, String labels[])
  {
    this(frame, flag, title, labels, null, null);
  }

  public AskForVisibleTextsDialog(Frame frame, boolean flag, 
                                  String title, String labels[], String defaultTexts[])
  {
    this(frame, flag, title, labels, defaultTexts, null);
  }

  public AskForVisibleTextsDialog(Frame frame, boolean flag, 
                                  String title, String labels[], String defaultTexts[], 
                                  AskForVisibleTextsAction afvta)
  {
    super(frame, flag);
    doneButton = null;
    this.afvta = afvta;
    this.title = title;
    if(defaultTexts == null)
    {
      defaultTexts = new String[labels.length];
      for(int i = 0 ; i < labels.length ; i++)
      {
        defaultTexts[i] = "";
      }
    }
    if(labels.length != defaultTexts.length)
    {
      JOptionPane.showConfirmDialog(frame, "Internal programming error when using 'AskForVisibleTextsDialog' ("+
         labels.length+" != "+defaultTexts.length);
      return;
    }
    this.labels = labels;
    this.defaultTexts = defaultTexts;
    initComponents();
    pack();
  }

  private void initComponents()
  {
    Container container = getContentPane();
    container.setLayout(new StringGridBagLayout());
    setTitle(title);
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent windowevent)
      {
        windowAction("Cancel");
        setVisible(false);
        dispose();
      }
    });
    
    jtextfields = new JTextField[labels.length];
    jlabels = new JLabel[labels.length];

    for(int i = 0 ; i < labels.length ; i++)
    {
      JTextField tf = new JTextField();
      jtextfields[i] = tf;
      tf.setText(defaultTexts[i]);
      JLabel jl = new JLabel();
      jlabels[i] = jl;

      jl.setLabelFor(tf);
      jl.setText(labels[i]);
      container.add("gridx=0,gridy="+i+",anchor=WEST,insets=[12,12,0,0]", jl);
      tf.setToolTipText("Type "+labels[i]+" here");
      container.add("gridx=1,gridy="+i+",fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", tf);
    }

    JPanel jpanel = createButtonPanel();
    container.add("gridx=0,gridy="+labels.length+",gridwidth=2,anchor=EAST,insets=[17,12,11,11]", jpanel);
    getRootPane().setDefaultButton(doneButton);
    //setPreferredSize(new Dimension(150, 50 + 20*labels.length));
  }

  private JPanel createButtonPanel()
  {
    JPanel jpanel = new JPanel();
    jpanel.setLayout(new BoxLayout(jpanel, 0));
    doneButton = new JButton();
    doneButton.setText("Done");
    doneButton.setToolTipText("Press Done when ready");
    doneButton.setActionCommand("Done");
    doneButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent actionevent)
      {
        windowAction(actionevent);
      }
    });
    jpanel.add(doneButton);
    jpanel.add(Box.createRigidArea(new Dimension(5, 0)));
    JButton jbutton = new JButton();
    jbutton.setText("Cancel");
    jbutton.setActionCommand("Cancel");
    jbutton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent actionevent)
      {
        windowAction(actionevent);
      }
    });
    jpanel.add(jbutton);
    jpanel.add(Box.createRigidArea(new Dimension(5, 0)));
    JButton jbutton1 = new JButton();
    jbutton1.setMnemonic('H');
    jbutton1.setText("Help");
    jbutton1.setActionCommand("Help");
    jbutton1.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent actionevent)
      {
        windowAction(actionevent);
      }
    });
    jpanel.add(jbutton1);
    Vector<JComponent> vector = new Vector<JComponent>(3);
    vector.add(jbutton);
    vector.add(jbutton1);
    vector.add(doneButton);
    equalizeComponentSizes(vector);
    vector.removeAllElements();
    return jpanel;
  }

  private void equalizeComponentSizes(java.util.List<JComponent> list)
  {
    Dimension dimension = new Dimension(0, 0);
    for(int i = 0; i < list.size(); i++)
    {
      JComponent jcomponent = (JComponent)list.get(i);
      Dimension dimension1 = jcomponent.getPreferredSize();
      dimension.width = Math.max(dimension.width, (int)dimension1.getWidth());
      dimension.height = Math.max(dimension.height, (int)dimension1.getHeight());
    }

    for(int j = 0; j < list.size(); j++)
    {
      JComponent jcomponent1 = (JComponent)list.get(j);
      jcomponent1.setPreferredSize((Dimension)dimension.clone());
      jcomponent1.setMaximumSize((Dimension)dimension.clone());
    }
  }

  private void windowAction(Object obj)
  {
    String s = null;
    if(obj != null)
    if(obj instanceof ActionEvent)
      s = ((ActionEvent)obj).getActionCommand();
    else
      s = obj.toString();
    if(s == null)
    {
      setVisible(false);
      dispose();
      return;
    }
    if(s.equals("Cancel"))
    {
      cancelHappened = true;
      if(afvta != null) afvta.cancelAction();
      setVisible(false);
      dispose();
      return;
    }
    if(s.equals("Help"))
    {
      helpHappened = true;
      if(afvta != null) afvta.helpAction();
      setVisible(false);
      dispose();
      return;
    }
    if(s.equals("Done"))
    {
      doneHappened = true;
      String outTexts[] = new String[labels.length];
      for(int i = 0 ; i < labels.length ; i++)
      {
        outTexts[i] = jtextfields[i].getText();
      }
      if(afvta != null) afvta.askforvisibletextsAction(outTexts);
      setVisible(false);
      dispose();
      return;   
    }
  }

  public String getEditedText(int i)
  {
    if(i < 0) return null;
    if(i >= labels.length) return null;
    return jtextfields[i].getText();
  }
 
  public String getLabelsText(int i)
  {
    if(i < 0) return null;
    if(i >= labels.length) return null;
    return labels[i];
  }
 
  public int getNoFields()
  {
    return labels.length;
  }
 
  public boolean getDoneHappened()
  {
    return doneHappened;
  }

  public boolean getCancelHappened()
  {
    return cancelHappened;
  }

  public boolean getHelpHappened()
  {
    return helpHappened;
  }

  public static void main(String args[])
  {
    JFrame jframe = new JFrame();/* {
      public Dimension getPreferredSize()
      {
        return new Dimension(200, 100);
      }
    };*/
    jframe.setTitle("Debugging frame");
    jframe.setDefaultCloseOperation(3);
    jframe.pack();
    jframe.setVisible(false);
    String texts[] = new String [] { "Fullst�ndigt namn", "Personnummer", "filnamn", "Annan text" };
    AskForVisibleTextsDialog afvtdialog = 
      new AskForVisibleTextsDialog(jframe, true, "Ask for some stuff",
                                   texts);
    afvtdialog.pack();
    afvtdialog.setVisible(true);
    if(afvtdialog.getDoneHappened())
    {
      System.out.println("Done");
      for(int i = 0 ; i <   afvtdialog.getNoFields() ; i++)
      {
        System.out.println("P� fr�gan '"+afvtdialog.getLabelsText(i)+"' blev svaret '"+afvtdialog.getEditedText(i)+"'");
      }  
    }
    if(afvtdialog.getCancelHappened())
    {
      System.out.println("Cancel");
    }
    if(afvtdialog.getHelpHappened())
    {
      System.out.println("Help");
    }
  }

}
