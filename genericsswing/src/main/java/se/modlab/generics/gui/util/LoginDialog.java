package se.modlab.generics.gui.util;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import se.modlab.generics.gui.find.StringGridBagLayout;

public class LoginDialog extends JDialog
{

	private static final long serialVersionUID = 1L;
	private JButton loginButton;
  protected LoginAction la;
  private JTextField jtextfield = new JTextField();
  private JPasswordField jpasswordfield = new JPasswordField();
  private boolean cancelHappened = false;
  private boolean helpHappened = false;
  private boolean loginHappened = false;

  public LoginDialog(Frame frame, boolean flag)
  {
    this(frame, flag, null);
  }

  public LoginDialog(Frame frame, boolean flag, LoginAction la)
  {
    super(frame, flag);
    loginButton = null;
    this.la = la;
    initComponents();
    pack();
  }

  private void initComponents()
  {
    Container container = getContentPane();
    container.setLayout(new StringGridBagLayout());
    setTitle("User login");
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent windowevent)
      {
        windowAction("Cancel");
        setVisible(false);
        dispose();
      }
    });
    JLabel jlabel = new JLabel();
    jlabel.setDisplayedMnemonic('U');
    jlabel.setLabelFor(jtextfield);
    jlabel.setText("Username");
    container.add("anchor=WEST,insets=[12,12,0,0]", jlabel);
    jtextfield.setToolTipText("Type username here");
    container.add("fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", jtextfield);
    JLabel jlabel1 = new JLabel();
    jlabel1.setDisplayedMnemonic('P');
    jlabel1.setText("Password");
    jlabel1.setLabelFor(jpasswordfield);
    container.add("gridx=0,gridy=1,anchor=WEST,insets=[11,12,0,0]", jlabel1);
    jpasswordfield.setToolTipText("Type password here");
    Font font = new Font("Lucida Sans", 0, 12);
    jpasswordfield.setFont(font);
    jpasswordfield.setEchoChar('\u2022');
    container.add("gridx=1,gridy=1,fill=HORIZONTAL,weightx=1.0,insets=[11,7,0,11]", jpasswordfield);
    JPanel jpanel = createButtonPanel();
    container.add("gridx=0,gridy=2,gridwidth=2,anchor=EAST,insets=[17,12,11,11]", jpanel);
    getRootPane().setDefaultButton(loginButton);
  }

  private JPanel createButtonPanel()
  {
    JPanel jpanel = new JPanel();
    jpanel.setLayout(new BoxLayout(jpanel, 0));
    loginButton = new JButton();
    loginButton.setText("Login");
    loginButton.setToolTipText("Press login when ready");
    loginButton.setActionCommand("Login");
    loginButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent actionevent)
      {
        windowAction(actionevent);
      }
    });
    jpanel.add(loginButton);
    jpanel.add(Box.createRigidArea(new Dimension(5, 0)));
    JButton jbutton = new JButton();
    jbutton.setText("Cancel");
    jbutton.setActionCommand("Cancel");
    jbutton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent actionevent)
      {
        windowAction(actionevent);
      }
    });
    jpanel.add(jbutton);
    jpanel.add(Box.createRigidArea(new Dimension(5, 0)));
    JButton jbutton1 = new JButton();
    jbutton1.setMnemonic('H');
    jbutton1.setText("Help");
    jbutton1.setActionCommand("Help");
    jbutton1.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent actionevent)
      {
        windowAction(actionevent);
      }
    });
    jpanel.add(jbutton1);
    Vector<JComponent> vector = new Vector<JComponent>(3);
    vector.add(jbutton);
    vector.add(jbutton1);
    vector.add(loginButton);
    equalizeComponentSizes(vector);
    vector.removeAllElements();
    return jpanel;
  }

  private void equalizeComponentSizes(java.util.List<JComponent> list)
  {
    Dimension dimension = new Dimension(0, 0);
    for(int i = 0; i < list.size(); i++)
    {
      JComponent jcomponent = (JComponent)list.get(i);
      Dimension dimension1 = jcomponent.getPreferredSize();
      dimension.width = Math.max(dimension.width, (int)dimension1.getWidth());
      dimension.height = Math.max(dimension.height, (int)dimension1.getHeight());
    }

    for(int j = 0; j < list.size(); j++)
    {
      JComponent jcomponent1 = (JComponent)list.get(j);
      jcomponent1.setPreferredSize((Dimension)dimension.clone());
      jcomponent1.setMaximumSize((Dimension)dimension.clone());
    }
  }

  private void windowAction(Object obj)
  {
    String s = null;
    if(obj != null)
    if(obj instanceof ActionEvent)
      s = ((ActionEvent)obj).getActionCommand();
    else
      s = obj.toString();
    if(s == null)
    {
      setVisible(false);
      dispose();
      return;
    }
    if(s.equals("Cancel"))
    {
      cancelHappened = true;
      if(la != null) la.cancelAction();
      setVisible(false);
      dispose();
      return;
    }
    if(s.equals("Help"))
    {
      helpHappened = true;
      if(la != null) la.helpAction();
      setVisible(false);
      dispose();
      return;
    }
    if(s.equals("Login"))
    {
      loginHappened = true;
      if(la != null) la.loginAction(jtextfield.getText(), new String(jpasswordfield.getPassword()));
      setVisible(false);
      dispose();
      return;   
    }
  }

  public String getUsername()
  {
    return jtextfield.getText();
  }
 
  public String getPassword()
  {
    return new String(jpasswordfield.getPassword());
  }

  public boolean getLoginHappened()
  {
    return loginHappened;
  }

  public boolean getCancelHappened()
  {
    return cancelHappened;
  }

  public boolean getHelpHappened()
  {
    return helpHappened;
  }

  public static void main(String args[])
  {
    JFrame jframe = new JFrame() {
      /**
		 * 
		 */
		private static final long serialVersionUID = 1L;

	public Dimension getPreferredSize()
      {
        return new Dimension(200, 100);
      }
    };
    jframe.setTitle("Debugging frame");
    jframe.setDefaultCloseOperation(3);
    jframe.pack();
    jframe.setVisible(false);
    LoginDialog logindialog = new LoginDialog(jframe, true, null);
    logindialog.pack();
    logindialog.setVisible(true);
    System.out.println("Efter u="+logindialog.getUsername()+", p="+logindialog.getPassword());
  }

}
