package se.modlab.generics.gui.find;

import java.awt.*;
import java.awt.event.*;
import java.util.Vector;

import javax.swing.*;

public class ChooseTextDialog extends JDialog
{

  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
private JButton openerButton[];
  private String title; 
  private String labels[];
  protected ChooseTextAction cta;

  private JLabel jlabels[];

  private boolean cancelHappened = false;

  public ChooseTextDialog(Frame frame, boolean flag, 
                          String title, String labels[])
  {
    this(frame, flag, title, labels, null);
  }

  public ChooseTextDialog(Frame frame, boolean flag, 
                                  String title, String labels[], 
                                  ChooseTextAction _cta)
  {
    super(frame, flag);
    cta = _cta;
    this.title = title;
    this.labels = labels;
    initComponents();
    pack();
  }

  private void initComponents()
  {
    Container container = getContentPane();
    container.setLayout(new StringGridBagLayout());
    setTitle(title);
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent windowevent)
      {
        windowAction("Cancel");
        setVisible(false);
        dispose();
      }
    });
    
    openerButton = new JButton[labels.length];
    jlabels = new JLabel[labels.length];

    for(int i = 0 ; i < labels.length ; i++)
    {
      final int _i = i;
      JButton btn = new JButton("Open");
      openerButton[i] = btn;
      btn.addActionListener(
        new ActionListener()
        {
          public void actionPerformed(ActionEvent ae)
          {
            gotOpenEvent(labels[_i]);
          }
        });
      JLabel jl = new JLabel();
      jlabels[i] = jl;

      jl.setLabelFor(btn);
      jl.setText(labels[i]);
      container.add("gridx=0,gridy="+i+",anchor=WEST,insets=[12,12,0,0]", jl);
      btn.setToolTipText("Open "+labels[i]+" here");
      container.add("gridx=1,gridy="+i+",fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", btn);
    }

    JPanel jpanel = createButtonPanel();
    container.add("gridx=0,gridy="+labels.length+",gridwidth=2,anchor=EAST,insets=[17,12,11,11]", jpanel);
    //setPreferredSize(new Dimension(150, 50 + 20*labels.length));
  }

  private JPanel createButtonPanel()
  {
    JPanel jpanel = new JPanel();
    jpanel.setLayout(new BoxLayout(jpanel, 0));
    jpanel.add(Box.createRigidArea(new Dimension(5, 0)));
    JButton jbutton = new JButton();
    jbutton.setText("Cancel");
    jbutton.setActionCommand("Cancel");
    jbutton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent actionevent)
      {
        windowAction(actionevent);
      }
    });
    jpanel.add(jbutton);
    jpanel.add(Box.createRigidArea(new Dimension(5, 0)));
    Vector<JComponent> vector = new Vector<JComponent>(3);
    vector.add(jbutton);
    equalizeComponentSizes(vector);
    vector.removeAllElements();
    return jpanel;
  }

  private void equalizeComponentSizes(java.util.List<JComponent> list)
  {
    Dimension dimension = new Dimension(0, 0);
    for(int i = 0; i < list.size(); i++)
    {
      JComponent jcomponent = (JComponent)list.get(i);
      Dimension dimension1 = jcomponent.getPreferredSize();
      dimension.width = Math.max(dimension.width, (int)dimension1.getWidth());
      dimension.height = Math.max(dimension.height, (int)dimension1.getHeight());
    }

    for(int j = 0; j < list.size(); j++)
    {
      JComponent jcomponent1 = (JComponent)list.get(j);
      jcomponent1.setPreferredSize((Dimension)dimension.clone());
      jcomponent1.setMaximumSize((Dimension)dimension.clone());
    }
  }

  private void windowAction(Object obj)
  {
    String s = null;
    if(obj != null)
    if(obj instanceof ActionEvent)
      s = ((ActionEvent)obj).getActionCommand();
    else
      s = obj.toString();
    if(s == null)
    {
      setVisible(false);
      dispose();
      return;
    }
    if(s.equals("Cancel"))
    {
      cancelHappened = true;
      if(cta != null) cta.cancelAction();
      setVisible(false);
      dispose();
      return;
    }
  }

  private void gotOpenEvent(String name)
  {
     if(cta != null) cta.textChosen(name);
  }

  public boolean getCancelHappened()
  {
    return cancelHappened;
  }

  public static void main(String args[])
  {
    JFrame jframe = new JFrame();/* {
      public Dimension getPreferredSize()
      {
        return new Dimension(200, 100);
      }
    };*/
    jframe.setTitle("Debugging frame");
    jframe.setDefaultCloseOperation(3);
    jframe.pack();
    jframe.setVisible(false);
    String texts[] = new String [] { "Fullst?ndigt namn", "Personnummer", "filnamn", "Annan text" };
    ChooseTextAction cta = new ChooseTextAction()
      {
        public void cancelAction()
        {
          //System.out.println("ChooseTextAction - cancelAction");
        }

        public void textChosen(String text)
        {
          //System.out.println("ChooseTextAction - textChosen("+text+")");
        }
      };
    ChooseTextDialog ctdialog = 
      new ChooseTextDialog(jframe, true, "Ask for some stuff", texts, cta);
    ctdialog.pack();
    ctdialog.setVisible(true);
  }

}
