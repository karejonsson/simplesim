package se.modlab.generics.gui.util;

import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.JScrollPane;
import javax.swing.JFrame;
import java.awt.*;
import java.awt.event.*;

public class TableDemo extends AbstractTableModel 
{
        
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    final String[] columnNames = {"First Name", 
                                  "Last Name",
                                  "Sport",
                                  "# of Years",
                                  "Vegetarian"};
    
                                  
    final Object[][] data = {
           {"Mary", "Campione", 
            "Snowboarding", new Integer(5), new Boolean(false)},
           {"Alison", "Huml", 
            "Rowing", new Integer(3), new Boolean(true)},
           {"Kathy", "Walrath",
            "Chasing toddlers", new Integer(2), new Boolean(false)},
           {"Sharon", "Zakhour",
            "Speed reading", new Integer(20), new Boolean(true)},
           {"Angela", "Lih",
            "Teaching high school", new Integer(4), new Boolean(false)}
    };

     public int getColumnCount() {
         return columnNames.length;
     }
     
     public int getRowCount() {
         return data.length;
     }

     public String getColumnName(int col) {
        return columnNames[col];
     }

     public Object getValueAt(int row, int col) {
         return data[row][col];
     }

     
     public Class<?> getColumnClass(int c) {
         return getValueAt(0, c).getClass();
     }
	
     
     public boolean isCellEditable(int row, int col) {
         return (col >= 2);
     }
           
    /*
     * Don't need to implement this method unless your table's
     * data can change.
     */
    public void setValueAt(Object value, int row, int col) {
        data[row][col] = value;
        fireTableCellUpdated(row, col);
    }
        
    public static void main(String[] args) 
    {
        JTable table = new JTable(new TableDemo());
        
        JScrollPane scrollPane = new JScrollPane(table);

        JFrame frame = new JFrame("Tabelldemo");
        frame.setSize(new Dimension(500, 700));
        frame.getContentPane().add(scrollPane, BorderLayout.CENTER);
        
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        frame.pack();
        frame.setVisible(true);
    }
}
