package se.modlab.generics.gui.progress;

import java.awt.*;
import java.util.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.text.*;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.gui.exceptions.UniversalTellUser;

/*
 * ProgressMonitorDemo.java is a 1.4 application that requires these files:
 *   LongTask.java
 *   SwingWorker.java
 */
public class ProgressDumpAndMonitor extends JPanel
                                    implements ActionListener 
{

  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
public static final float SCREEN_WIDTH = (int) Toolkit.getDefaultToolkit().getScreenSize().width;
  public static final float SCREEN_HEIGHT = (int) Toolkit.getDefaultToolkit().getScreenSize().height-35;

  public final static int ONE_SECOND = 1000;

  private static ProgressDumpAndMonitor instance = null;
  private ProgressMonitor progressMonitor;
  private static javax.swing.Timer timer;
  private static JTextArea taskOutput;
  private final static String newline = "\n";
  private static MonitoredWorker mw = null;
  private static JFrame myFrame;
  private static int lines = 60;

  public static ProgressDumpAndMonitor getInstance()
  {
    if(instance == null)
    {
      createGUI(null); 
    }
    return instance;
  }

  public static JFrame getFrame()
  {
    if(myFrame == null)
    {
      getInstance();
    }
    return myFrame;
  }

  private static void handleLineOverflow()
  {
    Document doc = taskOutput.getDocument();
    int linesToRemove = taskOutput.getLineCount() - lines - 2;
    try
    {
      int charsToRemove = taskOutput.getLineEndOffset(linesToRemove);
      doc.remove(0, charsToRemove);
    }
    catch(BadLocationException e)
    {
    }
  }

  private static void addText(String text)
  {
    if(text == null) return;
    if(text.trim().length() == 0) return;
    if(instance == null)
    {
      getInstance();
    }
    //System.out.println("PDM 1 "+text.trim().length());
    taskOutput.append(mw.getName()+": "+(new Date())+newline+text+newline);
    taskOutput.setCaretPosition(taskOutput.getDocument().getLength());
    if(taskOutput.getLineCount() < lines) return;
    handleLineOverflow();
  }

  public static void addText(String application, String text)
  {
    if(application == null) return;
    if(application.length() == 0) return;
    if(text == null) return;
    if(text.trim().length() == 0) return;
    if(instance == null)
    {
      getInstance();
    }
    //System.out.println("PDM 2 "+text.trim().length());
    taskOutput.append(application+": "+(new Date())+newline+text+newline);
    taskOutput.setCaretPosition(taskOutput.getDocument().getLength());
    if(taskOutput.getLineCount() < lines) return;
    handleLineOverflow();
  }

  public static void setMonitoredWorker(MonitoredWorker mw)
  {
    if(instance == null)
    {
      getInstance();
    }
    if(ProgressDumpAndMonitor.mw != null)
    {
      if(!ProgressDumpAndMonitor.mw.isDone())
      {
        UniversalTellUser.error(myFrame, "Textual dumps monitored worker not inactive on change!");
        return;
      } 
    }
    ProgressDumpAndMonitor.mw = mw;
  }

  private ProgressDumpAndMonitor(JFrame _frame, MonitoredWorker _mw) 
  {
    super(new BorderLayout());
    mw = _mw;
    myFrame = _frame;
    JMenuBar mb = myFrame.getJMenuBar();
    if(mb == null)
    {
      mb = new JMenuBar();
      myFrame.setJMenuBar(mb);
    }
    JMenu menu = new JMenu("Actions");
    mb.add(menu);
    JMenuItem mi = new JMenuItem("stop");
    menu.add(mi);
    mi.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          if(ProgressDumpAndMonitor.mw.isDone())
          {
            JOptionPane.showMessageDialog(ProgressDumpAndMonitor.myFrame, 
                                          "Already inactive", 
                                          "User stopped", 
                                          JOptionPane.ERROR_MESSAGE);
            return;
          }
          if(ProgressDumpAndMonitor.mw != null) ProgressDumpAndMonitor.mw.stop();
        }
      });
    mi = new JMenuItem("clear");
    menu.add(mi);
    mi.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          if(ProgressDumpAndMonitor.mw != null) ProgressDumpAndMonitor.clear();
        }
      });
    mi = new JMenuItem("hide");
    menu.add(mi);
    mi.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          ProgressDumpAndMonitor.myFrame.setVisible(false);
        }
      });
    menu = new JMenu("length");
    mb.add(menu);
    final int lens[] = new int[] {60, 100, 200, 500, 800, 1000, 1500, 2000, 5000, 
                           10000, 20000, 40000, 100000, 200000, 500000, 1000000 };
    for(int i = 0 ; i < lens.length ; i++)
    {
      final int fi = i;
      mi = new JMenuItem(""+lens[i]);
      menu.add(mi);
      mi.addActionListener(
        new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
            ProgressDumpAndMonitor.lines = lens[fi];
            ProgressDumpAndMonitor.addText("Textual log",
              "Changed textual log line amount to "+lens[fi]);
          }
        }
      );
    }

    taskOutput = new JTextArea(5, 20);
    taskOutput.setMargin(new Insets(5,5,5,5));
    taskOutput.setEditable(false);

    add(new JScrollPane(taskOutput), BorderLayout.CENTER);
    setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

    //Create a timer.
    timer = new javax.swing.Timer(ONE_SECOND, new TimerListener());
    //System.out.println("ProgressDumpAndMonitor "+mw);
  }

  public MonitoredWorker getWorker()
  {
    if(instance == null)
    {
      getInstance();
    }
    return mw;
  }

  /**
   * The actionPerformed method in this class
   * is called each time the Timer "goes off".
   */
  class TimerListener implements ActionListener 
  {
    private String last = "";
    public void actionPerformed(ActionEvent evt) 
    {
      //int i = mw.getCurrent();
      //System.out.println("ProgressDumpAndMonitor - Timerlistener - actionPerformed ");
        progressMonitor.setProgress(mw.getCurrent());
        progressMonitor.setMaximum(mw.getLengthOfTask());
      String s_bar = mw.getMessageForBar();
      String s_dump = mw.getMessageForDump();
      if((s_bar != null) && (myFrame.isVisible()))
      {
         //System.out.println("bar : "+s_bar);
         progressMonitor.setNote(s_bar);
      }
      if((s_dump != null) && (myFrame.isVisible()))
      {
         //System.out.println("dump : "+s_dump);
         if(last.compareTo(s_dump) != 0)
         {
           ProgressDumpAndMonitor.addText(s_dump + newline);
         }
         last = s_dump;
      }
      if(progressMonitor.isCanceled()) 
      {
        progressMonitor.close();
        mw.stop();
        Toolkit.getDefaultToolkit().beep();
        timer.stop();
        if(myFrame.isVisible()) 
        {
          ProgressDumpAndMonitor.addText("Task canceled." + newline);
        }
        return;
      }
      if(mw.isDone()) 
      {
        progressMonitor.close();
        Toolkit.getDefaultToolkit().beep();
        timer.stop();
        if(myFrame.isVisible())
        {
          ProgressDumpAndMonitor.addText("Task completed." + newline);
        } 
      }
    }
  }

  /**
   * Called when the user presses the start button.
   */
  public void actionPerformed(ActionEvent evt) 
  {
    clear();
  }

  public static void clear() 
  {
    taskOutput.setText("");
  }

  public boolean isDone()
  {  
    return mw.isDone();
  }

/*
    if(!mw.isDone())
    {
      JOptionPane.showMessageDialog(null, 
                                    "Work is already in progress", 
                                    "Pipe stuffed", 
                                    JOptionPane.ERROR_MESSAGE);
      return;
    }
 */

  public void monitoredWorkStarted()
  {
    progressMonitor = new ProgressMonitor(ProgressDumpAndMonitor.this,
                              mw.getTasksMessage(),
                              "", 
                              0, 
                              mw.getLengthOfTask());
    progressMonitor.setProgress(0);
    progressMonitor.setMillisToDecideToPopup(2 * ONE_SECOND); 
    timer.start();
  }
  
  /*
  public void monitoredWorkFinished()
  {
      if(mw.isDone()) 
      {
        progressMonitor.close();
        Toolkit.getDefaultToolkit().beep();
        timer.stop();
        if(myFrame.isVisible()) {
          ProgressDumpAndMonitor.addText("Task completed." + newline);
        } 
      }
    progressMonitor.close();
  }
  */
/*
  public static Frame getFrame(Component c)
  {
    if(c == null) return null;
    if(c instanceof Frame) return (Frame) c;
    return getFrame(c.getParent());
  }
 */
  /**
   * Create the GUI and show it.  For thread safety,
   * this method should be invoked from the
   * event-dispatching thread.
   */
  public static ProgressDumpAndMonitor createGUI(MonitoredWorker mw) 
  {
    if(instance != null) {
    	ProgressDumpAndMonitor.setMonitoredWorker(mw);
    	return instance;
    }
    //Make sure we have nice window decorations.
    //JFrame.setDefaultLookAndFeelDecorated(true);
    //Create and set up the window.
    JFrame frame = new JFrame("Textual dump");
    //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    instance = new ProgressDumpAndMonitor(frame, mw);
    instance.setOpaque(true); //content panes must be opaque
    frame.setContentPane(instance);

    //Display the window.
    //frame.pack();
    frame.setLocation(new Point(0, (int) (SCREEN_HEIGHT*0.6)));
    frame.setSize(new Dimension((int) (SCREEN_WIDTH/3), (int) (SCREEN_HEIGHT*0.3)));
    
    frame.setVisible(false);
    return instance;
  }

  public static void main(String[] args) 
  {
    testWorker tw = new testWorker();
    ProgressDumpAndMonitor pmd = createGUI(tw);
    JFrame frame = getFrame();
    System.out.println("Frame "+frame);
    frame.setVisible(true);
    pmd.monitoredWorkStarted();
    tw.go();
  //}
    
    //boolean again = true;
    while(true)
    {
      int resp = JOptionPane.showConfirmDialog(
        null,
        "Vill du k?ra ett varv till?", 
        "Varvstoppare", 
        JOptionPane.YES_NO_OPTION);
      if(resp == JOptionPane.NO_OPTION) System.exit(0); 
      pmd.monitoredWorkStarted();
      tw.go();
      
    }
    
  }

  public static class testWorker implements MonitoredWorker
  {
    private LongTask lt = new LongTask();

    public String getTasksMessage() { return "Tasks name"; }
    public String getMessageForBar() { return lt.getMessageForBar(); }
    public String getMessageForDump() { return lt.getMessageForDump(); }
    public boolean isDone() { return lt.isDone(); }
    public void stop() { lt.stop(); }
    public int getCurrent() {return lt.getCurrent(); }
    public int getLengthOfTask() { return lt.getLengthOfTask(); }
    public String getName() { return "Testworker"; }
    public void go() { lt.run(); }
    //public void go() { Thread t = new Thread(lt) ; t.start(); }
  }
  
}

