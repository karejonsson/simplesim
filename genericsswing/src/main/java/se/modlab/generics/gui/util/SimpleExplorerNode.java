package se.modlab.generics.gui.util;

import javax.swing.tree.TreeNode;
import java.awt.Component;

public interface SimpleExplorerNode extends TreeNode
{
  public Component getComponent();
}

