package se.modlab.generics.gui.util;

public interface LoginAction
{
  public void cancelAction();
  public void helpAction();
  public void loginAction(String username,
                          String password);
}