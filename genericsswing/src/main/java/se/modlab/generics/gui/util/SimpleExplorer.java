package se.modlab.generics.gui.util;

import javax.swing.JTree;
import javax.swing.event.TreeSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.tree.TreeSelectionModel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.tree.*;
import java.awt.*;
import java.awt.event.*;

public class SimpleExplorer extends JFrame {

  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
  //private static boolean DEBUG = true;
  private boolean playWithLineStyle = false;
  private String lineStyle = "Angled"; 
  private JScrollPane rightView = new JScrollPane();
  private JScrollPane leftView = new JScrollPane();
  //private int idx = 0;
  private SimpleExplorerNode topNode;
  private Runnable onExit;
  private JTree tree;

  public SimpleExplorer(String title, SimpleExplorerNode topNode, Runnable onExit) {
    super(title);
    this.topNode = topNode;
    this.onExit = onExit;

    //Create a tree that allows one selection at a time.
    tree = getTree(topNode);

    //Create the scroll pane and add the tree to it. 
    leftView = new JScrollPane(tree);

    //Add the scroll panes to a split pane.
    JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
    splitPane.setTopComponent(leftView);
    splitPane.setBottomComponent(rightView);

    Dimension minimumSize = new Dimension(100, 50);
    rightView.setMinimumSize(minimumSize);
    leftView.setMinimumSize(minimumSize);
    splitPane.setDividerLocation(300);
    splitPane.setPreferredSize(new Dimension(700, 500));

    //Add the split pane to this frame.
    getContentPane().add(splitPane, BorderLayout.CENTER);
    JMenuBar mb = new JMenuBar();
    JMenu m = new JMenu("File");
    mb.add(m);
    JMenuItem mi = new JMenuItem("Exit");
    m.add(mi);
    mi.addActionListener(
      new ActionListener()
      {  
        public void actionPerformed(ActionEvent e) 
        {
          Runnable r = SimpleExplorer.this.onExit;
          if(r != null) r.run();
          SimpleExplorer.this.dispose();
          Runtime.getRuntime().runFinalization();
        }
      });
    mi = new JMenuItem("Refresh");
    m.add(mi);
    mi.addActionListener(
      new ActionListener()
      {  
        public void actionPerformed(ActionEvent e) 
        {
          SimpleExplorer.this.refresh();
        }
      });
    setJMenuBar(mb);
    addWindowListener(
      new WindowAdapter() 
      {
        public void windowClosing(WindowEvent e) {
          Runnable r = SimpleExplorer.this.onExit;
          if(r != null) r.run();
          SimpleExplorer.this.dispose();
          //Runtime.getRuntime().runFinalization();
          //System.out.println("SimpleExplorer.WindowAdapter.windowClosing");
        }
      });  
  }

  private JTree getTree(SimpleExplorerNode node)
  {
    this.topNode = node;
    final JTree tree = new JTree(topNode);
    tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
    tree.addTreeSelectionListener(
      new TreeSelectionListener()
      {
        public void valueChanged(TreeSelectionEvent e)
        {
          //Debug.println("Interruptet kom ig?ng.");
          Object o = tree.getLastSelectedPathComponent();
          //System.out.println("valueChanged "+o);
          if(!(o instanceof SimpleExplorerNode))
          {
            SimpleExplorer.this.setRightSide(null);
            //Debug.println("Det blir null p? dumma st?llet"+
            //              o.getClass().getName());
            return;
          }
          SimpleExplorerNode sen = (SimpleExplorerNode) o;
          SimpleExplorer.this.setRightSide(sen.getComponent());
          //Debug.println("Det blir inte null");
        }
      });
    if(playWithLineStyle) {
      tree.putClientProperty("JTree.lineStyle", lineStyle);
    }

    return tree;
  }

  public void setNewTopNode(SimpleExplorerNode sen)
  {
    leftView.getViewport().setView(getTree(sen));
    setRightSide(sen.getComponent());
    topNode = sen;
  }

  public SimpleExplorerNode getTopNode()
  {
    return topNode;
  }

  public void refresh()
  {
    DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
    model.reload(topNode);
  }

  public void setRightSide(Component c)
  {
    rightView.setViewportView(c);
  }

  public static void main(String[] args) {
    JFrame frame = new SimpleExplorer("Egna mainet", null, null);
    frame.pack();
    frame.setVisible(true);
  }
}
