package se.modlab.generics.gui.find;

import java.awt.*;

class StringGridBagHLine extends StringGridBagLine
{

	private static final long serialVersionUID = 1L;

	public StringGridBagHLine()
    {
    }

    public void paint(Graphics g)
    {
        super.paint(g);
        Rectangle rectangle = getBounds();
        int i = rectangle.height / 2 - super.lineWidth / 2;
        if(i < 0)
            i = 0;
        int j = 0;
        int k = (j + rectangle.width) - 1;
        java.awt.Color color = g.getColor();
        g.setColor(super.lineColor);
        g.drawLine(j, i, k, i);
        g.setColor(color);
    }

    public Dimension getPreferredSize()
    {
        return new Dimension(10, super.lineWidth);
    }

    public Dimension getMinimumSize()
    {
        return new Dimension(3, 1);
    }
}
