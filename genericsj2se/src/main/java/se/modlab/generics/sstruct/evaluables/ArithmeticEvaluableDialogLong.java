package se.modlab.generics.sstruct.evaluables;

import javax.swing.JOptionPane;

import se.modlab.generics.exceptions.UserRuntimeError;
import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.sstruct.comparisons.ArithmeticEvaluable;
import se.modlab.generics.sstruct.comparisons.Scope;
import se.modlab.generics.sstruct.strings.StringEvaluable;
import se.modlab.generics.sstruct.values.sLong;
import se.modlab.generics.sstruct.values.sValue;

public class ArithmeticEvaluableDialogLong implements ArithmeticEvaluable {
	
	private StringEvaluable titleExpression;
	private StringEvaluable messageExpression;
	private StringEvaluable defaultValueExpression;
	
	public ArithmeticEvaluableDialogLong(
			StringEvaluable _titleExpression,
			StringEvaluable _messageExpression,
			StringEvaluable _defaultValueExpression) {
		titleExpression = _titleExpression;
		messageExpression = _messageExpression;
		defaultValueExpression = _defaultValueExpression;
	}

	public sValue evaluate(Scope s) throws  
	IntolerableException
	{
		String message = messageExpression.evaluate(s);
		String title = titleExpression.evaluate(s);
		String defaultValue = defaultValueExpression.evaluate(s);
		while(true) {
	 		Object schoice = JOptionPane.showInputDialog(null, message, title, JOptionPane.QUESTION_MESSAGE, null, null, defaultValue) ;
			if(schoice == null) {
				Object[] options = { "OK", "Stop execution" };
				int choice = JOptionPane.showOptionDialog(null, 
						"Do you want to cancel the execution?", 
						"Cancel?",
						JOptionPane.DEFAULT_OPTION, 
						JOptionPane.INFORMATION_MESSAGE,
						null, 
						options, 
						options[1]);
				if(choice == 1) {
					throw new UserRuntimeError("Stopped by user"); 
				}
			}
			try {
				long l = Long.parseLong(schoice.toString());
				return new sLong(l);
			}
			catch(Exception e) {
				Object[] options = { "OK", "Stop execution" };
				int choice = JOptionPane.showOptionDialog(null, 
						"Your entry was not of integer type. Try again?", 
						"Faulty entry",
						JOptionPane.DEFAULT_OPTION, 
						JOptionPane.ERROR_MESSAGE,
						null, 
						options, 
						options[1]);
				if(choice == 1) {
					throw new UserRuntimeError("Stopped by user"); 
				}
			}
		}
	}

	public String reproduceExpression() {
		return "dialogLong()";
	}

	public void verify(Scope s) throws IntolerableException {
	}

}
