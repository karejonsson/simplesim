package se.modlab.generics.sstruct.executables;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.Scope;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.strings.*;

import javax.swing.*;

public class DialogStatement extends ProgramStatement implements LogicalExpression {

	StringContainer ccc;

	public DialogStatement(StringContainer _ccc) {
		ccc = _ccc;
	}

	public void execute(Scope s) 
	throws ReturnException, 
	IntolerableException, 
	StopException,
	BreakException,
	ContinueException
	{
		Object[] options = { "OK", "Stop execution" };
		int choice = JOptionPane.showOptionDialog(null, 
				ccc.evaluate(s), 
				"Choice",
				JOptionPane.DEFAULT_OPTION, 
				JOptionPane.INFORMATION_MESSAGE,
				null, 
				options, 
				options[1]);
		if(choice == 1)
		{
			throw new StopExceptionUserProgrammatic("Stopped by user"); 
		}
	}

	public boolean evaluate(Scope s)
	throws IntolerableException
	{
		Object[] options = { "Yes", "No", "Stop execution" };
		int choice = JOptionPane.showOptionDialog(null, 
				ccc.evaluate(s), 
				"Choice",
				JOptionPane.DEFAULT_OPTION, 
				JOptionPane.INFORMATION_MESSAGE,
				null, 
				options, 
				options[1]);
		if(choice == 2)
		{
			throw new UserRuntimeError("Stopped by user"); 
		}
		return choice == 0;
	}

	public void verify(Scope s) throws IntolerableException {
		ccc.verify(s);
	}
	
	  public String reproduceExpression() {
		  return "dialog("+ccc.reproduceExpression()+")";
	  }


}
