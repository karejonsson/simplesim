package se.modlab.generics.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

public class Services {

	public static byte[] getCleartextFromFile(InputStream is) { 
		try { 
			final int len = 4096;
			byte[] buff = new byte[len];
			int nbt = 0;
			int accum = nbt;
			Vector<byte[]> v = new Vector<byte[]>();
			while((nbt = is.read(buff, 0, buff.length)) != -1) {
				byte tmp[] = new byte[nbt];
				System.arraycopy(buff, 0, tmp, 0, nbt);
				v.addElement(tmp);
				accum += nbt;
			}
			is.close();
			byte out[] = new byte[accum];
			nbt = 0;
			for(int i = 0 ; i < v.size() ; i++) {
				byte tmp[] = (byte[]) v.elementAt(i);
				System.arraycopy(tmp, 0, out, nbt, tmp.length);
				nbt += tmp.length;
			}
			return out;
		}  
		catch(IOException e) {
		}
		return null;
	}

	public static byte[] getCleartextFromFile(String filename) { 
		try {
			return getCleartextFromFile(new FileInputStream(filename));
		}
		catch(FileNotFoundException e) {
		} 
		return null;
	}

	public static byte[] getCleartextFromFile(File f) { 
		try {
			return getCleartextFromFile(new FileInputStream(f));
		}
		catch(FileNotFoundException e) {
		} 
		return null;
	}

}
