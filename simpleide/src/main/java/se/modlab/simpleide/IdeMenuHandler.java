package se.modlab.simpleide;

import javax.swing.JMenu;
import se.modlab.generics.gui.files.*;
import java.io.*;
import java.awt.*;

public interface IdeMenuHandler
{

  public JMenu[] getFilesMenu(File f);
  public JMenu[] getEditorsMenu(OneFileNotepad ofn);
  public JMenu[] getFoldersMenu(File f);
  public JMenu[] getMenubar();

}