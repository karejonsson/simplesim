package se.modlab.simpleide;

import javax.swing.tree.*;
import java.io.*;
import javax.swing.*;
import java.awt.*;

public abstract class FileSystemNode extends DefaultMutableTreeNode
{

  protected File f; 
  protected IdeMenuHandler handler[] = null;
  protected FrameTracker tracker = null;

  public FileSystemNode(File _f, 
                        IdeMenuHandler _handler[],
                        FrameTracker _tracker)
  {
    f = _f;
    handler = _handler;
    tracker = _tracker;
  }

  public IdeMenuHandler[] getIdeMenuHandlers()
  { 
    return handler;
  }

  public FrameTracker getFrameTracker()
  { 
    return tracker;
  }

  public String toString()
  {
    try
    {
      return f.getCanonicalPath();
    }
    catch(IOException ioe)
    {
      return f.getAbsolutePath();  
    }
  }

  public boolean isLeaf()
  {
    return !f.isDirectory();
  }

  protected Frame getCurrentFrame(Component tree) 
  {
    for(Container p = tree.getParent(); p != null; p = p.getParent()) 
    {
      if(p instanceof Frame) 
      {
	return (Frame) p;
      }
    }
    return null;
  }

  public abstract JPopupMenu getPopup(JTree tree);

  public File getFile()
  {
    return f;
  }

  public void accept(FileVisitor fv)
    throws Exception
  {
    fv.visit(this);
  }

}

