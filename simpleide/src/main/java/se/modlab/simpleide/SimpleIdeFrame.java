package se.modlab.simpleide;

import java.io.*;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;

import java.awt.*;
import java.awt.event.*;

import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.progress.*;
import se.modlab.generics.exceptions.*;

public class SimpleIdeFrame extends JFrame
{

  private static SimpleIdeFrame lastSIF = null;

  public static SimpleIdeFrame getLastSIF()
  {
    return lastSIF;
  }

  public static final float SCREEN_WIDTH = (int) Toolkit.getDefaultToolkit().getScreenSize().width;
  public static final float SCREEN_HEIGHT = (int) Toolkit.getDefaultToolkit().getScreenSize().height-35;

  private static final String defaultTitle = 
    "Simple integrated development environment";
  private TopNode tn = null;
  private JTree tree = null;
  private IdeMenuHandler handler[] = null;
  private FrameTracker tracker = null;
  private File tn_file = null;
  private JFrame mmf = null;
  private MemoryMonitor mm = null;
  

  public static void main(String args[])
  {
    File fi = null;
    if(args.length == 1)
    {
      fi = new File(args[0]);
    }
    else
    {
      fi = new File(System.getProperty("user.dir"));
    }
    FrameTracker tracker = new FrameTracker();
    
    Frame sif = new SimpleIdeFrame("Enkel utvecklingsmilj�",
                                   null,
                                   tracker,
                                   fi);
    sif.setVisible(true);
  }

  public SimpleIdeFrame(File _tn_file)
  {
    this(defaultTitle, null, null, _tn_file);
  }

  public SimpleIdeFrame(IdeMenuHandler handler[], File _tn_file)
  {
    this("Simple integrated development environment", handler, _tn_file);
  }

  public SimpleIdeFrame(String title, 
                        IdeMenuHandler handler[], 
                        File _tn_file)
  {
    this(title, handler, null, _tn_file);
  }

  public SimpleIdeFrame(String title, 
                        IdeMenuHandler _handler[],
                        FrameTracker _tracker,
                        File _tn_file)
  {
    super(title);
    lastSIF = this;
    handler = _handler;
    tracker = _tracker;
    tn_file = _tn_file;
    resetMainFrame();
    Thread t = new Thread(
      new Runnable()
      { 
        public void run()
        {
          loop();
        }
      }
    );
    t.start();
    refresh();
  }

  public File getInstallation()
  {
    return tn_file.getParentFile();
  }

  public String getInstallationS()
  {
    try
    {
      return tn_file.getParentFile().getCanonicalPath();
    }
    catch(IOException ioe)
    {
    }
    return tn_file.getParentFile().toString();
  }

  public File getMountPoint()
  {
    return tn_file;
  } 

  public String getMountPointS()
  {
    try
    {
      return tn_file.getCanonicalPath();
    }
    catch(IOException ioe)
    {
    }
    return tn_file.toString();
  } 

  public void refresh()
  { 
    invalidate();
    validate();
    super.repaint(); 
  }

  public void resetMainFrame()
  {
    if(!tn_file.exists())
    {
    	try {
           System.out.println("File "+tn_file.getCanonicalPath()+" does not exist (Canonical)");
        }
    	catch(Exception e) {
           System.out.println("File "+tn_file.getAbsolutePath()+" does not exist (Absolute)");
    	}
      return;
    }
    //System.out.println("File "+tn_file.getAbsolutePath()+" exists");
    tn = new TopNode(tn_file, handler, tracker);
/*
      DefaultMutableTreeNode ttn = new DefaultMutableTreeNode("Green 1");
      DefaultMutableTreeNode _tn = null;
      ttn.setUserObject("uGreen 1");
      tn.add(ttn);
      ttn.add(_tn = new DefaultMutableTreeNode("g 1 1"));
      _tn.setUserObject("gu 1 1");
      ttn.add(_tn = new DefaultMutableTreeNode("g 1 2"));
      _tn.setUserObject("gu 1 2");
      ttn.add(_tn = new DefaultMutableTreeNode("g 1 3"));
      _tn.setUserObject("gu 1 3");

      ttn = new DefaultMutableTreeNode("Green 2");
      ttn.setUserObject("uGreen 2");
      tn.add(ttn);
      ttn.add(_tn = new DefaultMutableTreeNode("g 2 1"));
      _tn.setUserObject("gu 2 1");
      ttn.add(_tn = new DefaultMutableTreeNode("g 2 2"));
      _tn.setUserObject("gu 2 2");
      ttn.add(_tn = new DefaultMutableTreeNode("g 2 3"));
      _tn.setUserObject("gu 2 3");
 */

    FileTreeCorrectionVisitor ftcv = new FileTreeCorrectionVisitor();

    try
    {
      tn.accept(ftcv);
    /*fileSystemNode fsn = ftcv.getNodeToRefresh();
    if(fsn != null)
    { 
      System.out.println("XXXyyy ");
      DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
      model.reload(fsn);
    } */
    }
    catch(Exception e)
    {
      System.out.println("XXX ");
      e.printStackTrace();
      System.exit(0);
    }
    tree = new DnDExplorerTree(tn);

    tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);

    JScrollPane view = new JScrollPane(tree);
    view.setMinimumSize(new Dimension(100, 50));
    view.setPreferredSize(new Dimension(700, 500));
    getContentPane().removeAll();
    getContentPane().add(view, BorderLayout.CENTER);
    tree.addMouseListener(new PopupListener());  
    pack();
    setLocation(new Point(0,0));
    setSize(new Dimension((int)(SCREEN_WIDTH/3),  (int)(SCREEN_HEIGHT*0.6)));
    addWindowListener(
      new WindowAdapter() 
      {
	public void windowClosing(WindowEvent event) 
        {
	  System.exit(0);
	}
	public void windowClosed(WindowEvent event) 
        {
	  System.exit(0);
	}
      }
    );
    handleMenuBar();
  }
  
  private static String firstEncoding = null;
  private final static String encodingPropertyName = "file.encoding";

  private final static String encs[] = new String[] {
	  "UTF-8",
	  "UTF-16",
	  "ISO-8859-1",
	  "Latin1",
	  "CP1252",
	  "MacRoman",
	  "ASCII"
  };
  
  public static String[] getListOfEncodings() {
	  return encs;
  }
  
  private JMenuItem getEncodingMenu() {
	  if(firstEncoding == null) {
		  firstEncoding = System.getProperty(encodingPropertyName);
	  }
	  JMenu out = new JMenu("Encoding");
	  JMenuItem menuItem = new JMenuItem("Reset");
	  out.add(menuItem);
	  menuItem.addActionListener(
			  new ActionListener()
			  {
				  public void actionPerformed(ActionEvent e)
				  {
					  System.setProperty(encodingPropertyName, firstEncoding);
				  }
			  }
			  );
	  JMenu setters = new JMenu("Set predefined");
	  out.add(setters);
	  for(int i = 0 ; i < encs.length ; i++) {
		  final int fi = i;
		  menuItem = new JMenuItem(encs[i]);
		  setters.add(menuItem);
		  menuItem.addActionListener(
				  new ActionListener() {
					  public void actionPerformed(ActionEvent e) {
						  System.setProperty(encodingPropertyName, encs[fi]);
					  }
				  }
				  );
		  
	  }
	  menuItem = new JMenuItem("Set by hand");
	  out.add(menuItem);
	  menuItem.addActionListener(
			  new ActionListener() {
				  public void actionPerformed(ActionEvent e) {
					    String inputValue = JOptionPane.showInputDialog(
					    	      SimpleIdeFrame.this,
					    	      "Type encodings name",
					    	      "x");
					    if(inputValue == null) return;
					    System.setProperty(encodingPropertyName, inputValue);
				  }
			  }
			  );

	  return out;
  }

  public void handleMenuBar()
  {
	  //boolean w = false;
    JMenuBar bar = null;//getJMenuBar();
    if(bar == null)
    {
      bar = new JMenuBar();
      setJMenuBar(bar);
    }
    else
    {
      bar.removeAll();
    }
    JMenu application = new JMenu("Application");
    bar.add(application);
    JMenuItem menuItem = new JMenuItem("Refresh file tree");
    application.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          try
          {
            resetMainFrame();
          }
          catch(Exception _e)
          {
            JOptionPane.showMessageDialog(
              SimpleIdeFrame.this, 
              "Reset main frame failed!", 
              "Unexpected failure", 
              JOptionPane.ERROR_MESSAGE);
          }
        }
      }
    );
    menuItem = getEncodingMenu();
    application.add(menuItem);
    menuItem = new JMenuItem("Installation");
    application.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          String path = null;
          try
          {
            path = tn_file.getParentFile().getCanonicalPath();
            JOptionPane.showMessageDialog(
              SimpleIdeFrame.this, 
              "This installation is at:\n\n"+
              path+"\n", 
              "Installation information.", 
              JOptionPane.INFORMATION_MESSAGE);
          }
          catch(Exception _e)
          {
            path = tn_file.getParentFile().getAbsolutePath();
            JOptionPane.showMessageDialog(
              SimpleIdeFrame.this, 
              "This installation is at:\n\n"+
              path+"\n", 
              "Installation information.", 
              JOptionPane.INFORMATION_MESSAGE);
          }
        }
      }
    );
    menuItem = new JMenuItem("Garbage collect");
    application.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          System.gc();
          JOptionPane.showMessageDialog(
            SimpleIdeFrame.this, 
            "Garbage collect done!", 
            "Done", 
            JOptionPane.INFORMATION_MESSAGE);
        }
      }
    );
    menuItem = new JMenuItem("Textual log");
    application.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          try
          {
            ProgressDumpAndMonitor pmd = ProgressDumpAndMonitor.getInstance();
            pmd.clear();
            ProgressDumpAndMonitor.getFrame().setVisible(true);
          }
          catch(Exception _e)
          {
            UniversalTellUser.general(
              ProgressDumpAndMonitor.getFrame(), 
              new UnclassedError("Application - Main - ProgressMonitor", _e));
          }
        }
      }
    );
    menuItem = new JMenuItem("Memory monitor");
    application.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          if(mmf == null)
          {
            mmf = new JFrame("Memory monitor");
            mm = new MemoryMonitor((int)(SCREEN_WIDTH/3), (int)(SCREEN_HEIGHT/10));
            mm.startMemoryMonitor();
            mmf.addWindowListener(new WindowAdapter() {
                  public void windowClosing(WindowEvent e) {
                      mm.stopMemoryMonitor();
                  }
              });
            mmf.getContentPane().setLayout(new BorderLayout());
            mmf.getContentPane().add(mm, BorderLayout.CENTER);
            mmf.pack();
            mmf.setSize(new Dimension((int)(SCREEN_WIDTH/3), (int)(SCREEN_HEIGHT/10)));
            mmf.setLocation(new Point(0, (int)(9*SCREEN_HEIGHT/10)));
            mmf.setVisible(true);
            return;
          }
          mmf.setVisible(!mmf.isVisible());
          if(mmf.isVisible())
          {
            mm.startMemoryMonitor();
          }
          else
          {
            mm.stopMemoryMonitor();
          }
        }
      }
    );
    JMenuItem item = new JMenuItem("exit");
    application.add(item);
    item.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          System.exit(0);
        }
      }
    );
    bar.add(tracker.getBarsEditorMenu());
    if(handler != null)
    {
      for(int i = 0 ; i < handler.length ; i++)
      {
        JMenu menus[] = handler[i].getMenubar();
        if(false) {
        	System.out.println("Handle == null "+(handler[i] == null));
        	System.out.println("menus == null "+(menus == null));
        }
        if(menus != null) {
        for(int j = 0 ; j < menus.length ; j++)
        {
          bar.add(menus[j]);
        }
        }
      }
    } 
    if(tracker != null)
    {
      bar.add(tracker.getBarsEditorMenu());
    } 
    invalidate();
    validate();
    repaint();
  }

  private void loop()
  {
    while(true)
    {
      try
      {
        Thread.sleep(1000);
        performControl();
      }
      catch(Exception e)
      {
        System.out.println("loop got exception\n"+e);
        e.printStackTrace();
      }
      catch(Throwable t)
      {
        System.out.println("loop got throwable\n"+t);
        t.printStackTrace();
      }
    }
  }

  public static class PopupListener extends MouseAdapter 
  {
    
    public PopupListener()
    {
    }

    public void mousePressed(MouseEvent e) 
    {
      maybeShowPopup(e);
    }

    public void mouseReleased(MouseEvent e) 
    {
      maybeShowPopup(e);
    }

    private void maybeShowPopup(MouseEvent e) 
    {
      if(e.isPopupTrigger()) 
      {
        //System.out.println(e.getComponent().getClass().getName());
        if(e == null) return;
        JTree tree = (JTree) e.getComponent();
        if(tree == null) return;
        TreePath tp = tree.getClosestPathForLocation(e.getX(), e.getY());
        tree.setSelectionPath(tp);
        FileSystemNode fsn = (FileSystemNode) tree.getLastSelectedPathComponent();
        if(fsn == null) return;
        JPopupMenu popup = fsn.getPopup(tree);
        if(popup != null) popup.show(e.getComponent(), e.getX(), e.getY());
      }
    }

  }

  private void performControl()
    throws Exception
  {
    //System.out.println("performControl");
    FileTreeCorrectionVisitor ftcv = new FileTreeCorrectionVisitor();
    tn.accept(ftcv);
    FileSystemNode fsn = ftcv.getNodeToRefresh();
    if(fsn != null)
    { 
      DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
      model.reload(fsn);
    }
   
  }

}
