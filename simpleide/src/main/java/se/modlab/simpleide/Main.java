package se.modlab.simpleide;

import java.io.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;
import java.awt.*;
import java.awt.event.*;


public class Main
{

  public static final float SCREEN_WIDTH = (int) Toolkit.getDefaultToolkit().getScreenSize().width;
  public static final float SCREEN_HEIGHT = (int) Toolkit.getDefaultToolkit().getScreenSize().height-35;

  private static TopNode tn = null;
  private static JFrame frame = null;
  private static JTree tree = null;
  private static IdeMenuHandler handler[] = null;
  private static FrameTracker tracker = null;

  public static void main(String args[])
  {
    handler = new IdeMenuHandler[] { new DefaultIdeMenuHandler() };
    tracker = new FrameTracker();
    String title = "Simple explorer";
    File tn_file = null;
    if(args.length == 1)
    {
      tn_file = new File(args[0]);
    }
    else
    {
      tn_file = new File(System.getProperty("user.dir"));
    }
    tn = new TopNode(tn_file, handler, tracker);
    frame = new JFrame(title);
    fillMainFrame();
    frame.setVisible(true);
    Thread t = new Thread(
      new Runnable()
      { 
        public void run()
        {
          Main.loop();
        }
      }
    );
    t.start();
  }

  public static void fillMainFrame()
  {
    tree = new JTree(tn);
    tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
    JScrollPane view = new JScrollPane(tree);
    view.setMinimumSize(new Dimension(100, 50));
    view.setPreferredSize(new Dimension(700, 500));
    frame.getContentPane().removeAll();
    frame.getContentPane().add(view, BorderLayout.CENTER);
    tree.addMouseListener(new PopupListener());  
    frame.pack();
    frame.setLocation(new Point(0,0));
    frame.setSize(new Dimension((int)(SCREEN_WIDTH/3),  (int)(SCREEN_HEIGHT/2)));
    frame.addWindowListener(
      new WindowAdapter() 
      {
	public void windowClosing(WindowEvent event) 
        {
	  System.exit(0);
	}
	public void windowClosed(WindowEvent event) 
        {
	  System.exit(0);
	}
      }
    );
    if(handler != null)
    {
      JMenuBar bar = frame.getJMenuBar();
      if(bar == null)
      {
        bar = new JMenuBar();
        frame.setJMenuBar(bar);
      }
      JMenu application = new JMenu("Simpleide");
      bar.add(application);
      JMenuItem item = new JMenuItem("exit");
      application.add(item);
      item.addActionListener(
        new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
            System.exit(0);
          }
        }
      );
      bar.add(tracker.getBarsEditorMenu());
    }  
  }

  private static void loop()
  {
    while(true)
    {
      try
      {
        Thread.sleep(1000);
        performControl();
      }
      catch(Exception e)
      {
        System.out.println("loop\n"+e);
        e.printStackTrace();
      }
    }
  }

  public static class PopupListener extends MouseAdapter 
  {
    
    public PopupListener()
    {
    }

    public void mousePressed(MouseEvent e) 
    {
      maybeShowPopup(e);
    }

    public void mouseReleased(MouseEvent e) 
    {
      maybeShowPopup(e);
    }

    private void maybeShowPopup(MouseEvent e) 
    {
      if(e.isPopupTrigger()) 
      {
        //System.out.println(e.getComponent().getClass().getName());
        if(e == null) return;
        JTree tree = (JTree) e.getComponent();
        if(tree == null) return;
        TreePath tp = tree.getClosestPathForLocation(e.getX(), e.getY());
        tree.setSelectionPath(tp);
        FileSystemNode fsn = (FileSystemNode) tree.getLastSelectedPathComponent();
        if(fsn == null) return;
        JPopupMenu popup = fsn.getPopup(tree);
        if(popup != null) popup.show(e.getComponent(), e.getX(), e.getY());
      }
    }

  }

  private static void performControl()
    throws Exception
  {
    //System.out.println("performControl");
    FileTreeCorrectionVisitor ftcv = new FileTreeCorrectionVisitor();
    tn.accept(ftcv);
    FileSystemNode fsn = ftcv.getNodeToRefresh();
    if(fsn != null)
    { 
      DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
      model.reload(fsn);
    }
   
  }
}