package se.modlab.simpleide;

import java.lang.reflect.*;

public class FileVisitor
{

  public void visit(Object o)
    throws Exception
  {
    try 
    {
      Class c = getClass();
      Method m = c.getMethod("visit", 
                             new Class[] { o.getClass() });
      m.invoke(this, new Object[] { o });
    } 
    catch(Exception e) 
    {
      //e = getException(e);
      //if(e instanceof intolerableException) throw (intolerableException)e;
      //throw new internalError(getMessage(e));
    }
  }
/*
  private Exception getException(Throwable e)
  {
    if(!(e instanceof InvocationTargetException))
    {  
      return (Exception)e;
    }
    while(e instanceof InvocationTargetException)
    {
      e = ((InvocationTargetException) e).getTargetException();
    }
    return (Exception)e;
  }

  private String getMessage(Throwable e)
  {
    Exception _e = getException(e);
    //_e.printStackTrace();
    return _e.toString();
  }
 */
}