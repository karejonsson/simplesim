package se.modlab.simpleide;

import se.modlab.generics.files.Unzip;
import se.modlab.generics.gui.files.*;
import se.modlab.generics.gui.util.Clipboard;

import javax.swing.tree.*;

import java.util.zip.*;
import java.io.*;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;

public class FileNode extends FileSystemNode
{

  public static final float SCREEN_WIDTH = (int) Toolkit.getDefaultToolkit().getScreenSize().width;
  public static final float SCREEN_HEIGHT = (int) Toolkit.getDefaultToolkit().getScreenSize().height-35;

  public final static int MIDDLE_LONG = 1;
  public final static int RIGHT_LONG = 2;
  public final static int LEFT_LONG = 3;
  public final static int MIDDLE_UPPER = 4;
  public final static int RIGHT_UPPER = 5;
  public final static int LEFT_UPPER = 6;
  public final static int MIDDLE_LOWER = 7;
  public final static int RIGHT_LOWER = 8;
  public final static int LEFT_LOWER = 9;
  
  public FileNode(File f, IdeMenuHandler handler[], FrameTracker tracker)
  {
    super(f, handler, tracker);
  }

  public String toString()
  {
    return f.getName();
  }

  public JPopupMenu getPopup(JTree tree)
  {
    final JTree t = tree;
    JPopupMenu popup = new JPopupMenu();
    JMenu menu = new JMenu("Edit");
    JMenu menu2 = new JMenu("Left");
    JMenuItem menuItem = new JMenuItem("Long");
    menuItem.addActionListener(
      new editKicker(0, 0, 
                     (int)(SCREEN_WIDTH/3), (int)(SCREEN_HEIGHT)));
    menu2.add(menuItem);
    menuItem = new JMenuItem("Upper");
    menuItem.addActionListener(
      new editKicker(0, 0, 
                     (int)(SCREEN_WIDTH/3), (int)(SCREEN_HEIGHT/2)));
    menu2.add(menuItem);
    menuItem = new JMenuItem("Lower");
    menuItem.addActionListener(
      new editKicker(0, (int)(SCREEN_HEIGHT/2), 
                     (int)(SCREEN_WIDTH/3), (int)(SCREEN_HEIGHT/2)));
    menu2.add(menuItem);
    menu.add(menu2);
    menuItem = new JMenuItem("2 / 3");
    menuItem.addActionListener(
      new editKicker(0, 0, 
                     (int)(SCREEN_WIDTH*2/3), (int)(SCREEN_HEIGHT)));
    menu2.add(menuItem);
    menu.add(menu2);
    menu2 = new JMenu("Middle");
    menuItem = new JMenuItem("Long");
    menuItem.addActionListener(
      new editKicker((int)(SCREEN_WIDTH/3), 0, 
                     (int)(SCREEN_WIDTH/3), (int)(SCREEN_HEIGHT)));
    menu2.add(menuItem);
    menuItem = new JMenuItem("Upper");
    menuItem.addActionListener(
      new editKicker((int)(SCREEN_WIDTH/3), 0, 
                     (int)(SCREEN_WIDTH/3), (int)(SCREEN_HEIGHT/2)));
    menu2.add(menuItem);
    menuItem = new JMenuItem("Lower");
    menuItem.addActionListener(
      new editKicker((int)(SCREEN_WIDTH/3), (int)(SCREEN_HEIGHT/2), 
                     (int)(SCREEN_WIDTH/3), (int)(SCREEN_HEIGHT/2)));
    menu2.add(menuItem);
    //menu.add(menu2);
    menuItem = new JMenuItem("Full screen");
    menuItem.addActionListener(
      new editKicker(0, 0, 
                     (int)(SCREEN_WIDTH), (int)(SCREEN_HEIGHT)));
    menu2.add(menuItem);
    menu.add(menu2);
    menu2 = new JMenu("Right");
    menuItem = new JMenuItem("Long");
    menuItem.addActionListener(
      new editKicker((int)(2*SCREEN_WIDTH/3), 0, 
                     (int)(SCREEN_WIDTH/3), (int)(SCREEN_HEIGHT)));
    menu2.add(menuItem);
    menuItem = new JMenuItem("Upper");
    menuItem.addActionListener(
      new editKicker((int)(2*SCREEN_WIDTH/3), 0, 
                     (int)(SCREEN_WIDTH/3), (int)(SCREEN_HEIGHT/2)));
    menu2.add(menuItem);
    menuItem = new JMenuItem("Lower");
    menuItem.addActionListener(
      new editKicker((int)(2*SCREEN_WIDTH/3), (int)(SCREEN_HEIGHT/2), 
                     (int)(SCREEN_WIDTH/3), (int)(SCREEN_HEIGHT/2)));
    menu2.add(menuItem);
    menuItem = new JMenuItem("2 / 3");
    menuItem.addActionListener(
      new editKicker((int)(SCREEN_WIDTH/3), 0, 
                     (int)(SCREEN_WIDTH*2/3), (int)(SCREEN_HEIGHT)));
    menu2.add(menuItem);
    menu.add(menu2);
    popup.add(menu);
    menuItem = new JMenuItem("Delete file");
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          FileNode.this.deleteFile(t);
        }
      });
    popup.add(menuItem);
    menuItem = new JMenuItem("Rename file");
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          FileNode.this.renameFile(t);
        }
      });
    popup.add(menuItem);
    menuItem = new JMenuItem("Desktop open");
    menuItem.addActionListener(
      new ActionListener() {
        public void actionPerformed(ActionEvent e) {
        	FileNode.this.desktopOpen(t);
        }
      });
    popup.add(menuItem);
/*
    menuItem = new JMenuItem("GZIP file");
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          fileNode.this.gzipFile(t);
        }
      });
    popup.add(menuItem);
    if(f.getName().endsWith(".gz"))
    { 
      menuItem = new JMenuItem("GUNZIP file");
      menuItem.addActionListener(
        new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
            fileNode.this.ungzipFile(t);
          }
        });
      popup.add(menuItem);
    }
    menuItem = new JMenuItem("zip file");
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          fileNode.this.zipFile(t);
        }
      });
    popup.add(menuItem);
    if(f.getName().endsWith(".zip"))
    { 
      menuItem = new JMenuItem("unzip file");
      menuItem.addActionListener(
        new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
            fileNode.this.unzip(t);
          }
        });
      popup.add(menuItem);
    }
 */
    if(handler != null) 
    {
      for(int j = 0 ; j < handler.length ; j++)
      {
        JMenu menus[] = handler[j].getFilesMenu(f);
        if(menus != null) 
        {
          for(int i = 0 ; i < menus.length ; i++)
          {
            popup.add(menus[i]);
          }
        }
      }
    }
    return popup;
  }

  public void editFile(int x, int y, int w, int h)
  {
    OneFileNotepad ofn = OneFileNotepad.note(f.getAbsolutePath());
    final JFrame fe = ofn.getFrame();
    fe.setLocation(new Point(x, y));
    fe.setSize(new Dimension(w, h));
    fe.setTitle(f.getName());
    fe.setVisible(true);
    fe.validate();
    if(tracker != null) 
    {
      fe.addWindowListener(
        new WindowAdapter() 
        {
          public void windowClosing(WindowEvent event) 
          {
  	    tracker.removeTrackedFrame(fe);
          }
        }
      );
      tracker.addTrackedFrame(fe);
    }
    if(handler != null)
    {
      JMenuBar mb = ofn.getMenubar();
      for(int j = 0 ; j < handler.length ; j++)
      {
        JMenu menus[] = handler[j].getEditorsMenu(ofn);
        if(menus != null) 
        {
          for(int i = 0 ; i < menus.length ; i++)
          {
            mb.add(menus[i]);
          }
        }
      }
    }
    fe.validate();
  }

  public void deleteFile(JTree tree)
  {
    int resp = JOptionPane.showConfirmDialog(getCurrentFrame(tree),
                                            "Remove "+f.getName()+" ?", 
                                            "Please confirm", 
                                            JOptionPane.YES_NO_OPTION);
    if(resp == JOptionPane.NO_OPTION) return;
    f.delete();
    FileSystemNode fsn = (FileSystemNode) getParent();
    //Debug.println(""+fsn);
    fsn.remove(this);
    DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
    model.reload(fsn);
  }

  public void renameFile(JTree tree)
  {
    String inputValue = JOptionPane.showInputDialog(getCurrentFrame(tree),
                                                    "Type files new name",
                                                    f.getName());
    if(inputValue == null) return;
    File parent = f.getParentFile();
    File f_new = new File(parent, inputValue.trim());
    if(!f.renameTo(f_new)) return;
    f = f_new;
    FileSystemNode fsn = (FileSystemNode) getParent();
    DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
    model.nodeChanged(this);
    model.reload(this);
  }

  public void desktopOpen(JTree tree) {
	  Clipboard.desktopOpen(f.getAbsolutePath(), tree);
  }

  public void gzipFile(JTree tree)
  {
    File parent = f.getParentFile();
    String outFileName = f.getName()+".gz";
    File f_gz = new File(parent, outFileName);
    GZIPOutputStream out = null;
    try 
    {
      out = new GZIPOutputStream(new FileOutputStream(f_gz));
    } 
    catch(FileNotFoundException e) 
    {
      JOptionPane.showMessageDialog(
         getCurrentFrame(tree), 
         "File "+f.getName()+" not found!", 
         "File error", 
         JOptionPane.ERROR_MESSAGE);
      return;
    }
    catch (IOException e) 
    {
      JOptionPane.showMessageDialog(
         getCurrentFrame(tree), 
         "Failed to create "+outFileName, 
         "File creation error", 
         JOptionPane.ERROR_MESSAGE);
      return;
    }
                    
    FileInputStream in = null;
    try 
    {
      in = new FileInputStream(f);
    } 
    catch (FileNotFoundException e) 
    {
      JOptionPane.showMessageDialog(
         getCurrentFrame(tree), 
         "File "+f.getName()+" not found!", 
         "File error", 
         JOptionPane.ERROR_MESSAGE);
      return;
    }
    catch (IOException e) 
    {
      JOptionPane.showMessageDialog(
         getCurrentFrame(tree), 
         "Failed to create "+outFileName, 
         "File creation error", 
         JOptionPane.ERROR_MESSAGE);
      return;
    }

    try 
    {
      byte[] buf = new byte[1024];
      int len;
      while((len = in.read(buf)) > 0) 
      {
        out.write(buf, 0, len);
      }
      in.close();
      out.finish();
      out.close();
    }
    catch (IOException e) 
    {
      JOptionPane.showMessageDialog(
         getCurrentFrame(tree), 
         "Failed to create "+outFileName, 
         "File creation error", 
         JOptionPane.ERROR_MESSAGE);
      return;
    }
        
    DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
    model.nodeChanged(this);
    model.reload(this);
  }

  public void ungzipFile(JTree tree)
  {
    String outFileName = null;
    try 
    {
      File parent = f.getParentFile();
      String tmp = f.getName();
      int i = tmp.lastIndexOf('.');
      if(i == -1)
      {
        JOptionPane.showMessageDialog(
           getCurrentFrame(tree), 
           "No dot in name of file "+tmp, 
           "File error", 
           JOptionPane.ERROR_MESSAGE);
        return;
      }
      GZIPInputStream in = null;
      try 
      {
        in = new GZIPInputStream(new FileInputStream(f));
      } 
      catch(FileNotFoundException e) 
      {
        JOptionPane.showMessageDialog(
           getCurrentFrame(tree), 
           "File "+f.getName()+" not found!", 
           "File error", 
           JOptionPane.ERROR_MESSAGE);
        return;
      }

      outFileName = tmp.substring(0,i);
      File f_gz = new File(parent, outFileName);

      FileOutputStream out = null;
      try 
      {
        out = new FileOutputStream(outFileName);
      } 
      catch(FileNotFoundException e) 
      {
        JOptionPane.showMessageDialog(
           getCurrentFrame(tree), 
           "File "+outFileName+" not found!", 
           "File error", 
           JOptionPane.ERROR_MESSAGE);
        return;
      }

      byte[] buf = new byte[1024];
      int len;
      while((len = in.read(buf)) > 0) 
      {
        out.write(buf, 0, len);
      }

      in.close();
      out.close();
    } 
    catch(IOException e) 
    {
      JOptionPane.showMessageDialog(
         getCurrentFrame(tree), 
         "Failed to create "+outFileName, 
         "File creation error", 
         JOptionPane.ERROR_MESSAGE);
      return;
    }
  }

  public void unzip(JTree tree)
  {
    int idx = f.getName().lastIndexOf('.');
    File nf = new File(f.getParent(), f.getName().substring(0, idx));
    Unzip.unzipit(f);
    /*
    JOptionPane.showMessageDialog(
       getCurrentFrame(tree), 
       "Folder ZIP not implemented", 
       "Information", 
       JOptionPane.INFORMATION_MESSAGE);
    /*
    byte[] buf = new byte[1024];

    try 
    {
      ZipOutputStream out = 
        new ZipOutputStream(
          new FileOutputStream(f.getName()+".zip"));
      // Compress the files
      //for (int i=1; i<files.length; i++) 
      {
        FileInputStream in = new FileInputStream(f);
        out.putNextEntry(new ZipEntry(f.getName()));
        // Transfer bytes from the file to the ZIP file
        int len;
        while((len = in.read(buf)) > 0) 
        {
          out.write(buf, 0, len);
        }
        // Complete the entry
        out.closeEntry();
        in.close();
      }
    } 
    catch(IOException e) 
    {
      e.printStackTrace();
      System.exit(1);
    }*/
    DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
    //model.nodeChanged(this);
    model.reload((FileSystemNode) this);
  }


  public void zipFile(JTree tree)
  {
    String inputValue = 
      JOptionPane.showInputDialog(
        getCurrentFrame(tree),
        "Type zip-files name",
        f.getName()+".zip");
    if(inputValue == null) return;
    File nf = new File(f.getParent(), inputValue.trim());
    byte[] buf = new byte[1024];

    try 
    {
      ZipOutputStream out = 
        new ZipOutputStream(
          new FileOutputStream(f.getName()+".zip"));
      // Compress the files
      //for (int i=1; i<files.length; i++) 
      {
        FileInputStream in = new FileInputStream(f);
        out.putNextEntry(new ZipEntry(f.getName()));
        // Transfer bytes from the file to the ZIP file
        int len;
        while((len = in.read(buf)) > 0) 
        {
          out.write(buf, 0, len);
        }
        // Complete the entry
        out.closeEntry();
        in.close();
      }
    } 
    catch(IOException e) 
    {
      e.printStackTrace();
      System.exit(1);
    }
/*
    JOptionPane.showMessageDialog(
       getCurrentFrame(tree), 
       "ZIP not implemented", 
       "Information", 
       JOptionPane.INFORMATION_MESSAGE);
 */    
    DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
    //model.nodeChanged(this);
    model.reload((FileSystemNode) this);
  }

  public class editKicker implements ActionListener
  {
    int x,y,w,h;
    public editKicker(int x, int y, int w, int h) {
      this.x = x; this.y = y; this.w = w; this.h = h;
    }
    public void actionPerformed(ActionEvent e) {
      FileNode.this.editFile(x,y,w,h);
    }
  }

}
