package se.modlab.simpleide;

import se.modlab.generics.*;
import javax.swing.tree.*;
import java.util.*;
import java.io.*;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;

public class TopNode extends FileSystemNode
{

  public TopNode(File f, IdeMenuHandler handler[], FrameTracker tracker)
  {
    super(f, handler, tracker);
  }

  public boolean contains(File _f)
  {
    for(int i = 0 ; i < getChildCount() ; i++)
    {
      File oneChildFile = ((FileSystemNode) getChildAt(i)).getFile();
      if(oneChildFile.compareTo(_f) == 0)
      {
        return true;
      }
    }
    return false;
  }

  public boolean remove(File _f)
  {
    for(int i = 0 ; i < getChildCount() ; i++)
    {
      File child = ((FileSystemNode)getChildAt(i)).getFile();
      if(child.compareTo(_f) == 0)
      {
        remove(i);
        return true;
      }
    }
    return false;
  }

  public String toString()
  {
    try
    {
      return f.getCanonicalPath();
    }
    catch(IOException ioe)
    {
      return f.getAbsolutePath();  
    }
  }

  public JPopupMenu getPopup(JTree tree)
  {
    JPopupMenu popup = new JPopupMenu();
    JMenuItem menuItem = new JMenuItem("New file");
    final JTree t = tree;
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          TopNode.this.newFile(t);
        }
      });
    popup.add(menuItem);
    menuItem = new JMenuItem("New folder");
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          TopNode.this.newFolder(t);
        }
      });
    popup.add(menuItem);
    menuItem = new JMenuItem("Refresh folder");
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          TopNode.this.refreshFolder(t);
        }
      });
    popup.add(menuItem);
    return popup;
  }

  public void newFile(JTree tree)
  {
    String inputValue = JOptionPane.showInputDialog(
      getCurrentFrame(tree),
      "Type files name");
    if(inputValue == null) return;
    File nf = new File(f, inputValue.trim());
    try
    {
      if(nf.createNewFile())
      {
        FileSystemNode fsn = new FileNode(nf, handler, tracker);
        add(fsn);
        fsn.setParent(this);
      }      
      DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
      model.reload(this);
    }
    catch(IOException e)
    {
    }
  }

  public void newFolder(JTree tree)
  {
    String inputValue = JOptionPane.showInputDialog(
      getCurrentFrame(tree),
      "Type folders name");
    if(inputValue == null) return;
    File nf = new File(f, inputValue.trim());
    if(nf.mkdir()) 
    {
      FileSystemNode fsn = new FolderNode(nf, handler, tracker);
      add(fsn);
      fsn.setParent(this);
    }      
    DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
    model.reload(this);
  }

  public void refreshFolder(JTree tree)
  {
    DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
    model.reload(this);
  }

}

