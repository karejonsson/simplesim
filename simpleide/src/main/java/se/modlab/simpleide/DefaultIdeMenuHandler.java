package se.modlab.simpleide;

import javax.swing.*;
import se.modlab.generics.gui.files.*;
import java.io.*;
import java.awt.*;

public class DefaultIdeMenuHandler implements IdeMenuHandler
{

  //Vector frames = new Vector();

  public static Frame getFrame(Component c)
  {
    if(c == null) return null;
    if(c instanceof Frame) return (Frame) c;
    return getFrame(c.getParent());
  }

  public static void info(Component c, String message)
  {
    Frame f = getFrame(c);
    info(f, message, "Information");
  }

  public static void info(Component c, String message, String title)
  {
    Frame f = getFrame(c);
    JOptionPane.showMessageDialog(f, message, title, JOptionPane.INFORMATION_MESSAGE);
  }

  public static void error(Component c, String message)
  {
    Frame f = getFrame(c);
    error(f, message, "Error");
  }

  public static void error(Component c, String message, String title)
  {
    Frame f = getFrame(c);
    JOptionPane.showMessageDialog(f, message, title, JOptionPane.ERROR_MESSAGE);
  }

  public JMenu[] getFilesMenu(File f)
  {
    return null;
  }

  public JMenu[] getEditorsMenu(OneFileNotepad ofn)
  { 
    return null;
  }

  public JMenu[] getFoldersMenu(File f)
  {
    return null;
  }

  public JMenu[] getMenubar()
  {
    return null;
  }

}