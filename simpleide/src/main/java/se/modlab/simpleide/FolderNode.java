package se.modlab.simpleide;

import se.modlab.generics.*;
import se.modlab.generics.files.*;
import javax.swing.tree.*;
import java.util.*;
import java.util.zip.*;
import java.io.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


public class FolderNode extends FileSystemNode
{

  public FolderNode(File f, IdeMenuHandler handler[], FrameTracker tracker)
  {
    super(f, handler, tracker);
  }

  public boolean contains(File _f)
  {
    for(int i = 0 ; i < getChildCount() ; i++)
    {
      File oneChildFile = ((FileSystemNode) getChildAt(i)).getFile();
      if(oneChildFile.compareTo(_f) == 0)
      {
        return true;
      }
    }
    return false;
  }

  public boolean remove(File _f)
  {
    for(int i = 0 ; i < getChildCount() ; i++)
    {
      File child = ((FileSystemNode)getChildAt(i)).getFile();
      if(child.compareTo(_f) == 0)
      {
        remove(i);
        return true;
      }
    }
    return false;
  }

  public String toString()
  {
    return f.getName();
  }

  public JPopupMenu getPopup(JTree tree)
  {
    JPopupMenu popup = new JPopupMenu();
    JMenuItem menuItem = new JMenuItem("New file");
    final JTree t = tree;
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          FolderNode.this.newFile(t);
        }
      });
    popup.add(menuItem);
    menuItem = new JMenuItem("New folder");
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          FolderNode.this.newFolder(t);
        }
      });
    popup.add(menuItem);
    menuItem = new JMenuItem("Delete folder");
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          FolderNode.this.deleteFolder(t);
        }
      });
    popup.add(menuItem);
    menuItem = new JMenuItem("Rename folder");
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          FolderNode.this.renameFolder(t);
        }
      });
    popup.add(menuItem);
    menuItem = new JMenuItem("Refresh folder");
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          FolderNode.this.refreshFolder(t);
        }
      });
    popup.add(menuItem);
/*
    menuItem = new JMenuItem("zip folder");
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          folderNode.this.zipFolder(t);
        }
      });
    popup.add(menuItem);
    if(f.getName().endsWith(".zip"))
    { 
      menuItem = new JMenuItem("unzip");
      menuItem.addActionListener(
        new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
            folderNode.this.unzip(t);
          }
        });
      popup.add(menuItem);
    }
 */
    if(handler != null) 
    {
      for(int j = 0 ; j < handler.length ; j++)
      {
        JMenu menus[] = handler[j].getFoldersMenu(f);
        if(menus != null) 
        {
          for(int i = 0 ; i < menus.length ; i++)
          {
            popup.add(menus[i]);
          }
        }
      }
    }
    return popup;
  }

  public void newFile(JTree tree)
  {
    String inputValue = JOptionPane.showInputDialog(
      getCurrentFrame(tree),
      "Type files name");
    if(inputValue == null) return;
    File nf = new File(f, inputValue.trim());
    try
    {
      if(nf.createNewFile())
      {
        FileSystemNode fsn = new FileNode(nf, handler, tracker);
        add(fsn);
        fsn.setParent(this);
      }      
      DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
      model.reload((FileSystemNode) this);
    }
    catch(IOException e)
    {
    }
  }

  public void newFolder(JTree tree)
  {
    String inputValue = JOptionPane.showInputDialog(
      getCurrentFrame(tree),
      "Type folders name");
    if(inputValue == null) return;
    File nf = new File(f, inputValue.trim());
    if(nf.mkdir()) 
    {
      FileSystemNode fsn = new FolderNode(nf, handler, tracker);
      add(fsn);
      fsn.setParent(this);
    }      
    DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
    //model.nodeChanged(this);
    model.reload((FileSystemNode) this);
  }

  private static void removeFolder(File f)
  {
    if(!f.isDirectory())
    {
      f.delete();
      return;  
    }
    File[] fs = f.listFiles();
    for(int i = 0 ; i < fs.length ; i++)
    {   
      removeFolder(fs[i]);
    }
    f.delete();
  }

  public void deleteFolder(JTree tree)
  {
    int resp = JOptionPane.showConfirmDialog(getCurrentFrame(tree),
                                            "Remove folder "+f.getName()+" ?", 
                                            "Please confirm", 
                                            JOptionPane.YES_NO_OPTION);
    if(resp != JOptionPane.YES_OPTION) return;
    removeFolder(f);
  }

  public void renameFolder(JTree tree)
  {
    String inputValue = JOptionPane.showInputDialog(getCurrentFrame(tree),
                                                    "Type folders new name");
    if(inputValue == null) return;
    File parent = f.getParentFile();
    File f_new = new File(parent, inputValue.trim());
    if(!f.renameTo(f_new)) return;
    f = f_new;
    FileSystemNode fsn = (FileSystemNode) getParent();
    DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
    model.nodeChanged(this);
    model.reload(this);
  }

  public void refreshFolder(JTree tree)
  {
    DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
    model.reload(this);
  }

  public void zipFolder(JTree tree)
  {
    String inputValue = 
      JOptionPane.showInputDialog(
        getCurrentFrame(tree),
        "Type zip-files name",
        f.getName()+".zip");
    if(inputValue == null) return;
    File nf = new File(f.getParent(), inputValue.trim());
    Zip.zipit(f, nf);
    /*
    JOptionPane.showMessageDialog(
       getCurrentFrame(tree), 
       "Folder ZIP not implemented", 
       "Information", 
       JOptionPane.INFORMATION_MESSAGE);
    /*
    byte[] buf = new byte[1024];

    try 
    {
      ZipOutputStream out = 
        new ZipOutputStream(
          new FileOutputStream(f.getName()+".zip"));
      // Compress the files
      //for (int i=1; i<files.length; i++) 
      {
        FileInputStream in = new FileInputStream(f);
        out.putNextEntry(new ZipEntry(f.getName()));
        // Transfer bytes from the file to the ZIP file
        int len;
        while((len = in.read(buf)) > 0) 
        {
          out.write(buf, 0, len);
        }
        // Complete the entry
        out.closeEntry();
        in.close();
      }
    } 
    catch(IOException e) 
    {
      e.printStackTrace();
      System.exit(1);
    }*/
    DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
    //model.nodeChanged(this);
    model.reload((FileSystemNode) this);
  }

  public void unzip(JTree tree)
  {
    int idx = f.getName().lastIndexOf('.');
    File nf = new File(f.getParent(), f.getName().substring(0, idx));
    Unzip.unzipit(f);
    /*
    JOptionPane.showMessageDialog(
       getCurrentFrame(tree), 
       "Folder ZIP not implemented", 
       "Information", 
       JOptionPane.INFORMATION_MESSAGE);
    /*
    byte[] buf = new byte[1024];

    try 
    {
      ZipOutputStream out = 
        new ZipOutputStream(
          new FileOutputStream(f.getName()+".zip"));
      // Compress the files
      //for (int i=1; i<files.length; i++) 
      {
        FileInputStream in = new FileInputStream(f);
        out.putNextEntry(new ZipEntry(f.getName()));
        // Transfer bytes from the file to the ZIP file
        int len;
        while((len = in.read(buf)) > 0) 
        {
          out.write(buf, 0, len);
        }
        // Complete the entry
        out.closeEntry();
        in.close();
      }
    } 
    catch(IOException e) 
    {
      e.printStackTrace();
      System.exit(1);
    }*/
    DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
    //model.nodeChanged(this);
    model.reload((FileSystemNode) this);
  }

}











