package se.modlab.simplesim.algorithms.bruteforcemax;

import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;
import se.modlab.generics.exceptions.*;
import java.util.*;
import se.modlab.generics.*;
import se.modlab.simplesim.algorithms.share.*;
import se.modlab.simplesim.scoping.*;

public class FirstLevelTreeNodeAverageCreatorVisitor
       extends InstanceVisitorAdapter
{

  private Vector averageCreators = new Vector();

  public FirstLevelTreeNodeAverageCreatorVisitor()
  {
  }

  public ArithmeticAverageCreator[] getCreators()
  {
    ArithmeticAverageCreator out[] = 
      new ArithmeticAverageCreator[averageCreators.size()];
    for(int i = 0 ; i < averageCreators.size() ; i++)
    {
      out[i] = (ArithmeticAverageCreator) averageCreators.elementAt(i);
    }
    return out;
  }

  public void visit(BooleanVariable inst) 
    throws IntolerableException 
  {
  }
 
  public void visit(BooleanConst inst) 
    throws IntolerableException 
  {
  }
 
  public void visit(SimBooleanExported inst) 
    throws IntolerableException 
  {
  }
 
  public void visit(SimBooleanMonitoredVariable inst) 
    throws IntolerableException 
  {
  }
 
  public void visit(DoubleVariable inst) 
    throws IntolerableException 
  {
    averageCreators.addElement(new ArithmeticAverageCreator(inst));
  }
 
  public void visit(DoubleConst inst) 
    throws IntolerableException 
  {
    averageCreators.addElement(new ArithmeticAverageCreator(inst));
  }
 
  public void visit(SimDoubleExported inst) 
    throws IntolerableException 
  {
  }
 
  public void visit(SimDoubleMonitoredVariable inst) 
    throws IntolerableException 
  {
    averageCreators.addElement(new ArithmeticAverageCreator(inst));
  }
 
  public void visit(LongVariable inst) 
    throws IntolerableException 
  {
    averageCreators.addElement(new ArithmeticAverageCreator(inst));
  }
 
  public void visit(LongConst inst) 
    throws IntolerableException 
  {
    averageCreators.addElement(new ArithmeticAverageCreator(inst));
  }
 
  public void visit(SimLongExported inst) 
    throws IntolerableException 
  {
  }
 
  public void visit(SimLongMonitoredVariable inst) 
    throws IntolerableException 
  {
    averageCreators.addElement(new ArithmeticAverageCreator(inst));
  }
 
  public void visit(SimComplexVariableExported inst) 
    throws IntolerableException 
  {
  }

  public void visit(ComplexVariable inst) 
    throws IntolerableException 
  {
    traverse(inst);
  }

  public void visit(SimVariableVectorExported inst) 
    throws IntolerableException 
  {
  }

  public void visit(VariableVector inst) 
    throws IntolerableException 
  {
    traverse(inst);
  }

  public void visit(ScopeReference inst) 
    throws IntolerableException 
  {
  }

  public void visit(ScopeReferenceExported inst) 
    throws IntolerableException 
  {
  }

  public void visit(SubscopeReference inst) 
    throws IntolerableException 
  {
    traverse((Complex)inst);
  }

  public void visit(SubscopeReferenceExported inst) 
    throws IntolerableException 
  {
    traverse((Complex)inst);
  }

}
