package se.modlab.simplesim.algorithms.average;

import se.modlab.generics.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.simplesim.events.*;
import se.modlab.simplesim.algorithms.setup.*;
import se.modlab.simplesim.algorithms.regression.*;
import se.modlab.simplesim.algorithms.share.*;
import se.modlab.simplesim.algorithms.once.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.swing.*;
import java.util.*;
import java.io.*;


public class AverageAlgorithmQuiet extends AverageAlgorithm
{

  public AverageAlgorithmQuiet(SimData sd,
                               double startTimeRegression,
                               double endTimeRegression,
                               long intervals,
                               long maximumIterations)
    throws IntolerableException
  {
    super(sd, startTimeRegression, endTimeRegression, intervals, maximumIterations);
  }

  public void addMessage(String msg) {};

}