package se.modlab.simplesim.algorithms.samplers;

import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.evaluables.*;
import java.util.*;
import se.modlab.simplesim.events.*;
import se.modlab.simplesim.references.*;
import se.modlab.simplesim.scoping.*;

public class InstanceEndValueSamplerCollectorVisitor extends InstanceVisitorAdapter
{

  private Vector samplers = new Vector();
  //private scope s;
  private Stack names = new Stack();

  public InstanceEndValueSamplerCollectorVisitor()
  {
    //this.s = s;
  }
 
  public EndValueSampler[] getSamplers()
  {
    EndValueSampler itrs[] = new EndValueSampler[samplers.size()];
    for(int i = 0 ; i < samplers.size() ; i++)
    {
      itrs[i] = (EndValueSampler) samplers.elementAt(i);
    }
    return itrs;
  }

  public String getFullName()
  {
    StringBuffer sb = new StringBuffer();
    for(int i = 0 ; i < names.size() ; i++)
    {
      sb.append(names.elementAt(i).toString());
    }
    return sb.toString();
  }
 
  private void traverse(VariableInstance inst[])
    throws IntolerableException 
  {
    for(int i = 0 ; i < inst.length ; i++)
    {
      inst[i].accept(this);
    }
  }

  public void visit(BooleanVariable inst) 
    throws IntolerableException 
  {
    //boolean b = inst.getValue().getBoolean().booleanValue();
    samplers.addElement(new BooleanEndValueSampler(inst, getFullName()+inst.getName()));
  }
 
  public void visit(BooleanConst inst) 
    throws IntolerableException 
  {
    throw new InternalError(
      "Visitor instanceEndValueSamplerCollectorVisitor fell on Boolean const. \n"+
      "Composite instance cannot contain constant");
  }
 
  public void visit(SimBooleanMonitoredVariable inst) 
    throws IntolerableException 
  {
    //boolean b = inst.getValue().getBoolean().booleanValue();
    samplers.addElement(new BooleanEndValueSampler(inst, getFullName()+inst.getName()));
  }
 
  public void visit(DoubleVariable inst) 
    throws IntolerableException 
  {
    //double d = inst.getValue().getValue();
    samplers.addElement(new ArithmeticEndValueSampler(inst, getFullName()+inst.getName()));
  }
 
  public void visit(DoubleConst inst) 
    throws IntolerableException 
  {
    throw new InternalError(
      "Visitor instanceEndValueSamplerCollectorVisitor fell on Double const. \n"+
      "Composite instance cannot contain constant");
  }
 
  public void visit(SimDoubleMonitoredVariable inst) 
    throws IntolerableException 
  {
    //double d = inst.getValue().getValue();
    samplers.addElement(new ArithmeticEndValueSampler(inst, getFullName()+inst.getName()));
  }
 
  public void visit(LongVariable inst) 
    throws IntolerableException 
  {
    //long l = inst.getValue().getLong().longValue();
    samplers.addElement(new ArithmeticEndValueSampler(inst, getFullName()+inst.getName()));
  }
 
  public void visit(LongConst inst) 
    throws IntolerableException 
  {
    throw new InternalError(
      "Visitor instanceEndValueSamplerCollectorVisitor fell on Long const. \n"+
      "Composite instance cannot contain constant");
  }
 
  public void visit(SimLongMonitoredVariable inst) 
    throws IntolerableException 
  {
    //long l = inst.getValue().getLong().longValue();
    samplers.addElement(new ArithmeticEndValueSampler(inst, getFullName()+inst.getName()));
  }
 
  public void visit(ComplexVariable inst) 
    throws IntolerableException 
  {
    names.push(inst.getName()+".");
    traverse(inst);
    names.pop();
  }

  public void visit(VariableVector inst) 
    throws IntolerableException 
  {
    names.push(inst.getName()+".");
    traverse(inst);
    names.pop();
  }

  public void visit(SimVariableVector inst) 
    throws IntolerableException 
  {
    names.push(inst.getName()+".");
    traverse(inst);
    names.pop();
  }

  public void visit(VariableVectorFromFile inst) 
    throws IntolerableException 
  {
    names.push(inst.getName());
    traverse(inst);
    names.pop();
  }

  public void visit(SimMonitoredVectorVariableFromFile inst) 
    throws IntolerableException 
  {
    names.push(inst.getName());
    traverse(inst);
    names.pop();
  }

  public void visit(ProcedureRepeating inst) 
    throws IntolerableException 
  {
  }

  public void visit(ProcedureSingle inst) 
    throws IntolerableException 
  {
  }

  public void visit(ProcedureTrigged inst) 
    throws IntolerableException 
  {
  }

  public void visit(ProcedureActor inst) 
    throws IntolerableException 
  {
  }

  public void visit(UntypedQueuableReference inst) 
    throws IntolerableException 
  {
  }

  public void visit(TypedQueuableReference inst) 
    throws IntolerableException 
  {
  }

  public void visit(SimVariableVectorExported inst) 
    throws IntolerableException 
  {
    //names.push(inst.getName());
    //traverse(inst);
    //names.pop();
  }

  public void visit(SimComplexVariableExported inst) 
    throws IntolerableException 
  {
    //names.push(inst.getName());
    //traverse(inst);
    //names.pop();
  }

  protected void traverse(VariableVectorFromFile inst)
    throws IntolerableException
  {
    //System.out.println("instanceVisitorAdapter traverse SimVariableVector "+inst.getName());
    for(int i = 0 ; i < inst.getLength() ; i++)
    {
      VariableInstance _inst = inst.getVectorElement(i);
      //names.push("["+i+"].");
      _inst.accept(this);
      //names.pop();
    }
  }

  public void visit(ScopeReference inst) 
    throws IntolerableException 
  {
  }

  public void visit(ScopeReferenceExported inst) 
    throws IntolerableException 
  {
  }

}