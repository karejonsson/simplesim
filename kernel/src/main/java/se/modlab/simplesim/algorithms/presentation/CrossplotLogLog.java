package se.modlab.simplesim.algorithms.presentation;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class CrossplotLogLog extends Crossplot 
{

  public CrossplotLogLog(MonitoredVariableExtracted mveh, 
                         MonitoredVariableExtracted mvev, 
                         CrossplotAdder cpa,
                         SegmentPainter sp)
  {
    super(mveh, mvev, cpa, sp);
  }
  protected boolean makeChecks(Graphics g, Dimension d)
  {
    int line = 1;
    if(mveh.getMinValue() <= 0)
    {
      line++;
      g.drawString("The variable "+mveh.getName()+" takes negative values which",
                   margin, line*margin);
      line++;
      g.drawString("cannot be plotted with logarithmic conversion.",
                   margin, line*margin);
      line++;
    }
    if(mvev.getMinValue() <= 0)
    {
      line++;
      g.drawString("The variable "+mvev.getName()+" takes negative values which",
                   margin, line*margin);
      line++;
      g.drawString("cannot be plotted with logarithmic conversion.",
                   margin, line*margin);
      line++;
    }
    return (line == 1);
  }

  public double getVerticalTransition(double v)
  {
    return Math.log(v);
  }

  public double getHorizontalTransition(double h)
  {
    return Math.log(h);
  }

  public double getVerticalInversion(double v)
  {
    return Math.exp(v);
  }

  public double getHorizontalInversion(double h)
  {
    return Math.exp(h);
  }

  public String getHorizontalTransitionName()
  {
    return "logarithmic";
  }

  public String getVerticalTransitionName()
  {
    return "logarithmic";
  }

}
