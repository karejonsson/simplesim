package se.modlab.simplesim.algorithms.presentation;

import se.modlab.generics.gui.util.*;
import se.modlab.generics.files.*;
import se.modlab.generics.sstruct.values.*;
import java.util.Enumeration;
import java.util.Vector;
import javax.swing.tree.TreeNode;
import javax.swing.*;
import java.awt.Component;
//import java.awt.*;
import se.modlab.generics.sstruct.variables.*;

public class Sources implements SimpleExplorerNode
{
  private TreeNode parent;
  private SourceNode sn[] = null;

  public Sources(TreeNode parent, FileCollector cls[])
  {
    this.parent = parent;
    sn = new SourceNode[cls.length];
    for(int i = 0 ; i < cls.length ; i++)
    {
      sn[i] = new SourceNode(this, cls[i]);
    }
  }

  public Enumeration children()
  {
    return null;
  }

  public boolean getAllowsChildren()
  {
    return false;
  }

  public TreeNode getChildAt(int childIndex)
  {
    return sn[childIndex];
  }

  public int getChildCount()
  {
    return sn.length;
  }

  public int getIndex(TreeNode tn)
  {
    return -1;
  }

  public TreeNode getParent()
  {
    return parent;
  }

  public boolean isLeaf()
  {
    return false;
  }

  public Component getComponent()
  {
    JTextArea ta = new JTextArea();
    ta.setText("Node for the sourcefiles used in this simualtion");
    ta.setEditable(false);
    return ta;
  }  

  public String toString()
  {
    if(sn.length == 0) return "No sources";
    if(sn.length == 1) return "The source";
    return "The sources";
  }

} 
