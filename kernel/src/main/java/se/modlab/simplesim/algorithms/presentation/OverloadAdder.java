package se.modlab.simplesim.algorithms.presentation;

import javax.swing.*;

public interface OverloadAdder
{
  public boolean addOneOverload(JComponent cp);
  public boolean removeOneOverload(JComponent cp);
}