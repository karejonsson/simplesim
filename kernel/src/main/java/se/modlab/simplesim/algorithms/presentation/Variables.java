package se.modlab.simplesim.algorithms.presentation;

import javax.swing.JTextArea;
import javax.swing.tree.*;
import java.awt.Component;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.algorithms.externalpainters.RightExplorerSideComponent;

public class Variables extends IntermediateTreeNode
{
	private TreeNode parent;
	private SegmentPainter sp;

	public Variables(SegmentPainter _sp)
	{
		super(null, "Globals");
		sp = _sp;
	}

	public Component getComponent()
	{
		JTextArea ta = new JTextArea();
		ta.append("Subjacent variables were monitored during simulation!");
		return ta;
	}  

	public String toString()
	{
		return "Variables";
	}

	public MonitoredSimVariable[] getStandardMonitoredVariablesComponents()
	throws IntolerableException
	{
		VariableComponentCollector vcc = new VariableComponentCollector();
		for(int i = 0 ; i < getChildCount() ; i++)
		{
			VariableNode vn = (VariableNode) getChildAt(i);
			vn.accept(vcc);
		}
		return vcc.getStandardMonitoredVariablesComponents();
	}

	public RightExplorerSideComponent[] getExternallyPaintedMonitoredVariablesComponents(String nameOfExternalPainter)
	throws IntolerableException
	{
		VariableComponentCollector vcc = new VariableComponentCollector();
		for(int i = 0 ; i < getChildCount() ; i++)
		{
			VariableNode vn = (VariableNode) getChildAt(i);
			vn.accept(vcc);
		}
		return vcc.getExternallyPaintedMonitoredVariablesComponents(nameOfExternalPainter);
	}

} 
