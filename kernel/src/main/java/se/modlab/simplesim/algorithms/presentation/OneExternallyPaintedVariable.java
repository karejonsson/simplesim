package se.modlab.simplesim.algorithms.presentation;

import java.awt.Component;
import javax.swing.tree.TreeNode;
import se.modlab.simplesim.algorithms.externalpainters.PlugInPainter;
import se.modlab.simplesim.algorithms.externalpainters.RightExplorerSideComponent;

public class OneExternallyPaintedVariable extends VariableNode
{
	private String fullName = null;
	private RightExplorerSideComponent exsc = null;
	private PlugInPainter pip;

	public OneExternallyPaintedVariable(String _fullName, 
			TreeNode _parent,
			PlugInPainter _pip)
	{
		super(_parent);
		fullName = _fullName;
		pip = _pip;
	}

	public Component getComponent()
	{
		if(exsc == null) { 
			exsc = new RightExplorerSideComponent(pip);
		}
		return exsc;
	}  

	public String toString()
	{
		return pip.getPluginsName();
	}
	
	public PlugInPainter getPlugInPainter() {
		return pip;
	}
	
	public String getFullName() {
		return fullName;
	}

} 