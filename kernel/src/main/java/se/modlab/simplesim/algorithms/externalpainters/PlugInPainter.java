package se.modlab.simplesim.algorithms.externalpainters;

import java.awt.Component;
import java.awt.Graphics;

import javax.swing.JPopupMenu;

public interface PlugInPainter {

	public String getPluginsName();
	public void paint(Component comp, Graphics g);
	public JPopupMenu getPopup(Component comp);
	public String getVariableName();
	
}
