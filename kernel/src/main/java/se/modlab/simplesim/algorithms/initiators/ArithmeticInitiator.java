package se.modlab.simplesim.algorithms.initiators;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;

public class ArithmeticInitiator implements Initiator
{

  private Variable var;
  private Scope s;
  private ArithmeticEvaluable ae;
  private String fakedName;
  private int samples;

  public ArithmeticInitiator(Variable _var,
                             Scope _s,
                             ArithmeticEvaluable _ae)
  {
    var = _var;
    s = _s;
    ae = _ae;
    fakedName = var.getName();
  }

  public ArithmeticInitiator(Variable _var, 
                             Scope _s,
                             ArithmeticEvaluable _ae,
                             String _fakedName)
  {
    var = _var;
    s = _s;
    ae = _ae;
    fakedName = _fakedName;
    //System.out.println("arithmeticInitiator<init> ae == null "+(ae == null));
  }

  public String getName()
  {
    return var.getName();
  }

  public VariableInstance getInstance()
  {
    return var;
  }
  
  public void setInitatingExpression(ArithmeticEvaluable _ae) {
	  ae = _ae;
  }

  public void runInitiation() throws IntolerableException
  {
    //System.out.println("arithmeticInitiator - runInitiation "+var+
    //                   ", ae = "+ae);
    //(new Throwable()).printStackTrace();
    
    if(ae == null)
    { 
      if(var instanceof LongVariable)
      {
        var.setInitialValue(new sLong(0));
        return;
      }
      if(var instanceof DoubleVariable)
      {
        var.setInitialValue(new sDouble(0));
        return;
      }
      throw new InternalError("Arithmetic initiator operates on variable\n"+
                              "of type "+var.getClass().getName());
    }
    sValue val = ae.evaluate(s);
    var.setInitialValue(val);
  }

  public String toString()
  {
    return "Arithmetic initiator for variable "+var;
  }

}

