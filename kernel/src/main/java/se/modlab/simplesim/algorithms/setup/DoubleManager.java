package se.modlab.simplesim.algorithms.setup;

import java.io.InputStream;
import java.util.Hashtable;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.logics.LogicalExpression;
import se.modlab.simplesim.variables.*;
import se.modlab.simplesim.algorithms.initiators.*;
import se.modlab.simplesim.algorithms.samplers.*;
import se.modlab.simplesim.scoping.*;

public class DoubleManager implements Manager
{
  
  private String name = null;
  private ArithmeticEvaluable ae;
  private String filename;
  private int line;
  private int column;
  private SimData sd = null;
  private boolean _con;
  private boolean _mon;
  private boolean _exp;
  private boolean _pub;
  public static final String type = "double";

	private Scope lastSeenScope = null;
	private Variable var = null;

  public DoubleManager(String _name,
                       ArithmeticEvaluable _ae,
                       String _filename,
                       int _line,
                       int _column,
                       SimData _sd,
                       boolean _const,
                       boolean _monitor,
                       boolean _export,
                       boolean _public)
  {
    name = _name;
    ae = _ae;
    filename = _filename;
    line = _line;
    column = _column;
    sd = _sd;
    _con = _const;
    _mon = _monitor;
    _exp = _export;
    _pub = _public;
  }

  public String getFilename()
  {
    return filename;
  }  

  public int getLine()
  {
    return line;
  }  

  public int getColumn()
  {
    return line;
  }  

  private void _initialize(Scope s, boolean isSharp, Hashtable<VariableInstance, Manager> managermap)
    throws IntolerableException
  {
	  lastSeenScope = s;
    VariableFactory vf = s.getFactory(type);
    if(vf == null)
    {
      throw new UserCompiletimeError(
         "No factory for type "+type+" found.\n"+
         "This happens with the variable "+name+".");
    }
    //System.out.println("Initialize in "+s.getName()+" for "+toString());
    if(_mon)
    {
      SimDoubleMonitoredVariable tmp = 
        new SimDoubleMonitoredVariable(name, 
                                       vf,
                                       filename, line, column, sd.getCommons());
      var = tmp;
    }
    else
    {
      if(!_con)
      {
        var = new DoubleVariable(name, vf, filename, line, column);
      }
      else
      {
        var = new DoubleConst(name, vf, filename, line, column);
      }
    }
    if(isSharp)
    {
      sd.addInitiator(new ArithmeticInitiator(var, s, ae));
      sd.addEndValueSampler(new ArithmeticEndValueSampler(var));
      if(_mon) 
      {
        //sd.addMonitoredVariable((monitoredVariable) var);
      }
    }
    sValue val = null;
    if(ae != null)
    {
      val = ae.evaluate(s);
    }
    else
    {
      val = new sDouble(0.0);
    }
    var.setInitialValue(val);
    s.addVariable(var);
    //System.out.println("Double "+var.getName()+" initialized to "+val+", scope = "+s.getName());
    if(_exp) {
      Scope gs = sd.getGlobalScope();
      if(gs == s) {
        throw new UserCompiletimeError(
          "You may not export variables from global scope.\n"+
          "This happens with the variable "+name+".");
      }
      SimScope ss = (SimScope) s;
      ss.addMember(new SimDoubleExported((DoubleVariable) var));
    }
    if(_pub) {
      Scope gs = sd.getGlobalScope();
      if(gs == s) {
        throw new UserCompiletimeError(
          "You may not declare variables public in global scope.\n"+
          "This happens with the variable "+name+".");
      }
      SimScope ss = (SimScope) s;
      ss.addMember(var);
    }
	if(managermap != null) {
		managermap.put(var, this);
	}
  } 
  
  public void initialize(Scope s, boolean isSharp, Hashtable<VariableInstance, Manager> managermap)
  throws IntolerableException
  {
	  //System.out.println("doubleManager: initialize sharp = "+isSharp);
    try {
      _initialize(s, isSharp, managermap);
    }
	catch(UserRuntimeError ue) {
	      String m = ue.getMessage()+"\n"+"This happened in file "+filename+", line "+line+", column "+column;
	      UserRuntimeError ue2 = new UserRuntimeError(m, ue.getCause(), ue.getCollectors());
		  ue2.setAction(ue.getAction());
		  throw ue2;
	    }
	catch(UserCompiletimeError ue) {
	      String m = ue.getMessage()+"\n"+"This happened in file "+filename+", line "+line+", column "+column;
	      UserCompiletimeError ue2 = new UserCompiletimeError(m, ue.getCause(), ue.getCollectors());
		  ue2.setAction(ue.getAction());
		  throw ue2;
	    }
  }

  public String toString()
  {
    return "doubleManager for "+name+
           " with const = "+_con+
           ", monitor = "+_mon+
           ", export = "+_exp+
           ", public = "+_pub+
           " in file "+filename+" on line "+line+" on column "+column;
  }
  
  
	public void reinitializeLogical(LogicalExpression le) throws IntolerableException {
		throw new UserRuntimeError(
				"Cannot set an arithmetic variable to the value of a boolean expression.\n"+
				"This happens to the variable "+name+" declared on line "+line+", column "+column+" in the file "+filename);
    }

	public void reinitializeArithmetic(ArithmeticEvaluable _ae) throws IntolerableException {
	    ae = _ae;
	    Initiator itor = sd.getInitiatorFromVariable(var);
	    if(itor == null) {
			throw new InternalError(
					"Cannot find the initiator.\n"+
					"This happens to the variable "+name+" declared on line "+line+", column "+column+" in the file "+filename);
	    }
	    if(!(itor instanceof ArithmeticInitiator)) {
			throw new InternalError(
					"A double variable is not initiated with an arithmetic initiator.\n"+
					"This happens to the variable "+name+" declared on line "+line+", column "+column+" in the file "+filename);
	    }
	    ArithmeticInitiator aitor = (ArithmeticInitiator) itor;
	    aitor.setInitatingExpression(ae);
    }

	public void reinitializeFromInputStream(InputStream is) throws IntolerableException {
		throw new InternalError(
				"Cannot set double to the value of input stream.\n"+
				"This happens to the double "+name+" declared on line "+line+", column "+column+" in the file "+filename);
    }


}