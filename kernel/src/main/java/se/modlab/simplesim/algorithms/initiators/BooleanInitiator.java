package se.modlab.simplesim.algorithms.initiators;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.values.*;

public class BooleanInitiator implements Initiator
{

  private BooleanVariable sbv;
  private Scope s;
  private LogicalExpression le;
  private String fakedName;

  public BooleanInitiator(BooleanVariable _sbv, 
                          Scope _s,
                          LogicalExpression _le)
  {
    sbv = _sbv;
    s = _s;
    le = _le;
    fakedName = _sbv.getName();
  }

  public BooleanInitiator(BooleanVariable _sbv, 
                          Scope _s,
                          LogicalExpression _le,
                          String _fakedName)
  {
    sbv = _sbv;
    s = _s;
    le = _le;
    fakedName = _fakedName;
  }
  
  public void setInitatingExpression(LogicalExpression _le) {
	  le = _le;
  }


  public String getName()
  {
    return sbv.getName();
  }

  public VariableInstance getInstance()
  {
    return sbv;
  }

  public void runInitiation() throws IntolerableException
  {
    if(le == null)
    { 
      sbv.setInitialValue(new sBoolean(true));
      return;
    }
    boolean b = le.evaluate(s);
    sbv.setInitialValue(new sBoolean(b));
  }


  public String toString()
  {
    return "Boolean initiator for variable "+sbv;
  }

}

