package se.modlab.simplesim.algorithms.samplers;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;

public class ArithmeticEndValueSampler implements EndValueSampler
{

  private Variable var;
  private Scope s;
  private ArithmeticEvaluable ae;
  private String fakedName;
  private double sum;
  private double maximum;
  private double minimum;
  private int samples;

  public ArithmeticEndValueSampler(Variable var)
  {
    this.var = var;
    fakedName = var.getName();
  }

  public ArithmeticEndValueSampler(Variable var,
                                   String fakedName)
  {
    this.var = var;
    this.fakedName = fakedName;
  }

  public String getName()
  {
    return var.getName();
  }

  public VariableInstance getInstance()
  {
    return var;
  }

  public void runSampling() throws IntolerableException
  {
    double current_value = var.getValue().getValue();
    sum += current_value;
    if(samples == 0)
    {
      maximum = current_value;
      minimum = current_value;
    }
    else
    {
      maximum = Math.max(maximum, current_value);
      minimum = Math.min(minimum, current_value);
    }
    samples++;
  }

  public void runSetAverageValue() throws IntolerableException
  {
    if(samples == 0)
    {
      throw new InternalError(
        "Zero samples in class SimArithmeticVariableInitiator");
    }
    if(var instanceof DoubleVariable)
    {
      var.setInitialValue(new sDouble(sum / samples));
    }
    if(var instanceof LongVariable)
    {
      var.setInitialValue(new sLong(Math.round(sum / samples)));
    }
    sum = 0;
    samples = 0;
  }

  public String toString()
  {
    try 
    {
      return var.getScopesName()+"/"+fakedName+
             " value "+var.getValue()+
             " Average = "+(sum/samples)+", maximum value = "+maximum+
             ", minimum value "+minimum;
    }
    catch(IntolerableException ie)
    {
      return var.getScopesName()+"/"+fakedName+
             " "+
             " Average = "+(sum/samples)+", maximum value = "+maximum+
             ", minimum value "+minimum;
    }
  }

}

