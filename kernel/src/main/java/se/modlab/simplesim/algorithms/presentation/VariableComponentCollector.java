package se.modlab.simplesim.algorithms.presentation;

import java.awt.Component;
import java.util.Hashtable;
import java.util.Vector;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.algorithms.externalpainters.PlugInPainter;
import se.modlab.simplesim.algorithms.externalpainters.RightExplorerSideComponent;

public class VariableComponentCollector extends VariableNodeVisitor
{

	private Vector<MonitoredSimVariable> monitoredSimVariables = new Vector<MonitoredSimVariable>();
	private Hashtable<String, Vector<OneExternallyPaintedVariable>> otherPainters = new Hashtable<String, Vector<OneExternallyPaintedVariable>>();

	public void visit(IntermediateTreeNode itn)
	throws IntolerableException
	{
		for(int i = 0 ; i < itn.getChildCount() ; i++)
		{
			VariableNode vn = (VariableNode) itn.getChildAt(i);
			vn.accept(this);
		}
	}

	public void visit(OneMonitoredVariable ov)
	throws IntolerableException
	{
		monitoredSimVariables.addElement((MonitoredSimVariable) ov.getComponent());
	}

	public void visit(OneBooleanVariable ov)
	throws IntolerableException
	{
	}

	public void visit(OneBagVariable ov)
	throws IntolerableException
	{
	}

	public void visit(OneQueuableReference ov)
	throws IntolerableException
	{
	}

	public void visit(OneStructReference ov)
	throws IntolerableException
	{
	}

	public void visit(OneArithmeticVariable ov)
	throws IntolerableException
	{
	}

	public void visit(OneExternallyPaintedVariable oepv)
	throws IntolerableException
	{
		String nameOfExternalPainter = oepv.getPlugInPainter().getPluginsName();
		Vector<OneExternallyPaintedVariable> v = otherPainters.get(nameOfExternalPainter);
		if(v == null) {
			v = new Vector<OneExternallyPaintedVariable>();
			otherPainters.put(nameOfExternalPainter, v);
		}
		v.addElement(oepv);
	}

	public MonitoredSimVariable[] getStandardMonitoredVariablesComponents()
	{
		MonitoredSimVariable mvs[] = new MonitoredSimVariable[monitoredSimVariables.size()];
		for(int i = 0 ; i < monitoredSimVariables.size() ; i++)
		{
			mvs[i] = monitoredSimVariables.elementAt(i);
		}
		return mvs;
	}

	public RightExplorerSideComponent[] getExternallyPaintedMonitoredVariablesComponents(String nameOfExternalPainter)
	{
		Vector<OneExternallyPaintedVariable> v = otherPainters.get(nameOfExternalPainter);
		RightExplorerSideComponent mvs[] = new RightExplorerSideComponent[v.size()];
		for(int i = 0 ; i < v.size() ; i++)
		{
			mvs[i] = (RightExplorerSideComponent) v.elementAt(i).getComponent();
		}
		return mvs;
	}

}