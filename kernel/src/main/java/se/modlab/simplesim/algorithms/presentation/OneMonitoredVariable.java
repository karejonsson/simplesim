package se.modlab.simplesim.algorithms.presentation;

import se.modlab.generics.*;
import se.modlab.generics.sstruct.values.*;
import java.util.Enumeration;
import java.util.Vector;
import javax.swing.tree.TreeNode;
import javax.swing.JTextArea;
import java.awt.Component;

import se.modlab.simplesim.algorithms.externalpainters.PlugInPainterFactory;
import se.modlab.simplesim.variables.*;

public class OneMonitoredVariable extends VariableNode
{
  private MonitoredVariableExtracted mve;
  private SegmentPainter sp;
  private PlugInPainterFactory pips[];

  public OneMonitoredVariable(MonitoredVariableExtracted _mve, 
                              TreeNode _parent, 
                              SegmentPainter _sp,
                              PlugInPainterFactory _pips[])
  {
	 super(_parent);
	 mve = _mve; 
	 sp = _sp;
	 pips = _pips;
  }

  public Component getComponent()
  {
    return new MonitoredSimVariable(mve, sp, pips, parent);
/*
    graphMakerVariableVisitor gmvv = new graphMakerVariableVisitor();
    mve.accept(gmvv);
    return gmvv.getGraph();
 */
  }  

  public String toString()
  {
    return mve.getName();
  }
/*
  private class graphMakerVariableVisitor implements monitoredVariableVisitor
  {

    private monitoredSimVariable msv = null;

    public void visitLong()
    {
      msv = new monitoredSimVariable(OneVariable.this.mve);
    }

    public void visitDouble()
    {
      msv = new monitoredSimVariable(OneVariable.this.mve);
    }

    public void visitBoolean()
    {
      msv = new monitoredSimVariable(OneVariable.this.mve);
    }

    public monitoredSimVariable getGraph()
    {
      return msv;
    }
  }
*/

} 
