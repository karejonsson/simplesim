package se.modlab.simplesim.algorithms.setup;

import java.util.Hashtable;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.evaluables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.simplesim.scoping.*;
import se.modlab.simplesim.events.*;

public class ProcedureRepeatingFactory 
       implements SimVariableFactory, EnqueuableFactory
{

  private SimScope s;
  private String type;
  private Manager localScope[];
  private ArithmeticEvaluable ae_first;
  private ProgramBlock p;
  private LogicalExpression le_requeue;
  private ArithmeticEvaluable ae_again;
  private SimData sd;
  private ScopeFactory sf;
  private boolean isSharp;

  //private static int _ctr = 0;
  //private int ctr = 0;

  public ProcedureRepeatingFactory(
                          SimScope _s,
                          String _type,
                          Manager _localScope[],
                          ArithmeticEvaluable _ae_first,
                          ProgramBlock _p,
                          LogicalExpression _le_requeue,
                          ArithmeticEvaluable _ae_again, 
                          SimData _sd,
                          ScopeFactory _sf,
                          boolean _isSharp)
  {
    s = _s;
    type = _type;
    localScope = _localScope;
    ae_first = _ae_first;
    p = _p;
    le_requeue = _le_requeue;
    ae_again = _ae_again;
    sd = _sd;
    sf = _sf;
    isSharp = _isSharp;
    //ctr = _ctr++;
    //System.out.println("procedureRepeatingFactory<init> type = "+type+" "+
    //                   "isSharp = "+isSharp+" id = "+ctr);
  }
 
  public VariableInstance getInstance(String name, String filename, int line, int column, Hashtable<VariableInstance, Manager> managermap) 
    throws IntolerableException
  {
    return getInstance(name, false, filename, line, column, managermap);
  }

  public VariableInstance getInstance(String name, boolean monitor, String filename, int line, int column, Hashtable<VariableInstance, Manager> managermap) 
    throws IntolerableException
  {
    //System.out.println("procedureRepeatingFactory getInstance type = "+type+" name = "+name+" isSharp = "+isSharp+" id = "+ctr);
    SimScope ls = (SimScope) sf.getInstance(s, s.getName()+"/"+name);
    //System.out.println("Repeating factory "+type);
    for(int i = 0 ; i < localScope.length ; i++)
    {
      localScope[i].initialize(ls, isSharp, managermap);
      //System.out.println("procedureRepeatingFactory \n"+
      //  localScope[i]+", isSharp = "+isSharp);
    }
    ProcedureRepeating pr = 
      new ProcedureRepeating(name, this, filename, line, column, ls, ae_first, ae_again, le_requeue,
                             p, sd.getCommons(), sf);
    return pr;
  }

  public String getTypesName()
  {
    return type;
  }
 
  public String toString()
  {
    return "Procedure repeating factory for type "+type+" in scope "+s.getName();
  }
  
	public VariableInstance getInstance(String name, String filename, int line, int column)
	throws IntolerableException {
		//System.out.println("Call to getInstance(s,s,i,i) i.e. addin null in class "+getClass().getName());
		return getInstance(name, filename, line, column, null);
	}
	public VariableInstance getInstance(String name, boolean monitor, String filename, int line, int column)
	throws IntolerableException {
		//System.out.println("Call to getInstance(s,b,s,i,i) i.e. addin null in class "+getClass().getName());
		return getInstance(name, monitor, filename, line, column, null);
	}


}

