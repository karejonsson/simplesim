package se.modlab.simplesim.algorithms.illustrate.onimage;

import se.modlab.simplesim.algorithms.illustrate.share.*;
import se.modlab.generics.files.*;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.evaluables.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.strings.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.util.Services;
import se.modlab.generics.bshro.ifc.HierarchyObject;

import java.awt.*;
import java.io.*;
import java.util.*;
import javax.swing.*;
import se.modlab.simplesim.algorithms.illustrate.struct.*;
import se.modlab.simplesim.events.*;

public class DynamicIllustrationUnit implements IllustrationUnitFactoried {
 
  //private String typesName = null;
  private VariableInstance vi;
  private Enqueuable eq = null; 
  //private scopeGetter sg;
  private ScopeGivingProgram calc;
  private LogicalExpression visi;
  private StringEvaluable iconfile;
  private String iconFileCalculated;
  private StringEvaluable background;
  private ArithmeticEvaluable hori;
  private ArithmeticEvaluable vert;
  private StringEvaluable text;
  private Scope localScope = null;

  private Hashtable<String, ImageIcon> icon = new Hashtable<String, ImageIcon>();
  private static Hashtable<String, Color> colors = new Hashtable<String, Color>();

  private int width = -1;
  private int height = -1;

  private LongVariable w;
  private LongVariable h;

  static
  {
    colors.put("black", Color.black);
    colors.put("BLACK", Color.BLACK);
    colors.put("blue", Color.blue);
    colors.put("BLUE", Color.BLUE);
    colors.put("cyan", Color.cyan);
    colors.put("CYAN", Color.CYAN);
    colors.put("DARK_GRAY", Color.DARK_GRAY);
    colors.put("darkGray", Color.darkGray);
    colors.put("gray", Color.gray);
    colors.put("GRAY", Color.GRAY);
    colors.put("green", Color.green);
    colors.put("GREEN", Color.GREEN);
    colors.put("LIGHT_GRAY", Color.LIGHT_GRAY);
    colors.put("lightGray", Color.lightGray);
    colors.put("magenta", Color.magenta);
    colors.put("MAGENTA", Color.MAGENTA);
    colors.put("orange", Color.orange);
    colors.put("pink", Color.PINK);
    colors.put("red", Color.RED);
    colors.put("white", Color.white);
    colors.put("yellow", Color.yellow);
    colors.put("YELLOW", Color.YELLOW);   
  }

  private static Color getColor(String clr) {
    return (Color) colors.get(clr);
  }

  public DynamicIllustrationUnit(
                    VariableInstance _vi,// _typesName,
//                    scopeGetter _sg,
                    ScopeGivingProgram _calc,
                    LogicalExpression _visi,
                    StringEvaluable _iconfile,
                    StringEvaluable _background,
                    ArithmeticEvaluable _hori,
                    ArithmeticEvaluable _vert,
                    StringEvaluable _text)
    throws IntolerableException
  {
    //typesName = _typesName;
    //sg = _sg;
    vi = _vi;
    calc = _calc;
    visi = _visi;
    iconfile = _iconfile;
    background = _background;
    hori = _hori;
    vert = _vert;
    text = _text;
    if(!(vi instanceof Enqueuable))
    {
      throw new UserCompiletimeError("The instance referred as \n"+
                          vi.getName()+" in the .ilu file is not associated with a\n"+
                          "local scope.");
    }
    eq = (Enqueuable) vi;
    localScope = new StaticScope(eq.getLocalScope());
  }

  public void init(int _width, int _height) throws IntolerableException {
    width = _width;
    height = _height;
    //System.out.println("W = "+width+", H = "+height);
    setGlobalScope();
  }

  public boolean isEnqueued() throws IntolerableException {
    return eq.isEnqueued(); 
  }

  private void setGlobalScope() throws IntolerableException {
    VariableType lt = localScope.getFactory("long");

    w = new LongVariable("imagewidth", lt, "", -1, -1);
    w.setValue(new sLong(width));
    localScope.addVariable(new LongConst(w));

    h = new LongVariable("imageheight", lt, "", -1, -1);
    h.setValue(new sLong(height));
    localScope.addVariable(new LongConst(h));
    //System.out.println("labelIllustrationUnit - setGlobalScope 3 - cosinus = "+(localScope.getVariable("cosinus") == null));
    //System.out.println("labelIllustrationUnit - setGlobalScope 3 - imagewidth = "+(localScope.getVariable("imagewidth") == null));
    w = new LongVariable("iconwidth", lt, "", -1, -1);
    w.setValue(new sLong(0));
    localScope.addVariable(new LongConst(w));
    h = new LongVariable("iconheight", lt, "", -1, -1);
    h.setValue(new sLong(0));
    localScope.addVariable(new LongConst(h));
  }

  public String getIconFilenameCalculated() {
    return iconFileCalculated;
  }

  public String getName() {
    return vi.getType().getTypesName();
  }
 
  public void paint(Graphics g) throws IntolerableException {
    //System.out.println("dynamicIllustrationUnit.paint w="+width+", h="+height);
    if(localScope == null) return;
   Scope tmp = null;
    try {
      tmp = calc.executeGiveScope(localScope);
    }
    catch(ReturnException re) {
      tmp = localScope;
    }
    catch(StopException se) {
      throw new UserRuntimeError("Stop in illustration", se);
    }
    catch(BreakException be) {
      throw new InternalProgrammingError("Unexpected break exception in illustration", be);
    }
    catch(ContinueException ce) {
      throw new InternalProgrammingError("Unexpected continue exception in illustration", ce);
    }
 
    boolean visible = visi.evaluate(tmp); // 184
    if(!visible) return;
    
    String iconfilename = iconfile.evaluate(tmp);
    ImageIcon iicon = icon.get(iconfilename);
    if(iicon == null) {
      iconFileCalculated = 
    		  HierarchyObject.getReferenceFilePath()+
        iconfile.evaluate(tmp);
      byte image[] = Services.getCleartextFromFile(iconFileCalculated);
      if(image == null) { 
        throw new UserCompiletimeError(
          "The file "+iconFileCalculated+" was expected to contain\n"+
          "an icon but did not exist!"); 
      }
      iicon = new ImageIcon(image);
      w.setValue(new sLong(iicon.getIconWidth()));
      h.setValue(new sLong(iicon.getIconHeight()));
      icon.put(iconfilename, iicon);
    }

    String backcolor = background.evaluate(tmp);
    sValue _xpos = hori.evaluate(tmp);
    int xpos = (int) _xpos.getValue();
    sValue _ypos = vert.evaluate(tmp);
    int ypos = (int) _ypos.getValue();
    String textual = text.evaluate(tmp);
    Color col = getColor(backcolor);
    g.drawImage(iicon.getImage(), xpos, ypos, col, null);

    FontMetrics fm = g.getFontMetrics();
    int theight = fm.getHeight();
    byte bytes[] = textual.getBytes();
    int twidth = fm.bytesWidth(bytes, 0, bytes.length);
    int down = (int)(fm.getHeight() + iicon.getIconHeight())/2;
    int typos = ypos+down;
    int txpos = xpos+iicon.getIconWidth();
    if((txpos + twidth > width) && (xpos - twidth > 0)) {
      g.drawString(textual, xpos - twidth, typos);
      //System.out.println("labelIllustrationUnit "+textual);
      return;
    }
    g.drawString(textual, txpos, typos);
    //System.out.println("labelIllustrationUnit "+textual);
  }

}
