package se.modlab.simplesim.algorithms.externalpainters;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;

import org.jrobin.core.RrdDb;
import org.jrobin.core.RrdDef;
import org.jrobin.core.RrdException;
import org.jrobin.core.RrdMemoryBackendFactory;
import org.jrobin.core.Sample;
import org.jrobin.core.Util;
import org.jrobin.graph.RrdGraph;
import org.jrobin.graph.RrdGraphDef;
import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.exceptions.MotherCatalogue;
import se.modlab.generics.exceptions.SystemError;
import se.modlab.generics.exceptions.UnclassedError;
import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.jpegcoding.JpegEncoder;
import se.modlab.generics.sstruct.values.sValue;
import se.modlab.generics.util.HexVisualizer;
import se.modlab.generics.util.Md5Helper;
import se.modlab.generics.bshro.ifc.HierarchyObject;

import se.modlab.simplesim.algorithms.presentation.MonitoredSimVariable;
import se.modlab.simplesim.algorithms.regression.Segment;
import se.modlab.simplesim.variables.MonitoredVariableExtracted;

public class JRobinPlugInPainter implements PlugInPainter {

	public static final String s_RRD_PATH = "random";
	public String RRD_PATH;
	public static final float SCREEN_WIDTH = (int)java.awt.Toolkit.getDefaultToolkit().getScreenSize().width;
	public static final float SCREEN_HEIGHT = (int)java.awt.Toolkit.getDefaultToolkit().getScreenSize().height;

	static {
		try {
			RrdDb.setDefaultFactory("MEMORY");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static int pathctr = 0;

	private MonitoredVariableExtracted mve = null;
	private RrdDef rrdDef = null;
	private RrdDb rrdDb = null;
	private RrdGraphDef gDef = null;
	private String exceptionMessage = null;
	private String name = null;
	private long start;
	private long end;
	private int period; 
	private int rasters = (int) (SCREEN_WIDTH / 3); // To be users choice.
	private double simulationTimeunitInSeconds;
	private String errorMessage[] = null;
	private boolean iterated;

	private String ds_average = null;
	private String ds_m1stddev = null;
	private String ds_p1stddev = null;
	private String ds_maximum = null;
	private String ds_minimum = null;

	private boolean hasRangeSetting = false;
	private double rangeLowerValue = Double.MIN_VALUE;
	private double rangeUpperValue = Double.MAX_VALUE;
	private Component comp = null;

	public JRobinPlugInPainter(MonitoredVariableExtracted _mve, boolean _iterated) {
		mve = _mve;
		name = mve.getName();
		iterated = _iterated;
		RRD_PATH = s_RRD_PATH+(pathctr++);

		Date d = new Date();
		String pieces[] = mve.getStartTimeTextual().split("[/ :]"); 

		d.setYear(Integer.parseInt(pieces[0]));
		d.setMonth(Integer.parseInt(pieces[1]));
		d.setDate(Integer.parseInt(pieces[2]));
		d.setHours(Integer.parseInt(pieces[3]));
		d.setMinutes(Integer.parseInt(pieces[4]));
		d.setSeconds(Integer.parseInt(pieces[5]));
		start = d.getTime() / 1000;
		//System.out.println("JRobinPlugInPainter - ctor. Start time = "+mve.getStartTimeTextual()+" = "+start+", Date = "+d);

		pieces = mve.getEndTimeTextual().split("[/ :]");
		d = new Date();
		d.setYear(Integer.parseInt(pieces[0]));
		d.setMonth(Integer.parseInt(pieces[1]));
		d.setDate(Integer.parseInt(pieces[2]));
		d.setHours(Integer.parseInt(pieces[3]));
		d.setMinutes(Integer.parseInt(pieces[4]));
		d.setSeconds(Integer.parseInt(pieces[5]));		
		end = d.getTime() / 1000;
		//System.out.println("JRobinPlugInPainter - ctor. End time = "+mve.getEndTimeTextual()+" = "+end+", Date = "+d);

		period = (int) (end - start) / rasters;

		if(period < 20) {
			errorMessage = new String[] {
					"Time span "+(end - start)+" seconds is to short for JRobin as it is applied in Simplesim.",
			"This might be improved in a future version of Simplesim."};
		}
		period = Math.max(300, period);
		//double 
		simulationTimeunitInSeconds = Math.max(1, (end - start) / mve.getEndTime());
		//System.out.println("JRobinPlugInPainter: simulationTimeunitInSeconds = "+simulationTimeunitInSeconds+
		//		", period = "+period+
		//		", rasters = "+rasters);
		//rrdPath = Util.getJRobinDemoPath(name + ".rrd");

		//timeUnit = mve.
	}

	public String getPluginsName() {
		return "JRobin";
	}

	public String getVariableName() {
		return name;
	}


	//private static final String leadingtext = null;//"Where will I see you";
	/**
	 * Lazy init
	 *
	 */
	private void init() throws RrdException, IOException, IntolerableException {

		double min = mve.getMinValue();
		double max = mve.getMaxValue();
		if (min == max) {
			errorMessage = new String[] {
					"Both minimum and maximum are "+min+".",
			"JRobin cannot make a graph."};
			//System.out.println("MIN == max");
			return;
			//exceptionMessage = "Both minimum and maximum are "+min;
		}

		rrdDef = new RrdDef(RRD_PATH, start-1, period);
		//double min = mve.getMinValue();
		//double max = mve.getMaxValue();
		//int steps = (int) Math.max(1, (24 * 3600) / (periodicitySeconds * grid));
		int steps = (int) Math.max(1, (end - start) / (period * rasters));
		//System.out.println("JRobinPlugInPainter - period = "+period+", min = "+min+", max = "+max+", steps = "+steps);
		if(iterated) {
			ds_average = HexVisualizer.bytesToString(Md5Helper.calc(name+" average")).substring(0, 19);
			ds_m1stddev = HexVisualizer.bytesToString(Md5Helper.calc(name+" -1 stddev")).substring(0, 19);
			ds_p1stddev = HexVisualizer.bytesToString(Md5Helper.calc(name+" +1 stddev")).substring(0, 19);
			ds_minimum = HexVisualizer.bytesToString(Md5Helper.calc(name+" minimum")).substring(0, 19);
			ds_maximum = HexVisualizer.bytesToString(Md5Helper.calc(name+" maximum")).substring(0, 19);
			rrdDef.addDatasource(ds_average, "GAUGE", 2*period, min, max);			
			rrdDef.addDatasource(ds_minimum, "GAUGE", 2*period, min, max);			
			rrdDef.addDatasource(ds_maximum, "GAUGE", 2*period, min, max);			
			rrdDef.addDatasource(ds_m1stddev, "GAUGE", 2*period, min, max);			
			rrdDef.addDatasource(ds_p1stddev, "GAUGE", 2*period, min, max);			
		}
		else {
			ds_average = HexVisualizer.bytesToString(Md5Helper.calc(name+" average")).substring(0, 19);
			rrdDef.addDatasource(ds_average, "GAUGE", 2*period, min, max);			
		}
		rrdDef.addArchive("AVERAGE", 0.5, steps, 2*period);
		rrdDb = new RrdDb(rrdDef);//, new RrdMemoryBackendFactory());
		int currentRaster = 0;
		int positionInVariablesArray = 0;
		double previousValue = 0;
		double previousValueAverage = 0;
		double previousValue_minusSTDDEV = 0;
		double previousValue_plusSTDDEV = 0;
		double previousValue_MAX = 0;
		double previousValue_MIN = 0;
		long epochSecondOfCurrentSample = start;
		long lastEpochSecondForPreviousRaster = 0;
		while(currentRaster <= rasters) {
			double doneFraction = (double)currentRaster / rasters;
			long doneTime = (long) (doneFraction * ((double) ((end - start))));
			long lastEpochSecondForThisRaster = start + doneTime; 
			/*
			System.out.println(
					"LOOP 1: currentRaster="+currentRaster+
					", lastEpochSecondForThisRaster="+lastEpochSecondForThisRaster+
					", rasters="+rasters+
					", doneFraction="+doneFraction+
					", doneTime="+doneTime); */
			if(lastEpochSecondForPreviousRaster != lastEpochSecondForThisRaster) {
				if(iterated) {
					int samples = 0;
					double sampleSumAverage = 0;
					double sampleSum_minusSTDDEV = 0;
					double sampleSum_plusSTDDEV = 0;
					double sampleMAX = Double.NEGATIVE_INFINITY;// MIN_VALUE;
					double sampleMIN = Double.POSITIVE_INFINITY;//MAX_VALUE;
					/*
    Segment s = (Segment) dp.mve.getGeneralizedValue(index);
    double val_1 = s.getAverage();
    double dev_1 = s.getDeviation();
    double max_1 = s.getMaximum();
    double min_1 = s.getMinimum();
    double time_1 = dp.mve.getValue(index).getTime();

					 */
					while((epochSecondOfCurrentSample <= lastEpochSecondForThisRaster) && 
							(positionInVariablesArray < mve.getSize())) {
						//System.out.println("LOOP 2: positionInVariablesArray="+positionInVariablesArray+", mve.getSize()="+mve.getSize());
						Segment s = (Segment) mve.getGeneralizedValue(positionInVariablesArray);
						double average = s.getAverage();
						double deviation = s.getDeviation();
						double maximum = s.getMaximum();
						double minimum = s.getMinimum();
						double time = mve.getValue(positionInVariablesArray).getTime();
						//sValue val = mve.getValue(positionInVariablesArray);
						epochSecondOfCurrentSample = start + ((long) (time * simulationTimeunitInSeconds));
						if(epochSecondOfCurrentSample > lastEpochSecondForThisRaster) {
							// Beyond the timegap of this raster
							break;
						}
						positionInVariablesArray++;
						sampleSumAverage += average;
						samples++;
						sampleSum_minusSTDDEV += (average - deviation);
						sampleSum_plusSTDDEV += (average + deviation);
						sampleMAX = Math.max(sampleMAX, maximum);
						sampleMIN = Math.min(sampleMIN, minimum);
					}
					Sample sample = rrdDb.createSample();
					sample.setTime(lastEpochSecondForThisRaster);
					if(samples != 0) {
						previousValueAverage = sampleSumAverage / samples;
						previousValue_minusSTDDEV = sampleSum_minusSTDDEV / samples;
						previousValue_plusSTDDEV = sampleSum_plusSTDDEV / samples;
						previousValue_MIN = sampleMIN;
						previousValue_MAX = sampleMAX;
						//System.out.println("Samples != 0, previousValue_MAX="+previousValue_MAX);
					}
					else {
						//System.out.println("Samples == 0, previousValue_MAX="+previousValue_MAX);
					}
					sample.setValue(ds_average, previousValueAverage);	
					sample.setValue(ds_m1stddev, previousValue_minusSTDDEV);	
					sample.setValue(ds_p1stddev, previousValue_plusSTDDEV);	
					sample.setValue(ds_minimum, previousValue_MIN);	
					sample.setValue(ds_maximum, previousValue_MAX);	
					//System.out.println("MAX "+previousValue_MAX);
					sample.update();
				}
				else {
					double sampleSum = 0;
					int samples = 0;
					while((epochSecondOfCurrentSample <= lastEpochSecondForThisRaster) && 
							(positionInVariablesArray < mve.getSize())) {
						//System.out.println("LOOP 2: positionInVariablesArray="+positionInVariablesArray+", mve.getSize()="+mve.getSize());
						sValue val = mve.getValue(positionInVariablesArray);
						epochSecondOfCurrentSample = start + ((long) (val.getTime() * simulationTimeunitInSeconds));
						if(epochSecondOfCurrentSample > lastEpochSecondForThisRaster) {
							// Beyond the timegap of this raster
							break;
						}
						positionInVariablesArray++;
						sampleSum += val.getValue();
						samples++;
					}
					Sample sample = rrdDb.createSample();
					sample.setTime(lastEpochSecondForThisRaster);
					if(samples != 0) {
						previousValue = sampleSum / samples;
						//System.out.println("YES, currentRaster="+currentRaster+", samples = "+samples+
						//		", lastEpochSecondForThisRaster="+lastEpochSecondForThisRaster+
						//		", previousValue = "+previousValue);					
					}
					else {
						//System.out.println("NO, currentRaster="+currentRaster+", samples = "+samples+
						//		", lastEpochSecondForThisRaster="+lastEpochSecondForThisRaster+
						//		", previousValue = "+previousValue);					
					}
					sample.setValue(ds_average, previousValue);	
					sample.update();
				}
			}
			currentRaster++;
			lastEpochSecondForPreviousRaster = lastEpochSecondForThisRaster;
		}
	}

	private void doPaint(Component comp, Graphics g) throws RrdException, IOException, IntolerableException {
		if(errorMessage != null) {
			for(int i = 0 ; i < errorMessage.length ; i++) {
				g.drawString(errorMessage[i], 25 , 25+20*i);
			}
			return;
		}
		if(exceptionMessage != null) {
			// Initiations failed
			g.drawString(exceptionMessage, 25 , 25);
			return;
		}
		this.comp = comp;
		if(rrdDef == null) {
			init();
		}
		/*
 		long t = start;
        rrdDef = new RrdDef(RRD_PATH, startTime - 1, 300);
        rrdDef.addDatasource("a", "GAUGE", 600, Double.NaN, Double.NaN);
        rrdDef.addArchive("AVERAGE", 0.5, 1, 300);
        rrdDef.addArchive("MIN", 0.5, 12, 300);
        rrdDef.addArchive("MAX", 0.5, 12, 300);
        rrdGraphDef = new RrdGraphDef(startTime, startTime + 86400);
        rrdGraphDef.setTitle("JRobin MIN/MAX demo");
        rrdGraphDef.setLowerLimit(0);
        rrdGraphDef.datasource("a", RRD_PATH, "a", "AVERAGE");
        rrdGraphDef.datasource("b", RRD_PATH, "a", "MIN");
        rrdGraphDef.datasource("c", RRD_PATH, "a", "MAX");
        rrdGraphDef.area("a", new Color(0, 0xb6, 0xe4), "real");
        rrdGraphDef.line("b", new Color(0, 0x22, 0xe9), "min", 2);
        rrdGraphDef.line("c", new Color(0, 0xee, 0x22), "max", 2);
		 */
		gDef = new RrdGraphDef();
		if(hasRangeSetting) {
			gDef.setGridRange(rangeLowerValue, rangeUpperValue, true);
		}

		//gDef.setFilename(imgPath);
		gDef.setTimePeriod(start-1, end);
		if(comp.getSize().width > 600) {
			gDef.setTitle("Variable "+name);
			//gDef.setTitle("Variable "+name+" from "+mve.getStartTimeTextual()+" until "+mve.getEndTimeTextual());
			gDef.setVerticalLabel("Simplesim calculated");
		} 
		else {
			gDef.setTitle("Variable "+name);
			gDef.setVerticalLabel("Simplesim calculated");
		}
		gDef.setVerticalLabel("Created with Simplesim, written by K�re Jonsson");
		//final String formatter = "     @7.1"; // From monitor viewer

		final String formatter = "@.3";
		//final String formatter = "%.3f%S";
		if(iterated) {
			// Average
			gDef.datasource(ds_average, RRD_PATH, ds_average, "AVERAGE");
			gDef.line(ds_average, Color.GREEN, name+" average");
			gDef.gprint(ds_average, "MIN", "MIN "+formatter); //      @7.1
			gDef.gprint(ds_average, "AVERAGE", "AVERAGE "+formatter);
			gDef.gprint(ds_average, "MAX", "MAX "+formatter);
			gDef.gprint(ds_average, "LAST", "LAST "+formatter+"\n");
			// minimum
			gDef.datasource(ds_minimum, RRD_PATH, ds_minimum, "AVERAGE");
			gDef.line(ds_minimum, Color.BLUE, name+" minimum");
			gDef.gprint(ds_minimum, "MIN", "MIN "+formatter);
			gDef.gprint(ds_minimum, "AVERAGE", "AVERAGE "+formatter);
			gDef.gprint(ds_minimum, "MAX", "MAX "+formatter);
			gDef.gprint(ds_minimum, "LAST", "LAST "+formatter+"\n");
			// maximum
			gDef.datasource(ds_maximum, RRD_PATH, ds_maximum, "AVERAGE");
			gDef.line(ds_maximum, Color.CYAN, name+" maximum");
			gDef.gprint(ds_maximum, "MIN", "MIN "+formatter);
			gDef.gprint(ds_maximum, "AVERAGE", "AVERAGE "+formatter);
			gDef.gprint(ds_maximum, "MAX", "MAX "+formatter);
			gDef.gprint(ds_maximum, "LAST", "LAST "+formatter+"\n");
			// -1 stddev
			gDef.datasource(ds_m1stddev, RRD_PATH, ds_m1stddev, "AVERAGE");
			gDef.line(ds_m1stddev, Color.MAGENTA, name+" -1 stddev");
			gDef.gprint(ds_m1stddev, "MIN", "MIN "+formatter);
			gDef.gprint(ds_m1stddev, "AVERAGE", "AVERAGE "+formatter);
			gDef.gprint(ds_m1stddev, "MAX", "MAX "+formatter);
			gDef.gprint(ds_m1stddev, "LAST", "LAST "+formatter+"\n");
			// +1 stddev
			gDef.datasource(ds_p1stddev, RRD_PATH, ds_p1stddev, "AVERAGE");
			gDef.line(ds_p1stddev, Color.ORANGE, name+" +1 stddev");
			gDef.gprint(ds_p1stddev, "MIN", "MIN "+formatter);
			gDef.gprint(ds_p1stddev, "AVERAGE", "AVERAGE "+formatter);
			gDef.gprint(ds_p1stddev, "MAX", "MAX "+formatter);
			gDef.gprint(ds_p1stddev, "LAST", "LAST "+formatter+"\n");
		}
		else {
			gDef.datasource(ds_average, RRD_PATH, ds_average, "AVERAGE");
			gDef.line(ds_average, Color.GREEN, name);
			gDef.gprint(ds_average, "MIN", "MIN "+formatter);
			gDef.gprint(ds_average, "AVERAGE", "AVERAGE "+formatter);
			gDef.gprint(ds_average, "MAX", "MAX "+formatter);
			gDef.gprint(ds_average, "LAST", "LAST "+formatter+"\n");
		}

		//gDef.setPoolUsed(false);
		//gDef.setImageFormat("jpg");
		RrdGraph graph = new RrdGraph(gDef);
		//gDef.setWidth(comp.getSize().width-90); // Somewhat magic. Hope to improve soon (20070531)
		//gDef.setHeight(comp.getSize().height-((iterated)? 125: 85));
		graph.renderImage((Graphics2D) g, comp.getSize().width-90, comp.getSize().height-((iterated)? 125: 85));
	}

	public void paint(Component comp, Graphics g) {
		try {
			doPaint(comp, g);
		}
		catch(RrdException rrde) {
			rrde.printStackTrace();
			exceptionMessage = rrde.getMessage();
			g.drawString("RRD exception: "+rrde.getMessage(), 25 , 25);
		}
		catch(IOException ioe) {
			ioe.printStackTrace();
			exceptionMessage = ioe.getMessage();
			g.drawString("IO exception: "+ioe.getMessage(), 25 , 25);
		}
		catch(NullPointerException npe) {
			npe.printStackTrace();
			exceptionMessage = npe.getMessage();
			g.drawString("Null pointer exception: "+npe.getMessage(), 25 , 25);
		}
		catch(IntolerableException ie) {
			ie.printStackTrace();
			exceptionMessage = ie.getMessage();
			g.drawString("Intolerable Simplesim exception: "+ie.getMessage(), 25 , 25);
		}
	}

	protected void createJPG(Component comp)
	{
		try
		{
			JFileChooser fc = new JFileChooser(HierarchyObject.getReferenceFilePath());
			fc.setDialogTitle("Point to the file. (With extension .jpg)");
			fc.setDialogType(JFileChooser.SAVE_DIALOG);
			fc.showSaveDialog(UniversalTellUser.getFrame(comp));
			File selFile = fc.getSelectedFile();
			if(selFile == null) return;
			String path = selFile.getAbsolutePath();
			if(path == null) return;
			if(!(path.endsWith(".jpg")))
			{
				JOptionPane.showMessageDialog(
						comp, 
						"The filename must end with .jpg", 
						"File not valid", 
						JOptionPane.ERROR_MESSAGE);
				return;
			}
			Dimension d = comp.getSize();
			BufferedImage image = new BufferedImage(d.width, 
					d.height, 
					BufferedImage.TYPE_USHORT_GRAY);
			Graphics2D graphics = image.createGraphics();
			graphics.setPaint(Color.BLACK);
			graphics.setBackground(Color.WHITE);
			graphics.clearRect(0, 0, d.width, d.height);
			paint(comp, graphics);
			FileOutputStream fos = new FileOutputStream(selFile);
			JpegEncoder jpg = new JpegEncoder(image, 50, fos);
			jpg.Compress();
			fos.close();
		}
		catch(FileNotFoundException fnf)
		{
			UniversalTellUser.general(UniversalTellUser.getFrame(comp), new SystemError("File not found", fnf));
		}
		catch(IOException ioe)
		{
			UniversalTellUser.general(UniversalTellUser.getFrame(comp), new SystemError("I/O error", ioe));
		}
		catch(Exception e)
		{
			UniversalTellUser.general(UniversalTellUser.getFrame(comp), 
					new UnclassedError("monitoredSimVariable - jpg-generation", e));
		}
	}
	//saveToFile

	protected String saveToFile(String message, Component comp)
	{
		try
		{
			JFileChooser fc = new JFileChooser(HierarchyObject.getReferenceFilePath());
			fc.setDialogTitle(message);
			fc.setDialogType(JFileChooser.SAVE_DIALOG);
			fc.showSaveDialog(UniversalTellUser.getFrame(comp));
			File selFile = fc.getSelectedFile();
			if(selFile == null) return null;
			String path = selFile.getAbsolutePath();
			if(path == null) return null;
			if(!(path.endsWith(".rrd")))
			{
				JOptionPane.showMessageDialog(
						comp, 
						"The filename must end with .rrd", 
						"File not valid", 
						JOptionPane.ERROR_MESSAGE);
				return null;
			}
			FileOutputStream fos = new FileOutputStream(selFile);
			fos.write(rrdDb.getBytes());
			fos.close();
			return selFile.getCanonicalPath();
		}
		catch(FileNotFoundException fnf)
		{
			UniversalTellUser.general(UniversalTellUser.getFrame(comp), new SystemError("File not found", fnf));
		}
		catch(IOException ioe)
		{
			UniversalTellUser.general(UniversalTellUser.getFrame(comp), new SystemError("I/O error", ioe));
		}
		catch(Exception e)
		{
			UniversalTellUser.general(UniversalTellUser.getFrame(comp), 
					new UnclassedError("monitoredSimVariable - jpg-generation", e));
		}
		return null;
	}

	protected void openInspector(Component comp)
	{
		try
		{
			JFileChooser fc = new JFileChooser(HierarchyObject.getReferenceFilePath());
			fc.setDialogTitle("First save JRobin .rrd-file");
			fc.setDialogType(JFileChooser.SAVE_DIALOG);
			fc.showSaveDialog(UniversalTellUser.getFrame(comp));
			File selFile = fc.getSelectedFile();
			if(selFile == null) return;
			String path = selFile.getAbsolutePath();
			if(path == null) return;
			if(!(path.endsWith(".rrd")))
			{
				JOptionPane.showMessageDialog(
						comp, 
						"The filename must end with .rrd", 
						"File not valid", 
						JOptionPane.ERROR_MESSAGE);
				return;
			}
			FileOutputStream fos = new FileOutputStream(selFile);
			fos.write(rrdDb.dump().getBytes());
			fos.close();
		}
		catch(FileNotFoundException fnf)
		{
			UniversalTellUser.general(UniversalTellUser.getFrame(comp), new SystemError("File not found", fnf));
		}
		catch(IOException ioe)
		{
			UniversalTellUser.general(UniversalTellUser.getFrame(comp), new SystemError("I/O error", ioe));
		}
		catch(Exception e)
		{
			UniversalTellUser.general(UniversalTellUser.getFrame(comp), 
					new UnclassedError("monitoredSimVariable - jpg-generation", e));
		}
	}


	public JPopupMenu getPopup(Component _comp) {
		final Component comp = _comp;
		JPopupMenu popup = new JPopupMenu();
		JMenuItem menuItem = new JMenuItem("Write .jpg file");
		menuItem.addActionListener(
				new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						createJPG(comp);
					}
				});
		popup.add(menuItem);

		menuItem = new JMenuItem("Set ranges lower value");
		menuItem.addActionListener(
				new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						double keepLower = rangeLowerValue;
						double keepUpper = rangeUpperValue;
						boolean keepHas = hasRangeSetting;
						try {
							String value = JOptionPane.showInputDialog(comp,
									"Type chosen lower range value",
									"Lower value choice",
									JOptionPane.OK_CANCEL_OPTION);


							if(value != null) {
								Double newValue = Double.parseDouble(value);
								if(mve.getMaxValue() < rangeLowerValue) {
									UniversalTellUser.error(comp, "Chosen lower value is higher than the max value of graph.");
									return;
								}
								if(!hasRangeSetting) {
									rangeUpperValue = mve.getMaxValue() + (mve.getMaxValue() - newValue)*0.1; 
								}
								rangeLowerValue = newValue;
								hasRangeSetting = true;
							}
						}
						catch(Exception e2) {
							rangeLowerValue = keepLower;
							rangeUpperValue = keepUpper;
							hasRangeSetting = keepHas;
							UniversalTellUser.error(comp, "Failed to set lower range value");
						}
						if(comp != null) {
							comp.repaint();
						}
					}
				});
		popup.add(menuItem);
		menuItem = new JMenuItem("Set ranges upper value");
		menuItem.addActionListener(
				new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						double keepUpper = rangeUpperValue;
						double keepLower = rangeLowerValue;
						boolean keepHas = hasRangeSetting;
						try {
							String value = JOptionPane.showInputDialog(comp,
									"Type chosen upper range value",
									"Upper value choice",
									JOptionPane.OK_CANCEL_OPTION);


							if(value != null) {
								Double newValue = Double.parseDouble(value);
								if(mve.getMinValue() > rangeUpperValue) {
									UniversalTellUser.error(comp, "Chosen upper value is lower than the min value of graph.");
									return;
								}
								if(!hasRangeSetting) {
									rangeLowerValue = mve.getMinValue() - (newValue - mve.getMinValue())*0.1; 
								}
								rangeUpperValue = newValue;
								hasRangeSetting = true;
							}
						}
						catch(Exception e2) {
							rangeUpperValue = keepUpper;
							rangeLowerValue = keepLower;
							hasRangeSetting = keepHas;
							UniversalTellUser.error(comp, "Failed to set upper range value");
						}
						if(comp != null) {
							comp.repaint();
						}
					}
				});
		popup.add(menuItem);
		if(hasRangeSetting) {
			menuItem = new JMenuItem("Reset range");
			menuItem.addActionListener(
					new ActionListener()
					{
						public void actionPerformed(ActionEvent e)
						{
							hasRangeSetting = false;
							if(comp != null) {
								comp.repaint();
							}
						}
					});
			popup.add(menuItem);
		}
		menuItem = new JMenuItem("Save data to file");
		menuItem.addActionListener(
				new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						saveToFile("Point to the .rrd-file to write", comp);
					}
				});
		popup.add(menuItem);
		/*
	    menuItem = new JMenuItem("Copy to clipboard");
	    menuItem.addActionListener(
	      new ActionListener()
	      {
	        public void actionPerformed(ActionEvent e)
	        {
		  	      universalTellUser.info(comp, 
		  	    		  "The copy to clipboard functionality is not yet implemented.\n"+
		  	    		  "Sorry for all inconvenience.");
	        }
	      });
	    popup.add(menuItem);
	    menuItem = new JMenuItem("Open Inspector");
	    menuItem.addActionListener(
	      new ActionListener()
	      {
	        public void actionPerformed(ActionEvent e)
	        {  	
		        RrdInspector.main(new String[] {});
	        }
	      });
	    popup.add(menuItem);
	    menuItem = new JMenuItem("Inspect");
	    menuItem.addActionListener(
	      new ActionListener()
	      {
	        public void actionPerformed(ActionEvent e)
	        {  	
	        	//String path = saveToFile("First save the .rrd-file", comp);
	        	//if(path == null) return;
		        RrdInspector.main(new String[] { RRD_PATH });
	        }
	      });
	    popup.add(menuItem);
		 */
		return popup;
	}

}
