package se.modlab.simplesim.algorithms.setup;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.events.*;
import se.modlab.simplesim.scoping.*;
import se.modlab.simplesim.references.*;

public class TypedefStructMember
{
  
  protected String type;
  protected String name;
  protected String filename;
  protected int line;
  protected int column;
  
  public TypedefStructMember(String _name, String _type,
                             String _filename, 
                             int _line, int _column)
  {
    type = _type;
    name = _name;
    filename = _filename;
    line = _line;
    column = _column;
  }

  public String getType()
  {
    return type;
  }

  public String getName()
  {
    return name;
  }
  
  public String getPlace() {
	  return "file "+filename+", line "+line+", column "+column;
  }
  
  public String getFilename() {
	  return filename;
  }
  
  public int getLine() {
	  return line;
  }
  
  public int getColumn() {
	  return column;
  }  

  public VariableFactory getFactory(Scope s)
    throws IntolerableException
  {
    VariableFactory vf = s.getFactory(type);
    if(vf == null)
    {
      throw new UserCompiletimeError(
        "There is no type named "+type+".\n"+
        "Referenced in file "+filename+" on line "+line+", column "+column);
    }
    //System.out.println("typedefStructMember.getFactory(s) - type = "+
    //                   type+"\n  Class = "+vf.getClass().getName()+"\n");
    /*if(vf instanceof enqueuableFactory)
    {
      System.out.println("  "+vf.getClass().getName()+" is an enqueuable factory");
      return new typedQueuableReferenceFactory((simScope) s, type, true);
    }*/
    return vf;
  }

  public String toString()
  {
    return "Typedef struct member: Type "+type+", name "+name+".\n"+
        "Declared in file "+filename+" on line "+line+", column "+column;
  }

}