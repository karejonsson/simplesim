package se.modlab.simplesim.algorithms.setup;

import java.io.InputStream;
import java.util.Hashtable;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.simplesim.algorithms.initiators.*;
import se.modlab.simplesim.algorithms.samplers.*;
import se.modlab.simplesim.events.*;
import se.modlab.simplesim.scoping.*;
import se.modlab.simplesim.variables.Manager;

public class ProcedureTriggedFactoryManager implements Manager
{

  private String typesName;
  private Manager localScope[];
  private LogicalExpression le_execute;
  private ProgramBlock p;
  private LogicalExpression le_requeue;
  private SimData sd;
  private ScopeFactory sf;
  
  public ProcedureTriggedFactoryManager(
           String _typesName,
           Manager _localScope[],
           LogicalExpression _le_execute,
           ProgramBlock _p,
           LogicalExpression _le_requeue,
           SimData _sd,
           ScopeFactory _sf)
  {
    typesName = _typesName;
    localScope = _localScope;
    le_execute = _le_execute;
    p = _p;
    le_requeue = _le_requeue;
    sd = _sd;
    sf = _sf;
  }

  public void initialize(Scope s, boolean isSharp, Hashtable<VariableInstance, Manager> managermap)
    throws IntolerableException
  {
    ProcedureTriggedFactory ptf = 
      new ProcedureTriggedFactory(
        (SimScope)s, 
        typesName, 
        localScope, 
        le_execute, 
        p, 
        le_requeue,
        sd, 
        sf, 
        isSharp);
    VariableFactory vf = s.addFactoryUniquely(ptf);
    if(vf != ptf)
    {
      throw new UserCompiletimeError("Type "+typesName+" multiply defined.");
    }
  } 
  
  public String toString()
  {
    return "procedure trigged factory manager for type "+typesName+" with scope ";
  }
  
	public void reinitializeLogical(LogicalExpression le) throws IntolerableException {
		throw new UserRuntimeError(
				"Cannot set a trigged type to the value of a boolean expression.\n"+
				"This happens to the type "+typesName+".");
    }

	public void reinitializeArithmetic(ArithmeticEvaluable ae) throws IntolerableException {
		throw new UserRuntimeError(
				"Cannot set a trigged type to the value of a arithmetic expression.\n"+
				"This happens to the type "+typesName+".");
    }

	public void reinitializeFromInputStream(InputStream is) throws IntolerableException {
		throw new InternalError(
				"Cannot set a trigged type to the value of an input stream.\n"+
				"This happens to the type "+typesName+".");
    }


}