package se.modlab.simplesim.algorithms.presentation;

import java.util.*;
import java.io.*;
import se.modlab.generics.files.*;
import se.modlab.generics.exceptions.*;

public class HtmlSiteGenerator
{

  private Vector timeGraphNames = new Vector();
  private Vector timeGraphPaths = new Vector();
  private Vector crossplotNames = new Vector();
  private Vector crossplotPaths = new Vector();
  private Vector overloadNames = new Vector();
  private Vector overloadPaths = new Vector();
  private FileWriter fw = null;
  private FileCollector fc[];

  public HtmlSiteGenerator()
  {
  }

  public void addTimeGraphGifReference(String name, String filename)
  {
    timeGraphNames.addElement(name);
    timeGraphPaths.addElement(filename);
  }

  public void addCrossplotGifReference(String name, String filename)
  {
    crossplotNames.addElement(name);
    crossplotPaths.addElement(filename);
  }

  public void addOverloadGifReference(String name, String filename)
  {
    overloadNames.addElement(name);
    overloadPaths.addElement(filename);
  }

  public void setCollectors(FileCollector fc[])
  {
    this.fc = fc;
  }

  private void add(String html)
    throws IOException
  {
    fw.write(html+"\n");
  }

  public void generate(String filename)
    throws IntolerableException
  {
    try
    {
      fw = new FileWriter(filename);
    }
    catch(IOException ioe)
    {
      throw new SystemError("File "+filename+" could not be created!", ioe);
    }
    try
    {
      add("<html>");
      add("<head>");
      add("<title>Generated report</title>");
      add("</head>");
      add("<body>");
      add("<br><br>");
      add("This is the program that was executed:");
      add("<br><br>");
      add("Name "+fc[0].getFilename());
      add("<br><br>");
      add("<pre>");
      add(fc[0].getFilecontents());
      add("</pre>");
      if(fc.length == 1)
      {
        add("There were no data sourcefiles.");
      }
      else
      {
        add("These are the data sourcefiles that were used:");
        for(int i = 1 ; i < fc.length ; i++)
        {
          add("<br><br>");
          add("Name "+fc[i].getFilename());
          add("<br><br>");
          add("<pre>");
          add(fc[i].getFilecontents());
          add("</pre>");
        }    
      }  
      add("<br><br><br><br>");
      for(int i = 0 ; i < timeGraphNames.size() ; i++)
      {
        add("The evolution of the variable "+timeGraphNames.elementAt(i));
        add("<br><br>");
        add("<img SRC=\""+timeGraphPaths.elementAt(i)+"\" WIDTH=700 HEIGHT=500>");
        add("<br><br><br><br>");
      }
      for(int i = 0 ; i < crossplotNames.size() ; i++)
      {
        add("This crossplot was created: "+crossplotNames.elementAt(i));
        add("<br><br>");
        add("<img SRC=\""+crossplotPaths.elementAt(i)+"\" WIDTH=700 HEIGHT=500>");
        add("<br><br><br><br>");
      }
      for(int i = 0 ; i < overloadNames.size() ; i++)
      {
        add("This overload was created: "+overloadNames.elementAt(i));
        add("<br><br>");
        add("<img SRC=\""+overloadPaths.elementAt(i)+"\" WIDTH=700 HEIGHT=500>");
        add("<br><br><br><br>");
      }
      add("</body>");
    }
    catch(IOException ioe)
    {
      throw new SystemError("I/O Error: HTML-site generation failure!", ioe);
    }
    try
    {
      fw.close();
    }
    catch(IOException ioe)
    {
      throw new SystemError("I/O Error: File "+filename+" could not be closed!", ioe);
    }
  }
  

}