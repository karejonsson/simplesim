package se.modlab.simplesim.algorithms.setup;

import java.io.InputStream;
import java.util.Hashtable;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.simplesim.algorithms.initiators.*;
import se.modlab.simplesim.algorithms.samplers.*;
import se.modlab.simplesim.bags.*;
import se.modlab.simplesim.scoping.*;
import se.modlab.simplesim.variables.Manager;

public class BooleanBagManager implements Manager
{

	private String type = null;
	private String name = null;
	private String filename;
	private int line;
	private int column;
	private SimData sd = null;

	public BooleanBagManager(String _type, 
			String _name, 
			String _filename,
			int _line,
			int _column,
			SimData _sd)
	{
		name = _name;
		type = _type; 
		filename = _filename;
		line = _line;
		column = _column;
		//System.out.println("Type "+type);
		sd = _sd;
	}

	private void _initialize(Scope s, boolean isSharp, Hashtable<VariableInstance, Manager> managermap)
	throws IntolerableException
	{
		VariableFactory _vf = s.getFactory(type);
		if(_vf == null)
		{
			new InternalError(
					"Factory of type "+type+" was not found!"+".\n"+
					"Referenced in file "+filename+" on line "+line+" on column "+column);
		}
		if(!(_vf instanceof BooleanBagFactory))
		{
			new InternalError( 
					"Not boolean bag type of factory!\n"+
					"It was of type "+_vf.getClass().getName()+".\n"+
					"Referenced in file "+filename+" on line "+line+" on column "+column);
		}
		BooleanBagFactory vf = (BooleanBagFactory) _vf;
		VariableInstance vi = vf.getInstance(name, filename, line, column, managermap);
		if(!(vi instanceof BooleanBag))
		{
			new InternalError(
					"Not a boolean bag from an arithmetic bag factory\n"+
					"It was of type "+vi.getClass().getName()+".\n"+
					"Referenced in file "+filename+" on line "+line+" on column "+column);
		}
		BooleanBag db = (BooleanBag) vi;
		s.addComplexInstance(db);
		if(isSharp)
		{
			sd.addInitiator(new BagInitiator(db));
		}
		if(managermap != null) {
			managermap.put(vi, this);
		}
	} 

	public void initialize(Scope s, boolean isSharp, Hashtable<VariableInstance, Manager> managermap)
	throws IntolerableException
	{
		try {
			_initialize(s, isSharp, managermap);
		}
		catch(UserRuntimeError ue) {
			String m = ue.getMessage()+"\n"+"This happened in file "+filename+", line "+line+", column "+column;
			UserRuntimeError ue2 = new UserRuntimeError(m, ue.getCause(), ue.getCollectors());
			ue2.setAction(ue.getAction());
			throw ue2;
		}
		catch(UserCompiletimeError ue) {
			String m = ue.getMessage()+"\n"+"This happened in file "+filename+", line "+line+", column "+column;
			UserCompiletimeError ue2 = new UserCompiletimeError(m, ue.getCause(), ue.getCollectors());
			ue2.setAction(ue.getAction());
			throw ue2;
		}
	}

	private String getTypesName()
	{
		return "";
	}
	
	public void reinitializeLogical(LogicalExpression le) throws IntolerableException {
		throw new UserRuntimeError(
				"Cannot set a boolean bag to the value of a boolean expression.\n"+
				"This happens to the bag "+name+" declared on line "+line+", column "+column+" in the file "+filename);
    }

	public void reinitializeArithmetic(ArithmeticEvaluable ae) throws IntolerableException {
		throw new UserRuntimeError(
				"Cannot set an boolean bag to the value of an arithmetic expression.\n"+
				"This happens to the bag "+name+" declared on line "+line+", column "+column+" in the file "+filename);
    }

	public void reinitializeFromInputStream(InputStream is) throws IntolerableException {
		throw new InternalError(
				"Cannot set boolean bag to the value of input stream.\n"+
				"This happens to the bag "+name+" declared on line "+line+", column "+column+" in the file "+filename);
    }

}