package se.modlab.simplesim.algorithms.illustrate.share;

import java.awt.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.exceptions.*;

public interface IllustrationUnitFactoried
{

  public void init(int _width, int _height) throws IntolerableException;
  public boolean isEnqueued() throws IntolerableException;
  public String getIconFilenameCalculated();
  public String getName();
  public void paint(Graphics g)
    throws IntolerableException;

}

