package se.modlab.simplesim.algorithms.presentation;

import java.lang.reflect.*;
import se.modlab.generics.exceptions.*;

public class VariableNodeVisitor
{

  public void visit(Object o)
    throws IntolerableException
  {
    try 
    {
      Class c = getClass();
      Method m = c.getMethod("visit", 
                             new Class[] { o.getClass() });
      m.invoke(this, new Object[] { o });
    } 
    catch(Exception e) 
    {
      //System.out.println("variableNodeVisitor - "+o.getClass().getName());
      e = getException(e);
      if(e instanceof IntolerableException) throw (IntolerableException)e;
      throw new InternalError(getMessage(e));
    }
  }

  private Exception getException(Throwable e)
  {
    if(!(e instanceof InvocationTargetException))
    {  
      return (Exception)e;
    }
    while(e instanceof InvocationTargetException)
    {
      e = ((InvocationTargetException) e).getTargetException();
    }
    return (Exception)e;
  }

  private String getMessage(Throwable e)
  {
    Exception _e = getException(e);
    //_e.printStackTrace();
    return _e.toString();
  }

}