package se.modlab.simplesim.algorithms.setup;

import java.io.InputStream;
import java.util.Hashtable;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.simplesim.variables.*;
import se.modlab.simplesim.references.*;
import se.modlab.simplesim.algorithms.initiators.*;
import se.modlab.simplesim.algorithms.samplers.*;
import se.modlab.simplesim.scoping.*;
import se.modlab.simplesim.events.*;

public class StructReferenceManager implements Manager
{
  
  private String type = null;
  private String name = null;
  private String filename;
  private int line;
  private int column;
  private SimData sd = null;
  private ReferenceEvaluable re = null;
  private boolean _con;
  private boolean _exp;
  private boolean _pub;

  public StructReferenceManager(String _type,
                                String _name,
                                String _filename,
                                int _line,
                                int _column,
                                SimData _sd,
                                ReferenceEvaluable _re,
                                boolean _const,
                                boolean _export,
                                boolean _public)
  {
    type = _type;
    name = _name;
    filename = _filename;
    line = _line;
    column = _column;
    sd = _sd;
    re = _re;
    _con = _const;
    _exp = _export;
    _pub = _public;
  }

  public String getFilename()
  {
    return filename;
  }  

  public int getLine()
  {
    return line;
  }  

  public int getColumn()
  {
    return line;
  }  

  private void _initialize(Scope s, boolean isSharp, Hashtable<VariableInstance, Manager> managermap)
    throws IntolerableException
  {
    VariableFactory vf = null;
    StructReference var = null;
    if(type == null)
    {
      vf = s.getFactory("struct");
      if(vf == null)
      {
        throw new InternalError(
          "No factory for the universal struct."); 
      }
      if(!_con)
      {
        var = new UntypedStructReference(name, vf, filename, line, column);
      }
      else
      {
        var = new UntypedStructReferenceConst(name, vf, filename, line, column);
      }
    }
    else
    {
      vf = s.getFactory(type);
      if(vf == null)
      {
        throw new InternalError(
          "No factory for type "+type+".\n"+
          "Referenced in file "+filename+" on line "+line+" on column "+column);

      }
      if(!_con)
      {
        var = new TypedStructReference(name, vf, filename, line, column);
      }
      else
      {
        var = new TypedStructReferenceConst(name, vf, filename, line, column);
      }
    }
    if(isSharp)
    {
      sd.addInitiator(new StructReferenceInitiator(var, s, re));
    }
    sValue val = null;
    if(re != null)
    {
      //variableInstance vi = vl.getInstance(s);
      sValue sv = re.evaluate(s);
      if(sv == null)
      {
        val = new sStruct();
      } 
      else
      {
        if(!(sv instanceof sStruct))
        {
          throw new UserCompiletimeError(
            "The variable "+name+" is a struct or reference to a\n"+
            "struct but assigned the value "+sv+".\n");
        }
        Complex cx = ((sStruct) sv).getStruct();
        if(cx == null)
        {
          val = new sStruct();
        }
        if(cx instanceof StructReference)
        {
          val = ((StructReference) cx).getValue();
        } 
        else
        {
          if(cx instanceof Complex)
          {
            val = new sStruct((Complex) cx);
          }
          if(val == null)
          {
            throw new InternalError(
              "cx is of type "+cx.getClass());
          }
        }
      }
    }
    else
    {
      val = new sStruct();
    }
    var.setInitialValue(val);
    s.addVariable(var);
    //System.out.println("Added variable "+var);
    if(_exp)
    {
      Scope gs = sd.getGlobalScope();
      if(gs == s)
      {
        throw new UserCompiletimeError(
          "You may not export variables from global scope.\n"+
          "This happens with the variable "+name+".");
      }
      SimScope ss = (SimScope) s;
      ss.addMember(new StructReferenceExported((StructReference) var));
      return;
    }
    if(_pub)
    {
      Scope gs = sd.getGlobalScope();
      if(gs == s)
      {
        throw new UserCompiletimeError(
          "You may not declare variables public in global scope.\n"+
          "This happens with the variable "+name+".");
      }
      SimScope ss = (SimScope) s;
      ss.addMember(var);
      return;
    }
	if(managermap != null) {
		managermap.put(var, this);
	}
  } 
  
  public void initialize(Scope s, boolean isSharp, Hashtable<VariableInstance, Manager> managermap)
  throws IntolerableException
  {
    try {
      _initialize(s, isSharp, managermap);
    }
	catch(UserRuntimeError ue) {
	      String m = ue.getMessage()+"\n"+"This happened in file "+filename+", line "+line+", column "+column;
	      UserRuntimeError ue2 = new UserRuntimeError(m, ue.getCause(), ue.getCollectors());
		  ue2.setAction(ue.getAction());
		  throw ue2;
	    }
	catch(UserCompiletimeError ue) {
	      String m = ue.getMessage()+"\n"+"This happened in file "+filename+", line "+line+", column "+column;
	      UserCompiletimeError ue2 = new UserCompiletimeError(m, ue.getCause(), ue.getCollectors());
		  ue2.setAction(ue.getAction());
		  throw ue2;
	    }
  }
  
  public String toString()
  {
    return "referenceManager for "+name+
           " of type "+type+
           " with const = "+_con+
           ", export = "+_exp+
           ", public = "+_pub+
           " in file "+filename+" on line "+line+" on column "+column;
  }

	public void reinitializeLogical(LogicalExpression le) throws IntolerableException {
		throw new UserRuntimeError(
				"Cannot set a struct reference to the value of a boolean expression.\n"+
				"This happens to the reference "+name+" declared on line "+line+", column "+column+" in the file "+filename);
    }

	public void reinitializeArithmetic(ArithmeticEvaluable ae) throws IntolerableException {
		throw new UserRuntimeError(
				"Cannot set a struct reference to the value of an arithmetic expression.\n"+
				"This happens to the reference "+name+" declared on line "+line+", column "+column+" in the file "+filename);
    }

	public void reinitializeFromInputStream(InputStream is) throws IntolerableException {
		throw new InternalError(
				"Cannot set struct reference to the value of input stream.\n"+
				"This happens to the reference "+name+" declared on line "+line+", column "+column+" in the file "+filename);
    }

}