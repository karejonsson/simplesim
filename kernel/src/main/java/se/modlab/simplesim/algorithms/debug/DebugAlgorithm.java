package se.modlab.simplesim.algorithms.debug;

import se.modlab.generics.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.gui.util.SimpleExplorerNode;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.simplesim.events.*;
import se.modlab.simplesim.algorithms.share.*;
import se.modlab.simplesim.algorithms.externalpainters.ExternalPainterFactoryHandle;
import se.modlab.simplesim.algorithms.once.*;
import se.modlab.simplesim.algorithms.setup.*;
import se.modlab.simplesim.algorithms.presentation.*;
import se.modlab.simplesim.scoping.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;

import javax.swing.*;

import java.util.*;
import java.io.*;

public class DebugAlgorithm extends OnceAlgorithm
{

  private SimpleExplorerNode nodes[];
  private double startTimeDebug;
  private double endTimeDebug;
  private Vector registeredEvents = new Vector();
  private Commons com;
  private int event_ctr = 0;

  public DebugAlgorithm(SimData sd, double startTimeDebug, double endTimeDebug)
  {
    super(sd);
    com = sd.getCommons();
    this.startTimeDebug = startTimeDebug;
    this.endTimeDebug = endTimeDebug;
  }

  private void addEvent(SimulationEvent event)
    throws IntolerableException
  {
    double currentTime = com.getCurrentSimTime();
    if((startTimeDebug <= currentTime) && 
       (endTimeDebug >= currentTime))
    {
      long l = com.getCurrentEpochSimTime();
      String caltime = Commons.getTimeStringFromLong(l);
      EventRow er = new EventRow(event_ctr,
                                 event.getEventTypeName(),
                                 event.getEventName(),
                                 currentTime,
                                 caltime);
      /*
      System.out.println(event.getEventTypeName()+"\t"+
                         event.getEventName()+"\t"+
                         currentTime+"\t"+
                         caltime);
       */
      registeredEvents.addElement(er);
    }
    event_ctr++;
  }

  public void executeSimulation()
    throws IntolerableException
  {
    //System.out.println("Debug - executeSimulation");
    Commons com = sd.getCommons();
    boolean stopHistoryNotTroublesome = true;
    try
    {
      //System.out.println("Innan:\n"+eq);
      Vector ies = sd.getInitially();
      for(int i = 0 ; i < ies.size() ; i++)
      {
        InitiallyEvent ie = (InitiallyEvent) ies.elementAt(i);
        addEvent(ie);
        ie.runEvent();
      }
      //System.out.println("Efter:\n"+eq);
      while(!queueEmpty())
      {
        SimulationEvent event = getFromQueue();    
        com.setCurrentSimTime(event.getSimTimeForNextEvent());
        addEvent(event);
        event.runEvent();
        //System.out.println("debugAlgo - executeSimulation - trigged.size() = "+trigged.size());
        if(event.shallBeRequeued())
        {
          queueUp(event);
        }
        for(int i = trigged.size() - 1 ; i >= 0 ; i--)
        {
          ProcedureTrigged triggedEvent = (ProcedureTrigged) trigged.elementAt(i);
          if(triggedEvent.shallRun())
          {
            addEvent(triggedEvent);
            triggedEvent.runEvent();
          }
          if(!triggedEvent.shallBeRequeued())
          {
            trigged.removeElement(triggedEvent);
          }
        }
      }
      com.setTextualStopReason("Queue ran empty");
    }
    catch(StopExceptionUserProgrammatic e)
    {
      com.setTextualStopReason(e.getMessage());
      stopHistoryNotTroublesome = false;
    }
    catch(StopException e)
    {
      com.setTextualStopReason(e.getMessage());
    }
    catch(IntolerableException e)
    {
      com.setTextualStopReason(e.getMessage());
      throw e;
    }
    catch(NullPointerException npe)
    {
      if(eq == null)
      {
        // Progress bar stop
        //done = true;
        throw new UserBreak();
      }
      //done = true;
      throw new UnclassedError(
          "(algorithm - executeSimulation - NullPointerException)", npe);
    }
    catch(Exception e)
    {
      com.setTextualStopReason("Internal error");
      throw new UnclassedError(
          "Internal error: (algorithm - executeSimulation - kernel)", e);
    }

    if(!stopHistoryNotTroublesome) return;

    try
    {
      FinallyEvent fe = sd.getFinally();
      if(fe != null) 
      {
        addEvent(fe);
        fe.runEvent();
      }
    }
    catch(StopExceptionUserProgrammatic e)
    {
      com.setTextualStopReason(e.getMessage());
    }
    catch(StopException e)
    {
      com.setTextualStopReason(e.getMessage());
    }
    catch(IntolerableException e)
    {
      com.setTextualStopReason(e.getMessage());
      throw e;
    }
    catch(NullPointerException npe)
    {
      if(eq == null)
      {
        // Progress bar stop
        //done = true;
        throw new UserBreak();
      }
      //done = true;
      throw new UnclassedError(
          "(algorithm - executeSimulation - NullPointerException)", npe);
    }
    catch(Exception e)
    {
      com.setTextualStopReason("Internal error");
      throw new UnclassedError(
          "Internal error: (algorithm - executeSimulation - finally)", e);
    }

  }

  public void runAlgorithm()
    throws IntolerableException
  {
    //System.out.println("debug - runAlgorithm");
    FirstLevelTreeNodeBuilderVisitor fltnbv = null;
    SegmentPainter sp = null;
    try
    {
      setupScenario();
    //System.out.println("debug - runAlgorithm - 2");
      runSimulation();
    //System.out.println("debug - runAlgorithm - 3");
     
      //System.out.println("PP "+sd.getNoOfMonitoredVariables());

      sp = new SegmentPainterOnce();
      fltnbv = new FirstLevelTreeNodeBuilderVisitor(sp, ExternalPainterFactoryHandle.get());
      SimScope s = (SimScope) sd.getGlobalScope();
      VariableInstance vis[] = s.getAllInstances();
      for(int i = 0 ; i < vis.length ; i++)
      {
        //System.out.println("onceAlgo - "+vis[i]);
        vis[i].accept(fltnbv);
      } 

      Variables vars = (Variables) fltnbv.getResult();
      node = new TopNode(sd, vars, sp);
      extracts = fltnbv.getExtracts();
      Runtime.getRuntime().runFinalization();
      setDone();
      //System.out.println("Slut - once - runAlgorithm");
    }
    catch(IntolerableException ie)
    {
    //System.out.println("debug - runAlgorithm - 4 "+ie);
      sp = new SegmentPainterOnce();
      fltnbv = new FirstLevelTreeNodeBuilderVisitor(sp, ExternalPainterFactoryHandle.get());
      SimScope s = (SimScope) sd.getGlobalScope();
      VariableInstance vis[] = s.getAllInstances();
      for(int i = 0 ; i < vis.length ; i++)
      {
        //System.out.println("onceAlgo - "+vis[i]);
        vis[i].accept(fltnbv);
      } 

      Variables vars = (Variables) fltnbv.getResult();
      node = new TopNode(sd, vars, sp);
      extracts = fltnbv.getExtracts();
      Runtime.getRuntime().runFinalization();
      displayResultsGraphically(true);
      setDone();
      throw ie;
    }
    catch(Exception e)
    {
    //System.out.println("debug - runAlgorithm - 5 "+e);
      //setDone();
      throw new UnclassedError("debug ...", e);
    }
    //displayResultsGraphically();
  }

  public void displayResultsGraphically(boolean visible)
  {
    //System.out.println("debug - displayResultsGraphically");
    super.displayResultsGraphically(visible);
    EventTable.flash(new EventTable(registeredEvents));
  }
 
}