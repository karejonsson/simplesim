package se.modlab.simplesim.algorithms.presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

//import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;

public class TextualReport extends Thread 
{
 
  private JTextArea ta = new JTextArea();
  private JScrollPane sp = new JScrollPane();
  private boolean stop = false;
  private String title;
  private JFrame f;
  private int x;
  private int y;
  private Commons com;
  private int messageCtr = 0;
  private Vector messages = new Vector();
  private JButton btn = new JButton("Stop simulation");

  public TextualReport(String f, int x, int y, Commons com, String startMessage)
  {
    title = f;
    this.x = x;
    this.y = y;
    this.com = com;
    messages.addElement(startMessage);
  }

  public void run()
  {
    ta.setEditable(false);
    sp.setViewportView(ta);
    sp.setPreferredSize(new Dimension(400,600));
    this.f = new JFrame(title);
    f.setLocation(x,y);
    //sp = new JScrollPane();
    f.getContentPane().add(sp, BorderLayout.CENTER);
    btn.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          TextualReport.this.terminate();
        }
      });
    f.getContentPane().add(btn, BorderLayout.SOUTH);
    f.pack();
    f.setVisible(true);
    try
    {
      while(!stop)
      {
        sleep(2000);
        periodicUpdate();
      }
    }
    catch(InterruptedException ie)
    {
    }
  }

  private synchronized void periodicUpdate()
  {
    if(!stop)
    { 
      if(com != null)
      {
        long l = com.getCurrentEpochSimTime();
        addMessage("Simulation at "+com.getSimTimeFromEpochTime(l)+
                   " or "+com.getTimeStringFromLong(l));

      }
      if(messages.size() > 30)
      {
        while(messages.size() > 5) 
        {
          messages.removeElementAt(1);
        }
        messageCtr = 0;
        ta.setText("");
      }
      while(messageCtr < messages.size())
      {
        ta.append("\n"+messages.elementAt(messageCtr++));
      }
    }
  }

  public synchronized void addMessage(String message)
  {
    messages.addElement(message);
  }

  public synchronized boolean getStopPushed()
  { 
    if(stop && (f != null))
    {
      f.setVisible(false);
      stop();
    }
    return stop;
    //return false;
  }

  synchronized void terminate()
  {
    stop = !stop;
    if(stop)
    {
      btn.setText("Simulation stopped. Continue anyway?");
    }
    else
    {
      btn.setText("Simulation continued. Stop again?");
    }
  }

  public synchronized void forceTermination()
  {
    stop();
    if(f != null)
    {
      f.setVisible(false);
    }
  }

}