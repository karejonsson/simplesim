package se.modlab.simplesim.algorithms.setup;

import java.io.InputStream;
import java.util.Hashtable;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.logics.LogicalExpression;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.scoping.*;
import se.modlab.simplesim.variables.*;
import se.modlab.simplesim.algorithms.initiators.*;
import se.modlab.simplesim.algorithms.samplers.*;

public class ComplexManager implements Manager
{

	private static CreateExportedVersionVisitor cevv = null;

	private String type;
	private String name = null;
	private String filename;
	private int line;
	private int column;
	private SimData sd;
	private boolean _con;
	private boolean _mon;
	private boolean _exp;
	private boolean _pub;

	public ComplexManager(String _type,
			String _name,
			String _filename,
			int _line,
			int _column,
			SimData _sd,
			boolean _const,
			boolean _monitor,
			boolean _export,
			boolean _public)
	{
		type = _type;
		name = _name;
		filename = _filename;
		line = _line;
		column = _column;
		sd = _sd;
		_con = _const;
		_mon = _monitor;
		_exp = _export;
		_pub = _public;
	}

	public String getFilename()
	{
		return filename;
	}  

	public int getLine()
	{
		return line;
	}  

	public int getColumn()
	{
		return line;
	}  

	private void _initialize(Scope s, boolean isSharp, Hashtable<VariableInstance, Manager> managermap)
	throws IntolerableException
	{
		//System.out.println("complexManager - initialize "+s.getName()+" isSharp = "+isSharp);
		if(cevv == null)
		{
			cevv = CreateExportedVersionVisitor.getInstance();
		}
		SimVariableFactory vf = (SimVariableFactory) s.getFactory(type);
		if(vf == null)
		{
			throw new UserCompiletimeError("No type "+name); 
		}
		//System.out.println("complexManager: Type "+vf.getClass().getName().toString()+", name = "+name);
		VariableInstance vi = null;// = factory.getInstance(name, _mon);
		if(_mon)
		{
			vi = vf.getInstance(name, true, filename, line, column, managermap);//factory.getInstance(name, true);
		}
		else
		{
			if(!_con)
			{
				vi = vf.getInstance(name, false, filename, line, column, managermap);//factory.getInstance(name, false);
			}
			else
			{
				vi.accept(cevv);
				vi = cevv.getExportedVersion();
			}
		}
		s.addComplexInstance(vi);
		if(isSharp)
		{
			sd.addInitiator(new ComplexInitiator(vi, s));
			sd.addEndValueSampler(new ComplexEndValueSampler(vi));
			if(_mon) 
			{
				//sd.addMonitoredVariable((monitoredVariable) vi);
			}
		}
		vi.setDefaultInitialValue();
		if(_exp)
		{
			Scope gs = sd.getGlobalScope();
			if(gs == s)
			{
				throw new UserCompiletimeError(
						"You may not export variables from global scope.\n"+
						"This happens with the variable "+name+".");
			}
			SimScope ss = (SimScope) s;
			vi.accept(cevv);
			ss.addMember(cevv.getExportedVersion());
		}
		if(_pub)
		{
			Scope gs = sd.getGlobalScope();
			if(gs == s)
			{
				throw new UserCompiletimeError(
						"You may not declare variables public in global scope.\n"+
						"This happens with the variable "+name+".");
			}
			SimScope ss = (SimScope) s;
			ss.addMember(vi);
		}
		if(managermap != null) {
			managermap.put(vi, this);
		}

	} 

	public void initialize(Scope s, boolean isSharp, Hashtable<VariableInstance, Manager> managermap)
	throws IntolerableException
	{
		try {
			_initialize(s, isSharp, managermap);
		}
		catch(UserRuntimeError ue) {
			String m = ue.getMessage()+"\n"+"This happened in file "+filename+", line "+line+", column "+column;
			UserRuntimeError ue2 = new UserRuntimeError(m, ue.getCause(), ue.getCollectors());
			ue2.setAction(ue.getAction());
			throw ue2;
		}
		catch(UserCompiletimeError ue) {
			String m = ue.getMessage()+"\n"+"This happened in file "+filename+", line "+line+", column "+column;
			UserCompiletimeError ue2 = new UserCompiletimeError(m, ue.getCause(), ue.getCollectors());
			ue2.setAction(ue.getAction());
			throw ue2;
		}
	}


	public String toString()
	{
		return "complexManager for "+name+
		" with const = "+_con+
		", monitor = "+_mon+
		", export = "+_exp+
		", public = "+_pub+
		" in file "+filename+" on line "+line+" on column "+column+
		"\n. Factory is for type "+type;
	}
	
	public void reinitializeLogical(LogicalExpression le) throws IntolerableException {
		throw new UserRuntimeError(
				"Cannot set a complex instance to the value of a boolean expression.\n"+
				"This happens to the actor "+name+" declared on line "+line+", column "+column+" in the file "+filename);
    }

	public void reinitializeArithmetic(ArithmeticEvaluable ae) throws IntolerableException {
		throw new UserRuntimeError(
				"Cannot set a complex instance to the value of an arithmetic expression.\n"+
				"This happens to the actor "+name+" declared on line "+line+", column "+column+" in the file "+filename);
    }

	public void reinitializeFromInputStream(InputStream is) throws IntolerableException {
		throw new InternalError(
				"Cannot set complex to the value of input stream.\n"+
				"This happens to the complex "+name+" declared on line "+line+", column "+column+" in the file "+filename);
    }


}