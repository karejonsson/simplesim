package se.modlab.simplesim.algorithms.presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.util.*;
import se.modlab.generics.gui.find.StringGridBagLayout;
import java.awt.geom.Dimension2D;
import se.modlab.simplesim.variables.*;
import se.modlab.generics.gui.util.*;

public class OverlayPlot extends JDialog 
{

  private JComboBox xaxis;// = new JComboBox();
  private JComboBox yaxis;// = new JComboBox();
  private JButton cancelButton = new JButton("Cancel");
  private JButton goButton = new JButton("Go!");

  private MonitoredVariableExtracted extracts[];
  private OverloadAdder ola;
  private SegmentPainter sp;
  private SimpleExplorer se;

  public OverlayPlot(SimpleExplorer _se, 
                     boolean flag, 
                     MonitoredVariableExtracted _extracts[],
                     OverloadAdder _ola,
                     SegmentPainter _sp)
  {
    super(_se, flag);
    extracts = _extracts;
    ola = _ola;
    sp = _sp;
    se = _se;
    xaxis = new JComboBox(extracts);
    yaxis = new JComboBox(extracts);
    initComponents();
    pack();
  }

  private void initComponents()
  {
    Container container = getContentPane();
    container.setLayout(new StringGridBagLayout());
    setTitle("Overlay plot");
    addWindowListener(new WindowAdapter() {

      public void windowClosing(WindowEvent windowevent)
      {
        setVisible(false);
        dispose();
      }

    });

    JLabel jlabel = new JLabel();
    jlabel.setDisplayedMnemonic('U');
    jlabel.setLabelFor(xaxis);
    jlabel.setText("Variable 1");
    container.add("anchor=WEST,insets=[12,12,0,0]", jlabel);
    xaxis.setToolTipText("Choose variable");
    container.add("fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", xaxis);
    
    JLabel jlabel1 = new JLabel();
    jlabel1.setDisplayedMnemonic('P');
    jlabel1.setText("Variable 2");
    jlabel1.setLabelFor(yaxis);
    container.add("gridx=0,gridy=1,anchor=WEST,insets=[11,12,0,0]", jlabel1);
    yaxis.setToolTipText("Choose variable");
    container.add("gridx=1,gridy=1,fill=HORIZONTAL,weightx=1.0,insets=[11,7,0,11]", yaxis);

    JPanel jpanel = createButtonPanel();
    container.add("gridx=0,gridy=2,gridwidth=2,anchor=EAST,insets=[17,12,11,11]", jpanel);
    getRootPane().setDefaultButton(goButton);
  }

  private JPanel createButtonPanel()
  {
    JPanel jpanel = new JPanel();
    jpanel.setLayout(new BoxLayout(jpanel, 0));
    goButton.setText("Go!");
    goButton.setToolTipText("Press to plot");
    goButton.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent actionevent)
      {
        plot();
      }
    });
    jpanel.add(goButton);
    jpanel.add(Box.createRigidArea(new Dimension(5, 0)));
    cancelButton.setText("Cancel");
    cancelButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent actionevent)
      {
        cancel();
      }
    });
    jpanel.add(cancelButton);
    jpanel.add(Box.createRigidArea(new Dimension(5, 0)));
    Vector vector = new Vector(2);
    vector.add(cancelButton);
    vector.add(goButton);
    equalizeComponentSizes(vector);
    vector.removeAllElements();
    return jpanel;
  }

  private void equalizeComponentSizes(java.util.List list)
  {
    boolean flag = false;
    Dimension dimension = new Dimension(0, 0);
    Object obj = null;
    Object obj1 = null;
    for(int i = 0; i < list.size(); i++)
    {
      JComponent jcomponent = (JComponent)list.get(i);
      Dimension dimension1 = jcomponent.getPreferredSize();
      dimension.width = Math.max(dimension.width, (int)dimension1.getWidth());
      dimension.height = Math.max(dimension.height, (int)dimension1.getHeight());
    }

    for(int j = 0; j < list.size(); j++)
    {
      JComponent jcomponent1 = (JComponent)list.get(j);
      jcomponent1.setPreferredSize((Dimension)dimension.clone());
      jcomponent1.setMaximumSize((Dimension)dimension.clone());
    }
  }

  private void plot()
  {
    MonitoredVariableExtracted mve1 = (MonitoredVariableExtracted) getVariableOne();
    MonitoredVariableExtracted mve2 = (MonitoredVariableExtracted) getVariableTwo();
    se.setRightSide(new Overload(mve1, mve2, ola, sp));
  }
  
  private void cancel()
  {
    setVisible(false);
    dispose();
  }
  
  public Object getVariableOne()
  {
    return xaxis.getSelectedItem();
  }

  public Object getVariableTwo()
  {
    return yaxis.getSelectedItem();
  }

}

