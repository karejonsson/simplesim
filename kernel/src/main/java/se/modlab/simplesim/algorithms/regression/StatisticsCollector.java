package se.modlab.simplesim.algorithms.regression;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;

public class StatisticsCollector implements MonitoredVariableExtracted
{

	//private static int antal = 0;

	private MonitoredVariable variable;
	private Segment segments[];
	private int no_segments;
	private double segmentSize;
	private int updates;
	private double startTimeRegression;
	private double endTimeRegression;
	private Commons com;
	private int timeLines = 0;
	private int variableLines = 0;
	private double maxValue = 0.0;
	private double minValue = 0.0;
	private String name;

	public StatisticsCollector(MonitoredVariable variable,
			long segments,
			double startTimeRegression,
			double endTimeRegression,
			Commons com)
	{
		this.variable = variable;
		this.segments = new Segment[(int) segments];

		for(int i = 0 ; i < segments ; i++)
		{
			this.segments[i] = new Segment(i);
		}
		no_segments = (int) segments;

		this.startTimeRegression = startTimeRegression;
		this.endTimeRegression = endTimeRegression;
		this.com = com;
		segmentSize = (endTimeRegression - startTimeRegression) / segments;
		name = variable.getName();
		//antal++;
		//System.out.println("ctor Antal statisticsCollector ?r "+antal);
	}
	
	public long getTimeUnit() {
		return com.getTimeUnit();
	}
	
    public long getEpochStartTime() {
    	return com.getEpochStartTime();
    }

	/*
  protected void finalize()
  {
    System.out.println("finalize statisticsCollector ?r "+(antal--));
  }
	 */

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	private double getValueGenerically(MonitoredVariableExtracted mve, int i)
			throws IntolerableException
			{
		sValue val = mve.getValue(i);
		if(val instanceof sBoolean)
		{
			return ((sBoolean) val).getBoolean().booleanValue() ? 1 : 0;
		}
		return val.getValue();
			}

	public void performUpdates()
			throws IntolerableException
			{
		// The values are put to place. At leaps the last value is added.
		MonitoredVariableExtracted mve = variable.getExtract();
		final int size = mve.getSize();
		
		//System.out.println("StatisticsCollector size = "+size);

		for(int i = 0 ; i < size-1 ; i++) {
			double time = mve.getValue(i).getTime();
			if((time >= startTimeRegression) && (time <= endTimeRegression)) {

				double value = getValueGenerically(mve, i);
				
				//System.out.println("i = "+i);
				//System.out.println("mve.getValue(i).getTime() = "+mve.getValue(i).getTime());
				//System.out.println("segmentSize = "+segmentSize);
				//System.out.println("startTimeRegression = "+startTimeRegression);
				
				int this_segment = (int) ((mve.getValue(i).getTime()-startTimeRegression)/ 
						segmentSize);

				//System.out.println("this_segment 1 = "+this_segment);

				this_segment = Math.min(segments.length, this_segment);

				//System.out.println("this_segment 2 = "+this_segment);

				//System.out.println("mve.getValue(i+1).getTime() = "+mve.getValue(i+1).getTime());

				int next_segment = (int) ((mve.getValue(i+1).getTime()-startTimeRegression)/ 
						segmentSize);

				//System.out.println("next_segment 1 = "+next_segment);

				next_segment = Math.min(segments.length, next_segment);

				//System.out.println("next_segment 2 = "+next_segment);

				if(this_segment != next_segment) {
					for(int ii = this_segment ; ii < next_segment ; ii++) {
						segments[ii].addOccurrance(value);
					}
				}
				else {
					segments[this_segment].addOccurrance(value);
				}
			}
		}

		// Fills out the last value from where the last value was put to the end.  
		double time = mve.getValue(size-1).getTime();
		if((time >= startTimeRegression) && (time <= endTimeRegression)) {
			double value = getValueGenerically(mve, size-1);
			int this_segment = (int) ((mve.getValue(size-1).getTime()-startTimeRegression)/ 
					segmentSize);
			if(this_segment < segments.length) {
				for(int ii = this_segment ; ii < segments.length ; ii++) {
					segments[ii].addOccurrance(value);
				}
			}
			else {
				segments[this_segment-1].addOccurrance(value);
			}
		}

		// Take the value
		for(int i = 0 ; i < segments.length ; i++) {
			//System.out.println("StatisticsCollector i = "+i);
			segments[i].consume();
		}

		// Calculates max and min
		minValue = segments[0].getMaximum();
		maxValue = segments[0].getMinimum();

		for(int i = 1 ; i < segments.length ; i++)
		{
			minValue = Math.min(minValue, segments[i].getMinimum());
			maxValue = Math.max(maxValue, segments[i].getMaximum());
		}

		//System.out.println("StatisticsCollector - max="+maxValue+
		//		", min="+minValue+
		//		",segments="+segments.length+
		//		",name="+name);

		updates++;

	}

	public double getMeasure()
			throws IntolerableException
			{
		if(updates < 2)
		{
			throw new InternalError(
					"Measure not valid until two values exists.\n"+
					"This error was diagnosticised in class regression/statisticsCollector.getMeasure()");
		}
		double measure = segments[0].getMeasure();
		for(int i = 1 ; i < segments.length ; i++)
		{
			double tmp = segments[i].getMeasure();
			if(tmp > measure) measure = tmp;
		}
		return measure;
			}

	public int getSize()
	{
		return segments.length;
	}

	public sValue getValue(int i) 
			throws IntolerableException
			{
		if(i < 0)
		{
			throw new InternalError("Index below zero. (= "+i+")\n"+
					"This error was diagnosticised in class regression/statisticsCollector.getValue(int)");
		}
		if(i >= segments.length)
		{
			throw new InternalError("Internal: Index exceeding limit. (= "+i+"), limit = "+segments.length+"\n"+
					"This error was diagnosticised in class regression/statisticsCollector.getValue(int)");
		}
		sDouble sd = new sDouble(segments[i].getAverage());
		sd.setTime(segmentSize * (i+0.5));
		return sd;
			}

	public Object getGeneralizedValue(int i) 
			throws IntolerableException
			{
		//System.out.println("statcol getGeneralizedValue");
		//System.exit(0);
		if(i < 0)
		{
			throw new InternalError("Index below zero. (= "+i+")\n"+
					"This error was diagnosticised in class regression/statisticsCollector.getGeneralizedValue(int)");
		}
		if(i >= segments.length)
		{
			throw new InternalError("Internal: Index exceeding limit. (= "+i+"), limit = "+segments.length+"\n"+
					"This error was diagnosticised in class regression/statisticsCollector.getGeneralizedValue(int)");
		}
		return segments[i];
			}

	public double getMaxValue()
	{
		//  System.out.println("StatisticsCollector getMaxValue "+maxValue+",name="+name);
		return maxValue;
	}

	public double getMinValue()
	{
		//  System.out.println("StatisticsCollector getMinValue "+minValue+",name="+name);
		return minValue;
	}

	public double getEndTime()
	{
		return endTimeRegression-startTimeRegression;
	}

	public String getStartTimeTextual()
	{
		long l = com.getEpochTimeFromSimTime(startTimeRegression);
		return Commons.getTimeStringFromLong(l);
	}

	public String getEndTimeTextual()
	{
		long l = com.getEpochTimeFromSimTime(endTimeRegression);
		return Commons.getTimeStringFromLong(l);
	}

	public void setNoLinesOnTimeAxis(int lines)
	{
		timeLines = lines;
	}

	public int getNoLinesOnTimeAxis()
	{
		return timeLines;
	}

	public void setNoLinesOnVariableAxis(int lines)
	{
		variableLines = lines;
	}

	public int getNoLinesOnVariableAxis()
	{
		return variableLines;
	}

	public void accept(MonitoredVariableVisitor mvv)
	{
		mvv.visitDouble();
	}

	public String toString()
	{
		return getName();
	}

}