package se.modlab.simplesim.algorithms.presentation;

import se.modlab.generics.*;
import se.modlab.generics.gui.util.SimpleExplorerNode;
import se.modlab.generics.sstruct.values.*;

import java.util.Enumeration;
import java.util.Vector;

import javax.swing.tree.TreeNode;
import javax.swing.*;

import java.awt.Component;

import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.algorithms.setup.*;

public class AllVariables implements SimpleExplorerNode
{
  private TreeNode parent;
  private SimData sd;
  private String theText;

  public AllVariables(TreeNode parent, SimData sd)
  {
    this.parent = parent;
    this.sd = sd;
    Vector vars = sd.getAllPermanentResetters();
    StringBuffer sb = new StringBuffer();
    for(int i = 0 ; i < vars.size() ; i++)
    {
      sb.append(vars.elementAt(i).toString()+"\n");
    }
    theText = sb.toString();
  }

  public Enumeration children()
  {
    return null;
  }

  public boolean getAllowsChildren()
  {
    return false;
  }

  public TreeNode getChildAt(int childIndex)
  {
    return null;
  }

  public int getChildCount()
  {
    return 0;
  }

  public int getIndex(TreeNode tn)
  {
    return -1;
  }

  public TreeNode getParent()
  {
    return parent;
  }

  public boolean isLeaf()
  {
    return false;
  }

  public Component getComponent()
  {
    JTextArea ta = new JTextArea();
    ta.setText(theText);
    ta.setEditable(false);
    return ta;
  }  

  public String toString()
  {
    return "All variables";
  }

} 
