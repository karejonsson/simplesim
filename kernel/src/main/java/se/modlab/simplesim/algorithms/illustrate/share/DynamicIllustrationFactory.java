package se.modlab.simplesim.algorithms.illustrate.share;

import java.awt.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.variables.*;

public interface DynamicIllustrationFactory
{

  public String getFactoryName();
  public IllustrationUnitFactoried getInstance(VariableInstance vi)
    throws IntolerableException;

}
