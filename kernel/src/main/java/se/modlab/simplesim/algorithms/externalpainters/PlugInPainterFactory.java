package se.modlab.simplesim.algorithms.externalpainters;

import se.modlab.simplesim.variables.MonitoredVariableExtracted;

public interface PlugInPainterFactory {
	
	public String getName();
	public PlugInPainter getPainter(MonitoredVariableExtracted mve, boolean iterated);
	
}
