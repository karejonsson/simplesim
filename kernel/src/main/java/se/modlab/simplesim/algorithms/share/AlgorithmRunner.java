package se.modlab.simplesim.algorithms.share;

import java.awt.*;

import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.progress.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.algorithms.setup.*;
import se.modlab.simplesim.algorithms.presentation.*;

public class AlgorithmRunner implements Runnable
{

  private Algorithm algo;
  private Component comp;
  private String action;
  private boolean visible;
  //private static int antal = 0;
  
  public AlgorithmRunner(Algorithm algo, Component comp, String action)
  {
    this(algo, comp, action, true);
  }
  
  public AlgorithmRunner(Algorithm algo, Component comp, String action, boolean _visible)
  {
    this.algo = algo;
    this.comp = comp;
    this.action = action;
    visible = _visible;
    //antal++;
    //System.out.println("Antal algorithmRunner "+antal);
  }

  public void run()
  {
    try
    { 
      ProgressDumpAndMonitor.addText("Simplesim", "Starts simulation "+algo.getName());
      algo.runAlgorithm();
      algo.displayResultsGraphically(visible);
    }
    catch(IntolerableException ie)
    {
      ie.setAction(action);
      if(algo != null) ie.setCollectors(algo.getCollectors());
      UniversalTellUser.general(comp, ie);
    }
    catch(OutOfMemoryError oome)
    { 
      UniversalTellUser.general(comp,
        new SystemError(
          "Ran out of memory.\n\n"+
          "Hint 1. Are there large amounts of monitored variables?\n\n"+
          "Hint 2. Is there a large and monitored vector?\n",
          oome));
    }
    catch(Throwable t)
    {
      IntolerableException e = new UnclassedError("algorithmRunner.run - Throwable", t, algo.getCollectors());
      if(algo != null) e.setCollectors(algo.getCollectors());
      e.setAction(action);
      UniversalTellUser.general(comp, e);
    }
    finally {
    	algo = null;
    }
  }

  
  public Algorithm getAlgorithm()
  {
    return algo;
  }
	
}
