package se.modlab.simplesim.algorithms.initiators;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.simplesim.events.*;
import se.modlab.simplesim.references.*;

public class StructReferenceInitiator implements Initiator
{
  private StructReference sr;
  private Scope s;
  private ReferenceEvaluable re;
  private String fakedName;

  public StructReferenceInitiator(StructReference _sr, 
                                  Scope _s, 
                                  ReferenceEvaluable _re)
    throws IntolerableException 
  {
    sr = _sr;
    s = _s;
    re = _re;
    fakedName = _sr.getName();
  }

  public StructReferenceInitiator(StructReference _sr, 
                                  Scope _s, 
                                  ReferenceEvaluable _re,
                                  String _fakedName)
    throws IntolerableException 
  {
    sr = _sr;
    s = _s;
    re = _re;
    fakedName = _fakedName;
  }

  public String getName()
  {
    return sr.getName();
  }

  public VariableInstance getInstance()
  {
    return sr;
  }

  public void runInitiation() throws IntolerableException
  {
    if(re == null)
    {
      sr.setInitialValue(new sStruct());
      return;
    }
    //variableInstance vi = vl.getInstance(s);
    sValue sv = re.evaluate(s);
    
    if(sv == null)
    {
      sr.setInitialValue(new sStruct());
      return;
    }
    
    if(!(sv instanceof sStruct))
    {
      throw new UserRuntimeError(
        "The variable "+sr+" is a struct or reference to a\n"+
        "struct but assigned the value "+sv);
    }
    Complex cx = ((sStruct) sv).getStruct();
    sr.setInitialValue((VariableInstance)cx);
  }

  public String toString()
  {
    return sr.toString()+" in scope "+s.getName()+
      " initiated to "+sr;
  }

}