package se.modlab.simplesim.algorithms.setup;

import java.io.InputStream;
import java.util.Hashtable;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;
import se.modlab.simplesim.algorithms.initiators.*;
import se.modlab.simplesim.algorithms.samplers.*;
import se.modlab.simplesim.scoping.*;
import se.modlab.simplesim.references.*;

public class StructReferenceVectorManager implements Manager
{
  
  private String type = null;
  private String name = null;
  private ArithmeticEvaluable ae;
  private String filename;
  private int line;
  private int column;
  private SimData sd = null;
  private boolean _con;
  private boolean _exp;
  private boolean _pub;

  public StructReferenceVectorManager(String _type,
                                      String _name,
                                      String _filename,
                                      int _line,
                                      int _column,
                                      SimData _sd,
                                      ArithmeticEvaluable _ae,
                                      boolean _const,
                                      boolean _export,
                                      boolean _public)
  {
    type = _type;
    name = _name;
    ae = _ae;
    filename = _filename;
    line = _line;
    column = _column;
    sd = _sd;
    _con = _const;
    _exp = _export;
    _pub = _public;
  }

  public String getFilename()
  {
    return filename;
  }  

  public int getLine()
  {
    return line;
  }  

  public int getColumn()
  {
    return line;
  }  

  private void _initialize(Scope s, boolean isSharp, Hashtable<VariableInstance, Manager> managermap)
    throws IntolerableException
  {
    VariableFactory vf = null;
    String emtype = null;
    if(type == null)
    {
      emtype = "queuable";
    } 
    else
    {
      emtype = type;
    }
    vf = s.getFactory(emtype);
    if(vf == null)
    {
      throw new InternalError("No factory for type "+emtype+" found.\n"+
         "This happens in file "+filename+" on line "+line+" on column "+column); 
    }
    if(!(vf instanceof UntypedStructReferenceFactory))
    {
      vf = new TypedStructReferenceFactory((SimScope) s, type, isSharp);
    }
    //System.out.println("Initialize in "+s.getName()+" for "+toString());
    int length = ae.evaluate(s).getLong().intValue();
    SimVariableFactory _vf = (SimVariableFactory) s.addFactoryUniquely(
      new SimVectorVariableFactory(vf, length));
    VariableInstance si = null;
    if(!_con)
    {
      si = _vf.getInstance(name, false, filename, line, column, managermap);
    }
    else
    {
      throw new UserCompiletimeError(
        "You may not declare a variable vector ("+name+") constant.\n"+
        "Constant null references make no sense.\n");
    }
    si.setDefaultInitialValue();
    s.addComplexInstance(si);
    if(isSharp)
    {
      sd.addInitiator(new ComplexInitiator(si, s));
    }
    if(_exp)
    {
      Scope gs = sd.getGlobalScope();
      if(gs == s)
      {
        throw new UserCompiletimeError(
          "You may not export variables from global scope.\n"+
          "This happens with the variable "+name+".");
      }
      SimScope ss = (SimScope) s;
      ss.addMember(new SimVariableVectorExported((VariableVector)si, "exported"));
    }
    if(_pub)
    {
      Scope gs = sd.getGlobalScope();
      if(gs == s)
      {
        throw new UserCompiletimeError(
          "You may not declare variables public in global scope.\n"+
          "This happens with the variable "+name+".");
      }
      SimScope ss = (SimScope) s;
      ss.addMember(si);
    }
	if(managermap != null) {
		managermap.put(si, this);
	}
  } 
  
  public void initialize(Scope s, boolean isSharp, Hashtable<VariableInstance, Manager> managermap)
  throws IntolerableException
  {
    try {
      _initialize(s, isSharp, managermap);
    }
	catch(UserRuntimeError ue) {
	      String m = ue.getMessage()+"\n"+"This happened in file "+filename+", line "+line+", column "+column;
	      UserRuntimeError ue2 = new UserRuntimeError(m, ue.getCause(), ue.getCollectors());
		  ue2.setAction(ue.getAction());
		  throw ue2;
	    }
	catch(UserCompiletimeError ue) {
	      String m = ue.getMessage()+"\n"+"This happened in file "+filename+", line "+line+", column "+column;
	      UserCompiletimeError ue2 = new UserCompiletimeError(m, ue.getCause(), ue.getCollectors());
		  ue2.setAction(ue.getAction());
		  throw ue2;
	    }
  }
  
	public void reinitializeLogical(LogicalExpression le) throws IntolerableException {
		throw new UserRuntimeError(
				"Cannot set a stgruct reference vector to the value of a boolean expression.\n"+
				"This happens to the vector "+name+" declared on line "+line+", column "+column+" in the file "+filename);
    }

	public void reinitializeArithmetic(ArithmeticEvaluable ae) throws IntolerableException {
		throw new UserRuntimeError(
				"Cannot set a struct reference vector to the value of an arithmetic expression.\n"+
				"This happens to the vector "+name+" declared on line "+line+", column "+column+" in the file "+filename);
    }

	public void reinitializeFromInputStream(InputStream is) throws IntolerableException {
		throw new InternalError(
				"Cannot set a struct reference vector to the value of an input stream.\n"+
				"This happens to the vector "+name+" declared on line "+line+", column "+column+" in the file "+filename);
    }


  public String toString()
  {
    return "structVectorManager for "+name+
           " of type "+type+
           " with export = "+_exp+
           ", public = "+_pub+
           " in file "+filename+" on line "+line+" on column "+column;
  }

}