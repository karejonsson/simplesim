package se.modlab.simplesim.algorithms.presentation;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class CrossplotLinearLinear extends Crossplot 
{

  public CrossplotLinearLinear(MonitoredVariableExtracted mveh, 
                               MonitoredVariableExtracted mvev, 
                               CrossplotAdder cpa,
                               SegmentPainter sp)
  {
    super(mveh, mvev, cpa, sp);
  }

  protected boolean makeChecks(Graphics g, Dimension d)
  {
    return true;
  }

  public double getVerticalTransition(double v)
  {
    return v;
  }

  public double getHorizontalTransition(double h)
  {
    return h;
  }

  public double getVerticalInversion(double v)
  {
    return v;
  }

  public double getHorizontalInversion(double h)
  {
    return h;
  }

  public String getHorizontalTransitionName()
  {
    return "linear";
  }

  public String getVerticalTransitionName()
  {
    return "linear";
  }

}
