package se.modlab.simplesim.algorithms.bruteforcemax;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.simplesim.algorithms.share.*;
import java.awt.*;
import se.modlab.simplesim.algorithms.presentation.*;
import se.modlab.simplesim.algorithms.initiators.*;

public class ValuesToExamine implements Initiator
{

  private double lower;
  private double upper;
  private int places;
  private String variableName = null;
  private Scope s = null;
  private DoubleConst sdc = null;
  private int incrementor = 0;

  public ValuesToExamine(String variableName,
                         ArithmeticEvaluable ae_lower,
                         ArithmeticEvaluable ae_upper,
                         ArithmeticEvaluable ae_places,
                         Scope s)
    throws IntolerableException
  {
    sValue sv_lower = ae_lower.evaluate(s);
    lower = sv_lower.getValue();
    sValue sv_upper = ae_upper.evaluate(s);
    upper = sv_upper.getValue();
    if(lower >= upper)
    {
      throw new UserCompiletimeError(
        "Upper bound for examination values must be desicevely above\n"+
        "the lower bound. Not the case for "+variableName);
    }
    sValue sv_places = ae_places.evaluate(s);
    if(!(sv_places instanceof sLong))
    {
      throw new UserCompiletimeError(
        "The number of values to examine must be an integer value.\n"+
        "Here it evaluated to "+sv_places.toString()+" for "+
        variableName);
    }
    places = (int) sv_places.getLong().longValue();
    this.variableName = variableName;
    this.s = s;
  }

  public void verificationBeforeAlgorithmExecution()
    throws IntolerableException
  {
    if(sdc != null) return;
    VariableInstance si = s.getComplexInstance(variableName);
    if(si == null)
    {
      throw new UserCompiletimeError(
        "The examinationvariable "+variableName+" is not in global scope");
    }
    if(!(si instanceof DoubleConst))
    {
      throw new UserCompiletimeError(
        "The examinationvariable "+variableName+" is not a constant\n"+
        "double variable though found in global scope as "+si);
    }
    sdc = (DoubleConst) si;
  }

  public DoubleConst getVariable()
  {
    return sdc;
  }

  public int getNumberOfValuesToExamine()
  {
    return places;  
  }

  public void runInitiation() 
    throws IntolerableException
  {
    double d = lower + ((incrementor*(upper - lower))/(places - 1));
    sdc.setInitialValue(new sDouble(d));
    //System.out.println("run - Variable "+sdc);
  }

/*
  public void runSampling() throws intolerableException
  {
  }

  public void runSetAverageValue() throws intolerableException
  {
  }
 */

  public VariableInstance getInstance()
  {
    return getVariable();
  }

  public int getIncrementor()
  {
    return incrementor;
  }

  public void setIncrementor(int incrementor) 
    throws IntolerableException
  {
    this.incrementor = incrementor;
    double d = lower + ((incrementor*(upper - lower))/(places - 1));
    sDouble doub = new sDouble(d);
    sdc.setInitialValue(doub);
    //System.out.println("Variable "+sdc+" xxx "+doub);
  }

  public boolean incrementAndReportFlipover()
  {
    incrementor++;
    if(incrementor >= places)
    {
      incrementor = 0;
      return true;
    }
    return false;
  }

  public String toString()
  {
    try
    {
      return variableName+" = "+sdc.getValue()+". Examination "+
             (incrementor+1)+" of "+places;
    }
    catch(IntolerableException ie)
    {
    }
    return variableName+" cannot be evaluated. Examination "+
           (incrementor+1)+" of "+places;

  }

}