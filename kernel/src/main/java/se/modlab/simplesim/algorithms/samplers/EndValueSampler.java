package se.modlab.simplesim.algorithms.samplers;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.variables.*;

public interface EndValueSampler
{
  public void runSampling() throws IntolerableException;
  public void runSetAverageValue() throws IntolerableException;
  public VariableInstance getInstance();
}
