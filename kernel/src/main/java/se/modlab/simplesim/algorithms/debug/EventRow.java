package se.modlab.simplesim.algorithms.debug;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.simplesim.variables.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.simplesim.algorithms.share.*;
import java.awt.*;
import java.util.*;

public class EventRow
{

  private int exec_nr;
  private String eventType;
  private String eventName;
  private double simTimeOfExecution;
  private String calendarTimeOfExecution;

  public EventRow(int exec_nr,
                  String eventType, 
                  String eventName, 
                  double simTimeOfExecution, 
                  String calendarTimeOfExecution)
    throws IntolerableException
  {
    this.exec_nr = exec_nr;
    this.eventType = eventType;
    this.eventName = eventName;
    this.simTimeOfExecution = simTimeOfExecution;
    this.calendarTimeOfExecution = calendarTimeOfExecution;
  }
 
  public String getValueAt(int idx)
  {
    if(idx == 0) return ""+exec_nr;
    if(idx == 1) return eventType;
    if(idx == 2) return eventName;
    if(idx == 3) return ""+simTimeOfExecution;
    if(idx == 4) return calendarTimeOfExecution;
    return "eventRow - getValueAt("+idx+") internal error";
  }

}