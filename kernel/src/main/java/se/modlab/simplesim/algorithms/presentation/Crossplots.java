package se.modlab.simplesim.algorithms.presentation;

import se.modlab.generics.gui.util.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import java.util.Enumeration;
import java.util.Vector; 
import java.awt.*;
import javax.swing.*;
import javax.swing.tree.*;
import java.awt.Component;

public class Crossplots implements SimpleExplorerNode, CrossplotAdder
{
  private Vector c = new Vector();
  private TreeNode parent;

  public Crossplots(TreeNode parent)
  {
    this.parent = parent;
  }

  public boolean addOneCrossplot(JComponent cp)
  {
    if(cp == null) return false;
    for(int i = 0 ; i < c.size() ; i++)
    {
      if(cp.toString().compareTo(c.elementAt(i).toString()) == 0)
      {
        return false;
      }
    }
    c.addElement(new OneCrossplot(cp, this));
    for(Container p = cp.getParent(); p != null; p = p.getParent()) 
    {
      if(p instanceof SimpleExplorer) 
      {
    	  ((SimpleExplorer) p).refresh();
      }
    }
    return true;
  }

  public boolean removeOneCrossplot(JComponent cp)
  {
    if(cp == null) return false;
    for(int i = 0 ; i < c.size() ; i++)
    {
      if(cp.toString().compareTo(c.elementAt(i).toString()) == 0)
      {
        c.removeElement(c.elementAt(i));
        for(Container p = cp.getParent(); p != null; p = p.getParent()) 
        {
          if(p instanceof SimpleExplorer) 
          {
            ((SimpleExplorer) p).refresh();
          }
        }
        return true;
      }
    }
    return false;
  }

  public Enumeration children()
  {
    return c.elements();
  }

  public boolean getAllowsChildren()
  {
    return true;
  }

  public TreeNode getChildAt(int childIndex)
  {
    if(childIndex >= c.size()) return null;
    if(childIndex < 0) return null;
    return (TreeNode) c.elementAt(childIndex); 
  }

  public int getChildCount()
  {
    return c.size();
  }

  public int getIndex(TreeNode tn)
  {
    if(c == null) return -1;
    if(tn == null) return -1;
    return c.indexOf(tn);
  }

  public TreeNode getParent()
  {
    return parent;
  }

  public boolean isLeaf()
  {
    return false;
  }

  public Component getComponent()
  {
    JTextArea ta = new JTextArea();
    ta.append("Subjacent crossplots has been created!");
    return ta;
  }  

  public String toString()
  {
    return "Crossplots";
  }

} 
