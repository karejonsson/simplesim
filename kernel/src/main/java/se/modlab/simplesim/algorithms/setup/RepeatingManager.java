package se.modlab.simplesim.algorithms.setup;

import java.io.InputStream;
import java.util.Hashtable;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.simplesim.algorithms.initiators.*;
import se.modlab.simplesim.algorithms.samplers.*;
import se.modlab.simplesim.events.*;
import se.modlab.simplesim.scoping.*;
import se.modlab.simplesim.variables.Manager;


public class RepeatingManager implements Manager
{

  protected String name;
  protected String type;
  protected SimData sd;
  protected String filename;
  protected int line;
  protected int column;
  
  public RepeatingManager(String _type, String _name, SimData _sd, String _filename, int _line, int _column)
  {
    name = _name;
    type = _type;
    sd = _sd;
    filename = _filename;
    line = _line;
    column = _column;
  }

  private void _initialize(Scope s, boolean isSharp, Hashtable<VariableInstance, Manager> managermap)
    throws IntolerableException
  {
    VariableFactory _vf = s.getFactory(type);
    if(!(_vf instanceof ProcedureRepeatingFactory))
    {
      new InternalError("Not repeating type of factory!\n"+
                        "It was of type "+_vf.getClass().getName());
    }
    ProcedureRepeatingFactory vf = (ProcedureRepeatingFactory) _vf;
    VariableInstance vi = vf.getInstance(name, filename, line, column, managermap);
    if(!(vi instanceof ProcedureRepeating))
    {
      new InternalError("Not a repeating from an repeating factory\n"+
                        "It was of type "+vi.getClass().getName());
    }
    ProcedureRepeating pr = (ProcedureRepeating) vi;
    s.addComplexInstance(pr);
    if(isSharp)
    {
      sd.addQueuable(pr);
      sd.addInitiator(new QueableInitiator(pr));
    }
	if(managermap != null) {
		managermap.put(vi, this);
	}
  } 
  
  public void initialize(Scope s, boolean isSharp, Hashtable<VariableInstance, Manager> managermap)
  throws IntolerableException
  {
    try {
      _initialize(s, isSharp, managermap);
    }
	catch(UserRuntimeError ue) {
	      String m = ue.getMessage()+"\n"+"This happened in file "+filename+", line "+line+", column "+column;
	      UserRuntimeError ue2 = new UserRuntimeError(m, ue.getCause(), ue.getCollectors());
		  ue2.setAction(ue.getAction());
		  throw ue2;
	    }
	catch(UserCompiletimeError ue) {
	      String m = ue.getMessage()+"\n"+"This happened in file "+filename+", line "+line+", column "+column;
	      UserCompiletimeError ue2 = new UserCompiletimeError(m, ue.getCause(), ue.getCollectors());
		  ue2.setAction(ue.getAction());
		  throw ue2;
	    }
  }


  public String toString()
  {
    return "procedure repeating manager for "+name;
  }
  
	public void reinitializeLogical(LogicalExpression le) throws IntolerableException {
		throw new UserRuntimeError(
				"Cannot set a repeater to the value of a boolean expression.\n"+
				"This happens to the repeater "+name+" declared on line "+line+", column "+column+" in the file "+filename);
    }

	public void reinitializeArithmetic(ArithmeticEvaluable ae) throws IntolerableException {
		throw new UserRuntimeError(
				"Cannot set a repeater to the value of an arithmetic expression.\n"+
				"This happens to the repeater "+name+" declared on line "+line+", column "+column+" in the file "+filename);
    }

	public void reinitializeFromInputStream(InputStream is) throws IntolerableException {
		throw new InternalError(
				"Cannot set a repeater to the value of an input stream.\n"+
				"This happens to the repeater "+name+" declared on line "+line+", column "+column+" in the file "+filename);
    }

}