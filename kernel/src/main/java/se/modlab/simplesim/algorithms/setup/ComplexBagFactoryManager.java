package se.modlab.simplesim.algorithms.setup;

import java.io.InputStream;
import java.util.Hashtable;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.simplesim.algorithms.initiators.*;
import se.modlab.simplesim.algorithms.samplers.*;
import se.modlab.simplesim.events.*;
import se.modlab.simplesim.scoping.*;
import se.modlab.simplesim.variables.Manager;

public class ComplexBagFactoryManager implements Manager
{

  private String type;
  private int algo;
  private VariableLookup vl = null;
  
  public ComplexBagFactoryManager(
           String _type,
           int _algo, 
           VariableLookup _vl)
  {
    type = _type;
    algo = _algo;
    vl = _vl;
  }

  public void initialize(Scope s, boolean isSharp, Hashtable<VariableInstance, Manager> managermap)
    throws IntolerableException
  {
    ComplexBagFactory out = 
      new ComplexBagFactory((SimScope)s, type, algo, vl);
    s.addFactoryUniquely(out);
  } 

  private String getTypesName()
  {
    return ComplexBagFactory.getTypesName(type, algo, vl);
    //"bag<"+type+","+arithmeticBagFactory.algos[algo]+">";
  }
 
  public String toString()
  {
    String tn = getTypesName();
    return "Bag factory manager for type "+tn;
  }
  
	public void reinitializeLogical(LogicalExpression le) throws IntolerableException {
		throw new InternalError(
				"Cannot set a complex bag type to the value of a boolean expression.\n"+
				"This happens to the bag type "+type+".");
    }

	public void reinitializeArithmetic(ArithmeticEvaluable ae) throws IntolerableException {
		throw new UserRuntimeError(
				"Cannot set a complex bag type to the value of an arithmetic expression.\n"+
				"This happens to the bag type "+type+".");
    }

	public void reinitializeFromInputStream(InputStream is) throws IntolerableException {
		throw new InternalError(
				"Cannot set complex bag factory to the value of input stream.\n"+
				"This happens to the bag type "+type);
    }



}