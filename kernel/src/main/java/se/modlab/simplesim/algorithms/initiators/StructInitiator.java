package se.modlab.simplesim.algorithms.initiators;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.events.*;

public class StructInitiator implements Initiator
{

  private Complex s;

  public StructInitiator(Complex _s)
  {
    s = _s;
  }


  public String getName()
  {
    return s.toString();
  }

  public VariableInstance getInstance()
  {
    return (VariableInstance) s;
  }

  public void runInitiation() throws IntolerableException
  {
    //s.reset();
    //System.out.println("initiated "+q.toString());
  }

  public String toString()
  {
    return "Struct initiator for "+s;
  }

}
