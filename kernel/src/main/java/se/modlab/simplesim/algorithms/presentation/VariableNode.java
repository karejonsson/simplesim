package se.modlab.simplesim.algorithms.presentation;

import java.awt.Component;
import java.util.Enumeration;
import java.util.Vector;
import javax.swing.tree.TreeNode;
import se.modlab.generics.gui.util.*;
import se.modlab.generics.exceptions.*;

public abstract class VariableNode implements SimpleExplorerNode
{
	protected TreeNode parent;
	private Vector<TreeNode> children = new Vector<TreeNode>();
	
	public VariableNode(TreeNode _parent) {
		parent = _parent;
	}
	
	public abstract Component getComponent();

	public Enumeration children()
	{
	  return children.elements();
	}

	  public boolean getAllowsChildren()
	  {
	    return true;
	  }

	  public TreeNode getChildAt(int childIndex)
	  {
	    return children.elementAt(childIndex);
	  }

	  public int getChildCount()
	  {
	    return children.size();
	  }

	  public int getIndex(TreeNode tn)
	  {
		  for(int i = 0 ; i < children.size() ; i++) {
			  if(children.elementAt(i) == tn) return i;
		  }
	    return -1;
	  }

	  public TreeNode getParent()
	  {
	    return parent;
	  }

	  public boolean isLeaf()
	  {
	    return children.size() == 0;
	  }
	  
	  public boolean addChild(TreeNode child) {
		  for(int i = 0 ; i < children.size() ; i++) {
			  if(children.elementAt(i).toString().compareTo(child.toString()) == 0) return false;
		  }
		  children.addElement(child);
		  return true;		  
	  }
	  
  public void accept(VariableNodeVisitor vnv)
    throws IntolerableException
  {
    vnv.visit(this);
  }

}