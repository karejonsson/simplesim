package se.modlab.simplesim.algorithms.setup;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.events.*;
import se.modlab.simplesim.scoping.*;
import se.modlab.simplesim.references.*;

public class TypedefTypedQueuableReferenceMember 
       extends TypedefStructMember
{
  
  public TypedefTypedQueuableReferenceMember(String _name, 
                                             String _type, 
                                             String _filename, 
                                             int _line, 
                                             int _column)
  {
    super(_name, _type, _filename, _line, _column);
  }

  public VariableFactory getFactory(Scope s)
    throws IntolerableException
  {
    VariableFactory vf = s.getFactory(type);
    if(vf == null)
    {
      throw new UserCompiletimeError(
        "There is no queuable type named "+type+" to reference.\n"+
        "Referenced in file "+filename+" on line "+line+", column "+column);
    }
    //System.out.println("typedefTypedQueuableReferenceMember - happens");
    return new TypedQueuableReferenceFactory((SimScope) s, type, true);
  }

  public String toString()
  {
    return "Typedef queuable reference member: Type "+type+", name "+name+".\n"+
        "Declared in file "+filename+" on line "+line+", column "+column;
  }

}