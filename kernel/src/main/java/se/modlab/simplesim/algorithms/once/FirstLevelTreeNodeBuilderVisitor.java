package se.modlab.simplesim.algorithms.once;

import se.modlab.simplesim.variables.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.gui.util.SimpleExplorerNode;

import java.util.*;

import javax.swing.tree.TreeNode;

import se.modlab.generics.*;
import se.modlab.simplesim.algorithms.share.*;
import se.modlab.simplesim.events.*;
import se.modlab.simplesim.algorithms.externalpainters.ExternalPainterFactoryHandle;
import se.modlab.simplesim.algorithms.externalpainters.PlugInPainter;
import se.modlab.simplesim.algorithms.externalpainters.PlugInPainterFactory;
import se.modlab.simplesim.algorithms.presentation.*;
import se.modlab.simplesim.scoping.*;
import se.modlab.simplesim.references.*;
import se.modlab.simplesim.bags.*;

public class FirstLevelTreeNodeBuilderVisitor 
               extends InstanceVisitorAdapter
{

  private Stack s = null;
  private Stack names = null;
  private SegmentPainter sp;
  private Vector<MonitoredVariableExtracted> extracts = new Vector<MonitoredVariableExtracted>();
  private PlugInPainterFactory pips[];

  public FirstLevelTreeNodeBuilderVisitor(SegmentPainter _sp, PlugInPainterFactory _pips[])
  {
    sp = _sp;
    s = new Stack<Variables>();
    names = new Stack();
    s.push(new Variables(sp));
    pips = _pips;
  }

  public String getFullName()
  {
    StringBuffer sb = new StringBuffer();
    for(int i = 0 ; i < names.size() ; i++)
    {
      sb.append(names.elementAt(i).toString());
    }
    return sb.toString();
  }
 
  public SimpleExplorerNode getResult()
    throws IntolerableException 
  {
    Variables sen = (Variables) s.pop();
    if(s.size() != 0)
    {
      throw new InternalError(
        "Stack not empty at call to getResult in class\n"+
        "firstLevelTreeNodeBuilderVisitor");
    }
    return sen;
  }

  public MonitoredVariableExtracted[] getExtracts()
  {
    //System.out.println("first level ... size = "+extracts.size());
    MonitoredVariableExtracted out[] = 
      new MonitoredVariableExtracted[extracts.size()];
    for(int i = 0 ; i < extracts.size() ; i++)
    {
      out[i] = (MonitoredVariableExtracted) extracts.elementAt(i);
      //System.out.println("["+i+"] : "+out[i]);
    }
    return out;
  }

  public void visit(BooleanVariable inst) 
    throws IntolerableException 
  {
    //inst.setName(getFullName()+inst.getName());
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneBooleanVariable(getFullName()+inst.getName(), inst, itn, sp));
  }
 
  public void visit(BooleanConst inst) 
    throws IntolerableException 
  {
    //inst.setName(getFullName()+inst.getName());
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneBooleanVariable(getFullName()+inst.getName(), inst, itn, sp));
  }
 
  public void visit(SimBooleanExported inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneBooleanVariable(getFullName()+inst.getName(), inst, itn, sp));
/*
    throw new internalError(
      "Builds monitoration tree and falls on\n"+
      "export node. SimBooleanExported");
 */
  }
 
  public void visit(SimBooleanMonitoredVariable inst) 
    throws IntolerableException 
  {
    MonitoredVariableExtracted mve = inst.getExtract();
    mve.setName(getFullName()+mve.getName());
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneMonitoredVariable(mve, itn, sp, pips));
    extracts.addElement(mve);
  }
 
  public void visit(DoubleVariable inst) 
    throws IntolerableException 
  {
    //inst.setName(getFullName()+inst.getName());
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneArithmeticVariable(getFullName()+inst.getName(), inst, itn, sp));
  }
 
  public void visit(DoubleConst inst) 
    throws IntolerableException 
  {
    //inst.setName(getFullName()+inst.getName());
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneArithmeticVariable(getFullName()+inst.getName(), inst, itn, sp));
  }
 
  public void visit(SimDoubleExported inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneArithmeticVariable(getFullName()+inst.getName(), inst, itn, sp));
/*
    throw new internalError(
      "Builds monitoration tree and falls on\n"+
      "export node. SimDoubleExported");
 */
  }
 
  public void visit(SimDoubleMonitoredVariable inst) 
    throws IntolerableException 
  {
    //System.out.println("firstLevelTreeNodeBuilderVisitor visit SimDoubleMonitoredVariable "+inst.getName());
    MonitoredVariableExtracted mve = inst.getExtract();
    String fullName = getFullName()+inst.getName();
    mve.setName(fullName);
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    IntermediateTreeNode itn_variable = new IntermediateTreeNode(itn, fullName+" (monitored)");
    itn.addChild(itn_variable);
    VariableNode vn = new OneMonitoredVariable(mve, itn, sp, pips);
    itn_variable.addChild(vn);
    for(int i = 0 ; i < pips.length ; i++) {
    	//System.out.println("firstLevelTreeNodeBuilderVisitor "+i);
    	itn_variable.addChild(new OneExternallyPaintedVariable(fullName, vn, pips[i].getPainter(mve, false)));
    }
    extracts.addElement(mve);
  }
  /*
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    String fullName = getFullName()+inst.getName();
    VariableNode vn = new OneArithmeticVariable(fullName, inst, itn, sp);
    itn.addChild(vn);
    PlugInPainterFactory pipfs[] = ExternalPainterFactoryHandle.get();
    if(pipfs == null) return;
    for(int i = 0 ; i < pipfs.length ; i++) {
    	System.out.println("firstLevelTreeNodeBuilderVisitor "+i);
    	vn.addChild(new OneExternallyPaintedVariable(fullName, vn, pipfs[i].getPainter()));
    }
 
   */
 
  public void visit(LongVariable inst) 
    throws IntolerableException 
  {
    //inst.setName(getFullName()+inst.getName());
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneArithmeticVariable(getFullName()+inst.getName(), inst, itn, sp));
  }
 
  public void visit(LongConst inst) 
    throws IntolerableException 
  {
    //inst.setName(getFullName()+inst.getName());
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneArithmeticVariable(getFullName()+inst.getName(), inst, itn, sp));
  }
 
  public void visit(SimLongExported inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneArithmeticVariable(getFullName()+inst.getName(), inst, itn, sp));
/*
    throw new internalError(
      "Builds monitoration tree and falls on\n"+
      "export node. SimLongExported");
 */
  }
 
  public void visit(SimLongMonitoredVariable inst) 
    throws IntolerableException 
  {
	    //System.out.println("firstLevelTreeNodeBuilderVisitor visit SimDoubleMonitoredVariable "+inst.getName());
	    MonitoredVariableExtracted mve = inst.getExtract();
	    String fullName = getFullName()+inst.getName();
	    mve.setName(fullName);
	    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
	    IntermediateTreeNode itn_variable = new IntermediateTreeNode(itn, fullName+" (monitored)");
	    itn.addChild(itn_variable);
	    VariableNode vn = new OneMonitoredVariable(mve, itn, sp, pips);
	    itn_variable.addChild(vn);
	    for(int i = 0 ; i < pips.length ; i++) {
	    	//System.out.println("firstLevelTreeNodeBuilderVisitor "+i);
	    	itn_variable.addChild(new OneExternallyPaintedVariable(fullName, vn, pips[i].getPainter(mve, false)));
	    }
	    extracts.addElement(mve);
  }

  public void visit(TypedQueuableReference inst) 
    throws IntolerableException 
  {
    //inst.setName(getFullName()+inst.getName());
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneQueuableReference(getFullName()+inst.getName(), inst, itn, sp));
  }
 
  public void visit(TypedQueuableReferenceConst inst) 
    throws IntolerableException 
  {
    //inst.setName(getFullName()+inst.getName());
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneQueuableReference(getFullName()+inst.getName(), inst, itn, sp));
  }
 
  public void visit(UntypedQueuableReference inst) 
    throws IntolerableException 
  {
    //inst.setName(getFullName()+inst.getName());
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneQueuableReference(getFullName()+inst.getName(), inst, itn, sp));
  }
 
  public void visit(UntypedQueuableReferenceConst inst) 
    throws IntolerableException 
  {
    //inst.setName(getFullName()+inst.getName());
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneQueuableReference(getFullName()+inst.getName(), inst, itn, sp));
  }
 
  public void visit(QueuableReferenceExported inst) 
    throws IntolerableException 
  {
    //inst.setName(getFullName()+inst.getName());
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneQueuableReference(getFullName()+inst.getName(), inst, itn, sp));
  }
 
  public void visit(TypedStructReference inst) 
    throws IntolerableException 
  {
    //inst.setName(getFullName()+inst.getName());
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneStructReference(getFullName()+inst.getName(), inst, itn, sp));
  }
 
  public void visit(TypedStructReferenceConst inst) 
    throws IntolerableException 
  {
    //inst.setName(getFullName()+inst.getName());
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneStructReference(getFullName()+inst.getName(), inst, itn, sp));
  }
 
  public void visit(UntypedStructReference inst) 
    throws IntolerableException 
  {
    //inst.setName(getFullName()+inst.getName());
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneStructReference(getFullName()+inst.getName(), inst, itn, sp));
  }
 
  public void visit(UntypedStructReferenceConst inst) 
    throws IntolerableException 
  {
    //inst.setName(getFullName()+inst.getName());
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneStructReference(getFullName()+inst.getName(), inst, itn, sp));
  }
 
  public void visit(StructReferenceExported inst) 
    throws IntolerableException 
  {
    //inst.setName(getFullName()+inst.getName());
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneStructReference(getFullName()+inst.getName(), inst, itn, sp));
  }
 
  public void visit(ProcedureTrigged inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode parent = (IntermediateTreeNode) s.peek(); 
    IntermediateTreeNode itn = new IntermediateTreeNode(parent, getFullName()+inst.getName());
    parent.addChild(itn);
    s.push(itn);
    VariableInstance vis[] = inst.getAllInstances();
    names.push(inst.getEventName()+".");
    for(int i = 0 ; i < vis.length ; i++)
    {
      if(vis[i] != inst) vis[i].accept(this);
    }
    names.pop();
    s.pop();
  }
 
  public void visit(ProcedureSingle inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode parent = (IntermediateTreeNode) s.peek(); 
    IntermediateTreeNode itn = new IntermediateTreeNode(parent, getFullName()+inst.getName());
    parent.addChild(itn);
    s.push(itn);
    VariableInstance vis[] = inst.getAllInstances();
    names.push(inst.getName()+".");
    for(int i = 0 ; i < vis.length ; i++)
    {
      if(vis[i] != inst) vis[i].accept(this);
    }
    names.pop();
    s.pop();
  }
 
  public void visit(ProcedureRepeating inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode parent = (IntermediateTreeNode) s.peek(); 
    IntermediateTreeNode itn = new IntermediateTreeNode(parent, getFullName()+inst.getName());
    parent.addChild(itn);
    s.push(itn);
    VariableInstance vis[] = inst.getAllInstances();
    names.push(inst.getName()+".");
    for(int i = 0 ; i < vis.length ; i++)
    {
      if(vis[i] != inst) vis[i].accept(this);
    }
    names.pop();
    s.pop();
  }
 
  public void visit(ProcedureActor inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode parent = (IntermediateTreeNode) s.peek(); 
    IntermediateTreeNode itn = new IntermediateTreeNode(parent, getFullName()+inst.getName());
    parent.addChild(itn);
    s.push(itn);
    VariableInstance vis[] = inst.getAllInstances();
    names.push(inst.getName()+".");
    for(int i = 0 ; i < vis.length ; i++)
    {
      if(vis[i] != inst) vis[i].accept(this);
    }
    names.pop();
    s.pop();
  }
 
  public void visit(SimComplexVariableExported inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode parent = null;
    parent = (IntermediateTreeNode) s.peek(); 
    IntermediateTreeNode itn = new IntermediateTreeNode(parent, getFullName()+inst.getName());
    parent.addChild(itn);
    s.push(itn);
    names.push(inst.getName()+".");
    traverse(inst);
    s.pop();
    names.pop();
/*
    throw new internalError(
      "Builds monitoration tree and falls on\n"+
      "export node. SimComplexVariableExported");
 */
  }

  public void visit(ComplexVariable inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode parent = null;
    parent = (IntermediateTreeNode) s.peek(); 
    IntermediateTreeNode itn = new IntermediateTreeNode(parent, getFullName()+inst.getName());
    parent.addChild(itn);
    s.push(itn);
    names.push(inst.getName()+".");
    traverse(inst);
    s.pop();
    names.pop();
  }

  public void visit(SimVariableVectorExported inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode parent = (IntermediateTreeNode) s.peek(); 
    IntermediateTreeNode itn = new IntermediateTreeNode(parent, getFullName()+inst.getName());
    parent.addChild(itn);
    s.push(itn);
    VariableInstance _inst = inst.getVectorElement(0);
    if(_inst instanceof ProcedureSingle)
    {
      names.push(inst.getName()+".");
      VariableInstance vis[] = ((ProcedureSingle) _inst).getAllInstances();
      for(int i = 0 ; i < vis.length ; i++)
      {
        vis[i].accept(this);
      }
      //_inst.accept(this);
      s.pop();
      names.pop();
      return;
    }
    names.push(inst.getName());
    traverse(inst);
    s.pop();
    names.pop();
/*
    throw new internalError(
      "Builds monitoration tree and falls on\n"+
      "export node. SimVariableVectorExported");
 */
  }

  public void visit(VariableVector inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode parent = (IntermediateTreeNode) s.peek(); 
    IntermediateTreeNode itn = new IntermediateTreeNode(parent, getFullName()+inst.getName());
    parent.addChild(itn);
    s.push(itn);
    VariableInstance _inst = inst.getVectorElement(0);
    if(_inst instanceof ProcedureSingle)
    {
      names.push(inst.getName()+".");
      VariableInstance vis[] = ((ProcedureSingle) _inst).getAllInstances();
      for(int i = 0 ; i < vis.length ; i++)
      {
        vis[i].accept(this);
      }
      //_inst.accept(this);
      s.pop();
      names.pop();
      return;
    }
    names.push(inst.getName());
    traverse(inst);
    s.pop();
    names.pop();
  }

  public void visit(SimVariableVector inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode parent = (IntermediateTreeNode) s.peek(); 
    IntermediateTreeNode itn = new IntermediateTreeNode(parent, getFullName()+inst.getName());
    parent.addChild(itn);
    s.push(itn);
    VariableInstance _inst = inst.getVectorElement(0);
    if(_inst instanceof ProcedureSingle)
    {
      names.push(inst.getName()+".");
      VariableInstance vis[] = ((ProcedureSingle) _inst).getAllInstances();
      for(int i = 0 ; i < vis.length ; i++)
      {
        if(vis[i] != _inst) vis[i].accept(this);
      }
      //_inst.accept(this);
      s.pop();
      names.pop();
      return;
    }
    names.push(inst.getName());
    traverse(inst);
    s.pop();
    names.pop();
  }

  public void visit(VariableVectorFromFile inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode parent = (IntermediateTreeNode) s.peek(); 
    IntermediateTreeNode itn = new IntermediateTreeNode(parent, getFullName()+inst.getName());
    parent.addChild(itn);
    s.push(itn);
    names.push(inst.getName());
    traverse(inst);
    s.pop();
    names.pop();
  }

  public void visit(SimMonitoredVectorVariableFromFile inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode parent = (IntermediateTreeNode) s.peek(); 
    IntermediateTreeNode itn = new IntermediateTreeNode(parent, getFullName()+inst.getName());
    parent.addChild(itn);
    s.push(itn);
    names.push(inst.getName());
    traverse(inst);
    s.pop();
    names.pop();
  }

  public void visit(ScopeReference inst) 
    throws IntolerableException 
  {
    //System.out.println("firstLevelTreeNodeBuilderVisitor - scopeReference");
  }

  public void visit(ScopeReferenceExported inst) 
    throws IntolerableException 
  {
    //System.out.println("firstLevelTreeNodeBuilderVisitor - scopeReferenceExported");
  }

  public void visit(SubscopeReference inst) 
    throws IntolerableException 
  {
    //System.out.println("firstLevelTreeNodeBuilderVisitor - subscopeReference");
    IntermediateTreeNode parent = (IntermediateTreeNode) s.peek(); 
    IntermediateTreeNode itn = new IntermediateTreeNode(parent, getFullName()+inst.getName());
    parent.addChild(itn);
    s.push(itn);
    names.push(inst.getName()+".");
    traverse((Complex) inst);
    s.pop();
    names.pop();
  }

  public void visit(SubscopeReferenceExported inst) 
    throws IntolerableException 
  {
    //System.out.println("firstLevelTreeNodeBuilderVisitor - subscopeReferenceExported");
    IntermediateTreeNode parent = (IntermediateTreeNode) s.peek(); 
    IntermediateTreeNode itn = new IntermediateTreeNode(parent, getFullName()+inst.getName());
    parent.addChild(itn);
    s.push(itn);
    names.push(inst.getName()+".");
    traverse((Complex) inst);
    s.pop();
    names.pop();
  }

  public void visit(LongLifoBag inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneBagVariable(getFullName()+inst.getName(), inst, itn, sp));
  }

  public void visit(LongLiloBag inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneBagVariable(getFullName()+inst.getName(), inst, itn, sp));
  }

  public void visit(LongRandomBag inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneBagVariable(getFullName()+inst.getName(), inst, itn, sp));
  }

  public void visit(LongSortBag inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneBagVariable(getFullName()+inst.getName(), inst, itn, sp));
  }


  public void visit(DoubleLifoBag inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneBagVariable(getFullName()+inst.getName(), inst, itn, sp));
  }

  public void visit(DoubleLiloBag inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneBagVariable(getFullName()+inst.getName(), inst, itn, sp));
  }

  public void visit(DoubleRandomBag inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneBagVariable(getFullName()+inst.getName(), inst, itn, sp));
  }

  public void visit(DoubleSortBag inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneBagVariable(getFullName()+inst.getName(), inst, itn, sp));
  }


  public void visit(BooleanLifoBag inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneBagVariable(getFullName()+inst.getName(), inst, itn, sp));
  }

  public void visit(BooleanLiloBag inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneBagVariable(getFullName()+inst.getName(), inst, itn, sp));
  }

  public void visit(BooleanRandomBag inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneBagVariable(getFullName()+inst.getName(), inst, itn, sp));
  }


  public void visit(ComplexLifoBag inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneBagVariable(getFullName()+inst.getName(), inst, itn, sp));
  }

  public void visit(ComplexLiloBag inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneBagVariable(getFullName()+inst.getName(), inst, itn, sp));
  }

  public void visit(ComplexRandomBag inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneBagVariable(getFullName()+inst.getName(), inst, itn, sp));
  }

  public void visit(ComplexSortBag inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneBagVariable(getFullName()+inst.getName(), inst, itn, sp));
  }

}
