package se.modlab.simplesim.algorithms.illustrate.struct;

import se.modlab.generics.sstruct.strings.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.variables.*;

public class PredefStimeLiteral implements StringEvaluable
{

	private ArithmeticEvaluable ae = null;
	private Commons com; 

	public PredefStimeLiteral(ArithmeticEvaluable _ae, Commons _com)
	{
		ae = _ae;
		com = _com;
	}

	public String evaluate(Scope s)
	throws IntolerableException
	{
		if(ae != null)
		{ 
			sValue sv_time = ae.evaluate(s);
			double d_time = sv_time.getValue();
			long curEpoch = com.getEpochTimeFromSimTime(d_time);
			return Commons.getTimeStringFromLong(curEpoch);
		}
		long curEpoch = com.getCurrentEpochSimTime();
		String t = Commons.getTimeStringFromLong(curEpoch);
		//System.out.println(t);
		return t;
	}

	public void verify(Scope s) throws IntolerableException {
		if(ae != null)
		{ 
			ae.verify(s);
			return;
		}
		long curEpoch = com.getCurrentEpochSimTime();
		String t = Commons.getTimeStringFromLong(curEpoch);
		//System.out.println(t);
		return;
	}
	
	public String reproduceExpression() {
		return "time()";
	}


}