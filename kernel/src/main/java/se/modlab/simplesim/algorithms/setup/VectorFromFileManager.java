package se.modlab.simplesim.algorithms.setup;

import java.io.InputStream;
import java.util.Hashtable;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.simplesim.algorithms.initiators.*;
import se.modlab.simplesim.algorithms.samplers.*;
import se.modlab.simplesim.events.*;
import se.modlab.simplesim.scoping.*;
import se.modlab.simplesim.variables.*;

public class VectorFromFileManager implements Manager
{

	private static CreateExportedVersionVisitor cevv = null;

	private String type;
	private String name;
	private String filename_tab;
	private String filename_sim;
	private int line;
	private int column;
	private SimData sd;
	private boolean _con;
	private boolean _mon;
	private boolean _exp;
	private boolean _pub;
	private Scope lastSeenScope = null;
	private VariableInstance si = null;

	public VectorFromFileManager(String _type,
			String _name,
			String _filename_tab,
			String _filename_sim,
			int _line,
			int _column,
			SimData _sd,
			boolean _const,
			boolean _monitor,
			boolean _export,
			boolean _public)
	{
		type = _type;
		name = _name;
		filename_tab = _filename_tab;
		filename_sim = _filename_sim;
		line = _line;
		column = _column;
		sd = _sd;
		_con = _const;
		_mon = _monitor;
		_exp = _export;
		_pub = _public;
	}

	public String getFilename_tab()
	{
		return filename_tab;
	}  

	public String getFilename()
	{
		return getFilename_sim();
	}  

	public String getFilename_sim()
	{
		return filename_sim;
	}  

	public int getLine()
	{
		return line;
	}  

	public int getColumn()
	{
		return line;
	}  

	private void _initialize(Scope s, boolean isSharp, Hashtable<VariableInstance, Manager> managermap)
	throws IntolerableException
	{
		lastSeenScope = s;
		//System.out.println("vectorFromFileManager - initialize - filename_sim="+filename_sim+", filename_tab="+filename_tab);
		if(cevv == null)
		{
			cevv = CreateExportedVersionVisitor.getInstance();
		}
		SimVariableFactory vf = (SimVariableFactory) s.getFactory(type);
		if(vf == null)
		{
			throw new UserCompiletimeError(
					"No factory for type "+type+" found.\n"+
					"This happens with the variable "+name+
					" in file "+filename_sim+" on line "+line+" on column "+column);
		}
		vf = (SimVariableFactory) s.addFactoryUniquely(
				new SimVariableVectorFromFileFactory(vf, filename_tab, sd.getCommons()));
		si = null;
		if(_mon)
		{
			si = vf.getInstance(name, true, filename_sim, line, column, managermap);
		}
		else
		{
			if(!_con)
			{
				si = vf.getInstance(name, false, filename_sim, line, column, managermap);
			}
			else
			{
				si = vf.getInstance(name, false, filename_sim, line, column, managermap);
				si.accept(cevv);
				si = cevv.getExportedVersion();
			}
		}
		s.addComplexInstance(si);
		if(isSharp)
		{
			sd.addInitiator(new ComplexInitiator(si, s));
			sd.addEndValueSampler(new ComplexEndValueSampler(si));
			if(_mon) 
			{
				//sd.addMonitoredVariable((monitoredVariable) si);
			}
		}
		if(_exp)
		{
			Scope gs = sd.getGlobalScope();
			if(gs == s)
			{
				throw new UserCompiletimeError(
						"You may not export variables from global scope.\n"+
						"This happens with the variable "+name+
						" in file "+filename_sim+" on line "+line+" on column "+column);
			}
			SimScope ss = (SimScope) s;
			if(si instanceof VariableVector)
			{
				ss.addMember(new SimVariableVectorExported((VariableVector)si, "exported"));
				if(managermap != null) {
					managermap.put(si, this);
				}
				return;
			}
			if(si instanceof VariableVectorFromFile)
			{
				ss.addMember(new SimVariableVectorExported((VariableVectorFromFile)si, "exported"));
				if(managermap != null) {
					managermap.put(si, this);
				}
				return;
			}
			throw new InternalError(
					"Vector from file manager does not know how to make an exported\n"+
					"version of this type. The type is "+si.getClass().getName()); 
		}
		if(_pub)
		{
			Scope gs = sd.getGlobalScope();
			if(gs == s)
			{
				throw new UserCompiletimeError(
						"You may not declare variables public in global scope.\n"+
						"This happens with the variable "+name+
						" in file "+filename_sim+" on line "+line+" on column "+column);
			}
			SimScope ss = (SimScope) s;
			ss.addMember(si);
		}
		if(managermap != null) {
			managermap.put(si, this);
		}
	} 

	public void initialize(Scope s, boolean isSharp, Hashtable<VariableInstance, Manager> managermap)
	throws IntolerableException
	{
		try {
			_initialize(s, isSharp, managermap);
		}
		catch(UserRuntimeError ue) {
			String m = ue.getMessage()+"\n"+"This happened in file "+filename_sim+", line "+line+", column "+column;
			UserRuntimeError ue2 = new UserRuntimeError(m, ue.getCause(), ue.getCollectors());
			ue2.setAction(ue.getAction());
			throw ue2;
		}
		catch(UserCompiletimeError ue) {
			String m = ue.getMessage()+"\n"+"This happened in file "+filename_sim+", line "+line+", column "+column;
			UserCompiletimeError ue2 = new UserCompiletimeError(m, ue.getCause(), ue.getCollectors());
			ue2.setAction(ue.getAction());
			throw ue2;
		}
	}

	public void reinitializeLogical(LogicalExpression le) throws IntolerableException {
		throw new UserRuntimeError(
				"Cannot set a file initiated vector to the value of a boolean expression.\n"+
				"This happens to the vector "+name+" initiated from the file "+filename_tab+".");
	}

	public void reinitializeArithmetic(ArithmeticEvaluable ae) throws IntolerableException {
		throw new UserRuntimeError(
				"Cannot set a file initiated vector to the value of an arithmetic expression.\n"+
				"This happens to the vector "+name+" initiated from the file "+filename_tab+".");
	}

	public void reinitializeFromInputStream(InputStream is) throws IntolerableException {
		//System.out.println("vectorFromFileManager - initialize - filename_sim="+filename_sim+", filename_tab="+filename_tab);
		if(cevv == null)
		{
			cevv = CreateExportedVersionVisitor.getInstance();
		}
		SimVariableFactory vf = (SimVariableFactory) lastSeenScope.getFactory(type);
		if(vf == null)
		{
			throw new UserCompiletimeError(
					"No factory for type "+type+" found.\n"+
					"This happens with the variable "+name+
					" in file "+filename_sim+" on line "+line+" on column "+column);
		}
		vf = (SimVariableFactory) lastSeenScope.addFactoryUniquely(
				new SimVariableVectorFromFileFactory(vf, is, sd.getCommons()));
		VariableInstance si_updated = null;
		Hashtable<VariableInstance, Manager> managermap = null;
		if(_mon)
		{
			si_updated = vf.getInstance(name, true, filename_sim, line, column, managermap);
		}
		else
		{
			if(!_con)
			{
				si_updated = vf.getInstance(name, false, filename_sim, line, column, managermap);
			}
			else
			{
				si_updated = vf.getInstance(name, false, filename_sim, line, column, managermap);
				si_updated.accept(cevv);
				si_updated = cevv.getExportedVersion();
			}
		}
		if(!lastSeenScope.removeComplexInstance(si)) {
			throw new InternalError("There was no previous version of the variable "+si.getName()+" in scope");
		}
		lastSeenScope.addComplexInstance(si_updated);
		sd.removeInitiator(si);
		sd.addInitiator(new ComplexInitiator(si_updated, lastSeenScope));
		sd.removeEndValueSampler(si);
		sd.addEndValueSampler(new ComplexEndValueSampler(si_updated));
		if(_exp)
		{
			Scope gs = sd.getGlobalScope();
			if(gs == lastSeenScope)
			{
				throw new UserCompiletimeError(
						"You may not export variables from global scope.\n"+
						"This happens with the variable "+name+
						" in file "+filename_sim+" on line "+line+" on column "+column);
			}
			SimScope ss = (SimScope) lastSeenScope;
			if(si_updated instanceof VariableVector)
			{
				ss.removeMember(si.getName());
				ss.addMember(new SimVariableVectorExported((VariableVector)si_updated, "exported"));
				if(managermap != null) {
					managermap.put(si_updated, this);
				}
				si = si_updated;
				return;
			}
			if(si_updated instanceof VariableVectorFromFile)
			{
				ss.removeMember(si.getName());
				ss.addMember(new SimVariableVectorExported((VariableVectorFromFile)si_updated, "exported"));
				if(managermap != null) {
					managermap.put(si_updated, this);
				}
				si = si_updated;
				return;
			}
			throw new InternalError(
					"Vector from file manager does not know how to make an exported\n"+
					"version of this type. The type is "+si_updated.getClass().getName()); 
		}
		if(_pub)
		{
			Scope gs = sd.getGlobalScope();
			if(gs == lastSeenScope)
			{
				throw new UserCompiletimeError(
						"You may not declare variables public in global scope.\n"+
						"This happens with the variable "+name+
						" in file "+filename_sim+" on line "+line+" on column "+column);
			}
			SimScope ss = (SimScope) lastSeenScope;
			ss.removeMember(si.getName());
			ss.addMember(si_updated);
		}
		if(managermap != null) {
			managermap.put(si_updated, this);
		}
		si = si_updated;
    }

	public String toString()
	{
		return "vectorFromFileManager for "+name+" of type "+type+
		"from file "+filename_tab+
		" with const = "+_con+
		", monitor = "+_mon+
		", export = "+_exp+
		", public = "+_pub+
		" in file "+filename_sim+" on line "+line+" on column "+column;
	}

}
