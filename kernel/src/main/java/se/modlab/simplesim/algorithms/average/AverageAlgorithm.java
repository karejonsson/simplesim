package se.modlab.simplesim.algorithms.average;

import se.modlab.generics.gui.util.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.simplesim.events.*;
import se.modlab.simplesim.algorithms.setup.*;
import se.modlab.simplesim.algorithms.presentation.*;
import se.modlab.simplesim.algorithms.regression.*;
import se.modlab.simplesim.algorithms.share.*;
import se.modlab.simplesim.algorithms.externalpainters.ExternalPainterFactoryHandle;
import se.modlab.simplesim.algorithms.once.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.swing.*;
import java.util.*;
import java.io.*;
import se.modlab.simplesim.scoping.*;

public class AverageAlgorithm extends AlgorithmBase 
implements Algorithm
{

	private double startTimeRegression;
	private double endTimeRegression;
	private long intervals;
	private long maximumIterations;
	private StatisticsCollector statCollectors[];
	private int finishedLoops = 0;

	private Comparator compare = new EventComparator();

	public AverageAlgorithm(SimData sd,
			double startTimeRegression,
			double endTimeRegression,
			long intervals,
			long maximumIterations)
					throws IntolerableException
					{
		super(sd);
		if(startTimeRegression < 0)
		{
			throw new UserCompiletimeError("Starttime for regression must not be negative.");
		}
		this.startTimeRegression = startTimeRegression;
					if(startTimeRegression >= endTimeRegression)
					{
						throw new UserCompiletimeError("Endtime for regression must be larger than starttime.");
					}
					this.endTimeRegression = endTimeRegression;
					if(intervals < 1)
					{
						throw new UserCompiletimeError("Number of intervals must be at least one.");
					}
					this.intervals = intervals;    
					if(maximumIterations < 2)
					{
						throw new UserCompiletimeError("Maximum number of iterations must be at\n"+
								"least two. Use once-algorithm otherwise.");
					}
					this.maximumIterations = maximumIterations;
					}

	private void runSimulationUpdateSampling()
			throws IntolerableException
			{
		//commons com = sd.getCommons();
		long beforeSimulationExec = System.currentTimeMillis();
		runSimulation();
		long beforeStatCollection = System.currentTimeMillis();
		//System.out.println("averageAlgorithm.runSimulationUpdateSampling() efter exekvering. Tids�tg�ng "+(beforeStatCollection - beforeSimulationExec));

		//System.out.println("averageAlgorithm.runSimulationUpdateSampling() antal "+statCollectors.length);
		for(int i = 0 ; i < statCollectors.length ; i++)
		{
			StatisticsCollector sc = statCollectors[i];
			sc.performUpdates();
		}
		long afterStatCollection = System.currentTimeMillis();
		//System.out.println("averageAlgorithm.runSimulationUpdateSampling() efter statsr�kning. Tids�tg�ng "+(afterStatCollection - beforeStatCollection));
		//sd.resetAllPermanentVariables();
			}

	public void runAlgorithm() throws IntolerableException {
		try {
			setupScenario();

			SegmentPainter sp = new SegmentPainterRegression();
			FirstLevelTreeNodeBuilderVisitorRegression fltnbvr = 
					new FirstLevelTreeNodeBuilderVisitorRegression(
							intervals,
							startTimeRegression,
							endTimeRegression,
							sd,
							sp, ExternalPainterFactoryHandle.get());

			SimScope s = (SimScope) sd.getGlobalScope();
			VariableInstance vis[] = s.getAllInstances();
			for(int i = 0 ; i < vis.length ; i++) {
				vis[i].accept(fltnbvr);
			} 

			Variables vars = (Variables) fltnbvr.getResult();

			node = new TopNode(sd, vars, new SegmentPainterRegression());
			MonitoredVariableExtracted mve[] = fltnbvr.getExtracts();
			statCollectors = new StatisticsCollector[mve.length];

			for(int i = 0 ; i < mve.length ; i++) {
				statCollectors[i] = (StatisticsCollector) mve[i];
			}

			addMessage(
					"Simulation starts at "+Commons.getTimeStringForExecutionTime()+
					". The simulations \nstart time for average analysis is "+
					Commons.getTimeStringFromLong(sd.getCommons().getEpochTimeFromSimTime(startTimeRegression))+
					"\nand the end time for average analysis is\n"+
					Commons.getTimeStringFromLong(sd.getCommons().getEpochTimeFromSimTime(endTimeRegression))+
					". The amount of intervals is "+intervals+"\n. The amount of iterations is "+
					maximumIterations+".\n");

			int loops = 0;
			long timeBeforeAlgo = System.currentTimeMillis();
			do
			{
				long timeToHere = System.currentTimeMillis();
				//System.out.println("averageAlgorithm.runAlgorithm: Time since last before average algo "+(timeToHere - timeBeforeAlgo));
				timeBeforeAlgo = timeToHere;
				runSimulationUpdateSampling();
				long timeAfterAlgo = System.currentTimeMillis();
				//System.out.println("averageAlgorithm.runAlgorithm: Time for average algo "+(timeAfterAlgo - timeBeforeAlgo));
				loops++;
				finishedLoops = loops;
				addMessage("Number of finished iterations "+loops);
				//if(getStopPushed())
				//{
				//tr.terminate();
				//  throw new userError("Terminated on users request");
				//}
			} while((loops < maximumIterations) && (getStopHistory()));

			setDone();

			node = new TopNode(sd,
					vars,
					new SegmentPainterRegression());
			extracts = statCollectors;
		}
		catch(IntolerableException ie) {
			//forceTermination();
			throw ie;
		}  
		Runtime.getRuntime().runFinalization();
		//displayResultsGraphically();
	}

	public String getSurveillanceMessageForBar()
	{
		return "Done with "+ ((int) (100*getCurrent()/getLengthOfTask())) +"%";
	}

	public String getSurveillanceMessageForDump()
	{
		return getMessage();
	}

	public int getCurrent()
	{
		return finishedLoops;
	}

	public int getLengthOfTask()
	{
		return (int) maximumIterations;
	}

	public boolean isDone() 
	{
		return finishedLoops == maximumIterations;
	}
	
	//private SimpleExplorer sex = null;

	public void _displayResultsGraphically(boolean visible) {
		final SimpleExplorer se = new SimpleExplorer("Evolution of variables", 
				node, 
				null);
		
		se.pack();
		se.setVisible(visible);
		try { Thread.sleep(2000); } catch(Exception e) {}
		se.dispatchEvent(new WindowEvent(se, WindowEvent.WINDOW_CLOSING));
		//se.dispose();
	}

	public void displayResultsGraphically(boolean visible)
	{
		final SimpleExplorer se = new SimpleExplorer("Evolution of variables", 
				node, 
				null);
		//sex = se;
		/*
		se.addWindowListener(
				new WindowAdapter() 
				{
					public void windowClosed(WindowEvent e)
					{
						//System.out.println("onImageIllustrator - window closed");
						// Retaking the line comment below will cause the window to go
						// in some staled state.
						//se.dispose();
						//se.setVisible(false);
						System.gc(); 
					}
					public void windowClosing(WindowEvent e)
					{
						//System.out.println("onImageIllustrator - window closing");
						// Retaking the line below will cause the window to go
						// in some staled state.
						//se.dispose();
						System.gc();
					}
				});
		*/
		final SegmentPainter sp = new SegmentPainterOnce();
		JMenuBar mb = se.getJMenuBar();//new JMenuBar();
		//se.setJMenuBar(mb);
		JMenu menu = new JMenu("Report");
		mb.add(menu);
		JMenuItem menuItem = new JMenuItem("Html site");
		menu.add(menuItem);
		menuItem.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						Runnable r = new Runnable() {
							public void run() {
								AverageAlgorithm.this.generateReport();
							}
						};
						Thread t = new Thread(r);
						t.start();
					}
				}
				);

		Commons com = sd.getCommons();
		final CrossplotAdder cpa = node.getCrossplotsNode();
		final OverloadAdder ola = node.getOverloadsNode();
		JMenu plots = new JMenu("Plots");
		mb.add(plots);
		bp = new Biplot(se, false, extracts, cpa, sp);
		menuItem = new JMenuItem("biplot");
		plots.add(menuItem);
		menuItem.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						bp.setVisible(true);
					}
				});
		op = new OverlayPlot(se, false, extracts, ola, sp);
		menuItem = new JMenuItem("overlay plot");
		plots.add(menuItem);
		menuItem.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						op.setVisible(true);
					}
				});
		JMenu exports = new JMenu("Export");
		mb.add(exports);
		etv = new ExportTwoVariables(se, false, extracts);
		menuItem = new JMenuItem("Export 2 variables");
		exports.add(menuItem);
		menuItem.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						etv.setVisible(true);
					}
				}); 
		se.pack();
		se.setVisible(visible);
		//try { Thread.sleep(2000); } catch(Exception e) {}
		//se.dispatchEvent(new WindowEvent(se, WindowEvent.WINDOW_CLOSING));
	}

	public Manager[] getManagerArray() {
		return sd.getManagerArray();
	}
	
	//public void tearDown() {
		//sex.setVisible(false);
		//sex.dispose();
	//}

}