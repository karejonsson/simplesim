package se.modlab.simplesim.algorithms.presentation;

import se.modlab.generics.*;
import se.modlab.generics.gui.util.SimpleExplorerNode;
import se.modlab.generics.sstruct.values.*;

import java.util.Enumeration;
import java.util.Vector;

import javax.swing.tree.TreeNode;
import javax.swing.*;

import java.awt.Component;

import se.modlab.generics.sstruct.variables.*;

public class OneCrossplot implements SimpleExplorerNode
{
  private TreeNode parent;
  private JComponent cp;

  public OneCrossplot(JComponent cp, TreeNode parent)
  {
    this.parent = parent;
    this.cp = cp;
  }

  public Enumeration children()
  {
    return null;
  }

  public boolean getAllowsChildren()
  {
    return false;
  }

  public TreeNode getChildAt(int childIndex)
  {
    return null;
  }

  public int getChildCount()
  {
    return 0;
  }

  public int getIndex(TreeNode tn)
  {
    return -1;
  }

  public TreeNode getParent()
  {
    return parent;
  }

  public boolean isLeaf()
  {
    return false;
  }

  public Component getComponent()
  {
    return cp;
  }  

  public String toString()
  {
    return cp.toString();
  }

} 
