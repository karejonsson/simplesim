package se.modlab.simplesim.algorithms.presentation;

import se.modlab.generics.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import java.util.Enumeration;
import java.util.Vector;
import javax.swing.tree.TreeNode;
import javax.swing.JTextArea;
import java.awt.Component;
import se.modlab.simplesim.variables.*;

public class OneBagVariable extends VariableNode
{
  private String fullName = null;
  private Object bv;
  private SegmentPainter sp;

  public OneBagVariable(String _fullName,
                        Object _bv, 
                        TreeNode _parent, 
                        SegmentPainter _sp)
  {
	super(_parent);
    fullName = _fullName;
    bv = _bv; 
    sp = _sp;
    parent = _parent;
  }

  public static final String noSampleMessage = 
    "No sampling done for bag";

  public Component getComponent()
  {
    String msg = noSampleMessage;
    if(bv != null) msg = bv.toString();
    return new JTextArea(msg);
  }  

  public String toString()
  {
    return fullName;
  }

} 