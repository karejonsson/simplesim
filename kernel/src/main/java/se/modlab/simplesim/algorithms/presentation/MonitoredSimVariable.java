package se.modlab.simplesim.algorithms.presentation;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.jpegcoding.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.bshro.ifc.HierarchyObject;
import se.modlab.simplesim.algorithms.externalpainters.PlugInPainterFactory;
import se.modlab.simplesim.algorithms.regression.Segment;
import se.modlab.simplesim.tables.SpreadsheetWindow_excel;
import se.modlab.simplesim.variables.*;
import se.modlab.tables.editor.koders.Cell;

import javax.swing.*;
import javax.swing.tree.TreeNode;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.io.*;

public class MonitoredSimVariable extends JComponent //Panel
{

  protected MonitoredVariableExtracted mve;
  protected static final int margin = 20;
  protected double endTime;
  protected int w_pix;
  protected int h_pix;
  protected double min_val;
  protected double max_val;
  protected double h_val;
  protected SegmentPainter sp;
  private PlugInPainterFactory pips[];
  private TreeNode parent;

  public MonitoredSimVariable(MonitoredVariableExtracted _mve,
                              SegmentPainter _sp,
                              PlugInPainterFactory _pips[],
                              TreeNode _parent)
  {
    mve = _mve;
    sp = _sp;
    pips = _pips;
    parent = _parent;
    invalidate();
    addMouseMotionListener(new ToolTipMouseMotionListener());
    addMouseListener(new PopupListener());
  }

  public String getName()
  {
    return mve.getName();
  }

  public void paint(Graphics g)
  {
    paint(g, getSize());
  }

  protected double getValue(int i) 
    throws IntolerableException
  {
    sValue sv = mve.getValue(i);
    if(sv instanceof sBoolean)
    {
      return (sv.getBoolean().booleanValue()) ? 1.0 : 0.0;
    }
    return sv.getValue();
  }

  protected void drawText(Graphics g, Dimension d, int h_pix)
    throws IntolerableException
  {
    g.drawString("Min value: "+mve.getMinValue(), margin, h_pix+((int)(1.5*margin)));
    g.drawString("Max value: "+mve.getMaxValue(), margin, h_pix+((int)(2.5*margin)));

    g.drawString("Start value: "+mve.getValue(0), 
                  d.width/2, h_pix+((int)(1.5*margin)));
    g.drawString("End value: "+mve.getValue(mve.getSize()-1), 
                  d.width/2, h_pix+((int)(2.5*margin)));
   
    g.drawString("Start time: "+mve.getStartTimeTextual(), 
                 margin, h_pix+((int)(3.5*margin)));
    g.drawString("End time: "+mve.getEndTimeTextual(), 
                 d.width/2, h_pix+((int)(3.5*margin)));
  }

  protected void drawFrame(Graphics g, Dimension d, int h_pix)
    throws IntolerableException
  {
    // Frame upper
    g.drawLine((int) margin/2, (int) margin/2,
               (int) (d.width - margin/2), (int) margin/2); 
    // Frame lower
    g.drawLine((int) margin/2, h_pix+(int) margin/2,
               (int) (d.width - margin/2), h_pix+(int) margin/2); 
    // Frame left
    g.drawLine((int) margin/2, (int) margin/2,
               (int) margin/2, h_pix+(int) margin/2); 
    // Frame right
    g.drawLine((int) (d.width - margin/2), (int) margin/2,
               (int) (d.width - margin/2), h_pix+(int) margin/2); 
  }

  protected void drawLines(Graphics g, Dimension d, int w_pix, int h_pix)
    throws IntolerableException
  {
    String lastLineOfText = "";
    //Vertical lines
    if(mve.getNoLinesOnTimeAxis() != 0)
    {
      int intervals = mve.getNoLinesOnTimeAxis() + 1;
      double unitLines = ((double) (d.width - 2*margin)) / ((double) (intervals));
      for(int i = 1 ; i <= mve.getNoLinesOnTimeAxis() ; i++)
      {
        g.drawLine((int) (margin+i*unitLines), margin, 
                   (int) (margin+i*unitLines), h_pix);
      }
      lastLineOfText = "Timeaxis split in "+(intervals)+" even intervals.";
    }
    //Horizontal lines
    if(mve.getNoLinesOnVariableAxis() != 0)
    {
      int intervals = mve.getNoLinesOnVariableAxis() + 1;
      double unitLines = ((double) (h_pix - margin)) / ((double) (intervals));
      for(int i = 1 ; i <= mve.getNoLinesOnVariableAxis() ; i++)
      {
        g.drawLine(margin                  , (int) (margin+i*unitLines),
                   (int) (d.width - margin), (int) (margin+i*unitLines));
      }
      lastLineOfText += " "+mve.getName()+" axis split in "+(intervals)+
        " even intervals. Unit is "+
        ((mve.getMaxValue()-mve.getMinValue())/((double) (intervals)))+":";
    }
    if((mve.getNoLinesOnTimeAxis() != 0) || (mve.getNoLinesOnVariableAxis() != 0)) 
    {
      g.drawString("Grid: "+lastLineOfText, margin, h_pix+((int)(4.5*margin)));
    }
  }

  public void paint(Graphics g, Dimension d)
  {
    try
    {
      min_val = mve.getMinValue();
      max_val = mve.getMaxValue();
      h_val = max_val - min_val;
      int linesBelow = 4;
      if((mve.getNoLinesOnTimeAxis() != 0) || (mve.getNoLinesOnVariableAxis() != 0)) 
      {
        linesBelow = 5;
      }
      h_pix = d.height-margin*linesBelow;
      w_pix = margin;
      double factor = h_pix / h_val;
      int size = mve.getSize();
      //System.out.println("size = "+size);
      if(mve.getSize() == 1) 
      {
        g.drawString("The initial value "+getValue(0)+" is never changed",
                     margin, margin);
        drawText(g, d, h_pix);
        return;
      }
      endTime = mve.getEndTime();

      drawFrame(g, d, h_pix);

      DrawingParameters dp = new DrawingParameters(w_pix, h_pix, min_val, h_val,
                                                   endTime, margin, mve);

      for(int i = 0 ; i < size-1 ; i++)
      {
        sp.paint(g, d, dp, i);
      }
      sp.paintLast(g, d, dp);
 
      drawText(g, d, h_pix);
      drawLines(g, d, w_pix, h_pix);
    }
    catch(IntolerableException ie)
    {
      UniversalTellUser.general(null, ie);
    }
    
  }

  protected void mouseMoved(MouseEvent e)
  {
	  try {
    if(mve.getSize() == 1) return;
    Dimension d = getSize();
    int w_pix = MonitoredSimVariable.margin;
    int x = e.getX();
    double time = (x - w_pix)*endTime/(d.width-2*w_pix);
    String sttext = mve.getStartTimeTextual();
    int colons = sttext.indexOf("::");
    if(colons != -1) sttext = sttext.substring(0, colons-1);
    long stlong = Commons.getTimeLongFromString(sttext);
    String ettext = mve.getEndTimeTextual();
    colons = ettext.indexOf("::");
    if(colons != -1) ettext = ettext.substring(0, colons-1);
    long etlong = Commons.getTimeLongFromString(ettext);
    long epochtime = stlong+((long) ((etlong-stlong)*time/endTime));
    String timeInChart = Commons.getTimeStringFromLong(epochtime);

    double min_val = mve.getMinValue();
    double max_val = mve.getMaxValue();
    double h_val = max_val - min_val;
    int h_pix = d.height-margin*4;
    int y = e.getY();

    double variable = min_val-(y - h_pix)*h_val/(h_pix-MonitoredSimVariable.margin);
    setToolTipText("time = "+timeInChart+", "+
                   mve.getName()+
                   " = "+variable);
	  }
	  catch(InternalError ie) {
		  setToolTipText("");
	  }
  }

  private class ToolTipMouseMotionListener implements MouseMotionListener
  {

    private int i = 0;

    public void mouseDragged(MouseEvent e)
    {
    }

    public void mouseMoved(MouseEvent e)
    {
      MonitoredSimVariable.this.mouseMoved(e);
    }

  }


  private class PopupListener extends MouseAdapter 
  {
    
    public PopupListener()
    {
    }

    public void mousePressed(MouseEvent e) 
    {
      maybeShowPopup(e);
    }

    public void mouseReleased(MouseEvent e) 
    {
      maybeShowPopup(e);
    }

    private void maybeShowPopup(MouseEvent e) 
    {
      if(mve.getSize() == 1) return;
      if(!e.isPopupTrigger()) return;
      JPopupMenu popup = MonitoredSimVariable.this.getPopup();
      if(popup != null) popup.show(e.getComponent(), e.getX(), e.getY());
    }

  }

  protected void setTimeAxisLines(int lines)
  {
    mve.setNoLinesOnTimeAxis(lines);
    //timeLines = lines;
    repaint();
  }

  protected void setVariableAxisLines(int lines)
  {
    mve.setNoLinesOnVariableAxis(lines);
    //variableLines = lines;
    repaint();
  }
  /*
   *     int rows = table.size();
    SpreadsheetWindow_excel frame = new SpreadsheetWindow_excel();
    frame.newTableModel(rows, columns);
    //System.out.println("rows "+rows+", columns "+columns);
    for(int i = 0 ; i < rows ; i++)
    {
      Vector<Cell> row = table.elementAt(i);
      for(int ii = 0 ; ii < columns ; ii++)
      {
        
        Cell entry = row.elementAt(ii);
        //System.out.println("row "+i+", column "+ii+", value "+entry+", rows "+rows+", columns "+columns);
        if(entry != null) frame.setCellAt(entry, i, ii);
      }
    }
    frame.pack();
    frame.setVisible(true);

   */
  protected void exportToTable() {
	  final long epochStartDate = mve.getEpochStartTime();
	  final long timeUnit = mve.getTimeUnit();
	  final int rows = mve.getSize();
	  SpreadsheetWindow_excel excel = new SpreadsheetWindow_excel();
	  //System.out.println("MonitoredSimVairable exportToTable 1");
	  try {
		  Object oval = mve.getGeneralizedValue(0);
		  if(oval instanceof Segment) {
			  //System.out.println("MonitoredSimVairable exportToTable Segment "+oval);
			  excel.newTableModel(rows+1, 6);
		  }
		  else {
			  //System.out.println("MonitoredSimVairable exportToTable Not Segment "+oval);
			  excel.newTableModel(rows+1, 2);
		  }
		  excel.getCellAt(0, 0).setValue("Date");
		  excel.getCellAt(0, 1).setValue("Average");
		  if(oval instanceof Segment) {
			  excel.getCellAt(0, 2).setValue("Minumum");
			  excel.getCellAt(0, 3).setValue("Maximum");
			  excel.getCellAt(0, 4).setValue("-1 stddev");
			  excel.getCellAt(0, 5).setValue("+1 stddev");
		  }
	  }
	  catch(IntolerableException ie) {
		  ie.printStackTrace();
		  //System.out.println("MonitoredSimVairable exportToTable Exception "+ie);
	  }
	  //System.out.println("rows "+rows+", columns "+columns);
	  for(int i = 0 ; i < rows ; i++)
	  {
		  /*
		  sValue val = null;
		  try {
			  val = mve.getValue(i);
		  }
		  catch(intolerableException ie) {
			  continue;
		  }*/
		  Cell date = excel.getCellAt(i+1, 0);

		  Object oval = null;
		  try {
			  oval = mve.getGeneralizedValue(i);
		  }
		  catch(IntolerableException ie) {
			  continue;
		  }

		  if(oval instanceof Segment) {
			  Segment val = (Segment) oval;
			  long momentsEpochTime = epochStartDate + Math.round(timeUnit * i * (mve.getEndTime() / rows));
			  date.setValue(getDateAsString(momentsEpochTime));
			  
			  try {
				  Cell value = excel.getCellAt(i+1, 1);
				  value.setValue(val.getAverage());
				  value = excel.getCellAt(i+1, 2);
				  value.setValue(val.getMinimum());
				  value = excel.getCellAt(i+1, 3);
				  value.setValue(val.getMaximum());
				  value = excel.getCellAt(i+1, 4);
				  value.setValue(val.getAverage() - val.getDeviation());
				  value = excel.getCellAt(i+1, 5);
				  value.setValue(val.getAverage() + val.getDeviation());
			  }
			  catch(IntolerableException ie) {
				  continue;
			  }
		  }
		  
		  if(oval instanceof sValue) {
			  sValue val = (sValue) oval;
			  long momentsEpochTime = epochStartDate + Math.round(timeUnit * val.getDouble());
			  date.setValue(getDateAsString(momentsEpochTime));

			  Cell value = excel.getCellAt(i+1, 1);
			  value.setValue(val.getDouble());
		  }
		  
		  
	  }
	  JFrame frame = SpreadsheetWindow_excel.getFrame("", excel);
	  frame.pack();
	  frame.setVisible(true);
  }  

  protected void createJPG()
  {
    try
    {
      JFileChooser fc = new JFileChooser(HierarchyObject.getReferenceFilePath());
      fc.setDialogTitle("Point to the file. (With extension .jpg)");
      fc.setDialogType(JFileChooser.SAVE_DIALOG);
      fc.showSaveDialog(UniversalTellUser.getFrame(this));
      File selFile = fc.getSelectedFile();
      if(selFile == null) return;
      String path = selFile.getAbsolutePath();
      if(path == null) return;
      if(!(path.endsWith(".jpg")))
      {
        JOptionPane.showMessageDialog(
          UniversalTellUser.getFrame(this), 
          "The filename must end with .jpg", 
          "File not valid", 
          JOptionPane.ERROR_MESSAGE);
        return;
      }
      Dimension d = getSize();
      BufferedImage image = new BufferedImage(d.width, 
                                              d.height, 
                                              BufferedImage.TYPE_USHORT_GRAY);
      Graphics2D graphics = image.createGraphics();
      graphics.setPaint(Color.BLACK);
      graphics.setBackground(Color.WHITE);
      graphics.clearRect(0, 0, d.width, d.height);
      paint(graphics, d);
      FileOutputStream fos = new FileOutputStream(selFile);
      JpegEncoder jpg = new JpegEncoder(image, 50, fos);
      jpg.Compress();
      fos.close();
    }
    catch(FileNotFoundException fnf)
    {
      UniversalTellUser.general(UniversalTellUser.getFrame(this), new SystemError("File not found", fnf));
    }
    catch(IOException ioe)
    {
      UniversalTellUser.general(UniversalTellUser.getFrame(this), new SystemError("I/O error", ioe));
    }
    catch(Exception e)
    {
      UniversalTellUser.general(UniversalTellUser.getFrame(this), 
                                new UnclassedError("monitoredSimVariable - jpg-generation", e));
    }
  }

  protected JPopupMenu getPopup()
  {
    if(mve.getSize() == 1) return null;;
    JPopupMenu popup = new JPopupMenu();
    JMenu menu = new JMenu("Lines on timeaxis");
    popup.add(menu);
    for(int i = 0 ; i < 20 ; i++)
    {
      JMenuItem menuItem = new JMenuItem(""+i);
      final int _i = i;
      menuItem.addActionListener(
        new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
            MonitoredSimVariable.this.setTimeAxisLines(_i);;
          }
        });
      menu.add(menuItem);
    }
    menu = new JMenu("Lines on "+MonitoredSimVariable.this.mve.getName()+
                     " axis");
    popup.add(menu);
    for(int i = 0 ; i < 20 ; i++)
    {
      JMenuItem menuItem = new JMenuItem(""+i);
      final int _i = i;
      menuItem.addActionListener(
        new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
            MonitoredSimVariable.this.setVariableAxisLines(_i);;
          }
        });
      menu.add(menuItem);
    }
    JMenuItem menuItem = new JMenuItem("Create .jpg-file");
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          MonitoredSimVariable.this.createJPG();
        }
      });
    popup.add(menuItem);
    menuItem = new JMenuItem("Export to table");
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          MonitoredSimVariable.this.exportToTable();
        }
      });
    popup.add(menuItem);
    /*
    if(pips != null) {
    	for(int i = 0 ; i < pips.length ; i++) {
    		final int _i = i;
		    menuItem = new JMenuItem("Add painted by "+pips[i].getName());
		    menuItem.addActionListener(
		      new ActionListener()
		      {
		        public void actionPerformed(ActionEvent e)
		        {
	        		System.out.println("The type is "+parent.getClass().getName());
		        	if(parent instanceof VariableNode) {
		        		VariableNode vn = (VariableNode) parent;
		        		vn.addChild(new OneExternallyPaintedVariable(mve.getName(), parent, pips[_i].getPainter()));
		        	}
		        	else {
		        		//System.out.println("The type is "+parent.getClass().getName());
		        	}
		        	//System.out.println("NISSE");
		        } 
		      });
		    popup.add(menuItem);
    	}
    }
    */
    return popup;
  }

  /*
  private void setCursorCrossHair()
  {
    setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
  }

  private void setCursorDefault()
  {
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }
  */
  
  public final static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
  
  public static String getDateAsString(long millisSinceEpoch) {
	  Date d = new Date(millisSinceEpoch);
	  return sdf.format(d);
  }
  
  public static void main(String arg[]) {
	  long now = System.currentTimeMillis();
	  System.out.println("Now = "+now);
	  Date d = new Date(now);
	  System.out.println("long -> toString -> "+d.toString());
	  String datepattern = "yyyy-MM-dd HH:mm:ss.SSS";
	  SimpleDateFormat sdf = new SimpleDateFormat(datepattern);
	  System.out.println("Datum -> "+sdf.format(d));
  }

}
