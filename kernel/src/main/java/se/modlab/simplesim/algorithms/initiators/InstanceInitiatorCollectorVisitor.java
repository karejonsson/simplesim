package se.modlab.simplesim.algorithms.initiators;

import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;
import se.modlab.simplesim.events.*;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.evaluables.*;
import java.util.*;
import se.modlab.simplesim.scoping.*;
import se.modlab.simplesim.references.*;

public class InstanceInitiatorCollectorVisitor extends InstanceVisitorAdapter
{

  private Vector initiators = new Vector();
  private Scope s;
  private Stack names = new Stack();
  private boolean fromFile = false;

  public InstanceInitiatorCollectorVisitor(Scope _s)
  {
    s = _s;
  }
 
  public Initiator[] getInitiators()
  {
    Initiator itrs[] = new Initiator[initiators.size()];
    for(int i = 0 ; i < initiators.size() ; i++)
    {
      itrs[i] = (Initiator) initiators.elementAt(i);
    }
    return itrs;
  }

  public String getFullName()
  {
    StringBuffer sb = new StringBuffer();
    for(int i = 0 ; i < names.size() ; i++)
    {
      sb.append(names.elementAt(i).toString());
    }
    return sb.toString();
  }

  public void visit(BooleanVariable inst) 
    throws IntolerableException 
  {
    if(fromFile)
    {
      boolean b = inst.getValue().getBoolean().booleanValue();
      initiators.addElement(
        new BooleanInitiator(inst, 
                             s, 
                             new LogicalExpressionBoolean(b), 
                             getFullName()+inst.getName()));
      return;
    }
    //boolean b = inst.getValue().getBoolean().booleanValue();
    initiators.addElement(new BooleanInitiator(inst, s, null, getFullName()+inst.getName()));
  }
 
  public void visit(BooleanConst inst) 
    throws IntolerableException 
  {
    throw new InternalError(
      "Visitor instanceInitiatorCollectorVisitor fell on Boolean const. \n"+
      "Composite instance cannot contain constant");
  }
 
  public void visit(SimBooleanMonitoredVariable inst) 
    throws IntolerableException 
  {
    if(fromFile)
    {
      boolean b = inst.getValue().getBoolean().booleanValue();
      initiators.addElement(
        new BooleanInitiator(inst, 
                             s, 
                             new LogicalExpressionBoolean(b), 
                             getFullName()+inst.getName()));
      return;
    }
    //boolean b = inst.getValue().getBoolean().booleanValue();
    initiators.addElement(new BooleanInitiator(inst, s, null, getFullName()+inst.getName()));
  }
 
  public void visit(DoubleVariable inst) 
    throws IntolerableException 
  {
    if(fromFile)
    {
      double d = inst.getValue().getValue();
      initiators.addElement(
        new ArithmeticInitiator(inst, 
                             s, 
                             new ArithmeticEvaluableDouble(d), 
                             getFullName()+inst.getName()));
      return;
    }
    initiators.addElement(new ArithmeticInitiator(inst, s, null, getFullName()+inst.getName()));
  }
 
  public void visit(DoubleConst inst) 
    throws IntolerableException 
  {
    throw new InternalError(
      "Visitor instanceInitiatorCollectorVisitor fell on Double const. \n"+
      "Composite instance cannot contain constant");
  }
 
  public void visit(SimDoubleMonitoredVariable inst) 
    throws IntolerableException 
  {
    if(fromFile)
    {
      double d = inst.getValue().getValue();
      initiators.addElement(
        new ArithmeticInitiator(inst, 
                             s, 
                             new ArithmeticEvaluableDouble(d), 
                             getFullName()+inst.getName()));
      return;
    }
    initiators.addElement(new ArithmeticInitiator(inst, s, null, getFullName()+inst.getName()));
  }
 
  public void visit(LongVariable inst) 
    throws IntolerableException 
  {
    if(fromFile)
    {
      long l = inst.getValue().getLong().longValue();
      initiators.addElement(
        new ArithmeticInitiator(inst, 
                             s, 
                             new ArithmeticEvaluableLong(l), 
                             getFullName()+inst.getName()));
      return;
    }
    initiators.addElement(new ArithmeticInitiator(inst, s, null, getFullName()+inst.getName()));
  }
 
  public void visit(LongConst inst) 
    throws IntolerableException 
  {
    throw new InternalError(
      "Visitor instanceInitiatorCollectorVisitor fell on Long const. \n"+
      "Composite instance cannot contain constant");
  }
 
  public void visit(SimLongMonitoredVariable inst) 
    throws IntolerableException 
  {
    if(fromFile)
    {
      long l = inst.getValue().getLong().longValue();
      initiators.addElement(
        new ArithmeticInitiator(inst, 
                             s, 
                             new ArithmeticEvaluableLong(l), 
                             getFullName()+inst.getName()));
      return;
    }
    initiators.addElement(new ArithmeticInitiator(inst, s, null, getFullName()+inst.getName()));
  }
 
  public void visit(ComplexVariable inst) 
    throws IntolerableException 
  {
    names.push(inst.getName());
    traverse(inst);
    names.pop();
  }

  public void visit(VariableVector inst) 
    throws IntolerableException 
  {
    names.push(inst.getName()+".");
    traverse(inst);
    names.pop();
  }

  public void visit(SimVariableVector inst) 
    throws IntolerableException 
  {
    names.push(inst.getName()+".");
    traverse(inst);
    names.pop();
  }

  public void visit(VariableVectorFromFile inst) 
    throws IntolerableException 
  {
    fromFile = true;
    names.push(inst.getName()+".");
    traverse(inst);
    names.pop();
    fromFile = false;
  }

  public void visit(SimVectorVariableFromFile inst) 
    throws IntolerableException 
  {
    fromFile = true;
    names.push(inst.getName()+".");
    traverse(inst);
    names.pop();
    fromFile = false;
  }

  public void visit(SimMonitoredVectorVariableFromFile inst) 
    throws IntolerableException 
  {
    fromFile = true;
    names.push(inst.getName());
    traverse(inst);
    names.pop();
    fromFile = false;
  }

  public void visit(ProcedureRepeating inst) 
    throws IntolerableException 
  {
    initiators.addElement(new QueableInitiator(inst));
  }

  public void visit(ProcedureSingle inst) 
    throws IntolerableException 
  {
    initiators.addElement(new QueableInitiator(inst));
  }

  public void visit(ProcedureTrigged inst) 
    throws IntolerableException 
  {
    initiators.addElement(new QueableInitiator(inst));
  }

  public void visit(ProcedureActor inst) 
    throws IntolerableException 
  {
    initiators.addElement(new QueableInitiator(inst));
  }

  public void visit(TypedQueuableReference inst) 
    throws IntolerableException 
  {
    initiators.addElement(
      new QueuableReferenceInitiator(
        inst, s, null, getFullName()+inst.getName()));
  }

  public void visit(UntypedQueuableReference inst) 
    throws IntolerableException 
  {
    initiators.addElement(
      new QueuableReferenceInitiator(
        inst, s, null, getFullName()+inst.getName()));
  }

  public void visit(TypedStructReference inst) 
    throws IntolerableException 
  {
    initiators.addElement(
      new StructReferenceInitiator(
        inst, s, null, getFullName()+inst.getName()));
  }

  public void visit(UntypedStructReference inst) 
    throws IntolerableException 
  {
    initiators.addElement(
      new StructReferenceInitiator(
        inst, s, null, getFullName()+inst.getName()));
  }

  public void visit(SimVariableVectorExported inst) 
    throws IntolerableException 
  {
  }

  public void visit(SimComplexVariableExported inst) 
    throws IntolerableException 
  {
  }

  protected void traverse(VariableVectorFromFile inst)
    throws IntolerableException
  {
    fromFile = true;
    //System.out.println("instanceVisitorAdapter traverse SimVariableVector "+inst.getName());
    for(int i = 0 ; i < inst.getLength() ; i++)
    {
      VariableInstance _inst = inst.getVectorElement(i);
      //names.push("["+i+"].");
      _inst.accept(this);
      //names.pop();
    }
  }

  public void visit(ScopeReference inst) 
    throws IntolerableException 
  {
  }

}