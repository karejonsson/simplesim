package se.modlab.simplesim.algorithms.bruteforcemax;

import se.modlab.generics.files.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.simplesim.algorithms.presentation.TopNode;
import se.modlab.simplesim.algorithms.setup.*;
import se.modlab.simplesim.algorithms.initiators.*;
import se.modlab.simplesim.algorithms.share.*;
import java.util.*;

public class BruteForcemaxAlgorithm implements Algorithm
{

	private SimData sd = null;
	private int valueCombinations_initially = 1;
	private int valueCombinations = 1;  
	private ArithmeticEvaluable ae_reward;
	private int keep;
	private LogicalExpression le_accept;
	private ValuesToExamine ortogonals[];
	private AlgorithmBase subalgo;
	private boolean wasStopped = false;

	public BruteForcemaxAlgorithm(SimData _sd,
			ArithmeticEvaluable _ae_reward,
			LogicalExpression _le_accept,
			ArithmeticEvaluable _ae_keep,
			ValuesToExamine _ortogonals[],
			AlgorithmBase _subalgo)
	throws IntolerableException
	{
		sd = _sd;
		ae_reward = _ae_reward;
		Long l = _ae_keep.evaluate(sd.getGlobalScope()).getLong();
		if(l == null)
		{
			throw new UserCompiletimeError(
					"Number of results to keep must evaluate to integer value.");
		}
		keep = (int) l.longValue();
		le_accept = _le_accept;
		ortogonals = _ortogonals;
		subalgo = _subalgo;
		for(int i = 0 ; i < ortogonals.length ; i++)
		{
			valueCombinations_initially *= ortogonals[i].getNumberOfValuesToExamine();
		}
	}

	  public SimData getSimData() {
		  return sd;
	  }

		public TopNode getTopnode() {
			return subalgo.getTopnode();
		}


	protected void setDone()
	{  
		valueCombinations = 0;
	}

	private boolean incrementOrtogonalsAndReportDone()
	{
		if(valueCombinations < 0) return true;
		int i = 0;
		while((i < ortogonals.length) && 
				(ortogonals[i].incrementAndReportFlipover()))
		{
			i++;
		}
		return !(i < ortogonals.length);
	}

	private Initiator getRightResetter(Vector<Initiator> orts, Initiator ir)
	{
		for(int i = 0 ; i < orts.size() ; i++)
		{
			Initiator _ir = orts.elementAt(i);
			if(ir.getInstance() == _ir.getInstance())
			{
				orts.removeElementAt(i);
				return _ir; // Use other resetter.
			}
		}
		return ir; // Use old resetter
	}

	private String getExaminedValueReport()
	{
		StringBuffer sb = new StringBuffer();
		sb.append(ortogonals[0].toString());
		for(int i = 1 ; i < ortogonals.length ; i++)
		{
			sb.append("\n"+ortogonals[i].toString());
		}
		return sb.toString();
	}

	public void stop()
	{
		//System.out.println("Brute force max stop");
		subalgo.stop();
		setDone();
		wasStopped = true;
	}
	
	public void setupScenario(final Hashtable<VariableInstance, Manager> managermap) throws IntolerableException
	{
		subalgo.setupScenario(managermap);
	}
	
	  public Scope getGlobalScope() {
		  return subalgo.getGlobalScope();
	  }


	public void runAlgorithm()
	throws IntolerableException
	{
		Vector resetters = new Vector();
		try
		{
			subalgo.setupScenario();

			// Ensure it can be evaluated
			ae_reward.evaluate(sd.getGlobalScope()); 

			// Initiations
			Vector orts = new Vector();
			for(int i = 0 ; i < ortogonals.length ; i++)
			{
				ortogonals[i].verificationBeforeAlgorithmExecution();
				valueCombinations *= ortogonals[i].getNumberOfValuesToExamine();
				orts.addElement(ortogonals[i]);
			}
			//valueCombinations_initially = valueCombinations;

			Vector subalgoResetters = sd.getAllPermanentResetters();
			for(int i = 0 ; i < subalgoResetters.size() ; i++)
			{
				Initiator ir = (Initiator) subalgoResetters.elementAt(i);
				resetters.addElement(getRightResetter(orts, ir));
			}
			sd.setAllPermanentResetters(resetters);

			if(orts.size() != 0)
			{
				// All resetters has not been replaced. Not reasonable. The 
				// user of this algorithm is examining values for non existing 
				// variables.
				VariableInstance si = ((Initiator) orts.elementAt(0)).getInstance();
				throw new UserCompiletimeError(
						"Examination values for nonexisting variable "+si+" and \n"+
				"there may be more.");
			}
		}
		catch(IntolerableException ie)
		{
			setDone();
			throw ie;
		}  
		catch(Throwable t)
		{
			setDone();
			throw new UnclassedError("Unexpected error bruteforcemax (1)", t);
		}  


		try
		{
			subalgo.addMessage( 
					"Algorithm maximize starts at "+Commons.getTimeStringForExecutionTime()+
					". The amount of \nvalue combinations is "+valueCombinations);

			subalgo.addMessage("Starts simulation.");

			subalgo.addMessage(getExaminedValueReport()+"\n"+
					"Remaining combinations "+(valueCombinations--));

			ResultTable rt = new ResultTable(ortogonals.length, 
					new RerunAlgo((Algorithm)subalgo, resetters),
					sd, keep);

			subalgo.runAlgorithm();
			subalgo.setAverageEndValue();

			if(le_accept.evaluate(sd.getGlobalScope()))
			{
				sValue best = ae_reward.evaluate(sd.getGlobalScope()); 
				rt.addRow(new ResultRow(ortogonals, best));
			}

			do 
			{
				subalgo.addMessage(getExaminedValueReport()+"\n"+
						"Remaining combinations "+(valueCombinations--));
				subalgo.runAlgorithm();
				subalgo.setAverageEndValue();
				if(le_accept.evaluate(sd.getGlobalScope()))
				{
					sValue best = ae_reward.evaluate(sd.getGlobalScope()); 
					rt.addRow(new ResultRow(ortogonals, best));
				}


			} while((!incrementOrtogonalsAndReportDone()) && (subalgo.getStopHistory()));

			setDone();
			if(!wasStopped) ResultTable.flash(rt);
		}
		catch(IntolerableException ie)
		{
			setDone();
			throw ie;
		}  
		catch(Throwable t)
		{
			setDone();
			throw new UnclassedError("Unexpected error bruteforcemax (2)", t);
		}  
		//displayResultsGraphically();
	}

	public FileCollector[] getCollectors()
	{
		return subalgo.getCollectors();
	}

	public void displayResultsGraphically(boolean visible)
	{
	}

	public String getSurveillanceMessageForBar()
	{
		return "Done with "+ ((int) (100*getCurrent() / getLengthOfTask())) +"%";
	}

	public String getSurveillanceMessageForDump()
	{
		return subalgo.getMessage();
	}

	public int getCurrent()
	{
		//System.out.println("Brute.getCurrent "+(valueCombinations_initially - valueCombinations));
		return valueCombinations_initially - valueCombinations;
	}

	public int getLengthOfTask()
	{
		//System.out.println("Brute.getLengthOfTask() "+valueCombinations_initially);
		return valueCombinations_initially;
	}

	public boolean isDone() 
	{
		return valueCombinations == 0;
	}

	public String getName()
	{
		return subalgo.getName();
	}

	public Manager[] getManagerArray() {
		return sd.getManagerArray();
	}

}