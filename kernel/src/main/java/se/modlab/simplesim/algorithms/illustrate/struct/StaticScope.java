package se.modlab.simplesim.algorithms.illustrate.struct;

import se.modlab.generics.sstruct.strings.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.simplesim.variables.*;

public class StaticScope extends Scope
{

  private static CreateExportedVersionVisitor cevv = 
    new CreateStaticVersionVisitor();;

  public StaticScope(Scope s) {
    super(s);
  }
 
/* 
  private static createExportedVersionVisitor cevv = 
    null;//new createStaticVersionVisitor();;

  public staticScope(scope s)
  {
    super(s);
    try
    {
      cevv = createExportedVersionVisitor.getInstance();
    }
    catch(intolerableException ie)
    {
    }
  }
 */

  public Variable getVariable(String s)
  {
    if(cevv == null) return null;
    try {
      Variable v = super.getVariable(s);
      if(v == null) return null;
      //System.out.println("staticScope - getVariable - not null for "+s);
      v.accept(cevv);
      return (Variable) cevv.getExportedVersion();
    }
    catch(IntolerableException ie) {
    }
    return null;
  }

  public VariableInstance getComplexInstance(String s)
  {
    if(cevv == null) return null;
    try
    {
      VariableInstance vi = super.getComplexInstance(s);
    //(new Throwable()).printStackTrace();
    //System.out.println("staticScope: Class "+vi.getClass().getName());
    //System.exit(0);
      if(vi == null) return null;
      //System.out.println("staticScope - getComplexInstance - not null for "+s);
      vi.accept(cevv);
      return cevv.getExportedVersion();
    }
    catch(IntolerableException ie)
    {
    }
    return null;
  }
  
}
