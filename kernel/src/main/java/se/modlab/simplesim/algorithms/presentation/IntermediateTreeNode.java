package se.modlab.simplesim.algorithms.presentation;

import java.util.*;
import java.awt.*;
import javax.swing.tree.*;
import se.modlab.generics.gui.util.*;
import org.xml.sax.*;
import javax.swing.*;

public class IntermediateTreeNode extends VariableNode
{
   private Vector children = new Vector();
   protected String name;
   //private SimpleExplorerNode parent;
   protected String defaultText = null;

   public IntermediateTreeNode(SimpleExplorerNode _parent,
                               String _name)
   {
     super(_parent);
     name = _name;
     defaultText = "Nothing to show here.";
   }

   public void setParent(SimpleExplorerNode n) {
      parent = n;
   }

   public void addChild(SimpleExplorerNode n)
   {
      if(n == null) return;
      children.addElement(n);
      //n.setParent(this);
   }

   public int getChildCount()
   {
      return children.size();
   }

   public TreeNode getChildAt(int child)
   {
      if(child >= children.size()) return null;
      return (SimpleExplorerNode) children.elementAt(child);
   }

   public String getName()
   {
      return name;
   }

   public void setName(String name)
   {
      this.name = name;
   }

   public boolean hasChildren()
   {
      return children.size() == 0;
   }

   public Enumeration children() 
   {
      return children.elements();
   }

   public boolean getAllowsChildren() 
   {
      return true;
   }

   public int getIndex(TreeNode n) 
    {
      return children.indexOf(n);
   }

   public TreeNode getParent()  
   {
      return parent;
   }

   public boolean isLeaf() 
   {
      return (children.size() == 0);
   }

   public String toString() 
   {
      return name;
   }

   public Component getComponent()
   { 
     JTextArea ta = new JTextArea();
     ta.setEditable(false);
     ta.setText(defaultText);
     return ta;
   }
}
