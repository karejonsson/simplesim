package se.modlab.simplesim.algorithms.presentation;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import java.awt.*;

public interface SegmentPainter
{

  public void paint(Graphics g, 
                    Dimension d, 
                    DrawingParameters dp,
                    int index)
    throws IntolerableException;

  public void paintLast(Graphics g, 
                        Dimension d, 
                        DrawingParameters dp)
    throws IntolerableException;

}