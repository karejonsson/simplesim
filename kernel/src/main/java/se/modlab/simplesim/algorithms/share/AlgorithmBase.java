package se.modlab.simplesim.algorithms.share;

import se.modlab.generics.*;
import se.modlab.generics.files.*;
import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.jpegcoding.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.bshro.ifc.HierarchyObject;
import se.modlab.simplesim.variables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.simplesim.events.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;

import javax.swing.*;

import java.util.*;
import java.io.*;

import se.modlab.simplesim.algorithms.presentation.*;
import se.modlab.simplesim.algorithms.setup.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.simplesim.scoping.*;

public abstract class AlgorithmBase
{

	//protected Vector queue = new Vector();
	//private Vector triggered = null;
	public SimData sd = null;
	protected TopNode node = null;
	protected MonitoredVariableExtracted extracts[];
	protected Vector trigged = null;
	//private textualReport tr = null;
	private boolean done = false;
	private StringBuffer sb = null;
	//private static int antal = 0;
	protected SimulationEventQueue eq = null;
	private boolean scenarioIsSetup = false;
	protected Biplot bp = null;
	protected ExportTwoVariables etv = null;
	protected OverlayPlot op = null;
	boolean stopHistoryNotTroublesome = true;

	private Comparator compare = new EventComparator();

	public AlgorithmBase(SimData sd)
	{
		this.sd = sd;
		eq = sd.getQueue();
		//antal++;
		//System.out.println("Antal algorithm "+antal);
	}

	public TopNode getTopnode() {
		return node;
	}

	public boolean getStopHistory() {
		return stopHistoryNotTroublesome;
	}

	public void setupScenario()
			throws IntolerableException
			{
		setupScenario(null);
			}

	public void setupScenario(final Hashtable<VariableInstance, Manager> managermap)
			throws IntolerableException
			{
		if(scenarioIsSetup) {
			//System.out.println("algorithmBase: setup already done");
			return;
		}
		//System.out.println("algorithmBase: setup to be done now. -----------------------------------------------------------------------------------");
		//System.out.println("Setup scenario - start");
		SimScope gs = (SimScope) sd.getGlobalScope();
		Vector<Manager> managementors = sd.getAllManagers();
		//System.out.println("Det finns "+managementors.size()+" managerare.");
		for(int i = 0 ; i < managementors.size() ; i++) {
			Manager m = managementors.get(i);
			//System.out.println("Manager "+i+" "+m);
			m.initialize(gs, true, managermap);
		}
		ProcedureSingle ps = sd.getSettingsGeneratedStopper();
		if(ps != null) {
			sd.addQueuable(ps);
		}
		VariableInstance vis[] = gs.getAllInstances();
		SetFullPathNamesOnQueablesVisitor s = new SetFullPathNamesOnQueablesVisitor();
		for(int i = 0 ; i < vis.length ; i++) {
			vis[i].accept(s);
		}
		scenarioIsSetup = true;
		//System.out.println("Setup scenario - end");
		//System.out.println("algorithmBase: setup finished. -----------------------------------------------------------------------------------------");
			}

	public Scope getGlobalScope() {
		return sd.getGlobalScope();
	}

	public SimData getSimData() {
		return sd;
	}

	/*
  protected void finalize()
  {
    antal--;
    System.out.println("Antal algorithm "+antal);
  }
	 */

	public String getName()
	{
		return sd.getName();
	}

	public Commons getCommons()
	{
		return sd.getCommons();
	}

	public void stop() 
	{
		//System.out.println("algorithmsBase - stop");
		eq = null;
		setDone();
	}

	public abstract String getSurveillanceMessageForBar();
	public abstract String getSurveillanceMessageForDump();
	public abstract int getCurrent();
	public abstract int getLengthOfTask();

	public boolean isDone() 
	{
		return done;
	}

	protected void setDone()
	{  
		done = true;
	}

	public FileCollector[] getCollectors()
	{
		return sd.getCollectors();
	}

	protected void queueUp(SimulationEvent event)
			throws IntolerableException
			{
		eq.queueUp(event);
			}

	private void emptyQueue()
	{
		eq.removeAll();
	}

	protected SimulationEvent getFromQueue()
	{
		return eq.getFromQueue();
	}

	protected boolean queueEmpty()
	{
		return eq.empty();
	}

	/*
  protected void messageWindowUp(String frameMessage,
                                 int x, int y,
                                 String permanentMessage)
  {
    tr = new textualReport(frameMessage, x, y, sd.getCommons(), permanentMessage);
    tr.start();
    tr.addMessage("Starts simulation.");
  }
	 */

	public void addMessage(String msg)
	{
		if(sb == null) sb = new StringBuffer();
		sb.append(msg+"\n");
	}

	public String getMessage()
	{
		if(sb ==  null) return null;
		String msg = sb.toString();
		sb = null;
		return msg;
	}

	/*
  protected boolean getStopPushed()
  {
    return tr.getStopPushed();
  }

  protected void forceTermination()
  {
    if(tr != null) tr.forceTermination();
  }
	 */

	public final void prepareForExecution()
			throws IntolerableException
			{
		Commons com = sd.getCommons();
		try
		{
			com.resetSimTime();
			emptyQueue();
			Vector queables = sd.getQueuables();
			for(int i = 0 ; i < queables.size() ; i++)
			{
				Enqueuable q = (Enqueuable) queables.elementAt(i);
				q.reset();
				q.queueUp(eq);
			}
			trigged = sd.getTrigged();
			eq.setInitialTrigged(trigged);
		}
		catch(NullPointerException npe)
		{
			if(eq == null)
			{
				// Progress bar stop
				//done = true;
				throw new UserBreak();
			}
			//done = true;
			throw new UnclassedError(
					"(algorithm - prepareForExecution - NullPointerException)", npe);
		}
		catch(Throwable e)
		{
			com.setTextualStopReason("Internal error");
			throw new InternalProgrammingError(
					"(algorithm - prepareForExecution) \n"+
							e.getMessage()+"\nClass name of exception is "+e.getClass().getName()+
							"The toString gives:\n"+e.toString(), e);
		}
		//System.out.println("AlgorithmBase - Queue after prepareForExecution\n"+eq);
			}

	public void executeSimulation()
			throws IntolerableException
			{
		Commons com = sd.getCommons();
		stopHistoryNotTroublesome = true;
		int maxlen = 0;
		try {
			Vector ies = sd.getInitially();
			for(int i = 0 ; i < ies.size() ; i++) {
				InitiallyEvent ie = (InitiallyEvent) ies.elementAt(i);
				ie.runEvent();
			}
			while(!queueEmpty()) {
				SimulationEvent event = getFromQueue();    
				com.setCurrentSimTime(event.getSimTimeForNextEvent());
				//System.out.println("algorithmBase - To now run event "+event);
				event.runEvent();
				if(event.shallBeRequeued()) {
					queueUp(event);
					maxlen = Math.max(maxlen, eq.length());
				}
				for(int i = trigged.size() - 1 ; i >= 0 ; i--) {
					ProcedureTrigged triggedEvent = (ProcedureTrigged) trigged.elementAt(i);
					if(triggedEvent.shallRun()) {
						triggedEvent.runEvent();
					}
					if(!triggedEvent.shallBeRequeued()) {
						trigged.removeElement(triggedEvent);
					}
				}
			}
			com.setTextualStopReason("Queue ran empty");
		}
		catch(StopExceptionUserProgrammatic e) {
			com.setTextualStopReason(e.getMessage());
			//done = true;
			stopHistoryNotTroublesome = false;
		}
		catch(StopException e) {
			com.setTextualStopReason(e.getMessage());
		}
		catch(IntolerableException e) {
			com.setTextualStopReason(e.getMessage());
			//done = true;
			throw e;
		}
		catch(NullPointerException npe) {
			if(eq == null) {
				// Progress bar stop
				//done = true;
				throw new UserBreak();
			}
			//done = true;
			throw new UnclassedError(
					"(algorithm - executeSimulation - NullPointerException)", npe);
		}
		catch(Throwable e) {
			com.setTextualStopReason("Internal error");
			//done = true;
			throw new UnclassedError(
					"(algorithm - executeSimulation - kernel)", e);
		}
		finally {
			//System.out.println("algorithmBase.executeSimulation max len "+maxlen);
		}

		if(!stopHistoryNotTroublesome) return;

		try
		{
			FinallyEvent fe = sd.getFinally();
			if(fe != null) 
			{
				fe.runEvent();
			}
		}
		catch(StopExceptionUserProgrammatic e)
		{
			com.setTextualStopReason(e.getMessage());
		}
		catch(StopException e)
		{
			com.setTextualStopReason(e.getMessage());
		}
		catch(IntolerableException e)
		{
			com.setTextualStopReason(e.getMessage());
			//done = true;
			throw e;
		}
		catch(NullPointerException npe)
		{
			if(eq == null)
			{
				// Progress bar stop
				//done = true;
				throw new UserBreak();
			}
			//done = true;
			throw new UnclassedError(
					"(algorithm - executeSimulation - finally - NullPointerException)", npe);
		}
		catch(Throwable e)
		{
			com.setTextualStopReason("Internal error");
			//done = true;
			throw new UnclassedError(
					"(algorithm - executeSimulation - finally)", e);
		}
		//done = true;
			}

	protected void runInitiators()
			throws IntolerableException
			{
		sd.resetAllPermanentVariables();
			}

	protected void runEndValueSamplers()
			throws IntolerableException
			{
		sd.samplingtAllPermanentVariables();
			}

	protected final void runSimulation()
			throws IntolerableException
			{
		long before = System.currentTimeMillis();
		//System.out.println("se.modlab.simplesim.algorithms.share.algorithmBase.runSimulation 1 - "+before);
		runInitiators();
		long next = System.currentTimeMillis();
		//System.out.println("se.modlab.simplesim.algorithms.share.algorithmBase.runSimulation 2 - "+(next-before));
		before = next;
		prepareForExecution();
		next = System.currentTimeMillis();
		//System.out.println("se.modlab.simplesim.algorithms.share.algorithmBase.runSimulation 3 - "+(next-before));
		before = next;
		executeSimulation();
		next = System.currentTimeMillis();
		//System.out.println("se.modlab.simplesim.algorithms.share.algorithmBase.runSimulation 4 - "+(next-before));
		before = next;
		runEndValueSamplers();
		next = System.currentTimeMillis();
		//System.out.println("se.modlab.simplesim.algorithms.share.algorithmBase.runSimulation 5 - "+(next-before));
			}

	public abstract void runAlgorithm() 
			throws IntolerableException;

	public void setAverageEndValue() 
			throws IntolerableException
			{
		sd.setAveragesAllPermanentVariables();
			}

	public void run()
			throws IntolerableException
			{
		try
		{ 
			runAlgorithm();
			displayResultsGraphically(true);
		}
		catch(IntolerableException ie)
		{
			//ie.setCollectors(getCollectors());
			throw ie;
		}
		catch(OutOfMemoryError oome)
		{ 
			throw new SystemError(
					"Ran out of memory.\n\n"+
							"Hint 1. Are there large amounts of monitored variables?\n\n"+
							"Hint 2. Is there a large and monitored vector?\n",
							oome);
		}
		catch(Throwable e)
		{ 
			throw new UnclassedError("Algorithm - run", e);
			//throw new unclassedError("Algorithm - run", e, getCollectors());
		}
			}

	public void generateReport()
	{
		JFileChooser fc = new JFileChooser(HierarchyObject.getReferenceFilePath());
		fc.setDialogTitle("Point to directory under which to put the sites files!");
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fc.setDialogType(JFileChooser.SAVE_DIALOG);
		fc.showSaveDialog(null);
		File selFile = fc.getSelectedFile();
		if(selFile == null) return;
		String path = selFile.getAbsolutePath();
		if(path == null) return;

		//System.out.println("path = '"+path+"'");
		TextualReport tr = new TextualReport("Generating html report", 100, 200, null,
				"Generating report in directory "+path);
		tr.start();

		HtmlSiteGenerator hsg = new HtmlSiteGenerator();
		try
		{
			Variables vars = node.getVariablesNode();
			int children = vars.getChildCount();
			//variableComponentCollector vcc = new variableComponentCollector();
			MonitoredSimVariable mvs[] = vars.getStandardMonitoredVariablesComponents();
			for(int i = 0 ; i < mvs.length ; i++)
			{
				//OneVariable ovar = (OneVariable) vars.getChildAt(i);
				MonitoredSimVariable msv = mvs[i];
				Dimension d = new Dimension(700, 500);
				BufferedImage image = new BufferedImage(d.width, 
						d.height, 
						BufferedImage.TYPE_USHORT_GRAY);
				Graphics2D graphics = image.createGraphics();
				graphics.setPaint(Color.BLACK);
				graphics.setBackground(Color.WHITE);
				graphics.clearRect(0, 0, d.width, d.height);
				msv.paint(graphics, d);
				tr.addMessage("Generates Jpeg file for variable "+msv.getName());
				File f = new File(path+File.separatorChar+"var"+i+".jpg");
				FileOutputStream fos = new FileOutputStream(f);
				//GIFEncoder encoder = new GIFEncoder(image);
				//encoder.Write(fos);
				JpegEncoder jpg = new JpegEncoder(image, 50, fos);
				jpg.Compress();
				fos.close();
				hsg.addTimeGraphGifReference(msv.getName(), 
						/*path+File.separatorChar+*/"var"+i+".jpg");
			}

			Crossplots cps = node.getCrossplotsNode();
			children = cps.getChildCount();
			for(int i = 0 ; i < children ; i++)
			{
				OneCrossplot ocp = (OneCrossplot) cps.getChildAt(i);
				Crossplot cp = (Crossplot) ocp.getComponent();
				Dimension d = new Dimension(700, 500);
				BufferedImage image = new BufferedImage(d.width, 
						d.height, 
						BufferedImage.TYPE_USHORT_GRAY);
				Graphics2D graphics = image.createGraphics();
				graphics.setPaint(Color.BLACK);
				graphics.setBackground(Color.WHITE);
				graphics.clearRect(0, 0, d.width, d.height);
				cp.paint(graphics, d);
				tr.addMessage("Generates Jpeg file for crossplot "+cp.toString());
						File f = new File(path+File.separatorChar+"cp"+i+".jpg");
						FileOutputStream fos = new FileOutputStream(f);
						//GIFEncoder encoder = new GIFEncoder(image);
						//encoder.Write(fos);
						JpegEncoder jpg = new JpegEncoder(image, 50, fos);
						jpg.Compress();
						fos.close();
						hsg.addCrossplotGifReference(cp.toString(), 
								/*path+File.separatorChar+*/"cp"+i+".jpg");
			}

			Overloads ols = node.getOverloadsNode();
			children = ols.getChildCount();
			for(int i = 0 ; i < children ; i++)
			{
				OneOverload ool = (OneOverload) ols.getChildAt(i);
				Overload ol = (Overload) ool.getComponent();
				Dimension d = new Dimension(700, 500);
				BufferedImage image = new BufferedImage(d.width, 
						d.height, 
						BufferedImage.TYPE_USHORT_GRAY);
				Graphics2D graphics = image.createGraphics();
				graphics.setPaint(Color.BLACK);
				graphics.setBackground(Color.WHITE);
				graphics.clearRect(0, 0, d.width, d.height);
				ol.paint(graphics, d);
				tr.addMessage("Generates Jpeg file for crossplot "+ol.toString());
						File f = new File(path+File.separatorChar+"ol"+i+".jpg");
						FileOutputStream fos = new FileOutputStream(f);
						//GIFEncoder encoder = new GIFEncoder(image);
						//encoder.Write(fos);
						JpegEncoder jpg = new JpegEncoder(image, 50, fos);
						jpg.Compress();
						fos.close();
						hsg.addOverloadGifReference(ol.toString(), 
								/*path+File.separatorChar+*/"ol"+i+".jpg");
			}

			hsg.setCollectors(sd.getCollectors());
			hsg.generate(path+File.separatorChar+"site.html");
			tr.forceTermination();
			UniversalTellUser.info(null, "File "+path+File.separatorChar+"site.html created!");
		}
		catch(FileNotFoundException fnf)
		{
			tr.forceTermination();
			UniversalTellUser.general(null, new SystemError("File not found", fnf));
			//tellUser(fnf.getMessage());
		}
		catch(IOException ioe)
		{
			tr.forceTermination();
			UniversalTellUser.general(null, new SystemError("I/O error", ioe));
			//tellUser(ioe.getMessage());
		}
		catch(IntolerableException ie)
		{
			tr.forceTermination();
			UniversalTellUser.general(null, ie);
			//tellUser(ie.getMessage());
		}

	}

	public abstract void displayResultsGraphically(boolean visible);

}