package se.modlab.simplesim.algorithms.illustrate.share;

import java.awt.*;
import se.modlab.simplesim.events.*;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;

public interface Illustrator
{
   
  public void addStaticIllustrationUnit(IllustrationUnit iu);
  public void addDynamicIllustrationFactory(DynamicIllustrationFactory dif);
  public void init()
    throws IntolerableException;
  public void setGlobalScope(Scope s)
    throws IntolerableException;
  public void updated(SimulationEvent se);
  public void repaint();
  public void paint(Graphics g);
  public IntolerableException getException();

}

