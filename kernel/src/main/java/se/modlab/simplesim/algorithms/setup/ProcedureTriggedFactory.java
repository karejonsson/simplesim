package se.modlab.simplesim.algorithms.setup;

import java.util.Hashtable;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.evaluables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.simplesim.scoping.*;
import se.modlab.simplesim.events.*;

public class ProcedureTriggedFactory 
       implements SimVariableFactory, EnqueuableFactory
{

  private SimScope s;
  private String type;
  private Manager localScope[];
  private LogicalExpression le_execute;
  private ProgramBlock p;
  private LogicalExpression le_requeue;
  private SimData sd;
  private ScopeFactory sf;
  private boolean isSharp;

  public ProcedureTriggedFactory(
                          SimScope _s,
                          String _type,
                          Manager _localScope[],
                          LogicalExpression _le_execute,
                          ProgramBlock _p,
                          LogicalExpression _le_requeue,
                          SimData _sd,
                          ScopeFactory _sf,
                          boolean _isSharp)
  {
    s = _s;
    type = _type;
    localScope = _localScope;
    le_execute = _le_execute;
    p = _p;
    le_requeue = _le_requeue;
    sd = _sd;
    sf = _sf;
    isSharp = _isSharp;
  }
 
  public VariableInstance getInstance(String name, String filename, int line, int column, Hashtable<VariableInstance, Manager> managermap) 
    throws IntolerableException
  {
    return getInstance(name, false, filename, line, column, managermap);
  }

  public VariableInstance getInstance(String name, boolean monitor, String filename, int line, int column, Hashtable<VariableInstance, Manager> managermap) 
    throws IntolerableException
  {
    SimScope ls = (SimScope) sf.getInstance(s, s.getName()+"/"+name);
    for(int i = 0 ; i < localScope.length ; i++)
    {
      localScope[i].initialize(ls, isSharp, managermap);
    }
    ProcedureTrigged pr = 
      new ProcedureTrigged(name, this, filename, line, column, ls, le_execute, le_requeue,
                             p, sf);
    return pr;
  }

  public String getTypesName()
  {
    return type;
  }
 
  public String toString()
  {
    return "Procedure repeating factory for type "+type+" in scope "+s.getName();
  }

	public VariableInstance getInstance(String name, String filename, int line, int column)
	throws IntolerableException {
		System.out.println("Call to getInstance(s,s,i,i) i.e. addin null in class "+getClass().getName());
		return getInstance(name, filename, line, column, null);
	}
	public VariableInstance getInstance(String name, boolean monitor, String filename, int line, int column)
	throws IntolerableException {
		System.out.println("Call to getInstance(s,b,s,i,i) i.e. addin null in class "+getClass().getName());
		return getInstance(name, monitor, filename, line, column, null);
	}

}
