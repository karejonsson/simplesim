package se.modlab.simplesim.algorithms.externalpainters;

import java.awt.Container;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import se.modlab.generics.gui.util.SimpleExplorer;
import se.modlab.simplesim.algorithms.presentation.MonitoredSimVariable;

public class RightExplorerSideComponent extends JComponent {
	
	private PlugInPainter pip = null;
	
	public RightExplorerSideComponent(PlugInPainter _pip) {
		pip = _pip;		
	    for(Container p = this.getParent(); p != null; p = p.getParent()) 
	    {
	      if(p instanceof SimpleExplorer) 
	      {
	    	  ((SimpleExplorer) p).refresh();
	      }
	    }
	    addMouseListener(new PopupListener());
	}
	
	public void paint(Graphics g) {
		pip.paint(this, g);
	}
	
	public String getVariableName() {
		return pip.getVariableName();
	}

	public JPopupMenu getPopup()
	{
		return pip.getPopup(this);
	}

	  private class PopupListener extends MouseAdapter 
	  {
	    
	    public PopupListener()
	    {
	    }

	    public void mousePressed(MouseEvent e) 
	    {
	      maybeShowPopup(e);
	    }

	    public void mouseReleased(MouseEvent e) 
	    {
	      maybeShowPopup(e);
	    }

	    private void maybeShowPopup(MouseEvent e) 
	    {
	      if(pip == null) return;
	      if(!e.isPopupTrigger()) return;
	      JPopupMenu popup = RightExplorerSideComponent.this.getPopup();
	      if(popup != null) popup.show(e.getComponent(), e.getX(), e.getY());
	    }

	  }

}
