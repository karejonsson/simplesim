package se.modlab.simplesim.algorithms.presentation;

import javax.swing.*;

public interface CrossplotAdder
{
  public boolean addOneCrossplot(JComponent cp);
  public boolean removeOneCrossplot(JComponent cp);
}