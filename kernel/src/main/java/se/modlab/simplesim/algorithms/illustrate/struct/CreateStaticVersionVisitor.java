package se.modlab.simplesim.algorithms.illustrate.struct;

import java.lang.reflect.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.events.*;
import se.modlab.simplesim.references.QueuableReference;
import se.modlab.simplesim.references.QueuableReferenceExported;
import se.modlab.simplesim.references.StructReference;
import se.modlab.simplesim.references.StructReferenceExported;
import se.modlab.simplesim.references.TypedQueuableReference;
import se.modlab.simplesim.references.TypedQueuableReferenceConst;
import se.modlab.simplesim.references.TypedStructReference;
import se.modlab.simplesim.references.TypedStructReferenceConst;
import se.modlab.simplesim.references.UntypedQueuableReference;
import se.modlab.simplesim.references.UntypedQueuableReferenceConst;
import se.modlab.simplesim.references.UntypedStructReference;
import se.modlab.simplesim.references.UntypedStructReferenceConst;
import se.modlab.simplesim.bags.*;

public class CreateStaticVersionVisitor 
       extends CreateAllExportedVersionsVisitor
{

  public void visit(BooleanLifoBag inst) 
    throws IntolerableException 
  {
    si = inst;
  }
 
  public void visit(BooleanLiloBag inst) 
    throws IntolerableException 
  {
    si = inst;
  }

  public void visit(DoubleLifoBag inst) 
    throws IntolerableException 
  {
    si = inst;
  }

  public void visit(DoubleLiloBag inst) 
    throws IntolerableException 
  {
    si = inst;
  }

  public void visit(DoubleRandomBag inst) 
    throws IntolerableException 
  {
    si = inst;
  }

  public void visit(DoubleSortBag inst) 
    throws IntolerableException 
  {
    si = inst;
  }

  public void visit(LongLifoBag inst) 
    throws IntolerableException 
  {
    si = inst;
  }

  public void visit(LongLiloBag inst) 
    throws IntolerableException 
  {
    si = inst;
  }

  public void visit(LongRandomBag inst) 
    throws IntolerableException 
  {
    si = inst;
  }

  public void visit(LongSortBag inst) 
    throws IntolerableException 
  {
    si = inst;
  }

  public void visit(ComplexLifoBag inst) 
    throws IntolerableException 
  {
    si = inst;
  }

  public void visit(ComplexLiloBag inst) 
    throws IntolerableException 
  {
    si = inst;
  }

  public void visit(ComplexRandomBag inst) 
    throws IntolerableException 
  {
    si = inst;
  }

  public void visit(ComplexSortBag inst) 
		    throws IntolerableException 
		  {
		    si = inst;
		  }
		  
  public void visit(QueuableReference inst) 
		    throws IntolerableException 
		  {
		    si = inst;
		  }
		  
  public void visit(QueuableReferenceExported inst) 
		    throws IntolerableException 
		  {
		    si = inst;
		  }
		  
  public void visit(StructReference inst) 
		    throws IntolerableException 
		  {
		    si = inst;
		  }
		  
  public void visit(StructReferenceExported inst) 
		    throws IntolerableException 
		  {
		    si = inst;
		  }
		  
  public void visit(TypedStructReference inst) 
		    throws IntolerableException 
		  {
		    si = inst;
		  }
		  
  public void visit(TypedStructReferenceConst inst) 
		    throws IntolerableException 
		  {
		    si = inst;
		  }
		  
  public void visit(TypedQueuableReference inst) 
		    throws IntolerableException 
		  {
		    si = inst;
		  }
		  
  public void visit(TypedQueuableReferenceConst inst) 
		    throws IntolerableException 
		  {
		    si = inst;
		  }
		  
  public void visit(UntypedStructReference inst) 
		    throws IntolerableException 
		  {
		    si = inst;
		  }
		  
  public void visit(UntypedStructReferenceConst inst) 
		    throws IntolerableException 
		  {
		    si = inst;
		  }
		  
  public void visit(UntypedQueuableReference inst) 
		    throws IntolerableException 
		  {
		    si = inst;
		  }
		  
  public void visit(UntypedQueuableReferenceConst inst) 
		    throws IntolerableException 
		  {
		    si = inst;
		  }
		  
}
