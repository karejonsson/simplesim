package se.modlab.simplesim.algorithms.initiators;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.variables.*;

public class ComplexInitiator implements Initiator
{
  private VariableInstance mv;
  private Initiator initiators[];

  public ComplexInitiator(VariableInstance _mv, Scope s)
    throws IntolerableException 
  {
    mv = _mv;
    InstanceInitiatorCollectorVisitor iicv = new InstanceInitiatorCollectorVisitor(s);
    mv.accept(iicv);
    initiators = iicv.getInitiators();
  }

  public String getName()
  {
    return mv.getName();
  }

  public VariableInstance getInstance()
  {
    return mv;
  }

  public void runInitiation() throws IntolerableException
  {
    for(int i = 0 ; i < initiators.length ; i++)
    {
      initiators[i].runInitiation();
    }
  }

  public String toString()
  {
    StringBuffer sb = new StringBuffer();
    for(int i = 0 ; i < initiators.length ; i++)
    {
      sb.append(initiators[i].toString());
      if(i != initiators.length -1) sb.append("\n");
    }
    return sb.toString();
  }

}

