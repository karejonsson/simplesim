package se.modlab.simplesim.algorithms.setup;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.simplesim.variables.*;
import se.modlab.generics.sstruct.variables.*;
import java.util.*;
import se.modlab.generics.*;
//import se.modlab.simplesim.algorithms.share.*;
import se.modlab.simplesim.events.*;
//import se.modlab.simplesim.algorithms.presentation.*;
import se.modlab.simplesim.scoping.*;
import se.modlab.simplesim.references.*;
import se.modlab.simplesim.bags.*;

public class SetFullPathNamesOnQueablesVisitor
               extends InstanceVisitorAdapter
{
  private Stack names = null;

  public SetFullPathNamesOnQueablesVisitor()
  {
    names = new Stack();
  }

  public String getFullName()
  {
    StringBuffer sb = new StringBuffer();
    for(int i = 0 ; i < names.size() ; i++)
    {
      sb.append(names.elementAt(i).toString());
    }
    return sb.toString();
  }
 
  public void visit(BooleanVariable inst) 
    throws IntolerableException 
  {
  }
 
  public void visit(BooleanConst inst) 
    throws IntolerableException 
  {
  }
 
  public void visit(SimBooleanExported inst) 
    throws IntolerableException 
  {
  }
 
  public void visit(SimBooleanMonitoredVariable inst) 
    throws IntolerableException 
  {
  }
 
  public void visit(DoubleVariable inst) 
    throws IntolerableException 
  {
  }
 
  public void visit(DoubleConst inst) 
    throws IntolerableException 
  {
  }
 
  public void visit(SimDoubleExported inst) 
    throws IntolerableException 
  {
  }
 
  public void visit(SimDoubleMonitoredVariable inst) 
    throws IntolerableException 
  {
  }
 
  public void visit(LongVariable inst) 
    throws IntolerableException 
  {
    //System.out.println("setFullPath --- longVariable "+inst.getName());
  }
 
  public void visit(LongConst inst) 
    throws IntolerableException 
  {
  }
 
  public void visit(SimLongExported inst) 
    throws IntolerableException 
  {
  }
 
  public void visit(SimLongMonitoredVariable inst) 
    throws IntolerableException 
  {
  }

  public void visit(TypedQueuableReference inst) 
    throws IntolerableException 
  {
    inst.setFullPathName(getFullName()+inst.getName());
  }

  public void visit(TypedQueuableReferenceConst inst) 
    throws IntolerableException 
  {
    inst.setFullPathName(getFullName()+inst.getName());
  }

  public void visit(UntypedQueuableReference inst) 
    throws IntolerableException 
  {
    inst.setFullPathName(getFullName()+inst.getName());
  }

  public void visit(UntypedQueuableReferenceConst inst) 
    throws IntolerableException 
  {
    inst.setFullPathName(getFullName()+inst.getName());
  }

  public void visit(QueuableReferenceExported inst) 
    throws IntolerableException 
  {
  }

  public void visit(TypedStructReference inst) 
    throws IntolerableException 
  {
    inst.setFullPathName(getFullName()+inst.getName());
  }

  public void visit(TypedStructReferenceConst inst) 
    throws IntolerableException 
  {
    inst.setFullPathName(getFullName()+inst.getName());
  }

  public void visit(UntypedStructReference inst) 
    throws IntolerableException 
  {
    inst.setFullPathName(getFullName()+inst.getName());
  }

  public void visit(UntypedStructReferenceConst inst) 
    throws IntolerableException 
  {
    inst.setFullPathName(getFullName()+inst.getName());
  }

  public void visit(StructReferenceExported inst) 
    throws IntolerableException 
  {
  }

  public void visit(ProcedureTrigged inst) 
    throws IntolerableException 
  {
    inst.setFullPathName(getFullName()+inst.getName());
    VariableInstance vis[] = inst.getAllInstances();
    names.push(inst.getName()+".");
    for(int i = 0 ; i < vis.length ; i++)
    {
      if(vis[i] != inst) vis[i].accept(this);
    }
    names.pop();
  }
 
  public void visit(ProcedureSingle inst) 
    throws IntolerableException 
  {
    inst.setFullPathName(getFullName()+inst.getName());
    VariableInstance vis[] = inst.getAllInstances();
    names.push(inst.getName()+".");
    for(int i = 0 ; i < vis.length ; i++)
    {
      if(vis[i] != inst) vis[i].accept(this);
    }
    names.pop();
  }
  
  public void visit(ProcedureRepeating inst) 
    throws IntolerableException 
  {
    inst.setFullPathName(getFullName()+inst.getName());
    VariableInstance vis[] = inst.getAllInstances();
    names.push(inst.getName()+".");
    for(int i = 0 ; i < vis.length ; i++)
    {
      if(vis[i] != inst) vis[i].accept(this);
    }
    names.pop();
  }
 
  public void visit(ProcedureActor inst) 
    throws IntolerableException 
  {
    inst.setFullPathName(getFullName()+inst.getName());
    VariableInstance vis[] = inst.getAllInstances();
    names.push(inst.getName()+".");
    for(int i = 0 ; i < vis.length ; i++)
    {
      if(vis[i] != inst) vis[i].accept(this);
    }
    names.pop();
  }
 
  public void visit(SimComplexVariableExported inst) 
    throws IntolerableException 
  {
  }

  public void visit(ComplexVariable inst) 
    throws IntolerableException 
  {
    names.push(inst.getName()+".");
    traverse(inst);
    names.pop();
  }

  public void visit(SimVariableVectorExported inst) 
    throws IntolerableException 
  {
  }

  public void visit(VariableVector inst) 
    throws IntolerableException 
  {
    VariableInstance _inst = inst.getVectorElement(0);
    if(_inst instanceof ProcedureSingle)
    {
      int len = inst.getLength();
      for(int i = 0 ; i < len ; i++)
      {
        ProcedureSingle ps = (ProcedureSingle) inst.getVectorElement(i);
        ps.setFullPathName(getFullName()+inst.getName());
      }
      return;
    }
    names.push(inst.getName());
    traverse(inst);
    names.pop();
  }

  public void visit(SimVariableVector inst) 
    throws IntolerableException 
  {
    VariableInstance _inst = inst.getVectorElement(0);
    if(_inst instanceof ProcedureSingle)
    {
      int len = inst.getLength();
      for(int i = 0 ; i < len ; i++)
      {
        ProcedureSingle ps = (ProcedureSingle) inst.getVectorElement(i);
        ps.setFullPathName(getFullName()+inst.getName());
      }
      return;
    }
    names.push(inst.getName());
    traverse(inst);
    names.pop();
  }

  public void visit(VariableVectorFromFile inst) 
    throws IntolerableException 
  {
  }

  public void visit(SimMonitoredVectorVariableFromFile inst) 
    throws IntolerableException 
  {
  }

  public void visit(ScopeReference inst) 
    throws IntolerableException 
  {
    //System.out.println("setFullPathNamesOnQueablesVisitor - scopeReference");
  }

  public void visit(ScopeReferenceExported inst) 
    throws IntolerableException 
  {
    //System.out.println("setFullPathNamesOnQueablesVisitor - scopeReferenceExported");
  }

  public void visit(SubscopeReference inst) 
    throws IntolerableException 
  {
    //System.out.println("setFullPathNamesOnQueablesVisitor - subscopeReference "+inst.getName());
    names.push(inst.getName()+".");
    traverse((Complex) inst);
    names.pop();
  }

  public void visit(SubscopeReferenceExported inst) 
    throws IntolerableException 
  {
    //System.out.println("setFullPathNamesOnQueablesVisitor - subscopeReferenceExported "+inst.getName());
    names.push(inst.getName()+".");
    traverse((VariableInstance) inst);
    names.pop();
  }

  public void visit(LongLifoBag inst) 
    throws IntolerableException 
  {
    inst.setFullPathName(getFullName()+inst.getName());
  }

  public void visit(LongLiloBag inst) 
    throws IntolerableException 
  {
    inst.setFullPathName(getFullName()+inst.getName());
  }

  public void visit(LongRandomBag inst) 
    throws IntolerableException 
  {
    inst.setFullPathName(getFullName()+inst.getName());
  }

  public void visit(LongSortBag inst) 
    throws IntolerableException 
  {
    inst.setFullPathName(getFullName()+inst.getName());
  }


  public void visit(DoubleLifoBag inst) 
    throws IntolerableException 
  {
    inst.setFullPathName(getFullName()+inst.getName());
  }

  public void visit(DoubleLiloBag inst) 
    throws IntolerableException 
  {
    inst.setFullPathName(getFullName()+inst.getName());
  }

  public void visit(DoubleRandomBag inst) 
    throws IntolerableException 
  {
    inst.setFullPathName(getFullName()+inst.getName());
  }

  public void visit(DoubleSortBag inst) 
    throws IntolerableException 
  {
    inst.setFullPathName(getFullName()+inst.getName());
  }


  public void visit(BooleanLifoBag inst) 
    throws IntolerableException 
  {
    inst.setFullPathName(getFullName()+inst.getName());
  }

  public void visit(BooleanLiloBag inst) 
    throws IntolerableException 
  {
    inst.setFullPathName(getFullName()+inst.getName());
  }

  public void visit(BooleanRandomBag inst) 
    throws IntolerableException 
  {
    inst.setFullPathName(getFullName()+inst.getName());
  }


  public void visit(ComplexLifoBag inst) 
    throws IntolerableException 
  {
    inst.setFullPathName(getFullName()+inst.getName());
  }

  public void visit(ComplexLiloBag inst) 
    throws IntolerableException 
  {
    inst.setFullPathName(getFullName()+inst.getName());
  }

  public void visit(ComplexRandomBag inst) 
    throws IntolerableException 
  {
    inst.setFullPathName(getFullName()+inst.getName());
  }

  public void visit(ComplexSortBag inst) 
    throws IntolerableException 
  {
    inst.setFullPathName(getFullName()+inst.getName());
  }

}
