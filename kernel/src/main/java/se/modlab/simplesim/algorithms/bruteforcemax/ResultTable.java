package se.modlab.simplesim.algorithms.bruteforcemax;

import javax.swing.*;
import javax.swing.table.*;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.util.Sort;
import se.modlab.simplesim.algorithms.setup.*;
import se.modlab.simplesim.algorithms.share.*;

public class ResultTable extends AbstractTableModel 
{

  private Vector rows = new Vector();
  private int ortogonals;
  private RerunAlgo ra;
  private SimData sd;
  private int keep;

  public ResultTable(int ortogonals, RerunAlgo ra, SimData sd, int keep)
  {
    this.ortogonals = ortogonals;
    this.ra = ra;
    this.sd = sd;
    this.keep = keep;
  }

  public void addRow(ResultRow rr)
    throws IntolerableException
  {
    if(rr.getLength() != ortogonals)
    {
      throw new InternalError(
        "Other length. Happened in resultTable.addRow(resultRow)");
    }
    Sort.add(rows, rr, ResultRow.comp);
    while(rows.size() > keep)
    {
      rows.removeElementAt(keep);
    }
  }

  public int getColumnCount() 
  {
    return ortogonals+1;
  }

  public int getRowCount() 
  {
    return rows.size();
  }

  public String getColumnName(int col) 
  {
    if(col == ortogonals) return "Result"; 
    if(rows.size() == 0) return "Column "+col;
    ResultRow rr = (ResultRow) rows.elementAt(0);
    return rr.getName(col);
  }

  public Object getValueAt(int row, int col) 
  {
    ResultRow rr = (ResultRow) rows.elementAt(row);
    return rr.getValueAt(col);
  }

  public void runAlgo(int idx)
  {
    try
    {
      //System.out.println("resultTable - Run algo "+idx);
      ResultRow rr = (ResultRow) rows.elementAt(idx);
      rr.setResettersBack();
      sd.setProgramHeader("This is a particular rerun of a choosen execution of\n"+
                          "the algorithm bruteforcemax\n\n"+rr.toString());
      ra.run();
    }
    catch(Exception e)
    {
      UniversalTellUser.general(null, new UnclassedError(null, e));
    }
  }

  public Class getColumnClass(int c) 
  {
    return getValueAt(0, c).getClass();
  }

  public boolean isCellEditable(int row, int col) 
  {
    return false;
  }

  public static final int SCREEN_WIDTH = (int) Toolkit.getDefaultToolkit().getScreenSize().width;
  public static final int SCREEN_HEIGHT = (int) Toolkit.getDefaultToolkit().getScreenSize().height-70;

  public static void flash(ResultTable rt) 
  {
    JTable table = new JTable(rt);
    final ResultTable _rt  = rt;;
    JScrollPane scrollPane = new JScrollPane(table);
    table.addMouseListener(new LocalTableListener());
    JFrame frame = new JFrame("Maximizations outcome");
    frame.setLocation(new Point((int)(SCREEN_WIDTH/2), 0));
    frame.setSize(new Dimension((int)(SCREEN_WIDTH/2), SCREEN_HEIGHT));
    frame.getContentPane().add(scrollPane, BorderLayout.CENTER);
    //frame.pack();
    frame.setVisible(true);
  }

  public static class LocalTableListener extends MouseAdapter 
  {
    
    public void mouseReleased(MouseEvent e) 
    {
      Point p = e.getPoint();
      JTable table = (JTable) e.getComponent();
      int column = table.columnAtPoint(p);
      int row = table.rowAtPoint(p);
      //System.out.println("Rad "+row);
      TableModel tm = table.getModel();
      ResultTable rt = (ResultTable) tm;
      rt.runAlgo(row);
    }

  }


}
