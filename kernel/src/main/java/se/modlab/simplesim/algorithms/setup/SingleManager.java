package se.modlab.simplesim.algorithms.setup;

import java.io.InputStream;
import java.util.Hashtable;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.simplesim.algorithms.initiators.*;
import se.modlab.simplesim.algorithms.samplers.*;
import se.modlab.simplesim.events.*;
import se.modlab.simplesim.scoping.*;

public class SingleManager implements Manager
{

  private String name;
  private Manager localScope[];
  private ArithmeticEvaluable simtime[];
  private ProgramBlock p;
  private SimData sd;
  private ScopeFactory sf;
  protected String filename;
  protected int line;
  protected int column;
  
  public SingleManager(String _name,
                       Manager _localScope[],
                       ArithmeticEvaluable _simtime[],
                       ProgramBlock _p,
                       SimData _sd,
                       ScopeFactory _sf, String _filename, int _line, int _column)
  {
    name = _name;
    localScope = _localScope;
    simtime = _simtime;
    p = _p;
    sd = _sd;
    sf = _sf;
    filename = _filename;
    line = _line;
    column = _column;
  }

  private void _initialize(Scope s, boolean isSharp, Hashtable<VariableInstance, Manager> managermap)
    throws IntolerableException
  {
    //System.out.println("Initialize single arithmetic in "+s.getName()+" for "+toString());
    SimScope ls = (SimScope) sf.getInstance(s, s.getName()+"/"+name);
    for(int i = 0 ; i < localScope.length ; i++)
    {
      localScope[i].initialize(ls, isSharp, managermap);
    }
    double time[] = new double[simtime.length];
    for(int i = 0 ; i < simtime.length ; i++)
    {
      time[i] = simtime[i].evaluate(s).getValue();
    }
    VariableFactory vf = new procedureSingleFactory("single vector "+name,
                                                    (SimScope)ls, p, time, sf);
    VariableFactory vf2 = s.addFactoryUniquely(vf);
    if(vf2 != vf)
    {
      throw new UserCompiletimeError("Type "+name+" multiply defined.");
    }
    SimVariableVector svv = new SimVariableVector(name, vf, filename, line, column, vf, time.length, false);
    s.addComplexInstance(svv);
    if(isSharp)
    {
      for(int i = 0 ; i < time.length ; i++)
      {
        ProcedureSingle ps = (ProcedureSingle) svv.getVectorElement(i);
        sd.addQueuable(ps);
        sd.addInitiator(new QueableInitiator(ps));
      }
    }
	if(managermap != null) {
		managermap.put(svv, this);
	}    
    
  } 
  
  public void initialize(Scope s, boolean isSharp, Hashtable<VariableInstance, Manager> managermap)
  throws IntolerableException
  {
    try {
      _initialize(s, isSharp, managermap);
    }
	catch(UserRuntimeError ue) {
	      String m = ue.getMessage()+"\n"+"This happened in file "+filename+", line "+line+", column "+column;
	      UserRuntimeError ue2 = new UserRuntimeError(m, ue.getCause(), ue.getCollectors());
		  ue2.setAction(ue.getAction());
		  throw ue2;
	    }
	catch(UserCompiletimeError ue) {
	      String m = ue.getMessage()+"\n"+"This happened in file "+filename+", line "+line+", column "+column;
	      UserCompiletimeError ue2 = new UserCompiletimeError(m, ue.getCause(), ue.getCollectors());
		  ue2.setAction(ue.getAction());
		  throw ue2;
	    }
  }
  
	public void reinitializeLogical(LogicalExpression le) throws IntolerableException {
		throw new UserRuntimeError(
				"Cannot set a single to the value of a boolean expression.\n"+
				"This happens to the single "+name+" declared on line "+line+", column "+column+" in the file "+filename);
    }

	public void reinitializeArithmetic(ArithmeticEvaluable ae) throws IntolerableException {
		throw new UserRuntimeError(
				"Cannot set a single to the value of an arithmetic expression.\n"+
				"This happens to the single "+name+" declared on line "+line+", column "+column+" in the file "+filename);
    }

	public void reinitializeFromInputStream(InputStream is) throws IntolerableException {
		throw new InternalError(
				"Cannot set a single to the value of an input stream.\n"+
				"This happens to the single "+name+" declared on line "+line+", column "+column+" in the file "+filename);
    }

  private static class procedureSingleFactory implements SimVariableFactory
  {
    private int iter = 0;
    private SimScope s = null;
    private ProgramBlock p = null;
    private double time[] = null;
    private ScopeFactory sf;
    private String factorysName;
    public procedureSingleFactory(String _factorysName, SimScope _s, ProgramBlock _p, double _time[], ScopeFactory _sf)
    {
      factorysName = _factorysName;
      s = _s;
      p = _p;
      time = _time;
      sf = _sf;
    } 
    public VariableInstance getInstance(String name, boolean monitor, String filename, int line, int column, Hashtable<VariableInstance, Manager> managermap) 
      throws IntolerableException
    {
      return new ProcedureSingle(name, filename, line, column, s, p, time[iter++], sf);
    }
    public VariableInstance getInstance(String name, String filename, int line, int column, Hashtable<VariableInstance, Manager> managermap) 
      throws IntolerableException
    {
      return new ProcedureSingle(name, filename, line, column, s, p, time[iter++], sf);
    }
    public String getTypesName()
    {
      return factorysName;
    }
	public VariableInstance getInstance(String name, String filename, int line, int column)
	throws IntolerableException {
		//System.out.println("Call to getInstance(s,s,i,i) i.e. addin null in class "+getClass().getName());
		return getInstance(name, filename, line, column, null);
	}
	public VariableInstance getInstance(String name, boolean monitor, String filename, int line, int column)
	throws IntolerableException {
		//System.out.println("Call to getInstance(s,b,s,i,i) i.e. addin null in class "+getClass().getName());
		return getInstance(name, monitor, filename, line, column, null);
	}
  }

}
