package se.modlab.simplesim.algorithms.regression;

import se.modlab.generics.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.simplesim.variables.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.gui.util.SimpleExplorer;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.simplesim.events.*;
import se.modlab.simplesim.algorithms.setup.*;
import se.modlab.simplesim.algorithms.presentation.*;
import se.modlab.simplesim.algorithms.share.*;
import se.modlab.simplesim.algorithms.externalpainters.ExternalPainterFactoryHandle;
import se.modlab.simplesim.algorithms.once.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;

import javax.swing.*;

import java.util.*;
import java.io.*;

import se.modlab.simplesim.scoping.*;

public class RegressionAlgorithm extends AlgorithmBase 
                                 implements Algorithm
{

  private double startTimeRegression;
  private double endTimeRegression;
  private long intervals;
  private String variablesName;
  private double limitation;
  private long maximumIterations;
  private StatisticsCollector statCollectors[];
  private StatisticsCollector measuredCollector;
  private int finishedLoops = 0;
  
  private Comparator compare = new EventComparator();

  public RegressionAlgorithm(SimData sd,
                             double startTimeRegression,
                             double endTimeRegression,
                             long intervals,
                             String variablesName,
                             double limitation,
                             long maximumIterations)
    throws IntolerableException
  {
    super(sd);
    if(startTimeRegression < 0)
    {
      throw new UserCompiletimeError("Starttime for regression must not be negative.");
    }
    this.startTimeRegression = startTimeRegression;
    if(startTimeRegression >= endTimeRegression)
    {
      throw new UserCompiletimeError("Endtime for regression must be larger than starttime.");
    }
    this.endTimeRegression = endTimeRegression;
    if(intervals < 1)
    {
      throw new UserCompiletimeError("Number of intervals must be\n"+
                                     "at least one.");
    }
    this.intervals = intervals;    
    this.variablesName = variablesName;
    if(variablesName == null)
    {
      throw new InternalError("No variable name. regressionAlgorithm.ctor");
    }
    if(limitation <= 0)
    {
      throw new UserCompiletimeError("Limitation must be positive.");
    }
    this.limitation = limitation;
    if(maximumIterations < 2)
    {
      throw new UserCompiletimeError("Maximum number of iterations must be at\n"+
                          "least two. Use once-algorithm otherwise.");
    }
    this.maximumIterations = maximumIterations;
  }

  private void runSimulationUpdateSampling()
    throws IntolerableException
  {
    //commons com = sd.getCommons();
    runSimulation();
    for(int i = 0 ; i < statCollectors.length ; i++)
    {
      StatisticsCollector sc = statCollectors[i];
      sc.performUpdates();
    }
    //sd.resetAllPermanentVariables();
  }

  public void runAlgorithm()
    throws IntolerableException
  {
    try
    {
      setupScenario();

      SegmentPainter sp = new SegmentPainterRegression();
      FirstLevelTreeNodeBuilderVisitorRegression fltnbvr = 
        new FirstLevelTreeNodeBuilderVisitorRegression(
          intervals,
          startTimeRegression,
          endTimeRegression,
          sd,
          sp, ExternalPainterFactoryHandle.get());

      SimScope s = (SimScope) sd.getGlobalScope();
      VariableInstance vis[] = s.getAllInstances();
      for(int i = 0 ; i < vis.length ; i++)
      {
        vis[i].accept(fltnbvr);
      } 

      Variables vars = (Variables) fltnbvr.getResult();

      node = new TopNode(sd, vars, new SegmentPainterRegression());
      MonitoredVariableExtracted mve[] = fltnbvr.getExtracts();
      statCollectors = new StatisticsCollector[mve.length];

      for(int i = 0 ; i < mve.length ; i++)
      {
        statCollectors[i] = (StatisticsCollector) mve[i];
        if(statCollectors[i].getName().compareTo(variablesName) == 0)
        {
          measuredCollector = statCollectors[i];
        }
      }
    
      if(measuredCollector == null)
      {
        throw new UserCompiletimeError(
          "Variable "+variablesName+" for the regression is not known!");
      }

      addMessage(
        "Simulation starts at "+Commons.getTimeStringForExecutionTime()+
        ". The simulations \nstart time for regression analysis is "+
        Commons.getTimeStringFromLong(sd.getCommons().getEpochTimeFromSimTime(startTimeRegression))+
        "\nand the end time for regression analysis is\n"+
        Commons.getTimeStringFromLong(sd.getCommons().getEpochTimeFromSimTime(endTimeRegression))+
        ". The amount of intervals is "+intervals+" and \nthe measures limitation is "+
        limitation+". The maximum amount of iterations \nbefore definite termination is "+
        maximumIterations+".\n");

      double measure;

      runSimulationUpdateSampling();
      addMessage("First iteration finished. Measure not valid yet.");
      finishedLoops = 1;
      //if(getStopPushed())
      //{
      //  throw new userError("Terminated on users request");
      //}
      runSimulationUpdateSampling();
      addMessage("Second iteration finished. Measure not valid yet.");
      finishedLoops = 2;
      //if(getStopPushed())
      //{
      //  throw new userError("Terminated on users request");
      //}

      int loops = 2;

      do
      {
        runSimulationUpdateSampling();
        loops++;
        finishedLoops = loops;
        measure = measuredCollector.getMeasure();
        addMessage("After "+loops+" iterations measure = "+measure);
        //if(getStopPushed())
        //{
          //tr.terminate();
          //throw new userError("Terminated on users request");
        //}
      } while(((loops < maximumIterations) && (measure > limitation)) &&
    		  (getStopHistory()));

      setDone();
      //System.out.println("Regressio ndone");

      extracts = statCollectors;
    }
    catch(IntolerableException ie)
    {
      //forceTermination();
      throw ie;
    }  
    Runtime.getRuntime().runFinalization();
    //displayResultsGraphically();
  }

  public String getSurveillanceMessageForBar()
  {
    return "Done with "+ ((int) (100*getCurrent() / getLengthOfTask())) +"%";
  }

  public String getSurveillanceMessageForDump()
  {
    return getMessage();
  }

  public int getCurrent()
  {
    return finishedLoops;
  }

  public int getLengthOfTask()
  {
    return (int) maximumIterations;
  }
  /*
  public boolean isDone() 
  {
    return (finishedLoops == maximumIterations) ||
           (measure < limitation);
  }
  */
  public void displayResultsGraphically(boolean visible)
  {
    final SimpleExplorer se = new SimpleExplorer("Evolution of variables", 
                                                 node, 
                                                 null);
    se.addWindowListener(
       new WindowAdapter() 
       {
         public void windowClosed(WindowEvent e)
         {
           //System.out.println("onImageIllustrator - window closed");
           // Retaking the line comment below will cause the window to go
           // in some staled state.
           //se.dispose();
           System.gc();
         }
         public void windowClosing(WindowEvent e)
         {
           //System.out.println("onImageIllustrator - window closing");
           // Retaking the line below will cause the window to go
           // in some staled state.
           //se.dispose();
           System.gc();
         }
       });
    final SegmentPainter sp = new SegmentPainterOnce();
    JMenuBar mb = se.getJMenuBar();//new JMenuBar();
    //se.setJMenuBar(mb);
    JMenu menu = new JMenu("Report");
    mb.add(menu);
    JMenuItem menuItem = new JMenuItem("Html site");
    menu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          Runnable r = new Runnable()
                       {
                         public void run()
                         {
                           RegressionAlgorithm.this.generateReport();
                         }
                       };
          Thread t = new Thread(r);
          t.start();
        }
      }
    );

    Commons com = sd.getCommons();
    final CrossplotAdder cpa = node.getCrossplotsNode();
    final OverloadAdder ola = node.getOverloadsNode();
    JMenu plots = new JMenu("Plots");
    mb.add(plots);
    bp = new Biplot(se, false, extracts, cpa, sp);
    menuItem = new JMenuItem("biplot");
    plots.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          bp.setVisible(true);
        }
      });
    op = new OverlayPlot(se, false, extracts, ola, sp);
    menuItem = new JMenuItem("overlay plot");
    plots.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          op.setVisible(true);
        }
      });
    JMenu exports = new JMenu("Export");
    mb.add(exports);
    etv = new ExportTwoVariables(se, false, extracts);
    menuItem = new JMenuItem("Export 2 variables");
    exports.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          etv.setVisible(true);
        }
      });
    se.pack();
    se.setVisible(visible);
  }

	public Manager[] getManagerArray() {
		return sd.getManagerArray();
	}

}