package se.modlab.simplesim.algorithms.regression;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;
import se.modlab.simplesim.algorithms.share.*;
import java.awt.*;
import se.modlab.simplesim.algorithms.presentation.*;

public class SegmentPainterRegression implements SegmentPainter
{

  protected void connectTwo(Graphics g, Dimension d,
                            double val_1, double time_1, double val_2, double time_2,
                            DrawingParameters dp)
  {
    g.drawLine((int) (dp.w_pix + (d.width-2*dp.w_pix)*(time_1/dp.endTime)),
               (int) (dp.h_pix - (val_1-dp.min_val)/(dp.h_val)*(dp.h_pix-dp.margin)),
               (int) (dp.w_pix + (d.width-2*dp.w_pix)*(time_2/dp.endTime)),
               (int) (dp.h_pix - (val_1-dp.min_val)/(dp.h_val)*(dp.h_pix-dp.margin)));

    g.drawLine((int) (dp.w_pix + (d.width-2*dp.w_pix)*(time_2/dp.endTime)),
               (int) (dp.h_pix - (val_1-dp.min_val)/(dp.h_val)*(dp.h_pix-dp.margin)),
               (int) (dp.w_pix + (d.width-2*dp.w_pix)*(time_2/dp.endTime)),
               (int) (dp.h_pix - (val_2-dp.min_val)/(dp.h_val)*(dp.h_pix-dp.margin)));
  }

  protected void connectTwoBold(Graphics g, Dimension d,
                                double val_1, double time_1, double val_2, double time_2,
                                DrawingParameters dp)
  {
    g.drawLine((int) (dp.w_pix + (d.width-2*dp.w_pix)*(time_1/dp.endTime)),
               (int) (dp.h_pix - (val_1-dp.min_val)/(dp.h_val)*(dp.h_pix-dp.margin)),
               (int) (dp.w_pix + (d.width-2*dp.w_pix)*(time_2/dp.endTime)),
               (int) (dp.h_pix - (val_1-dp.min_val)/(dp.h_val)*(dp.h_pix-dp.margin)));

    g.drawLine((int) (dp.w_pix + (d.width-2*dp.w_pix)*(time_1/dp.endTime)),
               (int) (dp.h_pix - (val_1-dp.min_val)/(dp.h_val)*(dp.h_pix-dp.margin))-1,
               (int) (dp.w_pix + (d.width-2*dp.w_pix)*(time_2/dp.endTime)),
               (int) (dp.h_pix - (val_1-dp.min_val)/(dp.h_val)*(dp.h_pix-dp.margin))-1);

    g.drawLine((int) (dp.w_pix + (d.width-2*dp.w_pix)*(time_1/dp.endTime)),
               (int) (dp.h_pix - (val_1-dp.min_val)/(dp.h_val)*(dp.h_pix-dp.margin))+1,
               (int) (dp.w_pix + (d.width-2*dp.w_pix)*(time_2/dp.endTime)),
               (int) (dp.h_pix - (val_1-dp.min_val)/(dp.h_val)*(dp.h_pix-dp.margin))+1);

    g.drawLine((int) (dp.w_pix + (d.width-2*dp.w_pix)*(time_2/dp.endTime)),
               (int) (dp.h_pix - (val_1-dp.min_val)/(dp.h_val)*(dp.h_pix-dp.margin)),
               (int) (dp.w_pix + (d.width-2*dp.w_pix)*(time_2/dp.endTime)),
               (int) (dp.h_pix - (val_2-dp.min_val)/(dp.h_val)*(dp.h_pix-dp.margin)));
  }

  public void paint(Graphics g, 
                    Dimension d, 
                    DrawingParameters dp,
                    int index)
    throws IntolerableException
  {
    Segment s = (Segment) dp.mve.getGeneralizedValue(index);
    double val_1 = s.getAverage();
    double dev_1 = s.getDeviation();
    double max_1 = s.getMaximum();
    double min_1 = s.getMinimum();
    double time_1 = dp.mve.getValue(index).getTime();

    s = (Segment) dp.mve.getGeneralizedValue(index+1);
    double val_2 = s.getAverage();
    double dev_2 = s.getDeviation();
    double max_2 = s.getMaximum();
    double min_2 = s.getMinimum();
    double time_2 = dp.mve.getValue(index+1).getTime();
    connectTwoBold(g, d, val_1, time_1, val_2, time_2, dp);
    if((max_1 > val_1+dev_1) && (max_2 > val_2+dev_2))
    { 
      connectTwo(g, d, val_1+dev_1, time_1, val_2+dev_2, time_2, dp);
    }
    if((min_1 < val_1-dev_1) && (min_2 < val_2-dev_2))
    { 
      connectTwo(g, d, val_1-dev_1, time_1, val_2-dev_2, time_2, dp);
    }
    connectTwo(g, d, max_1, time_1, max_2, time_2, dp);
    connectTwo(g, d, min_1, time_1, min_2, time_2, dp);
  }

  public void paintLast(Graphics g, 
                        Dimension d, 
                        DrawingParameters dp)
    throws IntolerableException
  {
    Segment s = (Segment) dp.mve.getGeneralizedValue(dp.mve.getSize()-1);
    double val_1 = s.getAverage();
    double dev_1 = s.getDeviation();
    double time_1 = dp.mve.getValue(dp.mve.getSize()-1).getTime();
    connectTwo(g, d, val_1, time_1, val_1, dp.endTime, dp);
    connectTwo(g, d, val_1-dev_1, time_1, val_1-dev_1, dp.endTime, dp);
    connectTwo(g, d, val_1+dev_1, time_1, val_1+dev_1, dp.endTime, dp);
  }

}