package se.modlab.simplesim.algorithms.setup;

import java.io.InputStream;
import java.util.Hashtable;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.simplesim.algorithms.initiators.*;
import se.modlab.simplesim.algorithms.samplers.*;
import se.modlab.simplesim.events.*;
import se.modlab.simplesim.scoping.*;
import se.modlab.simplesim.variables.Manager;

public class ProcedureActorFactoryManager implements Manager
{

  private String typesName;
  private Manager localScope[];
  private ProcedureActorState pass[];
  private SimData sd;
  private ScopeFactory sf;
  
  public ProcedureActorFactoryManager(
           String _typesName,
           Manager _localScope[],
           ProcedureActorState _pass[],
           SimData _sd,
           ScopeFactory _sf)
  {
    typesName = _typesName;
    localScope = _localScope;
    pass = _pass;
    sd = _sd;
    sf = _sf;
  }

  public void initialize(Scope s, boolean isSharp, Hashtable<VariableInstance, Manager> managermap)
    throws IntolerableException
  {
    ProcedureActorFactory paf = 
      new ProcedureActorFactory(
        (SimScope)s, 
        typesName, 
        localScope, 
        pass, 
        sd, 
        sf, 
        isSharp);
    VariableFactory vf = s.addFactoryUniquely(paf);
    if(vf != paf)
    {
      throw new UserCompiletimeError("Type "+typesName+" multiply defined.");
    }
  } 

  public String toString()
  {
    return "procedure actor factory manager for type "+typesName+" with scope ";
  }
  
	public void reinitializeLogical(LogicalExpression le) throws IntolerableException {
		throw new UserRuntimeError(
				"Cannot set procedure actor factory to the value of a boolean expression.\n"+
				"This happens to the factory "+typesName+".");
    }

	public void reinitializeArithmetic(ArithmeticEvaluable ae) throws IntolerableException {
		throw new UserRuntimeError(
				"Cannot set procedure actor factory to the value of a arithmetic expression.\n"+
				"This happens to the factory "+typesName+".");
    }

	public void reinitializeFromInputStream(InputStream is) throws IntolerableException {
		throw new InternalError(
				"Cannot set procedure actor factory to the value of input stream.\n"+
				"This happens to the factory "+typesName+".");
    }


}