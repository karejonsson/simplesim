package se.modlab.simplesim.algorithms.presentation;

import se.modlab.generics.gui.util.*;
import se.modlab.generics.files.*;
import se.modlab.generics.sstruct.values.*;
import java.util.Enumeration;
import java.util.Vector;
import javax.swing.tree.TreeNode;
import javax.swing.*;
import java.awt.Component;
//import java.awt.*;
import se.modlab.generics.sstruct.variables.*;

public class SourceNode implements SimpleExplorerNode
{
  private TreeNode parent;
  private FileCollector fc;

  public SourceNode(TreeNode parent, FileCollector fc)
  {
    this.parent = parent;
    this.fc = fc;
  }

  public Enumeration children()
  {
    return null;
  }

  public boolean getAllowsChildren()
  {
    return false;
  }

  public TreeNode getChildAt(int childIndex)
  {
    return null;
  }

  public int getChildCount()
  {
    return 0;
  }

  public int getIndex(TreeNode tn)
  {
    return -1;
  }

  public TreeNode getParent()
  {
    return parent;
  }

  public boolean isLeaf()
  {
    return false;
  }

  public Component getComponent()
  {
    JTextArea ta = new JTextArea();
    ta.setText(fc.getFilecontents());
    ta.setEditable(false);
    return ta;
  }  

  public String toString()
  {
    return fc.getFilename();
  }

} 
