package se.modlab.simplesim.algorithms.setup;

import java.io.InputStream;
import java.util.Hashtable;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.logics.LogicalExpression;
import se.modlab.simplesim.variables.*;
import se.modlab.simplesim.algorithms.initiators.*;
import se.modlab.simplesim.algorithms.samplers.*;
import se.modlab.simplesim.scoping.*;

public class SubScopeManager implements Manager
{
  
  private String name = null;
  private Manager members[];
  private String filename;
  private int line;
  private int column;
  private SimData sd = null;
  private boolean _exp;
  private boolean _pub;

  public SubScopeManager(String _name,
                         Manager _members[],
                         String _filename,
                         int _line,
                         int _column,
                         SimData _sd,
                         boolean _export,
                         boolean _public)
  {
    name = _name;
    members = _members;
    filename = _filename;
    line = _line;
    column = _column;
    sd = _sd;
    _exp = _export;
    _pub = _public;
  }

  public String getFilename()
  {
    return filename;
  }  

  public int getLine()
  {
    return line;
  }  

  public int getColumn()
  {
    return line;
  }  

  private void _initialize(Scope s, boolean isSharp, Hashtable<VariableInstance, Manager> managermap)
    throws IntolerableException
  {
    SimScope subscope = new SimScope(s, name);

    for(int i = 0 ; i < members.length ; i++) 
    {
      members[i].initialize(subscope, isSharp,managermap);
    }
    //subscope.removeSubjacent();
    SubscopeReference sr = new SubscopeReference(subscope, name, filename, line, column);
    s.addComplexInstance(sr);

    if(_exp)
    {
      Scope gs = sd.getGlobalScope();
      if(gs == s)
      {
        throw new UserCompiletimeError(
          "You may not export variables from global scope.\n"+
          "This happens with the subscope "+name+
          " in file "+filename+" on line "+line+" on column "+column);
      }
      SimScope ss = (SimScope) s;
      ss.addMember(new SubscopeReferenceExported(sr));
    }
    if(_pub)
    {
      Scope gs = sd.getGlobalScope();
      if(gs == s)
      {
        throw new UserCompiletimeError(
          "You may not declare subscopes public in global scope.\n"+
          "This happens with the subscope "+name);
      }
      SimScope ss = (SimScope) s;
      ss.addMember(sr);
    }
	if(managermap != null) {
		managermap.put(sr, this);
	}
  } 

  public void initialize(Scope s, boolean isSharp, Hashtable<VariableInstance, Manager> managermap)
    throws IntolerableException
  {
    try {
      _initialize(s, isSharp, managermap);
    }
	catch(UserRuntimeError ue) {
	      String m = ue.getMessage()+"\n"+"This happened in file "+filename+", line "+line+", column "+column;
	      UserRuntimeError ue2 = new UserRuntimeError(m, ue.getCause(), ue.getCollectors());
		  ue2.setAction(ue.getAction());
		  throw ue2;
	    }
	catch(UserCompiletimeError ue) {
	      String m = ue.getMessage()+"\n"+"This happened in file "+filename+", line "+line+", column "+column;
	      UserCompiletimeError ue2 = new UserCompiletimeError(m, ue.getCause(), ue.getCollectors());
		  ue2.setAction(ue.getAction());
		  throw ue2;
	    }
  }

  public String toString()
  {
    return "subscopeManager for "+name+
           " with export = "+_exp+
           ", public = "+_pub+
           " in file "+filename+" on line "+line+" on column "+column;
  }
  
	public void reinitializeLogical(LogicalExpression le) throws IntolerableException {
		throw new UserRuntimeError(
				"Cannot set a subscope to the value of a boolean expression.\n"+
				"This happens to the subscope "+name+" declared on line "+line+", column "+column+" in the file "+filename);
    }

	public void reinitializeArithmetic(ArithmeticEvaluable ae) throws IntolerableException {
		throw new UserRuntimeError(
				"Cannot set a subscope to the value of an arithmetic expression.\n"+
				"This happens to the subscope "+name+" declared on line "+line+", column "+column+" in the file "+filename);
    }

	public void reinitializeFromInputStream(InputStream is) throws IntolerableException {
		throw new InternalError(
				"Cannot set a subscope to the value of an input stream.\n"+
				"This happens to the subscope "+name+" declared on line "+line+", column "+column+" in the file "+filename);
    }


}