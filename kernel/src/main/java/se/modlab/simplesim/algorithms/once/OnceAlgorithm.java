package se.modlab.simplesim.algorithms.once;

import se.modlab.generics.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.simplesim.variables.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.gui.util.SimpleExplorer;
import se.modlab.generics.gui.util.SimpleExplorerNode;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.simplesim.events.*;
import se.modlab.simplesim.algorithms.share.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;

import javax.swing.*;

import java.util.*;
import java.io.*;

import se.modlab.simplesim.algorithms.setup.*;
import se.modlab.simplesim.algorithms.externalpainters.ExternalPainterFactoryHandle;
import se.modlab.simplesim.algorithms.presentation.*;
import se.modlab.simplesim.scoping.*;

public class OnceAlgorithm extends AlgorithmBase implements Algorithm
{

  private SimpleExplorerNode nodes[];

  public OnceAlgorithm(SimData sd)
  {
    super(sd);
  }

  public String getSurveillanceMessageForDump()
  {
    try
    {
      Commons com = sd.getCommons();
      long l = com.getCurrentEpochSimTime();
      String caltime = Commons.getTimeStringFromLong(l);
      if(!sd.endtimeIsValid())
      {
        return "Simulation time is "+caltime;
      }
      double endtime = sd.getEndTime();
      l = com.getEpochTimeFromSimTime(endtime);
      String s_endtime = com.getTimeStringFromLong(l);
      return "At "+caltime+", going for "+s_endtime;
    }  
    catch(Exception e)
    {
    }
    return "No message in GSMFB";
  }

  public String getSurveillanceMessageForBar()
  {
    try
    {
      Commons com = sd.getCommons();
      long l = com.getCurrentEpochSimTime();
      String caltime = Commons.getTimeStringFromLong(l);
      return "Simulation time is "+caltime;
    }  
    catch(Exception e)
    {
    }
    return "No message in GSMFB";
  }

  public int getCurrent()
  {
    try
    {
      if(!sd.endtimeIsValid())
      {
        return 50;
      }
      Commons com = sd.getCommons();
      double curSimTime = com.getCurrentSimTime();
      double endTime = sd.getEndTime();
      return (int) (100*curSimTime / endTime);
    }  
    catch(Exception e)
    {
      System.out.println("algorithm - once - getCurrent "+e);
    }
    return 0;
  }

  public int getLengthOfTask()
  {
    return 100;
  }

  public void runAlgorithm()
    throws IntolerableException
  {
    try
    {
      setupScenario();
      runSimulation();
     
      //System.out.println("PP "+sd.getNoOfMonitoredVariables());

      SegmentPainter sp = new SegmentPainterOnce();
      FirstLevelTreeNodeBuilderVisitor fltnbv = 
        new FirstLevelTreeNodeBuilderVisitor(sp, ExternalPainterFactoryHandle.get());
      SimScope s = (SimScope) sd.getGlobalScope();
      VariableInstance vis[] = s.getAllInstances();
      for(int i = 0 ; i < vis.length ; i++)
      {
        //System.out.println("onceAlgo - "+vis[i]);
        vis[i].accept(fltnbv);
      } 

      Variables vars = (Variables) fltnbv.getResult();

      node = new TopNode(sd, vars, sp);
      extracts = fltnbv.getExtracts();
      Runtime.getRuntime().runFinalization();
      setDone();
      //System.out.println("Slut - once - runAlgorithm");
    }
    catch(IntolerableException ie)
    {
      setDone();
      throw ie;
    }
    //displayResultsGraphically();
  }

  public void displayResultsGraphically(boolean visible)
  {
    //System.out.println("B�rjar - once - displayResultsGraphically");
    final SimpleExplorer se = new SimpleExplorer("Evolution of variables", 
                                                 node, 
                                                 null);
    se.addWindowListener(
       new WindowAdapter() 
       {
         public void windowClosed(WindowEvent e)
         {
           //System.out.println("onImageIllustrator - window closed");
           // Removing the line below will cause the window to go
           // in some staled state.
           //se.dispose();
           System.gc();
         }
         public void windowClosing(WindowEvent e)
         {
           //System.out.println("onImageIllustrator - window closing");
           // Removing the line below will cause the window to go
           // in some staled state.
           //se.dispose();
           System.gc();
         }
       });
    final SegmentPainter sp = new SegmentPainterOnce();
    JMenuBar mb = se.getJMenuBar();//new JMenuBar();
    //se.setJMenuBar(mb);
    JMenu menu = new JMenu("Report");
    mb.add(menu);
    JMenuItem menuItem = new JMenuItem("Html site");
    menu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          Runnable r = new Runnable()
                       {
                         public void run()
                         {
                           OnceAlgorithm.this.generateReport();
                         }
                       };
          Thread t = new Thread(r);
          t.start();
        }
      }
    );

    Commons com = sd.getCommons();
    final CrossplotAdder cpa = node.getCrossplotsNode();
    final OverloadAdder ola = node.getOverloadsNode();
    JMenu plots = new JMenu("Plots");
    mb.add(plots);
    bp = new Biplot(se, false, extracts, cpa, sp);
    menuItem = new JMenuItem("biplot");
    plots.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          bp.setVisible(true);
        }
      });
    op = new OverlayPlot(se, false, extracts, ola, sp);
    menuItem = new JMenuItem("overlay plot");
    plots.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          op.setVisible(true);
        }
      });
    JMenu exports = new JMenu("Export");
    mb.add(exports);
    etv = new ExportTwoVariables(se, false, extracts);
    menuItem = new JMenuItem("Export 2 variables");
    exports.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          etv.setVisible(true);
        }
      });
    se.pack();
    se.setVisible(visible);

  }

	public Manager[] getManagerArray() {
		return sd.getManagerArray();
	}

}