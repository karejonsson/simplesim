package se.modlab.simplesim.algorithms.presentation;

import se.modlab.generics.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import java.util.Enumeration;
import java.util.Vector;
import javax.swing.tree.TreeNode;
import javax.swing.JTextArea;
import java.awt.Component;
import se.modlab.simplesim.variables.*;

public class OneArithmeticVariable extends VariableNode
{
  private String fullName = null;
  private Object v;
  private SegmentPainter sp;

  public OneArithmeticVariable(String _fullName,
                               Object _v, 
                               TreeNode _parent, 
                               SegmentPainter _sp)
  {
	  super(_parent);
    fullName = _fullName;
    v = _v; 
    sp = _sp;
    parent = _parent;
  }

  public Component getComponent()
  {
    return new JTextArea(v.toString());
  }  

  public String toString()
  {
    return fullName;
  }

} 
