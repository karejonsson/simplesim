package se.modlab.simplesim.algorithms.setup;

import java.io.InputStream;
import java.util.Hashtable;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.simplesim.variables.*;
import se.modlab.simplesim.algorithms.initiators.*;
import se.modlab.simplesim.algorithms.samplers.*;
import se.modlab.simplesim.scoping.*;

public class BooleanManager implements Manager
{

	private String name = null;
	private LogicalExpression le;
	private String filename;
	private int line;
	private int column;
	private SimData sd = null;
	private boolean _con;
	private boolean _mon;
	private boolean _exp;
	private boolean _pub;
	public static final String type = "boolean";
	
	private Scope lastSeenScope = null;
	private BooleanVariable var = null;

	public BooleanManager(String _name,
			LogicalExpression _le,
			String _filename,
			int _line,
			int _column,
			SimData _sd,
			boolean _const,
			boolean _monitor,
			boolean _export,
			boolean _public)
	{
		name = _name;
		le = _le;
		filename = _filename;
		line = _line;
		column = _column;
		sd = _sd;
		_con = _const;
		_mon = _monitor;
		_exp = _export;
		_pub = _public;
	}

	public String getFilename()
	{
		return filename;
	}  

	public int getLine()
	{
		return line;
	}  

	public int getColumn()
	{
		return line;
	}  

	private void _initialize(Scope s, boolean isSharp, Hashtable<VariableInstance, Manager> managermap)
	throws IntolerableException
	{
		lastSeenScope = s;
		//booleanVariable var;
		VariableFactory vf = s.getFactory(type);
		if(vf == null)
		{
			throw new InternalError("No factory for type "+type+" found."); 
		}
		//System.out.println("Initialize in "+s.getName()+" for "+toString());
		if(_mon)
		{
			SimBooleanMonitoredVariable tmp = 
				new SimBooleanMonitoredVariable(name, 
						vf,
						filename, line, column, sd.getCommons());
			var = tmp;
		}
		else
		{
			if(!_con)
			{
				var = new BooleanVariable(name, vf, filename, line, column);
			}
			else
			{
				var = new BooleanConst(name, vf, filename, line, column);
			}
		}
		if(isSharp)
		{
			sd.addInitiator(new BooleanInitiator(var, s, le));
			sd.addEndValueSampler(new BooleanEndValueSampler(var));
			if(_mon) 
			{
				//sd.addMonitoredVariable((monitoredVariable) var);
			}
		}
		sValue val = null;
		if(le != null)
		{
			val = new sBoolean(le.evaluate(s));
		}
		else
		{
			val = new sBoolean(true);
		}
		var.setInitialValue(val);
		s.addVariable(var);
		if(_exp)
		{
			Scope gs = sd.getGlobalScope();
			if(gs == s)
			{
				throw new UserCompiletimeError(
						"You may not export variables from global scope.\n"+
						"This happens with the variable "+name+", file "+filename+", line "+line+".");
			}
			SimScope ss = (SimScope) s;
			ss.addMember(new SimBooleanExported((BooleanVariable) var));
		}
		if(_pub)
		{
			Scope gs = sd.getGlobalScope();
			if(gs == s)
			{
				throw new UserCompiletimeError(
						"You may not declare variables public in global scope.\n"+
						"This happens with the variable "+name+", file "+filename+", line "+line+".");
			}
			SimScope ss = (SimScope) s;
			ss.addMember(var);
		}
		if(managermap != null) {
			managermap.put(var, this);
		}
	} 

	public void initialize(Scope s, boolean isSharp, Hashtable<VariableInstance, Manager> managermap)
	throws IntolerableException
	{
		try {
			_initialize(s, isSharp, managermap);
		}
		catch(UserRuntimeError ue) {
			String m = ue.getMessage()+"\n"+"This happened in file "+filename+", line "+line+", column "+column;
			UserRuntimeError ue2 = new UserRuntimeError(m, ue.getCause(), ue.getCollectors());
			ue2.setAction(ue.getAction());
			throw ue2;
		}
		catch(UserCompiletimeError ue) {
			String m = ue.getMessage()+"\n"+"This happened in file "+filename+", line "+line+", column "+column;
			UserCompiletimeError ue2 = new UserCompiletimeError(m, ue.getCause(), ue.getCollectors());
			ue2.setAction(ue.getAction());
			throw ue2;
		}
	}

	public String toString()
	{
		return "booleanManager for "+name+
		" with const = "+_con+
		", monitor = "+_mon+
		", export = "+_exp+
		", public = "+_pub+
		" in file "+filename+" on line "+line+" on column "+column;
	}
	
	public void reinitializeLogical(LogicalExpression _le) throws IntolerableException {
	    sValue val = null;
	    le = _le;
	    Initiator itor = sd.getInitiatorFromVariable(var);
	    if(itor == null) {
			throw new InternalError(
					"Cannot find the initiator.\n"+
					"This happens to the variable "+name+" declared on line "+line+", column "+column+" in the file "+filename);
	    }
	    if(!(itor instanceof BooleanInitiator)) {
			throw new InternalError(
					"A boolean variable is not initiated with an arithmetic initiator.\n"+
					"This happens to the variable "+name+" declared on line "+line+", column "+column+" in the file "+filename);
	    }
	    BooleanInitiator bitor = (BooleanInitiator) itor;
	    bitor.setInitatingExpression(le);
    }

	public void reinitializeArithmetic(ArithmeticEvaluable ae) throws IntolerableException {
		throw new UserRuntimeError(
				"Cannot set an boolean variable to the value of an logical expression.\n"+
				"This happens to the variable "+name+" declared on line "+line+", column "+column+" in the file "+filename);
    }

	public void reinitializeFromInputStream(InputStream is) throws IntolerableException {
		throw new InternalError(
				"Cannot set boolean to the value of input stream.\n"+
				"This happens to the boolean "+name+" declared on line "+line+", column "+column+" in the file "+filename);
    }


}