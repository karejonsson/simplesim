package se.modlab.simplesim.algorithms.setup;

import java.io.InputStream;
import java.util.Hashtable;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.simplesim.algorithms.initiators.*;
import se.modlab.simplesim.algorithms.samplers.*;
import se.modlab.simplesim.events.*;
import se.modlab.simplesim.scoping.*;
import se.modlab.simplesim.variables.*;

public class RepeatingVectorManager extends RepeatingManager
{

  private ArithmeticEvaluable ae_vector;

  public RepeatingVectorManager(String _type, String _name,
                                ArithmeticEvaluable _ae_vector,
                                SimData _sd, String filename, int line, int column)
  {
    super(_type, _name, _sd, filename, line, column);
    ae_vector = _ae_vector;
  }

  private void _initialize(Scope s, boolean isSharp, Hashtable<VariableInstance, Manager> managermap)
    throws IntolerableException
  {
    VariableFactory _vf = s.getFactory(type);
    if(!(_vf instanceof ProcedureRepeatingFactory))
    {
      new InternalError("Not repeating type of factory!\n"+
                        "It was of type "+_vf.getClass().getName());
    }
    ProcedureRepeatingFactory vf = (ProcedureRepeatingFactory) _vf;

    sValue sLen = ae_vector.evaluate(s);
    if(!(sLen instanceof sLong))
    {
      throw new UserCompiletimeError("Expression for length of vector "+name+
         " does not evaluate to a long value.\n"+
         "It became "+sLen); 
    }
    long length = ((sLong) sLen).getLong().longValue();
    SimVectorVariableFactory svvf = new SimVectorVariableFactory(vf , (int) length);
    VariableVector vv = (VariableVector) svvf.getInstance(name, false, filename, line, column, managermap);
    s.addComplexInstance(vv);
    if(isSharp)
    {
      for(int i = 0 ; i < vv.getLength(); i ++)
      {
        ProcedureRepeating pr = (ProcedureRepeating) vv.getVectorElement(i);
        sd.addQueuable(pr);
        sd.addInitiator(new QueableInitiator(pr));
      }
    }
	if(managermap != null) {
		managermap.put(vv, this);
	}
  }
  
  public void initialize(Scope s, boolean isSharp, Hashtable<VariableInstance, Manager> managermap)
  throws IntolerableException
  {
    try {
      _initialize(s, isSharp,managermap);
    }
	catch(UserRuntimeError ue) {
	      String m = ue.getMessage()+"\n"+"This happened in file "+filename+", line "+line+", column "+column;
	      UserRuntimeError ue2 = new UserRuntimeError(m, ue.getCause(), ue.getCollectors());
		  ue2.setAction(ue.getAction());
		  throw ue2;
	    }
	catch(UserCompiletimeError ue) {
	      String m = ue.getMessage()+"\n"+"This happened in file "+filename+", line "+line+", column "+column;
	      UserCompiletimeError ue2 = new UserCompiletimeError(m, ue.getCause(), ue.getCollectors());
		  ue2.setAction(ue.getAction());
		  throw ue2;
	    }
  }

	public void reinitializeLogical(LogicalExpression le) throws IntolerableException {
		throw new UserRuntimeError(
				"Cannot set an repeater vector instance to the value of a boolean expression.\n"+
				"This happens to the "+name+" declared on line "+line+", column "+column+" in the file "+filename);
    }

	public void reinitializeArithmetic(ArithmeticEvaluable ae) throws IntolerableException {
		throw new UserRuntimeError(
				"Cannot set an repeater vector instance to the value of an arithmetic expression.\n"+
				"This happens to the vector "+name+" declared on line "+line+", column "+column+" in the file "+filename);
    }

	public void reinitializeFromInputStream(InputStream is) throws IntolerableException {
		throw new InternalError(
				"Cannot set an repeater vector instance to the value of input stream.\n"+
				"This happens to the vector "+name+" declared on line "+line+", column "+column+" in the file "+filename);
    }

}


