package se.modlab.simplesim.algorithms.illustrate.onimage;

import se.modlab.simplesim.algorithms.illustrate.share.*;
import se.modlab.generics.files.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.util.Services;
import se.modlab.simplesim.events.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import se.modlab.generics.exceptions.*;

public class OnImageIllustrator extends JComponent implements Illustrator
{ 

  private FileCollector fc;
  private Vector<IllustrationUnit> siunits = 
    new Vector<IllustrationUnit>();
  private Hashtable<String, Vector<DynamicIllustrationFactory>> diunitfactories = 
    new Hashtable<String, Vector<DynamicIllustrationFactory>>();
  private Hashtable<VariableInstance, Vector<IllustrationUnitFactoried>> diunitsactive = 
    new Hashtable<VariableInstance, Vector<IllustrationUnitFactoried>>();
  private ImageIcon back = null;
  private Scope gs = null;
  private IntolerableException ie;
  private JFrame frame = null;
  private boolean ready = false;
 
  public OnImageIllustrator(String filename)
    throws IntolerableException
  {
    byte image[] = Services.getCleartextFromFile(filename);
    if(image == null)
    {
      throw new UserCompiletimeError(
        "The file "+filename+"\n"+
        "does not exist!");
    }
    //System.out.println("Image = "+image.length+", filename = "+filename);
    back = new ImageIcon(image);
  }

  public void addStaticIllustrationUnit(IllustrationUnit iu)
  {
    siunits.addElement(iu);
  }

  public void addDynamicIllustrationFactory(DynamicIllustrationFactory dif)
  {
    Vector<DynamicIllustrationFactory> v = diunitfactories.get(dif.getFactoryName());
    if(v == null)
    {
      v = new Vector<DynamicIllustrationFactory>();
      diunitfactories.put(dif.getFactoryName(), v);
    }
    v.add(dif);
    //diunitfactories.put(dif.getFactoryName(), dif);
  }

  public void init()
    throws IntolerableException
  {
    frame = new JFrame();
    frame.getContentPane().add(this);
    frame.pack();
    Insets is = frame.getInsets();
    int width = back.getIconWidth();
    int height = back.getIconHeight();
    frame.setSize(width+is.left+is.right, height+is.top+is.bottom);
    frame.setVisible(true);
    frame.addWindowListener(
       new WindowAdapter() 
       {
         public void windowClosed(WindowEvent e)
         {
           //System.out.println("onImageIllustrator - window closed");
           if(OnImageIllustrator.this.frame == null) return;
           OnImageIllustrator.this.frame.dispose();
           OnImageIllustrator.this.frame = null;
           System.gc();
         }
         public void windowClosing(WindowEvent e)
         {
           //System.out.println("onImageIllustrator - window closing");
           if(OnImageIllustrator.this.frame == null) return;
           OnImageIllustrator.this.frame.dispose();
           OnImageIllustrator.this.frame = null;
           System.gc();
         }
       });
    for(int i = 0 ; i < siunits.size() ; i++)
    {
      IllustrationUnit iu = siunits.elementAt(i);
      iu.init(width, height);
    }
  }

  public void setGlobalScope(Scope s)
    throws IntolerableException
  {
    gs = s;
    //System.out.println("onImageIllustrator - setGlobalScope - cosinus = "+(s.getVariable("cosinus") == null));
    for(int i = 0 ; i < siunits.size() ; i++)
    {
      IllustrationUnit iu = siunits.elementAt(i);
      iu.setGlobalScope(s);
    }
    ready = true;
  }

  public void updated(SimulationEvent se)
  {
    //System.out.println("onImageIllustrator - name "+se.getEventName()+
    //                   ", Type "+se.getEventTypeName());
    VariableInstance vi = null;
    if(!(se instanceof VariableInstance))
    {
      if(se instanceof RunnableQueuer)
      {
        RunnableQueuer er = (RunnableQueuer) se;
        vi = er.getActor();
      }
      else 
      {
        //System.out.println("Not variable instace");
        return;
      }
    }
    else
    {
      vi = (VariableInstance) se;
    }
    String typename = vi.getType().getTypesName();
    //System.out.println("Types name "+typename);
    if(diunitsactive.get(vi) != null)
    {
      //System.out.println("onImageIllustrator - updated - remove 1");
      if(!(vi instanceof Enqueuable)) return;
      //System.out.println("onImageIllustrator - updated - remove 2");
      //enqueuable e = (enqueuable) vi;
      if(!se.shallBeRequeued())
      {
        diunitsactive.remove(se);
        //System.out.println("onImageIllustrator - updated - remove 3");
      }
      return; // Already represented
    }
    //System.out.println("Object "+vi+" not yet represented.");
    Vector<DynamicIllustrationFactory> difs = diunitfactories.get(typename);
    if(difs == null) return; // Type not mapped on graphic
    //System.out.println("Object "+vi+"of type "+typename+" shall be visualized.");
    IllustrationUnitFactoried iuf = null;
    Vector<IllustrationUnitFactoried> iufs = 
       new Vector<IllustrationUnitFactoried>();
    for(int i = 0 ; i < difs.size() ; i++)
    {
      try
      {
        DynamicIllustrationFactory dif = difs.get(i);
        iuf = dif.getInstance(vi);
        int width = back.getIconWidth();
        int height = back.getIconHeight();
        iuf.init(width, height);
        iufs.add(iuf);
      }
      catch(IntolerableException _ie)
      {
        ie = _ie;
        return;
      }
      diunitsactive.put(vi, iufs);
    };
  }
  private int ctr = 0;

  public void paint(Graphics g)
  {
    //System.out.println("onImageIllustrator - size = "+diunitsactive.size());
    ctr++;
    if(!ready) return;
    if(ie != null) 
    {
      //g.drawString(ie.getMessage(), 10, 10);
      frame.setVisible(false);
      return;
    }
    g.drawImage(back.getImage(), 0, 0, this);
    try
    {
      for(int i = 0 ; i < siunits.size() ; i++)
      {
        IllustrationUnit iu = siunits.elementAt(i);
        try
        {
          iu.paint(g);
        }
        catch(NullPointerException e)
        {
          ie = new UserCompiletimeError(
            "Cannot display the static illustration unit "+
            iu.getName(), e);
        }
      }
      Enumeration<Vector<IllustrationUnitFactoried>> dyns = diunitsactive.elements();
      //System.out.println("1");
      while(dyns.hasMoreElements())
      {
        //System.out.println("2");
      //for(int i = 0 ; i < diunitsactive.size() ; i++)
      //{
        Vector<IllustrationUnitFactoried> iufs = dyns.nextElement();
        if(iufs == null) continue;
        //illustrationUnitFactoried iuf = diunitsactive.elementAt(i);
        for(int i = 0 ; i < iufs.size() ; i++)
        {
          try
          {
            iufs.elementAt(i).paint(g);
          }
          catch(NullPointerException e)
          {
            ie = new UserCompiletimeError(
              "Cannot display the dynamic illustration unit "+
              iufs.elementAt(i).getName(), e);
          }
        }
      }
    }
    catch(IntolerableException _ie)
    {
      //System.out.println("onImageIllustrator - "+ctr+" : "+_ie);
      ie = _ie;
    }
  }

  public IntolerableException getException()
  { 
    return ie;
  }

}

