package se.modlab.simplesim.algorithms.bruteforcemax;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.simplesim.algorithms.share.*;
import java.awt.*;
import java.util.*;

public class ResultRow
{

  public static Comparator comp = 
    new Comparator()
    {
      public int compare(Object first, Object second)
      {
        ResultRow rrf = (ResultRow) first;
        ResultRow rrs = (ResultRow) second;
        if(rrf.getResult() - rrs.getResult() > 0) return -1;
        if(rrf.getResult() - rrs.getResult() < 0) return 1;
        return 0;
      }
    };
 

  private ValuesToExamine ortogonals[];
  private Vector v = new Vector();
  private double result;

  public ResultRow(ValuesToExamine ortogonals[], sValue result)
    throws IntolerableException
  {
    this.ortogonals = ortogonals;
    for(int i = 0 ; i < ortogonals.length ; i++)
    {
      v.addElement(new holder(ortogonals[i].getIncrementor(),
                              ortogonals[i].getVariable().getValue().getValue()));
    }
    this.result = result.getValue();
  }
 
  public int getLength()
  {
    return ortogonals.length;
  }

  public String getValueAt(int idx)
  {
    if(idx == ortogonals.length) return ""+result;
    if(idx == ortogonals.length+1) return "Go again";
    return v.elementAt(idx).toString();
  }

  public String getName(int idx)
  {
    if(idx < 0) return null;
    if(idx >= getLength()) return null;
    return ortogonals[idx].getVariable().getName();
  }

  public double getResult() 
  {
    return result;
  }

  public void setResettersBack()
    throws IntolerableException
  {
    for(int i = 0 ; i < ortogonals.length ; i++)
    {
      holder h = (holder) v.elementAt(i);
      ortogonals[i].setIncrementor(h._incrementor);
      //System.out.println("rr set back "+h._incrementor);
    }
  }

  private class holder 
  {
    public int _incrementor;
    public double d;
    public holder(int _incrementor, double d)
    {
      this._incrementor = _incrementor;
      this.d = d;  
    }
    public String toString()
    {
      return "Seq = "+_incrementor+", val = "+d;
    }
  }

  public String toString()
  {
    StringBuffer sb = new StringBuffer();
    sb.append(ortogonals[0].toString());
    for(int i = 1 ; i < ortogonals.length ; i++)
    {
      sb.append("\n"+ortogonals[i].toString());
    }
    return sb.toString();
  }

}