package se.modlab.simplesim.algorithms.setup;

import java.util.*;

import se.modlab.generics.files.*;
import se.modlab.generics.sstruct.strings.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;
import se.modlab.simplesim.references.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.events.*;
import se.modlab.simplesim.scoping.*;
import se.modlab.simplesim.algorithms.initiators.*;
import se.modlab.simplesim.algorithms.samplers.*;

public class SimData 
{

	SimScope globalScope = null;
	private Vector<Manager> managers = new Vector<Manager>();
	private Commons com = new Commons();
	private String program = null;
	private ScopeFactory sf = null;
	private String programHeader = null;
	private double stopSimTime = -1;
	private Vector<Initiator> initiators = new Vector<Initiator>();
	private Vector<EndValueSampler> samplers = new Vector<EndValueSampler>();
	private Vector monitored = new Vector();
	private Vector initially = new Vector();
	private FinallyEvent _finally = null;
	private Vector fileCollectors = new Vector();
	private SimulationEventQueue eq = new SimulationEventQueue();
	private ProcedureSingle sgs = null;

	private Vector triggers = new Vector();
	private Vector queable = new Vector();

	public SimData(String program, ScopeFactory _sf) throws IntolerableException
	{ 
		//System.out.println("se.modlab.simplesim.algorithms.setup.SimData.<ctor>");
		this.program = program;
		sf = _sf;
		globalScope = (SimScope) sf.getInstance("Global");
		new ScopeReference(globalScope, "globals", "", -1, -1);
		globalScope.addFactory(new SimBooleanVariableFactory(com));
		globalScope.addFactory(new SimDoubleVariableFactory(com));
		globalScope.addFactory(new SimLongVariableFactory(com));
		globalScope.addFactory(
				new UntypedQueuableReferenceFactory(globalScope));
		globalScope.addFactory(
				new UntypedStructReferenceFactory(globalScope));
	}

	public String getProgram()
	{
		if(programHeader != null)
		{
			return "/*\n"+programHeader+"\n*/\n"+program;
		}
		return program;
	}

	public void setProgramHeader(String header)
	{
		programHeader = header;
	}

	public Commons getCommons()
	{
		return com;
	}

	public Scope getGlobalScope()
	{
		return globalScope;
	}

	public void setTimeUnit(ArithmeticEvaluable ae)
	throws IntolerableException
	{
		sValue t = null;
		try
		{
			t = ae.evaluate(globalScope);
		}
		catch(IntolerableException e)
		{
			throw e;
		}
		catch(Exception e)
		{
			throw new UnclassedError(
					"Unexpected exception in method SimData.setTimeUnit()", e);
		}
		com.setTimeUnit((long) t.getValue());
	}

	public void setStartTime(String t) throws InternalError
	{
		if(t == null) {
			// Defaults to now!
			com.setEpochStartTime(new Date().getTime());
			return;
		}
		if(t.trim().length() == 0) {
			// Defaults to now!
			com.setEpochStartTime(new Date().getTime());
			return;
		}

		long timeInit = Commons.getTimeLongFromString(t);
		com.setEpochStartTime(timeInit);
	}

	public void setEndTimeAsDatetimeLiteral(String t)
	throws IntolerableException
	{
		long timeStop = Commons.getTimeLongFromString(t);
		stopSimTime = com.getSimTimeFromEpochTime(timeStop);
		ProgramBlock p = new ProgramBlock(sf);
		StringContainer ccc = new StringContainer();
		ccc.addStringEvaluable(new StringLiteral("\"Stop as stipulated in settings.\""));
		p.addStatement(new StopSimulation(ccc));
		ProcedureSingle sgs_tobe = new ProcedureSingle("Settings generated stopper", "", -1, -1, null, p, stopSimTime, sf);
		if(sgs != null) {
			// If endtime is changed by the endtime parameter of a guuiapp then it must also be added to queue.
			 queable.removeElement(sgs);
			 sgs = sgs_tobe;
			 addQueuable(sgs);
		}
		else {
			// Not yet queued up. Cion wait until setupScenario is called.
			sgs = sgs_tobe;
		}
	}

	public void setEndTimeAsArithmeticExpression(ArithmeticEvaluable ae)
	throws IntolerableException
	{
		sValue t = null;
		try
		{
			t = ae.evaluate(globalScope);
		}
		catch(IntolerableException e)
		{
			throw e;
		}
		catch(Exception e)
		{
			throw new UnclassedError(
					"Unexpected exception in method SimData.setEndTimeAsArithmeticExpression()", e);
		}
		stopSimTime = t.getValue();
		ProgramBlock p = new ProgramBlock(sf);
		StringContainer ccc = new StringContainer();
		ccc.addStringEvaluable(new StringLiteral("\"Stop as stipulated in settings.\""));
		p.addStatement(new StopSimulation(ccc));
		ProcedureSingle sgs_tobe = new ProcedureSingle("Settings generated stopper", "", -1, -1, null, p, stopSimTime, sf);
		if(sgs != null) {
			// If endtime is changed by the endtime parameter of a guuiapp then it must also be added to queue.
			 queable.removeElement(sgs);
			 sgs = sgs_tobe;
			 addQueuable(sgs);
		}
		else {
			// Not yet queued up. Cion wait until setupScenario is called.
			sgs = sgs_tobe;
		}
	}

	public ProcedureSingle getSettingsGeneratedStopper()
	{
		return sgs;
	}

	public boolean endtimeIsValid()
	{
		return stopSimTime != -1;
	}

	public double getEndTime()
	throws IntolerableException
	{
		if(stopSimTime == -1)
		{
			throw new InternalError(
					"Endtime not defined in parse-session but later demanded.");
		}
		return stopSimTime;
	}
	/*
  public void addRepeating(String name,
                           scope localScope, 
                           arithmeticEvaluable ae_first,
                           arithmeticEvaluable ae_again,
                           logicalExpression requeue,
                           program p)
    throws intolerableException
  {
    repeating.addElement(new procedureRepeating(name,
                                                localScope,
                                                ae_first, 
                                                ae_again, 
                                                requeue, 
                                                p, 
                                                com,
                                                sf));
  }

  public Vector getRepeating()
  {
    return repeating;
  }
	 */

	public void addManager(Manager mgr)
	{
		managers.addElement(mgr);
	}

	public void addManagers(Manager mgr[])
	{
		for(int i = 0 ; i < mgr.length ; i++)
		{
			managers.addElement(mgr[i]);
		}
	}

	public Vector<Manager> getAllManagers()
	{
		return managers;
	}

	public Manager[] getManagerArray() {
		Manager out[] = new Manager[managers.size()];
		for(int i = 0 ; i < out.length ; i++) {
			out[i] = managers.elementAt(i);
		}
		return out;
	}

	public void setName(String name)
	{
		com.setName(name);
	}

	public String getName()
	{
		return com.getName();
	}

	public void addInitiator(Initiator inr)
	{
		initiators.addElement(inr);
	}

	public Initiator getInitiatorFromVariable(VariableInstance vi) {
		for(int i = 0 ; i < initiators.size() ; i++) {
			if(vi == initiators.get(i).getInstance()) {
				return initiators.get(i);
			}
		}
		return null;
	}

	public void removeInitiator(VariableInstance vi)
	{
		Initiator i = getInitiatorFromVariable(vi);
		if(i != null) {
			initiators.remove(i);
		}
	}

	public void addEndValueSampler(EndValueSampler evs)
	{
		samplers.addElement(evs);
	}

	public EndValueSampler getSampler(VariableInstance _vi)
	{
		for(int i = 0 ; i < samplers.size() ; i++)
		{
			EndValueSampler evs = samplers.elementAt(i);
			VariableInstance vi = evs.getInstance();
			if(vi == _vi) return evs;
			if(evs instanceof ComplexEndValueSampler)
			{
				EndValueSampler _evs[] = ((ComplexEndValueSampler) evs).getSamplers();
				for(int j = 0 ; j < _evs.length ; j++)
				{
					vi = _evs[j].getInstance();
					if(vi == _vi) return _evs[j];
				}
			}
		}
		return null;
	}

	public EndValueSampler removeEndValueSampler(VariableInstance _vi) {
		EndValueSampler sampler = getSampler(_vi);
		if(sampler != null) {
			samplers.removeElement(sampler);
			return sampler;
		}
		return null;
	}

	/*
  public void addMonitoredVariable(monitoredVariable mv)
  {
    monitored.addElement(mv);

  }
	 */
	 public void resetAllPermanentVariables()
	 throws IntolerableException
	 {
		 //System.out.println("SimData.resetAllPermanentVariables");
		 for(int i = 0; i < initiators.size() ; i++)
		 {
			 Initiator inr = (Initiator) initiators.elementAt(i);
			 //System.out.println("Initiator "+i+" "+inr);
			 inr.runInitiation();
		 }
	 }

	 public void samplingtAllPermanentVariables()
	 throws IntolerableException
	 {
		 for(int i = 0; i < samplers.size() ; i++)
		 {
			 EndValueSampler r = (EndValueSampler) samplers.elementAt(i);
			 r.runSampling();
		 }
	 }

	 public void setAveragesAllPermanentVariables()
	 throws IntolerableException
	 {
		 for(int i = 0; i < samplers.size() ; i++)
		 {
			 EndValueSampler r = (EndValueSampler) samplers.elementAt(i);
			 r.runSetAverageValue();
		 }
	 }

	 public Vector getAllPermanentResetters()
	 {
		 //System.out.println("SimData - getAllPermanentResetters "+allPermanentVariables.size());    
		 return initiators;
	 }

	 public void setAllPermanentResetters(Vector _initiators)
	 {
		 //System.out.println("SimData - setAllPermanentResetters "+allPermanentVariables.size());
		 initiators = _initiators;
	 }

	 /*
  public variableInstance getMonitoredVariable(int index)
  {
    if(index < 0) return null;
    if(index >= monitored.size()) return null;
    return (variableInstance) monitored.elementAt(index);
  }
	  */

	 public int getNoOfMonitoredVariables()
	 {
		 return monitored.size();
	 }

	 public void addInitially(InitiallyEvent p)
	 {
		 initially.addElement(p);
	 }

	 public Vector getInitially()
	 {
		 return initially;
	 }

	 public void setFinally(FinallyEvent p)
	 {
		 _finally = p;
	 }

	 public FinallyEvent getFinally()
	 {
		 return _finally;
	 }

	 public FileCollector[] getCollectors()
	 {
		 FileCollector cls[] = new FileCollector[fileCollectors.size()];
		 for(int i = 0 ; i < fileCollectors.size() ; i++)
		 {
			 cls[i] = (FileCollector) fileCollectors.elementAt(i);
		 }
		 return cls;
	 }

	 public boolean addCollector(FileCollector fc)
	 {
		 String name = fc.getFilename();
		 for(int i = 0 ; i < fileCollectors.size() ; i++)
		 {
			 FileCollector _fc = (FileCollector) fileCollectors.elementAt(i);
			 String _name = _fc.getFilename();
			 if(name.compareTo(_name) == 0)
			 {
				 if(name.endsWith(".esim"))
				 {
					 return false;
				 }
				 return true;
			 }
		 }
		 fileCollectors.addElement(fc);
		 return true;
	 }

	 public SimulationEventQueue getQueue()
	 {
		 return eq;
	 }

	 public void addQueuable(ProcedureSingle ps)
	 {
		 queable.addElement(ps);
	 }

	 public void addQueuable(ProcedureRepeating ps)
	 {
		 queable.addElement(ps);
	 }

	 public void addQueuable(ProcedureActor ps)
	 {
		 queable.addElement(ps);
	 }

	 public void addQueuable(ProcedureTrigged ps)
	 {
		 triggers.addElement(ps);
	 }

	 public Vector getQueuables()
	 {
		 return queable;
	 }

	 public Vector getTrigged()
	 {
		 Vector v = new Vector();
		 for(int i = 0 ; i < triggers.size() ; i++)
		 {
			 v.addElement(triggers.elementAt(i));
		 }
		 return v;
	 }

}

