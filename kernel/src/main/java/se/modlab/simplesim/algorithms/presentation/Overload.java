package se.modlab.simplesim.algorithms.presentation;

import se.modlab.generics.*;
import se.modlab.generics.bshro.ifc.HierarchyObject;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.jpegcoding.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.util.*;
import java.io.*;

public class Overload extends JComponent 
{
  protected static final int margin = 20;

  protected MonitoredVariableExtracted mve1;
  protected MonitoredVariableExtracted mve2;
  protected double min_val;
  protected double max_val;
  private OverloadAdder ola;
  private SegmentPainter sp;

  private double  h_val;
  private int linesBelow = 4;
  private int h_pix;
  private int w_pix;
  private double endTime;
  private String startTimeTextual;
  private String endTimeTextual;

  public Overload(MonitoredVariableExtracted mve1, 
                  MonitoredVariableExtracted mve2, 
                  OverloadAdder ola,
                  SegmentPainter sp)
  {
    this.mve1 = mve1;
    this.mve2 = mve2;
    this.ola = ola;
    this.sp = sp;
    min_val = Math.min(mve1.getMinValue(), mve2.getMinValue());
    max_val = Math.max(mve1.getMaxValue(), mve2.getMaxValue());
    addMouseMotionListener(new ToolTipMouseMotionListener());
    addMouseListener(new PopupListener());

    endTime = Math.max(mve1.getEndTime(), mve2.getEndTime());
    endTimeTextual = mve1.getEndTimeTextual();
    if(mve2.getEndTime() > mve1.getEndTime()) endTimeTextual = mve2.getEndTimeTextual();
    startTimeTextual = mve1.getStartTimeTextual();
  }

  protected void drawFrame(Graphics g, Dimension d, int h_pix)
    throws IntolerableException
  {
    // Frame upper
    g.drawLine((int) margin/2, (int) margin/2,
               (int) (d.width - margin/2), (int) margin/2); 
    // Frame lower
    g.drawLine((int) margin/2, h_pix+(int) margin/2,
               (int) (d.width - margin/2), h_pix+(int) margin/2); 
    // Frame left
    g.drawLine((int) margin/2, (int) margin/2,
               (int) margin/2, h_pix+(int) margin/2); 
    // Frame right
    g.drawLine((int) (d.width - margin/2), (int) margin/2,
               (int) (d.width - margin/2), h_pix+(int) margin/2); 
  }

  public String toString()
  {
    return "Overload of "+mve1.getName()+" and "+mve2.getName();
  }

  protected void drawText(Graphics g, Dimension d, int h_pix)
    throws IntolerableException
  {
    g.drawString("Min value: "+min_val, margin, h_pix+((int)(1.5*margin)));
    g.drawString("Max value: "+max_val, margin, h_pix+((int)(2.5*margin)));

    g.drawString("Start time: "+mve1.getStartTimeTextual(), 
                 margin, h_pix+((int)(3.5*margin)));
    g.drawString("End time: "+endTimeTextual, 
                 d.width/2, h_pix+((int)(3.5*margin)));
  }

  public void paint(Graphics g, Dimension d)
  {
    try
    {
      h_val = max_val - min_val;
      int linesBelow = 4;
      h_pix = d.height-margin*linesBelow;
      w_pix = margin;
      double factor = h_pix / h_val;
      if(mve1.getSize() == 1) 
      {
        g.drawString("The initial value "+mve1.getValue(0)+" is never changed \n"+
                     "in variable "+mve1.getName(),
                     margin, margin);
        drawText(g, d, h_pix);
        return;
      }
      if(mve2.getSize() == 1) 
      {
        g.drawString("The initial value "+mve2.getValue(0)+" is never changed \n"+
                     "in variable "+mve2.getName(),
                     margin, margin);
        drawText(g, d, h_pix);
        return;
      }

      drawFrame(g, d, h_pix);

      DrawingParameters dp1 = new DrawingParameters(w_pix, h_pix, min_val, h_val,
                                                   endTime, margin, mve1);

      int size = mve1.getSize();
      for(int i = 0 ; i < mve1.getSize()-1 ; i++)
      {
        sp.paint(g, d, dp1, i);
      }
      sp.paintLast(g, d, dp1);
 
      DrawingParameters dp2 = new DrawingParameters(w_pix, h_pix, min_val, h_val,
                                                   endTime, margin, mve2);

      for(int i = 0 ; i < mve2.getSize()-1 ; i++)
      {
        sp.paint(g, d, dp2, i);
      }
      sp.paintLast(g, d, dp2);

      drawText(g, d, h_pix);
    }
    catch(IntolerableException ie)
    {
      UniversalTellUser.general(null, ie);
    }
    
  }

  public void paint(Graphics g)
  {
    paint(g, getSize());
  }

  protected void mouseMoved(MouseEvent e)
  {
	  try {
    if(mve1.getSize() == 1) return;
    if(mve2.getSize() == 1) return;
    Dimension d = getSize();
    int w_pix = MonitoredSimVariable.margin;
    int x = e.getX();
    double time = (x - w_pix)*endTime/(d.width-2*w_pix);
    String sttext = startTimeTextual;
    int colons = sttext.indexOf("::");
    if(colons != -1) sttext = sttext.substring(0, colons-1);
    long stlong = Commons.getTimeLongFromString(sttext);
    String ettext = endTimeTextual;
    colons = ettext.indexOf("::");
    if(colons != -1) ettext = ettext.substring(0, colons-1);
    long etlong = Commons.getTimeLongFromString(ettext);
    long epochtime = stlong+((long) ((etlong-stlong)*time/endTime));
    String timeInChart = Commons.getTimeStringFromLong(epochtime);

    int h_pix = d.height-margin*4;
    int y = e.getY();

    double variable = min_val-(y - h_pix)*h_val/(h_pix-MonitoredSimVariable.margin);
    setToolTipText("time = "+timeInChart+", variable = "+
                   " = "+variable);
	  }
	  catch(InternalError ie) {
		  setToolTipText("");
	  }
  }

  private class ToolTipMouseMotionListener implements MouseMotionListener
  {

    private int i = 0;

    public void mouseDragged(MouseEvent e)
    {
    }

    public void mouseMoved(MouseEvent e)
    {
      Overload.this.mouseMoved(e);
    }

  }

  protected void addToTree()
  {
    ola.addOneOverload(this);
  }

  protected void removeFromTree()
  {
    ola.removeOneOverload(this);
  }

  protected void createJPG()
  {
    try
    {
      JFileChooser fc = new JFileChooser(HierarchyObject.getReferenceFilePath());
      fc.setDialogTitle("Point to the file. (With extension .jpg)");
      fc.showSaveDialog(UniversalTellUser.getFrame(this));
      File selFile = fc.getSelectedFile();
      if(selFile == null) return;
      String path = selFile.getAbsolutePath();
      if(path == null) return;
      if(!(path.endsWith(".jpg")))
      {
        JOptionPane.showMessageDialog(
          UniversalTellUser.getFrame(this), 
          "The filename must end with .jpg", 
          "File not valid", 
          JOptionPane.ERROR_MESSAGE);
        return;
      }
      Dimension d = getSize();
      BufferedImage image = new BufferedImage(d.width, 
                                              d.height, 
                                              BufferedImage.TYPE_USHORT_GRAY);
      Graphics2D graphics = image.createGraphics();
      paint(graphics, d);
      FileOutputStream fos = new FileOutputStream(selFile);
      JpegEncoder jpg = new JpegEncoder(image, 50, fos);
      jpg.Compress();
      fos.close();
    }
    catch(FileNotFoundException fnf)
    {
      UniversalTellUser.general(UniversalTellUser.getFrame(this), new SystemError("File not found", fnf));
    }
    catch(IOException ioe)
    {
      UniversalTellUser.general(UniversalTellUser.getFrame(this), new SystemError("I/O error", ioe));
    }
    catch(Exception e)
    {
      UniversalTellUser.general(UniversalTellUser.getFrame(this), 
                                new UnclassedError("overload - jpg-generation", e));
    }
  }

  protected JPopupMenu getPopup()
  {
    JPopupMenu popup = new JPopupMenu();
    JMenuItem menuItem = new JMenuItem("Add to tree");
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          Overload.this.addToTree();
        }
      });
    popup.add(menuItem);
    menuItem = new JMenuItem("Remove from tree");
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          Overload.this.removeFromTree();
        }
      });
    popup.add(menuItem);
    menuItem = new JMenuItem("Create .jpg-file");
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          Overload.this.createJPG();
        }
      });
    popup.add(menuItem);
    return popup;
  }

  private class PopupListener extends MouseAdapter 
  {
    
    public PopupListener()
    {
    }

    public void mousePressed(MouseEvent e) 
    {
      maybeShowPopup(e);
    }

    public void mouseReleased(MouseEvent e) 
    {
      maybeShowPopup(e);
    }

    private void maybeShowPopup(MouseEvent e) 
    {
      if(!e.isPopupTrigger()) return;
      JPopupMenu popup = Overload.this.getPopup();
      if(popup != null) popup.show(e.getComponent(), e.getX(), e.getY());
    }

  }

}
