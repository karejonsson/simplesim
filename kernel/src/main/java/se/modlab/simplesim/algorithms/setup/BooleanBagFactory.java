package se.modlab.simplesim.algorithms.setup;

import java.util.Hashtable;

import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.scoping.*;
import se.modlab.simplesim.events.*;
import se.modlab.simplesim.bags.*;

public class BooleanBagFactory 
implements SimVariableFactory
{

	public static final String algos[] = 
		new String[] {"oldest", "youngest", "random"};

	private String type;
	private int algo;
	private SimScope s = null;
	private SimVariableFactory factory = null;

	public BooleanBagFactory(
			SimScope _s,
			String _type,
			int _algo)
	throws IntolerableException
	{
		s = _s;
		type = _type;
		algo = _algo;
		factory = (SimVariableFactory) s.getFactory(type);
		if(factory == null)
		{
			throw new UserCompiletimeError("Type "+type+" is not known!");  
		}    
	}

	public VariableInstance getInstance(String name, String filename, int line, int column, Hashtable<VariableInstance, Manager> managermap) 
	throws IntolerableException
	{
		return getInstance(name, false, filename, line, column, managermap);
	}

	public VariableInstance getInstance(String name, boolean monitor, String filename, int line, int column, Hashtable<VariableInstance, Manager> managermap) 
	throws IntolerableException
	{
		VariableInstance inst = null;
		switch(algo)
		{
		case 0: // oldest
		{
			inst = new BooleanLiloBag(name, BooleanBagFactory.this, filename, line, column);
			break;
		}
		case 1: // youngest
		{
			inst = new BooleanLifoBag(name, BooleanBagFactory.this, filename, line, column);
			break;
		}
		case 2: // random
		{
			inst = new BooleanRandomBag(name, BooleanBagFactory.this, filename, line, column);
			break;
		}
		};
		if(inst == null)
		{
			throw new InternalError("Null when it shouldn't!");
		}
		return inst;
	}

	public String getTypesName()
	{
		return getTypesName(type, algo);
	}

	public static String getTypesName(String t, int a)
	{
		return "bag<"+t+","+algos[a]+">";
	}

	public String toString()
	{
		String tn = getTypesName();
		return "Bag factory for type "+tn+" in scope "+s.getName();
	}

	public VariableInstance getInstance(String name, String filename, int line, int column)
	throws IntolerableException {
		System.out.println("Call to getInstance(s,s,i,i) i.e. addin null in class "+getClass().getName());
		return getInstance(name, filename, line, column, null);
	}
	public VariableInstance getInstance(String name, boolean monitor, String filename, int line, int column)
	throws IntolerableException {
		System.out.println("Call to getInstance(s,b,s,i,i) i.e. addin null in class "+getClass().getName());
		return getInstance(name, monitor, filename, line, column, null);
	}

}