package se.modlab.simplesim.algorithms.setup;

import java.io.InputStream;
import java.util.Hashtable;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.simplesim.algorithms.initiators.*;
import se.modlab.simplesim.algorithms.samplers.*;
import se.modlab.simplesim.events.*;
import se.modlab.simplesim.scoping.*;
import se.modlab.simplesim.variables.Manager;

public class ProcedureRepeatingFactoryManager implements Manager
{

  private String typesName;
  private Manager localScope[];
  private ArithmeticEvaluable ae_first;
  private ProgramBlock p;
  private LogicalExpression le_requeue;
  private ArithmeticEvaluable ae_again;
  private SimData sd;
  private ScopeFactory sf;
  
  public ProcedureRepeatingFactoryManager(
           String _typesName,
           Manager _localScope[],
           ArithmeticEvaluable _ae_first,
           ProgramBlock _p,
           LogicalExpression _le_requeue,
           ArithmeticEvaluable _ae_again, 
           SimData _sd,
           ScopeFactory _sf)
  {
    typesName = _typesName;
    localScope = _localScope;
    ae_first = _ae_first;
    p = _p;
    le_requeue = _le_requeue;
    ae_again = _ae_again;
    sd = _sd;
    sf = _sf;
  }

  public void initialize(Scope s, boolean isSharp, Hashtable<VariableInstance, Manager> managermap)
    throws IntolerableException
  {
    //Object o = s.getFactory(typesName);
    //System.out.println("WWW "+o);
    //System.out.println("procedureRepeatingFactoryManager "+typesName+" "+isSharp+" scope is "+s.getName());
    ProcedureRepeatingFactory prf = 
      new ProcedureRepeatingFactory(
        (SimScope)s, 
        typesName, 
        localScope, 
        ae_first, 
        p, 
        le_requeue,
        ae_again, 
        sd, 
        sf, 
        isSharp);
    VariableFactory vf = s.addFactoryUniquely(prf);
    if(vf != prf)
    {
      throw new UserCompiletimeError("Type "+typesName+" multiply defined.");
    }
    //System.out.println("DDD: "+(o == prf));
  } 
  
  public String toString()
  {
    return "procedure repeating factory manager for type "+typesName+" with scope ";
  }
  
	public void reinitializeLogical(LogicalExpression le) throws IntolerableException {
		throw new UserRuntimeError(
				"Cannot set a repeating type to the value of a boolean expression.\n"+
				"This happens to the type "+typesName+".");
    }

	public void reinitializeArithmetic(ArithmeticEvaluable ae) throws IntolerableException {
		throw new UserRuntimeError(
				"Cannot set a repeating type to the value of a arithmetic expression.\n"+
				"This happens to the type "+typesName+".");
    }

	public void reinitializeFromInputStream(InputStream is) throws IntolerableException {
		throw new InternalError(
				"Cannot set a repeating type to the value of an input stream.\n"+
				"This happens to the type "+typesName+".");
    }


}