package se.modlab.simplesim.algorithms.illustrate.share;

import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.events.*;
import se.modlab.generics.exceptions.*;

public class LookupScopeGetter implements ScopeGetter
{

  private VariableLookup vl;

  public LookupScopeGetter(VariableLookup _vl)
  {
    vl = _vl;
  }

  public Scope getScope(Scope globalScope)
    throws IntolerableException
  {
    VariableInstance vi = vl.getInstance(globalScope);
    if(vi == null) 
    {
      throw new UserCompiletimeError("There in no instance referred as \n"+
                          vl.toString()+" in the .ilu file.");
    }
    if(!(vi instanceof Enqueuable))
    {
      throw new UserCompiletimeError("The instance referred as \n"+
                          vl.toString()+" in the .ilu file is not associated with a\n"+
                          "local scope.");
    }
    Enqueuable eq = (Enqueuable) vi;
    return eq.getLocalScope();
  }

}
 