package se.modlab.simplesim.algorithms.illustrate.onimage;

import se.modlab.simplesim.algorithms.illustrate.share.*;
import se.modlab.generics.files.*;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.evaluables.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.strings.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.util.Services;

import java.awt.*;
import java.io.*;
import java.util.*;
import javax.swing.*;
import se.modlab.simplesim.algorithms.illustrate.struct.*;
import se.modlab.generics.bshro.ifc.HierarchyObject;

public class LabelIllustrationUnit implements IllustrationUnit
{
 
  private String staticsName = null;
  private ScopeGetter sg;
  private ScopeGivingProgram calc;
  private LogicalExpression visi;
  private StringEvaluable iconfile;
  private String iconFileCalculated;
  private StringEvaluable background;
  private ArithmeticEvaluable hori;
  private ArithmeticEvaluable vert;
  private StringEvaluable text;
  private Scope localScope = null;

  private ImageIcon icon = null;
  private static Hashtable colors = new Hashtable();

  private int width = -1;
  private int height = -1;

  static
  {
    colors.put("black", Color.black);
    colors.put("BLACK", Color.BLACK);
    colors.put("blue", Color.blue);
    colors.put("BLUE", Color.BLUE);
    colors.put("cyan", Color.cyan);
    colors.put("CYAN", Color.CYAN);
    colors.put("DARK_GRAY", Color.DARK_GRAY);
    colors.put("darkGray", Color.darkGray);
    colors.put("gray", Color.gray);
    colors.put("GRAY", Color.GRAY);
    colors.put("green", Color.green);
    colors.put("GREEN", Color.GREEN);
    colors.put("LIGHT_GRAY", Color.LIGHT_GRAY);
    colors.put("lightGray", Color.lightGray);
    colors.put("magenta", Color.magenta);
    colors.put("MAGENTA", Color.MAGENTA);
    colors.put("orange", Color.orange);
    colors.put("pink", Color.PINK);
    colors.put("red", Color.RED);
    colors.put("white", Color.white);
    colors.put("yellow", Color.yellow);
    colors.put("YELLOW", Color.YELLOW);   
  }

  private static Color getColor(String clr)
  {
    return (Color) colors.get(clr);
  }

  public LabelIllustrationUnit(
                    String _staticsName,
                    ScopeGetter _sg,
                    ScopeGivingProgram _calc,
                    LogicalExpression _visi,
                    StringEvaluable _iconfile,
                    StringEvaluable _background,
                    ArithmeticEvaluable _hori,
                    ArithmeticEvaluable _vert,
                    StringEvaluable _text)
  {
    staticsName = _staticsName;
    sg = _sg;
    calc = _calc;
    visi = _visi;
    iconfile = _iconfile;
    background = _background;
    hori = _hori;
    vert = _vert;
    text = _text;
  }

  public void init(int _width, int _height)
  {
    width = _width;
    height = _height;
    //System.out.println("W = "+width+", H = "+height);
  }

  public void setGlobalScope(Scope s)
    throws IntolerableException 
  {
    //System.out.println("labelIllustrationUnit - setGlobalScope 1");
    localScope = new StaticScope(sg.getScope(s));
    //System.out.println("labelIllustrationUnit - setGlobalScope 2 - cosinus = "+(localScope.getVariable("cosinus") == null));
    VariableType lt = localScope.getFactory("long");

    LongVariable w = new LongVariable("imagewidth", lt, "", -1, -1);
    w.setValue(new sLong(width));
    localScope.addVariable(new LongConst(w));

    LongVariable h = new LongVariable("imageheight", lt, "", -1, -1);
    h.setValue(new sLong(height));
    localScope.addVariable(new LongConst(h));
    //System.out.println("labelIllustrationUnit - setGlobalScope 3 - cosinus = "+(localScope.getVariable("cosinus") == null));
    //System.out.println("labelIllustrationUnit - setGlobalScope 3 - imagewidth = "+(localScope.getVariable("imagewidth") == null));
    iconFileCalculated = 
    		HierarchyObject.getReferenceFilePath()+
      iconfile.evaluate(localScope);
    byte image[] = Services.getCleartextFromFile(iconFileCalculated);
    if(image == null) 
    { 
      throw new UserCompiletimeError(
        "The file "+iconFileCalculated+" was expected to contain\n"+
        "an icon but did not exist!"); 
    }
    icon = new ImageIcon(image);
    w = new LongVariable("iconwidth", lt, "", -1, -1);
    w.setValue(new sLong(icon.getIconWidth()));
    localScope.addVariable(new LongConst(w));
    h = new LongVariable("iconheight", lt, "", -1, -1);
    h.setValue(new sLong(icon.getIconHeight()));
    localScope.addVariable(new LongConst(h));
  }

  public String getIconFilenameCalculated()
  {
    return iconFileCalculated;
  }

  public String getName()
  {
    return staticsName;
  }

  public void paint(Graphics g)
    throws IntolerableException 
  {
    if(localScope == null) return;
    Scope tmp = null;
    try
    {
      tmp = calc.executeGiveScope(localScope);
    }
    catch(ReturnException re)
    {
    }
    catch(StopException se)
    {
      throw new UserRuntimeError("Stop in illustration", se);
    }
    catch(BreakException be)
    {
      throw new InternalProgrammingError("Unexpected break exception in illustration", be);
    }
    catch(ContinueException ce)
    {
      throw new InternalProgrammingError("Unexpected continue exception in illustration", ce);
    }
    boolean visible = visi.evaluate(tmp);
    if(!visible) return;
    String backcolor = background.evaluate(tmp);
    sValue _xpos = hori.evaluate(tmp);
    int xpos = (int) _xpos.getValue();
    sValue _ypos = vert.evaluate(tmp);
    int ypos = (int) _ypos.getValue();
    String textual = text.evaluate(tmp);
    Color col = getColor(backcolor);
    g.drawImage(icon.getImage(), xpos, ypos, col, null);

    FontMetrics fm = g.getFontMetrics();
    int theight = fm.getHeight();
    byte bytes[] = textual.getBytes();
    int twidth = fm.bytesWidth(bytes, 0, bytes.length);
    int down = (int)(fm.getHeight() + icon.getIconHeight())/2;
    int typos = ypos+down;
    int txpos = xpos+icon.getIconWidth();
    if((txpos + twidth > width) &&
       (xpos - twidth > 0))
    {
      g.drawString(textual, xpos - twidth, typos);
      //System.out.println("labelIllustrationUnit "+textual);
      return;
    }
    g.drawString(textual, txpos, typos);
    //System.out.println("labelIllustrationUnit "+textual);
  }

}