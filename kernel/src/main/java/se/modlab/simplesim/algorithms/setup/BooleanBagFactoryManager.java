package se.modlab.simplesim.algorithms.setup;

import java.io.InputStream;
import java.util.Hashtable;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.simplesim.algorithms.initiators.*;
import se.modlab.simplesim.algorithms.samplers.*;
import se.modlab.simplesim.events.*;
import se.modlab.simplesim.scoping.*;
import se.modlab.simplesim.variables.Manager;

public class BooleanBagFactoryManager implements Manager
{

  private String type;
  private int algo;
  
  public BooleanBagFactoryManager(
           String _type,
           int _algo)
  {
    type = _type;
    algo = _algo;
  }

  public void initialize(Scope s, boolean isSharp, Hashtable<VariableInstance, Manager> managermap)
    throws IntolerableException
  {
    BooleanBagFactory out = 
      new BooleanBagFactory((SimScope)s, type, algo);
    s.addFactoryUniquely(out);
  } 

  private String getTypesName()
  {
    return BooleanBagFactory.getTypesName(type, algo);
    //"bag<"+type+","+booleanBagFactory.algos[algo]+">";
  }
 
  public String toString()
  {
    String tn = getTypesName();
    return "Bag factory manager for type "+tn;
  }
  
	public void reinitializeLogical(LogicalExpression le) throws IntolerableException {
		throw new InternalError(
				"Cannot set a boolean bag type to the value of a boolean expression.\n"+
				"This happens to the bag type "+type+".");
    }

	public void reinitializeArithmetic(ArithmeticEvaluable ae) throws IntolerableException {
		throw new UserRuntimeError(
				"Cannot set a boolean bag type to the value of an arithmetic expression.\n"+
				"This happens to the bag type "+type+".");
    }

	public void reinitializeFromInputStream(InputStream is) throws IntolerableException {
		throw new InternalError(
				"Cannot set boolean bag factory to the value of input stream.\n"+
				"This happens to the bag type "+type);
    }

}