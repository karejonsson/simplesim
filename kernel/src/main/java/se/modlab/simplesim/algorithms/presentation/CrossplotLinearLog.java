package se.modlab.simplesim.algorithms.presentation;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class CrossplotLinearLog extends Crossplot 
{

  public CrossplotLinearLog(MonitoredVariableExtracted mveh, 
                            MonitoredVariableExtracted mvev, 
                            CrossplotAdder cpa,
                            SegmentPainter sp)
  {
    super(mveh, mvev, cpa, sp);
  }

  protected boolean makeChecks(Graphics g, Dimension d)
  {
    if(mvev.getMinValue() <= 0)
    {
      g.drawString("The variable "+mvev.getName()+" takes negative values which",
                   margin, 2*margin);
      g.drawString("cannot be plotted with logarithmic conversion.",
                   margin, 3*margin);
      return false;
    }
    return true;
  }

  public double getVerticalTransition(double v)
  {
    return Math.log(v);
  }

  public double getHorizontalTransition(double h)
  {
    return h;
  }

  public double getVerticalInversion(double v)
  {
    return Math.exp(v);
  }

  public double getHorizontalInversion(double h)
  {
    return h;
  }

  public String getHorizontalTransitionName()
  {
    return "linear";
  }

  public String getVerticalTransitionName()
  {
    return "logarithmic";
  }

}
