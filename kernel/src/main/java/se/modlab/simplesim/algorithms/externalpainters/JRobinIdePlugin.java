package se.modlab.simplesim.algorithms.externalpainters;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import org.jrobin.inspector.*;
 
import se.modlab.generics.gui.files.OneFileNotepad;
import se.modlab.simpleide.DefaultIdeMenuHandler;

public class JRobinIdePlugin extends DefaultIdeMenuHandler
{

  private static final String packname = "JRobin";
  private static final String version = "1.5.4";

  public JRobinIdePlugin()
  {
  }

  public JMenu[] getFilesMenu(File _f)
  {
    return null;
  }

  public JMenu[] getEditorsMenu(OneFileNotepad ofn)
  { 
    return null;
  }

  public JMenu[] getFoldersMenu(File f)
  {
    return null;
  }

  public JMenu[] getMenubar()
  {
    final JMenu menu = new JMenu(packname);
    JMenuItem menuItem = new JMenuItem("About "+packname);
    menu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          info(
            getFrame(menu), 
            packname+" is created by:\n"+ 
            "\n"+ 
            "Sasa Markovic, Arne Vandamme. \n"+ 
            "\n"+ 
            "These names appear in the JavaDoc comments.\n"+ 
            "\n"+ 
            "The Simplesim project thanks for a very nice\n"+ 
            "piece of code to work with.\n"+ 
            "\n"+ 
            "More Info: http://www.jrobin.org and\n"+
            "http://oldwww.jrobin.org", 
            "Information for "+packname);
        }
      }
    );
    menuItem = new JMenuItem("Version");
    menu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          info(
            getFrame(menu), 
            "This is version "+version+" of "+packname+"\n",
            "Version");
        }
      }
    );
    menuItem = new JMenuItem("Open");
    menu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
            RrdInspector.main(new String[] {});
        } 
      } 
    );
    return new JMenu[] { menu };
  }

}
