package se.modlab.simplesim.algorithms.setup;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.simplesim.scoping.*;
import se.modlab.simplesim.references.*;

public class TypedefTypedStructReferenceVectorMember 
             extends TypedefStructMember
{

  private ArithmeticEvaluable ae;
  
  public TypedefTypedStructReferenceVectorMember(String _type,
                                                 String _name,
                                                 ArithmeticEvaluable _ae,
                                                 String _filename,
                                                 int _line, int _column)
  {   
    super(_type, _name, _filename, _line, _column);
    ae = _ae;
  }

  public VariableFactory getFactory(Scope s)
    throws IntolerableException
  {
    int length = -1;
    try
    {
      length = ae.evaluate(s).getLong().intValue();
    }
    catch(NullPointerException npe)
    {
      throw new UserCompiletimeError(
        "The length of the struct with typename "+type+"\n"+
        "does not evaluate to long value."+"\n"+
        "Referenced in file "+filename+" on line "+line+", column "+column);
    }
    VariableFactory vf = s.getFactory(type);
    if(vf == null)
    {
      throw new UserCompiletimeError("There is no type named "+type+".\n"+
        "Referenced in file "+filename+" on line "+line+", column "+column);
    }
    TypedStructReferenceFactory tsrf =
      new TypedStructReferenceFactory((SimScope) s, type, true);
    SimVectorVariableFactory svvf = new 
      SimVectorVariableFactory(tsrf, length);
    vf = s.addFactoryUniquely(svvf);
    return vf;
  }

  public String toString()
  {
    return "Typedef struct reference vector member: Type "+
           type+", name "+name+".\n"+
        "Declared in file "+filename+" on line "+line+", column "+column;
  }

}