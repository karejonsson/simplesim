package se.modlab.simplesim.algorithms.share;

import java.util.Hashtable;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.files.*;
import se.modlab.generics.sstruct.comparisons.Scope;
import se.modlab.generics.sstruct.variables.VariableInstance;
import se.modlab.simplesim.algorithms.presentation.TopNode;
import se.modlab.simplesim.algorithms.setup.SimData;
import se.modlab.simplesim.variables.Manager;

public interface Algorithm 
{
	public void setupScenario(final Hashtable<VariableInstance, Manager> managermap) throws IntolerableException;

	public Scope getGlobalScope();

	public SimData getSimData();
	
	public TopNode getTopnode();
	
	public void runAlgorithm() throws IntolerableException;

	public FileCollector[] getCollectors();  

	public int getCurrent();

	public int getLengthOfTask();

	public boolean isDone();

	public String getSurveillanceMessageForBar();

	public String getSurveillanceMessageForDump();

	public String getName();

	public void stop();

	public void displayResultsGraphically(boolean visible);

	public Manager[] getManagerArray();

}
