package se.modlab.simplesim.algorithms.presentation;

import se.modlab.generics.gui.util.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;
import se.modlab.generics.sstruct.comparisons.*;
import java.util.Enumeration;
import java.util.Vector; 
import javax.swing.JTextArea;
import javax.swing.tree.TreeNode;
import java.awt.Component;
import se.modlab.simplesim.algorithms.setup.*;

public class TopNode implements SimpleExplorerNode
{
  private Vector c;
  private Commons com;
  private SimData sd;
  private Variables vars;
  private Crossplots cps;
  private Overloads ols;
  private Sources ss;
  //private AllVariables av;
  private SegmentPainter sp;

  public TopNode(SimData _sd,
                 Variables _vars, 
                 SegmentPainter _sp)
  {
    sd = _sd;
    com = sd.getCommons();
    sp = _sp;
    vars = _vars;
    vars.setParent(this);
    cps = new Crossplots(this);
    ols = new Overloads(this);
    ss = new Sources(this, sd.getCollectors());
//    av = new AllVariables(this, sd);
  }

  public Variables getVariablesNode()
  {
    return vars;
  }

  public Crossplots getCrossplotsNode()
  {
    return cps;
  }

  public Overloads getOverloadsNode()
  {
    return ols;
  }

  private void fillVector()
  {
    if(c != null) return;
    c = new Vector();
    c.addElement(vars);
    c.addElement(cps);
    c.addElement(ols);
    c.addElement(ss);
    //c.addElement(av);
  }

  public Enumeration children()
  {
    if(c == null) fillVector();
    return c.elements();
  }

  public boolean getAllowsChildren()
  {
    return true;
  }

  public TreeNode getChildAt(int childIndex)
  {
    if(c == null) fillVector();
    if(childIndex >= c.size()) return null;
    if(childIndex < 0) return null;
    return (TreeNode) c.elementAt(childIndex); 
  }

  public int getChildCount()
  {
    if(c == null) fillVector();
    return c.size();
  }

  public int getIndex(TreeNode tn)
  {
    if(c == null) return -1;
    if(tn == null) return -1;
    return c.indexOf(tn);
  }

  public TreeNode getParent()
  {
    return null;
  }

  public boolean isLeaf()
  {
    return false;
  }

  public Component getComponent()
  {
    JTextArea ta = new JTextArea();
    ta.append("Simulation: "+com.getName()+"\n\n");
    ta.append("Reason for termination: "+com.getTextualStopReason()+"\n\n");
    ta.append("Epoch time unit:"+com.getTimeUnit()+"\n");
    ta.append("Epoch start time: "+com.getEpochStartTime()+"\n");
    ta.append("Calendar start time: "+Commons.getTimeStringFromLong(com.getEpochStartTime())+"\n");
    ta.append("Simulation time at termination: "+com.getCurrentSimTime()+"\n");
    ta.append("Calendar time at termination: "+Commons.getTimeStringFromLong(com.getCurrentEpochSimTime())+"\n");
    ta.append("Epoch time at termination : "+com.getCurrentEpochSimTime()+"\n");
    //ta.append(": "++"\n");
    //ta.append(": "++"\n");
    //ta.append(": "++"\n");
    
    return ta;
  }  

  public String toString()
  {
    return com.getName();
  }

} 
