package se.modlab.simplesim.algorithms.presentation;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.gui.find.StringGridBagLayout;
import se.modlab.generics.sstruct.values.sValue;

import se.modlab.simplesim.tables.SpreadsheetWindow_excel;
import se.modlab.simplesim.variables.*;
import se.modlab.tables.editor.koders.Cell;
import se.modlab.generics.gui.util.*;

public class ExportTwoVariables extends JDialog 
{

  //private static String elems[] = {"Ett", "Tv�", "Tre", "Fyra"};
  private JComboBox primary;// = new JComboBox();
  private JComboBox secondary;// = new JComboBox();
  private JButton cancelButton = new JButton("Cancel");
  private JButton goButton = new JButton("Export");

  private MonitoredVariableExtracted extracts[];

  public ExportTwoVariables(SimpleExplorer _se, 
                			boolean flag, 
                			MonitoredVariableExtracted _extracts[])
  {
    super(_se, flag);
    extracts = _extracts;
    //cpa = _cpa;
    //sp = _sp;
    //se = _se;
    primary = new JComboBox(extracts);
    secondary = new JComboBox(extracts);
    initComponents();
    pack();
  }

  private void initComponents()
  {
    Container container = getContentPane();
    container.setLayout(new StringGridBagLayout());
    setTitle("Export 2 variables");
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent windowevent) {
        setVisible(false);
        dispose();
      }
    });

    JLabel jlabel = new JLabel();
    jlabel.setDisplayedMnemonic('U');
    jlabel.setLabelFor(primary);
    jlabel.setText("Primary exported variable");
    container.add("anchor=WEST,insets=[12,12,0,0]", jlabel);
    primary.setToolTipText("Choose variable");
    container.add("fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", primary);
    
    JLabel jlabel1 = new JLabel();
    jlabel1.setDisplayedMnemonic('P');
    jlabel1.setText("Secondary exported variable");
    jlabel1.setLabelFor(secondary);
    container.add("gridx=0,gridy=1,anchor=WEST,insets=[11,12,0,0]", jlabel1);
    secondary.setToolTipText("Choose variable");
    container.add("gridx=1,gridy=1,fill=HORIZONTAL,weightx=1.0,insets=[11,7,0,11]", secondary);

    JPanel jpanel = createButtonPanel();
    container.add("gridx=0,gridy=2,gridwidth=2,anchor=EAST,insets=[17,12,11,11]", jpanel);
    getRootPane().setDefaultButton(goButton);
  }

  private JPanel createButtonPanel()
  {
    JPanel jpanel = new JPanel();
    jpanel.setLayout(new BoxLayout(jpanel, 0));
    goButton.setText("Export");
    goButton.setToolTipText("Press to make exported table");
    goButton.addActionListener(new ActionListener() {
    	public void actionPerformed(ActionEvent actionevent) {
    		makeTheExporttable();
    	}
    });
    jpanel.add(goButton);
    jpanel.add(Box.createRigidArea(new Dimension(5, 0)));
    cancelButton.setText("Cancel");
    cancelButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent actionevent)
      {
        cancel();
      }
    });
    jpanel.add(cancelButton);
    jpanel.add(Box.createRigidArea(new Dimension(5, 0)));
    Vector vector = new Vector(2);
    vector.add(cancelButton);
    vector.add(goButton);
    equalizeComponentSizes(vector);
    vector.removeAllElements();
    return jpanel;
  }

  private void equalizeComponentSizes(java.util.List list)
  {
    //boolean flag = false;
    Dimension dimension = new Dimension(0, 0);
    //Object obj = null;
    //Object obj1 = null;
    for(int i = 0; i < list.size(); i++)
    {
      JComponent jcomponent = (JComponent)list.get(i);
      Dimension dimension1 = jcomponent.getPreferredSize();
      dimension.width = Math.max(dimension.width, (int)dimension1.getWidth());
      dimension.height = Math.max(dimension.height, (int)dimension1.getHeight());
    }

    for(int j = 0; j < list.size(); j++)
    {
      JComponent jcomponent1 = (JComponent)list.get(j);
      jcomponent1.setPreferredSize((Dimension)dimension.clone());
      jcomponent1.setMaximumSize((Dimension)dimension.clone());
    }
  }
  
  private static sValue getValue(final double time, final MonitoredVariableExtracted var) {
	  try {
		  // Omitting the possibility size could be zero. 
		  sValue v = var.getValue(0);
		  int size = var.getSize();
		  if(size < 2) return v;  // If it is before the first one.
		  v = var.getValue(size -1);
		  if(v.getTime() <= time) return v; // If it is after the last one.
		  int before = 0;
		  int after = size -1;
		  while(before < after) {
			  //System.out.println("Before "+before+", after "+after);
			  if(before + 1 == after) {
				  return var.getValue(before); // What the variable held at that time.
			  }
			  int middle = (int) ((after + before) / 2);
			  sValue v2 = var.getValue(middle);
			  if(v2.getTime() == time) return v2;
			  if(v2.getTime() < time) {
				  before = middle;
			  }
			  else {
				  after = middle;
			  }
		  }
	  }
	  catch(IntolerableException ie) {
	  }
	  return null;
  }

  private void makeTheExporttable()
  {
    MonitoredVariableExtracted primary = (MonitoredVariableExtracted) getVariableOne();
    MonitoredVariableExtracted secondary = (MonitoredVariableExtracted) getVariableTwo();
    
    int rows = primary.getSize();
    SpreadsheetWindow_excel excel = new SpreadsheetWindow_excel();
    excel.newTableModel(rows, 3);
    //System.out.println("rows "+rows+", columns "+columns);
    for(int i = 0 ; i < rows ; i++)
    {
    	sValue val = null;
    	try {
    		val = primary.getValue(i);
    	}
    	catch(IntolerableException ie) {
    		continue;
    	}

    	Cell date = excel.getCellAt(i, 0);
    	Cell value_p = excel.getCellAt(i, 1);
    	Cell value_s = excel.getCellAt(i, 2);
    	
    	value_p.setValue(val.getDouble());
    	value_s.setValue(getValue(val.getTime(), secondary).getDouble());
    	date.setValue(val.getTime());
    }
    JFrame frame = SpreadsheetWindow_excel.getFrame("", excel);
    frame.pack();
    frame.setVisible(true);
 
    //System.out.println("Export. Primary "+primary.getName()+", secondary "+secondary.getName());
  }
  
  private void cancel()
  {
    setVisible(false);
    dispose();
  }
  
  public Object getVariableOne()
  {
    return primary.getSelectedItem();
  }

  public Object getVariableTwo()
  {
    return secondary.getSelectedItem();
  }

}

