package se.modlab.simplesim.algorithms.presentation;

import se.modlab.generics.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import java.util.Enumeration;
import java.util.Vector;
import javax.swing.tree.TreeNode;
import javax.swing.JTextArea;
import java.awt.Component;
import se.modlab.simplesim.variables.*;

public class OneBooleanVariable extends VariableNode
{
  private String fullName = null;
  private Object bv;
  private SegmentPainter sp;

  public OneBooleanVariable(String _fullName,
                            Object _bv, 
                            TreeNode _parent, 
                            SegmentPainter _sp)
  {
	super(_parent);
    fullName = _fullName;
    bv = _bv; 
    sp = _sp;
    parent = _parent;
  }

  public Component getComponent()
  {
    return new JTextArea(bv.toString());
  }  

  public String toString()
  {
    return fullName;
  }

} 
