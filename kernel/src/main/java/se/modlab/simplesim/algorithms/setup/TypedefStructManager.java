package se.modlab.simplesim.algorithms.setup;

import java.io.InputStream;
import java.util.Hashtable;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.logics.LogicalExpression;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;

public class TypedefStructManager implements Manager
{
  
  private String type = null;
  private String name = null;
  private TypedefStructMember members[];
  private SimData sd = null;
  /*private boolean _con;
  private boolean _mon;
  private boolean _exp;
  private boolean _pub;*/

  public TypedefStructManager(String _type,
                              String _name, 
                              TypedefStructMember _members[],
                              SimData _sd)
  {
    type = _type;
    name = _name;
    members = _members;
    sd = _sd;
  }

  public void initialize(Scope s, boolean isSharp, Hashtable<VariableInstance, Manager> managermap)
    throws IntolerableException
  {
    /*if(_con)
    {
      throw new userError("You may not define a struct as const. It\n"+
                          "makes no sense with constant zeros.");
    }*/
    SimComplexVariableFactory out = new SimComplexVariableFactory(type, sd.getCommons());
    for(int i = 0 ; i < members.length ; i++)
    {
      VariableFactory vf = members[i].getFactory(s);
      //System.out.println("TSM: "+name+"  i="+i+", "+members[i]+"\n"+
      //                   "  Type = "+vf.getClass().getName());
      out.addMember(vf, members[i].getName());
    }
    s.addFactoryUniquely(out);
  } 
  
	public void reinitializeLogical(LogicalExpression le) throws IntolerableException {
		throw new UserRuntimeError(
				"Cannot set a typedefinition of a struct to the value of a boolean expression.\n"+
				"This happens to the typedefinition "+name+".");
    }

	public void reinitializeArithmetic(ArithmeticEvaluable ae) throws IntolerableException {
		throw new UserRuntimeError(
				"Cannot set a typedefinition of a struct to the value of an arithmetic expression.\n"+
				"This happens to the typedefinition "+name+".");
    }

	public void reinitializeFromInputStream(InputStream is) throws IntolerableException {
		throw new InternalError(
				"Cannot set a typedefinition of a struct to the value of an input stream.\n"+
				"This happens to the typedefinition "+name+".");
    }

}