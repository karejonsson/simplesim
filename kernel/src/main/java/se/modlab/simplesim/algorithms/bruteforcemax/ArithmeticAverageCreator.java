package se.modlab.simplesim.algorithms.bruteforcemax;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.simplesim.algorithms.share.*;

public class ArithmeticAverageCreator
{

  private Variable var;
  private double sum;
  private int samples;

  public ArithmeticAverageCreator(Variable var)
    throws IntolerableException
  {
    if((!(var instanceof DoubleVariable)) &&
       (!(var instanceof LongVariable)))
    {
      throw new InternalError(
        "Not double or long in class arithemticAverageCreator.ctor");
    }
    this.var = var;
  }

  public void getSampleForAverage()
    throws IntolerableException
  {
    sum += var.getValue().getValue();
    samples++;
  }

  public void setAverageToVariable()
    throws IntolerableException
  {
    if(samples == 0)
    {
      throw new InternalError(
        "Number of samples is zero in class arithemticAverageCreator.setAverageToVariable");
    }
    if(var instanceof DoubleVariable)
    {
      var.setValue(new sDouble(sum / samples));
    }
    if(var instanceof LongVariable)
    {
      var.setValue(new sLong(Math.round(sum / samples)));
    }
  }

}
