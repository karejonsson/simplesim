package se.modlab.simplesim.algorithms.illustrate.share;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;

public class GlobalScopeGetter implements ScopeGetter
{

  public Scope getScope(Scope globalScope)
    throws IntolerableException
  {
    //System.out.println("globalScopeGetter "+globalScope.getName());
    return globalScope;
  }

}
