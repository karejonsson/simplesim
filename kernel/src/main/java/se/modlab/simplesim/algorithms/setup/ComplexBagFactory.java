package se.modlab.simplesim.algorithms.setup;

import java.util.Hashtable;

import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.scoping.*;
import se.modlab.simplesim.events.*;
import se.modlab.simplesim.bags.*;

public class ComplexBagFactory 
             implements SimVariableFactory
{

  public static final String algos[] = 
    new String[] {"oldest", "youngest", "largest", "smallest", "random"};

  private String type;
  private int algo;
  private SimScope s = null;
  private SimVariableFactory factory = null;
  private VariableLookup vl = null;

  public ComplexBagFactory(
                    SimScope _s,
                    String _type,
                    int _algo,
                    VariableLookup _vl)
    throws IntolerableException
  {
    s = _s;
    type = _type;
    algo = _algo;
    vl = _vl;
    factory = (SimVariableFactory) s.getFactory(type);
    if(factory == null)
    {
      throw new UserCompiletimeError("Type "+type+" is not known!");  
    }    
  }
 
  public VariableInstance getInstance(String name, String filename, int line, int column, Hashtable<VariableInstance, Manager> managermap
) 
    throws IntolerableException
  {
    return getInstance(name, false, filename, line, column, managermap);
  }

  public VariableInstance getInstance(String name, boolean monitor, String filename, int line, int column, Hashtable<VariableInstance, Manager> managermap) 
    throws IntolerableException
  {
    VariableInstance inst = null;
    switch(algo)
    {
      case 0: // oldest
      {
        inst = new ComplexLiloBag(name, ComplexBagFactory.this, factory, filename, line, column);
        break;
      }
      case 1: // youngest
      {
        inst = new ComplexLifoBag(name, ComplexBagFactory.this, factory, filename, line, column);
        break;
      }
      case 2: // largest
      {
        inst = new ComplexSortBag(name, ComplexBagFactory.this, factory, 
                                        new ComplexSortBag.complexSortLarger(vl), filename, line, column);
        break;
      }
      case 3: // smallest
      {
        inst = new ComplexSortBag(name, ComplexBagFactory.this, factory,
                                        new ComplexSortBag.complexSortSmaller(vl), filename, line, column);
        break;
      }
      case 4: // random
      {
        inst = new ComplexRandomBag(name, ComplexBagFactory.this, factory, filename, line, column);
        break;
      }
    };
    if(inst == null)
    {
      throw new InternalError("Null when it shouldn't!");
    }
    return inst;
  }

  public String getTypesName()
  {
    return getTypesName(type, algo, vl);
  }

  public static String getTypesName(String t, int a, VariableLookup vl)
  {
    return "bag<"+t+","+algos[a]+","+vl+">";
  }
 
  public String toString()
  {
    String tn = getTypesName();
    return "Bag factory for type "+tn+" in scope "+s.getName();
  }

	public VariableInstance getInstance(String name, String filename, int line, int column)
	throws IntolerableException {
		System.out.println("Call to getInstance(s,s,i,i) i.e. addin null in class "+getClass().getName());
		return getInstance(name, filename, line, column, null);
	}
	public VariableInstance getInstance(String name, boolean monitor, String filename, int line, int column)
	throws IntolerableException {
		System.out.println("Call to getInstance(s,b,s,i,i) i.e. addin null in class "+getClass().getName());
		return getInstance(name, monitor, filename, line, column, null);
	}

}