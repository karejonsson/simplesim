package se.modlab.simplesim.algorithms.presentation;

import se.modlab.generics.*;
import se.modlab.generics.bshro.ifc.HierarchyObject;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.jpegcoding.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.util.*;
import java.io.*;

public abstract class Crossplot extends JComponent 
{
  protected static final int margin = 20;

  private SegmentPainter sp;
  private String haxis;
  protected MonitoredVariableExtracted mveh;
  protected double min_val_h;
  protected double max_val_h;
  protected double min_val_h_trans;
  protected double max_val_h_trans;

  private String vaxis;
  protected MonitoredVariableExtracted mvev;
  protected double min_val_v;
  protected double max_val_v;
  protected double min_val_v_trans;
  protected double max_val_v_trans;

  private CrossplotAdder cpa;
  protected int h_pix;

  public Crossplot(MonitoredVariableExtracted mveh, 
                   MonitoredVariableExtracted mvev, 
                   CrossplotAdder cpa,
                   SegmentPainter sp)
  {
    this.mveh = mveh;
    this.mvev = mvev;
    this.haxis = mveh.getName();
    this.vaxis = mvev.getName();
    this.cpa = cpa;
    this.sp = sp;
    min_val_h = mveh.getMinValue();
    max_val_h = mveh.getMaxValue();
    min_val_v = mvev.getMinValue();
    max_val_v = mvev.getMaxValue();

    min_val_h_trans = getHorizontalTransition(min_val_h);
    max_val_h_trans = getHorizontalTransition(max_val_h);
    min_val_v_trans = getVerticalTransition(min_val_v);
    max_val_v_trans = getVerticalTransition(max_val_v);
    addMouseMotionListener(new ToolTipMouseMotionListener());
    addMouseListener(new PopupListener());
  }

  protected void drawFrame(Graphics g, Dimension d)
    throws IntolerableException
  {
    // Frame upper
    g.drawLine((int) margin/2, (int) margin/2,
               (int) (d.width - margin/2), (int) margin/2); 
    // Frame lower
    g.drawLine((int) margin/2, h_pix+(int) margin/2,
               (int) (d.width - margin/2), h_pix+(int) margin/2); 
    // Frame left
    g.drawLine((int) margin/2, (int) margin/2,
               (int) margin/2, h_pix+(int) margin/2); 
    // Frame right
    g.drawLine((int) (d.width - margin/2), (int) margin/2,
               (int) (d.width - margin/2), h_pix+(int) margin/2); 
  }

  public abstract double getVerticalTransition(double v);
  public abstract double getHorizontalTransition(double h);
  public abstract double getVerticalInversion(double v);
  public abstract double getHorizontalInversion(double h);
  public abstract String getHorizontalTransitionName();
  public abstract String getVerticalTransitionName();

  public String toString()
  {
    return haxis+" horizontal "+getHorizontalTransitionName()+", "+
           vaxis+" vertical "+getVerticalTransitionName();
  }

  protected void drawText(Graphics g, Dimension d)
    throws IntolerableException
  {
    g.drawString("Variable "+haxis+" on horizontal axis is "+getHorizontalTransitionName(), 
                 margin, h_pix+((int)(1.5*margin)));
    g.drawString("Min value: "+mveh.getMinValue(), 
                 margin, h_pix+((int)(2.5*margin)));
    g.drawString("Max value: "+mveh.getMaxValue(), 
                 margin, h_pix+((int)(3.5*margin)));

    g.drawString("Variable "+vaxis+" on vertical axis is "+getVerticalTransitionName(), 
                 ((int) d.width/2), h_pix+((int)(1.5*margin)));
    g.drawString("Min value: "+mvev.getMinValue(), 
                 ((int) d.width/2), h_pix+((int)(2.5*margin)));
    g.drawString("Max value: "+mvev.getMaxValue(), 
                 ((int) d.width/2), h_pix+((int)(3.5*margin)));
  }

  protected boolean makeChecks(Graphics g, Dimension d)
  {
    return true;
  }

  public void paint(Graphics g)
  {
    paint(g, getSize());
  }

  private class chunk
  {
    private Vector v = new Vector();
    private double time;

    public chunk(double time)
    {
      this.time = time;
    }

    public int getSize()
    {
      return v.size();
    }

    public void addElement(sValue sv)
      throws IntolerableException
    {
      if(sv.getTime() != time) throw 
        new InternalError("Not same time. In crossplot.addElement(sValue sv) in "+toString());
      v.addElement(sv);
    }

    public double getTime()
    {
      return time;
    }

    public sValue getValue(int i) 
      throws IntolerableException
    {
      if(i < 0) throw new InternalError("Internal error: Index ="+i+" in "+toString());
      if(i >= v.size()) throw 
        new InternalError("Index ="+i+" >= v.size()="+v.size()+" in "+toString());
      return (sValue) v.elementAt(i);
    }

    public double getDoubleValue(int i) 
      throws IntolerableException
    {
      sValue sv = getValue(i);
      if(sv instanceof sBoolean) 
        return (sv.getBoolean().booleanValue()) ? 1.0 : 0.0;
      return sv.getValue();
    }

  } // class chunk

  private int hpc_index = 0;
  private double horizontalPeekTime()
    throws IntolerableException
  {
    if(hpc_index >= mveh.getSize()) return -1.0;
    return ((sValue)mveh.getValue(hpc_index)).getTime();
  }  
  private chunk getHorizontalPointChunk()
    throws IntolerableException
  {
    if(hpc_index >= mveh.getSize()) return null;
    Vector v = new Vector();
    double time = ((sValue)mveh.getValue(hpc_index)).getTime();
    chunk ch = new chunk(time);
    while((hpc_index < mveh.getSize()) &&
          (((sValue)mveh.getValue(hpc_index)).getTime() == time))
    {
      ch.addElement((sValue)mveh.getValue(hpc_index));
      hpc_index++;
    }
    return ch;
  }

  private int vpc_index = 0;
  private double verticalPeekTime()
    throws IntolerableException
  {
    if(vpc_index >= mvev.getSize()) return -1.0;
    return ((sValue)mvev.getValue(vpc_index)).getTime();
  }  
  private chunk getVerticalPointChunk()
    throws IntolerableException
  {
    if(vpc_index >= mvev.getSize()) return null;
    Vector v = new Vector();
    double time = ((sValue)mvev.getValue(vpc_index)).getTime();
    chunk ch = new chunk(time);
    while((vpc_index < mvev.getSize()) &&
          (((sValue)mvev.getValue(vpc_index)).getTime() == time))
    {
      ch.addElement((sValue)mvev.getValue(vpc_index));
      vpc_index++;
    }
    return ch;
  }

  protected void plotOneDot(double vertical, double horizontal, 
                            Graphics g, Dimension d)
    throws IntolerableException
  {
    g.fillRect((int) (margin+((d.width-2*margin)*(getHorizontalTransition(horizontal)-min_val_h_trans))/
                              (max_val_h_trans-min_val_h_trans))-1,
               (int) (h_pix-((h_pix-margin)*(getVerticalTransition(vertical)-min_val_v_trans))/
                             (max_val_v_trans-min_val_v_trans))-1,
               3,
               3);
  }

  protected void plotChunks(chunk vertical, chunk horizontal, 
                            Graphics g, Dimension d)
    throws IntolerableException
  {
    for(int v = 0 ; v < vertical.getSize() ; v++)
    {
      for(int h = 0 ; h < horizontal.getSize() ; h++)
      {
        plotOneDot(vertical.getDoubleValue(v),
                   horizontal.getDoubleValue(h),
                   g, d);
      }
    }
    
  }

  public void paint(Graphics g, Dimension d)
  {
    vpc_index = 0;
    hpc_index = 0;
    try
    {
      h_pix = d.height-margin*4;
      drawFrame(g, d);
      drawText(g, d);
      if(!makeChecks(g, d)) return;
      chunk h = getHorizontalPointChunk();
      chunk v = getVerticalPointChunk();
      plotChunks(v, h, g, d);
      while((horizontalPeekTime() != -1.0) || (verticalPeekTime() != -1.0))
      {
        if(horizontalPeekTime() == -1.0)
        {
          v = getVerticalPointChunk();
        }
        else if(verticalPeekTime() == -1.0)
        {
          h = getHorizontalPointChunk();
        }
        else if((horizontalPeekTime() != -1.0) && (verticalPeekTime() != -1.0))
        {
          if(horizontalPeekTime() < verticalPeekTime())
          {
            h = getHorizontalPointChunk();
            if(h.getTime() >= verticalPeekTime()) v = getVerticalPointChunk();
          }
          else  
          {
            v = getVerticalPointChunk();
            if(v.getTime() >= horizontalPeekTime()) h = getHorizontalPointChunk();
          }
        }
        plotChunks(v, h, g, d);
      }
    }
    catch(IntolerableException ie)
    {
      UniversalTellUser.general(null, ie);
    }
  }

  protected void mouseMoved(MouseEvent e)
  {
    int v = e.getY();
    double v_range_trans = max_val_v_trans - min_val_v_trans;
    double v_val_trans = min_val_v_trans-(v - h_pix)*v_range_trans/
                                         (h_pix-margin);
    double v_val = getVerticalInversion(v_val_trans);

    int h = e.getX();
    double h_range_trans = max_val_h_trans - min_val_h_trans;
    double h_val_trans = min_val_h_trans+h_range_trans*(((double)(h-margin))
                                         /((double)(getSize().width-2*margin)));
    double h_val = getHorizontalInversion(h_val_trans);

    setToolTipText(haxis+" = "+h_val+", "+vaxis+" = "+v_val);
  }

  private class ToolTipMouseMotionListener implements MouseMotionListener
  {

    private int i = 0;

    public void mouseDragged(MouseEvent e)
    {
    }

    public void mouseMoved(MouseEvent e)
    {
      Crossplot.this.mouseMoved(e);
    }

  }

  protected void addToTree()
  {
    cpa.addOneCrossplot(this);
  }

  protected void removeFromTree()
  {
    cpa.removeOneCrossplot(this);
  }

  protected void createJPG()
  {
    try
    {
      JFileChooser fc = new JFileChooser(HierarchyObject.getReferenceFilePath());
      fc.setDialogTitle("Point to the file. (With extension .jpg)");
      fc.showSaveDialog(UniversalTellUser.getFrame(this));
      File selFile = fc.getSelectedFile();
      if(selFile == null) return;
      String path = selFile.getAbsolutePath();
      if(path == null) return;
      if(!(path.endsWith(".jpg")))
      {
        JOptionPane.showMessageDialog(
          UniversalTellUser.getFrame(this), 
          "The filename must end with .jpg", 
          "File not valid", 
          JOptionPane.ERROR_MESSAGE);
        return;
      }
      Dimension d = getSize();
      BufferedImage image = new BufferedImage(d.width, 
                                              d.height, 
                                              BufferedImage.TYPE_USHORT_GRAY);
      Graphics2D graphics = image.createGraphics();
      paint(graphics, d);
      FileOutputStream fos = new FileOutputStream(selFile);
      JpegEncoder jpg = new JpegEncoder(image, 50, fos);
      jpg.Compress();
      fos.close();
    }
    catch(FileNotFoundException fnf)
    {
      UniversalTellUser.general(UniversalTellUser.getFrame(this), new SystemError("File not found", fnf));
    }
    catch(IOException ioe)
    {
      UniversalTellUser.general(UniversalTellUser.getFrame(this), new SystemError("I/O error", ioe));
    }
    catch(Exception e)
    {
      UniversalTellUser.general(UniversalTellUser.getFrame(this), 
                                new UnclassedError("crossplot - jpg-generation", e));
    }
  }

  protected JPopupMenu getPopup()
  {
    JPopupMenu popup = new JPopupMenu();
    JMenuItem menuItem = new JMenuItem("Add to tree");
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          Crossplot.this.addToTree();
        }
      });
    popup.add(menuItem);
    menuItem = new JMenuItem("Remove from tree");
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          Crossplot.this.removeFromTree();
        }
      });
    popup.add(menuItem);
    menuItem = new JMenuItem("Create .jpg-file");
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          Crossplot.this.createJPG();
        }
      });
    popup.add(menuItem);
    return popup;
  }

  private class PopupListener extends MouseAdapter 
  {
    
    public PopupListener()
    {
    }

    public void mousePressed(MouseEvent e) 
    {
      maybeShowPopup(e);
    }

    public void mouseReleased(MouseEvent e) 
    {
      maybeShowPopup(e);
    }

    private void maybeShowPopup(MouseEvent e) 
    {
      if(!e.isPopupTrigger()) return;
      JPopupMenu popup = Crossplot.this.getPopup();
      if(popup != null) popup.show(e.getComponent(), e.getX(), e.getY());
    }

  }

}
