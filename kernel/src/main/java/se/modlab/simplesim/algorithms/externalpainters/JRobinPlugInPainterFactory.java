package se.modlab.simplesim.algorithms.externalpainters;

import se.modlab.simplesim.variables.MonitoredVariableExtracted;

public class JRobinPlugInPainterFactory implements PlugInPainterFactory {

	public String getName() {
		return "JRobin";
	}
	
	public PlugInPainter getPainter(MonitoredVariableExtracted mve, boolean iterated) {
		return new JRobinPlugInPainter(mve, iterated);
	}
	
	public String toString() {
		return getName();
	}
 
}
