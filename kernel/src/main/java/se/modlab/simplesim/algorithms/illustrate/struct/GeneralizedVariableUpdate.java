package se.modlab.simplesim.algorithms.illustrate.struct;

import se.modlab.generics.sstruct.strings.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.generics.exceptions.*;

public class GeneralizedVariableUpdate extends VarUpdate {

	private VariableLookup give;

	public GeneralizedVariableUpdate(VariableLookup _get, VariableLookup _give,
			String filename, int line, int column)
	{
		super(_get, filename, line, column);
		give = _give;
	}

	public void execute(Scope s) 
	throws ReturnException, 
	IntolerableException, 
	StopException,
	BreakException,
	ContinueException
	{
		Variable varget = vl.getVariable(s);
		if(varget == null)
		{
			throw new UserCompiletimeError(
					"Variable "+vl+" on line "+
					vl.getLine()+", col "+
					vl.getColumn()+" not in scope. "+s);
		}
		Variable vargive = give.getVariable(s);
		if(vargive == null)
		{
			throw new UserCompiletimeError(
					"Variable "+give+" on line "+
					give.getLine()+", col "+
					give.getColumn()+" not in scope. "+s);
		}
		varget.setValue(vargive);
	}

	public void verify(Scope s) throws IntolerableException {
		Variable varget = vl.getVariable(s);
		if(varget == null)
		{
			throw new UserCompiletimeError(
					"Variable "+vl+" on line "+
					vl.getLine()+", col "+
					vl.getColumn()+" not in scope. "+s);
		}
		Variable vargive = give.getVariable(s);
		if(vargive == null)
		{
			throw new UserCompiletimeError(
					"Variable "+give+" on line "+
					give.getLine()+", col "+
					give.getColumn()+" not in scope. "+s);
		}
	}

}