package se.modlab.simplesim.algorithms.initiators;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.variables.*;

public interface Initiator
{
  public void runInitiation() throws IntolerableException;
  public VariableInstance getInstance();
}
