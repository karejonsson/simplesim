package se.modlab.simplesim.algorithms.once;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.simplesim.variables.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.algorithms.share.*;
import java.awt.*;
import se.modlab.simplesim.algorithms.presentation.*;

public class SegmentPainterOnce implements SegmentPainter
{

  protected void connectTwo(Graphics g, Dimension d,
                            double val_1, double time_1, double val_2, double time_2,
                            DrawingParameters dp)
  {
    g.drawLine((int) (dp.w_pix + (d.width-2*dp.w_pix)*(time_1/dp.endTime)),
               (int) (dp.h_pix - (val_1-dp.min_val)/(dp.h_val)*(dp.h_pix-dp.margin)),
               (int) (dp.w_pix + (d.width-2*dp.w_pix)*(time_2/dp.endTime)),
               (int) (dp.h_pix - (val_1-dp.min_val)/(dp.h_val)*(dp.h_pix-dp.margin)));

    g.drawLine((int) (dp.w_pix + (d.width-2*dp.w_pix)*(time_2/dp.endTime)),
               (int) (dp.h_pix - (val_1-dp.min_val)/(dp.h_val)*(dp.h_pix-dp.margin)),
               (int) (dp.w_pix + (d.width-2*dp.w_pix)*(time_2/dp.endTime)),
               (int) (dp.h_pix - (val_2-dp.min_val)/(dp.h_val)*(dp.h_pix-dp.margin)));
  }

  protected double getValueGenerically(DrawingParameters dp, int index)
    throws IntolerableException
  {
    sValue sv = dp.mve.getValue(index);
    if(sv instanceof sBoolean)
    {
      return (sv.getBoolean().booleanValue())? 1:0;
    }
    return sv.getValue();
  }

  public void paint(Graphics g, 
                    Dimension d, 
                    DrawingParameters dp,
                    int index)
    throws IntolerableException
  {
    double val_1 = getValueGenerically(dp, index);
    double time_1 = dp.mve.getValue(index).getTime();
    double val_2 = getValueGenerically(dp, index+1);
    double time_2 = dp.mve.getValue(index+1).getTime();
    //System.out.println("paint");
    connectTwo(g, d, val_1, time_1, val_2, time_2, dp);
  }

  public void paintLast(Graphics g, 
                        Dimension d, 
                        DrawingParameters dp)
    throws IntolerableException
  {
    double val_1 = getValueGenerically(dp, dp.mve.getSize()-1);
    double time_1 = dp.mve.getValue(dp.mve.getSize()-1).getTime();
    connectTwo(g, d, val_1, time_1, val_1, dp.endTime, dp);
  }

}