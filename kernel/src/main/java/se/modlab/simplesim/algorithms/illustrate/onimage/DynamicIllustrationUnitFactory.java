package se.modlab.simplesim.algorithms.illustrate.onimage;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.logics.*;
import javax.swing.*;
import se.modlab.simplesim.algorithms.illustrate.struct.*;
import se.modlab.simplesim.algorithms.illustrate.share.*;
import se.modlab.generics.sstruct.strings.*;
import se.modlab.generics.sstruct.variables.*;

public class DynamicIllustrationUnitFactory 
       implements DynamicIllustrationFactory
{

  private String typesName = null;
  private ScopeGivingProgram calc;
  private LogicalExpression visi;
  private StringEvaluable iconfile;
  private String iconFileCalculated;
  private StringEvaluable background;
  private ArithmeticEvaluable hori;
  private ArithmeticEvaluable vert;
  private StringEvaluable text;
  private Scope localScope = null;

  private ImageIcon icon = null;
//  private static Hashtable colors = new Hashtable();

  private int width = -1;
  private int height = -1;

  public DynamicIllustrationUnitFactory(
                    String _typesName,
                    ScopeGivingProgram _calc,
                    LogicalExpression _visi,
                    StringEvaluable _iconfile,
                    StringEvaluable _background,
                    ArithmeticEvaluable _hori,
                    ArithmeticEvaluable _vert,
                    StringEvaluable _text)
  {
    typesName = _typesName;
    //sg = _sg;
    calc = _calc;
    visi = _visi;
    iconfile = _iconfile;
    background = _background;
    hori = _hori;
    vert = _vert;
    text = _text;
  }

  public String getFactoryName()
  {
    return typesName;
  }

  public IllustrationUnitFactoried getInstance(VariableInstance vi)
    throws IntolerableException
  {
    return new DynamicIllustrationUnit(
      vi, calc, visi, iconfile, background, hori, vert, text);
  }

}

