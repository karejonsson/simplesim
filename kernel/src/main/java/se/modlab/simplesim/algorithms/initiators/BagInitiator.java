package se.modlab.simplesim.algorithms.initiators;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.bags.*;

public class BagInitiator implements Initiator
{

  private Bag b;

  public BagInitiator(Bag _b)
  {
    b = _b;
  }


  public String getName()
  {
    return b.toString();
  }

  public VariableInstance getInstance()
  {
    return (VariableInstance) b;
  }

  public void runInitiation() throws IntolerableException
  {
    b.reset();
    //System.out.println("initiated "+q.toString());
  }

  public String toString()
  {
    return "Bag initiator for "+b;
  }

}
