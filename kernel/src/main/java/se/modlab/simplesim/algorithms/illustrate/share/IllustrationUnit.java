package se.modlab.simplesim.algorithms.illustrate.share;

import java.awt.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.exceptions.*;

public interface IllustrationUnit
{

  public void init(int _width, int _height);
  public void setGlobalScope(Scope s)
    throws IntolerableException;
  public String getIconFilenameCalculated();
  public String getName();
  public void paint(Graphics g)
    throws IntolerableException;

}
