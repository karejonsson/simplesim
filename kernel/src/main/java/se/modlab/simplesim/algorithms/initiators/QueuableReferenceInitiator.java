package se.modlab.simplesim.algorithms.initiators;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.simplesim.events.*;
import se.modlab.simplesim.references.*;

public class QueuableReferenceInitiator implements Initiator
{
  private QueuableReference qr;
  private Scope s;
  private ReferenceEvaluable re;
  private String fakedName;

  public QueuableReferenceInitiator(QueuableReference _qr, 
                                    Scope _s, 
                                    ReferenceEvaluable _re)
    throws IntolerableException 
  {
    qr = _qr;
    s = _s;
    re = _re;
    fakedName = _qr.getName();
  }

  public QueuableReferenceInitiator(QueuableReference _qr, 
                                    Scope _s, 
                                    ReferenceEvaluable _re,
                                    String _fakedName)
    throws IntolerableException 
  {
    qr = _qr;
    s = _s;
    re = _re;
    fakedName = _fakedName;
  }

  public String getName()
  {
    return qr.getName();
  }

  public VariableInstance getInstance()
  {
    return qr;
  }

  public void runInitiation() throws IntolerableException
  {
    if(re == null)
    {
      qr.setInitialValue(new sQueuable());
      return;
    }
    //variableInstance vi = re.evaluate(s);
    sValue sv = re.evaluate(s);

    if(sv == null)
    {
      qr.setInitialValue(new sQueuable());
      return;
    }

    if(!(sv instanceof sQueuable))
    {
      throw new UserRuntimeError(
        "The variable "+qr+" is a queuable or reference to a\n"+
        "queuable but assigned the value "+sv);
    }
    Enqueuable eq = ((sQueuable) sv).getQueuable();
    qr.setInitialValue((VariableInstance)eq);
  }

  public String toString()
  {
    return qr.toString()+" in scope "+s.getName()+
      " initiated to "+qr;
  }

}

