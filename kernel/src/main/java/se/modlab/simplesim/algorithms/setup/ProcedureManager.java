package se.modlab.simplesim.algorithms.setup;

import java.io.InputStream;
import java.util.Hashtable;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.logics.LogicalExpression;
import se.modlab.simplesim.variables.Manager;

public class ProcedureManager implements Manager
{

  private Procedure proc= null;

  public ProcedureManager(Procedure _proc)
  {
    proc = _proc;
  }

  public void initialize(Scope s, boolean isSharp, Hashtable<VariableInstance, Manager> managermap)
    throws IntolerableException
  {
    s.addProcedure(proc);
  } 
  
	public void reinitializeLogical(LogicalExpression le) throws IntolerableException {
		throw new UserRuntimeError(
				"Cannot set a procedure to the value of a boolean expression.\n"+
				"This happens to the procedure "+proc.getName()+".");
    }

	public void reinitializeArithmetic(ArithmeticEvaluable ae) throws IntolerableException {
		throw new UserRuntimeError(
				"Cannot set a procedure type to the value of a arithmetic expression.\n"+
				"This happens to the a procedure "+proc.getName()+".");
    }

	public void reinitializeFromInputStream(InputStream is) throws IntolerableException {
		throw new InternalError(
				"Cannot set an actor instance to the value of input stream.\n"+
				"This happens to the a procedure "+proc.getName()+".");
    }


}