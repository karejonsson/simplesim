package se.modlab.simplesim.algorithms.presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.util.*;
import se.modlab.generics.gui.find.StringGridBagLayout;
import java.awt.geom.Dimension2D;
import se.modlab.simplesim.variables.*;
import se.modlab.generics.gui.util.*;

public class Biplot extends JDialog 
{

  //private static String elems[] = {"Ett", "Tv�", "Tre", "Fyra"};
  private JComboBox xaxis;// = new JComboBox();
  private JComboBox yaxis;// = new JComboBox();
  private JButton cancelButton = new JButton("Cancel");
  private JButton goButton = new JButton("Go!");
  private JCheckBox xlin = new JCheckBox();
  private JCheckBox xlog = new JCheckBox();
  private JCheckBox ylin = new JCheckBox();
  private JCheckBox ylog = new JCheckBox();

  private MonitoredVariableExtracted extracts[];
  private CrossplotAdder cpa;
  private SegmentPainter sp;
  private SimpleExplorer se;

  public Biplot(SimpleExplorer _se, 
                boolean flag, 
                MonitoredVariableExtracted _extracts[],
                CrossplotAdder _cpa,
                SegmentPainter _sp)
  {
    super(_se, flag);
    extracts = _extracts;
    cpa = _cpa;
    sp = _sp;
    se = _se;
    xaxis = new JComboBox(extracts);
    yaxis = new JComboBox(extracts);
    initComponents();
    pack();
  }

  private void initComponents()
  {
    Container container = getContentPane();
    container.setLayout(new StringGridBagLayout());
    setTitle("Biplot");
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent windowevent) {
        setVisible(false);
        dispose();
      }
    });

    JLabel jlabel = new JLabel();
    jlabel.setDisplayedMnemonic('U');
    jlabel.setLabelFor(xaxis);
    jlabel.setText("X axis");
    container.add("anchor=WEST,insets=[12,12,0,0]", jlabel);
    xaxis.setToolTipText("Choose variable");
    container.add("fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", xaxis);
    
    xlin.setMargin(new Insets(0, 2, 0, 2));
    xlin.setToolTipText("Linear conversion");
    xlin.setSelected(true);
    xlin.setText("linear");
    container.add(
      "gridx=2,gridy=0,anchor=WEST,insets=[12,12,0,11]",
	    xlin);

    xlog.setMargin(new Insets(0, 2, 0, 2));
    xlog.setToolTipText("Logarithmic conversion");
    xlog.setSelected(true);
    xlog.setText("logarithmic");
    container.add(
      "gridx=3,gridy=0,anchor=WEST,insets=[12,12,0,11]",
	    xlog);

    ButtonGroup group1 = new ButtonGroup();
    group1.add(xlin);
    group1.add(xlog);

    JLabel jlabel1 = new JLabel();
    jlabel1.setDisplayedMnemonic('P');
    jlabel1.setText("Y axis");
    jlabel1.setLabelFor(yaxis);
    container.add("gridx=0,gridy=1,anchor=WEST,insets=[11,12,0,0]", jlabel1);
    yaxis.setToolTipText("Choose variable");
    container.add("gridx=1,gridy=1,fill=HORIZONTAL,weightx=1.0,insets=[11,7,0,11]", yaxis);

    ylin.setMargin(new Insets(0, 2, 0, 2));
    ylin.setToolTipText("Linear conversion");
    ylin.setSelected(true);
    ylin.setText("linear");
    container.add(
      "gridx=2,gridy=1,anchor=WEST,insets=[12,12,0,11]",
	    ylin);

    ylog.setMargin(new Insets(0, 2, 0, 2));
    ylog.setToolTipText("Logarithmic conversion");
    ylog.setSelected(true);
    ylog.setText("logarithmic");
    container.add(
      "gridx=3,gridy=1,anchor=WEST,insets=[12,12,0,11]",
	    ylog);

    ButtonGroup group2 = new ButtonGroup();
    group2.add(ylin);
    group2.add(ylog);

    JPanel jpanel = createButtonPanel();
    container.add("gridx=0,gridy=2,gridwidth=2,anchor=EAST,insets=[17,12,11,11]", jpanel);
    getRootPane().setDefaultButton(goButton);
  }

  private JPanel createButtonPanel()
  {
    JPanel jpanel = new JPanel();
    jpanel.setLayout(new BoxLayout(jpanel, 0));
    goButton.setText("Go!");
    goButton.setToolTipText("Press to plot");
    goButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent actionevent) {
        plot();
      }
    });
    jpanel.add(goButton);
    jpanel.add(Box.createRigidArea(new Dimension(5, 0)));
    cancelButton.setText("Cancel");
    cancelButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent actionevent)
      {
        cancel();
      }
    });
    jpanel.add(cancelButton);
    jpanel.add(Box.createRigidArea(new Dimension(5, 0)));
    Vector vector = new Vector(2);
    vector.add(cancelButton);
    vector.add(goButton);
    equalizeComponentSizes(vector);
    vector.removeAllElements();
    return jpanel;
  }

  private void equalizeComponentSizes(java.util.List list)
  {
    //boolean flag = false;
    Dimension dimension = new Dimension(0, 0);
    //Object obj = null;
    //Object obj1 = null;
    for(int i = 0; i < list.size(); i++)
    {
      JComponent jcomponent = (JComponent)list.get(i);
      Dimension dimension1 = jcomponent.getPreferredSize();
      dimension.width = Math.max(dimension.width, (int)dimension1.getWidth());
      dimension.height = Math.max(dimension.height, (int)dimension1.getHeight());
    }

    for(int j = 0; j < list.size(); j++)
    {
      JComponent jcomponent1 = (JComponent)list.get(j);
      jcomponent1.setPreferredSize((Dimension)dimension.clone());
      jcomponent1.setMaximumSize((Dimension)dimension.clone());
    }
  }

  private void plot()
  {
    MonitoredVariableExtracted mveh = (MonitoredVariableExtracted) getVariableOne();
    MonitoredVariableExtracted mvev = (MonitoredVariableExtracted) getVariableTwo();
    boolean xlin = getVariableOneLinear();
    boolean ylin = getVariableTwoLinear();
    if(xlin && ylin)
    {
      se.setRightSide(new CrossplotLinearLinear(mveh, mvev, cpa, sp));
      return;
    }
    if(xlin && !ylin)
    {
      se.setRightSide(new CrossplotLinearLog(mveh, mvev, cpa, sp));
      return;
    }
    if(!xlin && ylin)
    {
      se.setRightSide(new CrossplotLogLinear(mveh, mvev, cpa, sp));
      return;
    }
    if(!xlin && !ylin)
    {
      se.setRightSide(new CrossplotLogLog(mveh, mvev, cpa, sp));
      return;
    }
/*
    System.out.println("plot");
    System.out.println("X axis "+getVariableOne());
    System.out.println("X axis linear "+getVariableOneLinear());
    System.out.println("Y axis "+getVariableTwo());
    System.out.println("Y axis linear "+getVariableTwoLinear());
 */
  }
  
  private void cancel()
  {
    setVisible(false);
    dispose();
  }
  
  public Object getVariableOne()
  {
    return xaxis.getSelectedItem();
  }

  public boolean getVariableOneLinear()
  {
    return xlin.isSelected();
  }

  public Object getVariableTwo()
  {
    return yaxis.getSelectedItem();
  }

  public boolean getVariableTwoLinear()
  {
    return ylin.isSelected();
  }

/*
  public static void main(String args[])
  {
    JFrame jframe = new JFrame() {

      public Dimension getPreferredSize()
      {
        return new Dimension(200, 100);
      }
    };
    jframe.setTitle("Debugging frame");
    jframe.setDefaultCloseOperation(3);
    jframe.pack();
    jframe.setVisible(false);
    biplot bp = new biplot(jframe, true, null, null, null);
    bp.pack();
    bp.setVisible(true);
  }
 */

}

