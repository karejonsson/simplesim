package se.modlab.simplesim.algorithms.externalpainters;

public class ExternalPainterFactoryHandle {
	
	private static final PlugInPainterFactory[] painters = new PlugInPainterFactory[] { new JRobinPlugInPainterFactory() };

	public static PlugInPainterFactory[] get() {
		return painters;
	}
	// PlugInPainterFactory pipfs[] = ExternalPainterFactoryHandle.get();
}
