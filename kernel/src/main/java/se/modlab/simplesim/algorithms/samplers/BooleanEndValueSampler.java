package se.modlab.simplesim.algorithms.samplers;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.values.*;

public class BooleanEndValueSampler implements EndValueSampler
{

  private BooleanVariable sbv;
  private Scope s;
  private LogicalExpression le;
  private String fakedName;
  private int _true = 0;
  private int _false = 0;

  public BooleanEndValueSampler(BooleanVariable sbv)
  {
    this.sbv = sbv;
    fakedName = sbv.getName();
  }

  public BooleanEndValueSampler(BooleanVariable sbv,
                                String fakedName)
  {
    this.sbv = sbv;
    this.fakedName = fakedName;
  }

  public String getName()
  {
    return sbv.getName();
  }

  public VariableInstance getInstance()
  {
    return sbv;
  }

  public void runSampling() throws IntolerableException
  {
    if(sbv.getValue().getBoolean().booleanValue())
    {
       _true++;
    }
    else
    {
       _false++;
    }
  }

  public void runSetAverageValue() throws IntolerableException
  {
    if((_true+_false) == 0)
    {
      throw new InternalError(
        "Zero samples in class SimBooleanVariableInitiator");
    }
    sbv.setValue(new sBoolean(_true > _false));
    _true = 0;
    _false = 0;
  }

  public String toString()
  {
    try
    {
      return sbv.getScopesName()+"/"+fakedName+
             " value "+sbv.getValue()+
             " True happened "+_true+" times and false "+_false;
    }
    catch(IntolerableException ie)
    {
      return sbv.getScopesName()+"/"+fakedName+
             " "+
             " True happened "+_true+" times and false "+_false;
    }
  }

}

