package se.modlab.simplesim.algorithms.setup;

import java.io.InputStream;
import java.util.Hashtable;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.logics.LogicalExpression;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.simplesim.scoping.*;
import se.modlab.simplesim.variables.*;
import se.modlab.simplesim.algorithms.initiators.*;
import se.modlab.simplesim.algorithms.samplers.*;
import se.modlab.simplesim.variables.*;

public class ComplexVectorManager implements Manager
{

	private String type;
	private String name = null;
	private ArithmeticEvaluable ae = null;
	private String filename;
	private int line;
	private int column;
	private SimData sd;
	private boolean _con;
	private boolean _mon;
	private boolean _exp;
	private boolean _pub;

	public ComplexVectorManager(String _type,
			String _name,
			ArithmeticEvaluable _ae,
			String _filename,
			int _line,
			int _column,
			SimData _sd,
			boolean _const,
			boolean _monitor,
			boolean _export,
			boolean _public)
	{
		type = _type;
		name = _name;
		ae = _ae;
		filename = _filename;
		line = _line;
		column = _column;
		sd = _sd;
		_con = _const;
		_mon = _monitor;
		_exp = _export;
		_pub = _public;
	}

	public String getFilename()
	{
		return filename;
	}  

	public int getLine()
	{
		return line;
	}  

	public int getColumn()
	{
		return line;
	}  

	private void _initialize(Scope s, boolean isSharp, Hashtable<VariableInstance, Manager> managermap)
	throws IntolerableException
	{
		SimVariableFactory vf = (SimVariableFactory) s.getFactory(type);
		if(vf == null)
		{
			throw new UserCompiletimeError("No type "+name); 
		}
		sValue sLen = ae.evaluate(s);
		if(!(sLen instanceof sLong))
		{
			throw new UserCompiletimeError("Expression for length of vector "+name+
					" of type "+type+" does not evaluate to a long value.\n"+
					"It became "+sLen+".\n"); 
		}
		long length = ((sLong) sLen).getLong().longValue();
		SimVectorVariableFactory svvf = new SimVectorVariableFactory(vf , (int) length);
		svvf = (SimVectorVariableFactory) s.addFactoryUniquely(svvf);
		VariableInstance vi = null;// = factory.getInstance(name, _mon);
		if(_mon)
		{
			//System.out.println("complex vector manager mon = true "+name);
			vi = svvf.getInstance(name, true, filename, line, column, managermap);
		}
		else
		{
			if(!_con)
			{
				//System.out.println("complex vector manager com = false "+name);
				vi = svvf.getInstance(name, false, filename, line, column, managermap);
			}
			else
			{
				throw new UserCompiletimeError(
						"You may not declare a variable vector ("+name+") constant.\n"+
						"Constant trues makes no sense.\n"+
						"This happens with the variable "+name+".");
				/*vi = factory.getInstance(name, false);
        vi.accept(cevv);
        vi = cevv.getExportedVersion();*/
			}
		}
		s.addComplexInstance(vi);
		if(isSharp)
		{
			sd.addInitiator(new ComplexInitiator(vi, s));
			sd.addEndValueSampler(new ComplexEndValueSampler(vi));
			if(_mon) 
			{
				//sd.addMonitoredVariable((monitoredVariable) vi);
			}
		}
		vi.setDefaultInitialValue();
		if(_exp)
		{
			Scope gs = sd.getGlobalScope();
			if(gs == s)
			{
				throw new UserCompiletimeError(
						"You may not export variables from global scope.\n"+
						"This happens with the variable "+name+".");
			}
			SimScope ss = (SimScope) s;
			ss.addMember(new SimVariableVectorExported((VariableVector) vi, "exported"));
		}
		if(_pub)
		{
			Scope gs = sd.getGlobalScope();
			if(gs == s)
			{
				throw new UserCompiletimeError(
						"You may not declare variables public in global scope.\n"+
						"This happens with the variable "+name+".");
			}
			SimScope ss = (SimScope) s;
			ss.addMember(vi);
		}
		if(managermap != null) {
			managermap.put(vi, this);
		}
	} 

	public void initialize(Scope s, boolean isSharp, Hashtable<VariableInstance, Manager> managermap)
	throws IntolerableException
	{
		try {
			_initialize(s, isSharp, managermap);
		}
		catch(UserRuntimeError ue) {
			String m = ue.getMessage()+"\n"+"This happened in file "+filename+", line "+line+", column "+column;
			UserRuntimeError ue2 = new UserRuntimeError(m, ue.getCause(), ue.getCollectors());
			ue2.setAction(ue.getAction());
			throw ue2;
		}
		catch(UserCompiletimeError ue) {
			String m = ue.getMessage()+"\n"+"This happened in file "+filename+", line "+line+", column "+column;
			UserCompiletimeError ue2 = new UserCompiletimeError(m, ue.getCause(), ue.getCollectors());
			ue2.setAction(ue.getAction());
			throw ue2;
		}
	}


	public String toString()
	{
		return "complexVectorManager for "+name+
		" with const = "+_con+
		", monitor = "+_mon+
		", export = "+_exp+
		", public = "+_pub+
		" in file "+filename+" on line "+line+" on column "+column+
		".\n Factory is for type "+type;
	}
	
	public void reinitializeLogical(LogicalExpression le) throws IntolerableException {
		throw new UserRuntimeError(
				"Cannot set a complex vector to the value of a boolean expression.\n"+
				"This happens to the complex vector "+name+" declared on line "+line+", column "+column+" in the file "+filename);
    }

	public void reinitializeArithmetic(ArithmeticEvaluable ae) throws IntolerableException {
		throw new UserRuntimeError(
				"Cannot set a complex vector to the value of an arithmetic expression.\n"+
				"This happens to the complex vector "+name+" declared on line "+line+", column "+column+" in the file "+filename);
    }

	public void reinitializeFromInputStream(InputStream is) throws IntolerableException {
		throw new InternalError(
				"Cannot set complex vector to the value of input stream.\n"+
				"This happens to the vector "+name+" declared on line "+line+", column "+column+" in the file "+filename);
    }

}