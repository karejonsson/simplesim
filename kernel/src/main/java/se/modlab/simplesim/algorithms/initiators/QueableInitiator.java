package se.modlab.simplesim.algorithms.initiators;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.events.*;

public class QueableInitiator implements Initiator {

  private Enqueuable q;

  public QueableInitiator(Enqueuable _q) {
    q = _q;
  }

  public String getName() {
    return q.toString();
  }

  public VariableInstance getInstance() {
    return (VariableInstance) q;
  }

  public void runInitiation() throws IntolerableException {
    q.reset();
    //System.out.println("initiated "+q.toString());
  }

  public String toString() {
    return "Enqueuable initiator for "+q;
  }

}
