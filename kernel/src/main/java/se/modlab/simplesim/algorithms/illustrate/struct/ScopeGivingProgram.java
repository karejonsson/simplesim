package se.modlab.simplesim.algorithms.illustrate.struct;

import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.executables.*;

public class ScopeGivingProgram extends ProgramBlock
{

  public ScopeGivingProgram(ScopeFactory _sf)
  {
    super(_sf);
  }

  public Scope executeGiveScope(Scope s) 
    throws ReturnException, 
           IntolerableException, 
           StopException,
           BreakException,
           ContinueException
  {
    Scope tmp = sf.getInstance(s, "Temporary program");
    try {
      for(int i = 0 ; i < statements.size() ; i++) {
        ProgramStatement ps = (ProgramStatement) statements.elementAt(i);
        ps.execute(tmp);
      } 
    }
    catch(ReturnException re) {
    }
    catch(BreakException be) {
    }
    catch(ContinueException ce) {
    }
    return tmp;
  }

}
