package se.modlab.simplesim.algorithms.presentation;

import se.modlab.generics.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import java.util.Enumeration;
import java.util.Vector;
import javax.swing.tree.TreeNode;
import javax.swing.JTextArea;
import java.awt.Component;
import se.modlab.simplesim.variables.*;

public class OneQueuableReference extends VariableNode
{
  private String fullName = null;
  private Object v;
  //private TreeNode parent;
  private SegmentPainter sp;

  public OneQueuableReference(String _fullName,
                              Object _v, 
                              TreeNode _parent, 
                              SegmentPainter _sp)
  {
	super(_parent);
    fullName = _fullName;
    v = _v; 
    sp = _sp;
    parent = _parent;
  }

  public static final String noSampleMessage = 
    "No sampling done for queuable reference";

  public Component getComponent()
  {
    String msg = noSampleMessage;
    if(v != null) msg = v.toString();
    return new JTextArea(msg);
  }  

  public String toString()
  {
    return fullName;
  }

} 