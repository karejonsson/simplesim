package se.modlab.simplesim.algorithms.presentation;

import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;

public class DrawingParameters
{
  public int w_pix;
  public int h_pix;
  public double min_val;
  public double h_val;
  public double endTime;
  public int margin;
  public MonitoredVariableExtracted mve;
  
  public DrawingParameters(
    int w_pix,
    int h_pix,
    double min_val,
    double h_val,
    double endTime,
    int margin,
    MonitoredVariableExtracted mve)
  {
    this.w_pix = w_pix;
    this.h_pix = h_pix;
    this.min_val = min_val;
    this.h_val = h_val;
    this.endTime = endTime;
    this.margin = margin;
    this.mve = mve;
  }

}
