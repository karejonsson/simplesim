package se.modlab.simplesim.algorithms.samplers;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.variables.*;

public class ComplexEndValueSampler implements EndValueSampler
{
  private VariableInstance mv;
  //private scope s;
  private EndValueSampler samplers[];

  public ComplexEndValueSampler(VariableInstance mv)
    throws IntolerableException 
  {
    this.mv = mv;
    //this.s = s;
    InstanceEndValueSamplerCollectorVisitor iicv = 
      new InstanceEndValueSamplerCollectorVisitor();
    mv.accept(iicv);
    samplers = iicv.getSamplers();
  }

  public String getName()
  {
    return mv.getName();
  }

  public VariableInstance getInstance()
  {
    return mv;
  }

  public EndValueSampler[] getSamplers()
  {
    return samplers;
  }

  public void runSampling() throws IntolerableException
  {
    for(int i = 0 ; i < samplers.length ; i++)
    {
      samplers[i].runSampling();
    }
  }

  public void runSetAverageValue() throws IntolerableException
  {
    for(int i = 0 ; i < samplers.length ; i++)
    {
      samplers[i].runSetAverageValue();
    }
  }
  
  public String toString()
  {
    StringBuffer sb = new StringBuffer();
    for(int i = 0 ; i < samplers.length ; i++)
    {
      sb.append(samplers[i].toString());
      if(i != samplers.length -1) sb.append("\n");
    }
    return sb.toString();
  }

}

