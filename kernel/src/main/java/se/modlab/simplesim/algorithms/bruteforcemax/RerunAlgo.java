package se.modlab.simplesim.algorithms.bruteforcemax;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.simplesim.algorithms.share.*;

import java.awt.*;
import java.util.*;

public class RerunAlgo implements Runnable
{

  private Algorithm algo;
  private Vector resetters;

  public RerunAlgo(Algorithm algo, Vector resetters)
  {
    this.algo = algo;
    this.resetters = resetters;
  }

  public void run()
  {
    try
    {
      //System.out.println("YY "+algo.getClass().getName());
      algo.runAlgorithm();
      algo.displayResultsGraphically(true);
    }
    catch(Exception e)
    {
      UniversalTellUser.general(null, new UnclassedError(null, e));
    }
 
  }

}
