package se.modlab.simplesim.algorithms.regression;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.values.*;

import java.util.*;

public class Segment
{

  //private static int antal_instancierade = 0;
  //private static int antal_borttagna = 0;

  private double sum_straight;
  private double sum_squares;
  private double maximum;
  private double minimum;
  private boolean unset = true;
  private int no_values;
  private int place;
  private Vector<Double> occurrances = new Vector<Double>();

  public Segment(int place)
  {
    this.place = place;
    //antal_instancierade++;
  }

/*
  protected void finalize()
  {
    antal_borttagna++;
    System.out.println("segment r?kning I:"+antal_instancierade+", B:"+antal_borttagna+
      ", M: "+(antal_instancierade-antal_borttagna));
  }
 */

  public void addOccurrance(double value)
  { 
    occurrances.addElement(new Double(value));
    if(unset) {
      maximum = value;
      minimum = value;
      unset = false;
      return;
    }
    maximum = Math.max(maximum, value);
    minimum = Math.min(minimum, value);
  }

  public void addOccurrance(sValue value)
    throws IntolerableException
  {
    if(value instanceof sBoolean) {
      addOccurrance(((sBoolean) value).getBoolean().booleanValue() ? 1.0 : 0.0);
      return;
    }
    addOccurrance(value.getValue());
  }

  public void consume()
    throws IntolerableException
  {
    if(occurrances.size() == 0) {
      throw new InternalError("segment - consume. No values added");
    }
    double sum = occurrances.elementAt(0).doubleValue();
    for(int i = 1 ; i < occurrances.size() ; i++) {
      sum += occurrances.elementAt(i).doubleValue();
    }
    addValue(sum / occurrances.size());
    occurrances.removeAllElements();
  }

  private void addValue(double value)
  {
    sum_straight += value;
    sum_squares += Math.pow(value, 2);
    no_values++;
  }
/*
  private void addValue(SimValue value)
    throws intolerableException
  {
    if(value instanceof SimBoolean)
    {
      addValue(((SimBoolean) value).getBoolean().booleanValue() ? 1.0 : 0.0);
      return;
    }
    addValue(value.getValue());
  }
 */
  public double getVariation()
    throws IntolerableException
  {
    if(no_values == 0) return 0;
    return (sum_squares/no_values) - Math.pow(getAverage(), 2);
  }

  public double getDeviation()
    throws IntolerableException
  {
    double var = getVariation();
    if(var < 0) 
    {
      return 0;
      /*
         This is actually correct. When a variable is statically assigned a value 
         at its initiation it will always get the same value in every sweep.
         Then the variation should become zero analythically but in the real 
         world with just so many bits a round of error could make a value just
         below zero but something like -1.35E-20. 
       */
    }
/*
    {
      System.out.println("\nClass segment - getDeviation. Neg: sum_straight = "+
                         sum_straight+
 //                        ", sum_squares = "+sum_squares+
 //                        ", no_values = "+no_values+
                         ", Variation = "+var+
 //                        ", Average = "+getAverage()+
                         ", Square average = "+Math.pow(getAverage(), 2)+
                         ", Average square = "+(sum_squares/no_values)+
                         ", place = "+place);
      for(int i = 0 ; i < values.size() ; i++)
      {
        System.out.print(" "+values.elementAt(i));
      }
    }
 */
    return Math.sqrt(getVariation());
  }

  public double getMeasure()
    throws IntolerableException
  {
    if(no_values < 2)
    {
      throw new InternalError(
        "Measure not valid until two values exists.\n"+
        "This error was diagnosticised in class regression/segment");
    }
    return getDeviation() / (no_values-1);
  }

  public double getAverage()
    throws IntolerableException
  {
    if(no_values == 0)
    {
      throw new InternalError(
        "Average not valid until one value exists. ("+place+")\n"+
        "This error was diagnosticised in class regression.segment.getAverage");
    }
    return sum_straight / no_values;
  }

  public double getMaximum()
    throws IntolerableException
  {
    if(unset)
    {
      throw new InternalError(
        "Maximum not valid until one value exists.\n"+
        "This error was diagnosticised in class regression.segment.getMaximum");
    }
    return maximum;
  }

  public double getMinimum()
    throws IntolerableException
  {
    if(unset)
    {
      throw new InternalError(
        "Minimum not valid until one value exists.\n"+
        "This error was diagnosticised in class regression.segment.getMinimum");
    }
    return minimum;
  }

}