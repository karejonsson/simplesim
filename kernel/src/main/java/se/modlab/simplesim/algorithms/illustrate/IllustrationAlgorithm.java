package se.modlab.simplesim.algorithms.illustrate;

import se.modlab.generics.gui.util.*;
import se.modlab.generics.files.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.gui.progress.*;
import se.modlab.simplesim.variables.*; 
import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.simplesim.events.*;
import se.modlab.simplesim.algorithms.share.*;
import se.modlab.simplesim.algorithms.once.*;
import se.modlab.simplesim.algorithms.debug.*;
import se.modlab.simplesim.algorithms.externalpainters.ExternalPainterFactoryHandle;
import se.modlab.simplesim.algorithms.setup.*;
import se.modlab.simplesim.algorithms.presentation.*;
import se.modlab.simplesim.algorithms.illustrate.parse.*;
import se.modlab.simplesim.algorithms.illustrate.share.*;
import se.modlab.simplesim.scoping.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.swing.*;
import java.util.*;
import java.io.*;

public class IllustrationAlgorithm extends OnceAlgorithm
{

  private SimpleExplorerNode nodes[];
  private Vector registeredEvents = new Vector();
  private Commons com;
  private int event_ctr = 0;
  //private double timeunit;
  private String filesPath;
  private long timeAtStart;
  private long timeUnit;
  private Illustrator ill;
  private JFrame et = null;
  private boolean eventtable; 

  public IllustrationAlgorithm(SimData sd,
                               long _timeUnit, String _filesPath,
                               boolean _eventtable)
    throws IntolerableException
  {
    super(sd);
    com = sd.getCommons();
    //timeUnit = com.getTimeUnit();
    timeUnit = _timeUnit;
    //System.out.println("illustrationAlgorithm - ctor - timeUnit = "+timeUnit);
    filesPath = _filesPath;
    eventtable = _eventtable;
    timeAtStart = System.currentTimeMillis();
    sd.addCollector(new FileCollector(filesPath));
    ill = IluParser.parseFromFile(filesPath, new DefaultScopeFactory(), sd);
  }

  private void addEvent(SimulationEvent event)
    throws IntolerableException
  {
    long l = com.getCurrentEpochSimTime();
    String caltime = Commons.getTimeStringFromLong(l);
    EventRow er = null;
    if(eventtable)
    {
      double currentTime = com.getCurrentSimTime();
      er = new EventRow(event_ctr,
                        event.getEventTypeName(),
                        event.getEventName(),
                        currentTime,
                        caltime);
    }
    if(eventtable)
    {
      registeredEvents.addElement(er);
      event_ctr++;
    }
  }

  public void executeSimulation()
    throws IntolerableException
  {
    //System.out.println("illustrationAlgorithm - executeSimulation 0");
    //System.out.println("Debug - executeSimulation");
    Commons com = sd.getCommons();
    boolean stopHistoryNotTroublesome = true;
    try
    {
      //System.out.println("Innan:\n"+eq);
      Vector ies = sd.getInitially();
      for(int i = 0 ; i < ies.size() ; i++)
      {
        InitiallyEvent ie = (InitiallyEvent) ies.elementAt(i);
        addEvent(ie);
        ie.runEvent();
      }
      ill.repaint();
      if(eventtable) 
      { 
        et.repaint();
      }
      //System.out.println("illustrationAlgorithm - executeSimulation 1");
      boolean repaint = false;
      while(!queueEmpty())
      {
        SimulationEvent event = getFromQueue();    
        double timeForNext = event.getSimTimeForNextEvent();
        if(timeForNext != com.getCurrentSimTime())
        {
          repaint = true;
        }
        com.setCurrentSimTime(timeForNext);
        addEvent(event);
        event.runEvent();
        ill.updated(event);
        //System.out.println("debugAlgo - executeSimulation - trigged.size() = "+trigged.size());
        if(event.shallBeRequeued())
        {
          queueUp(event);
        }
        for(int i = trigged.size() - 1 ; i >= 0 ; i--)
        {
          ProcedureTrigged triggedEvent = (ProcedureTrigged) trigged.elementAt(i);
          if(triggedEvent.shallRun())
          {
            addEvent(triggedEvent);
            triggedEvent.runEvent();
            ill.updated(event);
          }
          if(!triggedEvent.shallBeRequeued())
          {
            trigged.removeElement(triggedEvent);
          }
        }

        double currentTime = com.getCurrentSimTime();
        long timeToDoIt = timeAtStart + ((long) (currentTime*timeUnit));
        long delay = timeToDoIt - System.currentTimeMillis();
        if(delay < -100)
        {
           //System.out.println("illustrationAlgorithm - Delay = "+delay);
           repaint = false;
        }
        long timeToWait = Math.max(0, delay);
        try
        {
          if(timeToWait > 3000)
          {
            ProgressDumpAndMonitor.addText(
              "Simplesim", "Long wait! Waits until "+
              new Date(timeToDoIt)+"\n"+
              "That is "+Math.round(timeToWait/1000)+" seconds.");
          }
          Thread.sleep(timeToWait);
        }
        catch(InterruptedException ie)
        {
          throw new InternalProgrammingError("The wait for illustration algorithm was\n"+
                                  "interrupted.", ie);
        }

        if(repaint)
        {
          IntolerableException _ie = ill.getException();
          if(_ie != null) throw _ie;
          //System.out.println("illustrationAlgorithm - executeSimulation 3");
          ill.repaint();
          if(eventtable)
          {
            et.invalidate();
            et.validate();
            et.repaint();
          }
          //System.out.println("illustrationAlgorithm - executeSimulation 4");
          _ie = ill.getException();
          if(_ie != null) throw _ie;
        }
      }
      com.setTextualStopReason("Queue ran empty");
    }
    catch(StopExceptionUserProgrammatic e)
    {
      com.setTextualStopReason(e.getMessage());
      stopHistoryNotTroublesome = false;
    }
    catch(StopException e)
    {
      com.setTextualStopReason(e.getMessage());
    }
    catch(IntolerableException e)
    {
      com.setTextualStopReason(e.getMessage());
      throw e;
    }
    catch(NullPointerException npe)
    {
      if(eq == null)
      {
        // Progress bar stop
        //done = true;
        throw new UserBreak();
      }
      //done = true;
      throw new UnclassedError(
          "(illustrationAlgorithm - executeSimulation - finally - NullPointerException)", npe);
    }
    catch(Exception e)
    {
      com.setTextualStopReason("Internal error");
      throw new UnclassedError(
          "Internal error: (algorithm - executeSimulation - kernel)", e);
    }

    if(!stopHistoryNotTroublesome) return;

    try
    {
      FinallyEvent fe = sd.getFinally();
      if(fe != null) 
      {
        addEvent(fe);
        fe.runEvent();
      }
    }
    catch(StopExceptionUserProgrammatic e)
    {
      com.setTextualStopReason(e.getMessage());
    }
    catch(StopException e)
    {
      com.setTextualStopReason(e.getMessage());
    }
    catch(IntolerableException e)
    {
      com.setTextualStopReason(e.getMessage());
      throw e;
    }
    catch(NullPointerException npe)
    {
      if(eq == null)
      {
        // Progress bar stop
        //done = true;
        throw new UserBreak();
      }
      //done = true;
      throw new UnclassedError(
          "(illustrationAlgorithm - executeSimulation - finally - NullPointerException)", npe);
    }
    catch(Exception e)
    {
      com.setTextualStopReason("Internal error");
      throw new UnclassedError(
          "Internal error: (algorithm - executeSimulation - finally)", e);
    }

  }

  public void runAlgorithm()
    throws IntolerableException
  {
    //System.out.println("debug - runAlgorithm");
    if(eventtable)
    {
      et = EventTable.flash(new EventTable(registeredEvents));
    }
    FirstLevelTreeNodeBuilderVisitor fltnbv = null;
    SegmentPainter sp = null;
    try
    {
      setupScenario();
      ill.init();
      ill.setGlobalScope(sd.getGlobalScope());
    //System.out.println("debug - runAlgorithm - 2");
      runSimulation();
    //System.out.println("debug - runAlgorithm - 3");
     
      //System.out.println("PP "+sd.getNoOfMonitoredVariables());

      sp = new SegmentPainterOnce();
      fltnbv = new FirstLevelTreeNodeBuilderVisitor(sp, ExternalPainterFactoryHandle.get());
      SimScope s = (SimScope) sd.getGlobalScope();
      VariableInstance vis[] = s.getAllInstances();
      for(int i = 0 ; i < vis.length ; i++)
      {
        //System.out.println("onceAlgo - "+vis[i]);
        vis[i].accept(fltnbv);
      } 

      Variables vars = (Variables) fltnbv.getResult();
      node = new TopNode(sd, vars, sp);
      extracts = fltnbv.getExtracts();
      Runtime.getRuntime().runFinalization();
      setDone();
      //System.out.println("Slut - once - runAlgorithm");
    }
    catch(IntolerableException ie)
    {
    //System.out.println("debug - runAlgorithm - 4 "+ie);
      sp = new SegmentPainterOnce();
      fltnbv = new FirstLevelTreeNodeBuilderVisitor(sp, ExternalPainterFactoryHandle.get());
      SimScope s = (SimScope) sd.getGlobalScope();
      VariableInstance vis[] = s.getAllInstances();
      for(int i = 0 ; i < vis.length ; i++)
      {
        //System.out.println("onceAlgo - "+vis[i]);
        vis[i].accept(fltnbv);
      } 

      Variables vars = (Variables) fltnbv.getResult();
      node = new TopNode(sd, vars, sp);
      extracts = fltnbv.getExtracts();
      Runtime.getRuntime().runFinalization();
      displayResultsGraphically(true);
      setDone();
      throw ie;
    }
    catch(Exception e)
    {
    //System.out.println("debug - runAlgorithm - 5 "+e);
      //setDone();
      throw new UnclassedError("debug ...", e);
    }
    //displayResultsGraphically();
  }

  public void displayResultsGraphically(boolean visible)
  {
    //System.out.println("debug - displayResultsGraphically");
    super.displayResultsGraphically(visible);
  }
 
}