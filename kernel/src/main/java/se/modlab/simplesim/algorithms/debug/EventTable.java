package se.modlab.simplesim.algorithms.debug;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.util.*;

public class EventTable extends AbstractTableModel 
{

  private Vector events;

  public EventTable(Vector events)
  {
    this.events = events;
  }

  public int getColumnCount() 
  {
    return 5;
  }

  public int getRowCount() 
  {
    return events.size();
  }

  public String getColumnName(int col) 
  {
    if(col == 0) return "Execution nr";
    if(col == 1) return "Event type";
    if(col == 2) return "Event name";
    if(col == 3) return "Simtime as double";
    if(col == 4) return "Calendar time";
    return "Result table - getColumnName("+col+") internal error";
  }

  public Object getValueAt(int row, int col) 
  {
    EventRow er = (EventRow) events.elementAt(row);
    return er.getValueAt(col);
  }

  public Class getColumnClass(int c) 
  {
    return String.class;
  }

  public boolean isCellEditable(int row, int col) 
  {
    return false;
  }

  public static final int SCREEN_WIDTH = (int) Toolkit.getDefaultToolkit().getScreenSize().width;
  public static final int SCREEN_HEIGHT = (int) Toolkit.getDefaultToolkit().getScreenSize().height-70;

  public static JFrame flash(EventTable rt) 
  {
    JTable table = new JTable(rt);
    //final eventTable _rt  = rt;
    JScrollPane scrollPane = new JScrollPane(table);
    //table.addMouseListener(new LocalTableListener());
    JFrame frame = new JFrame("Event table");
    frame.setLocation(new Point((int)(SCREEN_WIDTH/2), 0));
    frame.setSize(new Dimension((int)(SCREEN_WIDTH/2), SCREEN_HEIGHT));
    frame.getContentPane().add(scrollPane, BorderLayout.CENTER);
    //frame.pack();
    frame.setVisible(true);
    return frame;
  }
/*
  public static class LocalTableListener extends MouseAdapter 
  {
    
    public void mouseReleased(MouseEvent e) 
    {
      Point p = e.getPoint();
      JTable table = (JTable) e.getComponent();
      int column = table.columnAtPoint(p);
      int row = table.rowAtPoint(p);
      TableModel tm = table.getModel();
      resultTable rt = (resultTable) tm;
      rt.runAlgo(row);
    }

  }
 */

}
