package se.modlab.simplesim.algorithms.illustrate.share;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;

public interface ScopeGetter
{

  public Scope getScope(Scope globalScope)
    throws IntolerableException;

}
