package se.modlab.simplesim.algorithms.setup;

import java.io.InputStream;
import java.util.Hashtable;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.simplesim.variables.*;
import se.modlab.simplesim.references.*;
import se.modlab.simplesim.algorithms.initiators.*;
import se.modlab.simplesim.algorithms.samplers.*;
import se.modlab.simplesim.scoping.*;
import se.modlab.simplesim.events.*;

public class QueuableReferenceManager implements Manager
{
  
  private String type = null;
  private String name = null;
  private String filename;
  private int line;
  private int column;
  private SimData sd = null;
  private ReferenceEvaluable re = null;
  private boolean _con;
  private boolean _exp;
  private boolean _pub;

  public QueuableReferenceManager(String _type,
                                  String _name,
                                  String _filename,
                                  int _line,
                                  int _column,
                                  SimData _sd,
                                  ReferenceEvaluable _re,
                                  boolean _const,
                                  boolean _export,
                                  boolean _public)
  {
    type = _type;
    name = _name;
    filename = _filename;
    line = _line;
    column = _column;
    sd = _sd;
    re = _re;
    _con = _const;
    _exp = _export;
    _pub = _public;
  }

  public String getFilename()
  {
    return filename;
  }  

  public int getLine()
  {
    return line;
  }  

  public int getColumn()
  {
    return line;
  }  

  private void _initialize(Scope s, boolean isSharp, Hashtable<VariableInstance, Manager> managermap)
    throws IntolerableException
  {
    VariableFactory vf = null;
    QueuableReference var = null;
    if(type == null)
    {
      vf = s.getFactory("queuable");
      if(vf == null)
      {
        throw new InternalError(
          "No factory for the universal queuable."); 
      }
      if(!_con)
      {
        var = new UntypedQueuableReference(name, vf, filename, line, column);
      }
      else
      {
        var = new UntypedQueuableReferenceConst(name, vf, filename, line, column);
      }
    }
    else
    {
      vf = s.getFactory(type);
      if(vf == null)
      {
        throw new InternalError(
          "No factory for type "+type+" found."); 
      }
      if(!_con)
      {
        var = new TypedQueuableReference(name, vf, filename, line, column);
      }
      else
      {
        var = new TypedQueuableReferenceConst(name, vf, filename, line, column);
      }
    }
    if(isSharp)
    {
      sd.addInitiator(new QueuableReferenceInitiator(var, s, re));
    }
    sValue val = null;
    if(re != null)
    {
      //variableInstance vi = re.evaluate(s);
      sValue sv = re.evaluate(s);
      if(sv == null)
      {
        val = new sQueuable();
      } 
      else
      {
        if(!(sv instanceof sQueuable))
        {
          throw new UserCompiletimeError(
            "The variable "+name+
            " is a queuable or reference to a\n"+
            "queuable but assigned the value "+sv);
        }
        Enqueuable eq = ((sQueuable) sv).getQueuable();
        if(eq == null)
        {
          val = new sQueuable();
        }
        if(eq instanceof QueuableReference)
        {
          val = ((QueuableReference) eq).getValue();
        }
        else
        { 
          if(eq instanceof Enqueuable)
          {
            val = new sQueuable((Enqueuable) eq);
          }
          if(val == null)
          {
            throw new InternalError(
              "eq is of type "+eq.getClass());
          }
        }
      }
    }
    else
    {
      val = new sQueuable();
    }
    var.setInitialValue(val);
    s.addVariable(var);
    if(_exp)
    {
      Scope gs = sd.getGlobalScope();
      if(gs == s)
      {
        throw new UserCompiletimeError(
          "You may not export variables from global scope.\n"+
          "This happens with the variable "+name+".");
      }
      SimScope ss = (SimScope) s;
      ss.addMember(new QueuableReferenceExported((QueuableReference) var));
      return;
    }
    if(_pub)
    {
      Scope gs = sd.getGlobalScope();
      if(gs == s)
      {
        throw new UserCompiletimeError(
          "You may not declare variables public in global scope.\n"+
          "This happens with the variable "+name+".");
      }
      SimScope ss = (SimScope) s;
      ss.addMember(var);
      return;
    }
	if(managermap != null) {
		managermap.put(var, this);
	}

  } 

  public void initialize(Scope s, boolean isSharp, Hashtable<VariableInstance, Manager> managermap)
  throws IntolerableException
  {
    try {
      _initialize(s, isSharp, managermap);
    }
	catch(UserRuntimeError ue) {
	      String m = ue.getMessage()+"\n"+"This happened in file "+filename+", line "+line+", column "+column;
	      UserRuntimeError ue2 = new UserRuntimeError(m, ue.getCause(), ue.getCollectors());
		  ue2.setAction(ue.getAction());
		  throw ue2;
	    }
	catch(UserCompiletimeError ue) {
	      String m = ue.getMessage()+"\n"+"This happened in file "+filename+", line "+line+", column "+column;
	      UserCompiletimeError ue2 = new UserCompiletimeError(m, ue.getCause(), ue.getCollectors());
		  ue2.setAction(ue.getAction());
		  throw ue2;
	    }
  }

  public String toString()
  {
    return "referenceManager for "+name+
           " in file "+filename+" on line "+line+" on column "+column+
           " of type "+type+
           " with const = "+_con+
           ", export = "+_exp+
           ", public = "+_pub;
  }
  
	public void reinitializeLogical(LogicalExpression le) throws IntolerableException {
		throw new UserRuntimeError(
				"Cannot set a queuable reference to the value of a boolean expression.\n"+
				"This happens to the reference "+name+" declared on line "+line+", column "+column+" in the file "+filename);
    }

	public void reinitializeArithmetic(ArithmeticEvaluable ae) throws IntolerableException {
		throw new UserRuntimeError(
				"Cannot set a queuable reference to the value of an arithmetic expression.\n"+
				"This happens to the reference "+name+" declared on line "+line+", column "+column+" in the file "+filename);
    }

	public void reinitializeFromInputStream(InputStream is) throws IntolerableException {
		throw new InternalError(
				"Cannot set a queuable reference to the value of an input stream.\n"+
				"This happens to the reference "+name+" declared on line "+line+", column "+column+" in the file "+filename);
    }

}