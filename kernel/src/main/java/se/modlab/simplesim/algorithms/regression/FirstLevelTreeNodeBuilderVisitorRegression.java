package se.modlab.simplesim.algorithms.regression;

import se.modlab.simplesim.variables.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.gui.util.SimpleExplorerNode;

import java.util.*;

import se.modlab.generics.*;
import se.modlab.simplesim.algorithms.share.*;
import se.modlab.simplesim.algorithms.externalpainters.ExternalPainterFactoryHandle;
import se.modlab.simplesim.algorithms.externalpainters.PlugInPainterFactory;
import se.modlab.simplesim.algorithms.presentation.*;
import se.modlab.simplesim.algorithms.setup.*;
import se.modlab.simplesim.algorithms.samplers.*;
import se.modlab.simplesim.events.*;
import se.modlab.simplesim.scoping.*;
import se.modlab.simplesim.references.*;
import se.modlab.simplesim.bags.*;

public class FirstLevelTreeNodeBuilderVisitorRegression
               extends InstanceVisitorAdapter
{

  private Stack s = null;//new Stack();
  private Stack names = null;//new Stack();
  private long segments;
  private double startTimeRegression;
  private double endTimeRegression;
  private SegmentPainter sp;
  private SimData sd;
  private Vector extracts = new Vector();
  private PlugInPainterFactory pips[];

  public FirstLevelTreeNodeBuilderVisitorRegression(
    long _segments,
    double _startTimeRegression,
    double _endTimeRegression,
    SimData _sd,
    SegmentPainter _sp,
    PlugInPainterFactory _pips[])
  {
    segments = _segments;
    startTimeRegression = _startTimeRegression;
    endTimeRegression = _endTimeRegression;
    sd = _sd;
    sp = _sp;
    pips = _pips;
    s = new Stack();
    names = new Stack();
    s.push(new Variables(sp));
  }

  public String getFullName()
  {
    StringBuffer sb = new StringBuffer();
    for(int i = 0 ; i < names.size() ; i++)
    {
      sb.append(names.elementAt(i).toString());
    }
    return sb.toString();
  }
 
  public SimpleExplorerNode getResult()
    throws IntolerableException 
  {
    Variables sen = (Variables) s.pop();
    if(s.size() != 0)
    {
      throw new InternalError(
        "Stack not empty at call to getResult in class\n"+
        "firstLevelTreeNodeBuilderVisitorRegression");
    }
    return sen;
  }

  public MonitoredVariableExtracted[] getExtracts()
  {
    MonitoredVariableExtracted out[] = 
      new MonitoredVariableExtracted[extracts.size()];
    for(int i = 0 ; i < extracts.size() ; i++)
    {
      out[i] = (MonitoredVariableExtracted) extracts.elementAt(i);
    }
    return out;
  }

  public void visit(BooleanVariable inst) 
    throws IntolerableException 
  {
    EndValueSampler evs = sd.getSampler(inst);
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneBooleanVariable(getFullName()+inst.getName(), evs, itn, sp));
  }
 
  public void visit(BooleanConst inst) 
    throws IntolerableException 
  {
    EndValueSampler evs = sd.getSampler(inst);
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneBooleanVariable(getFullName()+inst.getName(), evs, itn, sp));
  }
 
  public void visit(SimBooleanExported inst) 
    throws IntolerableException 
  {
    EndValueSampler evs = sd.getSampler(inst);
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneBooleanVariable(getFullName()+inst.getName(), evs, itn, sp));
/*
    throw new internalError(
      "Builds monitoration tree and falls on\n"+
      "export node. SimBooleanExported"+
      "Happened in class firstLevelTreeNodeBuilderVisitorRegression");
 */
  }
 
  public void visit(SimBooleanMonitoredVariable inst) 
    throws IntolerableException 
  {
    StatisticsCollector sc = 
      new StatisticsCollector(inst, 
                              segments, 
                              startTimeRegression,
                              endTimeRegression, 
                              sd.getCommons());
    sc.setName(getFullName()+sc.getName());
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneMonitoredVariable(sc, itn, sp, ExternalPainterFactoryHandle.get()));
    extracts.addElement(sc);
  }
 
  public void visit(DoubleVariable inst) 
    throws IntolerableException 
  {
    EndValueSampler evs = sd.getSampler(inst);
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneArithmeticVariable(getFullName()+inst.getName(), evs, itn, sp));
  }
 
  public void visit(DoubleConst inst) 
    throws IntolerableException 
  {
    EndValueSampler evs = sd.getSampler(inst);
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneArithmeticVariable(getFullName()+inst.getName(), evs, itn, sp));
  }
 
  public void visit(SimDoubleExported inst) 
    throws IntolerableException 
  {
    EndValueSampler evs = sd.getSampler(inst);
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneArithmeticVariable(getFullName()+inst.getName(), evs, itn, sp));
/*
    throw new internalError(
      "Builds monitoration tree and falls on\n"+
      "export node. SimDoubleExported"+
      "Happened in class firstLevelTreeNodeBuilderVisitorRegression");
 */
  }
 
  public void visit(SimDoubleMonitoredVariable inst) 
    throws IntolerableException 
  {
    StatisticsCollector sc = 
      new StatisticsCollector(inst, 
                              segments, 
                              startTimeRegression,
                              endTimeRegression, 
                              sd.getCommons());
    String fullName = getFullName()+inst.getName();
    sc.setName(fullName);
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    //itn.addChild(new OneMonitoredVariable(sc, itn, sp, ExternalPainterFactoryHandle.get()));
    
    IntermediateTreeNode itn_variable = new IntermediateTreeNode(itn, fullName+" (monitored)");
    itn.addChild(itn_variable);
    VariableNode vn = new OneMonitoredVariable(sc, itn, sp, pips);
    itn_variable.addChild(vn);
    if(pips == null) return;
    for(int i = 0 ; i < pips.length ; i++) {
    	//System.out.println("firstLevelTreeNodeBuilderVisitor "+i);
    	itn_variable.addChild(new OneExternallyPaintedVariable(fullName, vn, pips[i].getPainter(sc, true)));
    }
    extracts.addElement(sc);   
  }
 
  public void visit(LongVariable inst) 
    throws IntolerableException 
  {
    EndValueSampler evs = sd.getSampler(inst);
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneArithmeticVariable(getFullName()+inst.getName(), evs, itn, sp));
  }
 
  public void visit(LongConst inst) 
    throws IntolerableException 
  {
    EndValueSampler evs = sd.getSampler(inst);
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneArithmeticVariable(getFullName()+inst.getName(), evs, itn, sp));
  }
 
  public void visit(SimLongExported inst) 
    throws IntolerableException 
  {
    EndValueSampler evs = sd.getSampler(inst);
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneArithmeticVariable(getFullName()+inst.getName(), evs, itn, sp));
/*    throw new internalError(
      "Builds monitoration tree and falls on\n"+
      "export node. SimLongExported"+
      "Happened in class firstLevelTreeNodeBuilderVisitorRegression");
 */
  }
 
  public void visit(SimLongMonitoredVariable inst) 
    throws IntolerableException {
	    StatisticsCollector sc = 
	        new StatisticsCollector(inst, 
	                                segments, 
	                                startTimeRegression,
	                                endTimeRegression, 
	                                sd.getCommons());
	    String fullName = getFullName()+inst.getName();
	    sc.setName(fullName);
	    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
	    //itn.addChild(new OneMonitoredVariable(sc, itn, sp, ExternalPainterFactoryHandle.get()));
	      
	    IntermediateTreeNode itn_variable = new IntermediateTreeNode(itn, fullName+" (monitored)");
	    itn.addChild(itn_variable);
	    VariableNode vn = new OneMonitoredVariable(sc, itn, sp, pips);
	    itn_variable.addChild(vn);
	    if(pips == null) return;
	    for(int i = 0 ; i < pips.length ; i++) {
	    	//System.out.println("firstLevelTreeNodeBuilderVisitor "+i);
	    	itn_variable.addChild(new OneExternallyPaintedVariable(fullName, vn, pips[i].getPainter(sc, true)));
	    }
	    extracts.addElement(sc);   
  }
 
  public void visit(TypedQueuableReference inst) 
    throws IntolerableException 
  {
    EndValueSampler evs = sd.getSampler(inst);
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneQueuableReference(getFullName()+inst.getName(), evs, itn, sp));
  }
 
  public void visit(TypedQueuableReferenceConst inst) 
    throws IntolerableException 
  {
    EndValueSampler evs = sd.getSampler(inst);
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneQueuableReference(getFullName()+inst.getName(), evs, itn, sp));
  }
 
  public void visit(UntypedQueuableReference inst) 
    throws IntolerableException 
  {
    EndValueSampler evs = sd.getSampler(inst);
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneQueuableReference(getFullName()+inst.getName(), evs, itn, sp));
  }
 
  public void visit(UntypedQueuableReferenceConst inst) 
    throws IntolerableException 
  {
    EndValueSampler evs = sd.getSampler(inst);
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneQueuableReference(getFullName()+inst.getName(), evs, itn, sp));
  }
 
  public void visit(QueuableReferenceExported inst) 
    throws IntolerableException 
  {
    EndValueSampler evs = sd.getSampler(inst);
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneQueuableReference(getFullName()+inst.getName(), evs, itn, sp));
  }
 
  public void visit(TypedStructReference inst) 
    throws IntolerableException 
  {
    EndValueSampler evs = sd.getSampler(inst);
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneStructReference(getFullName()+inst.getName(), evs, itn, sp));
  }
 
  public void visit(TypedStructReferenceConst inst) 
    throws IntolerableException 
  {
    EndValueSampler evs = sd.getSampler(inst);
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneStructReference(getFullName()+inst.getName(), evs, itn, sp));
  }
 
  public void visit(UntypedStructReference inst) 
    throws IntolerableException 
  {
    EndValueSampler evs = sd.getSampler(inst);
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneStructReference(getFullName()+inst.getName(), evs, itn, sp));
  }
 
  public void visit(UntypedStructReferenceConst inst) 
    throws IntolerableException 
  {
    EndValueSampler evs = sd.getSampler(inst);
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneStructReference(getFullName()+inst.getName(), evs, itn, sp));
  }
 
  public void visit(StructReferenceExported inst) 
    throws IntolerableException 
  {
    EndValueSampler evs = sd.getSampler(inst);
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneStructReference(getFullName()+inst.getName(), evs, itn, sp));
  }
 
  public void visit(ProcedureTrigged inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode parent = (IntermediateTreeNode) s.peek(); 
    IntermediateTreeNode itn = new IntermediateTreeNode(parent, getFullName()+inst.getName());
    parent.addChild(itn);
    s.push(itn);
    VariableInstance vis[] = inst.getAllInstances();
    names.push(inst.getName()+".");
    for(int i = 0 ; i < vis.length ; i++)
    {
      if(vis[i] != inst) vis[i].accept(this);
    }
    names.pop();
    s.pop();
  }
 
  public void visit(ProcedureSingle inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode parent = (IntermediateTreeNode) s.peek(); 
    IntermediateTreeNode itn = new IntermediateTreeNode(parent, getFullName()+inst.getName());
    parent.addChild(itn);
    s.push(itn);
    VariableInstance vis[] = inst.getAllInstances();
    names.push(inst.getName()+".");
    for(int i = 0 ; i < vis.length ; i++)
    {
      if(vis[i] != inst) vis[i].accept(this);
    }
    names.pop();
    s.pop();
  }
 
  public void visit(ProcedureRepeating inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode parent = (IntermediateTreeNode) s.peek(); 
    IntermediateTreeNode itn = new IntermediateTreeNode(parent, getFullName()+inst.getName());
    parent.addChild(itn);
    s.push(itn);
    VariableInstance vis[] = inst.getAllInstances();
    names.push(inst.getName()+".");
    for(int i = 0 ; i < vis.length ; i++)
    {
      if(vis[i] != inst) vis[i].accept(this);
    }
    names.pop();
    s.pop();
  }
 
  public void visit(ProcedureActor inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode parent = (IntermediateTreeNode) s.peek(); 
    IntermediateTreeNode itn = new IntermediateTreeNode(parent, getFullName()+inst.getName());
    parent.addChild(itn);
    s.push(itn);
    VariableInstance vis[] = inst.getAllInstances();
    names.push(inst.getName()+".");
    for(int i = 0 ; i < vis.length ; i++)
    {
      if(vis[i] != inst) vis[i].accept(this);
    }
    names.pop();
    s.pop();
  }

  public void visit(SimComplexVariableExported inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode parent = null;
    parent = (IntermediateTreeNode) s.peek(); 
    IntermediateTreeNode itn = new IntermediateTreeNode(parent, getFullName()+inst.getName());
    parent.addChild(itn);
    s.push(itn);
    names.push(inst.getName()+".");
    traverse(inst);
    s.pop();
    names.pop();
/*
    throw new internalError(
      "Builds monitoration tree and falls on\n"+
      "export node. SimComplexVariableExported"+
      "Happened in class firstLevelTreeNodeBuilderVisitorRegression");
 */
  }

  public void visit(ComplexVariable inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode parent = null;
    parent = (IntermediateTreeNode) s.peek(); 
    IntermediateTreeNode itn = new IntermediateTreeNode(parent, getFullName()+inst.getName());
    parent.addChild(itn);
    s.push(itn);
    names.push(inst.getName()+".");
    traverse(inst);
    s.pop();
    names.pop();
  }

  public void visit(SimVariableVectorExported inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode parent = (IntermediateTreeNode) s.peek(); 
    IntermediateTreeNode itn = new IntermediateTreeNode(parent, getFullName()+inst.getName());
    parent.addChild(itn);
    s.push(itn);
    VariableInstance _inst = inst.getVectorElement(0);
    if(_inst instanceof ProcedureSingle)
    {
      names.push(inst.getName()+".");
      VariableInstance vis[] = ((ProcedureSingle) _inst).getAllInstances();
      for(int i = 0 ; i < vis.length ; i++)
      {
        vis[i].accept(this);
      }
      //_inst.accept(this);
      s.pop();
      names.pop();
      return;
    }
    names.push(inst.getName());
    traverse(inst);
    s.pop();
    names.pop();
/*
    throw new internalError(
      "Builds monitoration tree and falls on\n"+
      "export node. SimVariableVectorExported"+
      "Happened in class firstLevelTreeNodeBuilderVisitorRegression");
 */
  }

  public void visit(VariableVector inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode parent = (IntermediateTreeNode) s.peek(); 
    IntermediateTreeNode itn = new IntermediateTreeNode(parent, getFullName()+inst.getName());
    parent.addChild(itn);
    s.push(itn);
    VariableInstance _inst = inst.getVectorElement(0);
    if(_inst instanceof ProcedureSingle)
    {
      names.push(inst.getName()+".");
      VariableInstance vis[] = ((ProcedureSingle) _inst).getAllInstances();
      for(int i = 0 ; i < vis.length ; i++)
      {
        vis[i].accept(this);
      }
      //_inst.accept(this);
      s.pop();
      names.pop();
      return;
    }
    names.push(inst.getName());
    traverse(inst);
    s.pop();
    names.pop();
  }

  public void visit(SimVariableVector inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode parent = (IntermediateTreeNode) s.peek(); 
    IntermediateTreeNode itn = new IntermediateTreeNode(parent, inst.getName());
    parent.addChild(itn);
    s.push(itn);
    VariableInstance _inst = inst.getVectorElement(0);
    if(_inst instanceof ProcedureSingle)
    {
      names.push(inst.getName()+".");
      VariableInstance vis[] = ((ProcedureSingle) _inst).getAllInstances();
      for(int i = 0 ; i < vis.length ; i++)
      {
        if(vis[i] != _inst) vis[i].accept(this);
      }
      //_inst.accept(this);
      s.pop();
      names.pop();
      return;
    }
    names.push(inst.getName());
    traverse(inst);
    s.pop();
    names.pop();
  }

  public void visit(VariableVectorFromFile inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode parent = (IntermediateTreeNode) s.peek(); 
    IntermediateTreeNode itn = new IntermediateTreeNode(parent, inst.getName());
    parent.addChild(itn);
    s.push(itn);
    names.push(inst.getName());
    traverse(inst);
    s.pop();
    names.pop();
  }

  public void visit(SimMonitoredVectorVariableFromFile inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode parent = (IntermediateTreeNode) s.peek(); 
    IntermediateTreeNode itn = new IntermediateTreeNode(parent, inst.getName());
    parent.addChild(itn);
    s.push(itn);
    names.push(inst.getName());
    traverse(inst);
    s.pop();
    names.pop();
  }

  public void visit(ScopeReference inst) 
    throws IntolerableException 
  {
  }

  public void visit(ScopeReferenceExported inst) 
    throws IntolerableException 
  {
  }

  public void visit(SubscopeReference inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode parent = (IntermediateTreeNode) s.peek(); 
    IntermediateTreeNode itn = new IntermediateTreeNode(parent, inst.getName());
    parent.addChild(itn);
    s.push(itn);
    names.push(inst.getName()+".");
    traverse((Complex)inst);
    s.pop();
    names.pop();
  }

  public void visit(SubscopeReferenceExported inst) 
    throws IntolerableException 
  {
    IntermediateTreeNode parent = (IntermediateTreeNode) s.peek(); 
    IntermediateTreeNode itn = new IntermediateTreeNode(parent, inst.getName());
    parent.addChild(itn);
    s.push(itn);
    names.push(inst.getName()+".");
    traverse((Complex)inst);
    s.pop();
    names.pop();
  }

  public void visit(LongLifoBag inst) 
    throws IntolerableException 
  {
    EndValueSampler evs = sd.getSampler(inst);
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneBagVariable(getFullName()+inst.getName(), evs, itn, sp));
  }

  public void visit(LongLiloBag inst) 
    throws IntolerableException 
  {
    EndValueSampler evs = sd.getSampler(inst);
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneBagVariable(getFullName()+inst.getName(), evs, itn, sp));
  }

  public void visit(LongRandomBag inst) 
    throws IntolerableException 
  {
    EndValueSampler evs = sd.getSampler(inst);
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneBagVariable(getFullName()+inst.getName(), evs, itn, sp));
  }

  public void visit(LongSortBag inst) 
    throws IntolerableException 
  {
    EndValueSampler evs = sd.getSampler(inst);
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneBagVariable(getFullName()+inst.getName(), evs, itn, sp));
  }


  public void visit(DoubleLifoBag inst) 
    throws IntolerableException 
  {
    EndValueSampler evs = sd.getSampler(inst);
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneBagVariable(getFullName()+inst.getName(), evs, itn, sp));
  }

  public void visit(DoubleLiloBag inst) 
    throws IntolerableException 
  {
    EndValueSampler evs = sd.getSampler(inst);
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneBagVariable(getFullName()+inst.getName(), evs, itn, sp));
  }

  public void visit(DoubleRandomBag inst) 
    throws IntolerableException 
  {
    EndValueSampler evs = sd.getSampler(inst);
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneBagVariable(getFullName()+inst.getName(), evs, itn, sp));
  }

  public void visit(DoubleSortBag inst) 
    throws IntolerableException 
  {
    EndValueSampler evs = sd.getSampler(inst);
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneBagVariable(getFullName()+inst.getName(), evs, itn, sp));
  }


  public void visit(BooleanLifoBag inst) 
    throws IntolerableException 
  {
    EndValueSampler evs = sd.getSampler(inst);
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneBagVariable(getFullName()+inst.getName(), evs, itn, sp));
  }

  public void visit(BooleanLiloBag inst) 
    throws IntolerableException 
  {
    EndValueSampler evs = sd.getSampler(inst);
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneBagVariable(getFullName()+inst.getName(), evs, itn, sp));
  }

  public void visit(BooleanRandomBag inst) 
    throws IntolerableException 
  {
    EndValueSampler evs = sd.getSampler(inst);
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneBagVariable(getFullName()+inst.getName(), evs, itn, sp));
  }


  public void visit(ComplexLifoBag inst) 
    throws IntolerableException 
  {
    EndValueSampler evs = sd.getSampler(inst);
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneBagVariable(getFullName()+inst.getName(), evs, itn, sp));
  }

  public void visit(ComplexLiloBag inst) 
    throws IntolerableException 
  {
    EndValueSampler evs = sd.getSampler(inst);
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneBagVariable(getFullName()+inst.getName(), evs, itn, sp));
  }

  public void visit(ComplexRandomBag inst) 
    throws IntolerableException 
  {
    EndValueSampler evs = sd.getSampler(inst);
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneBagVariable(getFullName()+inst.getName(), evs, itn, sp));
  }

  public void visit(ComplexSortBag inst) 
    throws IntolerableException 
  {
    EndValueSampler evs = sd.getSampler(inst);
    IntermediateTreeNode itn = (IntermediateTreeNode) s.peek();
    itn.addChild(new OneBagVariable(getFullName()+inst.getName(), evs, itn, sp));
  }

}
