package se.modlab.simplesim.events;

import java.util.Hashtable;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.evaluables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.simplesim.scoping.*;

public class ProcedureTrigged extends VariableInstance implements SimulationEvent, Enqueuable {

	SimScope localScope;
	private LogicalExpression le_execute;
	private LogicalExpression le_requeue;
	private ProgramBlock p;
	private ScopeFactory sf;
	private boolean enqueued = false;
	private String fullPathName;

	private boolean requeue = true;

	public ProcedureTrigged(String _name,
			String filename, int line, int column, 
			SimScope _localScope, 
			LogicalExpression _le_execute,
			LogicalExpression _le_requeue,
			ProgramBlock _p,
			ScopeFactory _sf)
					throws IntolerableException
					{
		this(_name, new procedureTriggedType(_name, filename, line, column), filename, line, column, _localScope,
				_le_execute, _le_requeue, _p, _sf);
					}

	public ProcedureTrigged(String _name,
			SimVariableFactory _vf,
			String filename, int line, int column, 
			SimScope _localScope, 
			LogicalExpression _le_execute,
			LogicalExpression _le_requeue,
			ProgramBlock _p,
			ScopeFactory _sf)
					throws IntolerableException
					{
		super(_name, _vf, filename, line, column);
		localScope = _localScope;
		le_execute = _le_execute;
		le_requeue = _le_requeue;
		p = _p;
		sf = _sf;
		new ScopeReference(localScope, "locals", filename, line, column);
		new ScopeReference(localScope, vt.getTypesName(), filename, line, column);
		//new scopeReference(localScope, "this");
		localScope.addComplexInstance(this, "this");
					}

	public Scope getLocalScope()
	{
		return localScope;
	}

	public boolean shallRun() 
			throws StopException, IntolerableException
			{
		boolean out = le_execute.evaluate(localScope);
		//System.out.println("ProcedureTrigged.shallRun "+le_execute.reproduceExpression()+" -> "+out);
		return out;
			}

	public void runEvent() 
			throws StopException, IntolerableException
			{
		Scope s = sf.getInstance(localScope);
		try
		{
			p.execute(s);
		}
		catch(ReturnException e)
		{
		}
		catch(BreakException e)
		{
			throw new InternalError(
					"Break exception happened in runtime.");
		}
		catch(ContinueException e)
		{
			throw new InternalError(
					"Continue exception happened in runtime.");
		}
		requeue = le_requeue.evaluate(localScope);
			}

	public boolean shallBeRequeued()
	{
		enqueued = requeue;
		return requeue;
	}

	public double getSimTimeForNextEvent()
			throws IntolerableException
			{ 
		throw new InternalError(
				"procedureTrigged.getSimTimeForNextEvent called for trigged event");
			}

	public String toString()
	{
		return "Trigged event "+name+", defined "+this.getPlace();
	}

	public void reset()
			throws IntolerableException
			{
		enqueued = false;
		requeue = true; // Correct?
			}

	public String getEventTypeName()
	{
		return "trigged";
	}

	public String getEventName()
	{
		return getFullPathName();
	}

	public void queueUp(SimulationEventQueue eq)
			throws IntolerableException
			{
		if(enqueued)
		{
			throw new UserRuntimeError(
					"Trigged "+name+", defined "+this.getPlace()+" was enqueued when already in\n"+
					"the event queue.");                   
		}
		requeue = true;
		eq.addTrigged(this);
		enqueued = true;
			}

	public void queueDown(SimulationEventQueue eq)
			throws IntolerableException
			{
		if(!enqueued)
		{
			throw new UserRuntimeError(
					"Trigged "+name+", defined "+this.getPlace()+" was dequeued when not in\n"+
					"the event queue.");                   
		}
		eq.removeTrigged(this);
		enqueued = false;
			}

	public boolean isEnqueued()
	{
		return enqueued;
	}

	public void setDefaultInitialValue()
			throws IntolerableException
			{
			}

	public void setInitialValue(VariableInstance _val) 
			throws IntolerableException
			{
		throw new InternalError(
				"procedureTrigged.setInitialValue(variableInstance)\n"+
						"called with object of class "+_val.getClass().getName()+".\n"+
				"This method should not be called at all!");
			}

	public void setValue(VariableInstance si)
			throws IntolerableException
			{
		throw new InternalError(
				"procedureTrigged.setValue was called\n"+
						"on trigged procedure "+name+", defined "+this.getPlace());
			}

	public void setFullPathName(String fpn)
			throws IntolerableException
			{
		fullPathName = fpn;
			}

	public String getFullPathName()
	{
		return fullPathName;
	}

	public void addMember(VariableInstance si)
			throws IntolerableException
			{
		localScope.addMember(si);
			}

	public int getSize()
	{
		return localScope.getSize();
	}

	public VariableInstance getMember(int i)
			throws IntolerableException
			{
		return localScope.getMember(i);
			}

	public VariableInstance getMember(String name)
			throws IntolerableException
			{
		return localScope.getMember(name);
			}

	public VariableInstance[] getAllInstances()
	{
		return localScope.getAllInstances();
	}

	private static class procedureTriggedType implements SimVariableFactory
	{
		private String name;
		private String filename;
		private int line;
		private int column;
		public procedureTriggedType(String _name, String _filename, int _line, int _column)
		{
			name = _name;
			filename = _filename;
			line = _line;
			column = _column;
		}
		public String getTypesName()
		{
			return name;
		}
		public VariableInstance getInstance(String name, String _filename, int _line, int _column)
		{
			return null;
		}
		public VariableInstance getInstance(String name, boolean monitor, String _filename, int _line, int _column)
		{
			return null;
		}
		public String toString()
		{
			return name+", implicitly defined in file "+filename+", line "+line+", column "+column;
		}
		public VariableInstance getInstance(String name, String filename, int line, int column, Hashtable<VariableInstance, Manager> managermap)
				throws IntolerableException
				{
			System.out.println("Call to getInstance(s,s,i,i,ht) i.e. addin null in class "+getClass().getName());
			return getInstance(name, false, filename, line, column, null);
				}
		public VariableInstance getInstance(String name, boolean monitor, String filename, int line, int column, Hashtable<VariableInstance, Manager> managermap)
				throws IntolerableException
				{
			System.out.println("Call to getInstance(s,b,s,i,i,ht) i.e. addin null in class "+getClass().getName());
			return getInstance(name, false, filename, line, column, null);
				}
	}

	public boolean isNull() {
		return false;
	}

	public boolean valueEquals(VariableInstance sv) throws IntolerableException {
		if(!(sv instanceof ProcedureTrigged)) {
			return false;
		}
		ProcedureTrigged other = (ProcedureTrigged) sv;
		if(this == other) {
			return true;
		}
		return p == other.p && localScope == other.localScope;
	}
}
