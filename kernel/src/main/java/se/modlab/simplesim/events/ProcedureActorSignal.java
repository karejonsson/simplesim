package se.modlab.simplesim.events;

/* import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.evaluables.*;
import se.modlab.generics.exceptions.*; */
import se.modlab.generics.sstruct.executables.*;

public class ProcedureActorSignal 
{

  private String name;
  private ProgramBlock onsignal;
  private String nextState;

  public ProcedureActorSignal(String _name,
                              ProgramBlock _onsignal,
                              String _nextState)
  {
    name = _name;
    onsignal = _onsignal;
    nextState = _nextState;
  }

  public String getName()
  { 
    return name;
  }

  public ProgramBlock getOnsignal()
  {
    return onsignal;
  }

  public String getNextState()
  { 
    return nextState;
  }

}

