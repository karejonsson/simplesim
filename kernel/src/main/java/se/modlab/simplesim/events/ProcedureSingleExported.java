package se.modlab.simplesim.events;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import java.util.*;

public class ProcedureSingleExported extends VariableInstance
   implements Enqueuable
{
  private ProcedureSingle inst;
  private static CreateExportedVersionVisitor cevv = null;
 
  static
  {
    try
    {
      cevv = CreateExportedVersionVisitor.getInstance();
    }
    catch(IntolerableException e)
    {
      //System.out.println("Commons laddning misslyckad. nsme");
    } 
  }

  public ProcedureSingleExported(ProcedureSingle _inst)
  {
    super(_inst.getName(), _inst.getType(), _inst.getFilename(), _inst.getLine(), _inst.getColumn());
    inst = _inst;
  }

  public void addMember(VariableInstance si)
    throws IntolerableException
  {
    throw new InternalError(
      "procedureSingleExported.addMember was called on "+name);
  }
 
  public int getSize()
  {
    return inst.getSize();
  }

  public VariableInstance getMember(int i)
    throws IntolerableException
  {
    VariableInstance si = (VariableInstance) inst.getMember(i);
    si.accept(cevv);
    return cevv.getExportedVersion();
  }

  public VariableInstance getMember(String name)
    throws IntolerableException
  {
    for(int i = 0 ; i < inst.getSize() ; i++)
    {
      VariableInstance si = (VariableInstance) inst.getMember(i);
      if(name.compareTo(si.getName()) == 0) 
      {
        si.accept(cevv);
        return cevv.getExportedVersion();
      }
    }
    return null;
  }

  public void setDefaultInitialValue()
    throws IntolerableException
  {
    throw new InternalError(
      "SimComplexVariableExported.setDefaultInitialValue was called on "+name);
  }

  public void setInitialValue(VariableInstance _val) 
    throws IntolerableException
  {
    throw new InternalError(
      "procedureSingleExported.setInitialValue(variableInstance)\n"+
      "called with object of class "+_val.getClass().getName()+".\n"+
      "This method should not be called at all!");
  }

  public void setValue(VariableInstance si)
    throws IntolerableException
  {
    throw new UserRuntimeError(
      "Complex exported "+inst.getType().getTypesName()+" variable "+name+", defined "+this.getPlace()+" set to new value "+si);
  }

  public void setValue(sValue val) 
    throws IntolerableException
  {
    throw new UserRuntimeError(
      "Complex exported "+inst.getType().getTypesName()+" variable "+name+", defined "+this.getPlace()+" set to new value "+val);
  }

  public String toString()
  {
    return name;
  }

  public void queueUp(SimulationEventQueue eq)
    throws IntolerableException, 
           StopException
  {
    throw new UserRuntimeError(
      "Single "+inst.getFullPathName()+", defined "+this.getPlace()+" was enqueued\n"+
      "from a scope where it is exported");
  }

  public void queueDown(SimulationEventQueue eq)
    throws IntolerableException, 
           StopException
  {
    throw new UserRuntimeError(
      "Single "+inst.getFullPathName()+", defined "+this.getPlace()+" was dequeued\n"+
      "from a scope where it is exported");
  }

  public boolean isEnqueued()
    throws IntolerableException
  {
    return inst.isEnqueued();
  }
 
  public void reset()
    throws IntolerableException
  {
    throw new InternalError(
      "procedureSingleExported.reset()\n"+
      "called. This method should not be called at all!");
  }

  public void setFullPathName(String fpn) 
    throws IntolerableException
  {
    throw new InternalError(
      "procedureSingleExported.setFullPathName("+fpn+")\n"+
      "called. This method should not be called at all!");
  }

  public String getFullPathName()
  {
    return inst.getFullPathName();
  }

  public Scope getLocalScope()
  {
    return inst.getLocalScope();
  }

@Override
public boolean isNull() {
	// TODO Auto-generated method stub
	return false;
}

public boolean valueEquals(VariableInstance sv) throws IntolerableException {
	if(!(sv instanceof ProcedureSingleExported)) {
		return false;
	}
	ProcedureSingleExported other = (ProcedureSingleExported) sv;
	return inst.valueEquals(other);
}


}