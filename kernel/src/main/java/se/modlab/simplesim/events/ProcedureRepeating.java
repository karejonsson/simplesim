package se.modlab.simplesim.events;

import java.util.Hashtable;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.evaluables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.simplesim.scoping.*;

public class ProcedureRepeating 
       extends VariableInstance
       implements SimulationEvent, Enqueuable
{

  private SimScope localScope;
  private ArithmeticEvaluable ae_first;
  private ArithmeticEvaluable ae_again;
  LogicalExpression le_requeue;
  private ProgramBlock p;
  private Commons com;
  private ScopeFactory sf;
  private String fullPathName;

  private double simTime;
  private boolean toBeRequeued = true;

  private boolean enqueued = false;

  public ProcedureRepeating(String _name,
		  	 				String filename, int line, int column, 
                            SimScope _localScope,
                            ArithmeticEvaluable _ae_first,
                            ArithmeticEvaluable _ae_again,
                            LogicalExpression _le_requeue,
                            ProgramBlock _p,
                            Commons _com,
                            ScopeFactory _sf)
    throws IntolerableException
  {
    this(_name, new procedureRepeatingType(_name, filename, line, column),
    	 filename, line, column,
         _localScope, _ae_first, _ae_again, _le_requeue,
         _p, _com, _sf);
  }

  public ProcedureRepeating(String _name, 
                            SimVariableFactory _vf,
    		  				String filename, int line, int column, 
                            SimScope _localScope,
                            ArithmeticEvaluable _ae_first,
                            ArithmeticEvaluable _ae_again,
                            LogicalExpression _le_requeue,
                            ProgramBlock _p,
                            Commons _com,
                            ScopeFactory _sf)
    throws IntolerableException
  {
    super(_name, _vf, filename, line, column);
    //System.out.println("Name = "+name+", types name = "+vt.getTypesName());
    localScope = _localScope;
    ae_first = _ae_first;
    ae_again = _ae_again;
    le_requeue = _le_requeue;
    p = _p;
    com = _com;
    sf = _sf;
    reset();
    new ScopeReference(localScope, "locals", filename, line, column);
    new ScopeReference(localScope, vt.getTypesName(), filename, line, column);
    //new scopeReference(localScope, "this");
    //System.out.println("FFFF: >"+ vt.getTypesName()+"<");
    localScope.addComplexInstance(this, "this");
  }

  public Scope getLocalScope()
  {
    return localScope;
  }

  public void runEvent() 
    throws StopException, IntolerableException
  {
    Scope s = sf.getInstance(localScope);
    try
    {
      p.execute(s);
    }
    catch(ReturnException e)
    {
    }
    catch(BreakException e)
    {
      throw new InternalError(
        "Break exception happened in runtime.");
    }
    catch(ContinueException e)
    {
      throw new InternalError(
        "Continue exception happened in runtime.");
    }
    toBeRequeued = le_requeue.evaluate(localScope);
    if(toBeRequeued)
    {
      simTime = com.getCurrentSimTime()+ae_again.evaluate(localScope).getValue();
    }
    //System.out.println("procedureRepeating - requeue "+toBeRequeued+" at "+simTime);
    //if(!toBeRequeued) System.exit(0);
  }

  public boolean shallBeRequeued()
  {
    enqueued = toBeRequeued;
    return toBeRequeued;
  }

  public double getSimTimeForNextEvent()
  {
    return simTime;
  }

  public String toString()
  {
    return "Repeating event "+name;//+" for time "+simTime;
  }

  public void reset()
    throws IntolerableException
  {
    simTime = com.getCurrentSimTime()+ae_first.evaluate(localScope).getValue();
    toBeRequeued = true;
    enqueued = false;
  }

  public String getEventTypeName()
  {
    return "repeating";
  }

  public String getEventName()
  {
    return getFullPathName();
  }

  public void queueUp(SimulationEventQueue eq)
    throws IntolerableException
  {
    if(enqueued)
    {
      throw new UserRuntimeError(
        "Repeating "+name+", defined "+this.getPlace()+" was enqueued when already in\n"+
        "the event queue.");                   
    }
    reset();
    eq.queueUpLate(this);
    enqueued = true;
  }

  public void queueDown(SimulationEventQueue eq)
    throws IntolerableException
  {
    if(!enqueued)
    {
      throw new UserRuntimeError(
        "Repeating "+name+", defined "+this.getPlace()+" was dequeued when not in\n"+
        "the event queue.");                   
    }
    eq.queueDown(this);
    enqueued = false;
  }

  public boolean isEnqueued()
  {
    return enqueued;
  }

  public void setDefaultInitialValue()
    throws IntolerableException
  {
  }

  public void setInitialValue(VariableInstance _val) 
    throws IntolerableException
  {
    throw new InternalError(
      "procedureRepeating.setInitialValue(variableInstance)\n"+
      "called with object of class "+_val.getClass().getName()+".\n"+
      "This method should not be called at all!");
  }

  public void setValue(VariableInstance si)
    throws IntolerableException
  {
    throw new InternalError(
      "procedureRepeating.setValue was called\n"+
      "on repeating procedure "+name+", defined "+this.getPlace());
  }

  public void setFullPathName(String fpn)
    throws IntolerableException
  {
    fullPathName = fpn;
  }

  public String getFullPathName()
  {
    return fullPathName;
  }

  public void addMember(VariableInstance si)
    throws IntolerableException
  {
    localScope.addMember(si);
  }

  public int getSize()
  {
    return localScope.getSize();
  }

  public VariableInstance getMember(int i)
    throws IntolerableException
  {
    return localScope.getMember(i);
  }

  public VariableInstance getMember(String name)
    throws IntolerableException
  {
    return localScope.getMember(name);
  }

  public VariableInstance[] getAllInstances()
  {
    return localScope.getAllInstances();
  }

  private static class procedureRepeatingType implements SimVariableFactory
  {
    private String name;
    private String filename;
    private int line;
    private int column;
    public procedureRepeatingType(String _name, String _filename, int _line, int _column)
    {
      name = _name;
      filename = _filename;
      line = _line;
      column = _column;
    }
    public String getTypesName()
    {
      return name;
    }
    public VariableInstance getInstance(String name, String _filename, int _line, int _column)
    {
      return null;
    }
    public VariableInstance getInstance(String name, boolean monitor, String _filename, int _line, int _column)
    {
      return null;
    }
    public String toString()
    {
      return name+", implicitly defined in file "+filename+", line "+line+", column "+column;
    }
	public VariableInstance getInstance(String name, String filename, int line, int column, Hashtable<VariableInstance, Manager> managermap)
	throws IntolerableException
	{
		System.out.println("Call to getInstance(s,s,i,i,ht) i.e. addin null in class "+getClass().getName());
		return getInstance(name, false, filename, line, column, null);
	}
	public VariableInstance getInstance(String name, boolean monitor, String filename, int line, int column, Hashtable<VariableInstance, Manager> managermap)
	throws IntolerableException
	{
		System.out.println("Call to getInstance(s,b,s,i,i,ht) i.e. addin null in class "+getClass().getName());
		return getInstance(name, false, filename, line, column, null);
	}
  }
  
	public boolean isNull() {
		return false;
	}

	public boolean valueEquals(VariableInstance sv) throws IntolerableException {
		if(!(sv instanceof ProcedureRepeating)) {
			return false;
		}
		ProcedureRepeating other = (ProcedureRepeating) sv;
		if(this == other) {
			return true;
		}
		return p == other.p && localScope == other.localScope;
	}

}
