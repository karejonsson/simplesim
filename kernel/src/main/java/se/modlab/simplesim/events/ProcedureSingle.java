package se.modlab.simplesim.events;

import java.util.Hashtable;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.evaluables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.simplesim.scoping.*;

public class ProcedureSingle 
       extends VariableInstance
       implements SimulationEvent, Enqueuable
{

  private SimScope localScope;
  private ProgramBlock p;
  private double simTime;
  private double simTime_original;
  private ScopeFactory sf;
  private boolean enqueued = false;
  private String fullPathName;

  public ProcedureSingle(String _name, 
			             String filename, int line, int column, 
                         SimScope _localScope, 
                         ProgramBlock _p, 
                         double _simTime,
                         ScopeFactory _sf)
    throws IntolerableException
  {
    this(_name, new procedureSingleType(_name, filename, line, column), filename, line, column, _localScope, _p, _simTime, _sf);
  }

  public ProcedureSingle(String _name,
                         SimVariableFactory _vf, 
 		  				 String filename, int line, int column, 
                         SimScope _localScope, 
                         ProgramBlock _p, 
                         double _simTime,
                         ScopeFactory _sf)
    throws IntolerableException
  {
    super(_name, _vf, filename, line, column);
    fullPathName = _name;
    localScope = _localScope;
    p = _p;
    simTime = _simTime;
    sf = _sf;
    simTime_original = _simTime;
    if(localScope != null)
    {
      if(localScope.getComplexInstance("locals") == null)
      {
        new ScopeReference(localScope, "locals", filename, line, column);
      }
      if(localScope.getComplexInstance(vt.getTypesName()) == null)
      {
        new ScopeReference(localScope, vt.getTypesName(), filename, line, column);
      }
      if(localScope.getComplexInstance("this") == null)
      {
        localScope.addComplexInstance(this, "this");
        //new scopeReference(localScope, "this");
      }
    }
  }

  public Scope getLocalScope()
  {
    return localScope;
  }

  public void runEvent() 
    throws StopException, IntolerableException
  {
    Scope s = sf.getInstance(localScope);
    try
    {
      p.execute(localScope);
    }
    catch(ReturnException e)
    {
    }
    catch(BreakException e)
    {
      throw new InternalError(
        "Break exception happened in runtime.");
    }
    catch(ContinueException e)
    {
      throw new InternalError(
        "Continue exception happened in runtime.");
    }
    simTime = -1;
  }

  public boolean shallBeRequeued()
  {
    enqueued = false;
    return false;
  }

  public double getSimTimeForNextEvent()
    throws IntolerableException
  { 
    return simTime;
  }

  public String toString()
  {
    return "Single event "+name+", defined "+this.getPlace()+"on time "+simTime;
  }

  public void reset()
    throws IntolerableException
  {
    simTime = simTime_original;
    enqueued = false;
  }

  public String getEventTypeName()
  {
    return "single";
  }

  public String getEventName()
  {
    return getFullPathName();
  }

  public void queueUp(SimulationEventQueue eq)
    throws IntolerableException
  {
    if(enqueued)
    {
      throw new UserRuntimeError(
        "Single "+name+", defined "+this.getPlace()+" was enqueued when already in\n"+
        "the event queue.");                   
    }
    eq.queueUpEarly(this);
    enqueued = true;
  }

  public void queueDown(SimulationEventQueue eq)
    throws IntolerableException
  {
    if(!enqueued)
    {
      throw new UserRuntimeError(
        "Single "+name+", defined "+this.getPlace()+" was dequeued when not in\n"+
        "the event queue.");                   
    }
    eq.queueDown(this);
    enqueued = false;
  }

  public boolean isEnqueued()
  {
    return enqueued;
  }

  public void setDefaultInitialValue()
    throws IntolerableException
  {
  }

  public void setInitialValue(VariableInstance _val) 
    throws IntolerableException
  {
    throw new InternalError(
      "procedureSingle.setInitialValue(variableInstance)\n"+
      "called with object of class "+_val.getClass().getName()+".\n"+
      "This method should not be called at all!");
  }

  public void setValue(VariableInstance si)
    throws IntolerableException
  {
    throw new InternalError(
      "procedureSingle.setValue was called\n"+
      "on repeating procedure "+name);
  }

  public void setFullPathName(String fpn)
    throws IntolerableException
  {
    fullPathName = fpn;
  }

  public String getFullPathName()
  {
    return fullPathName;
  }

  public void addMember(VariableInstance si)
    throws IntolerableException
  {
    localScope.addMember(si);
  }

  public int getSize()
  {
    return localScope.getSize();
  }

  public VariableInstance getMember(int i)
    throws IntolerableException
  {
    return localScope.getMember(i);
  }

  public VariableInstance getMember(String name)
    throws IntolerableException
  {
    return localScope.getMember(name);
  }

  public VariableInstance[] getAllInstances()
  {
    return localScope.getAllInstances();
  }

  private static class procedureSingleType implements SimVariableFactory
  {
    private String name;
    private String filename;
    private int line;
    private int column;
    public procedureSingleType(String _name, String _filename, int _line, int _column)
    {
      name = _name;
      filename = _filename;
      line = _line;
      column = _column;
    }
    public String getTypesName()
    {
      return name;
    }
    public VariableInstance getInstance(String name, String _filename, int _line, int _column)
    {
      return null;
    }
    public VariableInstance getInstance(String name, boolean monitor, String _filename, int _line, int _column)
    {
      return null;
    }
    public String toString()
    {
      return name+", implicitly defined in file "+filename+", line "+line+", column "+column;
    }
	public VariableInstance getInstance(String name, String filename, int line, int column, Hashtable<VariableInstance, Manager> managermap)
	throws IntolerableException
	{
		System.out.println("Call to getInstance(s,s,i,i,ht) i.e. addin null in class "+getClass().getName());
		return getInstance(name, false, filename, line, column, null);
	}
	public VariableInstance getInstance(String name, boolean monitor, String filename, int line, int column, Hashtable<VariableInstance, Manager> managermap)
	throws IntolerableException
	{
		System.out.println("Call to getInstance(s,b,s,i,i,ht) i.e. addin null in class "+getClass().getName());
		return getInstance(name, false, filename, line, column, null);
	}
  }

	public boolean isNull() {
		return false;
	}

	public boolean valueEquals(VariableInstance sv) throws IntolerableException {
		if(!(sv instanceof ProcedureSingle)) {
			return false;
		}
		ProcedureSingle other = (ProcedureSingle) sv;
		if(this == other) {
			return true;
		}
		return p == other.p && localScope == other.localScope;
	}
}
