package se.modlab.simplesim.events;

import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.evaluables.*;
import se.modlab.generics.exceptions.*;

public interface SimulationEvent
{

  public double getSimTimeForNextEvent()
    throws IntolerableException;

  public void runEvent() 
    throws IntolerableException, 
           StopException;

  public boolean shallBeRequeued();

  public void reset()
    throws IntolerableException;

  public String getEventTypeName();

  public String getEventName();
  
  public String getPlace();

}