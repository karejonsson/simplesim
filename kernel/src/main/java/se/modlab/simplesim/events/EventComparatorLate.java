package se.modlab.simplesim.events;

import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.events.*;
import java.util.*;

public class EventComparatorLate implements Comparator 
{

  public int compare(Object first, Object second)
  {
    double f = 0;
    double s = 0;
    try
    {
      f = ((SimulationEvent) first).getSimTimeForNextEvent();
      s = ((SimulationEvent) second).getSimTimeForNextEvent();
      //System.out.println("Event comparatro late compare "+first+" "+
      //                   second);
    }
    catch(IntolerableException e)
    {
      //System.out.println("eventComparatorLate exception");
      return 1;
    }
    int out;
    if(f - s <= 0) out = -1; else out = 1;
    //System.out.println("Event comparator late compare "+first+" "+
    //                   second+". Out = "+out);
    
    return out;
  }

}
