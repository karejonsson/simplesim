package se.modlab.simplesim.events;

import java.util.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;
//import se.modlab.simplesim.references.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.evaluables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.simplesim.scoping.*;

public class ProcedureActor extends VariableInstance 
implements Enqueuable
{

	private SimScope localScope;
	private ProcedureActorState pas[];
	private Commons com;
	private ScopeFactory sf;
	private SimulationEventQueue eq;
	private ProcedureActorState state = null;
	private String fullPathName;
	private String lastSuccessfulTransition = null;

	private boolean enqueued = false;

	public ProcedureActor(String _name,
			String filename, int line, int column, 
			SimScope _localScope,
			ProcedureActorState _pas[],
			Commons _com,
			ScopeFactory _sf)
					throws IntolerableException
					{
		this(_name, new procedureActorType(_name, filename, line, column),
				filename, line, column,
				_localScope, _pas, _com, _sf);
					}

	public ProcedureActor(String _name,
			SimVariableFactory _vf,
			String filename, int line, int column, 
			SimScope _localScope,
			ProcedureActorState _pas[],
			Commons _com,
			ScopeFactory _sf)
					throws IntolerableException
					{
		super(_name, _vf, filename, line, column);
		localScope = _localScope;
		pas = _pas;
		com = _com;
		sf = _sf;
		new ScopeReference(localScope, "locals", filename, line, column);
		new ScopeReference(localScope, vt.getTypesName(), filename, line, column);
		//new scopeReference(localScope, "this");
		//System.out.println("Procedure actor "+vt.getTypesName());
		localScope.addComplexInstance(this, "this");
					}

	public Scope getLocalScope()
	{
		return localScope;
	}

	public String getCurrentStateName()
	{
		/*if(state == null)
    {
      System.out.println("Actor "+getName()+"\n"+
        "Time "+com.getCurrentSimTime());
      System.out.println(eq.toString());
    }*/
		if(state == null)
		{
			return "";
		}
		if(!isEnqueued())
		{
			return "dequeued";
		}
		return state.getName();
	}

	public void queueUp(SimulationEventQueue eq) throws IntolerableException,  StopException
	{
		if(enqueued) {
			throw new UserRuntimeError(
					"Actor "+name+", defined "+this.getPlace()+" was enqueued when already active.");                   
		}
		//System.out.println("actor "+getName()+" queued up!");
		this.eq = eq;
		eq.queueUpEarly(new RunnableQueuer(com.getCurrentSimTime(), 
				new ExceptionRunnable() {
			public void run()
					throws IntolerableException, StopException {
				ProcedureActor.this.runInitially();
			}
			public VariableInstance getActor() {
				return ProcedureActor.this;
			}
		}, getFullPathName()+", defined "+this.getPlace()+" initially onEnter"));
		enqueued = true;
	}

	public void queueDown(SimulationEventQueue eq) throws IntolerableException {
		if(!enqueued) {
			throw new UserRuntimeError(
					"Actor "+getFullPathName()+", defined "+this.getPlace()+" was dequeued when not active.");                   
		}
		enqueued = false;
	}

	public boolean isEnqueued()
	{
		return enqueued;
	}

	public void reset()
	{
		state = null;
		enqueued = false;
		lastSuccessfulTransition = "No previous transition.";
	}

	public void accept(InstanceVisitor inst)
			throws IntolerableException {
		inst.visit(this);
	}

	public void signal(String signal, 
			VariableInstance sender, 
			VariableInstance message,
			final String leadingErrorText)
					throws IntolerableException {
		if(!enqueued) {
			throw new UserRuntimeError(
					"Actor "+name+", defined "+this.getPlace()+" recieved signal when inactive.\n\n"+leadingErrorText);                   
		}
		//System.out.println("Actor "+getName()+" got signal "+signal);
		final String sig = signal; 
		final VariableInstance sen = sender;
		final VariableInstance mes = message;
		//if(eq == null) System.out.println("EQ");
		//if(state == null) System.out.println("STATE");
		//if(sen == null) System.out.println("SEN");
		String eventDescription = getFullPathName()+" "+((state != null)?state.getName():"Unintialized")+
				" transfer on "+signal;
		if(sen != null) eventDescription = eventDescription + " from "+sen.toString();
		//final String leadingErrorMessage = leadingErrorText;
		eq.queueUpLate(new RunnableQueuer(com.getCurrentSimTime(), 
				new ExceptionRunnable()
		{
			public void run() throws IntolerableException, StopException {
				ProcedureActor.this.runExitOnSignal(sig, sen, mes, leadingErrorText);
			}
			public VariableInstance getActor() {
				return ProcedureActor.this;
			}
		}, eventDescription));
	}

	private void runInitially() throws IntolerableException,  StopException {
		if(state != null) {
			throw new InternalError("State not null in procedureActor.runInitially.\n"+
					"Actual state is "+state); 
		}    
		state = pas[0];
		ProgramBlock onenter = state.getOnenter();
		try {
			onenter.execute(localScope);
		}
		catch(ReturnException re) {
		}
		catch(BreakException be) {
			throw new InternalProgrammingError(
					"Break exception unhandled until procedureActor in runInitially.",
					be);
		}
		catch(ContinueException ce) {
			throw new InternalProgrammingError(
					"Continue exception unhandled until procedureActor in runInitially.",
					ce);
		}
		lastSuccessfulTransition = "Not enqueued to state "+state.getName()+
				" on queue up at time "+com.getCurrentSimTime();
	}

	private void runExitOnSignal(String signal, 
			VariableInstance sender,
			VariableInstance message,
			final String leadingErrorText) throws IntolerableException,  StopException {
		if(state == null) {
			throw new InternalProgrammingError("State null in procedureActor.runExitOnSignal"); 
		} 
		localScope.addSenderAndMessage(sender, message);   
		ProcedureActorSignal pasi = state.getTransition(signal);
		if(pasi == null) {
			//final String mess = leadingErrorText;
			throw new UserRuntimeError(
					"Signal "+signal+" is not declared in state "+getCurrentStateName()+
					" for\n"+"actor "+getFullPathName()+" of type "+getType()+"\n"+
					"on time "+com.getCurrentSimTime()+"\n\n"+
					leadingErrorText+"\n\nPrevious successful transition was\n"+
					lastSuccessfulTransition);
		}
		ProgramBlock onsignal = pasi.getOnsignal();
		String nextState = pasi.getNextState();
		if(nextState == null) {
			// Remain
			try {
				onsignal.execute(localScope);
				localScope.removeSenderAndMessage();   
			}
			catch(ReturnException re) {
				localScope.removeSenderAndMessage();   
			}
			catch(BreakException be) {
				localScope.removeSenderAndMessage();   
				throw new InternalProgrammingError(
						"Break exception unhandled until procedureActor in runTransitionOnSignal.",
						be);
			}
			catch(ContinueException ce) {
				localScope.removeSenderAndMessage();   
				throw new InternalProgrammingError(
						"Continue exception unhandled until procedureActor in runTransitionOnSignal.",
						ce);
			}
			//System.out.println("Sender "+(sender == null));
			//System.out.println("State "+(state == null));
			lastSuccessfulTransition = 
					"Remain in state "+state.getName()+" on signal "+signal+"\n"+
							" from sender "+sender.getName()+" at time \n"+
							com.getCurrentSimTime();
			return;      
		}
		ProgramBlock onexit = state.getOnexit();
		try {
			onexit.execute(localScope);
		}
		catch(ReturnException re) {
			localScope.removeSenderAndMessage();   
		}
		catch(BreakException be) {
			localScope.removeSenderAndMessage();   
			throw new InternalProgrammingError(
					"Break exception unhandled until procedureActor in runSignal.",
					be);
		}
		catch(ContinueException ce) {
			localScope.removeSenderAndMessage();   
			throw new InternalProgrammingError(
					"Continue exception unhandled until procedureActor in runSignal.",
					ce);
		}
		try {
			onsignal.execute(localScope);
		}
		catch(ReturnException re) {
			localScope.removeSenderAndMessage();   
		}
		catch(BreakException be) {
			localScope.removeSenderAndMessage();   
			throw new InternalProgrammingError(
					"Break exception unhandled until procedureActor in runTransitionOnSignal.",
					be);
		}
		catch(ContinueException ce) {
			localScope.removeSenderAndMessage();   
			throw new InternalProgrammingError(
					"Continue exception unhandled until procedureActor in runTransitionOnSignal.",
					ce);
		}
		String previousState = state.getName();
		for(int i = 0 ; i < pas.length ; i++) {
			//System.out.println("pas["+i+"] = >"+pas[i].getName()+"<");
			if(pas[i].getName().compareTo(nextState) == 0) {
				state = pas[i];
				runEnterAfterSignal();
				String sendersName = null;
				if(sender == null) {
					sendersName = "Non queuable";
				}  
				else {
					sendersName = sender.getName();
				}
				//if(state == null) System.out.println("state == null");
				//if(sender == null) System.out.println("sender == null");
				//if(com == null) System.out.println("com == null");
				lastSuccessfulTransition = 
						"From state "+previousState+" to state "+nextState+
						" on signal "+signal+
						"\nfrom sender "+sendersName+" at time "+com.getCurrentSimTime();
				/*
        if(sender == null)
        { 
          System.out.println(lastSuccessfulTransition);
          System.exit(0);
        }
				 */
				return;
			}
		}
		localScope.removeSenderAndMessage();   
		throw new UserRuntimeError(
				"No state named "+nextState+" in actor "+
						getFullPathName()+", defined "+this.getPlace()+"\n\n"+
						leadingErrorText+
						"\n\nPrevious successfull transition was\n"+lastSuccessfulTransition);
	}

	private void runEnterAfterSignal() throws IntolerableException, StopException {
		if(state == null) {
			throw new InternalError("State null in procedureActor.runExitOnSignal"); 
		}    
		ProgramBlock onenter = state.getOnenter();
		try {
			onenter.execute(localScope);
			localScope.removeSenderAndMessage();   
		}
		catch(ReturnException re) {
			localScope.removeSenderAndMessage();   
		}
		catch(BreakException be) {
			localScope.removeSenderAndMessage();   
			throw new InternalProgrammingError(
					"Break exception unhandled until ProcedureActor in runSignal.",
					be);
		}
		catch(ContinueException ce) {
			localScope.removeSenderAndMessage();   
			throw new InternalProgrammingError(
					"Continue exception unhandled until ProcedureActor in runSignal.",
					ce);
		}
	}

	public void setDefaultInitialValue()
			throws IntolerableException
			{
			}

	public void setInitialValue(VariableInstance _val) 
			throws IntolerableException
			{
		throw new InternalProgrammingError(
				"ProcedureActor.setInitialValue(variableInstance)\n"+
						"called with object of class "+_val.getClass().getName()+".\n"+
				"This method should not be called at all!");
			}

	public void setValue(VariableInstance si)
			throws IntolerableException
			{
		throw new InternalProgrammingError(
				"procedureActor.setValue was called\n"+
						"on repeating procedure "+name+", defined "+this.getPlace());
			}

	public void setFullPathName(String fpn) throws IntolerableException {
		fullPathName = fpn;
	}

	public String getFullPathName() {
		if(fullPathName == null) {
			// For event table algo Debug, to not have null names
			return "dynamic "+this.getType().getTypesName();
		}
		return fullPathName;
	}

	public void addMember(VariableInstance si) throws IntolerableException {
		localScope.addMember(si);
	}

	public int getSize() {
		return localScope.getSize();
	}

	public VariableInstance getMember(int i) throws IntolerableException {
		return localScope.getMember(i);
	}

	public VariableInstance getMember(String name) throws IntolerableException {
		return localScope.getMember(name);
	}

	public VariableInstance[] getAllInstances() {
		return localScope.getAllInstances();
	}

	public String toString() {
		return getFullPathName();
	}

	private static class procedureActorType implements SimVariableFactory {
		private String name;
		private String filename;
		private int line;
		private int column;
		public procedureActorType(String _name, String _filename, int _line, int _column) {
			name = _name;
			filename = _filename;
			line = _line;
			column = _column;
		}
		public String getTypesName() {
			return name;
		}
		public VariableInstance getInstance(String name, String _filename, int _line, int _column) {
			return null;
		}
		public VariableInstance getInstance(String name, boolean monitor, String _filename, int _line, int _column) {
			return null;
		}
		public String toString() {
			return name+", implicitly defined in file "+filename+", line "+line+", column "+column;
		}
		public VariableInstance getInstance(String name, String filename, int line, int column, Hashtable<VariableInstance, Manager> managermap)
				throws IntolerableException {
			System.out.println("Call to getInstance(s,s,i,i,ht) i.e. addin null in class "+getClass().getName());
			return getInstance(name, false, filename, line, column, null);
		}
		public VariableInstance getInstance(String name, boolean monitor, String filename, int line, int column, Hashtable<VariableInstance, Manager> managermap)
				throws IntolerableException {
			System.out.println("Call to getInstance(s,b,s,i,i,ht) i.e. addin null in class "+getClass().getName());
			return getInstance(name, false, filename, line, column, null);
		}
	}

	public boolean isNull() {
		return false;
	}

	public boolean valueEquals(VariableInstance sv) throws IntolerableException {
		if(!(sv instanceof ProcedureActor)) {
			return false;
		}
		ProcedureActor other = (ProcedureActor) sv;
		if(this == other) {
			return true;
		}
		return pas == other.pas && localScope == other.localScope;
	}

}
