package se.modlab.simplesim.events;

import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.events.*;
import java.util.*;

public class EventComparator implements Comparator 
{

  public int compare(Object first, Object second)
  {
    double f = 0;
    double s = 0;
    try
    {
      f = ((SimulationEvent) first).getSimTimeForNextEvent();
      s = ((SimulationEvent) second).getSimTimeForNextEvent();
    }
    catch(IntolerableException e)
    {
      //System.out.println("eventComparator exception");
      return -1;
    }
    if(f == s) 
    {
      //System.out.println("eventComparator equal on "+s);
      if(Math.random() < 0.5)
      { 
        return 1;
      }
      else
      {
        return -1;
      }
    }
    if(f < s) return -1;
    return 1;
  }

}
