package se.modlab.simplesim.events;

import se.modlab.simplesim.variables.*;
import se.modlab.simplesim.references.*;
import java.lang.reflect.*;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.scoping.*;

public abstract class CreateAllExportedVersionsVisitor 
extends CreateExportedVersionVisitor
{

	public void visit(ProcedureActor inst) throws IntolerableException {
		si = new ProcedureActorExported(inst);
	}

	public void visit(ProcedureActorExported inst) throws IntolerableException {
		si = inst;
	}

	public void visit(UntypedQueuableReference inst) throws IntolerableException {
		si = new UntypedQueuableReferenceConst(inst);
	}

	public void visit(UntypedQueuableReferenceConst inst) throws IntolerableException {
		si = inst;
	}

	public void visit(TypedQueuableReference inst) throws IntolerableException {
		si = new TypedQueuableReferenceConst(inst);
	}

	public void visit(TypedQueuableReferenceConst inst) throws IntolerableException {
		si = inst;
	}

	public void visit(StructReference inst) throws IntolerableException {
		si = new StructReferenceExported(inst);
	}

	public void visit(StructReferenceExported inst) throws IntolerableException {
		si = inst;
	}

	public void visit(QueuableReference inst) throws IntolerableException {
		si = new QueuableReferenceExported(inst);
	}

	public void visit(QueuableReferenceExported inst) throws IntolerableException {
		si = inst;
	}

	public void visit(ProcedureSingle inst) 
			throws IntolerableException 
			{
		si = new ProcedureSingleExported(inst);
			}

	public void visit(ProcedureSingleExported inst) 
			throws IntolerableException 
			{
		si = inst;
			}

	public void visit(ProcedureTrigged inst) 
			throws IntolerableException 
			{
		si = new ProcedureTriggedExported(inst);
			}

	public void visit(ProcedureTriggedExported inst) 
			throws IntolerableException 
			{
		si = inst;
			}

	public void visit(ProcedureRepeating inst) 
			throws IntolerableException 
			{
		si = new ProcedureRepeatingExported(inst);
			}

	public void visit(ProcedureRepeatingExported inst) 
			throws IntolerableException 
			{
		si = inst;
			}

	public void visit(ScopeReference inst) 
			throws IntolerableException 
			{
		System.out.println("createAllExportedVersionsVisitor - scopeReference "+inst.getName());
		si = new ScopeReferenceExported(inst, inst.getFilename(), inst.getLine(), inst.getColumn());
			}

	public void visit(SimDoubleExported inst) 
			throws IntolerableException 
			{
		si = inst;
			}

	public void visit(SimLongExported inst) 
			throws IntolerableException 
			{
		si = inst;
			}

	public void visit(SimBooleanExported inst) 
			throws IntolerableException 
			{
		si = inst;
			}

	public void visit(SimVectorExported inst) 
			throws IntolerableException 
			{
		si = inst;
			}

	public void visit(SimVariableVectorExported inst) 
			throws IntolerableException 
			{
		si = inst;
			}

	public void visit(SimComplexVariableExported inst) 
			throws IntolerableException 
			{
		si = inst;
			}

}