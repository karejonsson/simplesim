package se.modlab.simplesim.events;

import java.util.*;

import se.modlab.generics.*;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.util.Sort;

public class SimulationEventQueue 
{

	private Vector<SimulationEvent> queue = new Vector<SimulationEvent>();
	private Vector<SimulationEvent> triggers = new Vector<SimulationEvent>();
	private Comparator compare = new EventComparator();
	private Comparator compareLate = new EventComparatorLate();
	private Comparator compareEarly = new EventComparatorEarly();

	public void queueUp(SimulationEvent se) throws IntolerableException {
		if(se instanceof ProcedureTrigged) {
			addTrigged(se);
			return;
		}
		Sort.add(queue, se, compare);
		//System.out.println("Queue size is "+queue.size()+" added "+se+". Triggers "+triggers.size());
	}

	public int length() {
		return queue.size();
	}

	public void queueDown(SimulationEvent se) throws IntolerableException {
		if(se instanceof ProcedureTrigged) {
			if(!triggers.contains(se)) {
				throw new UserRuntimeError("Event "+se+" was dequeued without being\n"+
						"enqueued.\n"+
						"The event was defined "+se.getPlace());
			}
			removeTrigged(se);
			return;
		}
		if(!queue.contains(se)) {
			throw new UserRuntimeError("Event "+se+" was dequeued without being\n"+
					"enqueued.\n"+
					"The event was defined "+se.getPlace());
		}
		queue.removeElement(se);
		//Sort.add(queue, se, compare);
		//System.out.println("Queue size is "+queue.size());
	}

	public void queueUpLate(SimulationEvent se) {
		if(se instanceof ProcedureTrigged) {
			addTrigged(se);
			return;
		}
		//System.out.println("sort : "+se+" "+Sort.add_sayWhere(queue, se, compareLate));
		Sort.add(queue, se, compareLate);
	}

	public void queueUpEarly(SimulationEvent se) {
		if(se instanceof ProcedureTrigged) {
			addTrigged(se);
			return;
		}
		//System.out.println("sort : "+se+" "+Sort.add_sayWhere(queue, se, compareEarly));
		Sort.add(queue, se, compareEarly);
	}

	public void setInitialTrigged(Vector<SimulationEvent> initialTriggers) {
		//triggers.addAll(initialTriggers); 
		triggers = initialTriggers;
	}

	public void addTrigged(SimulationEvent se) {
		//int sizeBefore = triggers.size();
		triggers.addElement(se);
		//System.out.println("SimulationEventQueue: Added trigger "+se+". Triggers size is "+triggers.size()+", before "+sizeBefore);
	}

	public void removeTrigged(SimulationEvent se) {
		//int sizeBefore = triggers.size();
		triggers.removeElement(se);
		//System.out.println("SimulationEventQueue: Removed trigger "+se+". Triggers size is "+triggers.size()+", before "+sizeBefore);
	}

	public SimulationEvent getFromQueue() {
		if(queue.size() == 0) return null;
		return queue.remove(0);
	}

	public void removeAll() {
		queue.removeAllElements();
	}

	public boolean empty(){
		return queue.size() == 0;
	}

	public String toString() {
		if(queue.size() == 0) { 
			return "Queue is empty";
		}
		StringBuffer sb = new StringBuffer();
		for(int i = 0 ; i < queue.size() ; i++) {
			sb.append(queue.elementAt(i).toString()+"\n");
		}
		return "Queue has "+queue.size()+" elements.\n"+sb.toString();
	}
/*
	private interface sorter {
		public double getSortIndex(); 
	}
	*/
/*
	private static class ToSort implements sorter {
		private String name;
		private double idx;
		public ToSort(String _name, double _idx) {
			name = _name;
			idx = _idx;
		}
		public double getSortIndex() {
			return idx;
		}
		public String toString() {
			return name+" on "+idx;
		}
	}

	private static class CompE implements Comparator 
	{

		public int compare(Object first, Object second) {
			double f = 0;
			double s = 0;
			f = ((sorter) first).getSortIndex();
			s = ((sorter) second).getSortIndex();
			if(s >= f) return -1;
			return 1;
		}

	} 

	private static class CompL implements Comparator  {

		public int compare(Object first, Object second) {
			double f = 0;
			double s = 0;
			f = ((sorter) first).getSortIndex();
			s = ((sorter) second).getSortIndex();
			if(s > f) return -1;
			return 1;
		}

	} 
 */
	/*
	public static void print(Vector queue)   {
		if(queue.size() == 0) { 
			System.out.println("Queue is empty");
		}
		StringBuffer sb = new StringBuffer();
		for(int i = 0 ; i < queue.size() ; i++) {
			sb.append(queue.elementAt(i).toString()+"\n");
		}
		System.out.println("Queue has "+queue.size()+" elements.\n"+sb.toString());
	}
	*/

/*
	public static void main(String args[]) {
		Vector queue = new Vector();
		CompL cl = new CompL();
		CompE ce = new CompE();

		ToSort ts = new ToSort("Ett", 0);
		Sort.add(queue, ts, ce);
		ts = new ToSort("Tv�", 0);
		Sort.add(queue, ts, ce);
		ts = new ToSort("Tre", 0);
		Sort.add(queue, ts, cl);
		ts = new ToSort("Fyra", 1);
		Sort.add(queue, ts, ce);
		print(queue);
	}
	*/
}
