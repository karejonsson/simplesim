package se.modlab.simplesim.events;

import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.evaluables.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.generics.exceptions.*;

public class InitiallyEvent implements SimulationEvent
{

  private Scope localScope;
  private String name;
  private ProgramBlock p;
  private String filename;
  private int line;
  private int column;
  private String eventname;

  public InitiallyEvent(Scope _localScope, String _name, ProgramBlock _p,
                        String _filename, int _line, int _column)
  {
    localScope = _localScope;
    name = _name;
    p = _p;
    filename = _filename;
    line = _line;
    column = _column;
    eventname = "finally"+((name != null) ? " "+name : "")+
      " file "+filename+", line "+line;
  }

  public String getName() 
  {
    if(name == null) return "No name";
    return name;
  }

  public String getFilename()
  {
    return filename;
  }  

  public int getLine()
  {
    return line;
  }  

  public int getColumn()
  {
    return line;
  }  

  public void runEvent() 
    throws StopException, IntolerableException
  {
    try
    {
      p.execute(localScope);
    }
    catch(ReturnException e)
    {
    }
    catch(BreakException e)
    {
      throw new InternalError(
        "Break exception happened in runtime.");
    }
    catch(ContinueException e)
    {
      throw new InternalError(
        "Continue exception happened in runtime.");
    }
  }

  public boolean shallBeRequeued()
  {
    return false;
  }

  public double getSimTimeForNextEvent()
    throws IntolerableException
  { 
    return 0;
  }

  public String toString()
  {
    return "Initially event on time 0";
  }

  public void reset()
    throws IntolerableException
  {
  }

  public String getEventTypeName()
  {
    return "initially";
  }

  public String getEventName()
  {
    return eventname;
  }

  public String getPlace() {
	  return "file "+filename+", line "+line+", column "+column;
  }

}
