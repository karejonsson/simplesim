package se.modlab.simplesim.events;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.variables.*;

public class RunnableQueuer implements SimulationEvent {

  private double timeToRun = 0;
  private ExceptionRunnable r = null;
  private String name;

  public RunnableQueuer(double _timeToRun,
                        ExceptionRunnable _r, 
                        String _name) {
    timeToRun = _timeToRun;
    r  =_r;
    name = _name;
  }

  public VariableInstance getActor() {
    return r.getActor();
  }

  public double getSimTimeForNextEvent() throws IntolerableException {
    return timeToRun;
  }

  public void runEvent() throws IntolerableException, StopException {
      r.run();
  }

  public boolean shallBeRequeued() {
    return false;
  }

  public void reset() throws IntolerableException {
  }

  public String getEventTypeName() {
    return "Asynchronous executor";
  }

  public String getEventName() {
    return name;
  }

  public String toString() {
    return "Queuer "+name+" for time "+timeToRun;
  }

  public String getPlace() {
	  return "file <not known>, line <not known>, column <not known>";
  }

}

