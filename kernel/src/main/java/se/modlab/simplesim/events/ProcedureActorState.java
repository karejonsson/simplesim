package se.modlab.simplesim.events;

import se.modlab.generics.exceptions.*; 
import se.modlab.generics.sstruct.executables.*;

public class ProcedureActorState 
{

  private String name;
  private ProgramBlock onenter;
  private ProgramBlock onexit;
  private ProcedureActorSignal pas[];

  public ProcedureActorState(String _name,
                             ProgramBlock _onenter,
                             ProgramBlock _onexit, 
                             ProcedureActorSignal _pas[])
  {
    name = _name;
    onenter = _onenter;
    onexit = _onexit;
    pas = _pas;
  }

  public String getName()
  { 
    return name;
  }

  public ProgramBlock getOnenter()
  {
    return onenter;
  }

  public ProgramBlock getOnexit()
  {
    return onexit;
  }

  public ProcedureActorSignal getTransition(String signal)
    //throws intolerableException
  {
    for(int i = 0 ; i < pas.length ; i++)
    {
      if(pas[i].getName().compareTo(signal) == 0)
      {
        return pas[i];
      }
    }
    return null;
    //throw new userError("Signal "+signal+" is not declared in state "+name);
  }

  public String toString() 
  {
    return "State "+name;
  }

}