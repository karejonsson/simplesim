package se.modlab.simplesim.events;

import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.events.*;
import java.util.*;

public class EventComparatorEarly implements Comparator 
{

  public int compare(Object first, Object second)
  {
    double f = 0;
    double s = 0;
    try
    {
      f = ((SimulationEvent) first).getSimTimeForNextEvent();
      s = ((SimulationEvent) second).getSimTimeForNextEvent();
    }
    catch(IntolerableException e)
    {
      //System.out.println("eventComparatorEarly exception");
      return -1;
    }
    if(s >= f) return -1;
    return 1;
  }

}
