package se.modlab.simplesim.events;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.variables.*;

public interface Enqueuable extends Complex 
{
  public void queueUp(SimulationEventQueue eq) throws IntolerableException,  StopException;

  public void queueDown(SimulationEventQueue eq) throws IntolerableException, StopException;

  public boolean isEnqueued() throws IntolerableException;
 
  public void reset() throws IntolerableException;

  public void setFullPathName(String fpn)  throws IntolerableException;

  public String getFullPathName();

  public Scope getLocalScope();
}
