package se.modlab.simplesim.events;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.variables.*;

public interface ExceptionRunnable 
{
  public void run() 
    throws IntolerableException, 
           StopException;

  public VariableInstance getActor();
/*
  public void run() 
    throws returnException, 
           intolerableException, 
           stopException,
           breakException,
           continueException;
 */
}
