package se.modlab.simplesim.predefs;

import se.modlab.generics.sstruct.values.*;
import se.modlab.simplesim.variables.Commons;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.exceptions.*;

public class PredefSimulationTime implements ArithmeticEvaluable 
{
	private Commons com;

	public PredefSimulationTime(Commons com)
	{
		this.com = com;
	}

	public sValue evaluate(Scope s) throws IntolerableException
	{
		sDouble val = new sDouble(com.getCurrentSimTime());
		return val;
	}

	public void verify(Scope arg0) throws IntolerableException {
		// TODO Auto-generated method stub

	}
	
	public String reproduceExpression() {
		return "time()";
	}


}
