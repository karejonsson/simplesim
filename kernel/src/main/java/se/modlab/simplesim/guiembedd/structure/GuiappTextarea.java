package se.modlab.simplesim.guiembedd.structure;

import java.awt.Component;

import javax.swing.JTextArea;

public class GuiappTextarea implements GuiappField {

	private String name;
	private String message;
	private String helptext;
	
	public GuiappTextarea(String _name, String _message, String _helptext) {
		name = _name;
		message = _message;
		helptext = _helptext;
	}

	public String getHelptext() {
		return helptext;
	}

	public String getName() {
		return name;
	}

	public Component getComponent() {
		JTextArea area = new JTextArea(message);
		area.setEditable(false);
		return area;
	}
	
}
