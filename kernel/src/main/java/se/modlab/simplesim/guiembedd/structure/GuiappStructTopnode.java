package se.modlab.simplesim.guiembedd.structure;

import java.util.Vector;

public class GuiappStructTopnode {
	
	private String filename;
	private String frametitle;
	private boolean hassize = false;
	private int width = -1;
	private int height = -1;
	
	private Vector<GuiappTab> tabs = new Vector<GuiappTab>();
	
	private GuiappPresentation pres = null;

	public GuiappStructTopnode(String _filename, String _frametitle) {
		filename = _filename;
		frametitle = _frametitle;
		//System.out.println("GuiappStructTopnode("+filename+", "+frametitle+")");
	}
	
	public void setHeight(int h) {
		hassize = true;
		height = h;
	}
	
	public void setWidth(int w) {
		hassize = true;
		width = w;
	}
	
	public boolean hasSize() {
		return hassize;
	}
	
	public int getHeight() {
		return height;
	}
	
	public int getWidth() {
		return width;
	}
	
	public String getFilename() {
		return filename;
	}
	
	public String getFrametitle() {
		return frametitle;
	}
	
	public void addTab(GuiappTab tab) {
		//System.out.println("GuiappStructTopnode("+filename+") got "+tab);
		tabs.add(tab);
	}
	
	public void setPresentation(GuiappPresentation _pres) {
		pres = _pres;
	}
	
	public GuiappPresentation getPresentation() {
		return pres;
	}
	
	
	public int getNoTabs() {
		return tabs.size();
	}
	
	public GuiappTab getTab(int i) {
		if(i < 0) {
			return null;
		}
		if(tabs.size() <= i) {
			return null;
		}
		return tabs.elementAt(i);
	}
	
}
