package se.modlab.simplesim.guiembedd.structure;

import java.util.Vector;

import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.sstruct.comparisons.ArithmeticEvaluable;
import se.modlab.generics.sstruct.comparisons.VariableLookup;
import se.modlab.generics.sstruct.logics.LogicalExpression;

public class GuiappTab {
	
	private String tabname;
	
	private Vector<GuiappField> variables = new Vector<GuiappField>();
	
	/**
	 * 
	 * @param x Cannot be null
	 */
	public GuiappTab(String _tabname) {
		//System.out.println("GuiappTab("+_tabname+")");
		tabname = _tabname;
	}
	
	public String getTabname() {
		return tabname;
	}
	
	/**
	 * 
	 * @param vl Cannot be null
	 * @param le Cannot be null
	 * @param optionalname Can be null
	 */
	public void addLogicalVariable(VariableLookup vl, LogicalExpression le, String optionalname, String helptext) {
		//System.out.println("GuiappTab("+tabname+") got logical "+vl+", "+le+", "+ optionalname);
		variables.add(new GuiappLogicalVariable(vl, le, optionalname, helptext));
	}
    
	/**
	 * 
	 * @param vl Cannot be null
	 * @param ae Can be null
	 * @param optionalname Can be null
	 */
	public void addArithmeticVariable(VariableLookup vl, ArithmeticEvaluable ae, String optionalname, String helptext) {
		//System.out.println("GuiappTab("+tabname+") got arithmetic "+vl+", "+ae+", "+ optionalname);
		variables.add(new GuiappArithmeticVariable(vl, ae, optionalname, helptext));
	}
	
	public void addTableVariable(VariableLookup vl, String filename, String colnames[], String optionalname, String helptext) throws IntolerableException {
		variables.add(new GuiappTable(vl, filename, colnames, optionalname, helptext));
	}
	
	public void addStarttime(String starttime, String name, String tip) {
		variables.add(new GuiappStarttime(starttime, name, tip));
	}
	
	public void addEndtime(String endtime, String name, String tip) {
		variables.add(new GuiappEndtime(endtime, name, tip));
	}
	
	public void addTextarea(String name, String message, String helptext) {
		variables.add(new GuiappTextarea(name, message, helptext));
	}
	
	public String toString() {
		return "GuiappTab("+tabname+")";
	}
	
	public int getNoFields() {
		return variables.size();
	}
	
	public GuiappField getField(int i) {
		if(i < 0) {
			return null;
		}
		if(variables.size() <= i) {
			return null;
		}
		return variables.elementAt(i);
	}
	
}
