package se.modlab.simplesim.guiembedd.structure;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.JTextField;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import se.modlab.generics.exceptions.UserCompiletimeError;
import se.modlab.generics.exceptions.UserError;
import se.modlab.generics.exceptions.UserRuntimeError;
import se.modlab.generics.exceptions.InternalProgrammingError;
import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.sstruct.comparisons.ArithmeticEvaluable;
import se.modlab.generics.sstruct.comparisons.VariableLookup;
import se.modlab.generics.sstruct.evaluables.ArithmeticEvaluableLong;
import se.modlab.simplesim.algorithms.setup.SimData;
import se.modlab.simplesim.guiembedd.parse.GuiappParser;
import se.modlab.simplesim.variables.Commons;

public class GuiappStarttime implements GuiappField {
	
	private String starttime = null;
	private String name = null;
	private String tip = null;
	protected JTextField tf = null;
	protected boolean intact;

	public GuiappStarttime(String _starttime, String _name, String _tip) {
		starttime = _starttime;
		name = _name;
		tip = _tip;
	}

	public Component getComponent() {
		if(tf == null) {
			tf = new JTextField();
			tf.addActionListener(
					new ActionListener() {

						public void actionPerformed(ActionEvent arg0) {
							invalidateTextfield("action performed");
						}
					}
					);
			tf.addCaretListener(
					new CaretListener() {

						public void caretUpdate(CaretEvent arg0) {
							invalidateTextfield("caret update");
						}
					}
					);
			tf.setText(starttime);
		}
		return tf; 
	}

	public String getHelptext() {
		return tip;
	}

	public String getName() {
		if(name == null) {
			return "starttime";
		}
		return name;
	}
	
	public void performStarttimeUpdate(SimData sd) throws IntolerableException {

		String tmp = tf.getText();
		//System.out.println("Starttime <"+tmp+">");
		if((tmp == null) || (tmp.trim().length() == 0)) {
			long l = new Date().getTime();
			sd.setStartTime(Commons.getTimeStringFromLong(l));
			sd.getCommons().setEpochStartTime(l);
			return;
		}
		int colons = tmp.indexOf("::");
		if(colons != -1) tmp = tmp.substring(0, colons-1);
		try
		{
			long l = new Date(tmp).getTime();
			//System.out.println("getTimeLongFromString: Fick "+s+" svarar "+l);
			sd.setStartTime(tmp);
			sd.getCommons().setEpochStartTime(l);
			return;
		}
		catch(IllegalArgumentException e)
		{
			throw new UserRuntimeError("The special field "+getName()+" was set to "+tf.getText()+" for start time but it is wrong");
		}
	}

	protected void invalidateTextfield(String event) {
		//System.out.println("invalidateTextfield "+event+" on "+getName());
		intact = false;
	}


}
