package se.modlab.simplesim.guiembedd.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import se.modlab.generics.bshro.ifc.HierarchyObject;
import se.modlab.generics.exceptions.UserCompiletimeError;
import se.modlab.generics.exceptions.UserRuntimeError;
import se.modlab.generics.exceptions.InternalProgrammingError;
import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.exceptions.MotherCatalogue;
import se.modlab.generics.exceptions.UnclassedError;
import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.find.StringGridBagLayout;
import se.modlab.generics.sstruct.comparisons.ArithmeticEvaluable;
import se.modlab.generics.sstruct.comparisons.Scope;
import se.modlab.generics.sstruct.comparisons.VariableLookup;
import se.modlab.generics.sstruct.logics.LogicalExpression;
import se.modlab.generics.sstruct.variables.VariableInstance;
import se.modlab.simplesim.algorithms.externalpainters.ExternalPainterFactoryHandle;
import se.modlab.simplesim.algorithms.externalpainters.PlugInPainterFactory;
import se.modlab.simplesim.algorithms.externalpainters.RightExplorerSideComponent;
import se.modlab.simplesim.algorithms.presentation.MonitoredSimVariable;
import se.modlab.simplesim.algorithms.setup.SimData;
import se.modlab.simplesim.algorithms.share.Algorithm;
import se.modlab.simplesim.algorithms.share.AlgorithmRunner;
import se.modlab.simplesim.application.AppJoin;
import se.modlab.simplesim.guiembedd.structure.GuiappArithmeticVariable;
import se.modlab.simplesim.guiembedd.structure.GuiappEndtime;
import se.modlab.simplesim.guiembedd.structure.GuiappField;
import se.modlab.simplesim.guiembedd.structure.GuiappLogicalVariable;
import se.modlab.simplesim.guiembedd.structure.GuiappPresentation;
import se.modlab.simplesim.guiembedd.structure.GuiappStarttime;
import se.modlab.simplesim.guiembedd.structure.GuiappStructTopnode;
import se.modlab.simplesim.guiembedd.structure.GuiappTab;
import se.modlab.simplesim.guiembedd.structure.GuiappTable;
import se.modlab.simplesim.guiembedd.structure.GuiappTextarea;
import se.modlab.simplesim.guiembedd.structure.GuiappVariable;
import se.modlab.simplesim.parse.SimParser;
import se.modlab.simplesim.scoping.SimScopeFactory;
import se.modlab.simplesim.variables.Manager;

public class Execute { 

	public static void main(final GuiappStructTopnode gatn, final JMenu menu) {
		final JFrame frame = new JFrame();
		frame.setTitle(gatn.getFrametitle());

		frame.addWindowListener(new WindowAdapter() {

			public void windowClosing(WindowEvent windowevent)
			{
				// ? frame.windowAction("Cancel");
				frame.setVisible(false);
				frame.dispose();
			}

		});

		try 
		{ 
			Container cont = frame.getContentPane();
			cont.setLayout(new StringGridBagLayout());
			JPanel admin = new JPanel();
			JButton launch = new JButton("Run");
			final JCheckBox seeAllVariables = new JCheckBox("Get all variables window", false);
			launch.addActionListener(
					new ActionListener() {
				        public void actionPerformed(ActionEvent e)
				        {
				    		Runnable r = new Runnable() {
				    			public void run() {
						        	runGuiApp(gatn, menu, seeAllVariables.isSelected());
				    			}
				    		};
				    		Thread t = new Thread(r);
				    		t.start();
				        }
					}
			);
			admin.add(launch);
			admin.add(seeAllVariables);
			cont.add("gridx=0,gridy=0,anchor=NORTH,weighty=0.1,insets=[12,12,0,0]", admin);
			JTabbedPane jtp = new JTabbedPane();
			cont.add("gridx=0,gridy=1,fill=BOTH,insets=[12,7,0,11]", jtp);
			for(int i = 0 ; i < gatn.getNoTabs() ; i++) {
				GuiappTab gat = gatn.getTab(i);
				JPanel jp = new JPanel();
				JScrollPane sp = new JScrollPane();
				sp.getViewport().add(jp);
				jp.setLayout(new StringGridBagLayout());
				jp.setName(gat.getTabname());
				for(int j = 0 ; j < gat.getNoFields() ; j++) {
					GuiappField gav = gat.getField(j);
					JLabel lab = new JLabel();
					lab.setText(gav.getName());
					jp.add("gridx=0,gridy="+j+",anchor=WEST,insets=[12,12,0,0]", lab);
					String helptext = gav.getHelptext();
					if(helptext != null) {
						lab.setToolTipText(helptext);
					}
					Component comp = gav.getComponent();
					//tf.
					jp.add("gridx=1,gridy="+j+",fill=HORIZONTAL,weightx=1.0,insets=[12,7,0,11]", comp);
					//jp.add("gridx=1,gridy="+j+",fill=HORIZONTAL", gav.getTextField());
				}
				jtp.addTab(gat.getTabname(), sp);
			}
			frame.pack();
			Dimension d = frame.getSize();
			if(gatn.hasSize()) {
				int w = gatn.getWidth();
				if (w != -1) {
					d.width = w;
				}
				int h = gatn.getHeight();
				if (h != -1) {
					d.height = h;
				}
				frame.setSize(d);
				//frame.pack();
			}
			frame.setVisible(true);
		} 
		catch(Throwable t) 
		{
			System.out.println("uncaught exception: " + t);
			t.printStackTrace();
		}
	}

	/**
	 * Upon run.
	 * @param gatn
	 */
	private static void runGuiApp(GuiappStructTopnode gatn, final JMenu menu, boolean visible) {

		// First, parse the underlying sim file
		//System.out.println("======================================== Guiapp Execute.");
		Algorithm _algo = null; 
		Hashtable<VariableInstance, Manager> managermap = new Hashtable<VariableInstance, Manager>();
		try
		{
			File simfile = new File(gatn.getFilename());
			HierarchyObject.setReferenceFile(simfile.getParentFile());
			_algo = SimParser.parseFromFile(simfile.getName(), new SimScopeFactory(), false);
			//System.out.println("Execute: Setup to be initiated from Execute");
			_algo.setupScenario(managermap);
			//System.out.println("Execute: Setup DONE from Execute");
		}
		catch(IntolerableException ie)
		{
			System.out.println("se.modlab.simplesim.guiembedd.gui.Excecute.runGuiApp 1");
			ie.printStackTrace();
			if(_algo != null) ie.setCollectors(_algo.getCollectors());
			ie.setAction("Executed from treeview");
			UniversalTellUser.general(menu, ie);
			return;
		}
		catch(Exception _e)
		{
			System.out.println("se.modlab.simplesim.guiembedd.gui.Excecute.runGuiApp 2");
			_e.printStackTrace();
			IntolerableException ie = 
				new UnclassedError("Execute.runGuiApp f - run - ex",
						_e);
			if(_algo != null) ie.setCollectors(_algo.getCollectors());
			ie.setAction("Executed from treeview");
			UniversalTellUser.general(menu, ie);
			return;
		}
		catch(Throwable _e)
		{
			System.out.println("se.modlab.simplesim.guiembedd.gui.Excecute.runGuiApp 3");
			_e.printStackTrace();
			IntolerableException ie = 
				new UnclassedError("Execute.runGuiApp f - run - th",
						_e);
			if(_algo != null) ie.setCollectors(_algo.getCollectors());
			ie.setAction("Executed from treeview");
			UniversalTellUser.general(menu, ie);
			return;
		}
		final Algorithm algo = _algo;
		// Here, do the guiapp execution necessities. !!!!!!!!!!!!!
		// 1. Check all variables set in guiapp exists in global scope as constants of
		//    type double, long or boolean
		// 2. Replace the logical and arithmetic expressions.
		// That should be it!
		
		Scope gs = _algo.getGlobalScope();
		VariableLookup vl = null;
		VariableInstance vi = null;
		
		try {
			for(int i = 0 ; i < gatn.getNoTabs() ; i++) {
				GuiappTab gat = gatn.getTab(i);
				for(int j = 0 ; j < gat.getNoFields() ; j++) {
					GuiappField gaf = gat.getField(j);
					if(gaf instanceof GuiappVariable) {
						GuiappVariable gav = (GuiappVariable) gaf;
						vl = gav.getVariableLookup();
						vi = vl.getInstance(gs);
						if(vi == null) {
							throw new UserCompiletimeError("Variable "+vl+" referenced  on tab "+gat.getTabname()+" cannot be found in global scope.");
						}
						Manager m = managermap.get(vi);
						if((vi == null) && (m == null)) {
							throw new UserCompiletimeError(
									"Cannot find variable "+vl+" referenced on tab "+gat.getTabname()+"\n"+
									"cannot find manager for variable "+vl);
						}
						if(vi == null) {
							throw new UserCompiletimeError(
									"Cannot find variable "+vl+" referenced on tab "+gat.getTabname());
						}
						if(m == null) {
							for(VariableInstance _enum : managermap.keySet()) {
								System.out.println("Variable "+_enum);
							}
							throw new InternalProgrammingError("Cannot find manager for variable "+vl);
						}
						performGuiappVariableUpdate(m, gav);
					}
					else {
						performGuiappSpecialFieldUpdate(_algo.getSimData(), gaf);
					}
				}
			}
		}
		catch(IntolerableException ie)
		{
			if(_algo != null) ie.setCollectors(_algo.getCollectors());
			ie.setAction("Executed from treeview");
			UniversalTellUser.general(menu, ie);
			return;
		}
		catch(Exception _e)
		{
			IntolerableException ie = 
				new UnclassedError("SimFileMenyFactory.addRunAndVerifyToDotSim f - run - ex",
						_e);
			if(_algo != null) ie.setCollectors(_algo.getCollectors());
			ie.setAction("Executed from treeview");
			UniversalTellUser.general(menu, ie);
			return;
		}
		catch(Throwable _e)
		{
			IntolerableException ie = 
				new UnclassedError("SimFileMenyFactory.addRunAndVerifyToDotSim f - run - th",
						_e);
			if(_algo != null) ie.setCollectors(_algo.getCollectors());
			ie.setAction("Executed from treeview");
			UniversalTellUser.general(menu, ie);
			return;
		}

		// Run the actual algorithm
		
		try
		{
			//System.out.println("Execute: Now execute the algorithm");
			AlgorithmRunner ar = new AlgorithmRunner(_algo, menu, "Executing from GUI application", visible);
			AppJoin.getAppJoin().runAlgorithmSynchronized(ar, 700);
			//System.out.println("Execute: DONE with the algorithm");
		}
		catch(IntolerableException ie)
		{
			if(_algo != null) ie.setCollectors(_algo.getCollectors());
			ie.setAction("Executed from treeview");
			UniversalTellUser.general(menu, ie);
		}
		catch(Exception _e)
		{
			IntolerableException ie = 
				new UnclassedError("SimFileMenyFactory.addRunAndVerifyToDotSim f - run - ex",
						_e);
			if(_algo != null) ie.setCollectors(_algo.getCollectors());
			ie.setAction("Executed from treeview");
			UniversalTellUser.general(menu, ie);
		}
		catch(Throwable _e)
		{
			IntolerableException ie = 
				new UnclassedError("SimFileMenyFactory.addRunAndVerifyToDotSim f - run - th",
						_e);
			if(_algo != null) ie.setCollectors(_algo.getCollectors());
			ie.setAction("Executed from treeview");
			UniversalTellUser.general(menu, ie);
		}
		
		final GuiappPresentation pres = gatn.getPresentation();
		final JScrollPane rightView = new JScrollPane();
		final JScrollPane leftView = new JScrollPane();

	    JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
	    splitPane.setTopComponent(leftView);
	    splitPane.setBottomComponent(rightView);

	    Dimension minimumSize = new Dimension(100, 50);
	    rightView.setMinimumSize(minimumSize);
	    leftView.setMinimumSize(minimumSize);
	    splitPane.setDividerLocation(300);
	    splitPane.setPreferredSize(new Dimension(700, 500));
	    
	    final JFrame frame = new JFrame(pres.getFramename());
		frame.addWindowListener(new WindowAdapter() {

			public void windowClosed(WindowEvent windowevent)
			{
				// ? frame.windowAction("Cancel");
				frame.setVisible(false);
				frame.dispose();
			}

		});

	    frame.getContentPane().add(splitPane, BorderLayout.CENTER);
		
		JPanel jp = new JPanel();
		leftView.getViewport().add(jp);
		jp.setLayout(new StringGridBagLayout());
		Vector<String> graphstyles = new Vector<String>();
		final String standardname = "Standard";
		graphstyles.addElement(standardname);
		PlugInPainterFactory[] factories = ExternalPainterFactoryHandle.get();
		for(int i = 0 ; i < factories.length ; i++) {
			graphstyles.addElement(factories[i].getName());
		}
		final JLabel plotstyle = new JLabel("Chosen style of plot");
		jp.add("gridx=0,gridy=0,anchor=WEST,insets=[12,12,0,0]", plotstyle);
		final JComboBox cb = new JComboBox(graphstyles);
		jp.add("gridx=1,gridy=0,anchor=WEST,insets=[12,12,0,0]", cb);

		for(int i = 0 ; i < pres.getNoNames() ; i++) {
			final String name = pres.getName(i);
			final String fullyQualifyingName = pres.getFullyQualifyingName(i);
			JLabel varname = new JLabel(name);
			jp.add("gridx=0,gridy="+(i+1)+",anchor=WEST,insets=[12,12,0,0]", varname);
			JButton see = new JButton(">>");
			jp.add("gridx=1,gridy="+(i+1)+",anchor=WEST,insets=[12,12,0,0]", see);
			String helptext = pres.getHelptext(i);
			if(helptext != null) {
				see.setToolTipText(helptext);
				varname.setToolTipText(helptext);
			}
			see.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					String choice = (String) cb.getSelectedItem();
					//System.out.println("Execute. Choice = "+choice);
					if(choice.compareTo(standardname) == 0) {
						try {
							final MonitoredSimVariable monitoredVariables[] = algo.getTopnode().getVariablesNode().getStandardMonitoredVariablesComponents();
							final MonitoredSimVariable var = getStandardVariable(monitoredVariables, fullyQualifyingName);
							if(var == null) {
								//System.out.println("Execute. var = null Object = "+choice);
								JTextArea area = new JTextArea("No variable "+fullyQualifyingName+" found.");
								area.setEditable(false);
								rightView.setViewportView(area);
							}
							else {
								//System.out.println("Execute. var != null Object = "+choice);
								rightView.setViewportView(var);
							}
						}
						catch(IntolerableException ie) {
							UniversalTellUser.error(frame, ie.getMessage());
						}
					}
					else {
						try {
							final RightExplorerSideComponent monitoredVariables[] = algo.getTopnode().getVariablesNode().getExternallyPaintedMonitoredVariablesComponents(choice);
							final Component var = getExternalVariable(monitoredVariables, fullyQualifyingName);
							if(var == null) {
								JTextArea area = new JTextArea("No variable "+fullyQualifyingName+" found.");
								area.setEditable(false);
								rightView.setViewportView(area);
							}
							else {
								//System.out.println("Execute. var != null Object = "+choice);
								rightView.setViewportView(var);
							}
						}
						catch(IntolerableException ie) {
							UniversalTellUser.error(frame, ie.getMessage());
						}
					}
				}
			}); 
		}
		frame.pack();
		frame.setVisible(true);
	}	
	
	//RightExplorerSideComponent monitoredVariables[]
	
	public static MonitoredSimVariable getStandardVariable(MonitoredSimVariable monitoredVariables[], String name)   {
		if(monitoredVariables == null) {
			return null;
		}
		for(int i = 0 ; i < monitoredVariables.length ; i++) {
			String varname = monitoredVariables[i].getName();
			//System.out.println("Compare "+varname+" with "+name);
			if(monitoredVariables[i].getName().compareTo(name) == 0) {
				//System.out.println("Return VARIABLE");
				return monitoredVariables[i];
			}
		}
		//System.out.println("Return null");
		return null;
	}
	
	public static RightExplorerSideComponent getExternalVariable(RightExplorerSideComponent monitoredVariables[], String name)   {
		if(monitoredVariables == null) {
			return null;
		}
		for(int i = 0 ; i < monitoredVariables.length ; i++) {
			String varname = monitoredVariables[i].getVariableName();
			//System.out.println("Compare "+varname+" with "+name);
			if(varname.compareTo(name) == 0) {
				//System.out.println("Return VARIABLE");
				return monitoredVariables[i];
			}
		}
		//System.out.println("Return null");
		return null;
	}
	
	private static void performGuiappVariableUpdate(Manager m, GuiappVariable gav) throws IntolerableException {
		if(m == null) {
			throw new InternalProgrammingError(
					"No implementation to perform a GUI update of type "+gav.getClass().getName()+".\n"+
					"manager is null.");
		}
		if(gav == null) {
			throw new InternalProgrammingError(
					"No implementation to perform a GUI update of type "+gav.getClass().getName()+".\n"+
					"Guiapp variable is null.");
		}
		if(gav instanceof GuiappArithmeticVariable) {
			GuiappArithmeticVariable var = (GuiappArithmeticVariable) gav;
			ArithmeticEvaluable ae = var.getArithemeticEvaluable();
			m.reinitializeArithmetic(ae);
			return;
		}
		if(gav instanceof GuiappLogicalVariable) {
			GuiappLogicalVariable var = (GuiappLogicalVariable) gav;
			LogicalExpression le = var.getLogicalExpression();
			m.reinitializeLogical(le);
			return;
		}
		if(gav instanceof GuiappTable) {
			GuiappTable var = (GuiappTable) gav;
			InputStream is = var.getInputStream();
			m.reinitializeFromInputStream(is);
			return;
		}
		throw new InternalProgrammingError(
				"No implementation to perform a GUI variable update of type "+gav.getClass().getName()+".");
	}
	
	private static void performGuiappSpecialFieldUpdate(SimData sd, GuiappField gaf)  throws IntolerableException  {
		if(gaf instanceof GuiappStarttime) {
			GuiappStarttime gs = (GuiappStarttime) gaf;
			gs.performStarttimeUpdate(sd);
			return;
		}
		if(gaf instanceof GuiappEndtime) {
			GuiappEndtime ge = (GuiappEndtime) gaf;
			ge.performEndtimeUpdate(sd);
			return;
		}
		if(gaf instanceof GuiappTextarea) {
			return;
		}
		throw new InternalProgrammingError(
				"No implementation to perform a GUI special field update of type "+gaf.getClass().getName()+".");
	}

}
