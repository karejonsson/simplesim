package se.modlab.simplesim.guiembedd.structure;

import java.awt.Component;

import se.modlab.simplesim.algorithms.setup.SimData;

public interface GuiappField {

	public Component getComponent();
	
	public String getName();
	
	public String getHelptext();
	
}
