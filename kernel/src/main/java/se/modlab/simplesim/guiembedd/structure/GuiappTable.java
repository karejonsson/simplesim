package se.modlab.simplesim.guiembedd.structure;

import java.awt.Component;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Date;

import javax.swing.AbstractAction;

import se.modlab.generics.bshro.ifc.HierarchyObject;
import se.modlab.generics.exceptions.UserRuntimeError;
import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.exceptions.MotherCatalogue;
import se.modlab.generics.sstruct.comparisons.VariableLookup;
import se.modlab.generics.sstruct.tables.TabParser;
import se.modlab.generics.sstruct.tables.BooleanHolder;
import se.modlab.generics.sstruct.tables.DatetimeHolder;
import se.modlab.generics.sstruct.tables.DoubleHolder;
import se.modlab.generics.sstruct.tables.Holder;
import se.modlab.generics.sstruct.tables.LongHolder;
import se.modlab.generics.sstruct.tables.StringHolder;
import se.modlab.generics.sstruct.tables.TableHolder;
import se.modlab.simplesim.tables.OpenSimplesimTabfileAction;
import se.modlab.simplesim.tables.SaveAsSimplesimTabfileAction;
import se.modlab.simplesim.tables.SpreadsheetWindow_excel;
import se.modlab.tables.editor.koders.Cell;
import se.modlab.tables.editor.koders.JSpreadsheet;

public class GuiappTable extends GuiappVariable {
	
	private String filename;
	private TableHolder theTable = null;
	private JSpreadsheet ss = null;
	private SpreadsheetWindow_excel excel = null;
	private Component comp = null;
	private String columns[] = null;
	
	public GuiappTable(VariableLookup _vl, String _filename, String _columns[], String _optionalname, String _helptext) throws IntolerableException {
		super(_vl, _optionalname, _helptext);
		filename = _filename;
		columns = _columns;
		theTable = TabParser.parseFromFilename(filename);
	    ss = new JSpreadsheet(theTable.getNoRows()+1, theTable.getNoColumns());
	    int offset = 0;
	    if(columns != null) {
	    	offset = 1;
		    for(int col = 0 ; col < Math.min(theTable.getNoColumns(), columns.length) ; col ++) {
		    	ss.setValueAt(columns[col], 0, col);
		    } 
	    }
	    for(int row = 0 ; row < theTable.getNoRows() ; row ++) {
		    for(int col = 0 ; col < theTable.getNoColumns() ; col ++) {
		    	Holder h = theTable.getValue(row, col);
		    	Object o = null;
		    	if(h instanceof BooleanHolder) {
		    		o = new Boolean(((BooleanHolder) h).getBoolean());
		    	}
		    	if(h instanceof LongHolder) {
		    		o = new Long(((LongHolder) h).getLong());
		    	}
		    	if(h instanceof DoubleHolder) {
		    		o = new Double(((DoubleHolder) h).getDouble());
		    	}
		    	if(h instanceof DatetimeHolder) {
		    		o = new Date(((DatetimeHolder) h).getDatetime());
		    	}
		    	if(h instanceof StringHolder) {
		    		o = new String(((StringHolder) h).getString());
		    	}
		    	if(o == null) {
		    		o = "Type "+h.getClass().getName();
		    	}
		    	ss.setValueAt(o, row+offset, col);
		    }
	    }
	}
	
	public Component getComponent() {
		if(comp != null) {
			return comp;
		}
		if(excel == null) {
			excel = new SpreadsheetWindow_excel(ss);
		}
		comp = excel.getInteractiveComponent(
				ss, 
				new AbstractAction[] {
						new OpenTabfileWithColumnHandling(excel, ss, HierarchyObject.getReferenceFilePath(), columns),
						new SaveAsTabfileWithColumnHandling(excel, ss, HierarchyObject.getReferenceFilePath(), ',', columns),
						new SaveAsTabfileWithColumnHandling(excel, ss, HierarchyObject.getReferenceFilePath(), ';', columns),
						} );
		return comp;
	}
	
	public InputStream getInputStream() throws IntolerableException {
	    int offset = (columns != null) ? 1 : 0;
	    StringBuffer sb = new StringBuffer();
	    for(int row = 0 ; row < (ss.getRowCount() - offset) ; row ++) {
		    for(int col = 0 ; col < ss.getColumnCount() ; col ++) {
		    	Cell c = ss.getCellAt(row + offset, col);
		    	int type = c.getType();
		    	String out = "";
		    	if(type == Cell.FORMULA) {
		    		throw new UserRuntimeError("You cannot use formulaes in table editing.\n"+
		    				"See row "+(row + offset)+", column "+ col);
		    	}
		    	if(type == Cell.NUMBER) {
		    		sb.append(""+Double.parseDouble(c.getValue().toString())).toString();
		    	}
		    	if(type == Cell.TEXT) {
		    		if(c.getValue().toString().toUpperCase().contains("TRUE")) {
		    			sb.append("true");
		    		}
		    		else {
		    			sb.append("false");
		    		}
		    	}
		    	sb.append(" ");
		    }
		    sb.append("\n");
	    }
	    String tabfile = sb.toString();
	    //System.out.println("Tabfile:\n"+tabfile);
		return new ByteArrayInputStream(tabfile.getBytes());
	}
	
	private class OpenTabfileWithColumnHandling extends OpenSimplesimTabfileAction {

		private String columnNames[] = null;

		public OpenTabfileWithColumnHandling(Component _c, JSpreadsheet _ss,
				String _catalog, String _columnNames[]) {
			super(_c, _ss, _catalog);
			columnNames = _columnNames;
		}

		protected void updatetableModel(TableHolder t) {
			int rows = t.getNoRows();
			int columns = t.getNoColumns();
			int offset = 0;

			if(columnNames != null) {
				offset = 1;
			}
			ss.newTableModel(rows+offset, columns);
			if(columnNames != null) {
				for(int i = 0 ; i < Math.min(columns, columnNames.length) ; i++) 
				{
					ss.setValueAt(columnNames[i], 0, i);
				}
			}

			for(int i = 0 ; i < rows ; i++)
			{
				for(int ii = 0 ; ii < columns ; ii++) 
				{
					ss.setValueAt(getObject(t.getValue(i, ii)), i+offset, ii);
				}
			}	  
		}


	}
//SaveAsSimplesimTabfileAction

	private class SaveAsTabfileWithColumnHandling extends SaveAsSimplesimTabfileAction {

		private String columnNames[] = null;

		public SaveAsTabfileWithColumnHandling(Component _c, JSpreadsheet _ss,
				String _catalog, char delimiter, String _columnNames[]) {
			super(_c, _ss, _catalog, delimiter);
			columnNames = _columnNames;
		}

		protected StringBuffer getBuffer(int startRow) {
			return (columnNames == null) ? super.getBuffer(0) : super.getBuffer(1);
		}


	}

}
