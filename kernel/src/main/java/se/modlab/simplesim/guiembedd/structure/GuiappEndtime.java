package se.modlab.simplesim.guiembedd.structure;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.JTextField;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import se.modlab.generics.exceptions.UserCompiletimeError;
import se.modlab.generics.exceptions.UserRuntimeError;
import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.sstruct.comparisons.ArithmeticEvaluable;
import se.modlab.generics.sstruct.comparisons.VariableLookup;
import se.modlab.generics.sstruct.evaluables.ArithmeticEvaluableDouble;
import se.modlab.generics.sstruct.evaluables.ArithmeticEvaluableLong;
import se.modlab.generics.sstruct.values.sValue;
import se.modlab.simplesim.algorithms.setup.SimData;
import se.modlab.simplesim.guiembedd.parse.GuiappParser;
import se.modlab.simplesim.variables.Commons;

public class GuiappEndtime implements GuiappField {

	private String endtime = null;
	private String name = null;
	private String tip = null;
	protected JTextField tf = null;
	protected boolean intact;

	public GuiappEndtime(String _endtime, String _name, String _tip) {
		endtime = _endtime;
		name = _name;
		tip = _tip;
	}

	public Component getComponent() {
		if(tf == null) {
			tf = new JTextField();
			tf.addActionListener(
					new ActionListener() {

						public void actionPerformed(ActionEvent arg0) {
							invalidateTextfield("action performed");
						}
					}
					);
			tf.addCaretListener(
					new CaretListener() {

						public void caretUpdate(CaretEvent arg0) {
							invalidateTextfield("caret update");
						}
					}
					);
			tf.setText(endtime);
		}
		return tf; 
	}

	public String getHelptext() {
		return tip;
	}

	public String getName() {
		return name;
	}

	public ArithmeticEvaluable getArithemeticEvaluable() throws IntolerableException{
		String text = tf.getText();
		if(text == null) {
			return new ArithmeticEvaluableLong(0);
		}
		if(text.trim().length() == 0) {
			return new ArithmeticEvaluableLong(0);
		}
		ArithmeticEvaluable new_ae = null;
		try {
			new_ae = GuiappParser.parseArithemticExpressionFromString(text, "GUI application text field");
		}
		catch(UserRuntimeError ue) {
			String errortext = ue.getMessage()+"\n"+"Happened to field for "+getName()+".";
			UserRuntimeError ue2 = new UserRuntimeError(errortext, ue.getCause(), ue.getCollectors());
			ue2.setAction(ue.getAction());
			UniversalTellUser.general(
					tf, 
					ue,
					"Unparseable expression"
					);
			return null;
		}
		catch(UserCompiletimeError ue) {
			String errortext = ue.getMessage()+"\n"+"Happened to field for "+getName()+".";
			UserCompiletimeError ue2 = new UserCompiletimeError(errortext, ue.getCause(), ue.getCollectors());
			ue2.setAction(ue.getAction());
			UniversalTellUser.general(
					tf, 
					ue,
					"Unparseable expression"
					);
			return null;
		}
		return new_ae;
	}


	public void performEndtimeUpdate(SimData sd) throws IntolerableException {
		String tmp = tf.getText();
		if((tmp == null) || (tmp.trim().length() == 0)) {
			//System.out.println("Endtime <"+tmp+"> (null/tom)");
			long endtime = new Date().getTime();
			double simtime = sd.getCommons().getSimTimeFromEpochTime(endtime);
			//System.out.println("Endtime <"+tmp+"> (null/tom), simtime="+simtime+", endtime="+endtime);
			sd.setEndTimeAsArithmeticExpression(new ArithmeticEvaluableDouble(simtime));
			sd.setEndTimeAsDatetimeLiteral(Commons.getTimeStringFromLong(endtime));
			return;
		}
		int colons = tmp.indexOf("::");
		if(colons != -1) tmp = tmp.substring(0, colons-1);
		try
		{
			//System.out.println("Endtime <"+tmp+"> (datum)");
			long endtime = new Date(tmp).getTime();
			double simtime = sd.getCommons().getSimTimeFromEpochTime(endtime);
			//System.out.println("Endtime <"+tmp+"> (datum), simtime="+simtime+", endtime="+endtime);
			//System.out.println("getTimeLongFromString: Fick "+s+" svarar "+l);
			sd.setEndTimeAsArithmeticExpression(new ArithmeticEvaluableDouble(simtime));
			sd.setEndTimeAsDatetimeLiteral(tmp);
			return;
		}
		catch(IllegalArgumentException e)
		{
			// Datetime approach failed
			//System.out.println("Endtime as date failed");
		}
		ArithmeticEvaluable ae = getArithemeticEvaluable();
		//System.out.println("Endtime <"+tmp+"> (v�rde) ae="+ae.reproduceExpression());
		sd.setEndTimeAsArithmeticExpression(ae);
	}

	protected void invalidateTextfield(String event) {
		//System.out.println("invalidateTextfield "+event+" on "+getName());
		intact = false;
	}

}
