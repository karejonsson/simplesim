package se.modlab.simplesim.guiembedd.structure;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import se.modlab.generics.exceptions.UserCompiletimeError;
import se.modlab.generics.exceptions.UserRuntimeError;
import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.sstruct.comparisons.ArithmeticEvaluable;
import se.modlab.generics.sstruct.comparisons.VariableLookup;
import se.modlab.generics.sstruct.evaluables.ArithmeticEvaluableLong;
import se.modlab.generics.sstruct.evaluables.LogicalExpressionBoolean;
import se.modlab.generics.sstruct.logics.LogicalExpression;
import se.modlab.simplesim.guiembedd.parse.GuiappParser;

public class GuiappLogicalVariable extends GuiappVariable {
	
	private LogicalExpression le;
	protected JTextField tf = null;

	public GuiappLogicalVariable(VariableLookup vl, LogicalExpression _le, String optionalname, String helptext) {
		super(vl, optionalname, helptext);	
		le = _le;
	}

	public LogicalExpression getLogicalExpression() throws IntolerableException {
		if(intact) {
			return le;
		}
		String text = tf.getText();
		if(text.trim().length() == 0) {
			return new LogicalExpressionBoolean(true);
		}
		LogicalExpression new_le = null;
		try { 
			new_le = GuiappParser.parseLogicalExpressionFromString(text, "GUI application text field");
		}
		catch(UserRuntimeError ue) {
			String errortext = ue.getMessage()+"\n"+"Happened to field for variable "+getName()+".";
			UserRuntimeError ue2 = new UserRuntimeError(errortext, ue.getCause(), ue.getCollectors());
			ue2.setAction(ue.getAction());
			UniversalTellUser.general(
					tf, 
					ue,
					"Unparseable expression"
					);
			return null;
		}
		catch(UserCompiletimeError ue) {
			String errortext = ue.getMessage()+"\n"+"Happened to field for variable "+getName()+".";
			UserCompiletimeError ue2 = new UserCompiletimeError(errortext, ue.getCause(), ue.getCollectors());
			ue2.setAction(ue.getAction());
			UniversalTellUser.general(
					tf, 
					ue,
					"Unparseable expression"
					);
			return null;
		}
		return new_le;
	}
	
	private String getInitialText() {  
		return le.reproduceExpression();
	}

	public Component getComponent() {
		if(tf == null) {
			tf = new JTextField();
			tf.addActionListener(
					new ActionListener() {

						public void actionPerformed(ActionEvent arg0) {
							invalidateTextfield("action performed");
						}
					}
					);
			tf.addCaretListener(
					new CaretListener() {

						public void caretUpdate(CaretEvent arg0) {
							invalidateTextfield("caret update");
						}
					}
					);
			tf.setText(getInitialText());
		}
		return tf; 
	}

}
