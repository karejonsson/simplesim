package se.modlab.simplesim.guiembedd.structure;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.JTextField;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import se.modlab.generics.sstruct.comparisons.VariableLookup;

public abstract class GuiappVariable implements GuiappField  {
	
	private VariableLookup vl;
	private String optionalname;
	private String helptext;
	protected boolean intact;

	public GuiappVariable(VariableLookup _vl, String _optionalname, String _helptext) {
		vl = _vl;
		optionalname = _optionalname;
		helptext = _helptext;
	}
	
	public VariableLookup getVariableLookup() {
		return vl;
	}
	
	public String getOptionalname() {
		return optionalname;
	}
	
	public String getName() {
		if(optionalname != null) {
			return optionalname;
		}
		return vl.toString();
	}
	
	public String getHelptext() {
		return helptext;
	}
		
	public abstract Component getComponent();
	
	protected void invalidateTextfield(String event) {
		//System.out.println("invalidateTextfield "+event+" on "+getName());
		intact = false;
	}

}
