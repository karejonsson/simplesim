package se.modlab.simplesim.guiembedd.structure;

import java.util.Vector;

import se.modlab.generics.sstruct.comparisons.VariableLookup;

public class GuiappPresentation {
	
	private static class Couple {
		public VariableLookup vl = null;
		public String name = null;
		public String helptext;
		public Couple(VariableLookup _vl, String _name, String _helptext) {
			vl = _vl;
			name = _name;
			helptext = _helptext;
		}
	}
	
    private String framename = null;
    
    private Vector<Couple> couples = new Vector<Couple>();

	public void setFramename(String _framename) {
		framename =_framename;
	}

	public String getFramename() {
		return framename;
	}
	
	public void addName(VariableLookup vl, String name, String helptext) {
		couples.add(new Couple(vl, name, helptext));
	}
	
	public int getNoNames() {
		return couples.size();
	}
	
	public VariableLookup getVariableLookup(int i) {
		if(i < 0) {
			return null;
		}
		if(i >= couples.size()) {
			return null;
		}
		return couples.elementAt(i).vl;
	}
	
	public String getName(int i) {
		if(i < 0) {
			return null;
		}
		if(i >= couples.size()) {
			return null;
		}
		return couples.elementAt(i).name;
	}
	
	public String getHelptext(int i) {
		if(i < 0) {
			return null;
		}
		if(i >= couples.size()) {
			return null;
		}
		return couples.elementAt(i).helptext;
	}
	
	public String getFullyQualifyingName(int i) {
		if(i < 0) {
			return null;
		}
		if(i >= couples.size()) {
			return null;
		}
		return couples.elementAt(i).vl.reproduceExpression();
	}
	
}
