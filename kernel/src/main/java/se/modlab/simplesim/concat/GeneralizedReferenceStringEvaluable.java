package se.modlab.simplesim.concat;

import se.modlab.generics.sstruct.strings.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.simplesim.events.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.references.*;
import se.modlab.simplesim.evaluables.*;

public class GeneralizedReferenceStringEvaluable 
implements StringEvaluable
{

	private ReferenceEvaluable re;

	public GeneralizedReferenceStringEvaluable(ReferenceEvaluable _re)
	{
		re = _re;
	}

	public String evaluate(Scope s)
	throws IntolerableException
	{
		if(re instanceof ReferenceEvaluableVariableLookup)
		{
			ReferenceEvaluableVariableLookup revl = 
				(ReferenceEvaluableVariableLookup) re;
			VariableLookup vl = revl.getVariableReference();
			return getString(vl, s);
		}
		if(re instanceof ReferenceEvaluableGetFromBag)
		{
			ReferenceEvaluableGetFromBag revl = 
				(ReferenceEvaluableGetFromBag) re;
			VariableLookup vl = revl.getVariableReference();
			return getString(vl, s);
		}
		if(re instanceof ReferenceEvaluablePeekIntoBag)
		{
			ReferenceEvaluablePeekIntoBag revl = 
				(ReferenceEvaluablePeekIntoBag) re;
			VariableLookup vl = revl.getVariableReference();
			return getString(vl, s);
		}
		if(re instanceof ReferenceEvaluableNewStruct)
		{
			throw new UserRuntimeError(
					"You cannot evaluate a new struct of\n"+
					"type "+((ReferenceEvaluableNewStruct) re).getTypesName()+
			" to string");
		}
		if(re instanceof ReferenceEvaluableNewEnqueuable)
		{
			throw new UserRuntimeError(
					"You cannot evaluate a new queuable of\n"+
					"type "+((ReferenceEvaluableNewEnqueuable) re).getTypesName()+
			" to string");
		}
		throw new InternalError(
				"Class generalizedReferenceStringEvaluable is not \n"+
				"prepared for an instance of class \n"+re.getClass().getName());
	}

	private String getString(VariableLookup vl, Scope s)
	throws IntolerableException
	{
		VariableInstance vi = vl.getInstance(s);  
		if(vi instanceof StringVariable)
		{
			StringVariable svar = (StringVariable) vi;
			sString val = (sString) svar.getValue();
			return val.getString();
		}
		if(vi instanceof BooleanVariable)
		{
			BooleanVariable bvar = (BooleanVariable) vi;
			sBoolean sb = (sBoolean) bvar.getValue();
			boolean out = sb.getBoolean().booleanValue();
			if(out) return "true";
			return "false";
		}
		if(vi instanceof LongVariable)
		{
			LongVariable lvar = (LongVariable) vi;
			sLong sl = (sLong) lvar.getValue();
			return sl.getLong().toString();
		}
		if(vi instanceof DoubleVariable)
		{
			DoubleVariable dvar = (DoubleVariable) vi;
			sDouble sd = (sDouble) dvar.getValue();
			return sd.getDouble().toString();
		}
		if(vi instanceof Enqueuable)
		{
			Enqueuable evar = (Enqueuable) vi;
			return evar.getFullPathName();
		}
		if(vi instanceof Complex)
		{
			Complex evar = (Complex) vi;
			return evar.toString();
		}
		throw new UserRuntimeError(
				"You cannot evaluate "+vl+"\n"+
				" of type "+vi.getType().getTypesName()+" to string.");
	}

	public void verify(Scope s) throws IntolerableException {
		if(re instanceof ReferenceEvaluableVariableLookup)
		{
			ReferenceEvaluableVariableLookup revl = 
				(ReferenceEvaluableVariableLookup) re;
			VariableLookup vl = revl.getVariableReference();
			getString(vl, s);
			return;
		}
		if(re instanceof ReferenceEvaluableGetFromBag)
		{
			ReferenceEvaluableGetFromBag revl = 
				(ReferenceEvaluableGetFromBag) re;
			VariableLookup vl = revl.getVariableReference();
			getString(vl, s);
			return;
		}
		if(re instanceof ReferenceEvaluablePeekIntoBag)
		{
			ReferenceEvaluablePeekIntoBag revl = 
				(ReferenceEvaluablePeekIntoBag) re;
			VariableLookup vl = revl.getVariableReference();
			getString(vl, s);
			return;
		}
		if(re instanceof ReferenceEvaluableNewStruct)
		{
			throw new UserRuntimeError(
					"You cannot evaluate a new struct of\n"+
					"type "+((ReferenceEvaluableNewStruct) re).getTypesName()+
			" to string");
		}
		if(re instanceof ReferenceEvaluableNewEnqueuable)
		{
			throw new UserRuntimeError(
					"You cannot evaluate a new queuable of\n"+
					"type "+((ReferenceEvaluableNewEnqueuable) re).getTypesName()+
			" to string");
		}
		throw new InternalError(
				"Class generalizedReferenceStringEvaluable is not \n"+
				"prepared for an instance of class \n"+re.getClass().getName());
	}
	
	public String reproduceExpression() {
		return re.reproduceExpression();
	}


}