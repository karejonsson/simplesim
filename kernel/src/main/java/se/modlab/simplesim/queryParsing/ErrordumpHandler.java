package se.modlab.simplesim.queryParsing;

import java.io.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.files.*;
import se.modlab.generics.gui.exceptions.UniversalTellUser;

public class ErrordumpHandler
{

  public static final float SCREEN_WIDTH = (int) Toolkit.getDefaultToolkit().getScreenSize().width;
  public static final float SCREEN_HEIGHT = (int) Toolkit.getDefaultToolkit().getScreenSize().height-35;

  private String catalogue;
  private String comment;
  private String s_filename[] = null;
  private String s_filecontents[] = null;
  private Hashtable<String, String> names = new Hashtable<String, String>();
  private StringBuffer errormessages = null;

  public ErrordumpHandler(String _catalogue)
  {
    this.catalogue = _catalogue;
  }

  private void parse_filename(String filename)
    throws IntolerableException
  {
    try
    {
      FileCollector fc = new FileCollector(filename);
      parse_text(fc.getFilecontents());
      if(!fc.getOk()) throw fc.getException();
    }
    catch(IntolerableException ie)
    {
      throw ie;
    }
    catch(FileNotFoundException fnfe)
    {
      throw new SystemError("File "+filename+" not found", fnfe);
    }
    catch(IOException ioe)
    {
      throw new SystemError("Encountered error when accessing file\n"
                            +filename, ioe);
    }
    catch(Throwable t)
    {
      throw new UnclassedError("errorhandlerHandler - parse_filename", t);
    }
  }

  private static int[] indexesOf(String text, String seq)
  {
    Vector<Integer> vec = new Vector<Integer>();
    int idx = 0;
    do
    {
      idx = text.indexOf(seq, idx+1);
      if(idx != -1) vec.addElement(new Integer(idx));
    } 
    while(idx != -1);
    int out[] = new int[vec.size()];
    for(int i = 0 ; i < vec.size() ; i++)
    {
      out[i] = ((Integer) vec.elementAt(i)).intValue();
    }
    return out;
  }

  private static String bareFilename(String fullFilename)
  {
    if(fullFilename == null) return "Null name";
    int fslash = fullFilename.lastIndexOf('/');
    int bslash = fullFilename.lastIndexOf('\\');
    int placeSeparator = Math.max(fslash, bslash);
    if(placeSeparator == -1) return fullFilename;
    return fullFilename.substring(placeSeparator+1);
  }

  private void parse_text(String text)
    throws IntolerableException
  {
    int commentStart = text.indexOf("<COMMENT>");
    if(commentStart == -1)
    {
      throw new UserCompiletimeError("Comments start not identified.");
    }
    int commentEnd = text.indexOf("</COMMENT>");
    if(commentStart == -1)
    {
      throw new UserCompiletimeError("Comments end not identified.");
    }
    if(commentStart >= commentEnd)
    {
      throw new UserCompiletimeError("Comment not correctly identified.");
    }
    comment = text.substring(commentStart+9, commentEnd).trim();
    //universalTellUser.info(null, "Comment = \n"+comment);
    int file[] = indexesOf(text, "<FILE>");
    int efile[] = indexesOf(text, "</FILE>");
    int filename[] = indexesOf(text, "<FILENAME>");
    int efilename[] = indexesOf(text, "</FILENAME>");
    int filecontents[] = indexesOf(text, "<FILECONTENTS>");
    int efilecontents[] = indexesOf(text, "</FILECONTENTS>");
    if(file.length != efile.length)
    {
      throw new UserCompiletimeError("Balance of <FILE>...</FILE> incorrect.");
    }
    if(file.length != filename.length)
    {
      throw new UserCompiletimeError("Number of <FILE> "+file.length+", number of <FILENAME> "+filename.length);
    }
    if(file.length != efilename.length)
    {
      throw new UserCompiletimeError("Number of <FILE> "+file.length+", number of </FILENAME> "+efilename.length);
    }
    if(file.length != filecontents.length)
    {
      throw new UserCompiletimeError("Number of <FILE> "+file.length+", number of <FILECONTENTS> "+filecontents.length);
    }
    if(file.length != efilecontents.length)
    {
      throw new UserCompiletimeError("Number of <FILE> "+file.length+", number of </FILECONTENTS> "+efilecontents.length);
    }
    s_filename = new String[file.length];
    s_filecontents = new String[file.length];
    for(int i = 0 ; i < file.length ; i++)
    {
      if(file[i] > efile[i])
      {  
        throw new UserCompiletimeError("<FILE>...</FILE> number "+i+" incorrect.");
      }
      if(file[i] >= filename[i])
      {  
        throw new UserCompiletimeError("<FILE><FILENAME>...</FILE> number "+i+" incorrect.");
      }
      if(filename[i] >= efilename[i])
      {  
        throw new UserCompiletimeError("<FILENAME>...</FILENAME> number "+i+" incorrect.");
      }
      if(efilename[i] >= filecontents[i])
      {  
        throw new UserCompiletimeError("</FILENAME>...<FILECONTENTS> number "+i+" incorrect.");
      }
      if(filecontents[i] >= efilecontents[i])
      {  
        throw new UserCompiletimeError("<FILECONTENTS>...</FILECONTENTS> number "+i+" incorrect.");
      }
      if(efilecontents[i] >= efile[i])
      {  
        throw new UserCompiletimeError("</FILECONTENTS>...</FILE> number "+i+" incorrect.");
      }
      s_filename[i] = text.substring(filename[i]+10, efilename[i]).trim();
      String oldFullname = (String) names.put(bareFilename(s_filename[i]), s_filename[i]);
      if(oldFullname != null)
      {
        if(oldFullname.compareTo(s_filename[i]) != 0)
        { 
          // The names are the same and the catalogs are differrent. Thus a true problem.
          if(errormessages == null) 
          {
            errormessages = new StringBuffer();
          }
          errormessages.append(
            "Multiple use of filename '"+bareFilename(s_filename[i])+"'.\n"+
            "The following two are used:\n\n"+oldFullname+"\n"+s_filename[i]);
        }
      }
      s_filecontents[i] = text.substring(filecontents[i]+14, efilecontents[i]).trim();
      //universalTellUser.info(null, s_filename[i]+"\n\n"+s_filecontents[i]);
    }
    if(errormessages != null) 
    { 
      errormessages.append(
        "\n\nPlease note that the files in this errordump will be put in the same catalog\n"+
        "and therefore the user will in the case of relative path (i.e. contains ..)\n"+
        "have to edit the .sim-file so the other files are accessed correctly."); 
    }
  }

  public void verify_filename(String filename)
    throws IntolerableException
  {
    parse_filename(filename);
    if(errormessages != null) 
    {
      throw new UserCompiletimeError(
        "The syntax was correct but the following error messages\n"+
        "were registerred:\n\n"+errormessages.toString());
    }
  }

  public void verify_text(String text)
    throws IntolerableException
  {
    parse_text(text);
    if(errormessages != null) 
    {
      throw new UserCompiletimeError(
        "The syntax was correct but the following error messages\n"+
        "were registerred:\n\n"+errormessages.toString());
    }
  }

  public void analyse_doGraphics()
    throws IntolerableException
  {
    final JFrame frame = new JFrame("Files from errordump");
    JTabbedPane tp = new JTabbedPane();
    frame.getContentPane().add(tp);
    JTextArea ta = new JTextArea();
    ta.setText(comment);
    ta.setEditable(false);
    //JScrollPane sp = null;//new JScrollPane(ta);
    tp.add("comment", ta);
    for(int i = 0 ; i < s_filename.length ; i++)
    {
      ta = new JTextArea();
      ta.setText(s_filecontents[i]);
      ta.setEditable(false);
      //sp = new JScrollPane(ta);
      tp.add(bareFilename(s_filename[i]), ta);
    }
    JMenuBar mb = new JMenuBar();
    frame.setJMenuBar(mb);
    JMenu menu = new JMenu("File");
    mb.add(menu);
    JMenuItem item = new JMenuItem("Logical path");
    menu.add(item);
    item.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          UniversalTellUser.info(frame, "Path is:\n\n"+catalogue);
        }
      }
    ); 
    item = new JMenuItem("Extract");
    menu.add(item);
    item.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          try
          {
            ErrordumpHandler.this.extract_doer();
          }
          catch(IntolerableException ie)
          {
            UniversalTellUser.general(frame, ie);
          }
        }
      }
    ); 
    item = new JMenuItem("Exit");
    menu.add(item);
    item.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          frame.dispose();
        }
      }
    ); 
    frame.pack();
    frame.setLocation(new Point(((int) (SCREEN_WIDTH/3)), 
                                0));
    frame.setSize(new Dimension(((int) (SCREEN_WIDTH*2/3)), 
                                ((int) (SCREEN_HEIGHT))));
    frame.setVisible(true);
  }

  public void analyse_filename(String filename)
    throws IntolerableException
  {
    verify_filename(filename);
    analyse_doGraphics();
  }

  public void analyse_text(String text)
    throws IntolerableException
  {
    verify_text(text);
    analyse_doGraphics();
  }

  private void extract_doer()
    throws IntolerableException
  {
    StringBuffer sb = new StringBuffer();
    String mostCurrentFilename = null;
    try
    {
      for(int i = 0 ; i < s_filename.length ; i++)
      {
        mostCurrentFilename = catalogue+bareFilename(s_filename[i]);
        FileWriter fw = new FileWriter(catalogue+bareFilename(s_filename[i]));
        fw.write(s_filecontents[i]+"\n");
        fw.close();
        sb.append(mostCurrentFilename+"\n");
      }
    }
    catch(FileNotFoundException fnfe)
    {
      throw new SystemError("File "+mostCurrentFilename+" not writable", fnfe);
    }
    catch(IOException ioe)
    {
      throw new SystemError("Encountered error when writing file\n"
                            +mostCurrentFilename, ioe);
    }
    catch(Throwable t)
    {
      throw new UnclassedError("errordumpHandler - extract_filename", t);
    }
    UniversalTellUser.info(null, "The following files were created:\n\n"+
                           sb.toString());
  }

  public void extract_filename(String filename)
    throws IntolerableException
  {
    verify_filename(filename);
    extract_doer();
  }

  public void extract_text(String text)
    throws IntolerableException
  {
    verify_text(text);
    extract_doer();
  }

}
