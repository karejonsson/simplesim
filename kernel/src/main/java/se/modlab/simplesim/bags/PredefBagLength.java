package se.modlab.simplesim.bags;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.evaluables.*;
import se.modlab.generics.sstruct.predefs.*;
import java.util.*;

public class PredefBagLength extends PredefLength
{

  public PredefBagLength(ArithmeticEvaluable _ae)
  {
    super(_ae);
  }

  public sValue evaluate(Scope s)
    throws IntolerableException
  {
    if(!(ae instanceof ArithmeticEvaluableVariable))
    {
      throw new UserRuntimeError(
        "Expression within lengths parenthesises\n"+
        "must evaluate to a vector or bag.");
    }
    VariableLookup _vl = ((ArithmeticEvaluableVariable) ae).getVariableReference();
    VariableInstance sv = _vl.getInstance(s);
    if(sv instanceof VariableVector)
    {
      VariableVector svv = (VariableVector) sv;
      long l = svv.getLength();
      return new sLong(l);
    }
    if(sv instanceof VariableVectorFromFile)
    {
      VariableVectorFromFile svvff = (VariableVectorFromFile) sv;
      long l = svvff.getLength();
      return new sLong(l);
    }
    if(sv instanceof Bag)
    {
      Bag b = (Bag) sv;
      long l = b.length();
      return new sLong(l);
    }
    if(sv != null)
    {
      //System.out.println("predefBagLength - type = "+
      //  sv.getClass().getName());
    }
    else
    {
      //System.out.println("predefBagLength - sv = null");
    }
      throw new UserCompiletimeError(
        "Expression within lengths parenthesises\n"+
        "must evaluate to a vector or bag.");
    //return null;
  }
  
	public void verify(Scope s) throws IntolerableException {
	    if(!(ae instanceof ArithmeticEvaluableVariable))
	    {
	      throw new UserRuntimeError(
	        "Expression within lengths parenthesises\n"+
	        "must evaluate to a vector or bag.");
	    }
	    VariableLookup _vl = ((ArithmeticEvaluableVariable) ae).getVariableReference();
	    VariableInstance sv = _vl.getInstance(s);
	    if(sv instanceof VariableVector)
	    {
	      VariableVector svv = (VariableVector) sv;
	      long l = svv.getLength();
	      return;
	    }
	    if(sv instanceof VariableVectorFromFile)
	    {
	      VariableVectorFromFile svvff = (VariableVectorFromFile) sv;
	      long l = svvff.getLength();
	      return;
	    }
	    if(sv instanceof Bag)
	    {
	      Bag b = (Bag) sv;
	      long l = b.length();
	      return;
	    }
	    if(sv != null)
	    {
	      //System.out.println("predefBagLength - type = "+
	      //  sv.getClass().getName());
	    }
	    else
	    {
	      //System.out.println("predefBagLength - sv = null");
	    }
	      throw new UserCompiletimeError(
	        "Expression within lengths parenthesises\n"+
	        "must evaluate to a vector or bag.");
	    //return null;
	}
  
}