package se.modlab.simplesim.bags;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.values.*;

public class BooleanLiloBag extends BooleanBag
{

  public BooleanLiloBag(String name, VariableType vt, String _filename, int _line, int _column)
  {
    super(name, vt, _filename, _line, _column);
  }

  public sValue get()
		    throws IntolerableException
		  {
		    if(theBag.size() == 0)
		    {
		      throw new UserRuntimeError(
		        "The bag "+getFullPathName()+", defined "+this.getPlace()+" was operated on with get(bag)\n"+
		        "when it was empty!");
		    }
		    return (sValue) theBag.remove(0);
		  }
		  
	public sValue get(int idx)
			throws IntolerableException
			{
		if(theBag.size() == 0)
		{
			throw new UserRuntimeError(
					"The bag "+getFullPathName()+", defined "+this.getPlace()+" was operated on with get(bag, "+idx+")\n"+
					"when it was empty!");
		}
		if(theBag.size() - idx < 0)
		{
			throw new UserRuntimeError(
					"The bag "+getFullPathName()+", defined "+this.getPlace()+" was operated on with get(bag, "+idx+")\n"+
					"when there where "+theBag.size()+" elements!");
		}
		return (sValue) theBag.remove(idx);
			}
		  
  public sValue peek()
    throws IntolerableException
  {
    if(theBag.size() == 0)
    {
      throw new UserRuntimeError(
        "The bag "+getFullPathName()+", defined "+this.getPlace()+" was operated on with peek(bag)\n"+
        "when it was empty!");
    }
    return (sValue) theBag.elementAt(0);
  }
  
	public sValue peek(int idx)
			throws IntolerableException
			{
		if(theBag.size() == 0)
		{
			throw new UserRuntimeError(
					"The bag "+getFullPathName()+", defined "+this.getPlace()+" was operated on with peek(bag, "+idx+")\n"+
					"when it was empty!");
		}
		if(theBag.size() - idx < 0)
		{
			throw new UserRuntimeError(
					"The bag "+getFullPathName()+", defined "+this.getPlace()+" was operated on with peek(bag, "+idx+")\n"+
					"when there where "+theBag.size()+" elements!");
		}
		return (sValue) theBag.elementAt(idx);
			}

  public void put(sValue v)
    throws IntolerableException
  {
    if(!(v instanceof sBoolean))
    {
      throw new InternalError(
        "Bag "+getFullPathName()+", defined "+this.getPlace()+" recieves non boolean value "+v);
    }
    theBag.addElement(v);
  }

  public String toString()
  {
    return super.toString()+" oldest";
  }
	public boolean valueEquals(VariableInstance sv) throws IntolerableException {
		throw new InternalError("Simplesim has no implementation for comparing bags");
	}

}