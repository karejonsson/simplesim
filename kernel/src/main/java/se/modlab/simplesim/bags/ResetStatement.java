package se.modlab.simplesim.bags;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.comparisons.*;

import java.util.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.executables.*;

public class ResetStatement extends ProgramStatement
{

  protected VariableLookup vl;
  protected String filename;
  protected int line;
  protected int column;

  public ResetStatement(VariableLookup _vl, String _filename, int _line, int _column)
  {
    vl = _vl;
    filename = _filename;
    line = _line;
    column = _column;
  }

	public void execute(Scope s) 
	throws ReturnException, 
	IntolerableException, 
	StopException,
	BreakException,
	ContinueException
	{
		try {
			//System.out.println("doublePutStatement - execute: Bag = "+vl);
			VariableInstance vi = vl.getInstance(s);
			if(!(vi instanceof Bag))
			{
				throw new InternalError( 
						"Reference "+vl.reproduceExpression()+" defined at file "+vl.getFilename()+", line "+vl.getLine()+", column "+vl.getColumn()+"\n"+
				"is not a bag.");
			}
			Bag b = (Bag) vi;
			b.reset();
		}
		catch(UserRuntimeError ue) {
			String m = ue.getMessage()+"\n"+"This happened "+getPlace();
			UserRuntimeError ue2 = new UserRuntimeError(m, ue.getCause(), ue.getCollectors());
			ue2.setAction(ue.getAction());
			throw ue2;
		}
		catch(UserCompiletimeError ue) {
			String m = ue.getMessage()+"\n"+"This happened "+getPlace();
			UserCompiletimeError ue2 = new UserCompiletimeError(m, ue.getCause(), ue.getCollectors());
			ue2.setAction(ue.getAction());
			throw ue2;
		}
	}
  
  public String getPlace() {
	  return "in file "+filename+", line "+line+", column "+column;
  }

	public void verify(Scope s) throws IntolerableException {
		VariableInstance vi = vl.getInstance(s);
		if(!(vi instanceof Bag))
		{
			throw new InternalError( 
					"Reference "+vl.reproduceExpression()+" defined at file "+vl.getFilename()+", line "+vl.getLine()+", column "+vl.getColumn()+"\n"+
			"is not a bag.");
		}
	}
      
}
