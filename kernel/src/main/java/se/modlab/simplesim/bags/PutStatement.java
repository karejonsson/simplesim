package se.modlab.simplesim.bags;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.comparisons.*;
import java.util.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.executables.*;

public abstract class PutStatement 
                extends ProgramStatement
{

  protected VariableLookup vl;
  protected String filename;
  protected int line;
  protected int column;

  public PutStatement(VariableLookup _vl, String _filename, int _line, int _column)
  {
    vl = _vl;
    filename = _filename;
    line = _line;
    column = _column;
  }

  public abstract void execute(Scope s) 
    throws ReturnException, 
           IntolerableException, 
           StopException,
           BreakException,
           ContinueException;
  
  public String getPlace() {
	  return "in file "+filename+", line "+line+", column "+column;
  }
      
}
