package se.modlab.simplesim.bags;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.comparisons.*;

import java.util.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.generics.sstruct.logics.*;

public class BooleanPutStatement extends PutStatement
{

	protected LogicalExpression le;

	public BooleanPutStatement(VariableLookup _vl, LogicalExpression _le, String _filename, int _line, int _column)
	{
		super(_vl, _filename, _line, _column);
		le = _le;
	}

	public void execute(Scope s) 
	throws ReturnException, 
	IntolerableException, 
	StopException,
	BreakException,
	ContinueException
	{
		try {
			//System.out.println("longPutStatement - execute: Bag = "+vl);
			VariableInstance vi = vl.getInstance(s);
			if(!(vi instanceof BooleanBag))
			{
				throw new InternalError( 
						"Bag should have been of boolean type but was of class\n"+
						vi.getClass().getName()+". This should have been verified on\n"+
				"parsing! (se.xyz.simulation.bags.booleanPutStatement)");
			}
			BooleanBag lb = (BooleanBag) vi;
			boolean b = le.evaluate(s);
			//System.out.println("Puts "+v+" in bag "+ab);
			lb.put(new sBoolean(b));
		}
		catch(UserRuntimeError ue) {
			String m = ue.getMessage()+"\n"+"This happened "+getPlace();
			UserRuntimeError ue2 = new UserRuntimeError(m, ue.getCause(), ue.getCollectors());
			ue2.setAction(ue.getAction());
			throw ue2;
		}
		catch(UserCompiletimeError ue) {
			String m = ue.getMessage()+"\n"+"This happened "+getPlace();
			UserCompiletimeError ue2 = new UserCompiletimeError(m, ue.getCause(), ue.getCollectors());
			ue2.setAction(ue.getAction());
			throw ue2;
		}
	}

	public void verify(Scope s) throws IntolerableException {
		try {
			//System.out.println("longPutStatement - execute: Bag = "+vl);
			VariableInstance vi = vl.getInstance(s);
			if(!(vi instanceof BooleanBag))
			{
				throw new InternalError( 
						"Bag should have been of boolean type but was of class\n"+
						vi.getClass().getName()+". This should have been verified on\n"+
				"parsing! (se.xyz.simulation.bags.booleanPutStatement)");
			}
			BooleanBag lb = (BooleanBag) vi;
			le.verify(s);
			//System.out.println("Puts "+v+" in bag "+ab);
			lb.put(new sBoolean(true));
		}
		catch(UserRuntimeError ue) {
			String m = ue.getMessage()+"\n"+"This happened "+getPlace();
			UserRuntimeError ue2 = new UserRuntimeError(m, ue.getCause(), ue.getCollectors());
			ue2.setAction(ue.getAction());
			throw ue2;
		}
		catch(UserCompiletimeError ue) {
			String m = ue.getMessage()+"\n"+"This happened "+getPlace();
			UserCompiletimeError ue2 = new UserCompiletimeError(m, ue.getCause(), ue.getCollectors());
			ue2.setAction(ue.getAction());
			throw ue2;
		}
	}

}
