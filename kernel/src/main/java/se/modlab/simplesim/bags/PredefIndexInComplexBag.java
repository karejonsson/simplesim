package se.modlab.simplesim.bags;

import se.modlab.generics.exceptions.UserRuntimeError;
import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.sstruct.comparisons.Scope;
import se.modlab.generics.sstruct.comparisons.VariableLookup;
import se.modlab.generics.sstruct.values.sLong;
import se.modlab.generics.sstruct.values.sValue;
import se.modlab.generics.sstruct.variables.VariableInstance;
import se.modlab.simplesim.evaluables.ReferenceEvaluableVariableLookup;

public class PredefIndexInComplexBag extends PredefIndexInBag {
	
	private ReferenceEvaluableVariableLookup value = null;

	public PredefIndexInComplexBag(VariableLookup bag, String filename, int line, int column, ReferenceEvaluableVariableLookup value) {
		super(bag, filename, line, column);
		this.value = value;
	}

	public sValue evaluate(Scope s) throws IntolerableException {
	    VariableInstance vi = vl.getInstance(s);
	    if(!(vi instanceof ComplexBag)) {
	    	throw new UserRuntimeError("Reference "+vl+" was expected to refer to a complex worth bag but was not. Happened on "+getPlace());
	    }
	    ComplexBag cb = (ComplexBag) vi;
	    sValue val = value.evaluate(s);
		return new sLong(cb.index(val));
	}

	public String reproduceExpression() {
		return "index("+vl.reproduceExpression()+", "+value.reproduceExpression()+")";
	}

	public void verify(Scope s) throws IntolerableException {
	    VariableInstance vi = vl.getInstance(s);
	    if(!(vi instanceof ComplexBag)) {
	    	throw new UserRuntimeError("Reference "+vl+" was expected to refer to a complex worth bag but was not. Happened on "+getPlace());
	    }
	}

}
