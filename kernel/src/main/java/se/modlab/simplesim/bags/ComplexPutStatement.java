package se.modlab.simplesim.bags;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.comparisons.*;

import java.util.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.simplesim.references.*;
import se.modlab.simplesim.events.*;

public class ComplexPutStatement extends PutStatement
{

	protected ReferenceEvaluable re;

	public ComplexPutStatement(VariableLookup _vl, ReferenceEvaluable _re, String _filename, int _line, int _column)
	{
		super(_vl, _filename, _line, _column);
		re = _re;
	}

	public void execute(Scope s) 
	throws ReturnException, 
	IntolerableException, 
	StopException,
	BreakException,
	ContinueException
	{
		try {
			VariableInstance vi = vl.getInstance(s);
			if(!(vi instanceof ComplexBag))
			{
				throw new InternalError( 
						"Bag should have been of complex type but was of class\n"+
						vi.getClass().getName()+". This should have been verified on\n"+
				"parsing! (se.xyz.simulation.bags.complexPutStatement)");
			}
			ComplexBag db = (ComplexBag) vi;
			VariableType db_type = db.getHeldType();
			sValue val = re.evaluate(s);

			if(val instanceof sQueuable)
			{
				//System.out.println("complexPutStatement: db = "+db+", vl2 = "+vl2+
				//                   ", enqueuable");  
				VariableType vt = ((VariableInstance) ((sQueuable) val).getQueuable()).getType(); 

				if(vt != db_type)
				{
					throw new UserRuntimeError(
							"The bag "+db.getFullPathName()+", defined "+db.getPlace()+" holding instances of type\n"+
							db_type.getTypesName()+" recieved (put) an instance of type\n"+
							vt.getTypesName());
				}
				db.put(val);
				return;
			}
			if(val instanceof sStruct)
			{
				VariableType vt = ((VariableInstance) ((sStruct) val).getStruct()).getType();
				//System.out.println("complexPutStatement: db = "+db+", vl2 = "+vl2+
				//                   ", complex");
				if(vt != db_type)
				{
					throw new UserRuntimeError(
							"The bag "+db.getFullPathName()+", defined "+db.getPlace()+" holding instances of type\n"+
							db_type.getTypesName()+" recieved (put) an instance of type\n"+
							vt.getTypesName());
				}
				db.put(val);
				return;
			}
		}
		catch(UserRuntimeError ue) {
			String m = ue.getMessage()+"\n"+"This happened "+getPlace();
			UserRuntimeError ue2 = new UserRuntimeError(m, ue.getCause(), ue.getCollectors());
			ue2.setAction(ue.getAction());
			throw ue2;
		}
		catch(UserCompiletimeError ue) {
			String m = ue.getMessage()+"\n"+"This happened "+getPlace();
			UserCompiletimeError ue2 = new UserCompiletimeError(m, ue.getCause(), ue.getCollectors());
			ue2.setAction(ue.getAction());
			throw ue2;
		}
		catch(Exception ue) {
			String m = ue.getMessage()+"\n"+"This happened "+getPlace();
			UserRuntimeError ue2 = new UserRuntimeError(m, ue.getCause());
			throw ue2;
		}

		/*    
    variableInstance vi2 = vl2.getInstance(s);
    if(vi2.getType() != db_type)
    {
      throw new userError(
        "The bag "+db.getFullPathName()+" holding instances of type\n"+
        db_type.getTypesName()+" recieved (put) an instance of type\n"+
        v.getTypesName());
    }
    sValue in = null;
    if(vi2 instanceof enqueuable)
    {
      //System.out.println("complexPutStatement: db = "+db+", vl2 = "+vl2+
      //                   ", enqueuable");
      db.put(new sQueuable((enqueuable)vi2));
      return;
    }
    if(vi2 instanceof complex)
    {
      //System.out.println("complexPutStatement: db = "+db+", vl2 = "+vl2+
      //                   ", complex");
      db.put(new sStruct((complex)vi2));
      return;
    }
		 */

		throw new InternalError(
				"Variable "+re+" is not of complex of enqueuable type.\n"+
				"Its class was "+re.getClass().getName());
	}


	public void verify(Scope s) throws IntolerableException {
		VariableInstance vi = vl.getInstance(s);
		if(!(vi instanceof ComplexBag))
		{
			throw new InternalError( 
					"Bag should have been of complex type but was of class\n"+
					vi.getClass().getName()+". This should have been verified on\n"+
			"parsing! (se.xyz.simulation.bags.complexPutStatement)");
		}
		ComplexBag db = (ComplexBag) vi;
		VariableType db_type = db.getHeldType();
		re.verify(s);

	}

}
