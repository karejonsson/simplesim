package se.modlab.simplesim.bags;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.evaluables.*;
import se.modlab.generics.sstruct.logics.*;
import java.util.*;

public class BooleanPeek implements LogicalExpression
{

	private VariableLookup vl;

	public BooleanPeek(VariableLookup _vl)
	{
		vl = _vl;
	}

	public boolean evaluate(Scope s)
	throws IntolerableException
	{
		return true;
	}

	public void verify(Scope arg0) throws IntolerableException {
	}
	
	public String reproduceExpression() {
		return "peek("+vl.reproduceExpression()+")";
	}

}