package se.modlab.simplesim.bags;

import se.modlab.generics.*;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.evaluables.*;
import se.modlab.generics.util.Sort;
import se.modlab.simplesim.references.*;

import java.util.*;

public class ComplexSortBag extends ComplexBag
{

  protected static class complexSortSorter implements Comparator<Object>
  {
    private VariableFullLookup vfl;
    private IntolerableException ie = null;
    private ComplexBag enclosingBag;
    public complexSortSorter(VariableLookup _vl)
      throws IntolerableException
    {
      if(_vl instanceof VariableOptimizedLookup)
      {
        VariableOptimizedLookup vol = (VariableOptimizedLookup) _vl;
        VariablePartialLookup vpl = new VariablePartialLookup(
          vol.getName(),
          vol.getFilename(),
          vol.getLine(),
          vol.getColumn(),
          null);
        vfl = new VariableFullLookup(new VariablePartialLookup[]
          { vpl } );
        return;
      }
      vfl = (VariableFullLookup)_vl;
    }
    public IntolerableException getException()
    {
      return ie;
    }
    public void setEnclosingBag(ComplexBag b)
    {
      enclosingBag = b;
    }
    public VariableInstance getVariableInstance(Object o)
    {
      if(o instanceof sStruct)
      {
        return (VariableInstance) ((sStruct) o).getStruct();
      }
      if(o instanceof sQueuable)
      {
        return (VariableInstance) ((sQueuable) o).getQueuable();
      }
      return null;
    }
    public VariableFullLookup getVariableFullLookup()
    {
      return vfl;
    }
    public int compare(Object first, Object second)
    {
      StringBuffer sb = new StringBuffer();
      double d1 = 0;
      double d2 = 0;
      try
      {
        VariableInstance vi1 = getVariableInstance(first);
        VariableInstance vi1s = vfl.getInstanceScopelessByDereferencing(vi1, sb);
        sb = new StringBuffer();
        VariableInstance vi2 = getVariableInstance(second);
        VariableInstance vi2s = vfl.getInstanceScopelessByDereferencing(vi2, sb);
        boolean ok = false;
        if(vi1s instanceof LongVariable)
        { 
          d1 = ((LongVariable) vi1s).getValue().getValue();
          ok = true;
        }
        if(vi1s instanceof DoubleVariable)
        { 
          d1 = ((DoubleVariable) vi1s).getValue().getValue();
          ok = true;
        }
        if(!ok)
        {
          ie = new UserRuntimeError(
            "Dereferencing "+first+" with "+vfl+" leads to a variable\n"+
            "of type "+vi1s.getType().getTypesName()+". It is supposed\n"+
            "to be either long or double for a bag of type "+
            enclosingBag.getType().getTypesName());
        }
        if(vi2s instanceof LongVariable)
        { 
          d2 = ((LongVariable) vi2s).getValue().getValue();
          ok = true;
        }
        if(vi2s instanceof DoubleVariable)
        { 
          d2 = ((DoubleVariable) vi2s).getValue().getValue();
          ok = true;
        }
        if(!ok)
        {
          ie = new UserRuntimeError(
            "Dereferencing "+second+" with "+vfl+" leads to a variable\n"+
            "of type "+vi2s.getType().getTypesName()+". It is supposed\n"+
            "to be either long or double for a bag of type "+
            enclosingBag.getType().getTypesName());
        }
      }
      catch(IntolerableException ie)
      {}
      if(d1 > d2) return -1;
      return 1;
    }
  }

  public static class complexSortLarger extends complexSortSorter
  {
    public complexSortLarger(VariableLookup _vl)
      throws IntolerableException
    {
      super(_vl);
    }
    public int compare(Object first, Object second)
    {
      return -super.compare(first, second);
    }
  }

  public static class complexSortSmaller extends complexSortSorter
  {
    public complexSortSmaller(VariableLookup _vl)
      throws IntolerableException
    {
      super(_vl);
    }
    public int compare(Object first, Object second)
    {
      return super.compare(first, second);
    }
  }

  private complexSortSorter sorter;

  public ComplexSortBag(String name, 
                        VariableType vt_own,
                        VariableType _vt_other,
                        complexSortSorter _sorter, String _filename, int _line, int _column)
  {
    super(name, vt_own, _vt_other, _filename, _line, _column);
    sorter = _sorter;
    sorter.setEnclosingBag(this);
  }

  public sValue get()
		    throws IntolerableException
		  {
		    if(theBag.size() == 0)
		    {
		      throw new UserRuntimeError(
		        "The bag "+getFullPathName()+", defined "+this.getPlace()+" was operated on with get(bag)\n"+
		        "when it was empty!");
		    }
		    return (sValue) theBag.remove(theBag.size()-1);
		  }
		  
  public sValue get(int idx)
		    throws IntolerableException
		  {
		    if(theBag.size() == 0)
		    {
		      throw new UserRuntimeError(
		        "The bag "+getFullPathName()+", defined "+this.getPlace()+" was operated on with get(bag)\n"+
		        "when it was empty!");
		    }
		    if(theBag.size() - idx < 0)
		    {
		      throw new UserRuntimeError(
		        "The bag "+getFullPathName()+", defined "+this.getPlace()+" was operated on with get(bag, "+idx+")\n"+
		        "when there where "+theBag.size()+" elements!");
		    }
		    return (sValue) theBag.remove(theBag.size()-1-idx);
		  }
		  
  public sValue peek()
		    throws IntolerableException
		  {
		    if(theBag.size() == 0)
		    {
		      throw new UserRuntimeError(
		        "The bag "+getFullPathName()+" was operated on with peek(bag)\n"+
		        "when it was empty!");
		    }
		    return (sValue) theBag.elementAt(theBag.size()-1);
		  }
		  
  public sValue peek(int idx)
		    throws IntolerableException
		  {
		    if(theBag.size() == 0)
		    {
		      throw new UserRuntimeError(
		        "The bag "+getFullPathName()+" was operated on with peek(bag)\n"+
		        "when it was empty!");
		    }
		    if(theBag.size() - idx < 0)
		    {
		      throw new UserRuntimeError(
		        "The bag "+getFullPathName()+", defined "+this.getPlace()+" was operated on with peek(bag, "+idx+")\n"+
		        "when there where "+theBag.size()+" elements!");
		    }
		    return (sValue) theBag.elementAt(theBag.size()-1-idx);
		  }
		  
  public void put(sValue v)
    throws IntolerableException
  {
    int pos = Sort.add_sayWhere(theBag, v, sorter);
    //System.out.println("complexSortBag: Place = "+pos);
    IntolerableException ie = sorter.getException();
    if(ie != null) throw ie;
  }
  
  public String toString()
  {
    return super.toString()+" sort on "+sorter.getVariableFullLookup();
  }

	public int index(sValue v) throws IntolerableException {
		for(int i = theBag.size()-1 ; i >= 0; i--) {
			System.out.println("complexSortBag. Nr "+i);
			if(v.valueEquals(theBag.elementAt(i))) {
				return theBag.size()-1-i;
			}
		}
		return -1;
	}
	
	public boolean valueEquals(VariableInstance sv) throws IntolerableException {
		throw new InternalError("Simplesim has no implementation for comparing bags");
	}


}
