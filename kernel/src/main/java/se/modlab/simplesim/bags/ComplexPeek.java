package se.modlab.simplesim.bags;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.evaluables.*;
import java.util.*;

// Remove this class?
public class ComplexPeek implements ComplexEvaluable
{

  private VariableLookup vl;

  public ComplexPeek(VariableLookup _vl)
  {
    vl = _vl;
  }

  public Complex evaluate(Scope s)
    throws IntolerableException
  {
    return null;
  }
  
}