package se.modlab.simplesim.bags;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.values.*;

public class BooleanSortBag extends BooleanBag
{

  public BooleanSortBag(String name, VariableType vt, String _filename, int _line, int _column)
  {
    super(name, vt, _filename, _line, _column);
  }

  public sValue get()
		    throws IntolerableException
		  {
		    return null;
		  }
		  
  public sValue get(int idx)
		    throws IntolerableException
		  {
		    return null;
		  }
		  
  public sValue peek()
		    throws IntolerableException
		  {
		    return null;
		  }
		  
  public sValue peek(int idx)
		    throws IntolerableException
		  {
		    return null;
		  }
		  
  public void put(sValue v)
    throws IntolerableException
  {
  }

public boolean isNull() {
	// TODO Auto-generated method stub
	return false;
}

public boolean valueEquals(VariableInstance sv) throws IntolerableException {
	throw new InternalError("Simplesim has no implementation for comparing bags");
}

}