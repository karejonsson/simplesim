package se.modlab.simplesim.bags;

import se.modlab.generics.exceptions.UserRuntimeError;
import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.sstruct.comparisons.Scope;
import se.modlab.generics.sstruct.comparisons.VariableLookup;
import se.modlab.generics.sstruct.logics.LogicalExpression;
import se.modlab.generics.sstruct.values.sBoolean;
import se.modlab.generics.sstruct.values.sLong;
import se.modlab.generics.sstruct.values.sValue;
import se.modlab.generics.sstruct.variables.VariableInstance;

public class PredefIndexInBooleanBag extends PredefIndexInBag {
	
	private LogicalExpression le = null;

	public PredefIndexInBooleanBag(VariableLookup vl, String filename, int line, int column, LogicalExpression le) {
		super(vl, filename, line, column);
		this.le = le;
	}

	public sValue evaluate(Scope s) throws IntolerableException {
	    VariableInstance vi = vl.getInstance(s);
	    if(!(vi instanceof BooleanBag)) {
		    StringBuffer msg = new StringBuffer();
		    msg.append("Reference "+vl+" was expected to refer to a boolean worth bag but was not. Happened on "+getPlace());
		    if(vi != null) {
		    	msg.append("\nThe reference points to the declaration at "+vi.getPlace());
		    }
		    else {
		    	msg.append("\nThe reference points to nothing.");
		    }
	    	throw new UserRuntimeError(msg.toString());
	    }
	    BooleanBag bb = (BooleanBag) vi;
	    boolean value = le.evaluate(s);
		return new sLong(bb.index(new sBoolean(value)));
	}

	public String reproduceExpression() {
		return "index("+vl.reproduceExpression()+", "+le.reproduceExpression()+")";
	}

	public void verify(Scope s) throws IntolerableException {
	    VariableInstance vi = vl.getInstance(s);
	    if(vi instanceof BooleanBag) {
	    	return;
	    }
	    StringBuffer msg = new StringBuffer();
	    msg.append("Reference "+vl+" was expected to refer to a boolean worth bag but was not. Happened on "+getPlace());
	    if(vi != null) {
	    	msg.append("\nThe reference points to the declaration at "+vi.getPlace());
	    }
	    else {
	    	msg.append("\nThe reference points to nothing.");
	    }
    	throw new UserRuntimeError(msg.toString());
	}

}
