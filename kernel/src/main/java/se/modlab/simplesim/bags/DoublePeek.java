package se.modlab.simplesim.bags;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.evaluables.*;
import java.util.*;

public class DoublePeek implements ArithmeticEvaluable
{

	private VariableLookup vl;

	public DoublePeek(VariableLookup _vl)
	{
		vl = _vl;
	}

	public sValue evaluate(Scope s)
	throws IntolerableException
	{
		return null;
	}

	public void verify(Scope arg0) throws IntolerableException {
	}

	public String reproduceExpression() {
		return "peek("+vl.reproduceExpression()+")";
	}

}