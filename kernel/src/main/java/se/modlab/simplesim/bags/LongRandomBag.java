package se.modlab.simplesim.bags;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.comparisons.*;
import java.util.*;

public class LongRandomBag extends LongBag
{

  public LongRandomBag(String name, VariableType vt, String _filename, int _line, int _column)
  {
    super(name, vt, _filename, _line, _column);
  }

  public sValue get()
		    throws IntolerableException
		  {
		    if(theBag.size() == 0)
		    {
		      throw new UserRuntimeError(
		        "The bag "+getFullPathName()+", defined "+this.getPlace()+" was operated on with get(bag)\n"+
		        "when it was empty!");
		    }
		    //double d = theBag.size() * Math.random();
		    return (sValue) theBag.remove(0);//(int)Math.floor(d));
		  }
		  
  public sValue get(int idx)
		    throws IntolerableException
		  {
	    if(theBag.size() == 0)
	    {
	      throw new UserRuntimeError(
	        "The bag "+getFullPathName()+", defined "+this.getPlace()+" was operated on with get(bag, "+idx+")\n"+
	        "when it was empty!");
	    }
	    if(theBag.size() - idx < 0)
	    {
	      throw new UserRuntimeError(
	        "The bag "+getFullPathName()+", defined "+this.getPlace()+" was operated on with get(bag, "+idx+")\n"+
	        "when there where "+theBag.size()+" elements!");
	    }
		    //double d = theBag.size() * Math.random();
		    return (sValue) theBag.remove(idx);//(int)Math.floor(d));
		  }
		  
  public sValue peek()
		    throws IntolerableException
		  {
		    if(theBag.size() == 0)
		    {
		      throw new UserRuntimeError(
		        "The bag "+getFullPathName()+", defined "+this.getPlace()+" was operated on with peek(bag)\n"+
		        "when it was empty!");
		    }
		    //double d = theBag.size() * Math.random();
		    return (sValue) theBag.elementAt(0);//(int)Math.round(d));
		  }
		  
  public sValue peek(int idx)
		    throws IntolerableException
		  {
	    if(theBag.size() == 0)
	    {
	      throw new UserRuntimeError(
	        "The bag "+getFullPathName()+", defined "+this.getPlace()+" was operated on with peek(bag, "+idx+")\n"+
	        "when it was empty!");
	    }
	    if(theBag.size() - idx < 0)
	    {
	      throw new UserRuntimeError(
	        "The bag "+getFullPathName()+", defined "+this.getPlace()+" was operated on with peek(bag, "+idx+")\n"+
	        "when there where "+theBag.size()+" elements!");
	    }
		    //double d = theBag.size() * Math.random();
		    return (sValue) theBag.elementAt(idx);//(int)Math.round(d));
		  }
		  
  public void put(sValue v)
    throws IntolerableException
  {
		double d = (theBag.size()) * Math.random();
	    theBag.add((int)Math.floor(d), v);
  }

  public String toString()
  {
    return super.toString()+" random";
  }

	public boolean valueEquals(VariableInstance sv) throws IntolerableException {
		throw new InternalError("Simplesim has no implementation for comparing bags");
	}

}
