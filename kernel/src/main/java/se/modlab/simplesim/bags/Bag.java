package se.modlab.simplesim.bags;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.values.sValue;
import se.modlab.generics.sstruct.variables.*;
import java.util.*;

public abstract class Bag extends VariableInstance
{

  protected Vector<sValue> theBag = new Vector<sValue>();
  protected String fullPathName = "";

  public Bag(String name, VariableType vt, String _filename, int _line, int _column)
  {
    super(name, vt, _filename, _line, _column);
  }
  
  public int length()
  {
    return theBag.size();
  }

  public void reset()
  {
    theBag.removeAllElements();
  }

  public abstract void addDefaultValue()
    throws IntolerableException;

  public void setFullPathName(String fpn)
  {
    fullPathName = fpn;
  }

  public String getFullPathName()
  {
    if(fullPathName == null) return name;
    return fullPathName;
  }

  public String toString()
  {
    return "bag "+getFullPathName();
  }
  
  public boolean isNull() {
	  return false;
  }

  public abstract sValue get()
		  throws IntolerableException;

  public abstract sValue get(int idx)
		  throws IntolerableException;

  public abstract sValue peek()
		  throws IntolerableException;

  public abstract sValue peek(int idx)
		  throws IntolerableException;

  public abstract void put(sValue v)
		  throws IntolerableException;
  
	public boolean has(sValue v) throws IntolerableException {
		return index(v) != -1;
	}

	public int index(sValue v) throws IntolerableException {
		for(int i = 0 ; i < theBag.size(); i++) {
			//System.out.println("bag.index Nr "+i);
			if(v.valueEquals(theBag.elementAt(i))) {
				return i;
			}
		}
		return -1;
	}

}

