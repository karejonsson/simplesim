package se.modlab.simplesim.bags;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.comparisons.*;

import java.util.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.executables.*;

public class LongPutStatement extends PutStatement
{

	protected ArithmeticEvaluable ae;

	public LongPutStatement(VariableLookup _vl, ArithmeticEvaluable _ae, String _filename, int _line, int _column)
	{
		super(_vl, _filename, _line, _column);
		ae = _ae;
		//System.out.println("longPutStatement - ctor: Bag = "+vl);
	}

	public void execute(Scope s) 
	throws ReturnException, 
	IntolerableException, 
	StopException,
	BreakException,
	ContinueException
	{
		try {
			//System.out.println("longPutStatement - execute: Bag = "+vl);
			VariableInstance vi = vl.getInstance(s);
			if(!(vi instanceof LongBag))
			{
				throw new InternalError( 
						"Bag should have been of arithemitc type but was of class\n"+
						vi.getClass().getName()+". This should have been verified on\n"+
				"parsing! (se.xyz.simulation.bags.longPutStatement)");
			}
			LongBag lb = (LongBag) vi;
			sValue v = ae.evaluate(s);
			if(!(v instanceof sLong))
			{
				throw new UserRuntimeError(
						"Value put to bag "+vl+" is not of long type! The value\n"+
						"was "+v);
			}
			//System.out.println("Puts "+v+" in bag "+lb);
			lb.put(v);
		}
		catch(UserRuntimeError ue) {
			String m = ue.getMessage()+"\n"+"This happened "+getPlace();
			UserRuntimeError ue2 = new UserRuntimeError(m, ue.getCause(), ue.getCollectors());
			ue2.setAction(ue.getAction());
			throw ue2;
		}
		catch(UserCompiletimeError ue) {
			String m = ue.getMessage()+"\n"+"This happened "+getPlace();
			UserCompiletimeError ue2 = new UserCompiletimeError(m, ue.getCause(), ue.getCollectors());
			ue2.setAction(ue.getAction());
			throw ue2;
		}

	}

	public void verify(Scope s) throws IntolerableException {
		try {
			//System.out.println("longPutStatement - execute: Bag = "+vl);
			VariableInstance vi = vl.getInstance(s);
			if(!(vi instanceof LongBag))
			{
				throw new InternalError( 
						"Bag should have been of arithemitc type but was of class\n"+
						vi.getClass().getName()+". This should have been verified on\n"+
				"parsing! (se.xyz.simulation.bags.longPutStatement)");
			}
			LongBag lb = (LongBag) vi;
			ae.verify(s);
			//System.out.println("Puts "+v+" in bag "+lb);
			lb.put(new sLong(0));
		}
		catch(UserRuntimeError ue) {
			String m = ue.getMessage()+"\n"+"This happened "+getPlace();
			UserRuntimeError ue2 = new UserRuntimeError(m, ue.getCause(), ue.getCollectors());
			ue2.setAction(ue.getAction());
			throw ue2;
		}
		catch(UserCompiletimeError ue) {
			String m = ue.getMessage()+"\n"+"This happened "+getPlace();
			UserCompiletimeError ue2 = new UserCompiletimeError(m, ue.getCause(), ue.getCollectors());
			ue2.setAction(ue.getAction());
			throw ue2;
		}
	}

}
