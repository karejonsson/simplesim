package se.modlab.simplesim.bags;

import se.modlab.generics.exceptions.UserRuntimeError;
import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.sstruct.comparisons.ArithmeticEvaluable;
import se.modlab.generics.sstruct.comparisons.Scope;
import se.modlab.generics.sstruct.comparisons.VariableLookup;
import se.modlab.generics.sstruct.values.sBoolean;
import se.modlab.generics.sstruct.values.sLong;
import se.modlab.generics.sstruct.values.sValue;
import se.modlab.generics.sstruct.variables.VariableInstance;

public class PredefIndexInArithmeticBag extends PredefIndexInBag {
	
	private ArithmeticEvaluable ae = null;
	
	public PredefIndexInArithmeticBag(VariableLookup vl, String filename, int line, int column, ArithmeticEvaluable ae) {
		super(vl, filename, line, column);
		this.ae = ae;
	}
	
	private sValue handleLongBag(LongBag lb, Scope s) throws IntolerableException {
	    sValue value = ae.evaluate(s);
		return new sLong(lb.index(value));
	}

	private sValue handleDoubleBag(DoubleBag db, Scope s) throws IntolerableException {
	    sValue value = ae.evaluate(s);
		return new sLong(db.index(value));
	}

	public sValue evaluate(Scope s) throws IntolerableException {
	    VariableInstance vi = vl.getInstance(s);
	    if(vi instanceof LongBag) {
	    	return handleLongBag((LongBag) vi, s);
	    }
	    if(vi instanceof DoubleBag) {
	    	return handleDoubleBag((DoubleBag) vi, s);
	    }
	    StringBuffer msg = new StringBuffer();
	    msg.append("Reference "+vl+" was expected to refer to an arithmetic worth bag but was not. Happened on "+getPlace());
	    if(vi != null) {
	    	msg.append("\nThe reference points to the declaration at "+vi.getPlace());
	    }
	    else {
	    	msg.append("\nThe reference points to nothing.");
	    }
    	throw new UserRuntimeError(msg.toString());
	}

	public String reproduceExpression() {
		return "index("+vl.reproduceExpression()+", "+ae.reproduceExpression()+")";
	}

	public void verify(Scope s) throws IntolerableException {
	    VariableInstance vi = vl.getInstance(s);
	    if(!(vi instanceof LongBag)) {
	    	return;
	    }
	    if(!(vi instanceof DoubleBag)) {
	    	return;
	    }
	    StringBuffer msg = new StringBuffer();
	    msg.append("Reference "+vl+" was expected to refer to an arithmetic worth bag but was not. Happened on "+getPlace());
	    if(vi != null) {
	    	msg.append("\nThe reference points to the declaration at "+vi.getPlace());
	    }
	    else {
	    	msg.append("\nThe reference points to nothing.");
	    }
    	throw new UserRuntimeError(msg.toString());
	}

}
