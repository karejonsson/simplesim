package se.modlab.simplesim.bags;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.events.Enqueuable;
import se.modlab.simplesim.references.sQueuable;
import se.modlab.simplesim.references.sStruct;
import se.modlab.generics.sstruct.values.*;

public abstract class ComplexBag 
                extends Bag
{

  private VariableType vt_other;

  public ComplexBag(String name, VariableType vt_own, VariableType _vt_other, String _filename, int _line, int _column)
  {
    super(name, vt_own, _filename, _line, _column);
    vt_other = _vt_other;
  }

  public VariableType getHeldType()
  {
    return vt_other;
  }

  public void setDefaultInitialValue()
    throws IntolerableException
  {
    reset();
  }

  public void setInitialValue(VariableInstance val) 
    throws IntolerableException
  {
  }

  public void setValue(VariableInstance si)
    throws IntolerableException
  {
  }
    
  /*
  public abstract sValue get()
		  throws intolerableException;

  public abstract sValue peek()
		  throws intolerableException;

  public abstract void put(sValue v)
		  throws intolerableException;

   */

  public void addDefaultValue()
    throws IntolerableException
  {
    VariableInstance vi = ((VariableFactory) vt_other).getInstance("_ _", filename, line, column);
    vi.setDefaultInitialValue();
    if(vi instanceof Enqueuable)
    {
      put(new sQueuable((Enqueuable) vi));
      return;
    }
    if(vi instanceof Complex)
    {
      put(new sStruct((Complex) vi));
      return;
    }
    throw new InternalError(
      "Logic outdated in ComplexBag.addDefaultValue!\n"+
      "Actual class was "+vi.getClass().getName());
  }

  public String toString()
  {
    return vt_other.getTypesName()+" "+super.toString();
  }
  
  public boolean isNull() {
	  return false;
  }

}
