package se.modlab.simplesim.bags;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.comparisons.*;
import java.util.*;
import se.modlab.generics.sstruct.values.*;

public abstract class LongBag 
                extends Bag
{

  public LongBag(String name, VariableType vt, String _filename, int _line, int _column)
  {
    super(name, vt, _filename, _line, _column);
  }

  public void setDefaultInitialValue()
    throws IntolerableException {
    reset();
  }

  public void setInitialValue(VariableInstance val) 
    throws IntolerableException {
  }

  public void setValue(VariableInstance si)
    throws IntolerableException {
  }


  /*
  public abstract sValue get()
		  throws intolerableException;

  public abstract sValue peek()
		  throws intolerableException;

  public abstract void put(sValue v)
		  throws intolerableException;

   */
    
  public void addDefaultValue()
    throws IntolerableException {
    put(new sLong(0));
  }

  public String toString() {
    return "Long "+super.toString();
  }

}
