package se.modlab.simplesim.bags;

import se.modlab.generics.*;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.util.Sort;

import java.util.*;

public class LongSortBag extends LongBag
{

  public final static Comparator<Object> smaller = 
    new Comparator()
      {
	public int compare(Object first, Object second)
        {
          sValue v1 = (sValue) first;
          sValue v2 = (sValue) second;
          try
          {
            if(v1.getValue() > v2.getValue()) return -1;
          }
          catch(IntolerableException ie)  
          {
            return 1;
          }
          return 1;
        }
        public String toString()
        {
          return "smallest";
        }
      };

  public final static Comparator<Object> larger = 
    new Comparator()
      {
	public int compare(Object first, Object second)
	{
          sValue v1 = (sValue) first;
          sValue v2 = (sValue) second;
          try
          {
            if(v1.getValue() > v2.getValue()) return 1;
          }
          catch(IntolerableException ie)  
          {
            return -1;
          }
          return -1;
	}
        public String toString()
        {
          return "largest";
        }
      };

  private Comparator<Object> sorter;

  public LongSortBag(String name, 
                     VariableType vt,
                     Comparator<Object> _sorter, String _filename, int _line, int _column)
  {
    super(name, vt, _filename, _line, _column);
    sorter = _sorter;
  }

  public sValue get()
		    throws IntolerableException
		  {
		    if(theBag.size() == 0)
		    {
		      throw new UserRuntimeError(
		        "The bag "+getFullPathName()+", defined "+this.getPlace()+" was operated on with get(bag)\n"+
		        "when it was empty!");
		    }
		    return (sValue) theBag.remove(theBag.size()-1);
		  }
		  
  public sValue get(int idx)
		    throws IntolerableException
		  {
	    if(theBag.size() == 0)
	    {
	      throw new UserRuntimeError(
	        "The bag "+getFullPathName()+", defined "+this.getPlace()+" was operated on with get(bag, "+idx+")\n"+
	        "when it was empty!");
	    }
	    if(theBag.size() - idx < 0)
	    {
	      throw new UserRuntimeError(
	        "The bag "+getFullPathName()+", defined "+this.getPlace()+" was operated on with get(bag, "+idx+")\n"+
	        "when there where "+theBag.size()+" elements!");
	    }
		    return (sValue) theBag.remove(theBag.size()-1-idx);
		  }

  public sValue peek()
		    throws IntolerableException
		  {
		    if(theBag.size() == 0)
		    {
		      throw new UserRuntimeError(
		        "The bag "+getFullPathName()+", defined "+this.getPlace()+" was operated on with peek(bag)\n"+
		        "when it was empty!");
		    }
		    return (sValue) theBag.elementAt(theBag.size()-1);
		  }
		  
  public sValue peek(int idx)
		    throws IntolerableException
		  {
	    if(theBag.size() == 0)
	    {
	      throw new UserRuntimeError(
	        "The bag "+getFullPathName()+", defined "+this.getPlace()+" was operated on with peek(bag, "+idx+")\n"+
	        "when it was empty!");
	    }
	    if(theBag.size() - idx < 0)
	    {
	      throw new UserRuntimeError(
	        "The bag "+getFullPathName()+", defined "+this.getPlace()+" was operated on with peek(bag, "+idx+")\n"+
	        		"when there where "+theBag.size()+" elements!");
	    }
		    return (sValue) theBag.elementAt(theBag.size()-1-idx);
		  }
		  
  public void put(sValue v)
    throws IntolerableException
  {
    //if(v == null) System.out.println("lognSortBag - null");
    Sort.add(theBag, v, sorter);
  }

  public String toString()
  {
    return super.toString()+" sorted "+sorter;
  }
  
	public int index(sValue v) throws IntolerableException {
		for(int i = theBag.size()-1 ; i >= 0; i--) {
			if(v.valueEquals(theBag.elementAt(i))) {
				return theBag.size()-1-i;
			}
		}
		return -1;
	}

	public boolean valueEquals(VariableInstance sv) throws IntolerableException {
		throw new InternalError("Simplesim has no implementation for comparing bags");
	}

}
