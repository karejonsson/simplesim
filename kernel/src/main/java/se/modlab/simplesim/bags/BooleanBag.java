package se.modlab.simplesim.bags;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.values.*;

public abstract class BooleanBag 
                extends Bag
{

  public BooleanBag(String name, VariableType vt, String _filename, int _line, int _column)
  {
    super(name, vt, _filename, _line, _column);
  }

  public void setDefaultInitialValue()
    throws IntolerableException
  {
    reset();
  }

  public void setInitialValue(VariableInstance val) 
    throws IntolerableException
  {
  }

  public void setValue(VariableInstance si)
    throws IntolerableException
  {
  }
  
  /*
  public abstract sValue get()
		  throws intolerableException;

  public abstract sValue peek()
		  throws intolerableException;

  public abstract void put(sValue v)
		  throws intolerableException;

   */
  
  public void addDefaultValue()
    throws IntolerableException
  {
    put(new sBoolean(true));
  }

  public String toString()
  {
    return "Boolean "+super.toString();
  }

}
