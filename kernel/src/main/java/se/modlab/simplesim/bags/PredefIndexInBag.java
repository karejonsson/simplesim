package se.modlab.simplesim.bags;

import se.modlab.generics.sstruct.comparisons.ArithmeticEvaluable;
import se.modlab.generics.sstruct.comparisons.VariableLookup;

public abstract class PredefIndexInBag implements ArithmeticEvaluable {
	
	protected VariableLookup vl = null;
	private String filename = null;
	private int line = -1;
	private int column = -1;
	
	public PredefIndexInBag(VariableLookup vl, String filename, int line, int column) {
		this.vl = vl;
		this.filename = filename;
		this.line = line;
		this.column = column;
	}
	
	public String getFilename() {
		return filename;
	}

	public int getLine() {
		return line;
	}
	
	public int getColumn() {
		return column;
	}
	
	public String getPlace() {
		return "file "+getFilename()+", line "+getLine()+", column "+getColumn();
	}
	
}
