package se.modlab.simplesim.tables;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.event.UndoableEditEvent;
import javax.swing.undo.UndoManager;

import se.modlab.generics.bshro.ifc.HierarchyObject;
import se.modlab.generics.exceptions.*;
import se.modlab.tables.editor.koders.*;

/**
 * A simple demo of the JSpreadsheet component.
 * This is only a demo, it is not meant to be a serious spreadsheet
 * application.
 * @author Tony Johnson
 */
public class SpreadsheetWindow_excel extends JPanel
{
   private JSpreadsheet ss;
   
   private CopyAction copy = new CopyAction();
   private CutAction cut = new CutAction();
   private PasteAction paste = new PasteAction();
   private UndoAction undo = new UndoAction();
   private FillAction fill = new FillAction();
   private ClearAction clear = new ClearAction();
   private RedoAction redo = new RedoAction();
   private SortColumnAction sort = new SortColumnAction();
   private InsertColumnAction insertColumn = new InsertColumnAction();
   private InsertRowAction insertRow = new InsertRowAction();
   private AddRowAction addRow = new AddRowAction();
   private RemoveColumnAction removeColumn = new RemoveColumnAction();
   private RemoveRowAction removeRow = new RemoveRowAction();
   private FindNextAction findNext = new FindNextAction();

   private SpreadsheetSelectionListener sl = new SpreadsheetSelectionListener()
   {
      public void selectionChanged(SpreadsheetSelectionEvent e)
      {
         copy.update();
         cut.update();
         sort.update();
         insertColumn.update();
         removeColumn.update();
         insertRow.update();
         addRow.update();
         removeRow.update();
         fill.update();
         clear.update();
      }
   };

   private String findValue;

   private UndoManager um = new UndoManager()
   {
      public void undoableEditHappened(UndoableEditEvent e)
      {
         super.undoableEditHappened(e);
         undo.update();
         redo.update();
      }

      public void undo()
      {
         super.undo();
         undo.update();
         redo.update();
      }

      public void redo()
      {
         super.redo();
         undo.update();
         redo.update();
      }
   };

   private boolean matchCase;
   private boolean matchCell;

   public SpreadsheetWindow_excel()
   {
     this(40, 40);
   }

   public SpreadsheetWindow_excel(int row, int column)
   {
	   this(new JSpreadsheet(row, column));
   }
   
   public SpreadsheetWindow_excel(JSpreadsheet _ss)
   {
      //super(title);
      ss = _ss;

      ss.addUndoableEditListener(um);
      ss.addSelectionListener(sl);
      
      //setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      setLayout(new BorderLayout());
      add(ss,BorderLayout.CENTER);
      add(new ToolBar(),BorderLayout.NORTH);
   }
   
   public JMenuBar getMenuBar() {
	      JMenuBar menuBar = new JMenuBar();
	      menuBar.add(new FileMenu());
	      menuBar.add(new EditMenu());
	      menuBar.add(new SearchMenu());
	      menuBar.add(new TableMenu());	 
	      return menuBar;
   }
   
   public static JFrame getFrame(String title, int row, int column) {
	   JFrame out = new JFrame(title);
	   SpreadsheetWindow_excel main = new SpreadsheetWindow_excel(row, column);

	   JMenuBar mb = main.getMenuBar();
	      mb.add(new SimplesimMenu(out, main.ss, HierarchyObject.getReferenceFilePath()));
	      
	      out.setContentPane(main);
	      out.setJMenuBar(mb);
	      return out;
   }
   
   public static JFrame getFrame(String title, SpreadsheetWindow_excel main) {
	   JFrame out = new JFrame(title);

	   JMenuBar mb = main.getMenuBar();
	      mb.add(new SimplesimMenu(out, main.ss, HierarchyObject.getReferenceFilePath()));
	      
	      out.setContentPane(main);
	      out.setJMenuBar(mb);
	      return out;
   }
   
   // SpreadsheetWindow_excel.getInteractiveComponent(JSpreadsheet ss);
   
   public JPanel getInteractiveComponent(JSpreadsheet ss) {
	      return getInteractiveComponent(ss, null);
   }

   public JPanel getInteractiveComponent(JSpreadsheet ss, AbstractAction extraActions[]) {
	      JPanel main = new JPanel(new BorderLayout());
	      main.add(ss,BorderLayout.CENTER);
	      ToolBar tb = new ToolBar();
	      if(extraActions != null) {
	    	  for(int i = 0 ; i < extraActions.length ; i++) {
	    		  tb.add(extraActions[i]);
	    	  }
	      }
	      main.add(tb, BorderLayout.NORTH);
	      PopupListener pl = new PopupListener(new PopupMenu());
	      main.addMouseListener(pl);
	      return main;
   }

   public Cell getCellAt(int row, int col)
   {
      return ss.getCellAt(row, col);
   }

   public void setValueAt(Object o, int row, int col)
   {
      ss.setValueAt(o, row, col);
   }

   public void setCellAt(Cell c, int row, int col)
   {
      ss.setValueAt(c, row, col);
   }

   public int getRowCount()
   {
      return ss.getRowCount();
   }

   public void newTableModel(int rows, int cols)
   {
     ss.newTableModel(rows, cols);
   }

   /**
    * @param args the command line arguments
    */
   /*
   public static void main(String[] args)
   {
      try
      {
         //UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
      }
      catch (Exception x)
      {
         
      }
      SpreadsheetWindow_excel frame = new SpreadsheetWindow_excel();
      frame.newTableModel(10, 4);
      frame.setCellAt(new Cell(new Integer(67)), 2, 2);
      frame.pack();
      frame.setVisible(true);
   }
   */
   private void remove(boolean byRow)
   {
      CellRange range = ss.getSelectedRange();
      if (range != null)
      {
         if (byRow)
         {
            int start = range.getStartRow();
            int end = range.getEndRow();
            if ((end - start + 1) >= ss.getRowCount())
            {
               tooMuchDeletion();
            }
            else
            {
               if (ss.isDeletionSafe(byRow, start, end) || unsafeDeletion())
               {
                  ss.remove(byRow, start, end);
               }
            }
         }
         else
         {
            int start = range.getStartCol();
            int end = range.getEndCol();
            if ((end - start + 1) >= ss.getColumnCount())
            {
               tooMuchDeletion();
            }
            else
            {
               if (ss.isDeletionSafe(byRow, start, end) || unsafeDeletion())
               {
                  ss.remove(byRow, start, end);
               }
            }
         }
      }
   }

   private void sort(boolean byRow)
   {
      CellRange range = ss.getSelectedRange();
      if (range != null)
      {
         //create and show the sort dialog
         SortDialog sortDialog = new SortDialog(byRow, range);
         int rc = sortDialog.show(this, "Sort");
         if (rc == JOptionPane.OK_OPTION)
         {
            int first = sortDialog.getCriteriaA();
            first += (byRow ? range.getStartRow() : range.getStartCol());

            int second = sortDialog.getCriteriaB();
            if (second >= 0)
            {
               second += (byRow ? range.getStartRow() : range.getStartCol());
            }
            ss.sort(range, first, second, byRow, sortDialog.firstAscending(), sortDialog.secondAscending());
         }
      }
   }

   private void find(boolean newValue)
   {
      CellPoint start;

      //checks if anything is selected
      CellRange range = ss.getSelectedRange();

      if (range != null)
      {
         int x = range.getStartRow();
         int y = range.getStartCol();

         // start from the next cell
         if (!newValue)
         {
            if (y < ss.getColumnCount())
            {
               y++;
            }
            else
            {
               y = 1;
               x++;
            }
         }

         start = new CellPoint(x, y);
      }
      else
      {
         // or start from the beginning
         start = new CellPoint(0, 0);
      }

      if (newValue)
      {
         // ask for new value
         FindDialog findDialog = new FindDialog(findValue, matchCase, matchCell);
         int rc = findDialog.show(this, "Find");
         if (rc != FindDialog.OK_OPTION)
         {
            return;
         }

         String inputValue = findDialog.getString();

         //if input is cancelled or nothing is entered then don't change anything
         if ((inputValue == null) || (inputValue.length() == 0))
         {
            return;
         }
         else
         {
            findValue = inputValue;
            matchCase = findDialog.isCaseSensitive();
            matchCell = findDialog.isCellMatching();
         }
      }
      else if (findValue == null)
      {
         findNext.update();
         return;
      }

      CellPoint found = ss.find(start, findValue, matchCase, matchCell);
      if (found != null)
      {
         ss.setSelectedRange(new CellRange(found.getRow(), found.getRow(), found.getCol(), found.getCol()));
      }
      else
      {
         JOptionPane.showMessageDialog(this, "Search complete and no more \"" + findValue + "\" were found.");
      }
      findNext.update();
   }
   
   private void fill()
   {
      CellRange range = ss.getSelectedRange();
      Cell first = ss.getCellAt(range.getStartRow(), range.getStartCol());
      String fillValue = first.toString();

      Icon fillIcon = getIcon("fill32");
      String inputValue = (String) JOptionPane.showInputDialog(this, "Please enter a value to fill the range", "Fill", JOptionPane.INFORMATION_MESSAGE, fillIcon, null, fillValue);

	  //if input is cancelled or nothing is entered 
	  //then don't change anything
	  if ((inputValue != null) && (inputValue.length() != 0))
	  {
	       ss.fill(range, inputValue);
	  }  
   }
    
   private void clear()
   {
	  CellRange range = ss.getSelectedRange();
	  if (range != null)
	  {
	     ss.clear(range);
	  }
   }
   
   private void insert(boolean byRow)
   {
      CellRange range = ss.getSelectedRange();
      if (range != null)
      {
         if (byRow)
         {
            ss.insert(byRow, range.getStartRow(), range.getEndRow());
         }
         else
         {
            ss.insert(byRow, range.getStartCol(), range.getEndCol());
         }
      }
   }

   private void addRow()
   {  
      ss.addRow();
   }

   private void tooMuchDeletion()
   {
      JOptionPane.showMessageDialog(this, "You can not delete all the rows or columns!", "Delete", JOptionPane.ERROR_MESSAGE);
   }

   private boolean unsafeDeletion()
   {
      int choice = JOptionPane.showConfirmDialog(this, "The deletion may cause irriversible data loss in other cells.\n\n" + "Do you really want to proceed?\n\n", "Delete", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);

      return choice == JOptionPane.YES_OPTION;
   }
   
   static Icon getIcon(String name)
   {
      //System.out.println("SpreadsheetDemo.getIcon("+name+")");
      return null;
/*
      try
      {
         return null;//new ImageIcon(ImageIO.read(FindDialog.class.getResource("resources/"+name+".gif")));
      } 
      catch (IOException x) 
	  {
      	 return null;
      }   	
 */
   }

   private class ClearAction extends AbstractAction
   {
      ClearAction()
      {
         super("Clear");
         setEnabled(false);
      }

      public void actionPerformed(ActionEvent e)
      {
      	clear();
      }
      
      void update()
      {
      	setEnabled(ss.getSelectedRange() != null);
      }
   }

   private class CopyAction extends AbstractAction
   {
      CopyAction()
      {
         super("Copy");
         putValue(ACCELERATOR_KEY,KeyStroke.getKeyStroke('C',InputEvent.CTRL_MASK));
         setEnabled(false);
      }

      public void actionPerformed(ActionEvent e)
      {
         ss.copy();
      }

      void update()
      {
         setEnabled(ss.getSelectedRange() != null);
      }
   }

   private class CutAction extends AbstractAction
   {
      CutAction()
      {
         super("Cut");
         putValue(ACCELERATOR_KEY,KeyStroke.getKeyStroke('X',InputEvent.CTRL_MASK));
         setEnabled(false);
      }

      public void actionPerformed(ActionEvent e)
      {
         ss.cut();
      }

      void update()
      {
         setEnabled(ss.getSelectedRange() != null);
      }
   }

   private class EditMenu extends JMenu
   {
      EditMenu()
      {
         super("Edit");
         add(undo);
         add(redo);
         addSeparator();
         add(cut);
         add(copy);
         add(paste);
         addSeparator();
         add(new SelectAllAction());
      }
   }
   private class ToolBar extends JToolBar
   {
      ToolBar()
	  {
	     add(insertColumn);
	     add(removeColumn);
	     add(insertRow);
	     add(addRow);
	     add(removeRow);
	     add(sort);
	  }
   }
   
   private class PopupMenu extends JPopupMenu
   {
   	  PopupMenu()
	  {
   	  	 add(undo);
   	  	 add(redo);
   	  	 addSeparator();
   	  	 add(cut);
   	  	 add(copy);
   	  	 add(paste);
   	  	 addSeparator();
   	  	 add(fill);
   	  	 add(clear);
	  }
   }

   private class ExitAction extends AbstractAction
   {
      ExitAction()
      {
         super("Exit");
      }

      public void actionPerformed(ActionEvent e)
      {
         SpreadsheetWindow_excel.this.setVisible(false);
      }
   }

   private class FileMenu extends JMenu
   {
      FileMenu()
      {
         super("File");
         add(new ExitAction());
      }
   }

   private class FillAction extends AbstractAction
   {
      FillAction()
      {
         super("Fill...");
         setEnabled(false);
      }

      public void actionPerformed(ActionEvent e)
      {
      	 fill();
      }
      
      void update()
      {
      	setEnabled(ss.getSelectedRange() != null);
      }
   }

   private class FindAction extends AbstractAction
   {
      FindAction()
      {
         super("Find...");
      }

      public void actionPerformed(ActionEvent e)
      {
         find(true);
      }
   }

   private class FindNextAction extends AbstractAction
   {
      FindNextAction()
      {
         super("Find Next");
         setEnabled(false);
      }

      public void actionPerformed(ActionEvent e)
      {
         find(false);
      }
      
      void update()
      {
         setEnabled(findValue != null && findValue.length() > 0);
      }
   }

   private class InsertColumnAction extends AbstractAction
   {
      InsertColumnAction()
      {
         super("Insert Column");
         putValue(Action.SMALL_ICON,getIcon("insertcolumn"));
         setEnabled(false);
      }

      public void actionPerformed(ActionEvent e)
      {
         insert(false);
      }
      void update()
      {
         setEnabled(ss.getSelectedRange() != null);
      }
   }

   private class InsertRowAction extends AbstractAction
   {
      InsertRowAction()
      {
         super("Insert Row");
         putValue(Action.SMALL_ICON,getIcon("insertrow"));
         setEnabled(false);
      }

      public void actionPerformed(ActionEvent e)
      {
         insert(true);
      }
      
      void update()
      {
         setEnabled(ss.getSelectedRange() != null);
      }
   }

   private class AddRowAction extends AbstractAction
   {
      AddRowAction()
      {
         super("Add Row");
         putValue(Action.SMALL_ICON,getIcon("addrow"));
         setEnabled(true);
      }

      public void actionPerformed(ActionEvent e)
      {
         addRow();
      }
      
      void update()
      {
         setEnabled(true);
      }
   }

   private class PasteAction extends AbstractAction
   {
      PasteAction()
      {
         super("Paste");
         putValue(ACCELERATOR_KEY,KeyStroke.getKeyStroke('V',InputEvent.CTRL_MASK));
      }

      public void actionPerformed(ActionEvent e)
      {
         ss.paste();
      }
   }

   private class RedoAction extends AbstractAction
   {
      RedoAction()
      {
         super("Redo");
         putValue(ACCELERATOR_KEY,KeyStroke.getKeyStroke('Y',InputEvent.CTRL_MASK));
         setEnabled(false);
      }

      public void actionPerformed(ActionEvent e)
      {
         um.redo();
      }

      void update()
      {
         setEnabled(um.canRedo());
      }
   }

   private class RemoveColumnAction extends AbstractAction
   {
      RemoveColumnAction()
      {
         super("Remove Column");
         putValue(Action.SMALL_ICON,getIcon("deletecolumn"));
         setEnabled(false);
      }

      public void actionPerformed(ActionEvent e)
      {
         remove(false);
      }
      
      void update()
      {
         setEnabled(ss.getSelectedRange() != null);
      }
   }

   private class RemoveRowAction extends AbstractAction
   {
      RemoveRowAction()
      {
         super("Remove Row");
         putValue(Action.SMALL_ICON,getIcon("deleterow"));
         setEnabled(false);
      }

      public void actionPerformed(ActionEvent e)
      {
         remove(true);
      }
      void update()
      {
         setEnabled(ss.getSelectedRange() != null);
      }
   }

   private class SearchMenu extends JMenu
   {
      SearchMenu()
      {
         super("Search");
         add(new FindAction());
         add(findNext);
      }
   }

   private class SelectAllAction extends AbstractAction
   {
      SelectAllAction()
      {
         super("Select All");
         putValue(ACCELERATOR_KEY,KeyStroke.getKeyStroke('A',InputEvent.CTRL_MASK));
      }

      public void actionPerformed(ActionEvent e)
      {
         int rows = ss.getRowCount();
         int cols = ss.getColumnCount();
         ss.setSelectedRange(new CellRange(0, rows - 1, 0, cols - 1));
      }
   }

   private class SortColumnAction extends AbstractAction
   {
      SortColumnAction()
      {
         super("Sort Column...");
         putValue(Action.SMALL_ICON,getIcon("sort"));
         setEnabled(false);
      }

      public void actionPerformed(ActionEvent e)
      {
         sort(false);
      }
      
      void update()
      {
         setEnabled(ss.getSelectedRange() != null);
      }
   }

   private class TableMenu extends JMenu
   {
      TableMenu()
      {
         super("Table");
         add(insertColumn);
         add(insertRow);
         add(removeColumn);
         add(removeRow);
         addSeparator();
         add(fill);
         add(clear);
         addSeparator();
         add(sort);
      }
   }

   private class UndoAction extends AbstractAction
   {
      UndoAction()
      {
         super("Undo");
         putValue(ACCELERATOR_KEY,KeyStroke.getKeyStroke('Z',InputEvent.CTRL_MASK));
         setEnabled(false);
      }

      public void actionPerformed(ActionEvent e)
      {
         um.undo();
      }

      void update()
      {
         setEnabled(um.canUndo());
      }
   }
   
   private class PopupListener extends MouseAdapter
   {
      private JPopupMenu popup;
      
      public PopupListener(JPopupMenu popup)
      {
         this.popup = popup;
      }
      public void mousePressed(MouseEvent e)
      {
         maybeShowPopup(e);
      }
      public void mouseReleased(MouseEvent e)
      {
         maybeShowPopup(e);
      }
      protected void maybeShowPopup(MouseEvent e)
      {
         if (popup.isPopupTrigger(e))
         {
            popup.show(e.getComponent(), e.getX(), e.getY());
         }
      }
   }
}
