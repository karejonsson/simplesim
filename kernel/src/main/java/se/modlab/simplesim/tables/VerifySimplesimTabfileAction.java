package se.modlab.simplesim.tables;

import java.io.*;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.tables.editor.koders.*;
import se.modlab.generics.sstruct.tables.*;

public class VerifySimplesimTabfileAction extends AbstractAction
{
  public JFrame f;
  public JSpreadsheet ss;
  public String catalog;

  public VerifySimplesimTabfileAction(JFrame f, JSpreadsheet _ss, String _catalog)
  {
    super("Verify");
    f = f;
    ss = _ss;
    catalog = _catalog;
  }

  private boolean qualifies(Cell c) {
    if(c == null) return true;
    if(c.toString().trim().compareTo("") == 0) {
      return true;
    }
    if(c.getType() == Cell.NUMBER) return true;
    try {
      if(TabParser.parseDatetimeFromString(c.toString().trim())) {
        return true;
      }
    }
    catch(Exception e) {
      //signalError(e.getMessage(), c);
    }
    return false;
  }

  private void signalError(String msg, Cell c) {
    UniversalTellUser.error(f, 
      "Simplesim cannot generate a .tab-file from this sheet.\n\n"+
      msg+"\n\n"+
      "All fields should be numeric or blanc or a date."+((c != null) ? " Here it was \n"+
      "\""+c.toString()+"\"" : "")+"\n\n"+
      "The exception is in the first row where you can have Textual titles\n"+
      "but then ther must be a second row from which the type of the\n"+
      "column can be deduced.");
  }
  
  private boolean isTextual(Cell c) {
	  return c.getType() == Cell.TEXT; 
  }

	  
  private boolean isFirstRowPurelyTextual() {
	  int columns = ss.getColumnCount();
      for(int i = 1 ; i < columns ; i++) 
      {
        Cell c = ss.getCellAt(0, i);
        if(!isTextual(c)) return false;
      }
      return true;
  }

  public void actionPerformed(ActionEvent e)
  {
	int startRow = 0;
	if(isFirstRowPurelyTextual()) {
		startRow = 1;
	}
    int rows = ss.getRowCount();
    int columns = ss.getColumnCount();
    for(int i = startRow ; i < rows ; i++)
    {
      Cell c = ss.getCellAt(i, 0);
      if(!qualifies(c)) {
        signalError("Field "+(i+1)+", 0 is not ok.", c);
        return;
      }
      for(int ii = 1 ; ii < columns ; ii++) 
      {
        c = ss.getCellAt(i, ii);
        if(!qualifies(c)) {
          signalError("Field "+(i+1)+", "+ii+" is not ok.", c);
          return;
        }
      }
    }
    StringBuffer sb = new StringBuffer("Spreadsheet can generate Simplesim tabfile.");
    if(startRow != 0) {
      sb.append("\n");
      sb.append("The spreadsheet can also generate a struct for use within Simplesim.");
    }
    UniversalTellUser.info(f, sb.toString());
  }

}



