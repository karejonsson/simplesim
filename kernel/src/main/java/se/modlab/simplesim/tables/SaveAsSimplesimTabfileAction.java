package se.modlab.simplesim.tables;

import java.io.*;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.tables.editor.koders.*;
import se.modlab.generics.sstruct.tables.*;

public class SaveAsSimplesimTabfileAction extends AbstractAction
{
	public static final char delimiterDefault = ';';

	public Component c;
	public JSpreadsheet ss; 
	public String catalog;
	private char delimiter;

	public SaveAsSimplesimTabfileAction(Component _c, JSpreadsheet _ss, String _catalog, char _delimiter)
	{
		super("Save as tab or csv file, delimiter "+_delimiter);
		c = _c;
		ss = _ss;
		catalog = _catalog;
		delimiter = _delimiter;
	}

	private boolean qualifies(Cell c) {
		if(c == null) return true;
		if(c.toString().trim().compareTo("") == 0) {
			return true;
		}
		if(c.getType() == Cell.NUMBER) return true;
		try {
			if(TabParser.parseDatetimeFromString(c.toString().trim())) {
				return true;
			}
		}
		catch(Exception e) {
			//signalError(e.getMessage(), c);
		}
		try {
			if(TabParser.parseDatetimeFromString(c.toString().trim())) {
				return true;
			}
		}
		catch(Exception e) {
			//signalError(e.getMessage(), c);
		}
		try {
			if(TabParser.parseStringFromString("\""+c.toString().trim()+"\"")) {
				return true;
			}
		}
		catch(Exception e) {
			//signalError(e.getMessage(), c);
		}
		return false;
	}

	private void add(Cell c, StringBuffer sb) {
		//System.out.println("SaveAsSimplesimTabfileAction - add 1");
		if(c == null) {
			sb.append(" "); 
			//System.out.println("SaveAsSimplesimTabfileAction - add null");
			return;
		}
		//System.out.println("SaveAsSimplesimTabfileAction - add 2");
		if(c.toString().trim().compareTo("") == 0) {
			sb.append(" ");
			//System.out.println("SaveAsSimplesimTabfileAction - add tom str�ng");
			return;
		}
		//System.out.println("SaveAsSimplesimTabfileAction - add 3");
		if(c.getType() == Cell.NUMBER) {
			sb.append(c.toString());
			//System.out.println("SaveAsSimplesimTabfileAction - add number "+c.toString());
			return;
		}
		//System.out.println("SaveAsSimplesimTabfileAction - add 4");
		try {
			if(TabParser.parseDatetimeFromString(c.toString().trim())) {
				sb.append(c.toString().trim());
				//System.out.println("SaveAsSimplesimTabfileAction - add parseDatetimeFromString "+c.toString());
				return;
			}
		}
		catch(Exception e) {
			//System.out.println("SaveAsSimplesimTabfileAction - add 4 exception "+e);
			//signalError(e.getMessage(), c);
		}
		//System.out.println("SaveAsSimplesimTabfileAction - add 5");
		try {
			if(TabParser.parseStringFromString(c.toString().trim())) {
				sb.append(c.toString().trim());
				//System.out.println("SaveAsSimplesimTabfileAction - add parseStringFromString "+c.toString());
				return;
			}
		}
		catch(Exception e) {
			//System.out.println("SaveAsSimplesimTabfileAction - add 5 exception "+e);
			//signalError(e.getMessage(), c);
		}
		//System.out.println("SaveAsSimplesimTabfileAction - add 6");
		try {
			if(TabParser.parseStringFromString("\""+c.toString().trim()+"\"")) {
				sb.append("\""+c.toString().trim()+"\"");
				//System.out.println("SaveAsSimplesimTabfileAction - add parseStringFromString "+c.toString());
				return;
			}
		}
		catch(Exception e) {
			//System.out.println("SaveAsSimplesimTabfileAction - add 6 exception "+e);
			//signalError(e.getMessage(), c);
		}
		//System.out.println("SaveAsSimplesimTabfileAction - add - "+c.toString());
	}

	private void signalError(String msg, Cell c) {
		UniversalTellUser.error(UniversalTellUser.getFrame(this.c), 
				"Simplesim cannot generate a .tab or .csv-file from this sheet.\n\n"+
				msg+"\n\n"+
				"All fields should be numeric or blanc or a date."+((c != null) ? " Here it was \n"+
						"\""+c.toString()+"\"" : "")+"\n\n"+
						"The exception is in the first row where you can have Textual titles\n"+
						"but then there must be a second row from which the type of the\n"+
		"column can be deduced.");
	}

	private boolean isTextual(Cell c) {
		return c.getType() == Cell.TEXT; 
	}


	private boolean isFirstRowPurelyTextual() {
		int columns = ss.getColumnCount();
		for(int i = 1 ; i < columns ; i++) 
		{
			Cell c = ss.getCellAt(0, i);
			if(!isTextual(c)) return false;
		}
		return true;
	}

	protected StringBuffer getBuffer(int startRow) {
		int rows = ss.getRowCount();
		int columns = ss.getColumnCount();
		StringBuffer sb = new StringBuffer();
		for(int i = startRow ; i < rows ; i++)
		{
			Cell c = ss.getCellAt(i, 0);
			if(!qualifies(c)) {
				signalError("Field "+(i+1)+", 0 is not ok.", c);
				return null;
			}
			else {
				add(c, sb);
			}
			for(int ii = 1 ; ii < columns ; ii++) 
			{
				c = ss.getCellAt(i, ii);
				if(!qualifies(c)) {
					signalError("Field "+(i+1)+", "+ii+" is not ok.", c);
					return null;
				}
				else {
					sb.append(" "+delimiter+"  ");
					add(c, sb);
				}
			}
			sb.append("\n");
		}
		return sb;
	}

	public void actionPerformed(ActionEvent e)
	{
		int startRow = 0;
		if(isFirstRowPurelyTextual()) {
			//startRow = 1;
		}
		//System.out.println("SaveAsSimplesimTabfile - actionPerformed.\n"+
		//  "catalog = "+catalog+"\n"+
		//  "rows = "+rows+"\n"+
		//  "columns = "+columns);
		StringBuffer sb = getBuffer(startRow);//new StringBuffer();
		if(sb == null) {
			// Problem is already detected and communicated. Do nothing.
			return;
		}
		//System.out.println("SaveAsSimplesimTabfile.actionPerformed "+sb.toString());
		boolean outcome = false;
		try {
			outcome = (TabParser.parseFromString(sb.toString()) != null);
		}
		catch(Exception _e) {
			signalError(_e.getMessage(), null);
			return;
		}
		if(!outcome) {
			signalError("There is something wrong with the generated .tab-file generated.\n"+
					"This is an unlucky event. At the point the problem is detected\n"+
					"it is not possible to make a better diagnose.", null);
			return;
		}
		//signalError("OK", null);

		try
		{
			JFileChooser fc = new JFileChooser(catalog);
			fc.setDialogTitle("Save tabfile. Point to the file. (With extension .tab)");
			fc.setDialogType(JFileChooser.SAVE_DIALOG);
			fc.showSaveDialog(UniversalTellUser.getFrame(c));
			File selFile = fc.getSelectedFile();
			if(selFile == null) return;
			String path = selFile.getAbsolutePath();
			if(path == null) return;
			if(!(path.endsWith(".tab")) && !(path.endsWith(".csv")))
			{
				UniversalTellUser.error(
						UniversalTellUser.getFrame(c), 
						"The filename must end with .tab or .csv", 
				"File not valid");
				return;
			}
			FileOutputStream fos = new FileOutputStream(selFile);
			fos.write(sb.toString().getBytes());
			fos.close();
		}
		catch(FileNotFoundException fnf)
		{
			UniversalTellUser.general(UniversalTellUser.getFrame(c), new SystemError("File not found", fnf));
			return;
		}
		catch(IOException ioe)
		{
			UniversalTellUser.general(UniversalTellUser.getFrame(c), new SystemError("I/O error", ioe));
			return;
		}
		catch(Exception __e)
		{
			UniversalTellUser.general(UniversalTellUser.getFrame(c), 
					new UnclassedError("SaveAsSimplesimTabfileAction - tab-generation", __e));
			return;
		}

	}


}



