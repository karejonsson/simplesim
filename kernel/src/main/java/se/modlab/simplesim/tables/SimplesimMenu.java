package se.modlab.simplesim.tables;

import java.awt.event.*;
import javax.swing.*;
import se.modlab.tables.editor.koders.*;

public class SimplesimMenu extends JMenu
{
  public SimplesimMenu(JFrame f, JSpreadsheet ss, String catalog)
  {
    super("Simplesim");
    add(new GenerateSimplesimStructAction(f, ss, catalog));
    add(new SaveAsSimplesimTabfileAction(f, ss, catalog, ','));
    add(new SaveAsSimplesimTabfileAction(f, ss, catalog, ';'));
    add(new VerifySimplesimTabfileAction(f, ss, catalog));
    add(new OpenSimplesimTabfileAction(f, ss, catalog));
    add(new HandleCommasAndPercents(f, ss, catalog));
    add(new RemoveEmptyColumns(f, ss, catalog));
    add(new RemoveEmptyRows(f, ss, catalog));
  }
}


