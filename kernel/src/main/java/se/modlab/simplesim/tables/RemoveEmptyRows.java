package se.modlab.simplesim.tables;

import java.io.*;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.tables.editor.koders.*;
import se.modlab.generics.sstruct.tables.*;

public class RemoveEmptyRows extends AbstractAction
{
  public JFrame f;
  public JSpreadsheet ss;
  public String catalog;

  public RemoveEmptyRows(JFrame f, JSpreadsheet _ss, String _catalog)
  {
    super("Remove empty rows");
    f = f;
    ss = _ss;
    catalog = _catalog;
  }

  public void actionPerformed(ActionEvent e)
  {
    int rows = ss.getRowCount();
    int columns = ss.getColumnCount();
    int count = 0;
	for(int i = 0 ; i < rows ; i++)
    {
    	boolean allEmpty = true;
        for(int ii = columns-1 ; ii >= 0 ; ii--) 
    	{
    		Cell c = ss.getCellAt(i, ii);
    		if(c != null) { 
    			Object o = c.getValue();
    			if(o != null) {
    				if(o.toString().trim().length() != 0) {
    					allEmpty = false;
    				}
    			}
    			//System.out.println("Column "+ii+", row "+i+" contains "+c);
    		}
    		else {
    			//System.out.println("Column "+ii+", row "+i+" empty.");
    		}
    	}
    	if(allEmpty) { 
    		//System.out.println("Removes row "+i);
    		ss.remove(true, i, i);
    		count++;
    	}
    }
    StringBuffer sb = new StringBuffer("Removed "+count+" rows.");
    UniversalTellUser.info(f, sb.toString());
  }

}


