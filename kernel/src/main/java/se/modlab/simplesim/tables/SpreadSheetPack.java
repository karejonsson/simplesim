package se.modlab.simplesim.tables;

import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.files.OneFileNotepad;
import se.modlab.generics.exceptions.*;
import se.modlab.simpleide.*;

import java.awt.event.*;
import java.io.*;

import javax.swing.*;

public class SpreadSheetPack extends DefaultIdeMenuHandler
{

  private static final String packname = "Spreadsheet";
  private static final String version = "1";

  public SpreadSheetPack()
  {
  }

  private JMenu[] getExcelOptions(File f)
  {
    final File _f = f;
    final JMenu menu = new JMenu(packname);
    JMenuItem menuItem = new JMenuItem("Open");
    menu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          try 
          {
            SpreadsheetManager.openSheet(_f.getAbsolutePath());
          }
          catch(IntolerableException ie)
          {
            UniversalTellUser.general(null, ie);            
          }
        }
      }
    );
    return new JMenu[] { menu };
  }

  private JMenu[] getSimplesimTabfileOptions(File f)
  {
    final File _f = f;
    final JMenu menu = new JMenu(packname);
    JMenuItem menuItem = new JMenuItem("Open");
    menu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          OpenSimplesimTabfileAction.tabfileOpen(_f.getName());
        }
      }
    );
    return new JMenu[] { menu };
  }

  public JMenu[] getFilesMenu(File _f)
  {
    if(_f.getName().endsWith(".xls")) {
      return getExcelOptions(_f);
    }
    if(_f.getName().endsWith(".tab")) {
      return getSimplesimTabfileOptions(_f);
    }
    return null;
  }

  public JMenu[] getEditorsMenu(OneFileNotepad ofn)
  { 
    return null;
  }

  public JMenu[] getFoldersMenu(File f)
  {
    return null;
  }

  public JMenu[] getMenubar()
  {
    final JMenu menu = new JMenu(packname);
    JMenuItem menuItem = new JMenuItem("About "+packname);
    menu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          info(
            getFrame(menu), 
            packname+" is created by the koders project\n"+
            "JSpreadsheet. Some involved coders there are:\n"+ 
            "\n"+ 
            "Ricky Chin, Hua Zhong. \n"+ 
            "\n"+ 
            "These names appear in the JavaDoc comments.\n"+ 
            "\n"+ 
            "The Simplesim project thanks for a very nice\n"+ 
            "piece of code to work with.\n"+ 
            "\n"+ 
            "More Info: https://jspreadsheet.dev.java.net/\n"+ 
            "and: http://www.koders.com\n", 
            "Information for "+packname);
        }
      }
    );
    menuItem = new JMenuItem("Version");
    menu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          info(
            getFrame(menu), 
            "This is version "+version+" of "+packname+"\n",
            "Version");
        }
      }
    );
    menuItem = new JMenuItem("Open");
    menu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          SpreadsheetManager.openDefaultSpreadsheet();
        }
      }
    );
    return new JMenu[] { menu };
  }

}
