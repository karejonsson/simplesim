package se.modlab.simplesim.tables;

import java.util.*;
import se.modlab.generics.sstruct.tables.*;

public class SpreadsheetDatetimeHandler implements DatetimeHandler
{

  public Holder getHolder(String datetime, int beginLine, int beginColumn)
  {
    return new StringHolder(datetime, beginLine, beginColumn);
  }

}
