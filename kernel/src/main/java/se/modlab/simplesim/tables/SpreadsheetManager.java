package se.modlab.simplesim.tables;

import java.util.*;
import java.sql.*;

import se.modlab.tables.excel.*;
import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.find.*;
import se.modlab.generics.files.*;
import se.modlab.generics.exceptions.*;
import se.modlab.tables.editor.koders.*;

import java.awt.Component;

import javax.swing.JFrame;

public class SpreadsheetManager
{


  public static void main(String args[]) 
  {
    try {
      openSheet("DPDSM.xls");
    }
    catch(UserCompiletimeError ue) {
      System.out.println(ue.getMessage());
    }
  }

  public static void openSheet(String _filename) 
    throws UserCompiletimeError
  {
    final String filename = _filename;//"DPDSM.xls";
    String sheets[] = Read.getSheetNames(filename);
    for(int i = 0 ; i < sheets.length ; i++) {
      sheets[i] = Manipulations.getBareName(sheets[i]);
    }
    ChooseTextAction cta = new ChooseTextAction()
      {
        public void cancelAction()
        {
          //System.out.println("ChooseTextAction - cancelAction");
        }

        public void textChosen(String text)
        {
          try {
            activateSS(filename, Manipulations.getBareName(text));
          } 
          catch(UserError e) {
            UniversalTellUser.general((Component)null, e);
          }
        }
      };

    ChooseTextDialog ctd = new ChooseTextDialog(null, false, "Choose sheet", sheets, cta);
    ctd.setVisible(true);
  }

  private static Cell getCell(String s)
  {
    if(s == null) return null;
    Cell c = null;
    try {
    	c = new Cell(Integer.parseInt(s));
    	//System.out.println("Integer.parseInt");
    	return c;
    }
    catch(Exception e) {
    }
    try {
    	c = new Cell(Long.parseLong(s));
    	//System.out.println("Long.parseLong");
    	return c;
    }
    catch(Exception e) {
    }
    try {
    	c = new Cell(Double.parseDouble(s));
    	//System.out.println("Double.parseDouble");
    	return c;
    }
    catch(Exception e) {
    }
    try {
      if(s.toLowerCase().compareTo("true") == 0) {
      	//System.out.println("Boolean(true)");
        return new Cell(new Boolean(true));
      }
      if(s.toLowerCase().compareTo("false") == 0) {
    	//System.out.println("Boolean(false)");
        return new Cell(new Boolean(false));
      }
    }
    catch(Exception e) {
    }
  	//System.out.println("String");
    return new Cell(s);
  }

  public static void activateSS(String filename, String sheetname) 
    throws UserError
  {
    //System.out.println("SpreadsheetManager.activateSS(filename = "+filename+", text = "+text+")");
    ResultSet rs = null;
    try {
      String x = Manipulations.getCompletedName(sheetname);
      //System.out.println("X "+x);
      rs = Read.readExcelSheet(filename, x);
    }
    catch(Exception e) {
      throw new UserCompiletimeError(
        "Unable to read sheet "+sheetname+" from file "+filename, e);
    }
    ResultSetMetaData md = null;
    try {
      md = rs.getMetaData();
      int columns = md.getColumnCount();
      //System.out.println("ms not null, columns = "+columns);
      for(int i = 0 ; i < columns ; i++) {
    	  String s = md.getCatalogName(i+1);
    	  //System.out.println("Name "+s+", count "+i);
      }
    }
    catch(Exception e) {
      throw new UserCompiletimeError("Unable to get general meta data from sheet "+sheetname+" from file "+filename, e);
    }
    int columns = -1;
    try {
      columns = md.getColumnCount();
    }
    catch(Exception e) {
      throw new UserCompiletimeError("Unable to get column count meta data from sheet "+sheetname+" from file "+filename, e);
    }
    Vector<Vector<Cell>> table = new Vector<Vector<Cell>>();
    try {
      int ctr = 0;
      while(rs.next()) {
        Vector<Cell> row = new Vector<Cell>();
        table.add(row);
        for(int i = 1 ; i <= columns ; i++) {
        	String s = rs.getString(i);
        	//System.out.println("ctr = "+ctr+", i = "+i+", s = "+s);
        	row.add(getCell(s));
        }
        ctr++;
      }
    }
    catch(Exception e) {
      throw new UserCompiletimeError("Unable to step through rows from sheet "+sheetname+" from file "+filename, e);
    }
    int rows = table.size();
    SpreadsheetWindow_excel excel = new SpreadsheetWindow_excel();
    excel.newTableModel(rows, columns);
    //System.out.println("rows "+rows+", columns "+columns);
    for(int i = 0 ; i < rows ; i++)
    {
      Vector<Cell> row = table.elementAt(i);
      for(int ii = 0 ; ii < columns ; ii++)
      {
        
        Cell entry = row.elementAt(ii);
        //System.out.println("row "+i+", column "+ii+", value "+entry+", rows "+rows+", columns "+columns);
        if(entry != null) excel.setCellAt(entry, i, ii);
      }
    }
    JFrame frame = SpreadsheetWindow_excel.getFrame("", excel);
    frame.pack();
    frame.setVisible(true);    
  }

  public static void openDefaultSpreadsheet()
  {
    SpreadsheetWindow_excel excel = new SpreadsheetWindow_excel();
    JFrame frame = SpreadsheetWindow_excel.getFrame("", excel);
    frame.pack();
    frame.setVisible(true);
  }

}
