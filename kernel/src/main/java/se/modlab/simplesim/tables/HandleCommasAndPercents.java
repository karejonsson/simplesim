package se.modlab.simplesim.tables;

import java.io.*;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.tables.editor.koders.*;
import se.modlab.generics.sstruct.tables.*;

public class HandleCommasAndPercents extends AbstractAction
{
  public JFrame f;
  public JSpreadsheet ss;
  public String catalog;

  public HandleCommasAndPercents(JFrame _f, JSpreadsheet _ss, String _catalog)
  {
    super("Commas and Percents");
    f = _f;
    ss = _ss;
    catalog = _catalog;
  }

  private boolean handle(Cell c) {
    if(c == null) return false;
    String s = c.toString().trim();
    if(s.compareTo("") == 0) {
      return false;
    }
    if(c.getType() == Cell.NUMBER) return false;
    if(s.indexOf(',') != -1) s = s.replace(',', '.');
    if(s.indexOf("%") != -1) {
    	// There is a percentage sign.
    	s = s.replace('%', ' ').trim();
    	try {
        	c.setValue(Double.parseDouble(s) / 100);
        	return true;
    	}
    	catch(NumberFormatException nfe) {
    		return false;
    	}
    }
    else {
    	// There is no percentage sign.
    	try {
    		c.setValue(Double.parseDouble(s)); 	
        	return true;
    	}
    	catch(NumberFormatException nfe) {
    		return false;
    	}
    }
  }

  public void actionPerformed(ActionEvent e)
  {
    int rows = ss.getRowCount();
    int columns = ss.getColumnCount();
    int count = 0;
    for(int i = 0 ; i < rows ; i++)
    {
      for(int ii = 0 ; ii < columns ; ii++) 
      {
        Cell c = ss.getCellAt(i, ii);
        boolean handled = handle(c);
        if(handled) {
        	//System.out.println("Cell row "+i+", column "+ii+" was handled.");
        	count++;
        }
        //if(handle(c)) count++;
      }
    }
    StringBuffer sb = new StringBuffer("Handled "+count+" cells");
    UniversalTellUser.info(f, sb.toString());
    f.repaint();
  }

}


