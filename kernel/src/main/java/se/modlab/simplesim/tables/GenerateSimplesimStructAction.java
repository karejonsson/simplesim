package se.modlab.simplesim.tables;

import java.awt.event.*;

import javax.swing.*;

import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.tables.editor.koders.*;
import se.modlab.generics.sstruct.tables.*;

public class GenerateSimplesimStructAction extends AbstractAction
{
  public JFrame f;
  public JSpreadsheet ss; 
  public String catalog;

  public GenerateSimplesimStructAction(JFrame f, JSpreadsheet _ss, String _catalog)
  {
    super("Generate Simplesim struct");
    f = f;
    ss = _ss;
    catalog = _catalog;
  }
  
  private static final String letters = "abcdefghijklmnopqrstuvwxyz���0123456789ABCDEFGHILJKLMNOPQRSTUVWXYZ���_";
  
  private static String mangle(String s) {
	  for(int i = 0 ; i < s.length() ; i++) {
		  if(letters.indexOf(s.charAt(i)) == -1) s = s.replace(s.charAt(i), '_');
	  }
	  return s;
  }

  private boolean qualifies(Cell c) {
	    if(c == null) return true;
	    if(c.toString().trim().compareTo("") == 0) {
	      return true;
	    }
	    if(c.getType() == Cell.NUMBER) return true;
	    try {
	      if(TabParser.parseDatetimeFromString(c.toString().trim())) {
	        return true;
	      }
	    }
	    catch(Exception e) {
	      //signalError(e.getMessage(), c);
	    }
	    return false;
	  }

  private String getTypeAsString(Cell c) {
	    if(c == null) return null;
	    if(c.toString().trim().compareTo("") == 0) {
	      return null;
	    }
	    if(c.getType() == Cell.NUMBER) return "double";
	    try {
		    if(TabParser.parseDatetimeFromString(c.toString().trim())) {
		       return "double";
		    }
		}
		catch(Exception e) {
		}
		try {
			if(c.toString().trim().toLowerCase().compareTo("true") == 0) {
			   return "boolean";
			}
			if(c.toString().trim().toLowerCase().compareTo("false") == 0) {
			   return "boolean";
			}
		}
		catch(Exception e) {
			//signalError(e.getMessage(), c);
		}
	    return null;
	  }

  public static final char del = ';';

  private void add(Cell c, StringBuffer sb) {
    if(c == null) {
      sb.append(" ");
      return;
    }
    if(c.toString().trim().compareTo("") == 0) {
      sb.append(" ");
      return;
    }
    if(c.getType() == Cell.NUMBER) {
      sb.append(c.toString());
    }
    try {
      if(TabParser.parseDatetimeFromString(c.toString().trim())) {
        sb.append(c.toString().trim());
      }
    }
    catch(Exception e) {
      //signalError(e.getMessage(), c);
    }
  }

  private void signalError(String msg, Cell c) {
    UniversalTellUser.error(f, 
      "Simplesim cannot generate a simulation script struct from this sheet.\n\n"+
      msg+"\n\n"+
          "All fields should be numeric or blanc or a date."+((c != null) ? " Here it was \n"+
	      "\""+c.toString()+"\"" : "")+"\n\n"+
	      "The exception is in the first row where you can have textual titles\n"+
	      "but then there must be a second row from which the type of the\n"+
	      "column can be deduced.");
  }
  
  private boolean isTextual(Cell c) {
	  return c.getType() == Cell.TEXT; 
  }

	  
  private boolean isFirstRowPurelyTextual() {
	  int columns = ss.getColumnCount();
      for(int i = 1 ; i < columns ; i++) 
      {
        Cell c = ss.getCellAt(0, i);
        if(!isTextual(c)) return false;
      }
      return true;
  }

  public void actionPerformed(ActionEvent e)
  {
	int rows = ss.getRowCount();
	int columns = ss.getColumnCount();
	    
	int startRow = 0;
	if(isFirstRowPurelyTextual()) {
		startRow = 1;
	} 
    else {
        signalError(
        		"The first row is not of purely textual content.\n"+
        		"When generating structs this is necessary.", null);
        return;    	
    }
	if(columns < (startRow+1)) {
		if(startRow == 1) {
			signalError(
	        		"There is a purely textual initiating line for column titles\n"+
	        		"so the lowest allowed number of lines is 2 but there was "+columns, null);
		}
		if(startRow == 0) {
			signalError(
	        		"There is no purely textual initiating line for column titles\n"+
	        		"so the lowest allowed number of lines is 1 but there was "+columns, null);
		}
		return;
	}
	
		
    for(int i = startRow ; i < rows ; i++)
    {
      Cell c = ss.getCellAt(i, 0);
      if(!qualifies(c)) {
        signalError("Field "+(i+1)+", 0 is not ok.", c);
        return;
      }
      for(int ii = 1 ; ii < columns ; ii++) 
      {
        c = ss.getCellAt(i, ii);
        if(!qualifies(c)) {
          signalError("Field "+(i+1)+", "+ii+" is not ok.", c);
          return;
        }
      }
    }
    StringBuffer sb = new StringBuffer();
    sb.append("  typedef struct structname {\n");
    for(int i = 0 ; i < columns ; i++) {
        Cell c_type = ss.getCellAt(1, i);
        Cell c_name = ss.getCellAt(0, i);
        String type = getTypeAsString(c_type);
        if(type == null) {
			signalError(
	        		"The second line should be filled when the first has textual\n"+
	        		"contents for column titles. In column "+i+", row 2 it was.", null);
			return;
        }
    	sb.append("    "+type+" "+mangle(c_name.getValue().toString())+";\n");
    }
    sb.append("  };");
    //System.out.println("Struct:\n"+sb.toString());
    
    String struct = sb.toString();
    String message = 
    	  "The Simplesim script struct is\n\n"+
    	  struct;
    Object[] options = { "OK", "COPY+OK" };
    int choice = JOptionPane.showOptionDialog(null, 
    		 message, 
             "Calculated Simplesim scripting struct",
             JOptionPane.DEFAULT_OPTION, 
             JOptionPane.INFORMATION_MESSAGE,
             null, 
             options, 
             options[1]);
    if(choice == 1) {	
    	  JTextArea editor = new JTextArea();
    	  editor.setText(struct);
    	  editor.setSelectionStart(0);
    	  editor.setSelectionEnd(struct.length());
    	  editor.copy();
    }
  }


}



