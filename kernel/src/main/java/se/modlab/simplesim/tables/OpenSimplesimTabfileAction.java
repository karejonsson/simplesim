package se.modlab.simplesim.tables;

import java.io.*;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.tables.editor.koders.*;
import se.modlab.generics.sstruct.tables.*;

public class OpenSimplesimTabfileAction extends AbstractAction
{
  public Component c;
  public JSpreadsheet ss;
  public String catalog;

  public OpenSimplesimTabfileAction(Component _c, JSpreadsheet _ss, String _catalog)
  {
    super("Open Tabfile");
    c = _c;
    ss = _ss;
    catalog = _catalog;
  }

  protected static Object getObject(Holder h) 
  {
    if(h instanceof DatetimeHolder)
    {
      return "DATETIME";//((datetimeHolder) h).getString();
    }
    if(h instanceof LongHolder)
    {
      return new Long(((LongHolder) h).getLong());
    }
    if(h instanceof DoubleHolder)
    {
      return new Double(((DoubleHolder) h).getDouble());
    }
    if(h instanceof BooleanHolder)
    {
      return new Boolean(((BooleanHolder) h).getBoolean());
    }
    if(h instanceof StringHolder)
    {
      return ((StringHolder) h).getString();
    }
    return null;
  }

  public void actionPerformed(ActionEvent e)
  {
    TableHolder t = null;
    try
    {
      JFileChooser fc = new JFileChooser(catalog);
      fc.setDialogTitle("Open tabfile. Point to the file. (With extension .tab)");
      fc.setDialogType(JFileChooser.OPEN_DIALOG);
      fc.showSaveDialog(UniversalTellUser.getFrame(c));
      File selFile = fc.getSelectedFile();
      if(selFile == null) return;
      String filename = selFile.getName();
      if(filename == null) return;
      if(!(filename.endsWith(".tab")))
      {
        UniversalTellUser.error(
          UniversalTellUser.getFrame(c), 
          "The filename must end with .tab", 
          "File not valid");
        return;
      }
      t = TabParser.parseFromFilename(filename, new SpreadsheetDatetimeHandler());
      updatetableModel(t);
    }
    catch(Exception __e)
    {
      UniversalTellUser.general(UniversalTellUser.getFrame(c), 
                                new UnclassedError("SaveAsSimplesimTabfileAction - tab-generation", __e));
      return;
    }
  }

  protected void updatetableModel(TableHolder t) {
	  _updatetableModel(t, ss);
  }

  protected static void _updatetableModel(TableHolder t, JSpreadsheet ss) {
	    int rows = t.getNoRows();
	    int columns = t.getNoColumns();
	    
	    ss.newTableModel(rows, columns);

	    for(int i = 0 ; i < rows ; i++)
	    {
	      for(int ii = 0 ; ii < columns ; ii++) 
	      {
	        ss.setValueAt(getObject(t.getValue(i, ii)), i, ii);
	      }
	    }	  
  }

  public static void tabfileOpen(String filename)
  {
    TableHolder t = null;
    try
    {
      if(!(filename.endsWith(".tab")))
      {
        UniversalTellUser.error(
          null, 
          "The filename must end with .tab", 
          "File not valid");
        return;
      }
      t = TabParser.parseFromFilename(filename, new SpreadsheetDatetimeHandler());
    }
    catch(Exception __e)
    {
      UniversalTellUser.general(null, 
                                new UnclassedError("SaveAsSimplesimTabfileAction - tab-generation", __e));
      return;
    }

    JSpreadsheet ss = new JSpreadsheet(40, 40);
    SpreadsheetWindow_excel excel = new SpreadsheetWindow_excel(ss);
    _updatetableModel(t, ss);
    JFrame frame = SpreadsheetWindow_excel.getFrame("", excel);
    frame.pack();
    frame.setVisible(true);
  }

}



