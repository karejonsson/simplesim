package se.modlab.simplesim.evaluables;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.arithmetics.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.bags.*;

public class ArithmeticEvaluableGetFromDoubleBag implements ArithmeticEvaluable
{

	private VariableLookup vl = null;
	private ArithmeticEvaluable ae = null;

	public ArithmeticEvaluableGetFromDoubleBag(VariableLookup _vl, ArithmeticEvaluable _ae) {
		vl = _vl;
		ae = _ae;
	}

	public VariableLookup getVariableReference() {
		return vl;
	}
	
	public ArithmeticEvaluable getIndexExpression() {
		return ae;
	}

	public sValue evaluate(Scope s)
	throws IntolerableException
	{
		VariableInstance vi = vl.getInstance(s);
		if(!(vi instanceof DoubleBag))
		{
			throw new InternalError( 
					"Bag should have been of arithemitc type but was of class\n"+
					vi.getClass().getName()+". This should have been verified on\n"+
			"parsing! (se.xyz.simulation.evaluables.arithmeticEvaluableGetFromDoubleBag)");
		}
		DoubleBag ab = (DoubleBag) vi;
		//sValue out = null;
		//out = ab.get();
		//return out;
		sValue out = null;
		if(ae == null) {
			out = ab.get();
		}
		else {
			sValue idx = ae.evaluate(s);
			if(!(idx instanceof sLong)) {
				throw new UserCompiletimeError(
						"Expression "+ae+", evaluates to "+idx+" which is not long. "+s);
			}
			sLong lidx = (sLong) idx;
			out = ab.get((int)lidx.getLong().longValue());
		}
		return out;
	}

	public String toString()
	{
		if(ae == null) {
			return "get("+vl+")";
		}
		return "get("+vl+", "+ae+")";
	}

	public void verify(Scope s) throws IntolerableException {
		VariableInstance vi = vl.getInstance(s);
		if(!(vi instanceof DoubleBag))
		{
			throw new InternalError( 
					"Bag should have been of arithemitc type but was of class\n"+
					vi.getClass().getName()+". This should have been verified on\n"+
			"parsing! (se.xyz.simulation.evaluables.arithmeticEvaluableGetFromDoubleBag)");
		}
	}
	
	public String reproduceExpression() {
		if(ae == null) {
			return "get("+vl.reproduceExpression()+")";
		}
		return "get("+vl.reproduceExpression()+", "+ae.reproduceExpression()+")";
	}


}
