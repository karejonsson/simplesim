package se.modlab.simplesim.evaluables;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.references.*;
import se.modlab.simplesim.events.*;
import se.modlab.simplesim.bags.*;

public class ReferenceEvaluableGetFromBag
implements ReferenceEvaluable
{

	private VariableLookup vl = null;
	private ArithmeticEvaluable ae = null;

	public ReferenceEvaluableGetFromBag(VariableLookup _vl, ArithmeticEvaluable _ae)
	{ 
		vl = _vl;
		ae = _ae;
	}

	public VariableLookup getVariableReference()
	{
		return vl;
	}

	public sValue evaluate(Scope s)
	throws IntolerableException
	{
		VariableInstance vi = vl.getInstance(s);
		if(!(vi instanceof ComplexBag))
		{
			throw new UserRuntimeError(
					vl.toString()+" is not a bag holding structs or queuables!");
		}
		ComplexBag cb = (ComplexBag) vi;
		sValue out = null;
		if(ae == null) {
			out = cb.get();
		}
		else {
			sValue idx = ae.evaluate(s);
			if(!(idx instanceof sLong)) {
				throw new UserCompiletimeError(
						"Expression "+ae+", evaluates to "+idx+" which is not long. "+s);
			}
			sLong lidx = (sLong) idx;
			out = cb.get((int)lidx.getLong().longValue());
		}
		return out;
		//System.out.println("referenceEvaluableGetFromBag.evaluate cb="+cb+", class "+cb.getClass().getName());
		//return cb.get();
	}

	public String toString()
	{
		if(ae == null) {
			return "get("+vl+")";
		}
		return "get"+vl+", "+ae+")";
	}

	public void verify(Scope s) throws IntolerableException {
		VariableInstance vi = vl.getInstance(s);
		if(!(vi instanceof ComplexBag))
		{
			throw new UserRuntimeError(
					vl.toString()+" is not a bag holding structs or queuables!");
		}
	}
	
	public String reproduceExpression() {
		if(ae == null) {
			return "get("+vl.reproduceExpression()+")";
		}
		return "get("+vl.reproduceExpression()+", "+ae.reproduceExpression()+")";
	}


}
