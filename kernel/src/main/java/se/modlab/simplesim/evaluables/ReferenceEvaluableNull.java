package se.modlab.simplesim.evaluables;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.references.*;

public class ReferenceEvaluableNull
implements ReferenceEvaluable
{

	public ReferenceEvaluableNull()
	{ 
	}

	public sValue evaluate(Scope s)
	throws IntolerableException
	{
		return null;
	}


	public String toString()
	{
		return "null";
	}

	public void verify(Scope s) throws IntolerableException {
	}
	
	public String reproduceExpression() {
		return "null";
	}


}
