package se.modlab.simplesim.evaluables;

import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.events.*;
import se.modlab.simplesim.references.*;
import se.modlab.generics.exceptions.*;

public class LogicalExpressionStructComparison implements LogicalExpression
{

	private VariableLookup vl_l;
	private VariableLookup vl_r;

	public LogicalExpressionStructComparison(VariableLookup _vl_l, VariableLookup _vl_r)
	{  
		vl_l = _vl_l;
		vl_r = _vl_r;
	}

	private Complex dereferenced(Complex e)
	throws IntolerableException
	{
		if(!(e instanceof StructReference))
		{
			return e;
		}
		return ((sStruct) (((StructReference) e).getValue())).getStruct();
	}

	public boolean evaluate(Scope s)
	throws IntolerableException
	{
		VariableInstance e_l = vl_l.getInstance(s);
		VariableInstance e_r = vl_r.getInstance(s);
		if(!(e_l instanceof Complex))
		{
			throw new UserRuntimeError(vl_l.toString()+" does not refer to a struct.");
		}
		if(!(e_r instanceof Complex))
		{
			throw new UserRuntimeError(vl_r.toString()+" does not refer to a struct.");
		}
		return dereferenced((Complex) e_l) == dereferenced((Complex) e_r);
	}

	public String toString()
	{
		return vl_l.toString()+" == "+vl_r.toString();
	}

	public void verify(Scope s) throws IntolerableException {
		VariableInstance e_l = vl_l.getInstance(s);
		VariableInstance e_r = vl_r.getInstance(s);
		if(!(e_l instanceof Complex))
		{
			throw new UserRuntimeError(vl_l.toString()+" does not refer to a struct.");
		}
		if(!(e_r instanceof Complex))
		{
			throw new UserRuntimeError(vl_r.toString()+" does not refer to a struct.");
		}
	}
	
	public String reproduceExpression() {
		return "("+vl_l.reproduceExpression()+" == "+vl_r.reproduceExpression()+")";
	}


}
