package se.modlab.simplesim.evaluables;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.bags.*;

public class LogicalExpressionPeekFromBooleanBag 
implements LogicalExpression
{

	private VariableLookup vl = null;
	private ArithmeticEvaluable ae = null;

	public LogicalExpressionPeekFromBooleanBag(VariableLookup _vl, ArithmeticEvaluable _ae)
	{
		vl = _vl;
		ae = _ae;
	}

	public boolean evaluate(Scope s)
	throws IntolerableException
	{
		VariableInstance vi = vl.getInstance(s);
		if(!(vi instanceof BooleanBag))
		{
			throw new InternalError( 
					"Bag should have been of boolean type but was of class\n"+
					vi.getClass().getName()+". This should have been verified on\n"+
			"parsing! (se.xyz.simulation.evaluables.logicalExpressionGetFromBooleanBag)");
		}
		BooleanBag ab = (BooleanBag) vi;
		sBoolean out = null;
		if(ae == null) {
			out = (sBoolean) ab.get();
		}
		else {
			sValue idx = ae.evaluate(s);
			if(!(idx instanceof sLong)) {
				throw new UserCompiletimeError(
						"Expression "+ae+", evaluates to "+idx+" which is not long. "+s);
			}
			sLong lidx = (sLong) idx;
			out = (sBoolean) ab.get((int)lidx.getLong().longValue());
		}
		return out.getBoolean();
		//sBoolean b = (sBoolean) ab.peek();
		//return b.getBoolean().booleanValue();
	}

	public String toString()
	{
		if(ae == null) {
			return "peek("+vl+")";
		}
		return "peek("+vl+", "+ae+")";
	}

	public void verify(Scope s) throws IntolerableException {
		VariableInstance vi = vl.getInstance(s);
		if(!(vi instanceof BooleanBag))
		{
			throw new InternalError( 
					"Bag should have been of boolean type but was of class\n"+
					vi.getClass().getName()+". This should have been verified on\n"+
			"parsing! (se.xyz.simulation.evaluables.logicalExpressionGetFromBooleanBag)");
		}
	}
	
	public String reproduceExpression() {
		if(ae == null) {
			return "peek("+vl.reproduceExpression()+")";
		}
		return "peek("+vl.reproduceExpression()+", "+ae.reproduceExpression()+")";
	}

}
