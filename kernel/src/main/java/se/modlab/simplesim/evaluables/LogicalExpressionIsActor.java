package se.modlab.simplesim.evaluables;

import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.events.*;
import se.modlab.generics.exceptions.*;

public class LogicalExpressionIsActor implements LogicalExpression
{

	private VariableLookup vl;
	private String stateName;
	private String filename;
	private int beginLine;
	private int beginColumn;

	public LogicalExpressionIsActor(VariableLookup _vl,
			String _filename,
			int _beginLine,
			int _beginColumn)
	{  
		vl = _vl;
		filename = _filename;
		beginLine = _beginLine;
		beginColumn = _beginColumn;
	}

	public boolean evaluate(Scope s)
	throws IntolerableException
	{
		VariableInstance vi = null;
		try  {
			vi = vl.getInstance(s);
		}
		catch(UserRuntimeError ue) {
			String m = ue.getMessage()+"\n"+"This happened in file "+filename+", line "+beginLine+", column "+beginColumn;
			UserRuntimeError ue2 = new UserRuntimeError(m, ue.getCause(), ue.getCollectors());
			ue2.setAction(ue.getAction());
			throw ue2;
		}
		catch(UserCompiletimeError ue) {
			String m = ue.getMessage()+"\n"+"This happened in file "+filename+", line "+beginLine+", column "+beginColumn;
			UserCompiletimeError ue2 = new UserCompiletimeError(m, ue.getCause(), ue.getCollectors());
			ue2.setAction(ue.getAction());
			throw ue2;
		}
		return (vi instanceof ProcedureActor);
	}

	public String toString()
	{
		return "isActor("+vl+")";
	}

	public void verify(Scope s) throws IntolerableException {
		VariableInstance vi = null;
		try  {
			vi = vl.getInstance(s);
		}
		catch(UserRuntimeError ue) {
			String m = ue.getMessage()+"\n"+"This happened in file "+filename+", line "+beginLine+", column "+beginColumn;
			UserRuntimeError ue2 = new UserRuntimeError(m, ue.getCause(), ue.getCollectors());
			ue2.setAction(ue.getAction());
			throw ue2;
		}
		catch(UserCompiletimeError ue) {
			String m = ue.getMessage()+"\n"+"This happened in file "+filename+", line "+beginLine+", column "+beginColumn;
			UserCompiletimeError ue2 = new UserCompiletimeError(m, ue.getCause(), ue.getCollectors());
			ue2.setAction(ue.getAction());
			throw ue2;
		}
	}
	
	public String reproduceExpression() {
		return "("+vl.reproduceExpression()+" isActor "+stateName+")";
	}


}
