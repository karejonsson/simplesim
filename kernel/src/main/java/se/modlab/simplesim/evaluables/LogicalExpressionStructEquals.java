package se.modlab.simplesim.evaluables;

import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.simplesim.events.*;
import se.modlab.simplesim.references.*;
import se.modlab.generics.exceptions.*;

public class LogicalExpressionStructEquals implements LogicalExpression
{

	private ReferenceEvaluable re_l;
	private ReferenceEvaluable re_r;

	public LogicalExpressionStructEquals(ReferenceEvaluable _re_l, 
			ReferenceEvaluable _re_r)
	{  
		re_l = _re_l;
		re_r = _re_r;
	}

	private Complex dereferenced(Complex e)
	throws IntolerableException
	{
		if(e == null) return null;
		if(!(e instanceof StructReference))
		{
			return e;
		}
		return ((sStruct) (((StructReference) e).getValue())).getStruct();
	}

	public boolean evaluate(Scope s)
	throws IntolerableException
	{
		sValue ve_l = re_l.evaluate(s);//vl_l.getInstance(s);
		sValue ve_r = re_r.evaluate(s);//vl_r.getInstance(s);
		Complex e_l = ((sStruct) ve_l).getStruct();
		/*
    if(e_l == null)
    {
      throw new userError(re_l.toString()+" does not refer to a struct.");
    }
		 */
		Complex e_r = ((sStruct) ve_r).getStruct();
		/*
    if(e_r == null)
    {
      throw new userError(re_r.toString()+" does not refer to a struct.");
    }
		 */
		Complex c_l = dereferenced((Complex) e_l);
		Complex c_r = dereferenced((Complex) e_r);
		//if(c_l == c_r) return true;
		//if(c_l.getType() != c_r.getType()) return false;
		return c_l == c_r;
	}

	public String toString()
	{
		return re_l.toString()+" == "+re_r.toString();
	}

	public void verify(Scope s) throws IntolerableException {
		re_l.verify(s);//vl_l.getInstance(s);
		re_r.verify(s);//vl_r.getInstance(s);
	}
	
	public String reproduceExpression() {
		return "("+re_l.reproduceExpression()+" == "+re_r.reproduceExpression()+")";
	}


}
