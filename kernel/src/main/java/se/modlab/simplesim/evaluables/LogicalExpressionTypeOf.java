package se.modlab.simplesim.evaluables;

import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.simplesim.events.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.references.*;

public class LogicalExpressionTypeOf implements LogicalExpression
{

	private ReferenceEvaluable re;
	private String typeName;
	private int construct;

	public LogicalExpressionTypeOf(ReferenceEvaluable _re,
			String _typeName,
			int _construct)
	{  
		re = _re;
		typeName = _typeName;
		construct = _construct;
	}

	public boolean evaluate(Scope s)
	throws IntolerableException
	{
		sValue val = re.evaluate(s);
		/*
    System.out.println(
        "1: logicalExpressionTypeOf - "+val);
		 */
		VariableInstance vi = null;
		if(val instanceof sQueuable)
		{
			vi = (VariableInstance) ((sQueuable) val).getQueuable();
			if(vi == null)
			{
				throw new UserRuntimeError(re.toString()+" evaluates to null.");
			}
			VariableFactory vf = s.getFactory(typeName);
			return vi.getType() == vf;
		}
		if(val instanceof sStruct)
		{
			vi = (VariableInstance) ((sStruct) val).getStruct();
			if(vi == null)
			{
				return true;
				//throw new userError(re.toString()+" evaluates to null.");
			}
			VariableFactory vf = s.getFactory(typeName);
			/*
      System.out.println(
        "2: logicalExpressionTypeOf - sStruct\n"+
        "L: Variable is "+vi+" of class "+vi.getClass().getName()+" of type "+vi.getType()+" which is type "+vi.getType().getTypesName()+
        "\nR: "+vf+" which is type "+vf.getTypesName());
			 */
			return vi.getType() == vf;
		}
		throw new InternalError(
				"The logic in logicalExpressionTypeOf is outdated or wrong.\n"+
				"The actual type of val is "+val.getClass().getName());
	}

	public String toString()
	{
		if(construct == 0)
		{
			return "typeOf("+re.toString()+", "+typeName+")";
		}
		if(construct == 1)
		{
			return re.toString()+" typeOf "+typeName;
		}
		return "Method toString of class logicalExpressionTypeOf failed.\n"+
		"Number was "+construct;
	}

	public void verify(Scope s) throws IntolerableException {
		re.verify(s);
	}
	
	public String reproduceExpression() {
		return "("+re.reproduceExpression()+" typeOf "+typeName+")";
	}


}
