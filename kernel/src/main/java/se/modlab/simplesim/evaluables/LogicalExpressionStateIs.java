package se.modlab.simplesim.evaluables;

import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.simplesim.events.*;
import se.modlab.simplesim.references.*;
import se.modlab.generics.exceptions.*;

public class LogicalExpressionStateIs implements LogicalExpression
{

	private ReferenceEvaluable re;
	private String stateName;

	public LogicalExpressionStateIs(ReferenceEvaluable _re,
			String _stateName)
	{  
		re = _re;
		stateName = _stateName;
	}

	public boolean evaluate(Scope s)
	throws IntolerableException
	{
		sValue val = re.evaluate(s);
		if(!(val instanceof sQueuable))
		{
			//System.out.println("logicalExpressionStateIs: Type "+val.getClass().getName());
			VariableInstance vi = (VariableInstance) ((sStruct) val).getStruct();
			//System.out.println("logicalExpressionStateIs: IntType "+vi.getClass().getName());
			//System.out.println("logicalExpressionStateIs: ScopeType "+s.getClass().getName());

			throw new UserRuntimeError(re.toString()+" does not refer to an actor.");
		}
		VariableInstance vi = (VariableInstance) ((sQueuable) val).getQueuable();
		if(vi instanceof ProcedureActor)
		{
			ProcedureActor pa = (ProcedureActor) vi;
			return pa.getCurrentStateName().compareTo(stateName) == 0;
		}
		if(vi instanceof ProcedureActorExported)
		{
			ProcedureActorExported pa = (ProcedureActorExported) vi;
			return pa.getCurrentStateName().compareTo(stateName) == 0;
		}
		throw new InternalError(
				"The logic in logicalExpressionStateIs is outdated.");
	}

	public String toString()
	{
		return "stateIs("+re+", "+stateName+")";
	}

	public void verify(Scope s) throws IntolerableException {
		re.verify(s);
	}

	public String reproduceExpression() {
		return "("+re.reproduceExpression()+" stateIs "+stateName+")";
	}

}
