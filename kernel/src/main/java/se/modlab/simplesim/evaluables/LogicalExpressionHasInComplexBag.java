package se.modlab.simplesim.evaluables;

import se.modlab.generics.exceptions.UserRuntimeError;
import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.sstruct.comparisons.Scope;
import se.modlab.generics.sstruct.comparisons.VariableLookup;
import se.modlab.generics.sstruct.values.sValue;
import se.modlab.generics.sstruct.variables.VariableInstance;
import se.modlab.simplesim.bags.Bag;
import se.modlab.simplesim.bags.ComplexBag;
import se.modlab.simplesim.references.ReferenceEvaluable;

public class LogicalExpressionHasInComplexBag extends LogicalExpressionHasInBag {
	
	private ReferenceEvaluable re = null;
	
	public LogicalExpressionHasInComplexBag(VariableLookup vl, String filename, int line, int column, ReferenceEvaluable re) {
		super(vl, filename, line, column);
		this.re = re;
	}

	public boolean evaluate(Scope s) throws IntolerableException {
		VariableInstance vi = vl.getInstance(s);
		if(!(vi instanceof Bag)) {
			String m = "Variable "+vl.reproduceExpression()+" defined in file "+vi.getFilename()+", line "+vi.getLine()+", column "+vi.getColumn()+
					"\nis used as a bag in has-test but is not of bag type. has-invokation on "+getPlace();
			UserRuntimeError ue = new UserRuntimeError(m);
			throw ue;
		}
		if(!(vi instanceof ComplexBag)) {
			String m = "Bag is not for complex instance. Asking \"has\" for complex instance is illegal. Happened to bag defined in file "+vi.getFilename()+", line "+vi.getLine()+", column "+vi.getColumn()+" to has-invokation on "+getPlace();
			UserRuntimeError ue = new UserRuntimeError(m);
			throw ue;
		}
		ComplexBag cb = (ComplexBag) vi;
		sValue val = re.evaluate(s);
		return cb.has(val);
	}

	public String reproduceExpression() {
		return "has("+vl.reproduceExpression()+", "+re.reproduceExpression()+")";
	}

	public void verify(Scope s) throws IntolerableException {
		VariableInstance vi = vl.getInstance(s);
		if(!(vi instanceof ComplexBag)) {
			String m = "Bag is not for complex instances. Asking \"has\" for complex instances is illegal. Happened to bag defined in file "+vi.getFilename()+", line "+vi.getLine()+", column "+vi.getColumn()+" to has-invokation on "+getPlace();
			UserRuntimeError ue = new UserRuntimeError(m);
			throw ue;
		}
	}

}
