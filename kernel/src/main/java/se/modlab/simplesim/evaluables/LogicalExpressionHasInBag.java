package se.modlab.simplesim.evaluables;

import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.sstruct.comparisons.ArithmeticEvaluable;
import se.modlab.generics.sstruct.comparisons.Scope;
import se.modlab.generics.sstruct.comparisons.VariableLookup;
import se.modlab.generics.sstruct.logics.LogicalExpression;

public abstract class LogicalExpressionHasInBag implements LogicalExpression {

	protected VariableLookup vl = null;
	private String filename;
	private int line;
	private int column;

	public LogicalExpressionHasInBag(VariableLookup vl, String filename, int line, int column) {
		this.vl = vl;
		this.filename = filename;
		this.line = line;
		this.column = column;
	}
	
	public String getFilename() {
		return filename;
	}
	
	public int getLine() {
		return line;
	}

	public int getColumn() {
		return column;
	}
	
	public String getPlace() {
		return "file "+filename+", line "+line+", column "+column;
	}
		
}
