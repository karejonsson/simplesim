package se.modlab.simplesim.evaluables;

import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.simplesim.events.*;
import se.modlab.simplesim.references.*;
import se.modlab.generics.exceptions.*;

public class LogicalExpressionEnqueuableNotEqual implements LogicalExpression
{

	private ReferenceEvaluable re_l;
	private ReferenceEvaluable re_r;

	public LogicalExpressionEnqueuableNotEqual(ReferenceEvaluable _re_l, 
			ReferenceEvaluable _re_r)
	{  
		re_l = _re_l;
		re_r = _re_r;
	}

	private Enqueuable dereferenced(Enqueuable e)
	throws IntolerableException
	{
		if(!(e instanceof QueuableReference))
		{
			return e;
		}
		return ((sQueuable) (((QueuableReference) e).getValue())).getQueuable();
	}

	public boolean evaluate(Scope s)
	throws IntolerableException
	{
		sValue ve_l = re_l.evaluate(s);//vl_l.getInstance(s);
		sValue ve_r = re_r.evaluate(s);//vl_r.getInstance(s);
		Enqueuable e_l = ((sQueuable) ve_l).getQueuable();
		if(e_l == null)
		{
			throw new UserRuntimeError(re_l.toString()+" does not refer to a queuable.");
		}
		Enqueuable e_r = ((sQueuable) ve_r).getQueuable();
		if(e_r == null)
		{
			throw new UserRuntimeError(re_r.toString()+" does not refer to a queuable.");
		}
		return dereferenced((Enqueuable) e_l) != dereferenced((Enqueuable) e_r);
	}

	public String toString()
	{
		return re_l.toString()+" == "+re_r.toString();
	}

	public void verify(Scope s) throws IntolerableException {
		re_l.verify(s);//vl_l.getInstance(s);
		re_r.verify(s);//vl_r.getInstance(s);
	}
	
	public String reproduceExpression() {
		return re_l.reproduceExpression() + " != "+ re_r.reproduceExpression();
	}


}
