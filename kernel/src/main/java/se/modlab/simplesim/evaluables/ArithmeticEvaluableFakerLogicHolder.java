package se.modlab.simplesim.evaluables;

import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.events.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.comparisons.*;

public class ArithmeticEvaluableFakerLogicHolder 
implements ArithmeticEvaluable
{

	LogicalExpression le = null;

	public ArithmeticEvaluableFakerLogicHolder(LogicalExpression _le)
	{
		le = _le;
	}

	public LogicalExpression getLogicalExpression()
	{
		return le;
	}

	public sValue evaluate(Scope s)
	throws IntolerableException
	{
		return new sBoolean(le.evaluate(s));
	}

	public String toString()
	{
		return "arithmeticEvaluableFakerLogicHolder";
	}

	public void verify(Scope s) throws IntolerableException {
		le.verify(s);
	}
	
	public String reproduceExpression() {
		return le.reproduceExpression();
	}

}


