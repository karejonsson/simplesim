package se.modlab.simplesim.evaluables;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.references.*;
import se.modlab.simplesim.events.*;

public class ReferenceEvaluableVariableLookup
implements ReferenceEvaluable
{

	private VariableLookup vl = null;

	public ReferenceEvaluableVariableLookup(VariableLookup _vl)
	{ 
		vl = _vl;
	}

	public VariableLookup getVariableReference()
	{
		return vl;
	}

	public sValue evaluate(Scope s) throws IntolerableException {
		try {
			VariableInstance vi = vl.getInstance(s);
			if(vi == null)
			{
				throw new UserRuntimeError(
						"The reference "+vl+" is incorrect. No such instance!");
			}
			if(vi instanceof QueuableReference)
			{
				return ((QueuableReference) vi).getValue();
			}
			if(vi instanceof Enqueuable)
			{
				return new sQueuable((Enqueuable) vi);
			}
			if(vi instanceof StructReference)
			{
				return ((StructReference) vi).getValue();
			}
			if(vi instanceof Complex)
			{
				return new sStruct((Complex) vi);
			}
			// The case when the type of the variable has not been determined.
			// It could work but it is likley not to. This means that it will 
			// be a runtime error.
			return ((Variable) vi).getValue();  
		}
		catch(IntolerableException ie) {
			String mess = ie.getMessage();
			mess += "\nHappened at statement \""+vl.reproduceExpression()+"\" in file "+vl.getFilename()+", line "+vl.getLine()+", column "+vl.getColumn();
			throw new UserRuntimeError(mess, ie);
		}
	}

	public String toString()
	{
		return vl.toString();
	}

	public void verify(Scope s) throws IntolerableException {
		//System.out.println("referenceEvaluableVariableLookup.verify vl="+vl);
		vl.verify(s);
	}
	
	public String reproduceExpression() {
		return vl.reproduceExpression();
	}


}
