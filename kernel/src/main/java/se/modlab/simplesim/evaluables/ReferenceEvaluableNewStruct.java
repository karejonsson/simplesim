package se.modlab.simplesim.evaluables;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.references.*;
import se.modlab.simplesim.events.*;
import se.modlab.simplesim.bags.*;

public class ReferenceEvaluableNewStruct
implements ReferenceEvaluable
{

	private String typename = null;
	private String filename;
	private int line;
	private int column;

	public ReferenceEvaluableNewStruct(String _typename, String _filename, int _line, int _column)
	{ 
		typename = _typename;
		filename = _filename;
		line = _line;
		column = _column;
	}

	public String getTypesName()
	{
		return typename;
	}

	public sValue evaluate(Scope s)
	throws IntolerableException
	{
		VariableFactory vf = s.getFactory(typename);
		if(vf == null)
		{ 
			throw new InternalError(
					"Type "+typename+" not known in referenceEvaluableNewStruct.\n"+
			"This should have been verified at parsing.");
		}
		VariableInstance vi = vf.getInstance("_ _", filename, line, column);
		if(vi instanceof Enqueuable)
		{
			throw new InternalError(
					"Type "+typename+" is enqueuable in referenceEvaluableNewStruct.\n"+
			"This should have been verified at parsing.");
		}
		if(!(vi instanceof Complex))
		{
			throw new InternalError(
					"Type "+typename+" not complex in referenceEvaluableNewEnqueuable.\n"+
			"This should have been verified at parsing.");
		}
		vi.setDefaultInitialValue();
		return new sStruct((Complex)vi);
	}

	public String toString()
	{
		return "new("+typename+")";
	}

	public void verify(Scope s) throws IntolerableException {
		VariableFactory vf = s.getFactory(typename);
		if(vf == null)
		{ 
			throw new InternalError(
					"Type "+typename+" not known in referenceEvaluableNewStruct.\n"+
			"This should have been verified at parsing.");
		}
		VariableInstance vi = vf.getInstance("_ _", filename, line, column);
		if(vi instanceof Enqueuable)
		{
			throw new InternalError(
					"Type "+typename+" is enqueuable in referenceEvaluableNewStruct.\n"+
			"This should have been verified at parsing.");
		}
		if(!(vi instanceof Complex))
		{
			throw new InternalError(
					"Type "+typename+" not complex in referenceEvaluableNewEnqueuable.\n"+
			"This should have been verified at parsing.");
		}
		vi.setDefaultInitialValue();
	}
	
	public String reproduceExpression() {
		return "new("+typename+")";
	}


}