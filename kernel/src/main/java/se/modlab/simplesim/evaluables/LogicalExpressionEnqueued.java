package se.modlab.simplesim.evaluables;

import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.simplesim.events.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.references.*;

public class LogicalExpressionEnqueued implements LogicalExpression
{

	private ReferenceEvaluable re;
	//private String stateName;

	public LogicalExpressionEnqueued(ReferenceEvaluable _re)
	{  
		re = _re;
	}

	public boolean evaluate(Scope s)
	throws IntolerableException
	{
		sValue val = re.evaluate(s);
		Enqueuable eq = null;
		if(val instanceof sQueuable)
		{
			eq = (Enqueuable) ((sQueuable) val).getQueuable();
			return eq.isEnqueued();
		}
		if(val instanceof sStruct)
		{
			throw new UserRuntimeError(
					re.toString()+" evaluates to a struct.\n"+
			"Test for if it is enqueued is useless!");
		}
		throw new InternalError(
				"The logic in logicalExpressionEnqueued is outdated or wrong.\n"+
				"The actual type of val is "+val.getClass().getName());
	}

	public String toString()
	{
		return "enqueued("+re+")";
	}

	public void verify(Scope s) throws IntolerableException {
		sValue val = re.evaluate(s);
		Enqueuable eq = null;
		if(val instanceof sQueuable)
		{
			eq = (Enqueuable) ((sQueuable) val).getQueuable();
			return;
		}
		if(val instanceof sStruct)
		{
			throw new UserRuntimeError(
					re.toString()+" evaluates to a struct.\n"+
			"Test for if it is enqueued is useless!");
		}
		throw new InternalError(
				"The logic in logicalExpressionEnqueued is outdated or wrong.\n"+
				"The actual type of val is "+val.getClass().getName());
	}

	public String reproduceExpression() {
		return "enqueued("+re.reproduceExpression()+")";
	}

}
