package se.modlab.simplesim.evaluables;

import se.modlab.generics.exceptions.UserRuntimeError;
import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.sstruct.comparisons.Scope;
import se.modlab.generics.sstruct.comparisons.VariableLookup;
import se.modlab.generics.sstruct.logics.LogicalExpression;
import se.modlab.generics.sstruct.values.sBoolean;
import se.modlab.generics.sstruct.variables.VariableInstance;
import se.modlab.simplesim.bags.Bag;
import se.modlab.simplesim.bags.BooleanBag;

public class LogicalExpressionHasInBooleanBag extends LogicalExpressionHasInBag {
	
	private LogicalExpression le = null;
	
	public LogicalExpressionHasInBooleanBag(VariableLookup vl, String filename, int line, int column, LogicalExpression _le) {
		super(vl, filename, line, column);
		le = _le;
	}

	public boolean evaluate(Scope s) throws IntolerableException {
		VariableInstance vi = vl.getInstance(s);
		if(!(vi instanceof Bag)) {
			String m = "Variable "+vl.reproduceExpression()+" defined in file "+vi.getFilename()+", line "+vi.getLine()+", column "+vi.getColumn()+
					"\nis used as a bag in has-test but is not of bag type. has-invokation on "+getPlace();
			UserRuntimeError ue = new UserRuntimeError(m);
			throw ue;
		}
		if(!(vi instanceof BooleanBag)) {
			String m = "Bag is not for booleans. Asking \"has\" for a logical expression is illegal. Happened to bag defined in file "+vi.getFilename()+", line "+vi.getLine()+", column "+vi.getColumn()+" to has-invokation on "+getPlace();
			UserRuntimeError ue = new UserRuntimeError(m);
			throw ue;
		}
		BooleanBag bb = (BooleanBag) vi;
		boolean b = le.evaluate(s);
		return bb.has(new sBoolean(b));
	}

	public String reproduceExpression() {
		return "has("+vl.reproduceExpression()+", "+le.reproduceExpression()+")";
	}

	public void verify(Scope s) throws IntolerableException {
		VariableInstance vi = vl.getInstance(s);
		if(!(vi instanceof BooleanBag)) {
			String m = "Bag is not for booleans. Asking \"has\" for a logical expression is illegal. Happened to bag defined in file "+vi.getFilename()+", line "+vi.getLine()+", column "+vi.getColumn()+" to has-invokation on "+getPlace();
			UserRuntimeError ue = new UserRuntimeError(m);
			throw ue;
		}
	}

}
