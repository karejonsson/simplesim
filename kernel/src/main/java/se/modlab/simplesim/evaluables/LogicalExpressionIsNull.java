package se.modlab.simplesim.evaluables;

import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.simplesim.events.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.references.*;

public class LogicalExpressionIsNull implements LogicalExpression
{

	protected ReferenceEvaluable re;
	private String filename;
	private int beginLine;
	private int beginColumn;
	protected int construct;

	public LogicalExpressionIsNull(ReferenceEvaluable _re,
			String _filename,
			int _beginLine,
			int _beginColumn,
			int _construct)
	{  
		re = _re;
		filename = _filename;
		beginLine = _beginLine;
		beginColumn = _beginColumn;
		construct = _construct;
	}

	public boolean evaluate(Scope s)
	throws IntolerableException
	{
		sValue val = re.evaluate(s);
		VariableInstance vi = null;
		if(val instanceof sQueuable) {
			vi = (VariableInstance) ((sQueuable) val).getQueuable();
			if(vi == null) {
				return true;
			}
			if(vi instanceof Enqueuable) {
				return false;
			}
			if(!(vi instanceof QueuableReference)) { 
				System.out.println("QR: in se.modlab.simplesim.evaluables.LogicalExpressionIsNull, type is "+vi.getClass().getName());
				throw new UserRuntimeError(
						"The variable "+vi+" is tested for null in file "+filename+", on line "+
						beginLine+", column "+beginColumn+"\n"+
						"but it is not a reference!");
			}
			return ((QueuableReference) vi).isNull();
		}
		if(val instanceof sStruct) {
			vi = (VariableInstance) ((sStruct) val).getStruct();
			if(vi == null) {
				return true;
			}
			if(vi instanceof StructReference) {
				return ((StructReference) vi).isNull();
			}
			if(vi instanceof ComplexVariable) {
				return ((ComplexVariable) vi).isNull();
			}
			if(!(vi instanceof StructReference)) { 
				System.out.println("SR: in se.modlab.simplesim.evaluables.LogicalExpressionIsNull, type is "+vi.getClass().getName());
				throw new UserRuntimeError(
						"The variable "+vi+" is tested for null in file "+filename+", on line "+
						beginLine+", column "+beginColumn+"\n"+
				"but it is not a struct!");
			}
			return ((StructReference) vi).isNull();
		}
		throw new InternalError(
				"The logic in se.modlab.simplesim.evaluables.LogicalExpressionIsNull is outdated or wrong.\n"+
				"The actual type of val is "+val.getClass().getName());
	}

	public String toString()
	{
		if(construct == 0) {
			return "isNull("+re.toString()+")";
		}
		if(construct == 1) {
			return re.toString()+" == null";
		}
		return "Method toString of class se.modlab.simplesim.evaluables.LogicalExpressionIsNull failed.\n"+
		"Number was "+construct;
	}

	public void verify(Scope s) throws IntolerableException {
		re.verify(s);
	}
	
	public String reproduceExpression() {
		if(construct == 0) {
			return "isNull("+re.reproduceExpression()+")";
		}
		return "("+re.reproduceExpression()+" == null)";
	}

}
