package se.modlab.simplesim.evaluables;

import se.modlab.generics.exceptions.UserRuntimeError;
import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.sstruct.comparisons.ArithmeticEvaluable;
import se.modlab.generics.sstruct.comparisons.Scope;
import se.modlab.generics.sstruct.comparisons.VariableLookup;
import se.modlab.generics.sstruct.values.sBoolean;
import se.modlab.generics.sstruct.values.sValue;
import se.modlab.generics.sstruct.variables.VariableInstance;
import se.modlab.simplesim.bags.BooleanBag;
import se.modlab.simplesim.bags.DoubleBag;
import se.modlab.simplesim.bags.LongBag;
import se.modlab.simplesim.bags.Bag;

public class LogicalExpressionHasInArithmeticBag extends LogicalExpressionHasInBag {
	
	private ArithmeticEvaluable ae = null;
	
	public LogicalExpressionHasInArithmeticBag(VariableLookup vl, String filename, int line, int column, ArithmeticEvaluable _ae) {
		super(vl, filename, line, column);
		ae = _ae;
	}

	public boolean evaluate(Scope s) throws IntolerableException {
		VariableInstance vi = vl.getInstance(s);
		if(!(vi instanceof Bag)) {
			String m = "Variable "+vl.reproduceExpression()+" defined in file "+vi.getFilename()+", line "+vi.getLine()+", column "+vi.getColumn()+
					"\nis used as a bag in has-test but is not of bag type. has-invokation on "+getPlace();
			UserRuntimeError ue = new UserRuntimeError(m);
			throw ue;
		}
		if(vi instanceof LongBag) {
			LongBag lb = (LongBag) vi;
			sValue val = ae.evaluate(s);
			return lb.has(val);
		}
		if(vi instanceof DoubleBag) {
			DoubleBag db = (DoubleBag) vi;
			sValue val = ae.evaluate(s);
			return db.has(val);
		}
		String m = "Bag is not for arithmetic values. Asking \"has\" for arithmetic value is illegal. Happened to bag "+vl.reproduceExpression()+" defined in file "+vi.getFilename()+", line "+vi.getLine()+", column "+vi.getColumn()+" at has-invokation on "+getPlace();
		//System.out.println("LogicalExpressionHasInArithmeticBag.evaluate type "+vi.getClass().getName()+" : "+m);
		UserRuntimeError ue = new UserRuntimeError(m);
		throw ue;
	}

	public String reproduceExpression() {
		return "has("+vl.reproduceExpression()+", "+ae.reproduceExpression()+")";
	}

	public void verify(Scope s) throws IntolerableException {
		VariableInstance vi = vl.getInstance(s);
		if(vi instanceof LongBag) {
			return;
		}
		if(vi instanceof DoubleBag) {
			return;
		}
		String m = "Bag is not for arithmetic values. Asking \"has\" for arithmetic value is illegal. Happened to bag "+vl.reproduceExpression()+" defined in file "+vi.getFilename()+", line "+vi.getLine()+", column "+vi.getColumn()+" at has-invokation on "+getPlace();
		UserRuntimeError ue = new UserRuntimeError(m);
		throw ue;
	}

}
