package se.modlab.simplesim.evaluables;

import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.sstruct.comparisons.Scope;
import se.modlab.simplesim.references.ReferenceEvaluable;

public class LogicalExpressionIsNotNull extends LogicalExpressionIsNull {

	public LogicalExpressionIsNotNull(ReferenceEvaluable re,
			String filename,
			int beginLine,
			int beginColumn,
			int construct) {  
		super(re, filename, beginLine, beginColumn, construct);
	}
	
	public boolean evaluate(Scope s)
	throws IntolerableException
	{
		return !super.evaluate(s);
	}

	public String toString()
	{
		if(construct == 1 || construct == 0) {
			return re.toString()+" != null";
		}
		return "Method toString of class se.modlab.simplesim.evaluables.LogicalExpressionIsNull failed.\n"+
		"Number was "+construct;
	}

	public void verify(Scope s) throws IntolerableException {
		re.verify(s);
	}
	
	public String reproduceExpression() {
		if(construct == 0) {
			return "!isNull("+re.reproduceExpression()+")";
		}
		return "("+re.reproduceExpression()+" != null)";
	}

}
