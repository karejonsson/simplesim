package se.modlab.simplesim.scoping;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.variables.*;

public class ScopeReference extends VariableInstance implements Complex
{

  protected SimScope s = null;

  public ScopeReference(SimScope _s, String _name, String filename, int line, int column)
    throws IntolerableException
  {
    this(_s, _name, true, filename, line, column);
  }

  public ScopeReference(SimScope _s, String _name, boolean addOneself, String filename, int line, int column)
    throws IntolerableException
  {
    super(_name, new procedureScopeReferenceType(_name), filename, line, column);
    s = _s;
    if(addOneself) s.addComplexInstance(this);
  }

  public void setInitialValue(VariableInstance _val) 
    throws IntolerableException
  {
    throw new InternalError(
      "scopeReference.setInitialValue(variableInstance)\n"+
      "called with object of class "+_val.getClass().getName()+".\n"+
      "This method should not be called at all!");
  }

  public void setDefaultInitialValue()
    throws IntolerableException
  {
  }

  public void setValue(VariableInstance si)
    throws IntolerableException
  {
    throw new InternalError(
      "scopeReference.setValue was called\n"+
      "on scope reference "+name);
  }

  public void addMember(VariableInstance si)
    throws IntolerableException
  {
    throw new InternalError("Added member in scope reference named "+name);
  }

  public int getSize()
  {
    return s.getSize();
  }

  public VariableInstance getMember(int i)
    throws IntolerableException
  {
    return s.getMember(i);
    //throw new internalError("Get member in scope reference named "+name);
  }

  public VariableInstance getMember(String name)
    throws IntolerableException
  {
    //System.out.println("scope Reference: Get member "+name);
    return s.getComplexInstance(name);
  }

  public String toString()
  {
    return "Scope reference "+name+" on scope "+s;
  }

  private static class procedureScopeReferenceType implements VariableType
  {
    private String name;
    public procedureScopeReferenceType(String _name)
    {
      name = _name;
    }
    public String getTypesName()
    {
      return name;
    }
  }

@Override
public boolean isNull() {
	// TODO Auto-generated method stub
	return false;
}

public boolean valueEquals(VariableInstance sv) throws IntolerableException {
	if(!(sv instanceof Complex)) {
		return false;
	}
	Complex other = (Complex) sv;
	if(getSize() != other.getSize()) {
		return false;
	}
	for(int i = 0 ; i < getSize() ; i++) {
		VariableInstance vi = getMember(i);
		VariableInstance viother = other.getMember(vi.getName());
		if(viother == null) {
			return false;
		}
		if(!vi.valueEquals(viother)) {
			return false;
		}
	}
	return true;
}

}
