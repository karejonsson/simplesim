package se.modlab.simplesim.scoping;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;
import se.modlab.generics.exceptions.*;

import java.util.*;

public class ScopeReferenceExported extends VariableInstance
   implements Complex
{
  private ScopeReference inst;

  private static CreateExportedVersionVisitor cevv = null;
 
  static
  {
    try
    {
      cevv = CreateExportedVersionVisitor.getInstance();
    }
    catch(IntolerableException e)
    {
      //System.out.println("Commons laddning misslyckad. nsme");
    } 
  }

  public ScopeReferenceExported(ScopeReference _inst, String filename, int line, int column)
  {
    super(_inst.getName(), _inst.getType(), filename, line, column);
    inst = _inst;
  }

  public void addMember(VariableInstance si)
    throws IntolerableException
  {
    throw new InternalError(
      "SimComplexVariableExported.addMember was called on "+name);
  }
 
  public int getSize()
  {
    return inst.getSize();
  }

  public VariableInstance getMember(int i)
    throws IntolerableException
  {
    VariableInstance si = (VariableInstance) inst.getMember(i);
    si.accept(cevv);
    return cevv.getExportedVersion();
  }

  public VariableInstance getMember(String name)
    throws IntolerableException
  {
    for(int i = 0 ; i < inst.getSize() ; i++)
    {
      VariableInstance si = (VariableInstance) inst.getMember(i);
      if(name.compareTo(si.getName()) == 0) 
      {
        si.accept(cevv);
        return cevv.getExportedVersion();
      }
    }
    return null;
  }

  public void setInitialValue(VariableInstance _val) 
    throws IntolerableException
  {
    throw new InternalError(
      "scopeReferenceExported.setInitialValue(variableInstance)\n"+
      "called with object of class "+_val.getClass().getName()+".\n"+
      "This method should not be called at all!");
  }

  public void setDefaultInitialValue()
    throws IntolerableException
  {
    throw new InternalError(
      "SimComplexVariableExported.setDefaultInitialValue was called on "+name);
  }

  public void setValue(VariableInstance si)
    throws IntolerableException
  {
    throw new UserCompiletimeError(
      "Complex exported "+inst.getType().getTypesName()+" variable "+name+" set to new value "+si);
  }

@Override
public boolean isNull() {
	// TODO Auto-generated method stub
	return false;
}
	public boolean valueEquals(VariableInstance sv) throws IntolerableException {
		if(!(sv instanceof Complex)) {
			return false;
		}
		Complex other = (Complex) sv;
		if(getSize() != other.getSize()) {
			return false;
		}
		for(int i = 0 ; i < getSize() ; i++) {
			VariableInstance vi = getMember(i);
			VariableInstance viother = other.getMember(vi.getName());
			if(viother == null) {
				return false;
			}
			if(!vi.valueEquals(viother)) {
				return false;
			}
		}
		return true;
	}

}
