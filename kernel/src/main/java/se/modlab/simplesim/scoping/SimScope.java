package se.modlab.simplesim.scoping;

import java.util.*;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.simplesim.variables.*;

public class SimScope extends Scope implements Complex
{

	private Vector<VariableInstance> members = new Vector<VariableInstance>();

	public SimScope(String name)
	{
		super(name);
	}

	public SimScope(Scope subjacent)
	{
		super(subjacent);
	}

	public SimScope(Scope subjacent, String name)
	{
		super(subjacent, name);
	}

	public void removeSubjacent() 
	{
		subjacent = null;
	}

	public void addFactory(VariableFactory vf)
	throws IntolerableException
	{
		Object o = table_factories.get(vf.getTypesName());
		if(o != null) 
		{
			throw new UserCompiletimeError(
					"Second instanciation of type "+
					vf.getTypesName()+" in this scopes level. ("+name+")");
		}
		if(vf instanceof SimVectorVariableFactory)
		{

			SimVectorVariableFactory vvf = (SimVectorVariableFactory) vf;
			//System.out.println("FFFF "+vvf.getTypesName());
			VariableFactory subjacentFactory = vvf.getElementsFactory();
			SimScope s = (SimScope) getFactorysScope(subjacentFactory);
			if(s != null)
			{
				VariableFactory _vf = s.getFactory(vf.getTypesName());
				if(_vf == null)
				{
					//System.out.println("NU i "+s.getName()+" adderas "+vf.getTypesName());
					s.table_factories.put(vf.getTypesName(), vf);
				}
				return;
			}
			// subjacent factory and this factory are both unseen.
			table_factories.put(vf.getTypesName(), vf);
			table_factories.put(subjacentFactory.getTypesName(), subjacentFactory);
		}
		table_factories.put(vf.getTypesName(), vf);
	}

	public void addMember(VariableInstance si)
	throws IntolerableException
	{
		members.addElement(si);
	}

	public void removeMember(String name) throws IntolerableException
	{
		VariableInstance vi = getMember(name);
		if(vi != null) {
			members.remove(vi);
		}
	}

	public VariableType getType()
	{
		return null;
	}

	public int getSize()
	{
		return members.size();
	}

	public VariableInstance getMember(int i)
	throws IntolerableException
	{
		if(i < 0) return null;
		if(i >= members.size()) return null;
		return members.elementAt(i);
	}

	public VariableInstance getMember(String name)
	throws IntolerableException
	{
		for(int i = 0 ; i < members.size() ; i++)
		{
			VariableInstance si = members.elementAt(i);
			if(name.compareTo(si.getName()) == 0) return si;
		}
		return null;
	}

	public VariableInstance[] getAllInstances()
	{
		int size = table_variables.size();
		Enumeration<VariableInstance> e = table_variables.elements();
		VariableInstance out[] = new VariableInstance[size];
		int iter = 0;
		while(e.hasMoreElements())
		{
			VariableInstance vi = e.nextElement();
			out[iter++] = vi;
		}
		return out;
	}

	public void addSenderAndMessage(VariableInstance sender,
			VariableInstance message)
	throws IntolerableException
	{
		if(sender == null) return;
		table_variables.put("sender", sender);
		table_variables.put("message", message);
	}

	public void removeSenderAndMessage()
	throws IntolerableException
	{
		table_variables.remove("sender");
		table_variables.remove("message");
	}

	public VariableInstance getComplexInstanceSchallow(String s)
	{
		return (VariableInstance) table_variables.get(s);
	}

	public boolean valueEquals(VariableInstance sv) throws IntolerableException {
		if(!(sv instanceof Complex)) {
			return false;
		}
		Complex other = (Complex) sv;
		if(getSize() != other.getSize()) {
			return false;
		}
		for(int i = 0 ; i < getSize() ; i++) {
			VariableInstance vi = getMember(i);
			VariableInstance viother = other.getMember(vi.getName());
			if(viother == null) {
				return false;
			}
			if(!vi.valueEquals(viother)) {
				return false;
			}
		}
		return true;
	}

}
