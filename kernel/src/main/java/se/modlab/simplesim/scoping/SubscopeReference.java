package se.modlab.simplesim.scoping;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.variables.*;

public class SubscopeReference extends ScopeReference 
{

  private static int _ctr; 
  private int ctr; 

  public SubscopeReference(SimScope _s, String _name, String filename, int line, int column)
    throws IntolerableException
  {
    super(_s, _name, false, filename, line, column);
    VariableInstance vi[] = _s.getAllInstances();
    for(int i = 0 ; i < vi.length ; i++) 
    {
      if(vi[i] != this) _s.addMember(vi[i]);
    }
    ctr = _ctr++;
  }

  public int getSize()
  {
    return s.getSize();
  }

  public VariableInstance getMember(String name)
    throws IntolerableException
  {
    //System.out.println("scope Reference: Get member "+name);
    return s.getComplexInstanceSchallow(name);
  }

  public String toString() 
  {
    return "Class "+this.getClass().getName()+" ctr = "+ctr+" namer "+name;
  }

}
