package se.modlab.simplesim.scoping;

import se.modlab.generics.sstruct.comparisons.*;

public class SimScopeFactory implements ScopeFactory
{

  public Scope getInstance(Scope s)
  {
    return new SimScope(s);
  }

  public Scope getInstance(String name)
  {
    return new SimScope(name);
  }

  public Scope getInstance(Scope subjacent, String name)
  {
    return new SimScope(subjacent, name);
  }

}
