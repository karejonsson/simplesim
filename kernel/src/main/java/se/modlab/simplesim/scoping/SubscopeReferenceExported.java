package se.modlab.simplesim.scoping;

import se.modlab.generics.exceptions.*;

public class SubscopeReferenceExported extends ScopeReferenceExported 
{

  public SubscopeReferenceExported(SubscopeReference _inst)
    throws IntolerableException
  {
    super(_inst, _inst.getFilename(), _inst.getLine(), _inst.getColumn());
  }

}
