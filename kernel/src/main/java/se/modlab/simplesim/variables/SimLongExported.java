package se.modlab.simplesim.variables;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.exceptions.*;

public class SimLongExported extends LongVariable implements VariableConst
{
  private LongVariable var;
  
  public SimLongExported(LongVariable var)
  {
    super(var.getName(), var.getType(), var.getFilename(), var.getLine(), var.getColumn());
    this.var = var;
  }
 
  public sValue getValue()
    throws IntolerableException
  {
    return var.getValue();
  }

  public void setDefaultInitialValue()
    throws IntolerableException
  {
    throw new InternalError(
      "Internal: sLongExported.setDefaultInitialValue was called.");
  }

  public void setInitialValue(sValue initialVal) 
    throws IntolerableException
  {
    throw new InternalError(
      "Internal: sLongExported.setInitialValue was called.");
  }

/*
  public void resetInitialValue() 
    throws intolerableException
  {
    throw new intolerableException(
      "Internal: sLongExported.resetInitialValue was called.");
  }
 */

  public void setValue(sValue val) 
    throws IntolerableException
  {
    throw new UserRuntimeError(
      "Long exported variable "+name+" set to new value "+val
      +"\n"+"This happened to the variable declared "+getPlace());
  }

  public void setValue(VariableInstance si)
    throws IntolerableException
  {
    throw new UserRuntimeError(
      "Long exported variable "+name+" set to new value "+si
      +"\n"+"This happened to the variable declared "+getPlace());
  }

  public void setValue(boolean b)
    throws IntolerableException
  {
    throw new UserRuntimeError(
      "Long exported variable "+name+" set to new value "+b
      +"\n"+"This happened to the variable declared "+getPlace());
  }

  public String toString()
  {
    return var.toString()+" exported.";
  }

}
