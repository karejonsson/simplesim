package se.modlab.simplesim.variables;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.exceptions.*;

public class SimVariableVectorExported extends VariableVector 
                                       implements VariableConst
{
  private VariableVector inst;
  private VariableVectorFromFile instff;
  private static CreateExportedVersionVisitor cevv = null;
  private String ridgidness = null;

  static
  {
    try {
      cevv = CreateExportedVersionVisitor.getInstance();
    }
    catch(IntolerableException e) {
      //System.out.println("Commons laddning misslyckad. nsme");
    } 
  }

  public SimVariableVectorExported(VariableVector _inst, String _ridgidness)
    throws IntolerableException
  {
    super(_inst.getName(), 
          _inst.getType(),
          _inst.getFilename(),
          _inst.getLine(),
          _inst.getColumn(),
          _inst.getVariableFactory(),
          _inst.getLength());
    if(_inst == null) {
    	throw new InternalError("No instance passed!");
    }
    inst = _inst;
    ridgidness = _ridgidness;
  }

  public SimVariableVectorExported(VariableVectorFromFile _inst, String _ridgidness)
    throws IntolerableException
  {
    super(_inst.getName(), 
          _inst.getType(),
          _inst.getFilename(),
          _inst.getLine(),
          _inst.getColumn(),
          _inst.getVariableFactory(),
          _inst.getLength());
    instff = _inst;
    ridgidness = _ridgidness;
  }

  protected void initiateVector(int length)
    throws IntolerableException
  {
  }

  public int getLength()
  {
    if(inst == null) return instff.getLength();
    return inst.getLength();
  }

  public VariableInstance getVectorElement(int i)
    throws IntolerableException
  {
    VariableInstance si = null;
    if(inst != null) si = inst.getVectorElement(i);
    if(instff != null) si = instff.getVectorElement(i);
    if(si == null) return null;
    si.accept(cevv);
    return cevv.getExportedVersion();
  }

  public void setDefaultInitialValue()
    throws IntolerableException
  {
    throw new InternalError(
      "SimVariableVectorExported.setDefaultInitialValue was called on "+name);
  }

  public void setScopesName(String scopesName)
  {
  }

/*
  public void resetInitialValue() 
    throws intolerableException
  {
    throw new intolerableException(
      "Internal: SimVariableVectorExported.resetInitialValue was called.");
  }
 */

  public void setValue(VariableInstance si)
    throws IntolerableException
  {
    throw new UserRuntimeError(
      "Variable Vector exported variable "+name+" set to new value "+si
      +"\n"+"This happened to the variable declared "+getPlace());
  }
  
  private String getInstTypesName() {
	  if(inst != null) {
		  if(inst.getType() == null) {
			  return "[no type instance]";
		  }
		  else {
			  return inst.getType().getTypesName();
		  }
	  }
	  if(instff.getType() == null) {
		  return "[no type instance]";
	  }
	  else {
		  return instff.getType().getTypesName();
	  }  
  }

  public String toString()
  {
    return "Name "+name+
           ", vector length = "+getLength()+
           ", type "+getInstTypesName()+", style "+ridgidness;
  }

  public void setValue(sValue val) 
    throws IntolerableException
  {
    throw new UserRuntimeError(
      "Variable vector "+inst.getType().getTypesName()+" variable "+name+" set to new value "+val
      +"\n"+"This happened to the variable declared "+getPlace());
  }


}
