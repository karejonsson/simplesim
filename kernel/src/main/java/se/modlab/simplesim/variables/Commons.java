package se.modlab.simplesim.variables;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.values.*;
import java.text.*;
import java.util.*;
import java.lang.reflect.*;

public class Commons
{
  private double currentSimTime;
  private long epochStartTime;
  private long timeUnit;
  private String name;
  private String stopMessage;

  public void setTextualStopReason(String mess)
  {
    stopMessage = mess;
  }
 
  public String getTextualStopReason()
  {
    return stopMessage;
  }

	public final static String formats[] = {
		"yyyy/MM/dd HH:mm:ss",
		"yyyy/MM/dd HH:mm",
		"yyyy/MM/dd HHmm",
		"yyyy/MM/dd HH",
		"yyyy/MM/dd",
		"yyyy-MM-dd HH:mm:ss", 
		"yyyy-MM-dd HH:mm",
		"yyyy-MM-dd HHmm",
		"yyyy-MM-dd HH",
		"yyyy-MM-dd",
		"yyyyMMdd HH:mm:ss",
		"yyyyMMdd HH:mm",
		"yyyyMMdd HHmm",
		"yyyyMMdd HH",
		"yyyyMMdd", 
		"yy/MM/dd HH:mm:ss",
		"yy/MM/dd HH:mm",
		"yy/MM/dd HHmm",
		"yy/MM/dd HH",
		"yy/MM/dd",
		"yy-MM-dd HH:mm:ss", 
		"yy-MM-dd HH:mm",
		"yy-MM-dd HHmm",
		"yy-MM-dd HH",
		"yy-MM-dd",
		"yyMMdd HH:mm:ss",
		"yyMMdd HH:mm",
		"yyMMdd HHmm",
		"yyMMdd HH",
		"yyMMdd", 
	};

	public static Date getDateFromString(String s) {
		SimpleDateFormat sdf = null;
		for(int i = 0 ; i < formats.length ; i++) {
			try {
				String format = formats[i];
				sdf = new SimpleDateFormat(format);
				Date date = sdf.parse(s);
				return date;
			}
			catch(Exception e) {
				
			}
		}
		//System.out.println("Commons.getDateFromString -> null. Input "+s);
		return null;
	}

  public static long getTimeLongFromString(String s) throws InternalError
  {
    try {
  	  return getDateFromString(s).getTime();
    }
    catch(IllegalArgumentException e) {
      throw new InternalError("Unable to handle Date \""+s+"\"");
    }
  }

  public static String getTimeStringForExecutionTime()
  {
    return getTimeStringFromLong(new Date().getTime());
  }

  public static String getTimeStringFromLong(long l)
  {
    try
    {
      Date d = new Date(l);
      int year = d.getYear()+1900;
      int month = d.getMonth()+1;
      int day = d.getDate();
      int hours = d.getHours();
      int minutes = d.getMinutes();
      int seconds = d.getSeconds();
      int millis = (int)(d.getTime()-
                   1000*((int)(Math.floor(d.getTime()/1000))));
      
      return 
        ""+year+"/"+
        ((month < 10)?"0":"")+month+"/"+
        ((day < 10)?"0":"")+day+" "+
        ((hours < 10)?"0":"")+hours+":"+
        ((minutes < 10)?"0":"")+minutes+":"+
        ((seconds < 10)?"0":"")+seconds+"::"+millis;
        
    }
    catch(IllegalArgumentException e)
    {
      System.out.println("Gick ej med "+l);
      System.exit(0);
    }
    return null;
  }

  public double getCurrentSimTime()
  {
    return currentSimTime;
  }

  public void setCurrentSimTime(double time)
    throws IntolerableException
  {
    if(time < currentSimTime)
    {
      IntolerableException t =  new UserRuntimeError("Time set backwards."+
            "From "+currentSimTime+" to "+time);
      t.printStackTrace();
      throw t;
    }
    currentSimTime = time;
  }

  public void resetSimTime()
  {
    currentSimTime = 0;
  }

  public long getCurrentEpochSimTime()
  {
    long epochElapsed = (long) (((double)timeUnit)*currentSimTime);
    return epochStartTime+epochElapsed;
  }

  public double getSimTimeFromEpochTime(long epoch)
  {
    long elapsed = epoch - epochStartTime;
    double tmp = ((double)elapsed)/timeUnit;
    return tmp;
  }

  public long getEpochTimeFromSimTime(double simtime)
  {
    double epochElapsed = timeUnit*simtime;
    return (long) epochStartTime+((long) epochElapsed);
  }

  public void setEpochStartTime(long epochStartTime)
  {
    this.epochStartTime = epochStartTime;
  }

  public long getEpochStartTime()
  {  
    return epochStartTime;
  }

  public void setTimeUnit(long timeUnit)
  {
    this.timeUnit = timeUnit;
  }

  public long getTimeUnit()
  {
    return timeUnit;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getName()
  {
    return name;
  }
 
  public static void test1(String s) throws InternalError
  {
    long epoch = getTimeLongFromString(s);
    String s2 = getTimeStringFromLong(epoch);
    System.out.println(s+" : "+epoch+" : "+s2);
  }

  public static void test2(long e) throws InternalError
  {
    String s = getTimeStringFromLong(e);
    long e2 = getTimeLongFromString(s);
    System.out.println(""+e+" : "+s+" : "+e2);
  }

  public static void main(String args[]) throws InternalError
  {
    //commons com = new commons();
    test1("2003/04/14 19:45");
    test1("2003/07/24 10:32");
    test1("2006/07/24 10:32");
    test1("1999/12/14 02:57");
    test1("2003/05/01 08:00:00::0");
    test1("2003/08/15 00:00:00::0");
    test2(1051768800*1000);
    test2(1077688800*1000);
  }

}