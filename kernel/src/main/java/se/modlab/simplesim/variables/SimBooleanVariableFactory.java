package se.modlab.simplesim.variables;

import java.util.Hashtable;

import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.exceptions.*;

public class SimBooleanVariableFactory implements SimVariableFactory
{
 
  private Commons com;

  public SimBooleanVariableFactory(Commons _com)
  {
    super();
    com = _com;
  }

  public VariableInstance getInstance(String name, String filename, int line, int column, Hashtable<VariableInstance, Manager> managermap)
    throws IntolerableException
  {
    return getInstance(name, true, filename, line, column, managermap);
  }

  public VariableInstance getInstance(String name, boolean monitor, String filename, int line, int column, Hashtable<VariableInstance, Manager> managermap)
  {
    if(monitor)
    {
      return new SimBooleanMonitoredVariable(name, this, filename, line, column, com);
    }
    return new BooleanVariable(name, this, filename, line, column);
  }

  public String getTypesName()
  {
    return "boolean";
  }

	public VariableInstance getInstance(String name, String filename, int line, int column)
	throws IntolerableException {
		//System.out.println("Call to getInstance(s,s,i,i) i.e. addin null in class "+getClass().getName());
		return getInstance(name, filename, line, column, null);
	}
	public VariableInstance getInstance(
			  String name,
	          boolean monitor,
	          String filename, 
	          int line, 
	          int column) 
	    throws IntolerableException {
		//System.out.println("Call to getInstance(s,b,s,i,i) i.e. addin null in class "+getClass().getName());
		return getInstance(name, monitor, filename, line, column, null);
	}

}