package se.modlab.simplesim.variables;

import java.io.InputStream;
import java.util.Hashtable;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.logics.LogicalExpression;
import se.modlab.generics.sstruct.variables.VariableInstance;

public interface Manager
{

	public void initialize(Scope s, boolean isSharp, Hashtable<VariableInstance, Manager> managermap) throws IntolerableException;

	public void reinitializeLogical(LogicalExpression le) throws IntolerableException;

	public void reinitializeArithmetic(ArithmeticEvaluable ae) throws IntolerableException;

	public void reinitializeFromInputStream(InputStream is) throws IntolerableException;

}
