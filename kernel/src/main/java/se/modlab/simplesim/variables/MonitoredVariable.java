package se.modlab.simplesim.variables;

import java.awt.Component;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.variables.*;

public interface MonitoredVariable
{
  public MonitoredVariableExtracted getExtract()
    throws IntolerableException;
  public String getName();
}
