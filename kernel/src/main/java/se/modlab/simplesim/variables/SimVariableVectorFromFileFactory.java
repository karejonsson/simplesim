package se.modlab.simplesim.variables;

import java.io.InputStream;
import java.util.Hashtable;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.variables.*;

public class SimVariableVectorFromFileFactory implements SimVariableFactory
{
	private VariableFactory vf;
	private String filename = null;
	private InputStream is = null;
	private Commons com;

	public SimVariableVectorFromFileFactory(VariableFactory _vf, 
			String _filename,
			Commons _com)
	{
		super();
		com = _com;
		vf = _vf;
		filename = _filename;
		//System.out.println("SimVariableVectorFromFileFactory - ctor _filename = "+_filename);
	}

	public SimVariableVectorFromFileFactory(VariableFactory _vf, 
			InputStream _is,
			Commons _com)
	{
		super();
		com = _com;
		vf = _vf;
		is = _is;
		//System.out.println("SimVariableVectorFromFileFactory - ctor _filename = "+_filename);
	}

	public VariableInstance getInstance(String name, String filename, int line, int column)
	throws IntolerableException
	{
		return getInstance(name, true, filename, line, column);
	}

	public VariableInstance getInstance(String name, boolean monitor, String _filename, int _line, int _column, Hashtable<VariableInstance, Manager> managermap)
	throws IntolerableException
	{
		//H?r skall tabellen l?sas in. Det skall ocks? vara allt.
		//System.out.println("SimVariableVectorFromFileFactory - getInstance "+filename);
		if(filename != null) {
			if(monitor)
			{
				return new SimMonitoredVectorVariableFromFile(
						name, 
						this, 
						_filename, _line, _column,
						vf, 
						filename.substring(1, filename.length()-1),
						new SimDatetimeHandler(com));
			}
			return new VariableVectorFromFile(
					name, 
					this, 
					vf, 
					filename.substring(1, filename.length()-1),
					new SimDatetimeHandler(com),
					_filename, _line, _column);
		}
		if(is != null) {
			if(monitor)
			{
				return new SimMonitoredVectorVariableFromFile(
						name, 
						this, 
						_filename, _line, _column,
						vf, 
						is,
						new SimDatetimeHandler(com));
			}
			return new VariableVectorFromFile(
					name, 
					this, 
					vf, 
					is,
					new SimDatetimeHandler(com),
					_filename, _line, _column);
		}
		throw new InternalError("Vector from file neither from stream or filename");
	}

	public String getTypesName()
	{
		return "vector factory. File = "+filename+
		", type "+vf.getTypesName();
	}

	public VariableInstance getInstance(String name, String filename, int line, int column, Hashtable<VariableInstance, Manager> managermap)
	throws IntolerableException
	{
		System.out.println("Call to getInstance(s,s,i,i,ht) i.e. addin null in class "+getClass().getName());
		return getInstance(name, false, filename, line, column, null);
	}

	public VariableInstance getInstance(String name, boolean monitor, String filename, int line, int column)
	throws IntolerableException {
		System.out.println("Call to getInstance(s,b,s,i,i) i.e. addin null in class "+getClass().getName());
		return getInstance(name, monitor, filename, line, column, null);
	}

}