package se.modlab.simplesim.variables;

import se.modlab.generics.files.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.tables.*;

import java.io.InputStream;
import java.util.*;

public class SimMonitoredVectorVariableFromFile extends VariableVectorFromFile
{

	private boolean monitored;

	public SimMonitoredVectorVariableFromFile(String name, 
			VariableType vt,
			String _filename, int _line, int _column,
			VariableFactory vf, 
			String tabFilename,
			DatetimeHandler _dh)
	throws IntolerableException
	{
		super(name, vt, vf, tabFilename, _dh, _filename, _line, _column);
	}

	public SimMonitoredVectorVariableFromFile(String name, 
			VariableType vt,
			String _filename, int _line, int _column,
			VariableFactory vf, 
			InputStream is,
			DatetimeHandler _dh)
	throws IntolerableException
	{
		super(name, vt, vf, is, _dh, _filename, _line, _column);
	}

	protected void control()
	throws IntolerableException
	{
		if(!(vf instanceof SimComplexVariableFactory))
		{
			throw new UserRuntimeError(
					"The type of the vectors element must be a struct created by\n"+
					"a typedef declaration. Here it was a "+vf.getTypesName()
					+"\n"+"This happened to the variable declared "+getPlace());
		}
	}

	protected VariableInstance getInstance(String name)
	throws IntolerableException
	{
		SimComplexVariableFactory factory = (SimComplexVariableFactory) vf;
		return factory.getInstance(name, true, filename, line, column);
	}

}
