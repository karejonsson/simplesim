package se.modlab.simplesim.variables;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.exceptions.*;

public class SimVectorExported extends VariableVector implements VariableConst
{
  private VariableVector inst;
  private VariableVectorFromFile instff;
  private static CreateExportedVersionVisitor cevv = null;
  private String type = null;

  static
  {
    try
    {
      cevv = CreateExportedVersionVisitor.getInstance();
    }
    catch(IntolerableException e)
    {
      //System.out.println("Commons laddning misslyckad. nsme");
    } 
  }

  public SimVectorExported(VariableVector inst, String type)
    throws IntolerableException
  {
    super(inst.getName(), 
          inst.getType(),
          inst.getFilename(),
          inst.getLine(),
          inst.getColumn(),
          inst.getVariableFactory(),
          inst.getLength());
    this.inst = inst;
    this.type = type;
  }

  public SimVectorExported(VariableVectorFromFile inst, String type)
    throws IntolerableException
  {
    super(inst.getName(), 
          inst.getType(),
          inst.getFilename(),
          inst.getLine(),
          inst.getColumn(),
          inst.getVariableFactory(),
          inst.getLength());
    this.instff = inst;
    this.type = type;
  }

  protected void initiateVector(int length)
    throws IntolerableException
  {
  }

  public int getLength()
  {
    if(inst == null) return instff.getLength();
    return inst.getLength();
  }

  public VariableInstance getVectorElement(int i)
    throws IntolerableException
  {
    VariableInstance si = null;
    if(inst != null) si = inst.getVectorElement(i);
    if(instff != null) si = instff.getVectorElement(i);
    if(si == null) return null;
    si.accept(cevv);
    return cevv.getExportedVersion();
  }

  public void setDefaultInitialValue()
    throws IntolerableException
  {
    throw new InternalError(
      "variableInstanceVectorExported.setDefaultInitialValue was called on "+name);
  }

  public void setScopesName(String scopesName)
  {
  }

/*
  public void resetInitialValue() 
    throws intolerableException
  {
    throw new intolerableException(
      "Internal: variableInstanceVectorExported.resetInitialValue was called.");
  }
 */

  public void setValue(VariableInstance si)
    throws IntolerableException
  {
    throw new UserRuntimeError(
      "Variable Vector exported variable "+name+" set to new value "+si
      +"\n"+"This happened to the variable declared "+getPlace());
  }

  public String toString()
  {
    return "Name "+name+
           ", vector length = "+getLength()+
           ", type "+inst.getType().getTypesName()+" "+type;
  }

  public void setValue(sValue val) 
    throws IntolerableException
  {
    throw new UserRuntimeError(
      "Variable vector "+type+" variable "+name+" set to new value "+val
      +"\n"+"This happened to the variable declared "+getPlace());
  }


}
