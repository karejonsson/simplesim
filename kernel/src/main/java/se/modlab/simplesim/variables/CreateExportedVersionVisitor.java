package se.modlab.simplesim.variables;

import java.lang.reflect.*;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.variables.*;

public abstract class CreateExportedVersionVisitor 
       extends InstanceVisitor
{
  protected VariableInstance si;
  protected static CreateExportedVersionVisitor instance = null;
  
  public static CreateExportedVersionVisitor getInstance()
    throws IntolerableException 
  {
    if(instance == null)
    {
      throw new InternalError(
        "createExportedVersionVisitor.getInstance called before\n"+
        "the instance was set from SimVariableVectorExported classload.");
    }
    return instance;
  }

  public static void setInstance(CreateExportedVersionVisitor iv)
    throws IntolerableException 
  {
    if(instance != null)
    {
      throw new InternalError(
        "createExportedVersionVisitor.setInstance called after\n"+
        "the instance was set.");
    }
    instance = iv;
  }

  public VariableInstance getExportedVersion()
  {
    return si;
  }

  public void visit(BooleanVariable inst) 
    throws IntolerableException 
  {
    si = new SimBooleanExported(inst);
  }
 
  public void visit(BooleanConst inst) 
    throws IntolerableException 
  {
    si = new SimBooleanExported(inst);
  }
 
  public void visit(SimBooleanMonitoredVariable inst) 
    throws IntolerableException 
  {
    si = new SimBooleanExported(inst);
  }
 
  public void visit(DoubleVariable inst) 
    throws IntolerableException 
  {
    si = new SimDoubleExported(inst);
  }
 
  public void visit(DoubleConst inst) 
    throws IntolerableException 
  {
    si = new SimDoubleExported(inst);
  }
 
  public void visit(SimDoubleMonitoredVariable inst) 
    throws IntolerableException 
  {
    si = new SimDoubleExported(inst);
  }

  public void visit(LongVariable inst) 
    throws IntolerableException 
  {
    si = new SimLongExported(inst);
  }
 
  public void visit(LongConst inst) 
    throws IntolerableException 
  {
    si = new SimLongExported(inst);
  }
 
  public void visit(SimLongMonitoredVariable inst) 
    throws IntolerableException 
  {
    si = new SimLongExported(inst);
  }

  public void visit(ComplexVariable inst) 
    throws IntolerableException 
  {
    si = new SimComplexVariableExported(inst);
  }

  public void visit(VariableVector inst) 
    throws IntolerableException 
  {
    si = new SimVariableVectorExported(inst, "exported");
  }

  public void visit(SimVariableVector inst) 
    throws IntolerableException 
  {
    si = new SimVariableVectorExported(inst, "exported");
  }

  public void visit(VariableVectorFromFile inst) 
    throws IntolerableException 
  {
    si = new SimVariableVectorExported(inst, "exported");
  }

  public void visit(SimMonitoredVectorVariableFromFile inst) 
    throws IntolerableException 
  {
    si = new SimVariableVectorExported(inst, "exported");
  }

}