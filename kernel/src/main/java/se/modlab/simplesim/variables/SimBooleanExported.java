package se.modlab.simplesim.variables;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.exceptions.*;

public class SimBooleanExported extends BooleanVariable implements VariableConst
{
  private BooleanVariable var;
  
  public SimBooleanExported(BooleanVariable var)
  {
    super(var.getName(), var.getType(), var.getFilename(), var.getLine(), var.getColumn());
    this.var = var;
  }
 
  public sValue getValue()
    throws IntolerableException
  {
    return var.getValue();
  }

  public void setDefaultInitialValue()
    throws IntolerableException
  {
    throw new InternalError(
      "SimBooleanExported.setDefaultInitialValue was called on "+name);
  }

  public void setInitialValue(sValue initialVal) 
    throws IntolerableException
  {
    throw new InternalError(
      "SimBooleanExported.setInitialValue was called on "+name);
  }

/*
  public void resetInitialValue() 
    throws intolerableException
  {
    throw new intolerableException(
      "Internal: SimBooleanExported.resetInitialValue was called.");
  }
*/

  public void setValue(sValue val) 
    throws IntolerableException
  {
    throw new UserRuntimeError(
      "Boolean exported variable "+name+" set to new value "+val
      +"\n"+"This happened to the variable declared "+getPlace());
  }

  public void setValue(VariableInstance si)
    throws IntolerableException
  {
    throw new UserRuntimeError(
      "Boolean exported variable "+name+" set to new value "+si
      +"\n"+"This happened to the variable declared "+getPlace());
  }

  public void setValue(boolean b)
    throws IntolerableException
  {
    throw new UserRuntimeError(
      "Boolean exported variable "+name+" set to new value "+b
      +"\n"+"This happened to the variable declared "+getPlace());
  }

  public String toString()
  {
    return var.toString()+" exported.";
  }

}
