package se.modlab.simplesim.variables;

import java.util.*;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.tables.*;
import se.modlab.generics.sstruct.variables.*;

public class SimComplexVariableFactory 
extends ComplexVariableFactory
implements SimVariableFactory
{

	private Commons com;
	//private Vector members = new Vector();

	public SimComplexVariableFactory(String _name, Commons _com)
	{
		super(_name); 
		com = _com;
	}

	public final void addMember(VariableFactory vf, String name)
	throws IntolerableException
	{
		if(!(vf instanceof SimVariableFactory))
		{
			throw new InternalError(
					"Non simulation style instance factory used.\n"+
					"The toString() output is "+vf);
		}
		super.addMember(vf, name);
	}

	public VariableInstance getInstance(String name, boolean monitor, String filename, int line, int column, Hashtable<VariableInstance, Manager> managermap)
	throws IntolerableException
	{
		//System.out.println("Name = "+name);
		ComplexVariable scv = new ComplexVariable(name, this, filename, line, column);
		for(int i = 0 ; i < getNoMembers() ; i++)
		{
			VariableFactory vf = getMembersType(i);
			//System.out.println("member = "+h.name);
			if(!(vf instanceof SimVariableFactory))
			{
				throw new InternalError(
						"Non simulation style instance factory used.\n"+
						"The toString() output is "+vf);
			}
			SimVariableFactory svf = (SimVariableFactory) vf;
			//System.out.println("SimCOmplexVariableFacotry mointor = "+monitor);
			VariableInstance si = svf.getInstance(getMembersName(i), monitor, filename, line, column, managermap);
			scv.addMember(si);
		}
		return scv;
	}

	public VariableInstance getInstance(String name, boolean monitor, String filename, int line, int column)
	throws IntolerableException {
		//System.out.println("Call to getInstance(s,b,s,i,i) i.e. addin null in class "+getClass().getName());
		return getInstance(name, monitor, filename, line, column, null);
	}

	public VariableInstance getInstance(String name, String filename, int line, int column, Hashtable<VariableInstance, Manager> managermap)
	throws IntolerableException
	{
		//System.out.println("Call to getInstance(s,s,i,i,ht) i.e. addin null in class "+getClass().getName());
		return getInstance(name, false, filename, line, column, null);
	}


}

