package se.modlab.simplesim.variables;

import java.util.*;

import se.modlab.generics.exceptions.InternalProgrammingError;
import se.modlab.generics.sstruct.tables.*;

public class SimDatetimeHandler implements DatetimeHandler
{

	private Commons com;

	public SimDatetimeHandler(Commons _com)
	{
		com = _com;
	}

	public Holder getHolder(String datetime, int beginLine, int beginColumn)
	{
		long ep = Commons.getTimeLongFromString(datetime);
		double simtime = com.getSimTimeFromEpochTime(ep);
		return new DoubleHolder(simtime, beginLine, beginColumn);
	}

}
