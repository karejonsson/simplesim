package se.modlab.simplesim.variables;

import java.awt.Component;
import java.util.*;

import se.modlab.generics.exceptions.InternalProgrammingError;
import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.exceptions.UserRuntimeError;
import se.modlab.generics.sstruct.values.sBoolean;
import se.modlab.generics.sstruct.values.sLong;
import se.modlab.generics.sstruct.values.sValue;
import se.modlab.generics.sstruct.variables.LongVariable;
import se.modlab.generics.sstruct.variables.VariableType;

public class SimLongMonitoredVariable extends LongVariable
                                      implements MonitoredVariable
{
  
  private Commons com;
  private long maxValue;
  private long minValue;
  private Vector takenValues;// = new Vector();
  private MonitoredVariableExtracted mve = null;

  public SimLongMonitoredVariable(String name, VariableType vt, String _filename, int _line, int _column, Commons com)
  {
    super(name, vt, _filename, _line, _column);
    this.com = com;
  }
  
  public long getTimeUnit() {
	  return com.getTimeUnit();
  }
   
  public void setInitialValue(sValue initialVal) 
    throws IntolerableException
  {
    if(initialVal instanceof sBoolean) 
    {
      throw new UserRuntimeError(
        "Long variable "+name+" set to boolean value "+initialVal
        +"\n"+"This happened to the variable declared "+getPlace());
    }



    takenValues = new Vector();
    super.setInitialValue(initialVal);
    maxValue = val.getLong().longValue();
    minValue = val.getLong().longValue();
    sValue cp = this.val.copy();
    takenValues.addElement(cp);
  }

  public void setValue(sValue val) 
    throws IntolerableException
  {
    super.setValue(val);
    long _maxValue = this.val.getLong().longValue();
    if(_maxValue > maxValue) maxValue = _maxValue;
    long _minValue = this.val.getLong().longValue();
    if(_minValue < minValue) minValue = _minValue;
    sValue cp = this.val.copy();
    cp.setTime(com.getCurrentSimTime());
    takenValues.addElement(cp);
  }

/*
  public void resetInitialValue() 
    throws intolerableException
  {
    //getExtractResetVariable();
    takenValues = new Vector();
    super.resetInitialValue();
    maxValue = val.getLong().longValue();
    minValue = val.getLong().longValue();
    sValue cp = val.copy();
    takenValues.addElement(cp);
  }
 */

  public sValue getValue(int i)
  {
    if(i < 0) return null;
    if(i >= takenValues.size()) return null;
    return (sValue) takenValues.elementAt(i);
  }

  public Object getGeneralizedValue(int i)
  {
    return getValue(i);
  }

  public sValue getMaxValue()
  {
    return new sLong(maxValue);
  }

  public sValue getMinValue()
  {
    return new sLong(minValue);
  }

  public double getEndTime()
  {
    return com.getCurrentSimTime();
  }

  public int getSize()
  {
    return takenValues.size();
  }

  public String getStartTimeTextual()
  {
    sValue val = (sValue) takenValues.elementAt(0); 
    double simtime = val.getTime();
    long l = com.getEpochTimeFromSimTime(simtime);
    return com.getTimeStringFromLong(l);
  }

  public String getEndTimeTextual()
  {
    double endTime = getEndTime();
    long endTime_l = com.getEpochTimeFromSimTime(endTime);
    return Commons.getTimeStringFromLong(endTime_l);
  }

  public MonitoredVariableExtracted getExtract()
    throws IntolerableException
  {
    if(takenValues != null) 
    {
      mve = new monitoredLongVariableExtracted(takenValues,
                                               (double) maxValue,
                                               (double) minValue,
                                               getName(),
                                               getEndTime(),
                                               getStartTimeTextual(),
                                               getEndTimeTextual());
      takenValues = null;
    }
    //resetInitialValue();
    return mve;
  }

  private class monitoredLongVariableExtracted implements MonitoredVariableExtracted
  {
    
    private Vector takenValues = null;
    private double maxValue = 0.0;
    private double minValue = 0.0;
    private String variableName = null;
    private double endTime = 0.0;
    private String startTimeTextual = null;
    private String endTimeTextual = null;
    private int timeLines = 0;
    private int variableLines = 0;

    monitoredLongVariableExtracted(Vector takenValues,
                                   double maxValue,
                                   double minValue,
                                   String variableName,
                                   double endTime,
                                   String startTimeTextual,
                                   String endTimeTextual)
    {
      this.takenValues = takenValues;
      this.maxValue = maxValue;
      this.minValue = minValue;
      this.variableName = variableName;
      this.endTime = endTime;
      this.startTimeTextual = startTimeTextual;
      this.endTimeTextual = endTimeTextual;
    }

    public long getTimeUnit() {
    	return com.getTimeUnit();
    }

    public long getEpochStartTime() {
    	return com.getEpochStartTime();
    }
    
    public int getSize()
    {
      return takenValues.size();
    }

    public sValue getValue(int i)
      throws IntolerableException
    {
      if(i < 0) throw 
        new InternalProgrammingError("Index out of bounds (i="+i+") on "+name);
      if(i >= takenValues.size()) throw 
        new InternalProgrammingError("Index out of bounds (i="+i+") on "+name);
      return (sValue) takenValues.elementAt(i);
    }

    public Object getGeneralizedValue(int i)
      throws IntolerableException
    {
      return getValue(i);
    }

    public double getMaxValue()
    {
      return maxValue;
    }

    public double getMinValue()
    {
      return minValue;
    }

    public String getName()
    {
      return variableName;
    }

    public void setName(String name)
    {
      variableName = name;
    }

    public double getEndTime()
    {
      return endTime;
    }

    public String getStartTimeTextual()
    {
      return startTimeTextual;
    }

    public String getEndTimeTextual()
    {
      return endTimeTextual;
    }

    public void setNoLinesOnTimeAxis(int lines)
    {
      timeLines = lines;
    }

    public int getNoLinesOnTimeAxis()
    {
      return timeLines;
    }

    public void setNoLinesOnVariableAxis(int lines)
    {
      variableLines = lines;
    }

    public int getNoLinesOnVariableAxis()
    {
      return variableLines;
    }

    public void accept(MonitoredVariableVisitor mvv)
    {
      mvv.visitLong();
    }

    public String toString()
    {
      return getName();
    }

  }

}
