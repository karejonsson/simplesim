package se.modlab.simplesim.variables;

import java.util.Hashtable;

import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.exceptions.*;

public class SimLongVariableFactory implements SimVariableFactory
{
 
  private Commons com;

  public SimLongVariableFactory(Commons _com)
  {
    super();
    com = _com;
  }

  public VariableInstance getInstance(String name, String _filename, int _line, int _column, Hashtable<VariableInstance, Manager> managermap)
    throws IntolerableException
  {
    return getInstance(name, false, _filename, _line, _column, managermap);
  }

  public VariableInstance getInstance(String name, boolean monitor, String _filename, int _line, int _column, Hashtable<VariableInstance, Manager> managermap)
  {
    //System.out.println("SimLongVariableFactory get instance "+name+" "+monitor);
    if(monitor)
    {
      return new SimLongMonitoredVariable(name, this, _filename, _line, _column, com);
    }
    return new LongVariable(name, this, _filename, _line, _column);
  }

  public String getTypesName()
  {
    return "long";
  }

	public VariableInstance getInstance(String name, String filename, int line, int column)
	throws IntolerableException {
		//System.out.println("Call to getInstance(s,s,i,i) i.e. addin null in class "+getClass().getName());
		return getInstance(name, filename, line, column, null);
	}
	public VariableInstance getInstance(
			  String name,
	          boolean monitor,
	          String filename, 
	          int line, 
	          int column) 
	    throws IntolerableException {
		//System.out.println("Call to getInstance(s,b,s,i,i) i.e. addin null in class "+getClass().getName());
		return getInstance(name, monitor, filename, line, column, null);
	}

}