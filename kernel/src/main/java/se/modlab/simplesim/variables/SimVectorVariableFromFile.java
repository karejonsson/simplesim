package se.modlab.simplesim.variables;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.tables.*;
import se.modlab.generics.sstruct.variables.*;

public class SimVectorVariableFromFile extends VariableVectorFromFile
{

  public SimVectorVariableFromFile(String name, 
                                   VariableType vt,
                                   VariableFactory vf, 
                                   String filename,
                                   String _filename, int _line, int _column)
    throws IntolerableException
  {
    super(name, vt, vf, filename, null, _filename, _line, _column);
  }

  public SimVectorVariableFromFile(String name, 
                                   VariableType vt,
                                   VariableFactory vf, 
                                   String filename,
                                   DatetimeHandler _dh,
                                   String _filename, int _line, int _column)
    throws IntolerableException
  {
    super(name, vt, vf, filename, _dh, _filename, _line, _column);
  }
}

