package se.modlab.simplesim.variables;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.exceptions.*;
import java.awt.Component;
import java.util.*;

public class SimBooleanMonitoredVariable extends BooleanVariable
                                         implements MonitoredVariable
{
  
  private Commons com;


  private Vector takenValues;// = new Vector();
  private MonitoredVariableExtracted mve = null;

  public SimBooleanMonitoredVariable(String name, VariableType vt, String _filename, int _line, int _column, Commons com)
  {
    super(name, vt, _filename, _line, _column);
    this.com = com;
  }
   
  public void setInitialValue(sValue initialVal) 
    throws IntolerableException
  {
/*
    if(takenValues != null)
    {
      throw new intolerableException(
        "Internal: Variable "+name+" (double) had an initialized vector for taken\n"+
        "values prior to call to setInitialValue");         
    }
 */
    if(!(initialVal instanceof sBoolean)) 
    {
      throw new UserRuntimeError(
        "Boolean variable "+name+" set to non boolean value "+initialVal
        +"\n"+"This happened to the variable declared "+getPlace());
    }
    takenValues = new Vector();
    super.setInitialValue(initialVal);


    sValue cp = this.val.copy();
    takenValues.addElement(cp);
  }
  
  public void setValue(sValue val) 
    throws IntolerableException
  {
    super.setValue(val);




    sValue cp = this.val.copy();
    cp.setTime(com.getCurrentSimTime());
    takenValues.addElement(cp);
  }

/*
  public void resetInitialValue() 
    throws intolerableException
  {
    //getExtractResetVariable();
    takenValues = new Vector();
    super.resetInitialValue();


    sValue cp = this.val.copy();
    takenValues.addElement(cp);
  }
*/

  public sValue getValue(int i)
  {
    if(i < 0) return null;
    if(i >= takenValues.size()) return null;
    return (sValue) takenValues.elementAt(i);
  }
 
  public sValue getMaxValue()
  {
    return null;
  }

  public sValue getMinValue()
  {
    return null;
  }

  public double getEndTime()
  {
    return com.getCurrentSimTime();
  }

  public int getSize()
  {
    return takenValues.size();
  }

  public String getStartTimeTextual()
  {
    sValue val = (sValue) takenValues.elementAt(0); 
    double simtime = val.getTime();
    long l = com.getEpochTimeFromSimTime(simtime);
    return com.getTimeStringFromLong(l);
  }

  public String getEndTimeTextual()
  {
    double endTime = getEndTime();
    long endTime_l = com.getEpochTimeFromSimTime(endTime);
    return Commons.getTimeStringFromLong(endTime_l);
  }

  public MonitoredVariableExtracted getExtract()
    throws IntolerableException
  {
    if(takenValues != null) 
    {
      mve = new monitoredBooleanVariableExtracted(takenValues,
                                                  getName(),
                                                  getEndTime(),
                                                  getStartTimeTextual(),
                                                  getEndTimeTextual());
      takenValues = null;
    }
    //resetInitialValue();
    return mve;
  } 

  private class monitoredBooleanVariableExtracted implements MonitoredVariableExtracted
  {
    
    private Vector takenValues = null;
    private String variableName = null;
    private double endTime = 0.0;
    private String startTimeTextual = null;
    private String endTimeTextual = null;
    private int timeLines = 0;
    private int variableLines = 0;

    monitoredBooleanVariableExtracted(Vector takenValues,
                                      String variableName,
                                      double endTime,
                                      String startTimeTextual,
                                      String endTimeTextual)
    {
      this.takenValues = takenValues;
      this.variableName = variableName;
      this.endTime = endTime;
      this.startTimeTextual = startTimeTextual;
      this.endTimeTextual = endTimeTextual;
    }
    
    public long getTimeUnit() {
    	return com.getTimeUnit();
    }

    public long getEpochStartTime() {
    	return com.getEpochStartTime();
    }
    
    public int getSize()
    {
      return takenValues.size();
    }

    public Object getGeneralizedValue(int i)
      throws IntolerableException
    {
      return getValue(i);
    }

    public sValue getValue(int i)
      throws IntolerableException
    {
      if(i < 0) throw 
        new InternalError("Index out of bounds (i="+i+") on "+name);
      if(i >= takenValues.size()) throw 
        new InternalError("Index out of bounds (i="+i+") on "+name);
      return (sValue) takenValues.elementAt(i);
    }

    public double getMaxValue()
    {
      return 1.1;
    }

    public double getMinValue()
    {
      return -0.1;
    }

    public String getName()
    {
      return variableName;
    }

    public void setName(String name)
    {
      variableName = name;
    }

    public double getEndTime()
    {
      return endTime;
    }

    public String getStartTimeTextual()
    {
      return startTimeTextual;
    }

    public String getEndTimeTextual()
    {
      return endTimeTextual;
    }

    public void setNoLinesOnTimeAxis(int lines)
    {
      timeLines = lines;
    }

    public int getNoLinesOnTimeAxis()
    {
      return timeLines;
    }

    public void setNoLinesOnVariableAxis(int lines)
    {
      variableLines = lines;
    }

    public int getNoLinesOnVariableAxis()
    {
      return variableLines;
    }

    public void accept(MonitoredVariableVisitor mvv)
    {
      mvv.visitBoolean();
    }

    public String toString()
    {
      return getName();
    }

  }

}
