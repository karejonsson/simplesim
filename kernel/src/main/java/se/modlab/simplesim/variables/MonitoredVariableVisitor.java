package se.modlab.simplesim.variables;

import java.awt.Component;

public interface MonitoredVariableVisitor
{

  public void visitLong();
  public void visitDouble();
  public void visitBoolean();

}
