package se.modlab.simplesim.variables;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.exceptions.*;

public class SimDoubleExported extends DoubleVariable implements VariableConst
{
  private DoubleVariable var;
  
  public SimDoubleExported(DoubleVariable var)
  {
    super(var.getName(), var.getType(), var.getFilename(), var.getLine(), var.getColumn());
    this.var = var;
  }
 
  public sValue getValue()
    throws IntolerableException
  {
    return var.getValue();
  }

  public void setDefaultInitialValue()
    throws IntolerableException
  {
    throw new InternalError(
      "ISimDoubleExported.setDefaultInitialValue was called.");
  }

  public void setInitialValue(sValue initialVal) 
    throws IntolerableException
  {
    throw new InternalError(
      "SimDoubleExported.setInitialValue was called.");
  }

/*
  public void resetInitialValue() 
    throws intolerableException
  {
    throw new intolerableException(
      "Internal: SimDoubleExported.resetInitialValue was called.");
  }
*/

  public void setValue(sValue val) 
    throws IntolerableException
  {
    throw new UserRuntimeError(
      "Double exported variable "+name+" set to new value "+val
      +"\n"+"This happened to the variable declared "+getPlace());
  }

  public void setValue(VariableInstance si)
    throws IntolerableException
  {
    throw new UserRuntimeError(
      "Double exported variable "+name+" set to new value "+si
      +"\n"+"This happened to the variable declared "+getPlace());
  }

  public void setValue(double d)
    throws IntolerableException
  {
    throw new UserRuntimeError(
      "Double exported variable "+name+" set to new value "+d
      +"\n"+"This happened to the variable declared "+getPlace());
  }

  public String toString()
  {
    return var.toString()+" exported.";
  }

}
