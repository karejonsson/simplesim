package se.modlab.simplesim.variables;

import java.awt.Component;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.exceptions.*;

public interface MonitoredVariableExtracted
{

  public int getSize();
  public sValue getValue(int i) throws IntolerableException;
  public Object getGeneralizedValue(int i) throws IntolerableException;
  public double getMaxValue();
  public double getMinValue();
  public String getName();
  public void setName(String name);
  public double getEndTime();
  public String getStartTimeTextual();
  public long getEpochStartTime();
  public String getEndTimeTextual();
  public void setNoLinesOnTimeAxis(int lines);
  public int getNoLinesOnTimeAxis();
  public void setNoLinesOnVariableAxis(int lines);
  public int getNoLinesOnVariableAxis();
  public void accept(MonitoredVariableVisitor mvv);
  public long getTimeUnit();

}
