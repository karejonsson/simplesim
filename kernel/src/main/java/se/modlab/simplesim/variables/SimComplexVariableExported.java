package se.modlab.simplesim.variables;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.exceptions.*;
import java.util.*;

public class SimComplexVariableExported extends ComplexVariable 
                                        implements VariableConst
{
  private ComplexVariable inst;
  private static CreateExportedVersionVisitor cevv = null;
  //private String type = null;
 
  static
  {
    try
    {
      cevv = CreateExportedVersionVisitor.getInstance();
    }
    catch(IntolerableException e)
    {
      //System.out.println("Commons laddning misslyckad. nsme");
    } 
  }

  public SimComplexVariableExported(ComplexVariable inst)
  {
    super(inst.getName(), inst.getType(), inst.getFilename(), inst.getLine(), inst.getColumn());
    this.inst = inst;
    //this.type = type;
  }

  public void addMember(VariableInstance si)
    throws IntolerableException
  {
    throw new InternalError(
      "SimComplexVariableExported.addMember was called on "+name);
  }
 
  public int getSize()
  {
    return inst.getSize();
  }

  public VariableInstance getMember(int i)
    throws IntolerableException
  {
    VariableInstance si = (VariableInstance) inst.getMember(i);
    si.accept(cevv);
    return cevv.getExportedVersion();
  }

  public VariableInstance getMember(String name)
    throws IntolerableException
  {
    for(int i = 0 ; i < inst.getSize() ; i++)
    {
      VariableInstance si = (VariableInstance) inst.getMember(i);
      if(name.compareTo(si.getName()) == 0) 
      {
        si.accept(cevv);
        return cevv.getExportedVersion();
      }
    }
    return null;
  }

  public void setDefaultInitialValue()
    throws IntolerableException
  {
    throw new InternalError(
      "SimComplexVariableExported.setDefaultInitialValue was called on "+name);
  }

/*
  public void resetInitialValue() 
    throws intolerableException
  {
    throw new internalError(
      "SimComplexVariableExported.resetInitialValue was called on "+name);
  }
 */

  public void setValue(VariableInstance si)
    throws IntolerableException
  {
    throw new UserRuntimeError(
      "Complex exported "+inst.getType().getTypesName()+" variable "+name+" set to new value "+si
      +"\n"+"This happened to the variable declared "+getPlace());
  }

  public void setValue(sValue val) 
    throws IntolerableException
  {
    throw new UserRuntimeError(
      "Complex exported "+inst.getType().getTypesName()+" variable "+name+" set to new value "+val
      +"\n"+"This happened to the variable declared "+getPlace());
  }

  public String toString()
  {
    return name;
  }

}
