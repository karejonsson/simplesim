package se.modlab.simplesim.variables;

import java.util.Hashtable;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.variables.*;

public class SimVectorVariableFactory implements SimVariableFactory
{
  protected VariableFactory vf;
  protected int length;
  private Commons com;

  public SimVectorVariableFactory(VariableFactory _vf, int _length)
  {
    super();
    vf = _vf;
    length = _length;
  }

  public VariableInstance getInstance(String name, String _filename, int _line, int _column)
    throws IntolerableException
  {
    return getInstance(name, false, _filename, _line, _column);
  }

  public VariableInstance getInstance(String name, boolean monitor, String _filename, int _line, int _column, Hashtable<VariableInstance, Manager> managermap)
    throws IntolerableException
  {
    //System.out.println("SimVectorVariableFactory - "+name+" mon "+monitor);
    if(monitor)
    {
      return new SimVariableVector(name, this, _filename, _line, _column, vf, length, monitor);
    }
    return new VariableVector(name, this, _filename, _line, _column, vf, length);
  }

  public String getTypesName()
  {
    return "vector factory. Length = "+length+
           ", type "+vf.getTypesName();
  }

  public VariableFactory getElementsFactory()
  {
    return vf;
  }
  
	public VariableInstance getInstance(String name, String filename, int line, int column, Hashtable<VariableInstance, Manager> managermap)
	throws IntolerableException
	{
		//System.out.println("Call to getInstance(s,s,i,i,ht) i.e. addin null in class "+getClass().getName());
		return getInstance(name, false, filename, line, column, null);
	}

	public VariableInstance getInstance(String name, boolean monitor, String filename, int line, int column)
	throws IntolerableException {
		//System.out.println("Call to getInstance(s,b,s,i,i) i.e. addin null in class "+getClass().getName());
		return getInstance(name, monitor, filename, line, column, null);
	}


}