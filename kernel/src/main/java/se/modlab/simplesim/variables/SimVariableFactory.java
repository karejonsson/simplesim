package se.modlab.simplesim.variables;

import java.util.Hashtable;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.variables.VariableFactory;
import se.modlab.generics.sstruct.variables.VariableInstance;

public interface SimVariableFactory extends VariableFactory
{

  public VariableInstance getInstance(
		  String name, 
		  String filename, 
		  int line, 
		  int column, 
		  Hashtable<VariableInstance, Manager> managermap) 
    throws IntolerableException;

  public abstract VariableInstance getInstance(
		  String name,
          boolean monitor,
          String filename, 
          int line, 
          int column, 
          Hashtable<VariableInstance, Manager> managermap) 
    throws IntolerableException;

  public abstract VariableInstance getInstance(
		  String name,
          boolean monitor,
          String filename, 
          int line, 
          int column) 
    throws IntolerableException;

}
