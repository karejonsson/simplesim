package se.modlab.simplesim.variables;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.comparisons.*;

public interface ComplexEvaluable {
  
  public Complex evaluate(Scope s) throws IntolerableException;

}

