package se.modlab.simplesim.variables;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.variables.*;

public class SimVariableVector extends VariableVector
{
  
  private boolean monitor;
  private int length;

  public SimVariableVector(String name, 
                           VariableType vt,
                           String _filename, int _line, int _column,
                           VariableFactory vf, 
                           int _length,
                           boolean _monitor)
    throws IntolerableException
  {
    super(name, vt, _filename, _line, _column, vf, _length);
    monitor = _monitor;
    length = _length;
    //System.out.println("SimVariableVector - ctor - mon = "+monitor);
    if(!(vf instanceof SimVariableFactory))
    {
      throw new InternalError(
        "Factory of non simulation type passed to SimVariableMonitor.\n"+
        "The classname is "+vf.getClass().getName()+" and the toString\n"+
        "output is "+vf+" and the factory name is "+vf.getTypesName());
    }
    initiateVector();
  }

  protected void initiateVector(int length)
    throws IntolerableException
  {
  }

  protected void initiateVector()
    throws IntolerableException
  {
    /*if(monitor)
    {
      (new Throwable()).printStackTrace();
      System.exit(-1);
    }*/

    //System.out.println("Sim variable vector initiate vector monitor 0 "+monitor);
    siv = new VariableInstance[length];
    for(int i = 0 ; i < length ; i++)
    {
      //System.out.println("Typ "+vf+" monitor = "+monitor);
      siv[i] = ((SimVariableFactory)vf).getInstance("["+i+"]", monitor, filename, line, column);
      //System.out.println("SimVariableVector - initiateVector "+siv[i].getType().getClass().getName()+
      //  " mon "+monitor);
    }
  }

  public String toString()
  {
    return "Name "+name+
           ", vector length = "+siv.length+
           ", type "+vf.getTypesName();
  }

}
