package se.modlab.simplesim.application;
 
import java.io.*;
import java.awt.*;
import javax.swing.*;
import se.modlab.simpleide.*;

public class Main
{

  public static void main(String args[])
  {
    File fi = null;
    if(args.length == 1)
    {
      fi = new File(args[0]);
    }
    else
    {
      fi = new File(System.getProperty("user.dir"));
    }
    FrameTracker tracker = new FrameTracker();
    Simplesim ssim = new Simplesim();

    Frame sif = new SimpleIdeFrame(
      "Simplesim. Truly versatile simulator",
      new IdeMenuHandler[] 
        { 
          ssim,
        }, 
      tracker,
      fi);
    sif.setVisible(true);
    boolean b = ssim.disclaimer_continue_eng();
    if(!b)
    {
      System.exit(-1);
    }
  }

}