package se.modlab.simplesim.application;

import se.modlab.generics.files.*;
import se.modlab.generics.bshro.ifc.HierarchyObject;
import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.files.OneFileNotepad;
import se.modlab.generics.gui.progress.*;
import se.modlab.simpleide.*;

import java.awt.*;
import java.awt.event.*;
import java.io.*;

import javax.swing.*;

import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.tables.*;
import se.modlab.simplesim.algorithms.share.*;
import se.modlab.simplesim.algorithms.setup.*;
import se.modlab.simplesim.algorithms.illustrate.parse.*;
import se.modlab.simplesim.parse.*; 
import se.modlab.simplesim.queryParsing.*;
import se.modlab.simplesim.guiembedd.gui.Execute;
import se.modlab.simplesim.guiembedd.parse.*;
import se.modlab.simplesim.guiembedd.structure.GuiappStructTopnode;
import se.modlab.simplesim.scoping.*;

public class Simplesim extends DefaultIdeMenuHandler
{

  private static final String appname = "Simplesim";
  private final static String version = "33";
  private final static String compiledate = "2013-nov-20";
  private static final String comment = 
    "<COMMENT>\n"+
    "This is the place to put your own commentary.\n"+
    "</COMMENT>\n"; 

  private static final String calmmsg = 
      "The editor behind shows a query. It is not written to your file\n"+
      "system unless you do that after inspection. If you do save it from the\n"+
      "menubar it will be saved as\n";
 
  private Component someComponent = null;
  private ProgressDumpAndMonitor pmd = null;
  
  public Simplesim()
  {
    pmd = AppJoin.getAppJoin().getProgressMonitor();
  }

  public static Frame getFrame(Component c)
  {
    if(c == null) return null;
    if(c instanceof Frame) return (Frame) c;
    return getFrame(c.getParent());
  }

  public boolean disclaimer_continue_swe()
  {
    Object possibleValues[] = new Object[] { "Accepterar", "Refuserar" };
    int i = JOptionPane.showOptionDialog(
      getFrame(someComponent),
      "De slutsatser och konsekvenser av slutsatser som eventuellt\n"+
      "dras och görs av simuleringar och resultat av simuleringar\n"+
      "gjorda med Simplesim är helt på användarens ansvar.\n"+
      "Användaren avsäger sig genom acceptans alla yrkanden, alla\n"+
      "besvär, alla anspråk och all ansvarsfördelning av såväl\n"+
      "monetär som rättslig natur. Användaren antar ansvaret att\n"+
      "själv förvissa sig om att den simulering och simuleringsmodell\n"+
      "som görs överensstämmer med den egna föreställningen.\n"+
      "Simplesim är en generell simulator utan något\n"+
      "tillämningsområde som är mer avsiktligt än något annat.\n\n"+
      "Om det skulle kunna visas att det finns fel i Simplesim\n"+
      "har utgivande part inget ansvar för sekundära effekter.", 
      "Friskrivning", 
      JOptionPane.DEFAULT_OPTION,
      JOptionPane.INFORMATION_MESSAGE, 
      null,
      possibleValues, 
      possibleValues[1]);
     return (i != 1);
  }

  public boolean disclaimer_continue_eng()
  {
    Object possibleValues[] = new Object[] { "Accepts", "Declines" };
    int i = JOptionPane.showOptionDialog(
      getFrame(someComponent),
      "The conclusions and consequences of conclusions that eventually\n"+
      "are made from simulations and results of simulations made with\n"+
      "Simplesim are entirely on the users responsebility. The user\n"+
      "disclaims by accepting to all claims, all urges of responsibitlty\n"+
      "sharing of monetary and juriducal nature. The user assumes the\n"+
      "responsibility to ensure that the simulation and simulation model\n"+
      "in effect is what it is intended to be. Simplesim is a general\n"+
      "simulator without any domain of application that is more intended\n"+
      "than any other.\n\n"+
      "If it can be proven that there are errors in Simplesim the issuer\n"+
      "has no responsibilities for secondary effects.", 
      "Disclaimer", 
      JOptionPane.DEFAULT_OPTION,
      JOptionPane.INFORMATION_MESSAGE, 
      null,
      possibleValues, 
      possibleValues[1]);
     return (i != 1);
  }

  private void addVerifyToDotTab(JMenu menu, File f)
  {
    final File _f = f;
    final JMenu _menu = menu;
    JMenuItem menuItem = new JMenuItem("verify");
    menu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          try
          {
        	  HierarchyObject.setReferenceFile(_f.getParentFile());
            AppJoin.getAppJoin().setQuickMessage("Verifying "+_f.getName(), true);
            pmd.monitoredWorkStarted();
            TabParser.parseFromFile(_f, null);
            AppJoin.getAppJoin().setQuickMessage("Verified "+_f.getName(), false);
            UniversalTellUser.info(_menu, "The tablefile is correct and usable!", 
                                          "Syntax verification");
          }
          catch(IntolerableException ie)
          {
            AppJoin.getAppJoin().setQuickMessage("Verifying "+_f.getName()+" failed", false);
            ie.setAction("Verified from treeview");
            UniversalTellUser.general(_menu, ie);
          }
        }
      }
    );
  }

  private void addVerifyToDotIlu(JMenu menu, File f) {
	  final File _f = f;
	  final JMenu _menu = menu;
	  JMenuItem menuItem = new JMenuItem("verify");
	  menu.add(menuItem);
	  menuItem.addActionListener(
			  new ActionListener()
			  {
				  public void actionPerformed(ActionEvent e)
				  {
					  try
					  {
						  HierarchyObject.setReferenceFile(_f.getParentFile());
						  AppJoin.getAppJoin().setQuickMessage("Verifying "+_f.getName(), true);
						  pmd.monitoredWorkStarted();
						  SimScopeFactory ssf = new SimScopeFactory();
						  SimData sd = new SimData("", ssf);
						  sd.setEndTimeAsDatetimeLiteral("2005/01/01 00:00:00");
						  IluParser.parseFromFile(_f.getName(), ssf, sd);
						  AppJoin.getAppJoin().setQuickMessage("Verified "+_f.getName(), false);
						  UniversalTellUser.info(_menu, "The illustration file is correct and usable!", 
								  "Syntax verification");
					  }
					  catch(IntolerableException ie)
					  {
						  AppJoin.getAppJoin().setQuickMessage("Verifying "+_f.getName()+" failed", false);
						  ie.setAction("Verified from treeview");
						  UniversalTellUser.general(_menu, ie);
					  }
				  }
			  } 
			  );
  }

  private void addParseQueryToDotQry(JMenu menu, File f)
  {
    final File _f = f;
    final JMenu _menu = menu;
    JMenuItem menuItem = new JMenuItem("Verify query");
    menu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          try
          {
        	  HierarchyObject.setReferenceFile(_f.getParentFile());
            AppJoin.getAppJoin().setQuickMessage("Verifying query "+_f.getName(), true);
            pmd.monitoredWorkStarted();
            QueryHandler handler = new QueryHandler(HierarchyObject.getReferenceFilePath());
            handler.verify_filename(_f.getAbsolutePath());
            AppJoin.getAppJoin().setQuickMessage("Verifying query "+_f.getName(), false);
            UniversalTellUser.info(_menu, "The query is correct and usable!", 
                                          "Syntax verification");
          }
          catch(IntolerableException ie)
          {
            AppJoin.getAppJoin().setQuickMessage("Verifying query "+_f.getName()+" failed", false);
            ie.setAction("Query verifyed from treeview");
            UniversalTellUser.general(_menu, ie);
          }
        }
      }
    );
    menuItem = new JMenuItem("Analyse query");
    menu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          try
          {
        	  HierarchyObject.setReferenceFile(_f.getParentFile());
            AppJoin.getAppJoin().setQuickMessage("Analysing query "+_f.getName(), true);
            pmd.monitoredWorkStarted();
            QueryHandler handler = new QueryHandler(HierarchyObject.getReferenceFilePath());
            handler.analyse_filename(_f.getAbsolutePath());
            AppJoin.getAppJoin().setQuickMessage("Analysing query "+_f.getName(), false);
          }
          catch(IntolerableException ie)
          {
            AppJoin.getAppJoin().setQuickMessage("Analysing query "+_f.getName()+" failed", false);
            ie.setAction("Query analysed from treeview");
            UniversalTellUser.general(_menu, ie);
          }
        }
      }
    );
    menuItem = new JMenuItem("Extract querys files");
    menu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          try
          {
        	  HierarchyObject.setReferenceFile(_f.getParentFile());
            AppJoin.getAppJoin().setQuickMessage("Extracting files from query "+_f.getName(), true);
            pmd.monitoredWorkStarted();
            QueryHandler handler = new QueryHandler(HierarchyObject.getReferenceFilePath());
            handler.extract_filename(_f.getAbsolutePath());
            AppJoin.getAppJoin().setQuickMessage("Extracting files from query "+_f.getName(), false);
          }
          catch(IntolerableException ie)
          {
            AppJoin.getAppJoin().setQuickMessage("Extracting files from query "+_f.getName()+" failed", false);
            ie.setAction("Query analysed from treeview");
            UniversalTellUser.general(_menu, ie);
          }
        }
      }
    );
  }

  private void addParseQueryToDotEdq(JMenu menu, File f)
  {
    final File _f = f;
    final JMenu _menu = menu;
    JMenuItem menuItem = new JMenuItem("Verify errordump");
    menu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          try
          {
        	  HierarchyObject.setReferenceFile(_f.getParentFile());
            AppJoin.getAppJoin().setQuickMessage("Parsing errordump "+_f.getName(), true);
            pmd.monitoredWorkStarted();
            ErrordumpHandler handler = new ErrordumpHandler(HierarchyObject.getReferenceFilePath());
            handler.verify_filename(_f.getAbsolutePath());
            AppJoin.getAppJoin().setQuickMessage("Parsed errordump "+_f.getName(), false);
            UniversalTellUser.info(_menu, "The errordump is correct and usable!", 
                                          "Syntax verification");
          }
          catch(IntolerableException ie)
          {
            AppJoin.getAppJoin().setQuickMessage("Verifying errordump "+_f.getName()+" failed", false);
            ie.setAction("Errordump analysed from treeview");
            UniversalTellUser.general(_menu, ie);
          }
        }
      }
    );
    /*
    menuItem = new JMenuItem("Analyse errordump");
    menu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
        }
      }
    );
    menuItem = new JMenuItem("Extract errordumps files");
    menu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
        }
      }
    );
    */
  }

  private void addVerifyToDotTab(JMenu menu, OneFileNotepad ofn)
  {
    final OneFileNotepad _ofn = ofn;
    final JMenu _menu = menu;
    JMenuItem menuItem = new JMenuItem("verify");
    menu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          try
          {
        	  HierarchyObject.setReferenceFile(_ofn.getFile().getParentFile());
            //String path = _ofn.getFile().getParentFile().getAbsolutePath();
            //System.out.println("2: "+path);
            //System.setProperty("user.dir", path);
            AppJoin.getAppJoin().setQuickMessage("Verifying tabfile", true);
            pmd.monitoredWorkStarted();
            TabParser.parseFromString(_ofn.getText());
            AppJoin.getAppJoin().setQuickMessage("Done verifying tabfile", false);
            UniversalTellUser.info(_menu, "The tablefile is correct and usable!", 
                                          "Syntax verification");
          }
          catch(IntolerableException ie)
          {
            AppJoin.getAppJoin().setQuickMessage("Verifying tabfile failed", true);
            ie.setAction("Verified from editor");
            UniversalTellUser.general(_menu, ie);
          }
        }
      }
    );
  }

  private void addVerifyToDotIlu(JMenu menu, OneFileNotepad ofn)
  {
    final OneFileNotepad _ofn = ofn;
    final JMenu _menu = menu;
    JMenuItem menuItem = new JMenuItem("verify");
    menu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          try
          {
        	  HierarchyObject.setReferenceFile(_ofn.getFile().getParentFile());
            AppJoin.getAppJoin().setQuickMessage("Verifying illustration file", true);
            pmd.monitoredWorkStarted();
            SimScopeFactory ssf = new SimScopeFactory();
            SimData sd = new SimData("", ssf);
            sd.setEndTimeAsDatetimeLiteral("2005/01/01 00:00:00");
            IluParser.parseFromString(_ofn.getText(), ssf, sd, "Editor window");
            AppJoin.getAppJoin().setQuickMessage("Done verifying illustration file", false);
            UniversalTellUser.info(_menu, "The illustration file is correct and usable!", 
                                          "Syntax verification");
          }
          catch(IntolerableException ie)
          {
            AppJoin.getAppJoin().setQuickMessage("Verifying illustration failed", true);
            ie.setAction("Verified from editor");
            UniversalTellUser.general(_menu, ie);
          }
        }
      }
    );
  }

  private void addVerifyToDotGuiapp(final JMenu menu, OneFileNotepad ofn)
  {
    final OneFileNotepad _ofn = ofn;
    JMenuItem menuItem = new JMenuItem("verify");
    menu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          try
          {
        	  HierarchyObject.setReferenceFile(_ofn.getFile().getParentFile());
            GuiappParser.parseFromString(_ofn.getText(), "Editor window");
            UniversalTellUser.info(menu, "The GUI application file is correct and usable!", 
                                          "Syntax verification");
          }
          catch(IntolerableException ie)
          {
            AppJoin.getAppJoin().setQuickMessage("Verifying GUI application failed", true);
            ie.setAction("Verified from editor");
            UniversalTellUser.general(menu, ie);
          }
        }
      }
    );
    menuItem = new JMenuItem("run");
    menu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          try
          {
        	  HierarchyObject.setReferenceFile(_ofn.getFile().getParentFile());
            AppJoin.getAppJoin().setQuickMessage("Executing GUI application file", true);
            //pmd.monitoredWorkStarted();
            GuiappStructTopnode gatn = GuiappParser.parseFromString(_ofn.getText(), "Editor window");
            Execute.main(gatn, menu);
            AppJoin.getAppJoin().setQuickMessage("Done executing GUI application file", false);
            
          }
          catch(IntolerableException ie)
          {
            AppJoin.getAppJoin().setQuickMessage("Executing GUI application failed", true);
            ie.setAction("Executed from editor");
            UniversalTellUser.general(menu, ie);
          }
        }
      }
    );
  }

  private void addParseQueryToDotQry(JMenu menu, OneFileNotepad ofn)
  {
    final OneFileNotepad _ofn = ofn;
    final JMenu _menu = menu;
    JMenuItem menuItem = new JMenuItem("Verify query");
    menu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          try
          {
        	  HierarchyObject.setReferenceFile(_ofn.getFile().getParentFile());
            AppJoin.getAppJoin().setQuickMessage("Parsing query "+_ofn.getFile().getName(), true);
            pmd.monitoredWorkStarted();
            QueryHandler handler = new QueryHandler(HierarchyObject.getReferenceFilePath());
            handler.verify_text(_ofn.getText());
            AppJoin.getAppJoin().setQuickMessage("Parsed query "+_ofn.getFile().getName(), false);
            UniversalTellUser.info(_menu, "The query is correct and usable!", 
                                          "Syntax verification");
          }
          catch(IntolerableException ie)
          {
            AppJoin.getAppJoin().setQuickMessage("Verifying query "+_ofn.getFile().getName()+" failed", false);
            ie.setAction("Query analysed from editor");
            UniversalTellUser.general(_menu, ie);
          }
        }
      }
    );
    menuItem = new JMenuItem("Analyse query");
    menu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          try
          {
        	  HierarchyObject.setReferenceFile(_ofn.getFile().getParentFile());
            AppJoin.getAppJoin().setQuickMessage("Analysing query "+_ofn.getFile().getName(), true);
            pmd.monitoredWorkStarted();
            QueryHandler handler = new QueryHandler(HierarchyObject.getReferenceFilePath());
            handler.analyse_filename(_ofn.getFile().getAbsolutePath());
            AppJoin.getAppJoin().setQuickMessage("Analysing query "+_ofn.getFile().getName(), false);
          }
          catch(IntolerableException ie)
          {
            AppJoin.getAppJoin().setQuickMessage("Analysing query "+_ofn.getFile().getName()+" failed", false);
            ie.setAction("Query analysed from editor");
            UniversalTellUser.general(_menu, ie);
          }
        }
      }
    );
    menuItem = new JMenuItem("Extract querys files");
    menu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          try
          {
        	  HierarchyObject.setReferenceFile(_ofn.getFile().getParentFile());
            AppJoin.getAppJoin().setQuickMessage("Extracting files from query "+_ofn.getFile().getName(), true);
            pmd.monitoredWorkStarted();
            QueryHandler handler = new QueryHandler(HierarchyObject.getReferenceFilePath());
            handler.extract_filename(_ofn.getFile().getAbsolutePath());
            AppJoin.getAppJoin().setQuickMessage("Extracting files from query "+_ofn.getFile().getName(), false);
          }
          catch(IntolerableException ie)
          {
            AppJoin.getAppJoin().setQuickMessage("Extracting files from query "+_ofn.getFile().getName()+" failed", false);
            ie.setAction("Query analysed from editor");
            UniversalTellUser.general(_menu, ie);
          }
        }
      }
    );
  }

  private void addParseQueryToDotEdq(JMenu menu, OneFileNotepad ofn)
  {
    final OneFileNotepad _ofn = ofn;
    final JMenu _menu = menu;
    JMenuItem menuItem = new JMenuItem("Verify query");
    menu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          try
          {
        	  HierarchyObject.setReferenceFile(_ofn.getFile().getParentFile());
            AppJoin.getAppJoin().setQuickMessage("Parsing query "+_ofn.getFile().getName(), true);
            pmd.monitoredWorkStarted();
            ErrordumpHandler handler = new ErrordumpHandler(HierarchyObject.getReferenceFilePath());
            handler.verify_text(_ofn.getText());
            AppJoin.getAppJoin().setQuickMessage("Parsed query "+_ofn.getFile().getName(), false);
            UniversalTellUser.info(_menu, "The query is correct and usable!", 
                                          "Syntax verification");
          }
          catch(IntolerableException ie)
          {
            AppJoin.getAppJoin().setQuickMessage("Verifying query "+_ofn.getFile().getName()+" failed", false);
            ie.setAction("Query analysed from editor");
            UniversalTellUser.general(_menu, ie);
          }
        }
      }
    );
    menuItem = new JMenuItem("Analyse query");
    menu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
        }
      }
    );
    menuItem = new JMenuItem("Extract querys files");
    menu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
        }
      }
    );
  }


  private void addRunAndVerifyToDotGuiapp(final JMenu menu, final File f)
  {
    JMenuItem menuItem = new JMenuItem("run");
    menu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
            try
            {
            	HierarchyObject.setReferenceFile(f.getParentFile());
                GuiappStructTopnode gatn = GuiappParser.parseFromFile(f.getName());
                Execute.main(gatn, menu);
            }
            catch(IntolerableException ie)
            {
              AppJoin.getAppJoin().setQuickMessage("Starting GUI application failed", true);
              ie.setAction("Executed from editor");
              UniversalTellUser.general(menu, ie);
            }
        }
      });
    menuItem = new JMenuItem("verify");
    menu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
        	try 
        	{
        		HierarchyObject.setReferenceFile(f.getParentFile());
                //appJoin.getAppJoin().setQuickMessage("Verify GUI application", true);
                //pmd.monitoredWorkStarted();
                GuiappStructTopnode gatn = GuiappParser.parseFromFile(f.getName());
                //Execute.main(gatn, menu);
                //appJoin.getAppJoin().setQuickMessage("Done executing GUI application file", false);
                UniversalTellUser.info(menu, "Verification successful!",
                	"Verify GUI application");
              }
              catch(IntolerableException ie)
              {
                AppJoin.getAppJoin().setQuickMessage("Verifying GUI application failed", true);
                ie.setAction("Verified from file");
                UniversalTellUser.general(menu, ie);
              }
        }
      });
  }
  
  private void addRunAndVerifyToDotSim(JMenu menu, final File f)
  {
    JMenuItem menuItem = new JMenuItem("run");
    menu.add(menuItem);
    final JMenu _menu = menu;
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          Algorithm algo = null;
          try
          {
        	  HierarchyObject.setReferenceFile(f.getParentFile());
            algo = SimParser.parseFromFile(f.getName(), new SimScopeFactory(), false);
            AlgorithmRunner ar = new AlgorithmRunner(algo, _menu, "Executing from treeview");
            AppJoin.getAppJoin().runAlgorithm(ar);
            //appJoin.getAppJoin().displayResultsGraphically(ar);
          }
          catch(IntolerableException ie)
          { 
            if(algo != null) ie.setCollectors(algo.getCollectors());
            ie.setAction("Executed from treeview");
            UniversalTellUser.general(_menu, ie);
          }
          catch(Exception _e)
          {
            IntolerableException ie = 
              new UnclassedError("SimFileMenyFactory.addRunAndVerifyToDotSim f - run - ex",
                                 _e);
            if(algo != null) ie.setCollectors(algo.getCollectors());
            ie.setAction("Executed from treeview");
            UniversalTellUser.general(_menu, ie);
          }
          catch(Throwable _e)
          {
            IntolerableException ie = 
              new UnclassedError("SimFileMenyFactory.addRunAndVerifyToDotSim f - run - th",
                                 _e);
            if(algo != null) ie.setCollectors(algo.getCollectors());
            ie.setAction("Executed from treeview");
            UniversalTellUser.general(_menu, ie);
          }
        }
      }
    );
    menuItem = new JMenuItem("verify");
    menu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          Algorithm algo = null;
          try
          {
        	  HierarchyObject.setReferenceFile(f.getParentFile());
            //String path = _f.getParentFile().getAbsolutePath()+File.separator;
            //System.out.println("4: "+path);
            //System.setProperty("user.dir", path);
            //System.setProperty("user.dir", _f.getParentFile().getAbsolutePath());
            algo = SimParser.parseFromFile(f.getName(), new SimScopeFactory(), true);
            //Thread t = new Thread(algo);
            UniversalTellUser.info(_menu, "The program is correct and runnable!", 
                                         "Syntax verification");
          }
          catch(IntolerableException ie)
          {
            if(algo != null) ie.setCollectors(algo.getCollectors());
            ie.setAction("Verified from treeview");
            UniversalTellUser.general(_menu, ie);
          }
          catch(Exception _e)
          {
            IntolerableException ie = 
              new UnclassedError("SimFileMenyFactory.addRunAndVerifyToDotSim f - verify - ex",
                                 _e);
            if(algo != null) ie.setCollectors(algo.getCollectors());
            ie.setAction("Verified from treeview");
            UniversalTellUser.general(_menu, ie);
          }
          catch(Throwable _e)
          {
            IntolerableException ie = 
              new UnclassedError("SimFileMenyFactory.addRunAndVerifyToDotSim f - verify - th",
                                 _e);
            if(algo != null) ie.setCollectors(algo.getCollectors());
            ie.setAction("Verified from treeview");
            UniversalTellUser.general(_menu, ie);
          }
        }
      }
    );
    menuItem = new JMenuItem("create query");
    menu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          Algorithm algo = null;
          try
          {
        	  HierarchyObject.setReferenceFile(f.getParentFile());
            String simFile = f.getName();
            int dot = simFile.lastIndexOf('.');
            algo = SimParser.parseFromFile(f.getName(), new SimScopeFactory(), false);
            StringBuffer sb = new StringBuffer("<QUERY>\n"+comment);
            FileCollector _fc[] = algo.getCollectors();
            if(_fc != null)
            {
              for(int i = 0 ; i < _fc.length ; i++)
              {
                sb.append("<FILE>\n"+ 
                          "<FILENAME>\n"+_fc[i].getFilename()+"\n</FILENAME>\n"+
                          "<FILECONTENTS>\n"+_fc[i].getFilecontents()+"\n</FILECONTENTS>\n"+
                          "</FILE>\n");
              }
            }
            sb.append("</QUERY>");
            String qryFile = HierarchyObject.getReferenceFilePath()+simFile.substring(0, dot)+".qry";
            OneFileNotepad __ofn = OneFileNotepad.note(qryFile);
            __ofn.setText(sb.toString());
            UniversalTellUser.info(__ofn, calmmsg+qryFile, "About this editor");
          }
          catch(IntolerableException ie)
          {
            if(algo != null) ie.setCollectors(algo.getCollectors());
            ie.setAction("Creating query from treeview");
            UniversalTellUser.general(_menu, ie);
          }
          catch(Exception _e)
          {
            IntolerableException ie = 
              new UnclassedError("SimFileMenyFactory.addRunAndVerifyToDotSim ofn - verify - ex",
                                 _e);
            if(algo != null) ie.setCollectors(algo.getCollectors());
            ie.setAction("Creating query from treeview");
            UniversalTellUser.general(_menu, ie);
          }
          catch(Throwable _e)
          {
            IntolerableException ie = 
              new UnclassedError("SimFileMenyFactory.addRunAndVerifyToDotSim ofn - verify - th",
                                 _e);
            if(algo != null) ie.setCollectors(algo.getCollectors());
            ie.setAction("Creating query from treeview");
            UniversalTellUser.general(_menu, ie);
          }
        }
      }
    );
  }

  private void addVerifyToDotEsim(JMenu menu, File f)
  {
    JMenuItem menuItem = new JMenuItem("verify");
    menu.add(menuItem);
    final JMenu _menu = menu;
    final File _f = f;  
    //final OneFileNotepad _ofn = ofn;
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          try
          {
        	  HierarchyObject.setReferenceFile(_f.getParentFile());
            //String path = _f.getParentFile().getAbsolutePath()+File.separator;
            //System.out.println("4: "+path);
            //System.setProperty("user.dir", path);
            //System.setProperty("user.dir", _f.getParentFile().getAbsolutePath());
            SimScopeFactory ssf = new SimScopeFactory();
            SimData sd = new SimData("", ssf);
            sd.setEndTimeAsDatetimeLiteral("2005/01/01 00:00:00");
            SimScope globalScope = SimParser.makeFakeGlobalScope(sd, ssf);
            SimParser.parseFromIncludeFile(_f.getName(), 
                                           sd, 
                                           ssf,
                                           true,
                                           globalScope);
            //Thread t = new Thread(algo);
            UniversalTellUser.info(_menu, "The program extention is correct and useable!", 
                                         "Syntax verification");
          }
          catch(IntolerableException ie)
          {
            ie.setAction("Verified from treeview");
            UniversalTellUser.general(_menu, ie);
          }
          catch(Exception _e)
          {
            IntolerableException ie = 
              new UnclassedError("SimFileMenyFactory.addRunAndVerifyToDotSim f - verify - ex",
                                 _e);
            ie.setAction("Verified from treeview");
            UniversalTellUser.general(_menu, ie);
          }
          catch(Throwable _e)
          {
            IntolerableException ie = 
              new UnclassedError("SimFileMenyFactory.addRunAndVerifyToDotSim f - verify - th",
                                 _e);
            ie.setAction("Verified from treeview");
            UniversalTellUser.general(_menu, ie);
          }
        }
      }
    );
  }

  private void addRunAndVerifyToDotSim(JMenu menu, OneFileNotepad ofn)
  {
    JMenuItem menuItem = new JMenuItem("run");
    menu.add(menuItem);
    //final File _f = ofn.getFile();
    final JMenu _menu = menu;
    final OneFileNotepad _ofn = ofn;
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          Algorithm algo = null;
          try
          {
        	  HierarchyObject.setReferenceFile(_ofn.getFile().getParentFile());
            algo = SimParser.parseFromString(_ofn.getText(), new SimScopeFactory(), false);
            AlgorithmRunner ar = new AlgorithmRunner(algo, _menu, "Executing from editor");
            AppJoin.getAppJoin().runAlgorithm(ar);
            //appJoin.getAppJoin().displayResultsGraphically(ar);
          }
          catch(IntolerableException ie)
          {
            if(algo != null) ie.setCollectors(algo.getCollectors());
            ie.setAction("Executed from Notepad");
            UniversalTellUser.general(_menu, ie);
          }
          catch(Exception _e)
          {
            IntolerableException ie = 
              new UnclassedError("SimFileMenyFactory.addRunAndVerifyToDotSim ofn - run - ex",
                                 _e);
            if(algo != null) ie.setCollectors(algo.getCollectors());
            ie.setAction("Executed from Notepad");
            UniversalTellUser.general(_menu, ie);
          }
          catch(Throwable _e)
          {
            IntolerableException ie = 
              new UnclassedError("SimFileMenyFactory.addRunAndVerifyToDotSim ofn - run - th",
                                 _e);
            if(algo != null) ie.setCollectors(algo.getCollectors());
            ie.setAction("Executed from Notepad");
            UniversalTellUser.general(_menu, ie);
          }
        }
      }
    );
    menuItem = new JMenuItem("verify");
    menu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          Algorithm algo = null;
          try
          {
        	  HierarchyObject.setReferenceFile(_ofn.getFile().getParentFile());
            //String path = _ofn.getFile().getParentFile().getAbsolutePath()+File.separator;
            //System.out.println("6: "+path);
            //System.setProperty("user.dir", path);
            //System.setProperty("user.dir", _ofn.getFile().getParentFile().getAbsolutePath());
            algo = SimParser.parseFromString(_ofn.getText(), new SimScopeFactory(), true);
            //Thread t = new Thread(algo);
            UniversalTellUser.info(_menu, "The program is correct and runnable!", 
                                         "Syntax verification");
          }
          catch(IntolerableException ie)
          {
            if(algo != null) ie.setCollectors(algo.getCollectors());
            ie.setAction("Verified from Notepad");
            UniversalTellUser.general(_menu, ie);
          }
          catch(Exception _e)
          {
            IntolerableException ie = 
              new UnclassedError("SimFileMenyFactory.addRunAndVerifyToDotSim ofn - verify - ex",
                                 _e);
            if(algo != null) ie.setCollectors(algo.getCollectors());
            ie.setAction("Verified from Notepad");
            UniversalTellUser.general(_menu, ie);
          }
          catch(Throwable _e)
          {
            IntolerableException ie = 
              new UnclassedError("SimFileMenyFactory.addRunAndVerifyToDotSim ofn - verify - th",
                                 _e);
            if(algo != null) ie.setCollectors(algo.getCollectors());
            ie.setAction("Verified from Notepad");
            UniversalTellUser.general(_menu, ie);
          }
        }
      }
    );
    menuItem = new JMenuItem("create query");
    menu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          Algorithm algo = null;
          try
          {
        	  HierarchyObject.setReferenceFile(_ofn.getFile().getParentFile());
            String simFile = _ofn.getFile().getName();
            int dot = simFile.lastIndexOf('.');
            String qryFile = simFile.substring(0, dot)+".qry";
            algo = SimParser.parseFromFile(_ofn.getFile().getName(), new SimScopeFactory(), false);
            StringBuffer sb = new StringBuffer("<QUERY>\n"+comment);
            FileCollector _fc[] = algo.getCollectors();
            if(_fc != null)
            {
              for(int i = 0 ; i < _fc.length ; i++)
              {
                sb.append("<FILE>\n"+ 
                          "<FILENAME>\n"+_fc[i].getFilename()+"\n</FILENAME>\n"+
                          "<FILECONTENTS>\n"+_fc[i].getFilecontents()+"\n</FILECONTENTS>\n"+
                          "</FILE>\n");
              }
            }
            sb.append("</QUERY>");
            OneFileNotepad __ofn = OneFileNotepad.note(qryFile);
            __ofn.setText(sb.toString());
            UniversalTellUser.info(__ofn, calmmsg+qryFile, "About this editor");
          }
          catch(IntolerableException ie)
          {
            if(algo != null) ie.setCollectors(algo.getCollectors());
            ie.setAction("Creating query from Notepad");
            UniversalTellUser.general(_menu, ie);
          }
          catch(Exception _e)
          {
            IntolerableException ie = 
              new UnclassedError("SimFileMenyFactory.addRunAndVerifyToDotSim ofn - verify - ex",
                                 _e);
            if(algo != null) ie.setCollectors(algo.getCollectors());
            ie.setAction("Creating query from Notepad");
            UniversalTellUser.general(_menu, ie);
          }
          catch(Throwable _e)
          {
            IntolerableException ie = 
              new UnclassedError("SimFileMenyFactory.addRunAndVerifyToDotSim ofn - verify - th",
                                 _e);
            if(algo != null) ie.setCollectors(algo.getCollectors());
            ie.setAction("Creating query from Notepad");
            UniversalTellUser.general(_menu, ie);
          }
        }
      }
    );
  }

  private void addVerifyToDotEsim(JMenu menu, OneFileNotepad ofn)
  {
    JMenuItem menuItem = new JMenuItem("verify");
    menu.add(menuItem);
    //final File _f = ofn.getFile();
    final JMenu _menu = menu;
    final OneFileNotepad _ofn = ofn;
    menu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          try
          {
        	  HierarchyObject.setReferenceFile(_ofn.getFile().getParentFile());
            //String path = _ofn.getFile().getParentFile().getAbsolutePath()+File.separator;
            //System.out.println("6: "+path);
            //System.setProperty("user.dir", path);
            //System.setProperty("user.dir", _ofn.getFile().getParentFile().getAbsolutePath());
            SimScopeFactory ssf = new SimScopeFactory();
            SimData sd = new SimData("", ssf);
            sd.setEndTimeAsDatetimeLiteral("2005/01/01 00:00:00");
            SimScope globalScope = SimParser.makeFakeGlobalScope(sd, ssf);
            SimParser.parseFromIncludeString(_ofn.getText(), 
                                             sd,
                                             ssf,
                                             true,
                                             globalScope);
            //Thread t = new Thread(algo);
            UniversalTellUser.info(_menu, "The program extension is correct and useable!", 
                                         "Syntax verification");
          }
          catch(IntolerableException ie)
          {
            ie.setAction("Verified from Notepad");
            UniversalTellUser.general(_menu, ie);
          }
          catch(Exception _e)
          {
            IntolerableException ie = 
              new UnclassedError("SimFileMenyFactory.addRunAndVerifyToDotSim ofn - verify - ex",
                                 _e);
            ie.setAction("Verified from Notepad");
            UniversalTellUser.general(_menu, ie);
          }
          catch(Throwable _e)
          {
            IntolerableException ie = 
              new UnclassedError("SimFileMenyFactory.addRunAndVerifyToDotSim ofn - verify - th",
                                 _e);
            ie.setAction("Verified from Notepad");
            UniversalTellUser.general(_menu, ie);
          }
        }
      }
    );
  }

  private void addInsertTemplatesToDotGuiapp(JMenu menu, OneFileNotepad ofn)
  {
	  final JMenu _menu = menu;
	  final OneFileNotepad _ofn = ofn;
	  JMenu templates = new JMenu("insert templates");
	  menu.add(templates);
	  JMenuItem menuItem = new JMenuItem("settings");
	  templates.add(menuItem);
	  menuItem.addActionListener(
			  new ActionListener()
			  {
				  public void actionPerformed(ActionEvent e)
				  {
					  _ofn.insertAtCaretPosition(
							  "settings\n"+
							  "{\n"+
								  "  File \"scenario.sim\";\n"+
								  "  title \"Framtitle of parameter window\";\n"+
								  "  width 890\n"+
								  "  height 630\n"+
								  "}\n"
					  );
				  }
			  });
	  menuItem = new JMenuItem("tab");
	  templates.add(menuItem);
	  menuItem.addActionListener(
			  new ActionListener()
			  {
				  public void actionPerformed(ActionEvent e)
				  {
					  _ofn.insertAtCaretPosition(
							  "tab \"Name of tab\"\n"+
							  "{\n"+
						      "  {\n"+
							  "  // insert field here\n"+
							  "  }\n"+
							  "}\n"
					  );
				  }
			  });
	  JMenu titledFields = new JMenu("Variable fields");
	  templates.add(titledFields);
	  menuItem = new JMenuItem("boolean field");
	  titledFields.add(menuItem);
	  menuItem.addActionListener(
			  new ActionListener()
			  {
				  public void actionPerformed(ActionEvent e)
				  {
					  _ofn.insertAtCaretPosition(
						      "  {\n"+
							  "    boolean a,      // Variable name from underlying script\n"+
							  "    true,           // Initiation value trye/false/expression\n"+
							  "    \"Nicer name\", // Variable name as to be presented in GUI\n"+
							  "    tip \"Leading text to appear as tooltip\" // Not necessary\n"+
							  "  }\n"
					  );
				  }
			  });
	  menuItem = new JMenuItem("long field");
	  titledFields.add(menuItem);
	  menuItem.addActionListener(
			  new ActionListener()
			  {
				  public void actionPerformed(ActionEvent e)
				  {
					  _ofn.insertAtCaretPosition(
						      "  {\n"+
							  "    long a,         // Variable name from underlying script\n"+
							  "    1,              // Initiation value trye/false/expression\n"+
							  "    \"Nicer name\", // Variable name as to be presented in GUI\n"+
							  "    tip \"Leading text to appear as tooltip\" // Not necessary\n"+
							  "  }\n"
					  );
				  }
			  });
	  menuItem = new JMenuItem("double field");
	  titledFields.add(menuItem);
	  menuItem.addActionListener(
			  new ActionListener()
			  {
				  public void actionPerformed(ActionEvent e)
				  {
					  _ofn.insertAtCaretPosition(
						      "  {\n"+
							  "    double a,       // Variable name from underlying script\n"+
							  "    1.0,            // Initiation value trye/false/expression\n"+
							  "    \"Nicer name\", // Variable name as to be presented in GUI\n"+
							  "    tip \"Leading text to appear as tooltip\" // Not necessary\n"+
							  "  }\n"
					  );
				  }
			  });
	  menuItem = new JMenuItem("table field");
	  titledFields.add(menuItem);
	  menuItem.addActionListener(
			  new ActionListener()
			  {
				  public void actionPerformed(ActionEvent e)
				  {
					  _ofn.insertAtCaretPosition(
						      "  {\n"+
							  "    table\n"+
							  "    columns \"col1\", \"col2\", \"col3\", \"col4\", \"col5\"; // Not necessary\n"+
							  "    vectorname,   // Variable name\n"+
							  "    \"x.tab\",      // Initiation value from same folder\n"+
							  "    \"Nicer name\"  // To appear in the graphics\n"+
							  "    tip \"Leading text to appear as tooltip\" // Not necessary\n"+
							  "  }\n"
					  );
				  }
			  });
	  titledFields = new JMenu("Special tabfields");
	  templates.add(titledFields);
	  menuItem = new JMenuItem("starttime");
	  titledFields.add(menuItem);
	  menuItem.addActionListener(
			  new ActionListener()
			  {
				  public void actionPerformed(ActionEvent e)
				  {
					  _ofn.insertAtCaretPosition(
						      "  {\n"+
							  "    starttime \"YYYY/MM/DD HH:MM:SS\", // Initial date. Blanc = now\n"+
							  "    \"Etart time\", // As you want start time expressed in teh GUI\n"+
							  "    tip \"Leading text to appear as tooltip\" // Not necessary\n"+
							  "  }\n"
					  );
				  }
			  });
	  menuItem = new JMenuItem("endtime");
	  titledFields.add(menuItem);
	  menuItem.addActionListener(
			  new ActionListener()
			  {
				  public void actionPerformed(ActionEvent e)
				  {
					  _ofn.insertAtCaretPosition(
						      "  {\n"+
							  "    endtime \"YYYY/MM/DD HH:MM:SS\", // Final date or nummer of simulation timeunits. Blanc = now\n"+
							  "    \"End time\", // As you want start time expressed in teh GUI\n"+
							  "    tip \"Leading text to appear as tooltip\" // Not necessary\n"+
							  "  }\n"
					  );
				  }
			  });
	  menuItem = new JMenuItem("Text area");
	  titledFields.add(menuItem);
	  menuItem.addActionListener(
			  new ActionListener()
			  {
				  public void actionPerformed(ActionEvent e)
				  {
					  _ofn.insertAtCaretPosition(
						      "  {\n"+
							  "    textarea \"name me\"\n"+
							  "    \"Message line 1\", // Repeat this line \n"+
							  "    tip \"Leading text to appear as tooltip\" // Not necessary\n"+
							  "  }\n"
					  );
				  }
			  });
	  titledFields = new JMenu("Presentation");
	  templates.add(titledFields);
	  menuItem = new JMenuItem("Surrounding");
	  titledFields.add(menuItem);
	  menuItem.addActionListener(
			  new ActionListener()
			  {
				  public void actionPerformed(ActionEvent e)
				  {
					  _ofn.insertAtCaretPosition(
							  "presentation \"Presentation window frame title\"\n"+
							  "{\n"+
						      "  {\n"+
							  "  // Insert variable information within curly braces here here\n"+
							  "  }\n"+
							  "}\n"
					  );
				  }
			  });
	  menuItem = new JMenuItem("Variable");
	  titledFields.add(menuItem);
	  menuItem.addActionListener(
			  new ActionListener()
			  {
				  public void actionPerformed(ActionEvent e)
				  {
					  _ofn.insertAtCaretPosition(
						      "  {\n"+
							  "    double a,       // Variable \n"+
							  "    \"Nicer name\", // Variable name as to be presented in GUI\n"+
							  "    tip \"Leading text to appear as tooltip\" // Not necessary\n"+
							  "  }\n"
					  );
				  }
			  });
  }

  private void addInsertTemplatesToDotIlu(JMenu menu, OneFileNotepad ofn)
  {
    JMenu tempMenu = new JMenu("insert templates");
    JMenuItem menuItem = new JMenuItem("settings");
    tempMenu.add(menuItem);
    menu.add(tempMenu);
    final JMenu _menu = menu;
    final OneFileNotepad _ofn = ofn;
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(
            "settings\n"+
            "{\n"+
            "  illustration onImage;             // Only onImage available yet.\n"+
            "  image \"images/screendump.jpg\";  // The image in the background. The filename.\n"+
            "}\n"
          );
        }
      });
    menuItem = new JMenuItem("static label");
    tempMenu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(
            "static someNameOfStatic\n"+
            "{\n"+
            "  scope globals;\n"+
            "  icon \"filename.gif\";   // Icon to show. .gif, .jpg, .png .\n"+
            "  calculate\n"+
            "  {\n"+
            "    long xpos = trunc(10 + ((((imagewidth - 130) / 2) - 10) * cosinus));\n"+
            "    long ypos = trunc(10 + ((((imageheight - 80) / 2) - 10) * sinus));\n"+
            "    string back = \"red\";\n"+
            "    string text = \"circle\";\n"+
            "    boolean shown = true;\n"+
            "  }\n"+
            "  label\n"+
            "  {\n"+
            "    visible shown;    // Logical expression. Weather to show...\n"+
            "    background back;  // Background color as string.\n"+
            "    horizontal xpos;  // Arithemtic expression. \n"+
            "    vertical ypos;    // Arithmetic expression. \n"+
            "    textual text;     // Text to the right of icon.\n"+
            "  }\n"+
            "}\n");
        }
      });
    menuItem = new JMenuItem("dynamic label");
    tempMenu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(
            "dynamic someNameOfDynamic\n"+
            "{\n"+
            "  calculate\n"+
            "  {\n"+
            "    long xpos = trunc(10 + ((((imagewidth - 130) / 2) - 10) * cosinus));\n"+
            "    long ypos = trunc(10 + ((((imageheight - 80) / 2) - 10) * sinus));\n"+
            "    string back = \"red\";\n"+
            "    string text = \"circle\";\n"+
            "    boolean shown = true;\n"+
            "  }\n"+
            "  label\n"+
            "  {\n"+
            "    visible shown;    // Logical expression. Weather to show...\n"+
            "    icon \"filename.gif\";   // Icon to show. .gif, .jpg, .png .\n"+
            "    background back;  // Background color as string.\n"+
            "    horizontal xpos;  // Arithemtic expression. \n"+
            "    vertical ypos;    // Arithmetic expression. \n"+
            "    textual text;     // Text to the right of icon.\n"+
            "  }\n"+
            "}\n");
        }
      });
  }

  private final String staticRepeating =
            "repeating someRepeating\n"+
            "{\n"+
            "  locals\n"+
            "  {\n"+
            "    long a = 1;\n"+
            "    // typedef struct or queuable ... \n"+
            "  }\n"+
            "  first 0;\n"+
            "  execute\n"+
            "  {\n"+
            "    a = 2;\n"+
            "  }\n"+
            "  requeue a == 2;\n"+
            "  again 1*2;\n"+
            "}\n";

  private final String staticSingle = 
            "single someSingle\n"+
            "{\n"+
            "  locals\n"+
            "  {\n"+
            "    double a = 1;\n"+
            "    // typedef struct or queuable ... \n"+
            "  }\n"+
            "  pointInTime 2004/05/01 08:00:00;\n"+
            "  pointInTime 1.4*3;\n"+
            "  execute\n"+
            "  {\n"+
            "    a = 2;\n"+
            "  }\n"+
            "}\n";

  private final String staticTrigged = 
            "trigged someTrigged\n"+
            "{\n"+
            "  locals\n"+
            "  {\n"+
            "    double a = 1;\n"+
            "    // typedef struct or queuable ... \n"+
            "  }\n"+
            "  criterion a > 0;\n"+
            "  execute\n"+
            "  {\n"+
            "     a = 2;\n"+
            "  }\n"+
            "  requeue true;\n"+
            "}\n";

  private final String staticActor = 
            "actor someName\n"+
            "{\n"+
            "  locals\n"+
            "  {\n"+
            "    // Variables, vectors ... \n"+
            "    // typedef struct or queuable ... \n"+
            "  }\n"+
            "  statemachine\n"+
            "  {\n"+
            "    initially: // Must be the name initially here\n"+
            "    {\n"+
            "      onEnter { /* Code on state entry */ }\n"+
            "      onExit { /* Code on state exit */ }\n"+
            "      onSignal\n"+
            "        becomeReady { /* Code on transition. */ } transfer available;\n"+
            "        // Repeat signals with transitions.\n"+
            "        // Remain means no exit or enter.\n"+
            "    }\n"+
            "    state available: // Can choose states name here.\n"+
            "    {\n"+
            "      onEnter { /* Code on state entry */ }\n"+
            "      onExit { /* Code on state exit */ }\n"+
            "      onSignal\n"+
            "        startOperation { /* Code on transition. */ } transfer available;\n"+
            "        becomeUnready { /* Code on transition. */ } transfer available;\n"+
            "        unexpectedSignal { /* Code on transition. */ } remain;\n"+
            "        // Repeat signals with transitions.\n"+
            "        // Remain means no exit or enter.\n"+
            "    }\n"+
            "    // Repeat the state passage.\n"+
            "  }\n"+
            "}\n";

  private final String staticSettings = 
            "settings\n"+
            "{\n"+
            "  name \"some name of simulation\";\n"+
            "  timeUnit 60*1000;// One minute in millis\n"+
            "  starttime 2011/01/01 00:00:00;\n"+
            "  endtime 2012/01/01 00:00:00;\n"+
            "  algorithm once;\n"+
            "}\n";

  private final String staticGlobals = 
            "globals\n"+
            "{\n"+
            "  // Repeated typedef and variable declarations here.\n"+
            "}\n";

  private final String procedureDeclaration = 
            "  procedure procName(long var l)\n"+
            "  {\n"+
            "    l = 1.3*a;\n"+
            "  };\n";

  private final String subscopeDeclaration = 
            "  subscope scopesName\n"+
            "  {\n"+
            "    long i = 4;\n"+
            "  }; // [ public | private ] \n";

  private final String typedefStruct = 
            "  typedef struct someStruct\n"+
            "  {\n"+
            "    double d;\n"+
            "    long l;\n"+
            "    long vec[10];\n"+
            "    struct someOtherStruct instanceName1;\n"+
            "  };\n";

  private final String queuableRepetating = 
            "  typedef queuable someDynamicRepeatingTypename\n"+
            "    repeating\n"+
            "    {\n"+
            "      locals\n"+
            "      {\n"+
            "        long a = 1;\n"+
            "        // typedef struct or queuable ... \n"+
            "      }\n"+
            "      first 0;\n"+
            "      execute\n"+
            "      {\n"+
            "        a = 2;\n"+
            "      }\n"+
            "      requeue a == 2;\n"+
            "      again 1*2;\n"+
            "    };\n";

  private final String queueableTrigged = 
            "  typedef queuable someDynamicTriggedTypename\n"+
            "    trigged\n"+
            "    {\n"+
            "      locals\n"+
            "      {\n"+
            "        double a = 1;\n"+
            "        // typedef struct or queuable ... \n"+
            "      }\n"+
            "      criterion a > 0;\n"+
            "      execute\n"+
            "      {\n"+
            "        a = 2;\n"+
            "      }\n"+
            "      requeue true;\n"+
            "    };\n";

  private final String queueableActor = 
            "  typedef queuable someDynamicActorTypename\n"+
            "    actor\n"+
            "    {\n"+
            "      locals\n"+
            "      {\n"+
            "        // Variables, vectors ... \n"+
            "        // typedef struct or queuable ... \n"+
            "      }\n"+
            "      statemachine\n"+
            "      {\n"+
            "        initially:\n"+
            "        // Must start with initially.\n"+
            "        {\n"+
            "          onEnter { /* Code on state entry */ }\n"+
            "          onExit { /* Code on state exit */ }\n"+
            "          onSignal\n"+
            "            becomeReady { /* Code on transition. */ } transfer available;\n"+
            "            unexpectedSignal { /* Code on transition. */ } remain;\n"+
            "            // Repeat signals with transitions.\n"+
            "            // Remain means no exit or enter.\n"+
            "        }\n"+
            "        state available:\n"+
            "        {\n"+
            "          onEnter { /* Code on state entry */ }\n"+
            "          onExit { /* Code on state exit */ }\n"+
            "          onSignal\n"+
            "            startOperation { /* Code on transition. */ } transfer available;\n"+
            "            becomeUnready { /* Code on transition. */ } transfer available;\n"+
            "            unexpectedSignal { /* Code on transition. */ } remain;\n"+
            "            // Repeat signals with transitions.\n"+
            "            // Remain means no exit or enter.\n"+
            "        }\n"+
            "        // Repeat the state passage.\n"+
            "      }\n"+
            "    };\n";

  private final String longBag = 
    "  bag<long, oldest> myBag;      // Straight queue.\n"+
    "  bag<long, youngest> myBag;    // LIFO\n"+
    "  bag<long, largest> myBag;     // Sort for largest\n"+
    "  bag<long, smallest> myBag;    // Sort for smallest\n"+
    "  bag<long, random> myBag;      // random\n";

  private final String booleanBag = 
    "  bag<boolean, oldest> myBag;      // Straight queue.\n"+    
    "  bag<boolean, youngest> myBag;    // LIFO\n"+   
    "  bag<boolean, random> myBag;      // random\n";

  private final String doubleBag = 
    "  bag<double, oldest> myBag;      // Straight queue.\n"+
    "  bag<double, youngest> myBag;    // LIFO\n"+
    "  bag<double, largest> myBag;     // Sort for largest\n"+
    "  bag<double, smallest> myBag;    // Sort for smallest\n"+
    "  bag<double, random> myBag;      // random\n";


  private final String structQueuableBag = 
    "  bag<someStructOrQueuable, oldest> myBag;      // Straight queue.\n"+
    "  bag<someStructOrQueuable, youngest> myBag;    // LIFO\n"+
    "  bag<someStructOrQueuable, largest, d> myBag;  // Sort for largest\n"+
    "  bag<someStructOrQueuable, smallest, d> myBag; // Sort for smallest\n"+
    "  bag<someStructOrQueuable, random> myBag;      // random\n";

  private final String staticInitially = 
            "initially\n"+
            "{\n"+
            "  dialog(\"initially\");\n"+
            "}\n";

  private final String staticFinally = 
            "finally\n"+
            "{\n"+
            "  dialog(\"finally\");\n"+
            "}\n";

  private final String algoOnce = 
            "  algorithm once;";

  private final String algoDebug =
            "  algorithm debug starttime, endtime;";

  private final String algoIllustration = 
            "  algorithm illustration timeUnit 1000 \"test.ilu\" eventtable false;";

  private final String algoAverage = 
            "  algorithm average starttime, endtime, 500, 50;";

  private final String algoRegression = 
            "  algorithm regression starttime, endtime, 500, a, 0.001, 50;";

  private final String algoBruteforcemax = 
            "  algorithm bruteforcemax sum+1\n"+
            "    accept true\n"+
            "    keep 300\n"+
            "    parameters\n"+
            "      double p1 0.1, 0.9, 20;\n"+
            "      double p2 1.1, 3.9, 25;\n"+
            "    subalgorithm once;\n";

  private void addInsertTemplatesToDotSim(JMenu menu, OneFileNotepad ofn)
  {
    JMenu tempMenu = new JMenu("insert templates");
    JMenuItem menuItem = new JMenuItem("repeating");
    tempMenu.add(menuItem);
    menu.add(tempMenu);
    final JMenu _menu = menu;
    final OneFileNotepad _ofn = ofn;
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(staticRepeating);
        }
      });
    menuItem = new JMenuItem("single");
    tempMenu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(staticSingle);
        }
      });
    menuItem = new JMenuItem("trigged");
    tempMenu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(staticTrigged);
        }
      });
    menuItem = new JMenuItem("actor");
    tempMenu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(staticActor);
        }
      });
    menuItem = new JMenuItem("settings");
    tempMenu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(staticSettings);
        }
      });
    menuItem = new JMenuItem("globals");
    tempMenu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(staticGlobals);
        }
      });
    menuItem = new JMenuItem("procedure");
    tempMenu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(procedureDeclaration);
        }
      });
    menuItem = new JMenuItem("subscope");
    tempMenu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(subscopeDeclaration);
        }
      });
    JMenu typedefsMenu = new JMenu("typedef");
    tempMenu.add(typedefsMenu);
    menuItem = new JMenuItem("struct");
    typedefsMenu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(typedefStruct);
        }
      });
    menuItem = new JMenuItem("queuable repeating");
    typedefsMenu.add(menuItem); // REP
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(queuableRepetating);
        }
      });
    menuItem = new JMenuItem("queuable trigged");
    typedefsMenu.add(menuItem); // TRI
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(queueableTrigged);
        }
      });
    menuItem = new JMenuItem("queuable actor");
    typedefsMenu.add(menuItem); // ACT
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(queueableActor);
        }
      });
    menuItem = new JMenuItem("initially");
    tempMenu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(staticInitially);
        }
      });
    menuItem = new JMenuItem("finally");
    tempMenu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(staticFinally);
        }
      });

    JMenu bagsMenu = new JMenu("bags");
    tempMenu.add(bagsMenu);
    menuItem = new JMenuItem("boolean");
    bagsMenu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(booleanBag);
        }
      });
    menuItem = new JMenuItem("long");
    bagsMenu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(longBag);
        }
      });
    menuItem = new JMenuItem("double");
    bagsMenu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(doubleBag);
        }
      });
    menuItem = new JMenuItem("struct and queuable");
    bagsMenu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(structQueuableBag);
        }
      });

    JMenu algosMenu = new JMenu("algorithms");
    tempMenu.add(algosMenu);
    menuItem = new JMenuItem("once");
    algosMenu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(algoOnce);
        }
      });
    menuItem = new JMenuItem("debug");
    algosMenu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(algoDebug);
        }
      });
    menuItem = new JMenuItem("illustrate");
    algosMenu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(algoIllustration);
        }
      });
    menuItem = new JMenuItem("average");
    algosMenu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(algoAverage);
        }
      });
    menuItem = new JMenuItem("regression");
    algosMenu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(algoRegression);
        }
      });
    menuItem = new JMenuItem("bruteforcemax");
    algosMenu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(algoBruteforcemax);
        }
      });
  }

  private void addInsertTemplatesToDotEsim(JMenu menu, OneFileNotepad ofn)
  {
    JMenu tempMenu = new JMenu("insert templates");
    JMenuItem menuItem = new JMenuItem("repeating");
    tempMenu.add(menuItem);
    menu.add(tempMenu);
    final JMenu _menu = menu;
    final OneFileNotepad _ofn = ofn;
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(staticRepeating);
        }
      });
    menuItem = new JMenuItem("single");
    tempMenu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(staticSingle);
        }
      });
    menuItem = new JMenuItem("trigged");
    tempMenu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(staticTrigged);
        }
      });
    menuItem = new JMenuItem("actor");
    tempMenu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(staticActor);
        }
      });
    menuItem = new JMenuItem("globals");
    tempMenu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(staticGlobals);
        }
      });
    menuItem = new JMenuItem("procedure");
    tempMenu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(procedureDeclaration);
        }
      });  
    menuItem = new JMenuItem("subscope");
    tempMenu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(subscopeDeclaration);
        }
      });
    JMenu typedefsMenu = new JMenu("typedef");
    tempMenu.add(typedefsMenu);
    menuItem = new JMenuItem("struct");
    typedefsMenu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(typedefStruct);
        }
      });
    menuItem = new JMenuItem("queuable repeating");
    typedefsMenu.add(menuItem); // REP
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(queuableRepetating);
        }
      });
    menuItem = new JMenuItem("queuable trigged");
    typedefsMenu.add(menuItem); // TRI
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(queueableTrigged);
        }
      });
    menuItem = new JMenuItem("queuable actor");
    typedefsMenu.add(menuItem); // ACT
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(queueableActor);
        }
      });
    menuItem = new JMenuItem("initially");
    tempMenu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(staticInitially);
        }
      });
    JMenu bagsMenu = new JMenu("bags");
    tempMenu.add(bagsMenu);
    menuItem = new JMenuItem("boolean");
    bagsMenu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(booleanBag);
        }
      });
    menuItem = new JMenuItem("long");
    bagsMenu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(longBag);
        }
      });
    menuItem = new JMenuItem("double");
    bagsMenu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(doubleBag);
        }
      });
    menuItem = new JMenuItem("struct and queuable");
    bagsMenu.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          _ofn.insertAtCaretPosition(structQueuableBag);
        }
      });
  }

  public JMenu[] getFilesMenu(File f)
  {
    if(f == null) return null;
    JMenu menu = new JMenu(appname);
    if(f.getAbsolutePath().endsWith(".sim"))
    {
      addRunAndVerifyToDotSim(menu, f);
    }
    if(f.getAbsolutePath().endsWith(".esim"))
    {
      addVerifyToDotEsim(menu, f);
    }
    if(f.getAbsolutePath().endsWith(".tab"))
    {
      addVerifyToDotTab(menu, f);
    }
    if(f.getAbsolutePath().endsWith(".ilu"))
    {
      addVerifyToDotIlu(menu, f);
    }
    if(f.getAbsolutePath().endsWith(".qry"))
    {
      addParseQueryToDotQry(menu, f);
    }
    if(f.getAbsolutePath().endsWith(".edq"))
    {
      addParseQueryToDotEdq(menu, f);
    }
    if(f.getAbsolutePath().endsWith(".guiapp"))
    {
    	addRunAndVerifyToDotGuiapp(menu, f);
    }
    final File _f = f;
    if(menu.getItemCount() == 0) return null;
    return new JMenu[] { menu };
  }

  public JMenu[] getEditorsMenu(OneFileNotepad ofn)
  { 
    if(ofn == null) return null;
    File f = ofn.getFile();
    JMenu menu = new JMenu(appname);
    if(f.getAbsolutePath().endsWith(".sim"))
    {
      addRunAndVerifyToDotSim(menu, ofn);
      addInsertTemplatesToDotSim(menu, ofn);
    } 
    if(f.getAbsolutePath().endsWith(".esim"))
    {
      addVerifyToDotEsim(menu, ofn);
      addInsertTemplatesToDotEsim(menu, ofn);
    } 
    if(f.getAbsolutePath().endsWith(".tab"))
    {
      addVerifyToDotTab(menu, ofn);
    }
    if(f.getAbsolutePath().endsWith(".ilu"))
    {
      addVerifyToDotIlu(menu, ofn);
      addInsertTemplatesToDotIlu(menu, ofn);
    }
    if(f.getAbsolutePath().endsWith(".guiapp"))
    {
      addVerifyToDotGuiapp(menu, ofn);
      addInsertTemplatesToDotGuiapp(menu, ofn);
    }
    if(f.getAbsolutePath().endsWith(".qry"))
    {
      addParseQueryToDotQry(menu, ofn);
    }
    if(f.getAbsolutePath().endsWith(".edq"))
    {
      addParseQueryToDotEdq(menu, ofn);
    }
    if(menu.getItemCount() == 0) return null;
    return new JMenu[] { menu };
  }

  private void makeSubjacentExampleFile(File f)
    throws IntolerableException 
  {
    String inputValue = 
      JOptionPane.showInputDialog("Type files name: (.sim as postfix)");
    if(!(inputValue.endsWith(".sim"))) 
      inputValue = inputValue+".sim";
    String newFilesAbsolutePath = f.getAbsolutePath()+File.separatorChar+inputValue;
    
    try
    {
      File newFile = new File(newFilesAbsolutePath);
      FileWriter fw = new FileWriter(newFile);
      fw.write("settings\n");
      fw.write("{\n");
      fw.write("  name \"A textual name\";\n");
      fw.write("  timeUnit 60*1000; // The simulation unit in milliseconds.\n");
      fw.write("  starttime 2003/05/01 08:00:00;\n");
      fw.write("  endtime 2003/08/15 08:00:00;\n");
      fw.write("  algorithm once;\n");
      fw.write("}\n");
      fw.write("\n");
      fw.write("globals\n");
      fw.write("{\n");
      fw.write("  double var1 = 0.0 monitor;\n");
      fw.write("  long var2 = 0 monitor;\n");
      fw.write("  boolean var3 = false monitor;\n");
      fw.write("}\n");
      fw.write("\n");
      fw.write("single\n");
      fw.write("{\n");
      fw.write("  locals {}\n");
      fw.write("  pointInTime 2003/05/08 08:00:00;\n");
      fw.write("  pointInTime 2003/05/18 08:00:00;\n");
      fw.write("  execute\n");
      fw.write("  {\n");
      fw.write("    var1 = var1 + 1.1;\n");
      fw.write("    var2 = var2 + 1;\n");
      fw.write("    var3 = !var3;\n");
      fw.write("  }\n");
      fw.write("}\n");
      fw.write("\n");
      fw.write("repeating\n");
      fw.write("{\n");
      fw.write("  locals {}\n");
      fw.write("  first 0;\n");
      fw.write("  execute\n");
      fw.write("  {\n");
      fw.write("    var1 = var1 + (1.1 + var2);\n");
      fw.write("  }\n");
      fw.write("  again 1;\n");
      fw.write("  requeue true;\n");
      fw.write("}\n");
      fw.write("\n");
      fw.write("trigged\n");
      fw.write("{\n");
      fw.write("  locals {}\n");
      fw.write("  criterion var1 > 100;\n");
      fw.write("  execute\n");
      fw.write("  {\n");
      fw.write("    var1 = var1 - 50;\n");
      fw.write("  }\n");
      fw.write("  requeue time() < 60;\n");
      fw.write("}\n");
      fw.close();
    }
    catch(IOException ioe)
    {
      throw new SystemError("Error writing template .sim-file.", ioe);
/*
      JOptionPane.showMessageDialog(null,
                                    "Failed due to: "+ioe.getMessage(), 
                                    "Simulation failure", 
                                    JOptionPane.YES_OPTION);
 */
    }
  }

  public JMenu[] getFoldersMenu(File f)
  {
    JMenu menu = new JMenu(appname);
    JMenuItem menuItem = new JMenuItem("new template file");
    menu.add(menuItem);
    final File _f = f;
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          try
          {
            makeSubjacentExampleFile(_f);
          }
          catch(IntolerableException ie)
          {
            UniversalTellUser.general(null, ie);
          }
        }
      }
    );
    return new JMenu[] { menu };
  }

  public JMenu[] getMenubar()
  {
    final JMenu ssim = new JMenu(appname);
    JMenuItem menuItem = new JMenuItem("About "+appname);
    ssim.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          UniversalTellUser.info(
            getFrame(someComponent), 
            appname+" is a truely versatile simulator based on repetition of\n"+
            "simple arithmetic expressions. It is a script interpretor that executes\n"+
            "a proprietary language designed to be the best of the following desired\n"+
            "qualities:\n"+
            "\n"+
            "1. Being a model of some kind of rule based course of events.\n"+
            "2. Can be executed and realised and inspected thoroughly.\n"+
            "   The course of events is entirely white box.\n"+ 
            "3. Being a short notation of a huge amount of calculations that leaves\n"+ 
            "   the user in the sence of knowing what is going on. It is not so\n"+ 
            "   short that it becomes cryptic.\n"+ 
            "\n"+ 
            appname+" is entierly created by Kåre Jonsson and the full ownership belongs\n"+ 
            "to him. It is written in Java.\n"+ 
            "\n", "About "+appname);
        }
      }
    );
    /*
    menuItem = new JMenuItem("Disclaimer (Swedish)");
    ssim.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          if(!disclaimer_continue_swe()) System.exit(0);
        }
      }
    );
    menuItem = new JMenuItem("Disclaimer (English)");
    ssim.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          if(!disclaimer_continue_eng()) System.exit(0);
        }
      }
    );
    */
    menuItem = new JMenuItem("State dialog");
    ssim.add(menuItem);
    //final JMenu __menu = ssim;
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          try
          {
            UniversalTellUser.info(
              getFrame(someComponent), 
              "State is "+AppJoin.getAppJoin().getStateString());
          }
          catch(Exception _e)
          {
            UniversalTellUser.general(
              getFrame(someComponent), 
              new UnclassedError("Application - Main - State dialog", _e));
          }
        }
      }
    );
    menuItem = new JMenuItem("version");
    ssim.add(menuItem);
    menuItem.addActionListener(
      new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          info(getFrame(someComponent), 
               "This is version "+version+" of "+appname+".\n"+
               "It was compiled on "+compiledate+"\n", 
               "Version");
        }
      }
    );
    return new JMenu[] { ssim };
  }

}
