package se.modlab.simplesim.application;

import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.generics.gui.progress.*;
import se.modlab.simplesim.algorithms.share.*;

public class AppJoin implements MonitoredWorker
{ 

  private final static AppJoin instance = new AppJoin();
  private ProgressDumpAndMonitor _pmd = null;
  private String tasksMessage = "Tasks message";
  private String dumpMessage = null;
  private String barMessage = null;
  private boolean busy = false;
  private Algorithm algo = null;

  public static AppJoin getAppJoin()
  {
    return instance;
  } 
  
  private AppJoin()
  {
  }

  public ProgressDumpAndMonitor getProgressMonitor()
  {
    if(_pmd != null) return _pmd;
    _pmd = ProgressDumpAndMonitor.createGUI(this);
    return _pmd;
  }
/*
  public PKIManager getPKIManager()
  {
    if(pkiman != null) return pkiman;
    try
    {  
      pkiman = new PKIManager();
    }
    catch(Exception e)
    {
      return null;
    }
    return pkiman;
  }
*/
  public void setQuickMessage(String message, boolean busy)
  { 
    /*if(!busy)
    {*/
      this.dumpMessage = message;
      this.barMessage = message;
      this.busy = busy;/*
      return;
    }
    this.dumpMessage = null;
    this.barMessage = null;*/
  }

  public void runAlgorithm(AlgorithmRunner ar)
    throws Exception
  {
    ProgressDumpAndMonitor pmd = getProgressMonitor();
    if(busy) {
      MonitoredWorker mw = pmd.getWorker();
      if(!mw.isDone()) {
        UniversalTellUser.info(null, 
          "There is already an ongoing execution!", 
          "Busy message");
        return;
      } 
    }
    algo = ar.getAlgorithm();
    busy = true;
    Thread t = new Thread(ar);
    t.start();
    pmd.monitoredWorkStarted();
    //System.out.println("appJoin.runAlgorithm - t.join innan "+t.getName());
    //t.join();
    //algo = null;
    //busy = false;
    //System.out.println("appJoin.runAlgorithm - t.join efter "+t.getName());
  }

   
  public void runAlgorithmSynchronized(AlgorithmRunner ar, long waitMillis)
  throws Exception
  {
	  runAlgorithm(ar);
	  // When time allows. Investigate why the test for null is necessary. 
	  while(algo != null && !algo.isDone()) {
		  Thread.sleep(waitMillis);
	  }
  }
  
  public String getTasksMessage() 
  { 
    if(algo == null) return dumpMessage;
    String out = algo.getName();
    if(algo.isDone())
    {
      algo = null;
      busy = false;
    }
    return out; 
  }

  public String getMessageForBar() 
  { 
    if(algo == null) return barMessage;
    String out = algo.getSurveillanceMessageForBar();
    if(algo.isDone())
    {
      algo = null;
      busy = false;
    }
    return out; 
  }

  public String getMessageForDump() 
  { 
    if(algo == null) return dumpMessage;
    String out = algo.getSurveillanceMessageForDump();
    if(algo.isDone())
    {
      algo = null;
      busy = false;
    }
    return out; 
  }

  public boolean isDone() 
  { 
    if(algo == null) return !busy;
    if(algo.isDone())
    {
      algo = null;
      busy = false;
      return true;
    }
    return false;
  }

  public void stop() 
  {
    busy = false;
    if(algo != null) 
    {
      Algorithm a = algo;
      algo = null;
      if(a != null) a.stop();
    }
  }

  public int getCurrent() 
  {
    if(algo == null) return 0;
    int out = algo.getCurrent();
    if(algo.isDone())
    {
      algo = null;
      busy = false;
    }
    return out;
  }

  public int getLengthOfTask()  
  {
    if(algo == null) return 1;
    int out = algo.getLengthOfTask();
    if(algo.isDone())
    {
      algo = null;
      busy = false;
    }
    return out;
  }

  public String getStateString()
  {
    ProgressDumpAndMonitor pmd = getProgressMonitor();
    String out = null;
    if(busy)
    {
      MonitoredWorker mw = pmd.getWorker();
      if(!mw.isDone())
      {
        out = "Busy: "+getTasksMessage();
      }
    }
    if(out == null) out = "Idle";
    if((algo != null) && (algo.isDone()))
    {
      algo = null;
      busy = false;
      out = "Idle";
    }
    return out;
  }
 
  public String getName()
  {
    return "Simplesim"; 
  }

}
