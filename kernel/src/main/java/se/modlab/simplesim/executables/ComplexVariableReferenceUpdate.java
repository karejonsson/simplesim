package se.modlab.simplesim.executables;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.evaluables.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.references.*;
import se.modlab.simplesim.events.*;
import se.modlab.simplesim.bags.*;

public class ComplexVariableReferenceUpdate 
extends VarUpdate   
{

	private ReferenceEvaluable re;

	public ComplexVariableReferenceUpdate(VariableLookup _vl, 
			ReferenceEvaluable _re,
			String filename, int line, int column)
	{
		super(_vl, filename, line, column);
		re = _re;
	}

	public void execute(Scope s) 
	throws ReturnException, 
	IntolerableException, 
	StopException,
	BreakException,
	ContinueException
	{
		VariableInstance vi = vl.getInstance(s);
		if(vi == null)
		{
			throw new UserRuntimeError(
					"Variable "+vl+", updated at "+this.getPlace()+" not in scope. "+s);
		}
		sValue val = re.evaluate(s);
		if(val == null)
		{
			throw new UserRuntimeError(
					"Expression "+re+" not evaluable. Supposed to update variable "+vl+" defined "+vi.getPlace());
		}
		if(val instanceof sQueuable)
		{
			Enqueuable eq = ((sQueuable) val).getQueuable();
			try
			{
				vi.setValue((VariableInstance) eq);
			} 
			catch(IntolerableException ie)
			{
				//System.out.println("complexVariableReferenceUpdate - "+vl.getClass().getName());
				throw new UserRuntimeError(
						"The variable "+vl+", defined "+vi.getPlace()+" does not accept the assignment.");
			}
			return;
		}
		if(val instanceof sStruct)
		{
			Complex c = ((sStruct) val).getStruct();
			try
			{
				vi.setValue((VariableInstance) c);
			}
			catch(IntolerableException ie)
			{  
				throw new UserRuntimeError(
						"The variable "+vl+" defined "+vi.getPlace()+" does not accept the assignment.");
			}
			return;
		}
		if(vi instanceof Variable)
		{
			((Variable) vi).setValue(val);
			return;
		}
		//vi.setValue(val);
		throw new InternalError(
		"Obsolete logic in class complexVariableReferenceUpdate\n"+
		"Class vi: "+vi.getClass().getName()+"\n"+
		"Class val: "+val.getClass().getName());
	}

	public void verify(Scope s) throws IntolerableException {
		VariableInstance vi = vl.getInstance(s);
		if(vi == null)
		{
			throw new UserCompiletimeError(
					"Variable "+vl+", updated at "+this.getPlace()+" not in scope. "+s);
		}
		re.verify(s);
		vi.setDefaultInitialValue();
	}

}

