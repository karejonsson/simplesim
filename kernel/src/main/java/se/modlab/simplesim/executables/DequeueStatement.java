package se.modlab.simplesim.executables;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.evaluables.*;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.variables.*;
import se.modlab.simplesim.references.*;
import se.modlab.simplesim.events.*;

public class DequeueStatement extends ProgramStatement
{
	private int beginLine;
	private int beginColumn;
	private ReferenceEvaluable re;
	private Commons com;
	private SimulationEventQueue eq;
	private ScopeFactory sf;
	private String filename = null;

	public DequeueStatement(String _filename,
			int _beginLine,
			int _beingColumn,
			ReferenceEvaluable _re, 
			Commons _com,
			SimulationEventQueue _eq,
			ScopeFactory _sf)
	{
		filename = _filename;
		beginLine = _beginLine;
		beginColumn = _beingColumn;
		re = _re;
		com = _com;
		eq = _eq;
		sf = _sf;
	}

	public void execute(Scope s) 
	throws ReturnException, 
	IntolerableException, 
	StopException,
	BreakException,
	ContinueException
	{
		sValue val = re.evaluate(s);
		if(val == null)
		{
			throw new UserCompiletimeError(re.toString()+" does not exist.\n"+
					"It was referred in the dequeue statement on line "+beginLine+
					"\ncolumn "+beginColumn+" in the file "+filename+".");
		}
		if(!(val instanceof sQueuable))
		{
			throw new UserCompiletimeError(re.toString()+" is not queuable. Type is "+
					val.toString()+"\n"+
					"It was referred in the dequeue statement on line "+beginLine+
					"\ncolumn "+beginColumn+" in the file "+filename+".");
		}
		sQueuable sq = (sQueuable) val;
		final Enqueuable qi = (Enqueuable) sq.getQueuable();
		try {
			qi.queueDown(eq);
		}
		catch(UserRuntimeError ue) {
			String m = ue.getMessage()+"\n"+"This happened in file "+filename+", line "+beginLine+", column "+beginColumn;
			UserRuntimeError ue2 = new UserRuntimeError(m, ue.getCause(), ue.getCollectors());
			ue2.setAction(ue.getAction());
			throw ue2;
		}
		catch(UserCompiletimeError ue) {
			String m = ue.getMessage()+"\n"+"This happened in file "+filename+", line "+beginLine+", column "+beginColumn;
			UserCompiletimeError ue2 = new UserCompiletimeError(m, ue.getCause(), ue.getCollectors());
			ue2.setAction(ue.getAction());
			throw ue2;
		}

	}

	public void verify(Scope s) throws IntolerableException {
		re.verify(s);
	}

}
