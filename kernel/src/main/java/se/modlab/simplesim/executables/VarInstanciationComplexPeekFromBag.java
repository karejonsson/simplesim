package se.modlab.simplesim.executables;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.evaluables.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.simplesim.bags.*;
import se.modlab.simplesim.references.*;
import se.modlab.simplesim.events.*;
import se.modlab.generics.exceptions.*;

public class VarInstanciationComplexPeekFromBag extends VarInstanciation
{

	private VariableLookup vl;
	private String typesname;
	private ArithmeticEvaluable ae;

	public VarInstanciationComplexPeekFromBag(String varName, 
			String _filename, int _line, int _column,
			String _typesname,
			VariableLookup _vl,
			ArithmeticEvaluable _ae)
	{
		super(varName, _filename, _line, _column);
		typesname = _typesname;
		vl = _vl;
		ae  =_ae;
	}

	public void execute(Scope s) 
	throws ReturnException, 
	IntolerableException, 
	StopException,
	BreakException,
	ContinueException
	{
		VariableFactory vf = s.getFactory(typesname);
		VariableInstance vi = vf.getInstance(varName, filename, line, column);
		ComplexBag b = (ComplexBag) vl.getInstance(s);
		sValue val = null;
		if(ae == null) {
			val = b.peek();
		}
		else {
			sValue idx = ae.evaluate(s);
			if(!(idx instanceof sLong)) {
				throw new UserCompiletimeError(
						"Expression "+ae+", evaluated at "+this.getPlace()+" evaluates to "+idx+" which is not long. "+s);
			}
			sLong lidx = (sLong) idx;
			val = b.peek((int)lidx.getLong().longValue());
		}
		if(val instanceof sQueuable)
		{
			Enqueuable eq = ((sQueuable) val).getQueuable();
			vi.setInitialValue((VariableInstance) eq);
			s.addComplexInstance(vi);
			return;
		}
		if(val instanceof sStruct)
		{
			Complex c = ((sStruct) val).getStruct();
			vi.setInitialValue((VariableInstance) c);
			s.addComplexInstance(vi);
			return;
		}
	}

	public void verify(Scope s) throws IntolerableException {
		VariableFactory vf = s.getFactory(typesname);
		VariableInstance vi = vf.getInstance(varName, filename, line, column);
		ComplexBag b = (ComplexBag) vl.getInstance(s);

		if(vi == null)
		{
			throw new UserCompiletimeError(
					"Variable "+vl+", updated at "+this.getPlace()+" not in scope. "+s);
		}
		if(b == null)
		{
			throw new UserCompiletimeError(
					"Bag "+b+", updated at "+this.getPlace()+" not in scope. "+s);
		}
		vi.setDefaultInitialValue();
		s.addComplexInstance(vi);
	}

}
