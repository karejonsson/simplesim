package se.modlab.simplesim.executables;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.evaluables.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.references.*;

public class VarInstanciationTypedQueuableReference extends VarInstanciation   
{

	private String type;
	private ReferenceEvaluable re;

	public VarInstanciationTypedQueuableReference(String varName, 
			String _filename, int _line, int _column,
			String _type,
			ReferenceEvaluable _re) {
		super(varName, _filename, _line, _column);
		type = _type;
		re = _re;
	}

	public void execute(Scope s) throws ReturnException, IntolerableException, StopException, BreakException, ContinueException {
		VariableFactory vf = s.getFactory(type);
		TypedQueuableReference qr = new TypedQueuableReference(varName, vf, filename, line, column);
		if(re == null) {
			qr.setDefaultInitialValue();
		}
		else {
			sValue sv = re.evaluate(s);
			qr.setInitialValue(sv);
		}
		s.addComplexInstance(qr);
	}

	public void verify(Scope s) throws IntolerableException {
		VariableFactory vf = s.getFactory(type);
		TypedQueuableReference qr = new TypedQueuableReference(varName, vf, filename, line, column);
		if(re != null) {
			re.verify(s);
		}
		qr.setDefaultInitialValue();
		s.addComplexInstance(qr);
	}

}
