package se.modlab.simplesim.executables;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.evaluables.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.variables.*;
import se.modlab.simplesim.references.*;
import se.modlab.simplesim.scoping.*;

public class VarInstanciationUntypedQueuableReferenceVector 
extends VarInstanciation
{

	private ArithmeticEvaluable ae;

	public VarInstanciationUntypedQueuableReferenceVector(String varName, 
			String _filename, int _line, int _column,
			ArithmeticEvaluable _ae)
	{
		super(varName, _filename, _line, _column);
		ae = _ae;
	}

	public void execute(Scope s) 
	throws ReturnException, 
	IntolerableException, 
	StopException,
	BreakException,
	ContinueException
	{
		VariableFactory vf = s.getFactory(UntypedQueuableReferenceFactory.typesname);
		Long length = ae.evaluate(s).getLong();
		if(length == null)
		{
			throw new UserRuntimeError(
					"The length of the vector "+varName+" does not evaluate\n"+
					"to an integer value.\n"+
					"Defined at "+getPlace()+".");
		}

		UntypedQueuableReferenceFactory rf = 
			new UntypedQueuableReferenceFactory(
					(SimScope) s);

		ReferenceVariableVector rvv = 
			new ReferenceVariableVector(
					varName,
					vf, vf, filename, line, column,
					length.intValue(),
					rf);
		rvv.setDefaultInitialValue();
		s.addComplexInstance(rvv);
	}

	public void verify(Scope s) throws IntolerableException {
		VariableFactory vf = s.getFactory(UntypedQueuableReferenceFactory.typesname);
		Long length = ae.evaluate(s).getLong();
		if(length == null)
		{
			throw new UserRuntimeError(
					"The length of the vector "+varName+" does not evaluate\n"+
					"to an integer value.\n"+
					"Defined at "+getPlace()+".");
		}

		UntypedQueuableReferenceFactory rf = 
			new UntypedQueuableReferenceFactory(
					(SimScope) s);

		ReferenceVariableVector rvv = 
			new ReferenceVariableVector(
					varName,
					vf, vf, filename, line, column,
					length.intValue(),
					rf);
		rvv.setDefaultInitialValue();
		s.addComplexInstance(rvv);
	}

}
