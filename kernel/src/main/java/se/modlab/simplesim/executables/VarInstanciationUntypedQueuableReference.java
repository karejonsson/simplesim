package se.modlab.simplesim.executables;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.evaluables.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.references.*;

public class VarInstanciationUntypedQueuableReference 
extends VarInstanciation   
{

	private ReferenceEvaluable re;

	public VarInstanciationUntypedQueuableReference(String varName, 
			String _filename, int _line, int _column,
			ReferenceEvaluable _re)
	{
		super(varName, _filename, _line, _column);
		re = _re;
	}

	public void execute(Scope s) throws ReturnException, IntolerableException, StopException, BreakException, ContinueException {
		VariableFactory vf = s.getFactory(UntypedQueuableReferenceFactory.typesname);
		UntypedQueuableReference qr = new UntypedQueuableReference(varName, vf, filename, line, column);
		if(re == null) {
			qr.setDefaultInitialValue();
		}
		else {
			sValue rv = re.evaluate(s);
			qr.setInitialValue(rv);
		}
		s.addComplexInstance(qr);
	}

	public void verify(Scope s) throws IntolerableException {
		VariableFactory vf = s.getFactory(UntypedQueuableReferenceFactory.typesname);
		UntypedQueuableReference qr = new UntypedQueuableReference(varName, vf, filename, line, column);
		if(re != null)
		{
			re.verify(s);
		}
		qr.setDefaultInitialValue();
		s.addComplexInstance(qr);
	}

}
