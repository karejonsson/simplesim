package se.modlab.simplesim.executables;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.evaluables.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.references.*;

public class VarInstanciationTypedStructReference 
extends VarInstanciation   
{

	private String type;
	private ReferenceEvaluable re;

	public VarInstanciationTypedStructReference(String varName, 
			String _filename, int _line, int _column,
			String _type,
			ReferenceEvaluable _re)
	{
		super(varName, _filename, _line, _column);
		type = _type;
		re = _re;
	}

	public void execute(Scope s) 
	throws ReturnException, 
	IntolerableException, 
	StopException,
	BreakException,
	ContinueException
	{
		VariableFactory vf = s.getFactory(type);
		TypedStructReference sr = new TypedStructReference(varName, vf, filename, line, column);
		if(re == null)
		{
			sr.setDefaultInitialValue();
		}
		else
		{
			sValue sv = re.evaluate(s);
			sr.setInitialValue(sv);
		}
		s.addComplexInstance(sr);
	}

	public void verify(Scope s) throws IntolerableException {
		VariableFactory vf = s.getFactory(type);
		TypedStructReference sr = new TypedStructReference(varName, vf, filename, line, column);
		if(re != null)
		{
			re.verify(s);
		}
		sr.setDefaultInitialValue();
		s.addComplexInstance(sr);
	}

}
