package se.modlab.simplesim.executables;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.evaluables.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.references.*;
import se.modlab.simplesim.bags.*;

public class ComplexVariableUpdateGetFromBag 
extends VarUpdate   
{

	private VariableLookup cb;
	private ArithmeticEvaluable ae;

	public ComplexVariableUpdateGetFromBag(VariableLookup _vl, 
			VariableLookup _cb,
			ArithmeticEvaluable _ae,
			String filename, int line, int column)
	{
		super(_vl, filename, line, column);
		cb = _cb;
		ae = _ae;
	}

	public void execute(Scope s) 
	throws ReturnException, 
	IntolerableException, 
	StopException,
	BreakException,
	ContinueException
	{
		VariableInstance vi = vl.getInstance(s);
		ComplexBag actualBag = (ComplexBag) cb.getInstance(s);
		//sValue v = actualBag.get();
		sValue v = null;
		if(ae == null) {
			v = actualBag.get();
		}
		else {
			sValue idx = ae.evaluate(s);
			if(!(idx instanceof sLong)) {
				throw new UserCompiletimeError(
						"Expression "+ae+", evaluated at "+this.getPlace()+" evaluates to "+idx+" which is not long. "+s);
			}
			sLong lidx = (sLong) idx;
			v = actualBag.get((int)lidx.getLong().longValue());
		}
		if(v instanceof sQueuable)
		{
			vi.setValue((VariableInstance) ((sQueuable) v).getQueuable());
			return;
		}
		if(v instanceof sStruct)
		{
			vi.setValue((VariableInstance) ((sStruct) v).getStruct());
			return;
		}
		throw new InternalError(
				"Logic in class complexVariableUpdateGetFromBag outdated.\n"+
				"Class name was "+v.getClass().getName());
	}

	public void verify(Scope s) throws IntolerableException {
		VariableInstance vi = vl.getInstance(s);
		if(vi == null)
		{
			throw new UserCompiletimeError(
					"Variable "+vl+", updated at "+this.getPlace()+" not in scope. "+s);
		}
		ComplexBag actualBag = (ComplexBag) cb.getInstance(s);
		if(actualBag == null)
		{
			throw new UserCompiletimeError(
					"Bag "+actualBag+", updated at "+this.getPlace()+" not in scope. "+s);
		}
		vi.setDefaultInitialValue();
	}

}