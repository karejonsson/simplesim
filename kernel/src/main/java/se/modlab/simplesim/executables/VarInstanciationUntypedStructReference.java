package se.modlab.simplesim.executables;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.evaluables.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.references.*;

public class VarInstanciationUntypedStructReference 
extends VarInstanciation   
{

	private ReferenceEvaluable re;

	public VarInstanciationUntypedStructReference(String varName, 
			String _filename, int _line, int _column,
			ReferenceEvaluable _re)
	{
		super(varName, _filename, _line, _column);
		re = _re;
	}

	public void execute(Scope s) 
	throws ReturnException, 
	IntolerableException, 
	StopException,
	BreakException,
	ContinueException
	{
		VariableFactory vf = s.getFactory("struct");
		UntypedStructReference sr = new UntypedStructReference(varName, vf, filename, line, column);
		if(re == null)
		{
			sr.setDefaultInitialValue();
		}
		else
		{
			sValue sv = re.evaluate(s);
			//System.out.println("varInstanciationUntypedStructReference - "+sv);
			sr.setInitialValue(sv);
		}
		s.addComplexInstance(sr);
	}

	public void verify(Scope s) throws IntolerableException {
		VariableFactory vf = s.getFactory("struct");
		UntypedStructReference sr = new UntypedStructReference(varName, vf, filename, line, column);
		if(re != null)
		{
			re.verify(s);
		}
		sr.setDefaultInitialValue();
		s.addComplexInstance(sr);
	}

}
