package se.modlab.simplesim.executables;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.evaluables.*;
import se.modlab.generics.sstruct.logics.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.variables.*;
import se.modlab.simplesim.events.*;
import se.modlab.simplesim.references.*;

public class SendStatement extends ProgramStatement
{
	private int line;
	private int column;
	private VariableLookup vl;
	private String signal; 
	private ArithmeticEvaluable delay;
	private ReferenceEvaluable message;
	private Commons com;
	private SimulationEventQueue eq;
	private ScopeFactory sf;
	private String filename = null;

	public SendStatement(String _filename,
			int _line,
			int _column,
			VariableLookup _vl, 
			String _signal, 
			ArithmeticEvaluable _delay,
			ReferenceEvaluable _message,
			Commons _com,
			SimulationEventQueue _eq,
			ScopeFactory _sf)
	{
		filename = _filename;
		line = _line;
		column = _column;
		vl = _vl;
		signal = _signal;
		delay = _delay;
		message = _message;
		com = _com;
		eq = _eq;
		sf = _sf;
	}

	public void execute(Scope s) 
	throws ReturnException, 
	IntolerableException, 
	StopException,
	BreakException,
	ContinueException
	{
		VariableInstance vi = vl.getInstance(s);
		if(vi == null)
		{
			throw new UserRuntimeError(vl.toString()+" does not exist.");
		}
		ProcedureActor _pa = null;
		if(vi instanceof ProcedureActor)
		{
			_pa = (ProcedureActor) vi;
		}
		if(vi instanceof QueuableReference)
		{
			sQueuable q = (sQueuable) ((QueuableReference) vi).getValue();
			if(q == null)
			{
				throw new UserRuntimeError(
						vl.toString()+" was null worth.\n"+
						"This happened in the send statement in file "+filename+", on line "+line+"\n"+
						"column "+column+" in file "+filename);
			}
			if(q.getQueuable() instanceof ProcedureActor)
			{
				_pa = (ProcedureActor)q.getQueuable();
			}
		}
		if(_pa == null)
		{
			String type = "unknown";
			try { type = vi.getType().getTypesName(); } catch(Exception e) {}
			throw new UserRuntimeError(
					vl.toString()+" is no actor. Type is "+
					type+".\n"+
					"This happened in the send statement in file "+filename+", on line "+line+"\n"+
					"column "+column+" in file "+filename);
		}
		final ProcedureActor pa = _pa;
		VariableInstance sender_tmp = s.getComplexInstance("this");
		if(sender_tmp == null)
		{
			VariableFactory vf = s.getFactory("queuable");
			sender_tmp = new UntypedQueuableReference("sender", vf, filename, line, column);
			sender_tmp.setDefaultInitialValue();
			//sender_tmp = new dummySender();
		}
		final VariableInstance sender = sender_tmp;
		String _plausibleErrorMessage = null;
		if(sender instanceof ProcedureActor)
		{
			ProcedureActor sender_pa = (ProcedureActor) sender;
			String id = sender_pa.getFullPathName();
			_plausibleErrorMessage = 
				"Sender of this signal was "+id+". It was sent at simulation\n"+
				"time "+com.getCurrentSimTime()+" from the send statement in file "+filename+", on "+
				"line "+line+", column "+column+" in file "+filename+" in the state\n"+
				sender_pa.getCurrentStateName()+". The reciever is actor\n"+
				pa.getFullPathName()+" which at the time of the send was\n"+
				"in state <"+pa.getCurrentStateName()+">";
		}
		else
		{
			String id = sender.toString();
			_plausibleErrorMessage = 
				"Sender of this signal was "+id+". It was sent at simulation\n"+
				"time "+com.getCurrentSimTime()+" from the send statement in file "+filename+", on "+
				"line "+line+", column "+column+" in file "+filename+". The reciever is actor\n"+
				pa.getFullPathName()+" which at the time of the send was "+
				"in state <"+pa.getCurrentStateName()+">";
		}
		VariableFactory vf = s.getFactory("struct");
		StructReference actualMessage = new UntypedStructReference("message", vf, filename, line, column);
		if(message == null)
		{
			// No message supplied.
			actualMessage.setDefaultInitialValue();
		}
		else
		{
			// Message supplied
			sValue val = message.evaluate(s);
			if(!(val instanceof sStruct))
			{
				throw new UserRuntimeError(
						"The message sent was not of type struct. It was "+val+".n"+
						_plausibleErrorMessage);
			}
			if(val instanceof sQueuable)
			{
				throw new UserRuntimeError(
						"The message sent was of type queuable. It was "+val+".n"+
						"The contract is that it shall be a struct.\n"+
						_plausibleErrorMessage);
			}
			actualMessage.setInitialValue(val);
		}
		final StructReference sr = actualMessage;
		if(delay != null)
		{
			sValue val = delay.evaluate(s);
			final double d = val.getValue();
			final String plausibleErrorMessage = _plausibleErrorMessage+
			"\nSignal was delayed with "+d+" time units.";
			eq.queueUpLate(new RunnableQueuer(com.getCurrentSimTime()+d, 
					new ExceptionRunnable()
			{
				public void run()
				throws IntolerableException,
				StopException
				{
					pa.signal(signal, sender, sr, plausibleErrorMessage);
				}
				public VariableInstance getActor()
				{
					return pa;
				}
			}, "Signal waiter: "+pa.getFullPathName()+" "+signal));
			return;
		}
		final String plausibleErrorMessage = _plausibleErrorMessage+
		"\nSignal queued up without delay.";
		eq.queueUpLate(new RunnableQueuer(com.getCurrentSimTime(), 
				new ExceptionRunnable()
		{
			public void run()
			throws IntolerableException,
			StopException
			{
				pa.signal(signal, sender, 
						sr, plausibleErrorMessage);
			}
			public VariableInstance getActor()
			{
				return pa;
			}
		}, "Signal waiter: "+pa.getFullPathName()+" "+signal));
	}

	private class dummyType implements VariableType
	{
		public String getTypesName()
		{
			return "Dummy type";
		}
	}

	private class dummySender extends VariableInstance
	{
		public dummySender()
		{
			super("Dummy sender", new dummyType(), SendStatement.this.filename, SendStatement.this.line , SendStatement.this.column);
		}
		public void setDefaultInitialValue()
		{
		}
		public void setInitialValue(VariableInstance _val) 
		throws IntolerableException
		{
			throw new InternalError(
					"sendStatement.dummySender.setInitialValue(variableInstance)\n"+
					"called with object of class "+_val.getClass().getName()+".\n"+
			"This method should not be called at all!");
		}

		public void setValue(VariableInstance vi)
		{
		}
		public String toString()
		{
			return "Dummy sender";
		}
		@Override
		public boolean isNull() {
			// TODO Auto-generated method stub
			return false;
		}
		public boolean valueEquals(VariableInstance sv) throws IntolerableException {
			throw new InternalError("Compare value equals on dummySender in send statement");
		}

	}

	public void verify(Scope s) throws IntolerableException {
		VariableInstance vi = vl.getInstance(s);
		if(vi == null)
		{
			throw new UserCompiletimeError(vl.toString()+" does not exist.");
		}
		VariableFactory vf = s.getFactory("struct");
		StructReference actualMessage = new UntypedStructReference("message", vf, filename, line, column);
		if(message != null)
		{
			// No message supplied.
			actualMessage.setDefaultInitialValue();
		}
		actualMessage.setDefaultInitialValue();
		if(delay != null)
		{
			delay.verify(s);
		}
	}

}

