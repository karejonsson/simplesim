package se.modlab.simplesim.test;

import java.io.File;

import se.modlab.generics.bshro.ifc.HierarchyObject;
import se.modlab.generics.exceptions.IntolerableException;
import se.modlab.generics.exceptions.MotherCatalogue;
import se.modlab.generics.exceptions.UnclassedError;
import se.modlab.generics.gui.exceptions.UniversalTellUser;
import se.modlab.simplesim.algorithms.average.AverageAlgorithm;
import se.modlab.simplesim.algorithms.once.OnceAlgorithm;
import se.modlab.simplesim.algorithms.share.AlgorithmRunner;
import se.modlab.simplesim.application.AppJoin;
import se.modlab.simplesim.parse.SimParser;
import se.modlab.simplesim.scoping.SimScopeFactory;

public class RunOneScriptProgrammatically {

	public static void runOnce(final File f) {

		OnceAlgorithm algo = null;
		AlgorithmRunner ar = null;
		try {
			HierarchyObject.setReferenceFile(f.getParentFile());
			algo = (OnceAlgorithm) SimParser.parseFromFile(f.getName(), new SimScopeFactory(), false);
			ar = new AlgorithmRunner(algo, null, "Executing from treeview");
			AppJoin.getAppJoin().runAlgorithm(ar);
			//appJoin.getAppJoin().displayResultsGraphically(ar);
		}
		catch(IntolerableException ie)
		{ 
			if(algo != null) ie.setCollectors(algo.getCollectors());
			ie.setAction("Executed from treeview");
			UniversalTellUser.general(null, ie);
		}
		catch(Exception _e)
		{
			IntolerableException ie = 
					new UnclassedError("SimFileMenyFactory.addRunAndVerifyToDotSim f - run - ex",
							_e);
			if(algo != null) ie.setCollectors(algo.getCollectors());
			ie.setAction("Executed from treeview");
			UniversalTellUser.general(null, ie);
		}
		catch(Throwable _e)
		{
			IntolerableException ie = 
					new UnclassedError("SimFileMenyFactory.addRunAndVerifyToDotSim f - run - th",
							_e);
			if(algo != null) ie.setCollectors(algo.getCollectors());
			ie.setAction("Executed from treeview");
			UniversalTellUser.general(null, ie);
		}
		finally {
			//algo.tearDown();
		}

	}
	
	public static void main(String args[]) {
		
		String file = "/Users/karejonsson/tmp/modlab/files/bagtest_index/3.sim";
		runOnce(new File(file));
		
	}

}
