package se.modlab.simplesim.references;

import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.events.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.comparisons.*;

public abstract class StructReference extends Variable
                                      implements Complex
{

  //protected queuableReference ref = null;
  private String fullPathName;

  public StructReference(String name, VariableType vt, String filename, int line, int column)
  {
    super(name, vt, filename, line, column);
    fullPathName = name;
  }

  public void setDefaultInitialValue()
    throws IntolerableException
  {
    val = new sStruct();
  }

  public abstract void setValue(VariableInstance si)
    throws IntolerableException;

  public abstract sValue getValue()
    throws IntolerableException;

  public boolean isNull()
  {
    return ((sStruct)val).getStruct() == null;
  }
 
  public String getFullPathName()
  {
    return fullPathName;
  }
 
  public void reset()
    throws IntolerableException
  {
    throw new InternalError(
      "Method structReference.reset was called!");
  }

  public void setFullPathName(String fpn) 
    throws IntolerableException
  {
    fullPathName = fpn;
  }

  public Scope getLocalScope()
  {
    return ((sStruct)val).getQueuable().getLocalScope();
  }

  public void addMember(VariableInstance si)
    throws IntolerableException
  {
    ((sStruct)val).getStruct().addMember(si);
  }

  public int getSize()
  {
    return ((sStruct)val).getStruct().getSize();
  }

  public VariableInstance getMember(int i)
    throws IntolerableException
  {
    return ((sStruct)val).getStruct().getMember(i);
  }

  public VariableInstance getMember(String name)
    throws IntolerableException
  {
    if(val == null)
    {
      throw new InternalError(
        "Struct reference variable "+getFullPathName()+", defined "+this.getPlace()+" was\n"+
        "dereferenced when null worth!"); 
    }
    Complex c = ((sStruct)val).getStruct();
    if(c == null)
    {
      throw new UserRuntimeError(
        "Struct reference variable "+getFullPathName()+", defined "+this.getPlace()+" was\n"+
        "dereferenced when null worth!"); 
    }
    return ((sStruct)val).getStruct().getMember(name);
  }

}
