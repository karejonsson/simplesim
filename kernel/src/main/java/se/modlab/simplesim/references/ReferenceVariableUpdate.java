package se.modlab.simplesim.references;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.evaluables.*;
import se.modlab.generics.exceptions.*;

public class ReferenceVariableUpdate extends VarUpdate
{

	private ReferenceEvaluable re = null;

	public ReferenceVariableUpdate(VariableLookup vl,
			ReferenceEvaluable _re,
			String filename, int line, int column)
	{
		super(vl, filename, line, column);
		re = _re;
	}

	public void execute(Scope s) 
	throws ReturnException, 
	IntolerableException, 
	StopException,
	BreakException,
	ContinueException
	{
		Variable var = vl.getVariable(s);
		if(var == null)
		{
			throw new UserCompiletimeError(
					"Variable "+vl+", defined "+this.getPlace()+" not in scope. "+s);
		}
		sValue val = re.evaluate(s);
		var.setValue(val);
	}

	public void verify(Scope s) throws IntolerableException {
		Variable var = vl.getVariable(s);
		if(var == null)
		{
			throw new UserCompiletimeError(
					"Variable "+vl+", defined "+this.getPlace()+" not in scope. "+s);
		}
		re.verify(s);
		var.setDefaultInitialValue();
	}

}
