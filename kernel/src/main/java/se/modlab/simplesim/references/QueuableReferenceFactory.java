package se.modlab.simplesim.references;

import java.util.Hashtable;

import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.variables.*;
import se.modlab.simplesim.scoping.*;
import se.modlab.simplesim.references.*;
import se.modlab.simplesim.events.*;

public class QueuableReferenceFactory implements SimVariableFactory, EnqueuableFactory
{
	protected SimScope s; 
	protected String typesname; 
	protected VariableFactory unitsFactory;

	public QueuableReferenceFactory(SimScope _s, 
			String _typesname)
	{
		super();
		s = _s;
		typesname = _typesname;
		unitsFactory = s.getFactory(typesname);
	}

	public VariableInstance getInstance(String name, String filename, int line, int column, Hashtable<VariableInstance, Manager> managermap) throws IntolerableException {
		return getInstance(name, false, filename, line, column, managermap);
	}

	public VariableInstance getInstance(String name, boolean monitor, String filename, int line, int column, Hashtable<VariableInstance, Manager> managermap) throws IntolerableException {
		UntypedQueuableReference out = 
				new UntypedQueuableReference(name, unitsFactory, filename, line, column);
		return out;
	}

	public VariableInstance getInstance(String name, String filename, int line, int column) throws IntolerableException {
		System.out.println("Call to QueuableReferenceFactory.getInstance(s,s,i,i) i.e. addin null in class "+getClass().getName());
		return getInstance(name, false, filename, line, column, null);
	}

	public VariableInstance getInstance(String name, boolean monitor, String filename, int line, int column) throws IntolerableException {
		System.out.println("Call to QueuableReferenceFactory.getInstance(s,b,s,i,i) i.e. addin null in class "+getClass().getName());
		return getInstance(name, monitor, filename, line, column, null);
	}

	public String getTypesName() {
		return typesname;
	}

}