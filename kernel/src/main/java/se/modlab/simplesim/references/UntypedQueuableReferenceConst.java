package se.modlab.simplesim.references;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.exceptions.*;

public class UntypedQueuableReferenceConst 
       extends UntypedQueuableReference implements VariableConst
{
  
  public UntypedQueuableReferenceConst(String name, VariableType vt, String _filename, int _line, int _column)
  {
    super(name, vt, _filename, _line, _column);
  }

  public UntypedQueuableReferenceConst(UntypedQueuableReference uqr)
    throws IntolerableException
  {
    super(uqr.getName(), uqr.getType(), uqr.getFilename(), uqr.getLine(), uqr.getColumn());
    super.setValue(uqr.getValue());
  }

  public void setValue(sValue val) 
    throws IntolerableException
  {
    throw new UserRuntimeError(
      "Untyped queuable reference constant "+name+", defined "+this.getPlace()+" set to new value "+val);
  }

  public void setValue(VariableInstance si)
    throws IntolerableException
  {
    throw new UserRuntimeError(
      "Untyped queuable reference constant "+name+", defined "+this.getPlace()+" set to new value "+si);
  }

  public String toString()
  {
    return "Untyped queuable reference constant, "+super.toString();
  }


}
