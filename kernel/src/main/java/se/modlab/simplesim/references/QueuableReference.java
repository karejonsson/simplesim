package se.modlab.simplesim.references;

import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.events.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.comparisons.*;

public abstract class QueuableReference extends Variable
                                        implements Enqueuable
{

  //protected queuableReference ref = null;
  private String fullPathName;

  public QueuableReference(String name, VariableType vt, String _filename, int _line, int _column)
  {
    super(name, vt, _filename, _line, _column);
    fullPathName = name;
  }

  public void setDefaultInitialValue()
    throws IntolerableException
  {
    val = new sQueuable();
  }

  public abstract void setValue(VariableInstance si)
    throws IntolerableException;

  public abstract sValue getValue()
    throws IntolerableException;

  public boolean isNull()
  {
    return ((sQueuable)val).getQueuable() == null;
  }
 
  public boolean isEnqueued()
    throws IntolerableException
  {
    if(isNull())
    {
      throw new UserRuntimeError(
        "isNull operation on null reference "+name+", defined "+this.getPlace());
    }
    return ((sQueuable)val).getQueuable().isEnqueued();
  }
 
  public String getFullPathName()
  {
    return fullPathName;
  }
 
  public void queueUp(SimulationEventQueue eq)
    throws IntolerableException, 
           StopException
  {
    if(isNull())
    {
      throw new UserRuntimeError(
        "Enqueue operation on null reference "+name+", defined "+this.getPlace());
    }
    ((sQueuable)val).getQueuable().queueUp(eq);
  }

  public void queueDown(SimulationEventQueue eq)
    throws IntolerableException, 
           StopException
  {
    if(isNull())
    {
      throw new UserRuntimeError("Dequeue operation on null reference "+
                          getFullPathName()+", defined "+this.getPlace());
    }
    ((sQueuable)val).getQueuable().queueDown(eq);
  }

  public void reset()
    throws IntolerableException
  {
    throw new InternalError(
      "Method queuableReference.reset was called!");
  }

  public void setFullPathName(String fpn) 
    throws IntolerableException
  {
    fullPathName = fpn;
  }

  public Scope getLocalScope()
  {
    return ((sQueuable)val).getQueuable().getLocalScope();
  }

  public void addMember(VariableInstance si)
    throws IntolerableException
  {
    ((sQueuable)val).getQueuable().addMember(si);
  }

  public int getSize()
  {
    return ((sQueuable)val).getQueuable().getSize();
  }

  public VariableInstance getMember(int i)
    throws IntolerableException
  {
    return ((sQueuable)val).getQueuable().getMember(i);
  }

  public VariableInstance getMember(String name)
    throws IntolerableException
  {
    if(val == null)
    {
      throw new InternalError(
        "Queuable reference variable "+getFullPathName()+" was\n"+
        "dereferenced when null worth!"); 
    }
    Enqueuable eq = ((sQueuable)val).getQueuable();
    if(eq == null)
    {
      throw new UserRuntimeError(
        "Queuable reference variable "+getFullPathName()+", defined "+this.getPlace()+" was\n"+
        "dereferenced when null worth!"); 
    }
    return ((sQueuable)val).getQueuable().getMember(name);
  }

}

