package se.modlab.simplesim.references;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.exceptions.*;

public interface ReferenceEvaluable 
{

	public sValue evaluate(Scope s) throws IntolerableException;

	public void verify(Scope s) throws IntolerableException;

	public String reproduceExpression();


}
