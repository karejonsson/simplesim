package se.modlab.simplesim.references;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.evaluables.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.generics.exceptions.*;

public class QueuableVariableUpdate extends VarUpdate
{

	private VariableLookup give;

	public QueuableVariableUpdate(VariableLookup _receive,
			VariableLookup _give,
			String filename, int line, int column)
	{
		super(_receive, filename, line, column);
		give = _give;
	}

	public void execute(Scope s) 
	throws ReturnException, 
	IntolerableException, 
	StopException,
	BreakException,
	ContinueException
	{
		VariableInstance si_give = give.getInstance(s);
		VariableInstance si_receive = vl.getInstance(s);
		si_receive.setValue(si_give);
	}

	public void verify(Scope s) throws IntolerableException {
		VariableInstance si_give = give.getInstance(s);
		if(si_give == null) {
			throw new UserRuntimeError("Variable "+give+" does not exist.");
		}
		VariableInstance si_receive = vl.getInstance(s);
		if(si_receive == null) {
			throw new UserRuntimeError("Variable "+vl+" does not exist.");
		}
		si_receive.setDefaultInitialValue();
	}

}
