package se.modlab.simplesim.references;

import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.events.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.tables.*;

public class TypedStructReference extends StructReference
{

  private String fullPathName;

  public TypedStructReference(String name, VariableType vt, String _filename, int _line, int _column)
  {
    super(name, vt, _filename, _line, _column);
    fullPathName = name;
  }

  public void setFullPathName(String fpn)
  {
    fullPathName = fpn;
  }

  public String getFullPathName()
  {
    return fullPathName;
  }

  public sValue getValue()
    throws IntolerableException
  {
    if(val == null)
    {
      throw new UserRuntimeError("Reference "+getFullPathName()+", defined "+this.getPlace()+
                          " was dereferenced");
    }
    return val;
  }

  public void setValue(VariableInstance si)
    throws IntolerableException
  {
    if(si == null)
    {
      val = new sStruct();
      return;
    }
    if(si instanceof StructReference)
    {
      sValue v = ((StructReference) si).getValue();
      VariableInstance vi = (VariableInstance) ((sStruct) v).getStruct();
      if(vi == null)
      {
        val = new sStruct();
        return;
      }  
      if(getType() != vi.getType())
      {
        throw new UserRuntimeError(
          "Variable reference "+fullPathName+", defined "+this.getPlace()+" of type "+getType().getTypesName()+"\n"+
          "was set to reference a struct of type "+vi.getType().getTypesName());
      }
      val = new sStruct((Complex) vi);
      return;
    }
    if(getType() != si.getType())
    {
      throw new UserRuntimeError(
        "Variable reference "+fullPathName+", defined "+this.getPlace()+" of type "+getType().getTypesName()+"\n"+
        "was set to reference a struct of type "+si.getType().getTypesName());
    }
    if(si instanceof Complex)
    {
      // This is the handler of null 
      // Comment above is wrong. It is at least not only null!
      val = new sStruct((Complex) si);
      return; 
    }
    throw new UserRuntimeError(
      "Instance "+si+" is not struct!\n"+
      "Variable reference "+fullPathName+" referring to type\n"+
      vt.getTypesName()+" cannot take its value");
  }

  public void setValue(sValue _val) 
    throws IntolerableException
  {
    if(_val == null)
    {
      val = new sStruct();
      return;
    }
    if(!(_val instanceof sStruct))
    {
      throw new UserRuntimeError(
        "Variable reference "+fullPathName+", defined "+this.getPlace()+" referring to type\n"+
        vt.getTypesName()+" cannot take a non struct value.");
    }
    Complex cp = ((sStruct) _val).getStruct();
    if(cp == null)
    {
      val = _val;
      return;
    }
    if(!(cp instanceof VariableInstance))
    {
      throw new InternalError(
        "In typedStructReference a struct is not of type\n"+
        "variableInstance. The actual type was "+
        cp.getClass().getName()+".");
    }
    VariableInstance si = (VariableInstance) cp;
    if(getType() != si.getType())
    {
      throw new UserRuntimeError(
        "Variable reference "+fullPathName+", defined "+this.getPlace()+" of type \""+getType().getTypesName()+"\"\n"+
        "was set to reference a queuable of type "+si.getType().getTypesName());
    }
    val = _val;
  }

  public Class getValueClass()
  {
    return new sStruct().getClass();
  }

  public void setInitialValue(Holder val) 
    throws IntolerableException
  {
    throw new InternalError(
      "typedStructReference.setInitialValue(holder) was called.\n"+
      "Sinse references cannot come from tablefiles this is only wrong!");
  }

  public void setInitialValue(sValue initialVal) 
    throws IntolerableException
  {
    if(initialVal == null)
    {
      val = new sStruct();
      return;
    }
    setValue(initialVal);
  }

  public void setInitialValue(VariableInstance _val) 
    throws IntolerableException
  {
    if(_val == null)
    {
      val = new sStruct();
      return;
    }
    if(_val instanceof StructReference)
    {
      sValue v = ((StructReference) _val).getValue();
      VariableInstance vi = (VariableInstance) ((sStruct) v).getStruct();
      if(vi == null)
      {
        val = new sStruct();
        return;
      }  
      if(getType() != vi.getType())
      {
        throw new UserRuntimeError(
          "Variable reference "+fullPathName+", defined "+this.getPlace()+" of type "+getType().getTypesName()+"\n"+
          "was set to reference a struct of type "+vi.getType().getTypesName());
      }
      val = new sStruct((Enqueuable) vi);
      return;
    }    
    if(getType() != _val.getType())
    {
      throw new UserRuntimeError(
        "Variable reference "+fullPathName+", defined "+this.getPlace()+" of type "+getType().getTypesName()+"\n"+
        "was set to reference a struct of type "+_val.getType().getTypesName());
    }
    if(!(_val instanceof Complex))
    {
      throw new UserRuntimeError(
        "Instance "+_val+" is not a struct!\n"+
        "Variable reference "+fullPathName+", defined "+this.getPlace()+" referring to type "+vt.getTypesName()+
        " cannot take its value.");
    }
    val = new sStruct((Complex)_val);
  }

  public void setDefaultInitialValue()
    throws IntolerableException
  {
    setInitialValue(new sStruct());
  }

  public String toString()
  {
    return "Typed struct reference, "+super.toString();
  }

}
