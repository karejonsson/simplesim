package se.modlab.simplesim.references;

import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.events.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.tables.*;

public class TypedQueuableReference extends QueuableReference
{

  private String fullPathName;

  public TypedQueuableReference(String name, VariableType vt, String _filename, int _line, int _column)
  {
    super(name, vt, _filename, _line, _column);
    fullPathName = name;
  }

  public void setFullPathName(String fpn)
  {
    fullPathName = fpn;
  }

  public String getFullPathName()
  {
    return fullPathName;
  }

  public sValue getValue()
    throws IntolerableException
  {
    if(val == null)
    {
      throw new UserRuntimeError("Reference "+getFullPathName()+", defined "+this.getPlace()+
                          " was dereferenced");
    }
    return val;
  }

  public void setValue(VariableInstance si)
    throws IntolerableException
  {
    if(si == null)
    {
      val = new sQueuable();
      return;
    }
    if(si instanceof QueuableReference)
    {
      sValue v = ((QueuableReference) si).getValue();
      VariableInstance vi = (VariableInstance) ((sQueuable) v).getQueuable();
      if(vi == null)
      {
        val = new sQueuable();
        return;
      }  
      if(getType() != vi.getType())
      {
        throw new UserRuntimeError(
          "Variable reference "+fullPathName+", defined "+this.getPlace()+" of type "+getType().getTypesName()+"\n"+
          "was set to reference a queuable of type "+vi.getType().getTypesName());
      }
      val = new sQueuable((Enqueuable) vi);
      return;
    }
    if(getType() != si.getType())
    {
      throw new UserRuntimeError(
        "Variable reference "+fullPathName+", defined "+this.getPlace()+" of type "+getType().getTypesName()+"\n"+
        "was set to reference a queuable of type "+si.getType().getTypesName());
    }
    if(si instanceof Enqueuable)
    {
      // This is the handler of null
      val = new sQueuable((Enqueuable) si);
      return; 
    }
    throw new UserRuntimeError(
      "Instance "+si+" is not queuable!\n"+
      "Variable reference "+fullPathName+", defined "+this.getPlace()+" referring to type\n"+
      vt.getTypesName()+" cannot take its value");
  }

  public void setValue(sValue _val) 
    throws IntolerableException
  {
    if(_val == null)
    {
      val = new sQueuable();
      return;
    }
    if(!(_val instanceof sQueuable))
    {
      System.out.println("typedQueuableReference.setValue. Instance "+_val);
      if(_val != null) {
    	  System.out.println("typedQueuableReference.setValue. class "+_val.getClass().getName());
      }
      throw new UserRuntimeError(
        "Variable reference "+fullPathName+", defined "+this.getPlace()+" referring to type\n"+
        vt.getTypesName()+" cannot take a non queuables value.");
    }
    Enqueuable eq = ((sQueuable) _val).getQueuable();
    if(eq == null)
    {
      val = _val;
      return;
    }
    if(!(eq instanceof VariableInstance))
    {
      throw new InternalError(
        "In TypedQueuableReference an enqueuable is not of type\n"+
        "VariableInstance. The actual type was "+
        eq.getClass().getName()+".");
    }
    VariableInstance si = (VariableInstance) eq;
    if(getType() != si.getType()) {
    	VariableType tsource = getType();
    	String sourcetype = tsource == null ? "Unknown" : tsource.getTypesName();
    	VariableType ttarget = si.getType();
    	String targettype = ttarget == null ? "Unknown" : ttarget.getTypesName();
      throw new UserRuntimeError(
        "Variable reference "+fullPathName+", defined "+this.getPlace()+" of type \""+sourcetype+"\"\n"+
        "was set to reference a queuable of type "+targettype);
    }
    val = _val;
  }

  public Class getValueClass()
  {
    return new sQueuable().getClass();
  }

  public void setInitialValue(Holder val) 
    throws IntolerableException
  {
    throw new InternalError(
      "typedQueueableReference.setInitialValue(holder) was called.\n"+
      "Sinse references cannot come from tablefiles this is only wrong!");
  }

  public void setInitialValue(sValue initialVal) 
    throws IntolerableException
  {
    setValue(initialVal);
  }

  public void setInitialValue(VariableInstance _val) 
    throws IntolerableException
  {
    if(_val == null)
    {
      val = new sQueuable();
      return;
    }
    if(_val instanceof QueuableReference)
    {
      sValue v = ((QueuableReference) _val).getValue();
      VariableInstance vi = (VariableInstance) ((sQueuable) v).getQueuable();
      if(vi == null)
      {
        val = new sQueuable();
        return;
      }  
      if(getType() != vi.getType())
      {
        throw new UserRuntimeError(
          "Variable reference "+fullPathName+", defined "+this.getPlace()+" of type "+getType().getTypesName()+"\n"+
          "was set to reference a queuable of type "+vi.getType().getTypesName());
      }
      val = new sQueuable((Enqueuable) vi);
      return;
    }    
    if(getType() != _val.getType())
    {
      throw new UserRuntimeError(
        "Variable reference "+fullPathName+", defined "+this.getPlace()+" of type "+getType().getTypesName()+"\n"+
        "was set to reference a queuable of type "+_val.getType().getTypesName());
    }
    if(!(_val instanceof Enqueuable))
    {
      throw new UserRuntimeError(
        "Instance "+_val+", defined "+this.getPlace()+" is not queuable!\n"+
        "variable reference "+fullPathName+" referring to type "+vt.getTypesName()+
        " cannot take its value.");
    }
    val = new sQueuable((Enqueuable)_val);
  }

  public void setDefaultInitialValue()
    throws IntolerableException
  {
    setInitialValue(new sQueuable());
  }

  public String toString()
  {
    return "Typed queuable reference, "+super.toString();
  }

}
