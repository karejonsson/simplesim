package se.modlab.simplesim.references;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.evaluables.*;
import se.modlab.generics.sstruct.executables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.variables.*;
import se.modlab.simplesim.references.*;

public class ReferenceVariableVector
       extends VariableVector
{

  private VariableFactory trueFactory;

  public ReferenceVariableVector(String name, 
                                 VariableType vt,
                                 VariableFactory vf, String _filename, int _line, int _column,
                                 int length,
                                 VariableFactory _trueFactory)
    throws IntolerableException
  {
    super(name, vt, _filename, _line, _column, vf, length);
    trueFactory = _trueFactory;
    replacingInitiateVector(length);
  }

  protected void initiateVector(int length)
    throws IntolerableException
  {
  }

  protected void replacingInitiateVector(int length)
    throws IntolerableException
  {
    siv = new VariableInstance[length];
    for(int i = 0 ; i < length ; i++)
    {
      siv[i] = trueFactory.getInstance("["+i+"]", filename, line, column);
    }
  }

}
