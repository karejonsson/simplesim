package se.modlab.simplesim.references;

import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.events.*;
import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.comparisons.*;

public class ArithmeticEvaluableFakerReferenceHolder 
implements ArithmeticEvaluable
{

	ReferenceEvaluable re = null;

	public ArithmeticEvaluableFakerReferenceHolder(ReferenceEvaluable _re)
	{
		re = _re;
	}

	public ReferenceEvaluable getReferenceEvaluable()
	{
		return re;
	}

	public sValue evaluate(Scope s)
	throws IntolerableException
	{
		return re.evaluate(s);
		//throw new userError( 
		//  return re.toString()+" cannot be evaluated arithmetically.";);
	}

	public String toString()
	{
		return "arithmeticEvaluableFakerReferenceHolder holding "+re;
	}

	public void verify(Scope s) throws IntolerableException {
		re.verify(s);
	}
	
	public String reproduceExpression() {
		return re.reproduceExpression();
	}

}


