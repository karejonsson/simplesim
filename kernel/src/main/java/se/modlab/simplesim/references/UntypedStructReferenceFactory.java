package se.modlab.simplesim.references;

import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.scoping.*;

public class UntypedStructReferenceFactory extends StructReferenceFactory
{
  public static final String typesname = "struct"; 

  public UntypedStructReferenceFactory(SimScope _s)
  {
    super(_s, typesname);
  }

  public VariableInstance getInstance(String name, String filename, int line, int column)
    throws IntolerableException
  {
    UntypedStructReference out = 
      new UntypedStructReference(name, unitsFactory,filename, line, column);
    return out;
  }

  public String toString()
  {
    return "Untyped struct reference factory";
  }

}