package se.modlab.simplesim.references;

import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.variables.*;
import se.modlab.simplesim.scoping.*;
import se.modlab.simplesim.references.*;

public class TypedStructReferenceFactory extends StructReferenceFactory
{
 
  public TypedStructReferenceFactory(SimScope _s, 
                                     String _typesname,
                                     boolean _isSharp)
  {
    super(_s, _typesname);
  }

  public VariableInstance getInstance(String name, String filename, int line, int column)
    throws IntolerableException
  {
    return getInstance(name, false, filename, line, column);
  }

  public VariableInstance getInstance(String name, boolean monitor, String filename, int line, int column)
    throws IntolerableException
  {
    TypedStructReference out = 
      new TypedStructReference(name, unitsFactory, filename, line, column);
    return out;
  }

  public String toString()
  {
    return "Typed struct reference factory for "+getTypesName();
  }

}