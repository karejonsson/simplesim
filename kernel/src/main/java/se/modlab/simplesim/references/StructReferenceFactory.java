package se.modlab.simplesim.references;

import java.util.Hashtable;

import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.variables.*;
import se.modlab.simplesim.scoping.*;

public class StructReferenceFactory implements SimVariableFactory, StructFactory
{
	protected SimScope s; 
	protected String typesname; 
	protected VariableFactory unitsFactory;

	public StructReferenceFactory(SimScope _s, String _typesname) {
		super();
		s = _s;
		typesname = _typesname;
		unitsFactory = s.getFactory(typesname);
	}

	public VariableInstance getInstance(String name, String _filename, int _line, int _column, Hashtable<VariableInstance, Manager> managermap)
			throws IntolerableException
			{
		return getInstance(name, false, _filename, _line, _column, managermap);
			}

	public VariableInstance getInstance(String name, boolean monitor, String _filename, int _line, int _column, Hashtable<VariableInstance, Manager> managermap)
			throws IntolerableException
			{
		UntypedStructReference out = 
				new UntypedStructReference(name, unitsFactory, _filename, _line, _column);
		return out;
			}

	public String getTypesName()
	{
		return typesname;
	}

	public VariableInstance getInstance(String name, String filename, int line, int column)
			throws IntolerableException
			{
		System.out.println("Call to StructReferenceFactory.getInstance(s,s,i,i) i.e. addin null in class "+getClass().getName());
		return getInstance(name, false, filename, line, column, null);
			}

	public VariableInstance getInstance(String name, boolean monitor, String filename, int line, int column)
			throws IntolerableException {
		System.out.println("Call to StructReferenceFactory.getInstance(s,b,s,i,i) i.e. addin null in class "+getClass().getName());
		return getInstance(name, monitor, filename, line, column, null);
	}

}