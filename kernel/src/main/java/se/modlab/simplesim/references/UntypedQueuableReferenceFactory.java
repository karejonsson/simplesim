package se.modlab.simplesim.references;

import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.sstruct.comparisons.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.variables.*;
import se.modlab.simplesim.scoping.*;
import se.modlab.simplesim.references.*;

public class UntypedQueuableReferenceFactory extends QueuableReferenceFactory
{
  public static final String typesname = "queuable"; 

  public UntypedQueuableReferenceFactory(SimScope _s)
  {
    super(_s, typesname);
  }

  public VariableInstance getInstance(String name, String _filename, int _line, int _column)
    throws IntolerableException
  {
    UntypedQueuableReference out = 
      new UntypedQueuableReference(name, unitsFactory, _filename, _line, _column);
    return out;
  }

  public String toString()
  {
    return "Untyped queuable reference factory";
  }

}