package se.modlab.simplesim.references;

import java.util.Date;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.VariableInstance;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.events.*;

public class sQueuable extends sValue
{

  public final static Class _class = new sQueuable().getClass();
  private Enqueuable val;

  public sQueuable()
  {
  }

  public sQueuable(Enqueuable _val)
  {
    val = _val;
  }

  public sQueuable(sQueuable _val)
  {
    val = _val.getQueuable();
  }

  public sValue copy()
  {
    return new sQueuable(val);
  }

  public void setValue(Enqueuable qr)
  {
    val = qr;
  }

  public double getValue()
    throws IntolerableException
  {
    throw new UserRuntimeError("Queuable reference value may not be "+
                        "arithmetically evaluated.");
  }

  public String getString()
  {
    return null;
  }

  public Enqueuable getQueuable()
  {
    return val;
  }

  public Long getLong()
  {
    return null;
  }

  public Double getDouble()
  {
    return null;
  }

  public Boolean getBoolean()
  {
    return null;
  }

  public boolean equals(sValue sv)
    throws IntolerableException
  {
    if(!(sv instanceof sQueuable)) 
    {
      throw new UserCompiletimeError(
        "You cannot operate with equality operators on\n"+
        this+" and "+sv);
    }
    return val == ((sQueuable) sv).val;
  }

  public boolean less(sValue sv)
    throws IntolerableException
  {
    throw new UserCompiletimeError(
      "You cannot operate with magnitude operators on\n"+
      this+" and "+sv);
  }

  public boolean lessEq(sValue sv)
    throws IntolerableException
  {
    throw new UserCompiletimeError(
      "You cannot operate with magnitude operators on\n"+
      this+" and "+sv);
  }

  public String toString()
  {
    return ""+val+", type queuable";
  }

	public Date getDate() {
		return null;
	}

	public Object getObject() {
		return val;
	}
	
	public boolean valueEquals(sValue otherVal) throws IntolerableException {
		Object obj = otherVal.getObject();
		if(!(obj instanceof VariableInstance)) {
			return false;
		}
		if(val == null) {
			return obj == null;
		}
		return val.valueEquals((VariableInstance) obj);
	}

}