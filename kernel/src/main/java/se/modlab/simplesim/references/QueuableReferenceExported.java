package se.modlab.simplesim.references;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.simplesim.variables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.generics.sstruct.tables.*;

public class QueuableReferenceExported 
       extends QueuableReference implements VariableConst
{
  private QueuableReference var;
  
  public QueuableReferenceExported(QueuableReference var)
  {
    super(var.getName(), var.getType(), var.getFilename(), var.getLine(), var.getColumn());
    this.var = var;
  }
 
  public sValue getValue()
    throws IntolerableException
  {
    return var.getValue();
  }

/*
  public void resetInitialValue() 
    throws intolerableException
  {
    throw new intolerableException(
      "Internal: queuableReferenceExported.resetInitialValue was called.");
  }
*/

  public void setValue(sValue val) 
    throws IntolerableException
  {
    throw new UserRuntimeError(
      "Queuable reference exported variable "+name+", defined "+this.getPlace()+" set to new value "+val);
  }

  public void setValue(VariableInstance si)
    throws IntolerableException
  {
    throw new UserRuntimeError(
      "Queuable reference exported variable "+name+", defined "+this.getPlace()+" set to new value "+si);
  }

  public void setValue(QueuableReference qr)
    throws IntolerableException
  {
    throw new UserRuntimeError(
      "Queuable reference exported variable "+name+", defined "+this.getPlace()+" set to new value "+qr);
  }

  public void setInitialValue(Holder val) 
    throws IntolerableException
  {
    throw new InternalError(
      "queueableReferenceExported.setInitialValue(holder) was called.\n");
  }

  public void setInitialValue(sValue initialVal) 
    throws IntolerableException
  {
    throw new InternalError(
      "queueableReferenceExported.setInitialValue(sValue) was called.\n");
  }

  public void setInitialValue(VariableInstance _val) 
    throws IntolerableException
  {
    throw new InternalError(
      "typedQueuableReference.setInitialValue(variableInstance)\n"+
      "called with object of class "+_val.getClass().getName()+".\n"+
      "This method should not be called at all!");
  }

  public void setDefaultInitialValue()
    throws IntolerableException
  {
    throw new InternalError(
      "queueableReferenceExported.setDefaultInitialValue() was called.\n");
  }

  public String toString()
  {
    return var.toString()+" exported.";
  }

  public Class getValueClass()
  {
    return new sQueuable().getClass();
  }

}