package se.modlab.simplesim.references;

import java.util.Date;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.exceptions.*;
import se.modlab.simplesim.events.*;

public class sStruct extends sValue {

	public final static Class _class = new sQueuable().getClass();
	private Complex val;

	public sStruct() {
	}

	public sStruct(Complex _val) {
		val = _val;
	}

	public sStruct(sStruct _val) {
		val = _val.getStruct();
	}

	public sValue copy() {
		return new sStruct(val);
	}

	public void setValue(Complex cp) {
		val = cp;
	}

	public double getValue() throws IntolerableException {
		throw new UserRuntimeError("Struct reference value may not be "
				+ "arithmetically evaluated.");
	}

	public String getString() {
		return null;
	}

	public Complex getStruct() {
		return val;
	}

	public Enqueuable getQueuable() {
		return null;
	}

	public Long getLong() {
		return null;
	}

	public Double getDouble() {
		return null;
	}

	public Boolean getBoolean() {
		return null;
	}

	public boolean equals(sValue sv) throws IntolerableException {
		if (!(sv instanceof sStruct)) {
			throw new UserCompiletimeError(
					"You cannot operate with equality operators on\n" + this
							+ " and " + sv);
		}
		Complex ss = ((sStruct) sv).val; 
		if(val == ss) {
			return true;
		}
		return val.equals(ss);
	}

	public boolean less(sValue sv) throws IntolerableException {
		throw new UserCompiletimeError(
				"You cannot operate with magnitude operators on\n" + this
						+ " and " + sv);
	}

	public boolean lessEq(sValue sv) throws IntolerableException {
		throw new UserCompiletimeError(
				"You cannot operate with magnitude operators on\n" + this
						+ " and " + sv);
	}

	public String toString() {
		return "" + val + ", type struct";
	}

	public Date getDate() {
		return null;
	}

	public Object getObject() {
		return val;
	}
	
	public boolean valueEquals(sValue otherVal) throws IntolerableException {
		Object obj = otherVal.getObject();
		if(!(obj instanceof VariableInstance)) {
			return false;
		}
		return val.valueEquals((VariableInstance) obj);
	}


}