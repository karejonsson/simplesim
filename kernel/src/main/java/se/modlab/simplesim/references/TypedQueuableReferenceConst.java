package se.modlab.simplesim.references;

import se.modlab.generics.sstruct.values.*;
import se.modlab.generics.sstruct.variables.*;
import se.modlab.generics.exceptions.*;

public class TypedQueuableReferenceConst 
       extends TypedQueuableReference implements VariableConst
{
  
  public TypedQueuableReferenceConst(String name, VariableType vt, String _filename, int _line, int _column)
  {
    super(name, vt, _filename, _line, _column);
  }

  public TypedQueuableReferenceConst(TypedQueuableReference tqr)
    throws IntolerableException
  {
    super(tqr.getName(), tqr.getType(), tqr.getFilename(), tqr.getLine(), tqr.getColumn());
    super.setValue(tqr.getValue());
  }

  public void setValue(sValue val) 
    throws IntolerableException
  {
    throw new UserRuntimeError(
      "Typed queuable reference constant "+name+", defined "+this.getPlace()+" set to new value "+val);
  }

  public void setValue(VariableInstance si)
    throws IntolerableException
  {
    throw new UserRuntimeError(
      "Typed queuable reference constant "+name+", defined "+this.getPlace()+" set to new value "+si);
  }

  public String toString()
  {
    return "Typed queuable reference constant, "+super.toString();
  }

}
