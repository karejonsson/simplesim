package se.modlab.tables.excel;

import java.sql.*;
import java.util.*;

public class Manipulations 
{
  
  public static String getBareName(String name)
  {
    if(name.startsWith("'") && name.endsWith("$\'")) 
    {
      return name.substring(1, name.length()-2);
    }
    if(name.endsWith("$"))
    {
      return name.substring(0, name.length()-1);
    }
    return name;
  }

  public static String getCompletedName(String name)
  {
    if(name.startsWith("'") && name.endsWith("$\'")) 
    {
      return name;
    }
    if(name.endsWith("$"))
    {
      return name;
    }
    if(name.indexOf(" ") != -1) 
    {
      return "'"+name+"$'";
    }
    return name+"$";
  }


  public static void main(String args[]) {
    String x = "'collapse DNS$'";
    String y = getBareName(x);
    String z = getCompletedName(y);
    System.out.println("Test : "+x+" -> "+y+" -> "+z);
    x = "DNS$";
    y = getBareName(x);
    z = getCompletedName(y);
    System.out.println("Test : "+x+" -> "+y+" -> "+z);
  }

}

