package se.modlab.tables.excel;

import java.sql.*;
import java.util.*;

public class Read 
{

  public static ResultSet readExcelSheet(String filename, String sheet) throws Exception
  {
    Connection c = null;
    Statement stmnt = null;
    try { 
      Class.forName("sun.jdbc.odbc.JdbcOdbcDriver" );
      c = DriverManager.getConnection("jdbc:odbc:Driver={Microsoft Excel Driver (*.xls)};DBQ=" + filename);
      stmnt = c.createStatement();
      String query = "Select * from ["+sheet+"]" ;
      return stmnt.executeQuery( query );
    }
    catch(Exception e) {
      throw e;
    }
  }

  public static void readExcel(String filename, String tab)
  {
    Connection c = null;
    Statement stmnt = null;
    try 
    { 
      Class.forName("sun.jdbc.odbc.JdbcOdbcDriver" );
      c = DriverManager.getConnection("jdbc:odbc:Driver={Microsoft Excel Driver (*.xls)};DBQ=" + filename);
      stmnt = c.createStatement();
      String query = "Select * from ["+tab+"$]" ;
      ResultSet rs = stmnt.executeQuery( query );
      while( rs.next() )
      {
        System.out.println( rs.getString(1) );
      }
    }
    catch( Exception e )
    {
      System.err.println( "Exception: "+e );
    }
  }

  public static void readMetaData(String filename) 
  {
    Connection c = null;
    Statement stmnt = null;
    try 
    { 
      Class.forName("sun.jdbc.odbc.JdbcOdbcDriver" );
      c = DriverManager.getConnection("jdbc:odbc:Driver={Microsoft Excel Driver (*.xls)};DBQ=" + filename);
      DatabaseMetaData metaData = c.getMetaData();
      
      //ResultSet rs = metaData.getCatalogs(); 
      //ResultSet rs = metaData.getTableTypes(); 
      //ResultSet rs = metaData.getSchemas(); 
      ResultSet rs = metaData.getTables(null, null, "%", null); 
      int ctr = 0;
      while( rs.next() )
      {
        System.out.println( "Ctr "+(ctr++)+" : "+rs.getString(1)+ " : "+rs.getString(2) );
      }
    }
    catch( Exception e )
    {
      System.err.println( "Exception: "+e );
    }
  }

  public static String[] getSheetNames(String filename)
  {
    Vector<String> v = new Vector<String>();
    Connection c = null;
    Statement stmnt = null;
    String data="";
    try {
      Class.forName( "sun.jdbc.odbc.JdbcOdbcDriver" );
      //c = DriverManager.getConnection("jdbc:odbc:Driver={Microsoft Excel Driver (*.xls)};DBQ=" + filename);
      c = DriverManager.getConnection("jdbc:odbc:Driver={Microsoft Excel Driver (*.xls)};DBQ="+filename+";DriverID=22;READONLY=false","","");
           
      stmnt = c.createStatement();
      DatabaseMetaData dbmd=c.getMetaData();
      ResultSet set = dbmd.getTables(null,null,null,null);
      int i=0;
      while( set.next() ) 
      {
        v.addElement(set.getString(3));
      }
    }
    catch(Exception e) {
      System.err.println( "Exception: "+e );
    }
    String out[] = new String[v.size()];
    for(int i = 0 ; i < v.size() ; i++) {
      out[i] = (String) v.elementAt(i);
    }
    return out;
  } 

  public static void print(String a[]) {
    for(int i = 0 ; i < a.length ; i++) {
      System.out.println("NR "+i+" : "+a[i]);
    }
  }

  public static void main(String args[]) 
  {
    //readExcel("tpm2.xls", "Rules");
    //readExcel("DPDSM.xls", "DSM");
    //readExcel("DPDSM.xls", "TABLE");
    print(getSheetNames("DPDSM.xls"));
  }

}

