package se.modlab.tables.editor.koders;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.KeyEvent;

import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author tonyj
 */
public class FindDialog extends JOptionPane
{
   private JTextField textField;
   private JCheckBox caseSensitiveBox;
   private JCheckBox matchCellBox;
   /** Creates a new instance of FindDialog */
   public FindDialog(String findValue, boolean mCase, boolean mCell)
   {
      textField = new JTextField(findValue);
      
      caseSensitiveBox = new JCheckBox("Match Case");
      caseSensitiveBox.setMnemonic(KeyEvent.VK_M);
      caseSensitiveBox.setSelected(mCase);
      
      matchCellBox = new JCheckBox("Match Entire Cell Only");
      matchCellBox.setMnemonic(KeyEvent.VK_E);
      matchCellBox.setSelected(mCell);
      
      JPanel box = new JPanel(new BorderLayout(0, 5));
      
      box.add(textField, BorderLayout.NORTH);
      box.add(caseSensitiveBox, BorderLayout.WEST);
      box.add(matchCellBox, BorderLayout.EAST);
      setMessage(box);
      setIcon(SpreadsheetDemo.getIcon("find32"));
      setOptionType(OK_CANCEL_OPTION);
   }

   public boolean isCaseSensitive()
   {
      return caseSensitiveBox.isSelected();
   }
   
   public boolean isCellMatching()
   {
      return matchCellBox.isSelected();
   }
   
   public String getString()
   {
      return textField.getText();
   }

   public int show(Component parent, String title)
   {
      JDialog dlg = createDialog(parent,title);
      textField.selectAll();
      textField.requestFocus();
      dlg.pack();
      dlg.setVisible(true);
      Object object = getValue();
      return object instanceof Integer ? ((Integer) object).intValue() : CLOSED_OPTION;
   }
}
